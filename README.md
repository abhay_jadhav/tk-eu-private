# README #
This project contains the Thyssenkrupp Europe code base.

### Pre-requisites ###
1. Access to http://artifacts.mindcurv.com/
2. Maven
3. Gradle
4. A MySQL database
    - The schema for EU is hybris_eu , which needs to be created 
           (Refer document : https://tk-bamx.atlassian.net/wiki/spaces/CTKS/pages/158269676/Create+database+schema for schema creation) 
    - Please align with schema, userId, password in local.properties, when creating schema for EU.

**Note****: Pre-requisites #2 -#4 can be ignored if you are using virtual machine. Details can be found in section `Using the Developer VM`.

### Steps ###

1. Clone the repo into a desired location. `git clone git@bitbucket.org:thyssenkruppbamx/tk-eu.git`
2. Go to tk-eu folder `cd tk-eu` and run `gradle setUp`. This will download hybirs commerce and baseshop and install in the current location.
3. To start the server. Goto `cd /hybris/bin/platform` and start the server by `sh hybrisserver.sh`
4. Initialize the system by accessing https://localhost:9002/admin
6. Add the host entries in `/etc/hosts `

```
#!

127.0.0.1       thyssenkrupp-plastics.local
127.0.0.1       thyssenkrupp-schulte.local
```

7. To view the website, use browser https://thyssenkrupp-plastics.local:9002/ or https://thyssenkrupp-schulte.local:9002/

### Using the Developer VM ###
In case you are using the developer VM the above prerequisites(#2-#4) are already installed. For more info please see https://bitbucket.org/thyssenkruppbamx/tk-vm

### Set up project in IDE ###
Please follow hybris recommendation to set up projects in IDE:
https://wiki.hybris.com/display/hybrisALF/SAP+Hybris+Commerce+Development+Tools+for+Eclipse



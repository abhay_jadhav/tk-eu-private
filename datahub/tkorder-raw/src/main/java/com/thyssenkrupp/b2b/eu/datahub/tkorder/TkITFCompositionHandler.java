package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.runtime.domain.DataHubAction;
import com.hybris.datahub.saperpproduct.composition.itf.ItfLine;
import com.hybris.datahub.saperpproduct.composition.itf.ItfText;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TkITFCompositionHandler implements CompositionRuleHandler {
    private static final Function<String, Locale>          LOCALE_NEW = Locale::new;
    private static final ConcurrentHashMap<String, Locale> LOCALES    = new ConcurrentHashMap();
    private              int                               order      = 900;
    private              String                            canonicalAttribute;
    private              String                            canonicalItem;
    private              String                            rawISO;
    private              String                            rawItem;
    private              String                            rawTDFORMAT;
    private              String                            rawTDLINE;
    private              String                            rawConditionField;
    private              String                            conditionValue;

    public TkITFCompositionHandler() {
    }

    @Override
    public <T extends CanonicalItem> T compose(CanonicalAttributeDefinition cad, CompositionGroup<? extends RawItem> cg, T canonicalItem) {
        Long lastActionId = (Long) cg.getItems().stream().map(RawItem::getDataLoadingAction).map(DataHubAction::getActionId).max(Comparator.naturalOrder()).get();
        List<? extends RawItem> items = (List) cg.getItems().stream().filter((r) -> lastActionId.equals(r.getDataLoadingAction().getActionId())).collect(Collectors.toList());
        List<ItfLine> list = new ArrayList();
        String lastIso = null;
        Iterator var8 = items.iterator();

        while(true) {
            String iso;
            Optional format;
            Optional text;
            RawItem item;
            do {
                if (!var8.hasNext()) {
                    if (lastIso != null && !list.isEmpty()) {
                        this.setLongText(canonicalItem, list, lastIso);
                    }

                    return canonicalItem;
                }

                item = (RawItem)var8.next();
                iso = (String)item.getField(this.rawISO);

                if ((iso == null || !iso.equals(lastIso)) && !list.isEmpty()) {
                    this.setLongText(canonicalItem, list, lastIso);
                    list.clear();
                    lastIso = null;
                }

                format = Optional.ofNullable((String)item.getField(this.rawTDFORMAT));
                text = Optional.ofNullable((String)item.getField(this.rawTDLINE));
            } while (!format.isPresent() && !text.isPresent());

            if (isConditionTrue(item)) {
                list.add(new ItfLine((String) format.orElse(""), (String) text.orElse("")));
                lastIso = iso;
            }
        }
    }

    private void removeConsecutiveDuplicates(List<?> list) {
        Iterator<?> iterator = list.iterator();

        Object next;
        for(Object old = iterator.next(); iterator.hasNext(); old = next) {
            next = iterator.next();
            if (next.equals(old)) {
                iterator.remove();
            }
        }

    }

    private <T extends CanonicalItem> void setLongText(T canonicalItem, List<ItfLine> list, String lastIso) {
        this.removeConsecutiveDuplicates(list);
        String longText = ItfText.convertItf(list);
        canonicalItem.setField(this.canonicalAttribute, longText, (Locale) LOCALES.computeIfAbsent(lastIso, LOCALE_NEW));
    }

    public int getOrder() {
        return this.order;
    }

    public boolean isApplicable(CanonicalAttributeDefinition cad) {
        if (!this.rawItem.equals(cad.getRawItemType())) {
            return false;
        } else {
            CanonicalAttributeModelDefinition camd = cad.getCanonicalAttributeModelDefinition();
            return this.canonicalAttribute.equals(camd.getAttributeName()) && this.canonicalItem.equals(camd.getCanonicalItemMetadata().getItemType());
        }
    }

    private boolean isConditionTrue(RawItem item) {
        String conditionValueFromField = Optional.ofNullable((String)item.getField(rawConditionField)).orElse("");
        return conditionValueFromField.equals(conditionValue);
    }

    @Required
    public void setRawConditionField(String rawConditionField) {
        assert rawConditionField != null : rawConditionField;

        assert !rawConditionField.isEmpty() : rawConditionField;

        this.rawConditionField = rawConditionField.intern();
    }

    @Required
    public void setConditionValue(String conditionValue) {
        assert conditionValue != null : conditionValue;

        assert !conditionValue.isEmpty() : conditionValue;

        this.conditionValue = conditionValue.intern();
    }

    @Required
    public void setCanonicalAttribute(String canonicalAttribute) {
        assert canonicalAttribute != null : canonicalAttribute;

        assert !canonicalAttribute.isEmpty() : canonicalAttribute;

        this.canonicalAttribute = canonicalAttribute.intern();
    }

    @Required
    public void setCanonicalItem(String canonicalItemType) {
        assert canonicalItemType != null : canonicalItemType;

        assert !canonicalItemType.isEmpty() : canonicalItemType;

        this.canonicalItem = canonicalItemType.intern();
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Required
    public void setRawISO(String rawISO) {
        assert rawISO != null : rawISO;

        assert !rawISO.isEmpty() : rawISO;

        this.rawISO = rawISO.intern();
    }

    @Required
    public void setRawItem(String rawItemType) {
        assert rawItemType != null : rawItemType;

        assert !rawItemType.isEmpty() : rawItemType;

        this.rawItem = rawItemType.intern();
    }

    @Required
    public void setRawTDFORMAT(String rawTDFORMAT) {
        assert rawTDFORMAT != null : rawTDFORMAT;

        assert !rawTDFORMAT.isEmpty() : rawTDFORMAT;

        this.rawTDFORMAT = rawTDFORMAT.intern();
    }

    @Required
    public void setRawTDLINE(String rawTDLINE) {
        assert rawTDLINE != null : rawTDLINE;

        assert !rawTDLINE.isEmpty() : rawTDLINE;

        this.rawTDLINE = rawTDLINE.intern();
    }
}

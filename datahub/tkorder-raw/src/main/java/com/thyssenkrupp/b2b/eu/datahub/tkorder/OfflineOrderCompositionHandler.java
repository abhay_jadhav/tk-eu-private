package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class OfflineOrderCompositionHandler implements CompositionRuleHandler {

    private static final Logger       LOGGER               = LoggerFactory.getLogger(OfflineOrderCompositionHandler.class);
    private static final String       HYBRIS_ORDER_ID_ATTR = "hybrisOrderId";
    private              int          order;
    private              String       rawItemName;
    private              List<String> canonicalItems;
    private              String       offlineOrderDistributionChannelValue;

    @Override
    public <T extends CanonicalItem> T compose(CanonicalAttributeDefinition canonicalAttributeDefinition, CompositionGroup<? extends RawItem> compositionGroup, T canonicalItem) {
        final String distributionChannel = (String) canonicalItem.getField("distributionChannel");
        final String sapOrderId = (String) canonicalItem.getField("orderId");
        final String hybrisOrderId = (String) canonicalItem.getField(HYBRIS_ORDER_ID_ATTR);
        if (getOfflineOrderDistributionChannelValue().equals(distributionChannel) && isNotEmpty(sapOrderId) && isEmpty(hybrisOrderId)) {
            LOGGER.debug("Populating additional fields for offline order with key {}", canonicalItem.getIntegrationKey());
            canonicalItem.setField(HYBRIS_ORDER_ID_ATTR, sapOrderId);
        }
        return canonicalItem;
    }

    @Override
    public boolean isApplicable(CanonicalAttributeDefinition canonicalAttributeDefinition) {
        final String itemType = canonicalAttributeDefinition.getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
        return getRawItemName().equals(canonicalAttributeDefinition.getRawItemType()) && getCanonicalItems().contains(itemType);
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Required
    public void setOrder(int order) {
        this.order = order;
    }

    public String getRawItemName() {
        return rawItemName;
    }

    @Required
    public void setRawItemName(String rawItemName) {
        this.rawItemName = rawItemName;
    }

    public List<String> getCanonicalItems() {
        return canonicalItems;
    }

    @Required
    public void setCanonicalItems(List<String> canonicalItems) {
        this.canonicalItems = canonicalItems;
    }

    public String getOfflineOrderDistributionChannelValue() {
        return offlineOrderDistributionChannelValue;
    }

    @Required
    public void setOfflineOrderDistributionChannelValue(String offlineOrderDistributionChannelValue) {
        this.offlineOrderDistributionChannelValue = offlineOrderDistributionChannelValue;
    }
}

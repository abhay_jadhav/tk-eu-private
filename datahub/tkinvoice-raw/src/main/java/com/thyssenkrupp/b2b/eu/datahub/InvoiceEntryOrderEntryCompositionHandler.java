package com.thyssenkrupp.b2b.eu.datahub;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.thyssenkrupp.b2b.eu.datahub.tkorder.TkOrderItemNumberConverter;
import org.springframework.util.CollectionUtils;

import java.util.*;


public class InvoiceEntryOrderEntryCompositionHandler implements CompositionRuleHandler {

    private static final String VALID_CANONICAL_TYPES = "CanonicalInvoiceEntryOrderEntry";
    private static final String ORDER_ENTRIES = "orderEntries";
    private static final String REQUIRED_QUALIFIER = "002";
    private int order;
    private TkOrderItemNumberConverter orderItemNumberConverter;

    public <T extends CanonicalItem> T compose(CanonicalAttributeDefinition cad, CompositionGroup<? extends RawItem> cg, T canonicalItem) {
        String qualifier = (String) (cg.getItems().get(0)).getField("E1EDP01-E1EDP02-QUALF");
        if ((REQUIRED_QUALIFIER).equalsIgnoreCase(qualifier)) {
            String orderId = (String) canonicalItem.getField("orderId");
            Set<String> entryNumbers = (Set) (canonicalItem.getField(ORDER_ENTRIES));
            List<String> modifiedEntryNumbers = new ArrayList<>();
            if (!CollectionUtils.isEmpty(entryNumbers)) {
                for (String entryNumber : entryNumbers) {
                    String convertedEntryNumber = this.orderItemNumberConverter.convertFromErpToHybris(entryNumber, orderId);
                    modifiedEntryNumbers.add(new StringBuilder(orderId).append(":").append("<null>").append(":").append(convertedEntryNumber).toString());
                }
                canonicalItem.setField(ORDER_ENTRIES, modifiedEntryNumbers);
            }
        }

        return canonicalItem;
    }

    public boolean isApplicable(CanonicalAttributeDefinition canonicalAttributeDefinition) {
        boolean itemStatus = false;

        CanonicalAttributeModelDefinition definition = canonicalAttributeDefinition.getCanonicalAttributeModelDefinition();
        String canonicalItemType = definition.getCanonicalItemMetadata().getItemType();
        if (VALID_CANONICAL_TYPES.equalsIgnoreCase(canonicalItemType)) {
            itemStatus = true;
        }
        return itemStatus;
    }

    public int getOrder() {
        return order;
    }

    public void setOrderItemNumberConverter(TkOrderItemNumberConverter orderItemNumberConverter) {
        this.orderItemNumberConverter = orderItemNumberConverter;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}

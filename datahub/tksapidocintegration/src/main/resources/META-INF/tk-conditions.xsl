<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" encoding="UTF-8" />
    <xsl:template match="/">
        <xsl:for-each select="COND_A04/IDOC/E1KOMG">
            <xsl:variable name="vakey" select="VAKEY" />
            <xsl:variable name="kschl" select="KSCHL" />
            <xsl:for-each select="E1KONH">
                <xsl:variable name="knumh" select="KNUMH" />
                <xsl:variable name="datab" select="DATAB" />
                <xsl:variable name="datbi" select="DATBI" />
                <xsl:for-each select="E1KONP">
                    <xsl:variable name="konms" select="KONMS" />
                    <xsl:variable name="kpein" select="KPEIN" />
                    <xsl:variable name="kbetr" select="KBETR" />
                    <xsl:for-each select="E1KONM">
                        <xsl:text>&#xD;&#xA;</xsl:text>
                        <xsl:value-of select="$kschl" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="$datbi" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="$datab" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="$vakey" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="concat($knumh,'_',position())" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="$konms" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="$kpein" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="KSTBM" />
                        <xsl:text>;</xsl:text>
                        <xsl:value-of select="KBETR" />
                        <xsl:text>;</xsl:text>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
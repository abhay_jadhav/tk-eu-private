package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.service;

public class DumpInboundIDocDto {
    private final String idocType;
    private final String idocXml;
    private final String idocRootType;
    private       String idocEncoding;

    public DumpInboundIDocDto(String idocType, String idocXml, String idocRootType) {
        this.idocType = idocType;
        this.idocXml = idocXml;
        this.idocRootType = idocRootType;
    }

    public String getIdocEncoding() {
        return idocEncoding;
    }

    public void setIdocEncoding(String idocEncoding) {
        this.idocEncoding = idocEncoding;
    }

    public String getIdocType() {
        return idocType;
    }

    public String getIdocXml() {
        return idocXml;
    }

    public String getIdocRootType() {
        return idocRootType;
    }
}

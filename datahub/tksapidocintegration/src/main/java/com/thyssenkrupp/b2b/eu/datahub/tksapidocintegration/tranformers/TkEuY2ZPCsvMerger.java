package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.tranformers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TkEuY2ZPCsvMerger {

    private static final Logger LOGGER = LoggerFactory.getLogger(TkEuY2ZPCsvMerger.class);
    private static final String LINE_SEPARATOR_PROPERTY = "line.separator";
    private static final String FILE_NAME_SEPARATOR_PROPERTY ="-";
    private static final String FILE_NAME_EXTENSION_PROPERTY =".csv";
    private static final String DATE_FORMAT="yyyyMMddHHmmssSSS";

    private String csvSourceFolder;
    private String  csvArchiveFolder;
    private String targetFolder;
    private String fileNamePrefix;
    private int csvFileCountLimit;

    public void aggregate() {
        Path dir = Paths.get(getCsvSourceFolder());
        try (Stream<Path> stream = Files.list(dir)) {
            List<Path> paths = stream.filter(s -> s.toString().endsWith(FILE_NAME_EXTENSION_PROPERTY)).limit(getCsvFileCountLimit()).collect(Collectors.toList());
            if (paths.size() > 0) {
                File masterFile = new File(getTargetFolder()+getFileNamePrefix()+FILE_NAME_SEPARATOR_PROPERTY + new SimpleDateFormat(DATE_FORMAT).format(new java.util.Date()));
                Path resultPath = Paths.get(masterFile.getAbsolutePath());

                try (BufferedWriter writer = Files.newBufferedWriter(resultPath)) {
                    for (Path path : paths) {
                        if (Files.isWritable(path)) {
                            LOGGER.debug("Reading file" + path.toString());
                            try (BufferedReader reader = Files.newBufferedReader(path)) {
                                List<String> linesInFile = reader.lines().collect(Collectors.toList());
                                linesInFile.forEach(fileLine -> {
                                    try {
                                        writer.write(fileLine + System.getProperty(LINE_SEPARATOR_PROPERTY));
                                    } catch (IOException e) {
                                        LOGGER.error("Failed to write to file" + e);
                                    }
                                });
                            }
                            Files.move(path, Paths.get(getCsvArchiveFolder() +path.getFileName().toString()+FILE_NAME_SEPARATOR_PROPERTY+System.currentTimeMillis()));
                        } else {
                            LOGGER.info("File is locked" + path.toString());
                        }
                        
                       

                    }
                    LOGGER.info("Finished aggregating " + paths.size() + "files.");
                    Files.move(resultPath, Paths.get(resultPath.toString()+FILE_NAME_EXTENSION_PROPERTY));
                    
                }
            }

        } catch (IOException e) {
            LOGGER.error("Failed to aggregate csv files" + e);
        }
    }

    public String getCsvSourceFolder() {
        return csvSourceFolder;
    }

    public void setCsvSourceFolder(String csvSourceFolder) {
        this.csvSourceFolder = csvSourceFolder;
    }

    public String getFileNamePrefix() {
        return fileNamePrefix;
    }

    public void setFileNamePrefix(String fileNamePrefix) {
        this.fileNamePrefix = fileNamePrefix;
    }

    public String getTargetFolder() {
        return targetFolder;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

    public int getCsvFileCountLimit() {
        return csvFileCountLimit;
    }

    public void setCsvFileCountLimit(int csvFileCountLimit) {
        this.csvFileCountLimit = csvFileCountLimit;
    }

    public String getCsvArchiveFolder() {
        return csvArchiveFolder;
    }

    public void setCsvArchiveFolder(String csvArchiveFolder) {
        this.csvArchiveFolder = csvArchiveFolder;
    }

}

package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration;

import com.hybris.datahub.sapidocintegration.spring.HttpInboundService;
import com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.service.DumpInboundIDocDto;
import com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.service.DumpInboundIDocToFileService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import javax.ws.rs.core.Response;

public class TkHttpInboundService extends HttpInboundService {

    private DumpInboundIDocToFileService dumpInboundIDocToFileService;

    @Override
    public Response receiveIdoc(String iDoc) {
        if (!StringUtils.isEmpty(iDoc) && getDumpInboundIDocToFileService().isDumpEnabled()) {
            final String iDocType = getIdocType(iDoc);
            getDumpInboundIDocToFileService().isValidIDocType(iDocType).ifPresent(s -> {
                DumpInboundIDocDto dto = new DumpInboundIDocDto(iDocType, iDoc, s);
                getDumpInboundIDocToFileService().saveToFile(dto);
            });
        }
        return super.receiveIdoc(iDoc);
    }

    private DumpInboundIDocToFileService getDumpInboundIDocToFileService() {
        return dumpInboundIDocToFileService;
    }

    @Required
    public void setDumpInboundIDocToFileService(DumpInboundIDocToFileService dumpInboundIDocToFileService) {
        this.dumpInboundIDocToFileService = dumpInboundIDocToFileService;
    }
}

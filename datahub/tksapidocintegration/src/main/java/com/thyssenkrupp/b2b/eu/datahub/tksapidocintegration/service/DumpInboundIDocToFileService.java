package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class DumpInboundIDocToFileService implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(DumpInboundIDocToFileService.class);
    private final TaskExecutor                  taskExecutor;
    private final DumpInboundIDocToFileStrategy dumpInboundIDocToFileStrategy;
    private       boolean                       dumpEnabled;
    private       boolean                       useDedicatedThreadPool;
    private       Map<String, String>           allIDocTypesMap;
    private       Map<String, String>           validIDocTypesMap;

    public DumpInboundIDocToFileService(TaskExecutor taskExecutor, DumpInboundIDocToFileStrategy dumpInboundIDocToFileStrategy) {
        this.taskExecutor = taskExecutor;
        this.dumpInboundIDocToFileStrategy = dumpInboundIDocToFileStrategy;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (isDumpEnabled()) {
            this.validIDocTypesMap = new IDocTypesPropertyParser(this.allIDocTypesMap).getValidIDocTypesMap();
            generateInitialDirectories(this.validIDocTypesMap);
        }
    }

    public void saveToFile(final DumpInboundIDocDto dto) {
        if (isUseDedicatedThreadPool()) {
            CompletableFuture.runAsync(() -> dumpInboundIDocToFileStrategy.saveToFile(dto), this.taskExecutor);
        } else {
            CompletableFuture.runAsync(() -> dumpInboundIDocToFileStrategy.saveToFile(dto));
        }
    }

    public Optional<String> isValidIDocType(String iDocType) {
        if (!StringUtils.isEmpty(iDocType) && getValidIDocTypesMap().containsKey(iDocType)) {
            return Optional.of(getValidIDocTypesMap().get(iDocType));
        }
        return Optional.empty();
    }

    private void generateInitialDirectories(Map<String, String> map) throws IOException {
        if (map != null && !map.isEmpty()) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                final String directoryPath = dumpInboundIDocToFileStrategy.generateDirectoryPath(entry.getValue(), entry.getKey());
                createDirectoryIfNotExist(directoryPath);
            }
        }
    }

    private void createDirectoryIfNotExist(String directoryPath) throws IOException {
        if (!StringUtils.isEmpty(directoryPath)) {
            Path directory = Paths.get(directoryPath);
            if (!directory.toFile().exists()) {
                LOGGER.debug("Creating new directory path:{}", directoryPath);
                Files.createDirectories(directory);
            }
        }
    }

    public boolean isDumpEnabled() {
        return dumpEnabled;
    }

    public void setDumpEnabled(boolean dumpEnabled) {
        this.dumpEnabled = dumpEnabled;
    }

    private boolean isUseDedicatedThreadPool() {
        return useDedicatedThreadPool;
    }

    public void setUseDedicatedThreadPool(boolean useDedicatedThreadPool) {
        this.useDedicatedThreadPool = useDedicatedThreadPool;
    }

    public void setAllIDocTypesMap(Map<String, String> allIDocTypesMap) {
        this.allIDocTypesMap = allIDocTypesMap;
    }

    private Map<String, String> getValidIDocTypesMap() {
        return validIDocTypesMap;
    }

    private static final class IDocTypesPropertyParser {

        private Map<String, String> validIDocTypesMap = new HashMap<>();

        public IDocTypesPropertyParser(Map<String, String> map) {
            generateValidIDocMap(map);
        }

        public Map<String, String> getValidIDocTypesMap() {
            return validIDocTypesMap;
        }

        private void generateValidIDocMap(final Map<String, String> map) {
            if (map != null && !map.isEmpty()) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    final Optional<List<String>> list = parseStringToList(entry.getValue());
                    list.ifPresent(stringList -> stringList.forEach(s -> addToMap(s, entry.getKey())));
                }
                LOGGER.info("Finished generating ValidIDocMap, mapSize:{}", validIDocTypesMap.size());
            }
        }

        private Optional<List<String>> parseStringToList(final String value) {
            if (!StringUtils.isEmpty(value)) {
                return Optional.of(Arrays.asList(value.split(",")));
            }
            return Optional.empty();
        }

        private void addToMap(final String key, final String value) {
            this.validIDocTypesMap.put(key, value);
        }
    }
}

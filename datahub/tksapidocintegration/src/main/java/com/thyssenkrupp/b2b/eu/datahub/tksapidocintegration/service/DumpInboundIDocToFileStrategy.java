package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DumpInboundIDocToFileStrategy {

    private static final Logger LOGGER              = LoggerFactory.getLogger(DumpInboundIDocToFileStrategy.class);
    private static final String FILE_DATE_FORMAT    = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final String FILE_EXTENSION      = "xml";
    private static final String DIRECTORY_SEPARATOR = "/";
    private String fileWriterDirectory;

    public void saveToFile(final DumpInboundIDocDto dto) {
        try {
            final String filePath = generateFilePath(dto.getIdocRootType(), dto.getIdocType());
            LOGGER.debug("Started writing to file :{}", filePath);
            final Path path = Paths.get(filePath);
            Files.write(path, dto.getIdocXml().getBytes(StandardCharsets.UTF_8));
            LOGGER.debug("Finished writing to file :{}", filePath);
        } catch (IOException e) {
            LOGGER.error(MessageFormat.format("Error while writing IDoc of type:{0} to file", dto.getIdocType()), e);
        }
    }

    public String generateDirectoryPath(final String iDocRootType, final String iDocType) {
        String directory = getDirectoryPathWithTrailingSlash();
        return directory + iDocRootType + DIRECTORY_SEPARATOR + iDocType + DIRECTORY_SEPARATOR;
    }

    private String generateFilePath(final String iDocRootType, final String iDocType) {
        String directory = generateDirectoryPath(iDocRootType, iDocType);
        return directory + iDocType + "_" + getCurrentTime() + "." + FILE_EXTENSION;
    }

    private String getDirectoryPathWithTrailingSlash() {
        String directory = getFileWriterDirectory();
        return directory.endsWith(DIRECTORY_SEPARATOR) ? directory : directory + DIRECTORY_SEPARATOR;
    }

    private String getCurrentTime() {
        return new SimpleDateFormat(FILE_DATE_FORMAT).format(new Date());
    }

    private String getFileWriterDirectory() {
        return fileWriterDirectory;
    }

    @Required
    public void setFileWriterDirectory(String fileWriterDirectory) {
        this.fileWriterDirectory = fileWriterDirectory;
    }
}

package com.thyssenkrupp.b2b.eu.datahub.tksapidocintegration.Compartor;

import com.hybris.datahub.model.TargetItem;

import java.util.Comparator;

public class ShippingNoteComparator implements Comparator {

    @Override
    public int compare(Object obj1, Object obj2) {
        if(obj1 instanceof TargetItem && obj2 instanceof TargetItem){
            TargetItem targetItem1 = (TargetItem) obj1;
            TargetItem targetItem2 = (TargetItem) obj2;

            if(targetItem1.getFields().containsKey("ITM_NUMBER") && targetItem2.getFields().containsKey("ITM_NUMBER")) {
                Object itmNumber1 = targetItem1.getField("ITM_NUMBER");

                Object itmNumber2 = targetItem2.getField("ITM_NUMBER");

                if(String.class.isInstance(itmNumber1) && String.class.isInstance(itmNumber2)){

                    return ((String)itmNumber1).compareTo((String)itmNumber2);

                }
            }
        }
        return 0;
    }
}
package com.hybris.datahub.sapidocoutboundadapter.impl;

import com.hybris.datahub.domain.ItemMetadata;
import com.hybris.datahub.domain.TargetItemMetadata;
import com.hybris.datahub.model.BaseDataItem;
import com.hybris.datahub.model.TargetItem;
import com.hybris.datahub.runtime.domain.TargetSystemPublication;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class TkDefaultIDocOutboundAdapter extends DefaultIDocOutboundAdapter {

    protected Comparator shippingNotecomparator;

    @Required
    public void setComparator(Comparator comparator) {

        this.shippingNotecomparator = comparator;
    }

    @Override
    protected Map<IDocIdentifier, IDocWithTargetItems> addAllTargetItems(TargetSystemPublication pub) throws ReflectiveOperationException {
        List<TargetItem> collect = (List) (new TransactionTemplate(transactionManager)).execute((status) -> {
            return (List) pub.getTargetSystem().getTargetItemMetadata().stream().map(ItemMetadata::getItemType).sorted().map((type) -> {
                return publicationActionService.findTargetItemsByPublication(pub.getPublicationId(), type, ALL);
            }).flatMap(Collection::stream).peek(TargetItem::getCanonicalItem).sorted(Comparator.comparing(BaseDataItem::getType).thenComparing(shippingNotecomparator)).collect(Collectors.toList());
        });

        Map<IDocIdentifier, IDocWithTargetItems> iDocs = new TreeMap();
        Set<TargetItemMetadata> targetItemMetadata = pub.getTargetSystem().getTargetItemMetadata();
        Iterator iterator = collect.iterator();

        while (iterator.hasNext()) {
            TargetItem targetItem = (TargetItem) iterator.next();

            filterShippingNoteSequenceNo(targetItem);
            TargetItemContextInfo contextInfo = this.createContextInfoForTargetItem(targetItemMetadata, targetItem);
            iDocBuilder.addTargetItem(iDocs, contextInfo, targetItem);
        }

        return iDocs;
    }

    private void filterShippingNoteSequenceNo(TargetItem targetItem) {
        if (targetItem.getFields().containsKey("TEXT_LINE")) {
            targetItem.setField("ITM_NUMBER", "000000");
        }
    }
}

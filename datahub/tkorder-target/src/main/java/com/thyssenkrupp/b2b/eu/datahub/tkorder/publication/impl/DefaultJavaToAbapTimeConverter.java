package com.thyssenkrupp.b2b.eu.datahub.tkorder.publication.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodExecutor;
import org.springframework.expression.TypedValue;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;

public class DefaultJavaToAbapTimeConverter implements MethodExecutor {

    private static final DateTimeFormatter ABAP_FORMATTER = DateTimeFormatter.ofPattern("HHmm");
    private static final TypedValue        EMPTY_DATE     = new TypedValue("");
    private static final DateTimeFormatter HYBRIS_FORMATTER;
    private static final Logger            LOGGER;
    protected            DateTimeFormatter hybrisFormatter;

    public DefaultJavaToAbapTimeConverter() {
        this.hybrisFormatter = HYBRIS_FORMATTER;
    }

    public TypedValue execute(EvaluationContext context, Object target, Object... arguments) throws AccessException {
        assert arguments.length == 1 : arguments.length;

        Object hybrisDate = arguments[0];

        assert hybrisDate instanceof String || hybrisDate == null : hybrisDate;

        if (hybrisDate == null) {
            return EMPTY_DATE;
        } else {
            TemporalAccessor accessor = this.hybrisFormatter.parse((String) hybrisDate);
            String abapTime = ABAP_FORMATTER.format(accessor);
            LOGGER.debug("hybrisDate={} -> ABAPDate={}", hybrisDate, abapTime);
            return new TypedValue(abapTime);
        }
    }

    static {
        HYBRIS_FORMATTER = (new DateTimeFormatterBuilder()).appendPattern("yyyy-MM-dd HH:mm:ss").appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).toFormatter();
        LOGGER = LoggerFactory.getLogger(DefaultJavaToAbapTimeConverter.class);
    }
}



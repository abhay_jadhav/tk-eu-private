package com.thyssenkrupp.b2b.eu.datahub.tkproduct.datacleanup;

import com.hybris.datahub.api.event.PublicationCompletedEvent;
import com.hybris.datahub.cleanup.jdbc.AbstractJdbcDataHubEventListener;
import com.hybris.datahub.domain.CompositionStatusType;

public class TkCanonicalItemDeleteCleanupListener  extends AbstractJdbcDataHubEventListener<PublicationCompletedEvent> {
            public static final String ARCHIVED;
            public static final String SUCCESS;
     
            static {
                ARCHIVED = CompositionStatusType.ARCHIVED.getCode();
                SUCCESS = CompositionStatusType.SUCCESS.getCode();
            }
     
   
        public Class<PublicationCompletedEvent> getEventClass() {
           return PublicationCompletedEvent.class;
       }
     }



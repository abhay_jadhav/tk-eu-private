package com.thyssenkrupp.b2b.eu.datahub.tkproduct.datacleanup;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import com.hybris.datahub.cleanup.jdbc.UpdateSqlStatement;
import com.hybris.datahub.cleanup.service.strategies.SqlStrategy;

public class TkSqlCanonicalCleanupStrategy implements SqlStrategy {
    
        @Override
        public String getIdSql() {
           StringBuilder builder = new StringBuilder(" SELECT distinct(a.id) FROM CanonicalItem a JOIN CanItemPubStatus b ON a.id = b.canonicalitem where a.itemtype ='CanonicalProduct' and a.status ='SUCCESS' and b.status IN ('SUCCESS','FAILURE')  ");
           
           return builder.toString();
        }


        @Override
        public List<UpdateSqlStatement> buildUpdateSqlStatements() {
            ArrayList updateSqlStatements = Lists
                    .newArrayList(
                            new UpdateSqlStatement[]{
                                    UpdateSqlStatement.builder()
                                            .setIdSupplierSql(
                                                    "SELECT id FROM CanItemPubStatus WHERE canonicalitem IN (:ids)")
                                            .setSql("DELETE FROM PublicationError WHERE canonicalitempublicationstatus IN (:secondaryids)")
                                            .build(),
                                    UpdateSqlStatement
                                            .builder().setSql("DELETE FROM CanItemPubStatus WHERE canonicalitem IN (:ids)")
                                            .build(),
                                    UpdateSqlStatement.builder().setSql("DELETE FROM CanonicalItem WHERE id IN (:ids)")
                                            .build()});
         

            return updateSqlStatements;
        }


}

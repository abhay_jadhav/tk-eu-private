package com.thyssenkrupp.b2b.eu.datahub.tkproduct.target.publication;

import com.hybris.datahub.grouping.TargetItemCreationContext;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.saperpproduct.publication.SalesProductAttributesPublicationHandler;

public class TkSalesProductAttributesPublicationHandler extends SalesProductAttributesPublicationHandler {

	@Override
	public <T extends CanonicalItem> boolean isApplicable(T item, TargetItemCreationContext context) {
		String targetItemType = context.getTargetItemTypeCode();
		return "SalesProductAttributes".equals(targetItemType) || "SalesVariantAttributes".equals(targetItemType)
				|| "CleanSalesProductAttributes".equals(targetItemType)
				|| "CleanSalesVariantAttributes".equals(targetItemType)
				|| "BaseERPVariantProductAttributes".equals(targetItemType)
				|| "SalesERPVariantProductAttributes".equals(targetItemType);
	}

}

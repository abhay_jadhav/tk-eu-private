package com.thyssenkrupp.b2b.eu.datahub.tkproduct.datacleanup;

import com.hybris.datahub.cleanup.jdbc.AbstractJdbcCleanupService;
import com.hybris.datahub.domain.CompositionStatusType;

public class TkCanonicalItemDeleteCleanupService extends AbstractJdbcCleanupService {

    public static final String ARCHIVED;
    public static final String SUCCESS;
    private boolean deleteCanonicalEnabled;

    static {
        ARCHIVED = CompositionStatusType.ARCHIVED.getCode();
        SUCCESS = CompositionStatusType.SUCCESS.getCode();
    }

    public void afterPropertiesSet() {
        if (isDeleteCanonicalEnabled()) {
            super.afterPropertiesSet();
            this.updateStatementsByIdMap.put("MYSQL", this.buildUpdateStatementsById(new TkSqlCanonicalCleanupStrategy()));
        }
    }

    @Override
    public String getName() {
        return "CanonicalItem cleanup";
    }

    public boolean isDeleteCanonicalEnabled() {
        return deleteCanonicalEnabled;
    }

    public void setDeleteCanonicalEnabled(boolean deleteCanonicalEnabled) {
        this.deleteCanonicalEnabled = deleteCanonicalEnabled;
    }

}

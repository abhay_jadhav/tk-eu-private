# README #
Datahub that runs on a embedded tomcat.

### Pre-requisites ###
1. Maven
2. Access to https://toolset.mx.thyssenkrupp.com/
3. This project needs to be set up in a machine where hybris commerce is set up.

### Steps ###

1. `mvn clean install` to download and install dependencies.
2. `mvn tomcat7:run-war` to start the datahub
3. Issue `curl -X GET -H "Authorization: Basic YWRtaW46bmltZGE=" -H "Cache-Control: no-cache" -H "Postman-Token: 719d4714-502c-42c4-5c44-09bd25fc942e" "http://localhost:9090/datahub-webapp/v1/version" ` to see the version.
4. Please see the downloads section to find the sample data files and script to upload these to data hub.
5. For more info on how to compose and publish data, please go through the hybris help documentatiom on datahub.



### Importing SAP Sample Data ###
1. Make sure Datahub is up and running, and Hybris has the datahub and sap integration related extensions enabled.
2. From the download section, download the files q32.customizing.impex and SAP-Product-Sample.zip
3. Import the q32.customizing.impex into hybris from the HAC console.
4. Unzip SAP-Product-Sample.zip. Execute the `send.sh` file.
5. Use the Backoffice console or REST APIs to compose and publish data to Hybris.


### How to create a new extension ###
1. Go to custom folder.
2. Use the archetype to create the new extension, `mvn archetype:generate -DarchetypeGroupId=com.hybris.datahub -DarchetypeArtifactId=datahub-extension-archetype -DarchetypeVersion=6.4.0.0-RC5 -DgroupId=com.thyssenkrupp.b2b.eu.datahub -DartifactId=<your-artifact-id> -DsdkVersion=6.4.0.0-RC5 -DarchetypeCatalog=https://toolset.mx.thyssenkrupp.com/artifactory/repo/com/hybris/datahub/archetype-catalog.xml`
3. Modify the pom.xml to include the datahub-parent
4. Modify the parent pom and include the newly created extension underneath modules.
5. Add the newly created extensions as dependencies into the tomcat-server pom.xml.

### Upgrade from datahub 6.3 to 6.4
We upgraded to the hybris and datahub version 6.4 recently, and when you checkout new datahub version
after already have used old one, you have to:

* Either upgrade your local database schema by executing update scripts.
* Or drop and recreate your local datahub database.

Of course you have only the first option for other than local systems.

To update your local database schema, perform following steps(check https://help.hybris.com/6.4.0/hcd/fd0a5efc50a44ea7bee6374df02c4552.html#copyfd0a5efc50a44ea7bee6374df02c4552 for details):

* Shutdown datahub.
* Run the database upgrade script from `./updatescripts` 
corresponding to your database (e.g. `datahub-mysql.sql` for mysql or `datahub-mssql.sql` for MS SQL)
* Start datahub again
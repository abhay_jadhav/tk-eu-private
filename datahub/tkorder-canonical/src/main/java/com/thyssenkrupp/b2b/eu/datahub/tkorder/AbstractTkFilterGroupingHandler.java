package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.saporder.grouping.impl.AbstractGroupingHandler;

import java.util.List;
import java.util.stream.Stream;

public abstract class AbstractTkFilterGroupingHandler extends AbstractGroupingHandler {

    protected AbstractTkFilterGroupingHandler(String rawItem) {
        super(rawItem);
    }

    protected <T extends RawItem> Stream getItemsForKeyAndValue(Stream<T> itemStream, String fieldKey, String fieldValue) {
        return itemStream.filter(item -> item.getField(fieldKey) != null && item.getField(fieldKey).equals(fieldValue));
    }

    protected <T extends RawItem> Stream getValueStreamForField(Stream<T> valueStream, String field) {
        return valueStream.map(item -> item.getField(field));
    }

    protected  <T extends RawItem> Stream getValueStreamForField(CompositionGroup<T> compositionGroup, String field) {
        return compositionGroup.getItems().stream().map(item -> item.getField(field));
    }

    protected boolean isMatchingAny(String condition, Stream valueStream) {
        return valueStream.map(String.class::cast).anyMatch(string -> string != null && string.equals(condition));
    }

    protected boolean isMatchingAll(String condition, Stream valueStream) {
        return valueStream.map(String.class::cast).allMatch(string -> string != null && string.equals(condition));
    }
    
    protected boolean containsAny(List<String> conditionList, Stream valueStream) {
        return valueStream.map(String.class::cast).allMatch(string -> string != null && conditionList.contains(string));
    }
}

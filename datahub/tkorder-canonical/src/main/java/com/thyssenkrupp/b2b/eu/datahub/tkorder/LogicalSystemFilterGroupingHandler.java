package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class LogicalSystemFilterGroupingHandler extends AbstractTkFilterGroupingHandler {
    private static final Logger       LOGGER                     = LoggerFactory.getLogger(LogicalSystemFilterGroupingHandler.class);
    private static final String       IDOC_SEGMENT               = "EDI_DC40-IDOCTYP";
    private static final String       RAW_ITEM_ORDER             = "RawORDERS";
    private static final String       ORDER_IDOC_STATUS_SEGMENT  = "EDI_DC40-MESCOD";
    private static final String       ORDER_IDOC_STATUS_VALUE_AB = "AB";
    private static final String       RAW_ITEM_DELIVERY          = "RawDELVRY";
    private final        String       rawItemName;
    private              String       logicalSystem;
    private              String       logicalSystemSegment;
    private              String       idocType;
    private              List<String> distributionChannel;
    private              String       distributionChannelSegment;
    private              String       offlineOrderDistributionChannelValue;

    protected LogicalSystemFilterGroupingHandler(String rawItem) {
        super(rawItem);
        rawItemName = rawItem;
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }

        Stream idocTypes = getValueStreamForField(compositionGroup, IDOC_SEGMENT);
        Stream filteredSystems = getValueStreamForField(compositionGroup, logicalSystemSegment);
        Stream filteredDistributionChannels = getValueStreamForField(compositionGroup, distributionChannelSegment);

        boolean isCorrectSystem = isMatchingAll(logicalSystem, filteredSystems);
        boolean isValidIdocStatus = true;
        if (isOfflineOrderItem(compositionGroup)) {
            isCorrectSystem = true;
            isValidIdocStatus = isValidOfflineOrderIdocStatus(compositionGroup);
        }
        if (isOfflineDeliveryItem(compositionGroup)) {
            isCorrectSystem = true;
        }
        boolean matchesIdocType = isMatchingAny(idocType, idocTypes);
        boolean isCorrectDistributionChannel = containsAny(distributionChannel, filteredDistributionChannels);
        boolean isIncorrectLogicalSystem = !(matchesIdocType && isCorrectSystem && isCorrectDistributionChannel && isValidIdocStatus);

        LOGGER.info("Logical system filter execution is complete. The value of flag isIncorrectLogicalSystem is " + isIncorrectLogicalSystem);

        LOGGER.debug("Is any IDOC type matched flag : " + matchesIdocType);
        LOGGER.debug("Is Valid IDOC status flag: " + isValidIdocStatus);
        LOGGER.debug("Is Correct logical system flag: " + isCorrectSystem);
        LOGGER.debug("Is correct distribution channel flag: " + isCorrectDistributionChannel);

        return isIncorrectLogicalSystem;
    }

    private <T extends RawItem> boolean isOfflineDeliveryItem(CompositionGroup<T> compositionGroup) {
        if (RAW_ITEM_DELIVERY.equals(rawItemName) && compositionGroup != null && compositionGroup.getItems() != null) {
            return compositionGroup.getItems().stream().map(item -> (String) item.getField(distributionChannelSegment))
                .anyMatch(s -> isNotEmpty(s) && s.equals(offlineOrderDistributionChannelValue));
        }
        return false;
    }

    private <T extends RawItem> boolean isOfflineOrderItem(CompositionGroup<T> compositionGroup) {
        if (RAW_ITEM_ORDER.equals(rawItemName) && compositionGroup != null && compositionGroup.getItems() != null) {
            return compositionGroup.getItems().stream().map(item -> (String) item.getField(distributionChannelSegment))
                .anyMatch(s -> isNotEmpty(s) && s.equals(offlineOrderDistributionChannelValue));
        }
        return false;
    }

    private <T extends RawItem> boolean isValidOfflineOrderIdocStatus(CompositionGroup<T> compositionGroup) {
        final Stream idocStatus = getValueStreamForField(compositionGroup, ORDER_IDOC_STATUS_SEGMENT);
        return isMatchingAll(ORDER_IDOC_STATUS_VALUE_AB, idocStatus);
    }

    public void setIdocType(String idocType) {
        this.idocType = idocType;
    }

    public void setLogicalSystem(String logicalSystem) {
        this.logicalSystem = logicalSystem;
    }

    public void setLogicalSystemSegment(String logicalSystemSegment) {
        this.logicalSystemSegment = logicalSystemSegment;
    }

    public void setDistributionChannel(List<String> distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    public void setDistributionChannelSegment(String distributionChannelSegment) {
        this.distributionChannelSegment = distributionChannelSegment;
    }

    public void setOfflineOrderDistributionChannelValue(String offlineOrderDistributionChannelValue) {
        this.offlineOrderDistributionChannelValue = offlineOrderDistributionChannelValue;
    }
}

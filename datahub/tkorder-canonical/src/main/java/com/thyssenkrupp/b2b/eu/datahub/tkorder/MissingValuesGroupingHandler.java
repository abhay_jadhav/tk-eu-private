package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.RawItemStatusType;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.runtime.domain.DataLoadingAction;
import com.hybris.datahub.service.DataHubFeedService;
import com.hybris.datahub.service.RawItemService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class MissingValuesGroupingHandler extends AbstractTkFilterGroupingHandler {

    private static final Logger LOGGER                                = LoggerFactory.getLogger(MissingValuesGroupingHandler.class);
    private static final int    RAW_ITEM_LIMIT                        = 1000;
    private static final String RAW_ITEM_ORDER                        = "RawORDERS";
    private static final String RAW_ITEM_DELIVERY                     = "RawDELVRY";
    private static final String ORDER_DIST_CHANNEL_SELECTOR_SEG_KEY   = "E1EDK14-QUALF";
    private static final String ORDER_DIST_CHANNEL_SELECTOR_SEG_VALUE = "007";
    private static final String ORDER_DIST_CHANNEL_SEG_KEY            = "E1EDK14-ORGID";
    private String             logicalSystemSegment;
    private String             logicalSystemKeySegment;
    private String             logicalSystemKey;
    private DataHubFeedService feedService;
    private String             rawItemName;
    private String             poolName;
    private RawItemService     rawItemService;
    private List<String>       canonicalItems;
    private String             distributionChannelSegment;
    private String             cancellationkey;

    protected MissingValuesGroupingHandler(String rawItem) {
        super(rawItem);
        this.rawItemName = rawItem;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        DataLoadingAction dataLoadingAction = compositionGroup.getItems().get(0).getDataLoadingAction();
        List<? extends RawItem> rawItems = getRawItems(dataLoadingAction);
        Stream logicalSystems = getItemsForKeyAndValue(rawItems.stream(), logicalSystemKeySegment, logicalSystemKey);
        Stream filteredSystems = getValueStreamForField(logicalSystems, logicalSystemSegment);
        final String logicalSystem = getFirstOrEmptyString(filteredSystems);
        if (isNotEmpty(logicalSystem)) {
            addLogicalSystemToItems(compositionGroup.getItems(), logicalSystem);
        }

        final String distributionChannel = getDistributionChannelFromRawItems(rawItems);
        updateDistributionChannelToItems(compositionGroup.getItems(), distributionChannel);

        if(RAW_ITEM_ORDER.equals(rawItemName)){
            Stream cancellationStream = getValueStreamForField(rawItems.stream(), cancellationkey);
            final Optional<String> cancellationValue = getFirstNonEmptyString(cancellationStream);
            if (cancellationValue.isPresent()) {
                String itemType = ((CanonicalAttributeDefinition) compositionGroup.getAttributes().get(0)).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
                addCancellationValueToitems(compositionGroup.getItems(), cancellationValue.get());
            }
        }

        return Collections.singletonList(compositionGroup);
    }

    private Optional<String> getFirstNonEmptyString(Stream cancellationStream) {
        Optional<String> firstNonNull = cancellationStream.filter(p -> p != null).findFirst();
        return firstNonNull;
    }

    private <T extends RawItem> void addCancellationValueToitems(List<? extends RawItem> items, String cancellationValue) {
        if (isNotEmpty(cancellationValue)) {
            items.forEach(item -> item.setField(cancellationkey, cancellationValue));
        }
    }

    @SuppressWarnings("unchecked")
    private String getDistributionChannelFromRawItems(List<? extends RawItem> rawItems) {
        Stream<String> distributionChannels = Stream.of("");
        if (RAW_ITEM_ORDER.equals(rawItemName)) {
            Stream<RawItem> itemsWithDistributionChannelKey = getItemsForKeyAndValue(rawItems.stream(), ORDER_DIST_CHANNEL_SELECTOR_SEG_KEY, ORDER_DIST_CHANNEL_SELECTOR_SEG_VALUE);
            distributionChannels = itemsWithDistributionChannelKey.map(o -> (String) o.getField(ORDER_DIST_CHANNEL_SEG_KEY)).filter(StringUtils::isNotEmpty);
        } else if (RAW_ITEM_DELIVERY.equals(rawItemName)) {
            distributionChannels = getValueStreamForField(rawItems.stream(), distributionChannelSegment);
        }
        return getFirstOrEmptyString(distributionChannels);
    }

    private void updateDistributionChannelToItems(List<? extends RawItem> items, final String distributionChannel) {
        if (isNotEmpty(distributionChannel)) {
            items.forEach(item -> item.setField(distributionChannelSegment, distributionChannel));
        }
    }

    private String getFirstOrEmptyString(Stream<String> stream) {
        return stream.map(Optional::ofNullable).findFirst().flatMap(identity()).orElse(EMPTY);
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        String itemType = ((CanonicalAttributeDefinition) compositionGroup.getAttributes().get(0)).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
        return isMatchingCanonical(itemType);
    }

    private boolean isMatchingCanonical(String itemType) {
        if (canonicalItems != null && !canonicalItems.isEmpty() && !canonicalItems.contains(itemType)) {
            return false;
        }
        return true;
    }

    private List<? extends RawItem> getRawItems(DataLoadingAction dataLoadingAction) {
        DataHubPool configPool = this.feedService.findPoolByName(poolName);
        return rawItemService.findInPool(configPool, rawItemName, RAW_ITEM_LIMIT, Collections.singletonList(dataLoadingAction), new HashMap<>(), RawItemStatusType.PROCESSED, RawItemStatusType.PENDING);
    }

    private void addLogicalSystemToItems(List<? extends RawItem> items, String logicalSystem) {
        items.forEach(item -> {
            item.setField(logicalSystemSegment, logicalSystem);
        });
    }

    public void setFeedService(DataHubFeedService feedService) {
        this.feedService = feedService;
    }

    public void setRawItemService(RawItemService rawItemService) {
        this.rawItemService = rawItemService;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public void setCanonicalItems(List<String> canonicalItems) {
        this.canonicalItems = canonicalItems;
    }

    public void setLogicalSystemKey(String logicalSystemKey) {
        this.logicalSystemKey = logicalSystemKey;
    }

    public void setLogicalSystemSegment(String logicalSystemSegment) {
        this.logicalSystemSegment = logicalSystemSegment;
    }

    public void setLogicalSystemKeySegment(String logicalSystemKeySegment) {
        this.logicalSystemKeySegment = logicalSystemKeySegment;
    }

    public void setDistributionChannelSegment(String distributionChannelSegment) {
        this.distributionChannelSegment = distributionChannelSegment;
    }

    public void setCancellationkey(String cancellationkey) {
        this.cancellationkey = cancellationkey;
    }
}

package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.hybris.datahub.domain.RawItemStatusType;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.runtime.domain.DataLoadingAction;
import com.hybris.datahub.service.RawItemService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class TkOrderEntryConfigPopulatorGroupingHandler extends AbstractTkFilterGroupingHandler {

    private static final Logger         LOGGER                     = LoggerFactory.getLogger(TkOrderEntryConfigPopulatorGroupingHandler.class);
    private static final int            RAW_ITEM_LIMIT             = 1000;
    private static final String         ORDER_ITEM_CANONICAL       = "CanonicalOrderItemCreationNotification";
    private static final String         ORDER_ENTRY_ORDER_ID_KEY   = "E1EDK01-BELNR";
    private static final String         ORDER_ENTRY_POSITION_KEY   = "E1EDP01-POSEX";
    private static final String         CONFIG_ORDER_ID_KEY        = "E1EDK01-BELNR";
    private static final String         CONFIG_POSITION_KEY        = "E1CUCFG-POSEX";
    private static final String         CONFIG_CHAR_KEY            = "E1CUCFG-E1CUVAL-CHARC";
    private static final String         CONFIG_CHAR_VALUE_KEY      = "E1CUCFG-E1CUVAL-VALUE";
    private static final String         RAW_ITEM_CONFIGS_ATTRIBUTE = "configurations";
    private final        String         rawItemName;
    private              RawItemService rawItemService;
    private              List<String>   ignoredConfigurationKeys;

    protected TkOrderEntryConfigPopulatorGroupingHandler(String rawItem) {
        super(rawItem);
        this.rawItemName = rawItem;
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        checkNotNull(compositionGroup.getItems(), "CompositionGroup RawItems cannot be null");
        checkArgument(!compositionGroup.getItems().isEmpty(), "CompositionGroup must contain RawItems");
        final T firstItem = compositionGroup.getItems().get(0);
        final DataLoadingAction dataLoadingAction = firstItem.getDataLoadingAction();

        final String sapOrderId = (String) firstItem.getField(ORDER_ENTRY_ORDER_ID_KEY);
        final String entryPosition = (String) firstItem.getField(ORDER_ENTRY_POSITION_KEY);
        if (isEmpty(sapOrderId) || isEmpty(entryPosition)) {
            return singletonList(compositionGroup);
        }
        List<RawItem> rawItems = getRawItems(compositionGroup, dataLoadingAction);
        final Optional<String> maybeJson = generateJsonString(rawItems, sapOrderId, entryPosition);
        if (maybeJson.isPresent()) {
            final String json = maybeJson.get();
            LOGGER.debug("Generated json for order:{} and position:{} json:{}", sapOrderId, entryPosition, json);
            final List<T> updatedRawItems = getUpdatedRawItems(compositionGroup.getItems(), json);
            return singletonList(new CompositionGroup<>(updatedRawItems, compositionGroup.getAttributes(), compositionGroup.getPool()));
        }
        return singletonList(compositionGroup);
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }
        return ORDER_ITEM_CANONICAL.equals(compositionGroup.getCanonicalItemMetadata().getItemType());
    }

    private Optional<String> generateJsonString(final List<RawItem> rawItems, final String sapOrderId, final String position) {
        if (rawItems != null && !rawItems.isEmpty()) {
            try {
                final List<Configuration> configurations = rawItems.stream().filter(isValidOrderIdAndPosition(sapOrderId, position))
                    .filter(isValidConfiguration()).filter(isIgnoredConfiguration().negate())
                    .map(item -> new Configuration((String) item.getField(CONFIG_CHAR_KEY), (String) item.getField(CONFIG_CHAR_VALUE_KEY))).collect(Collectors.toList());
                if (configurations != null && !configurations.isEmpty()) {
                    String json = new ObjectMapper().writeValueAsString(configurations);
                    return Optional.ofNullable(json);
                }
            } catch (Exception e) {
                LOGGER.error("Error creating json string from Configuration RawItems", e);
            }
        }
        return Optional.empty();
    }

    private <T extends RawItem> List<T> getUpdatedRawItems(final ImmutableList<T> items, final String json) {
        return items.stream().map(item -> {
            final T clonedItem = getRawItemService().clone(item);
            clonedItem.setField(RAW_ITEM_CONFIGS_ATTRIBUTE, json);
            return clonedItem;
        }).collect(Collectors.toList());
    }

    private Predicate<RawItem> isValidOrderIdAndPosition(final String orderId, final String position) {
        return item -> {
            final String itemOrderId = (String) item.getField(CONFIG_ORDER_ID_KEY);
            final String itemPosition = (String) item.getField(CONFIG_POSITION_KEY);
            return StringUtils.equals(orderId, itemOrderId) && StringUtils.equals(position, itemPosition);
        };
    }

    private Predicate<RawItem> isValidConfiguration() {
        return item -> {
            final String configKey = (String) item.getField(CONFIG_CHAR_KEY);
            final String configValue = (String) item.getField(CONFIG_CHAR_VALUE_KEY);
            return isNotEmpty(configKey) && isNotEmpty(configValue);
        };
    }

    private Predicate<RawItem> isIgnoredConfiguration() {
        return item -> {
            final String configKey = (String) item.getField(CONFIG_CHAR_KEY);
            return getIgnoredConfigurationKeys().stream().anyMatch(configKey::equalsIgnoreCase);
        };
    }

    private List<RawItem> getRawItems(final CompositionGroup compositionGroup, final DataLoadingAction dataLoadingAction) {
        DataHubPool configPool = compositionGroup.getPool();
        return getRawItemService().findInPool(configPool, rawItemName, RAW_ITEM_LIMIT, singletonList(dataLoadingAction), new HashMap<>(), RawItemStatusType.PROCESSED, RawItemStatusType.PENDING);
    }

    private RawItemService getRawItemService() {
        return rawItemService;
    }

    public void setRawItemService(RawItemService rawItemService) {
        this.rawItemService = rawItemService;
    }

    private List<String> getIgnoredConfigurationKeys() {
        return ignoredConfigurationKeys;
    }

    public void setIgnoredConfigurationKeys(List<String> ignoredConfigurationKeys) {
        this.ignoredConfigurationKeys = ignoredConfigurationKeys;
    }

    private static class Configuration {
        private final String key;
        private final String value;

        public Configuration(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}

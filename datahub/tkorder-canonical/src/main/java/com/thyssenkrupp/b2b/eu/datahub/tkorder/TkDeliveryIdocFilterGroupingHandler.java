package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

public class TkDeliveryIdocFilterGroupingHandler extends AbstractTkFilterGroupingHandler {

    private static final String DELIVERYENTRY_CANONICAL  = "CanonicalDeliveryEntryNotification";
    private static final String BATCHNUM_SEGMENT = "E1EDL20-E1EDL24-CHARG";


    protected TkDeliveryIdocFilterGroupingHandler(String rawItem) {
        super(rawItem);
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }

        boolean emptyBatchNum =Boolean.FALSE;
        if (DELIVERYENTRY_CANONICAL.equals(compositionGroup.getCanonicalItemMetadata().getItemType())) {
            emptyBatchNum = Boolean.TRUE;
            Stream filteredDeliveryEntry = getValueStreamForField(compositionGroup, BATCHNUM_SEGMENT);
          
                String batchNum = (String) filteredDeliveryEntry.filter(Objects::nonNull).findFirst().orElse("");
                if(StringUtils.isNotEmpty(batchNum)){
                    emptyBatchNum = Boolean.FALSE;
                }
        }
        
        return emptyBatchNum;
    }


}

package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.saporder.util.impl.DefaultOrderItemNumberConverter;

/**
 * Convert hybris item number (0,1,2,3) into the SAP one (000010, 000020, 000030, 000040)
 */
public class TkOrderItemNumberConverter extends DefaultOrderItemNumberConverter {


    @Override
    public String convertFromHybrisToErp(Object hybrisOrderItemNumber, Object orderId) {
    	return String.format("%06d", (Long.parseLong((String) hybrisOrderItemNumber) + 1));
    }

    @Override
    public String convertFromErpToHybris(Object erpOrderItemNumber, Object orderId) {
        return super.convertFromErpToHybris(erpOrderItemNumber,orderId);
        }
}

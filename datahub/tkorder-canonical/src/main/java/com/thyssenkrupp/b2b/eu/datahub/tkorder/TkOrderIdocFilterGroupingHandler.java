package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;


public class TkOrderIdocFilterGroupingHandler extends AbstractTkFilterGroupingHandler {
 
    private static final String ADDRESS_TYPE = "E1EDKA1-PARVW";
    private static final String ADDRESS_CANONICAL = "CanonicalErpOrderAddressNotification";
    private static final String CERTIFICATE_CANONICAL  = "CanonicalOrderConfiguredProductInfo";
    private static final String CERTIFICATE_CONF = "E1CUCFG-E1CUVAL-CHARC";

    private List<String> validAddressTypes;
    private String certificateValue;

    protected TkOrderIdocFilterGroupingHandler(String rawItem) {
        super(rawItem);
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }

        boolean matchesAddressType = Boolean.TRUE;
        if (ADDRESS_CANONICAL.equals(compositionGroup.getCanonicalItemMetadata().getItemType())) {
            matchesAddressType = Boolean.FALSE;
            Stream filteredAddressType = getValueStreamForField(compositionGroup, ADDRESS_TYPE);
            matchesAddressType = containsAny(validAddressTypes, filteredAddressType);
        }
        
        boolean matchesCertificateConfig = Boolean.TRUE;
        if (CERTIFICATE_CANONICAL.equals(compositionGroup.getCanonicalItemMetadata().getItemType())) {
            matchesCertificateConfig = Boolean.FALSE;
            Stream filteredCertType = getValueStreamForField(compositionGroup, CERTIFICATE_CONF);
            matchesCertificateConfig = isMatchingAny(certificateValue, filteredCertType);
        }
        
        return !(matchesAddressType && matchesCertificateConfig );
    }

    public void setValidAddressTypes(List<String> validAddressTypes) {
        this.validAddressTypes = validAddressTypes;
    }

    public void setCertificateValue(String certificateValue) {
        this.certificateValue = certificateValue;
    }
}

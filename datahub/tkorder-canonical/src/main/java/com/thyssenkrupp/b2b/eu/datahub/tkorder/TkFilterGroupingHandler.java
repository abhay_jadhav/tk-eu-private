package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.saporder.grouping.impl.AbstractGroupingHandler;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TkFilterGroupingHandler extends AbstractGroupingHandler {

    private String       fieldToCheck;
    private String       valueToCheck;
    private List<String> canonicalItemsToRemoveIfConditionMet;
    private List<String> canonicalItemsToRemoveIfConditionNotMet;

    protected TkFilterGroupingHandler(String rawItem) {
        super(rawItem);
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }
        String itemType = ((CanonicalAttributeDefinition)compositionGroup.getAttributes().get(0)).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
        Stream fieldValue = compositionGroup.getItems().stream().map(item -> item.getField(fieldToCheck));

        boolean matchesCondition = fieldValue.map(String.class::cast).anyMatch(string -> string != null && string.equals(valueToCheck));
        if (matchesCondition) {
            return isMatchingCanonical(canonicalItemsToRemoveIfConditionMet, itemType);
        } else {
            return isMatchingCanonical(canonicalItemsToRemoveIfConditionNotMet, itemType);
        }
    }

    private boolean isMatchingCanonical(List<String> canonicalItems, String itemType) {
        if (canonicalItems != null && !canonicalItems.isEmpty() && !canonicalItems.contains(itemType)) {
            return false; // if this is not set all items should be filtered
        }
        return true;
    }

    public void setFieldToCheck(String fieldToCheck) {
        this.fieldToCheck = fieldToCheck;
    }

    public void setValueToCheck(String valueToCheck) {
        this.valueToCheck = valueToCheck;
    }

    public void setCanonicalItemsToRemoveIfConditionMet(List<String> canonicalItemsToRemoveIfConditionMet) {
        this.canonicalItemsToRemoveIfConditionMet = canonicalItemsToRemoveIfConditionMet;
    }

    public void setCanonicalItemsToRemoveIfConditionNotMet(List<String> canonicalItemsToRemoveIfConditionNotMet) {
        this.canonicalItemsToRemoveIfConditionNotMet = canonicalItemsToRemoveIfConditionNotMet;
    }
}

package com.thyssenkrupp.b2b.eu.datahub.tkorder;

import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.saporder.grouping.impl.FilterGroupingHandlerOrderInbound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TKFilterGroupingHandlerOrderInbound extends FilterGroupingHandlerOrderInbound {

    private static final Logger LOGGER = LoggerFactory.getLogger(TKFilterGroupingHandlerOrderInbound.class);
    private String rawItemName;

    public TKFilterGroupingHandlerOrderInbound(String rawItem) {

        super();
        this.rawItemName = rawItem;
    }

    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> cg) {
        return Collections.emptyList();
    }

    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> cg) {
        if (!isApplicableFromSuper(cg)) {
            return false;
        } else {
            String itemType = ((CanonicalAttributeDefinition) cg.getAttributes().get(0)).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
            Stream var10000;
            if (!"CanonicalOrderItemCreationNotification".equals(itemType)
                && !"CanonicalOrderCreationNotification".equals(itemType)
                && !"CanonicalERPOrderAddress".equals(itemType)
                && !"CanonicalOrderCertificateConfiguredProductInfo".equals(itemType)
                && !"CanonicalNonABOrderCreationNotification".equals(itemType)
                && !"CanonicalErpOrderAddressNotification".equals(itemType)
                && !"CanonicalOrderConfiguredProductInfo".equals(itemType)
                && !"CanonicalOrderCancelNotification".equals(itemType)) {

                if ("CanonicalOrderItemCancelNotification".equals(itemType)) {
                    var10000 = cg.getItems().stream().map((r) -> {
                        return r.getField("E1EDP01-ABGRU");
                    });
                    String.class.getClass();
                    return var10000.map(String.class::cast).allMatch((s) -> {
                        return s == null || ((String) s).isEmpty();
                    });
                } else {
                    return false;
                }
            } else {
                var10000 = cg.getItems().stream().map((r) -> {
                    return r.getField("E1EDP01-ABGRU");
                });
                String.class.getClass();
                return var10000.map(String.class::cast).anyMatch((s) -> {
                    return s != null && !((String) s).isEmpty();
                });
            }
        }
    }

    public <T extends RawItem> boolean isApplicableFromSuper(CompositionGroup<T> cg) {
        return this.rawItemName.equals(((RawItem) cg.getItems().get(0)).getType());
    }
}

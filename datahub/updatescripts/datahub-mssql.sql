--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--
-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE "DataHubVersion";

INSERT INTO "DataHubVersion" values ('6.5.0');

-- ==========================================================================================
-- Add new columns
-- ==========================================================================================

ALTER TABLE "DataHubPool" ADD "deletable" INT;

-- ==========================================================================================
-- Update column values
-- ==========================================================================================

UPDATE "DataHubPool" SET "deletable" = 1;

-- ==========================================================================================
-- Add index to the existing tables
-- ==========================================================================================

CREATE INDEX "IDX_RawItem_DataPoolCreationtime" ON "RawItem" ("datapool", "creationtime");

CREATE INDEX "IDX_CanonIte_DataPoolCreationtime" ON "CanonicalItem" ("datapool", "creationtime");

CREATE INDEX "IDX_TarItem_DataPoolCreationtime" ON "TargetItem" ("datapool", "creationtime");

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE "DataHubVersion";

INSERT INTO "DataHubVersion" values ('6.6.0');


-- ==========================================================================================
-- UPGRADE to 6.7
-- ==========================================================================================
-- ==========================================================================================
-- TYF-7643: Add missing PRIMARY KEY constraints / remove redundant indexes
-- ==========================================================================================

DROP INDEX "IDX_TargetSysExportCode_destid" ON "TargetSysExportCode";
DROP INDEX "IDX_TargetItemMetadata_dependsOn_srcid" ON "TargetItemMetadata_dependsOn";
DROP INDEX "IDX_CanonicalItem_RawItem_srcid" ON "CanonicalItem_RawItem";
DROP INDEX "IDX_DataHubFeed_Pool_srcid" ON "DataHubFeed_Pool";
DROP INDEX "PK_ManagedTarItem_expCodeAttrMap_" ON "ManagedTarItem_expCodeAttrMap";

ALTER TABLE "TargetSysExportCode"
  ALTER COLUMN "destid" VARCHAR(255) NOT NULL;
ALTER TABLE "TargetSysExportCode"
  ADD CONSTRAINT "TarSysExpCode_dest_def" DEFAULT '' FOR "destid";
ALTER TABLE "TargetSysExportCode"
  ADD CONSTRAINT "PK_TargetSysExportCode" PRIMARY KEY ("srcid", "destid");
ALTER TABLE "TargetItemMetadata_dependsOn"
  ADD CONSTRAINT "PK_TargetItemMetadata_dependsOn" PRIMARY KEY ("srcid", "destid");
ALTER TABLE "CanonicalItem_RawItem"
  ADD CONSTRAINT "PK_CanonicalItem_RawItem" PRIMARY KEY ("srcid", "destid");
ALTER TABLE "DataHubFeed_Pool"
  ADD CONSTRAINT "PK_DataHubFeed_Pool" PRIMARY KEY ("srcid", "destid");
ALTER TABLE "ManagedTarItem_expCodeAttrMap"
  ADD CONSTRAINT "PK_ManagedTarItem_expCodeAttrMap" PRIMARY KEY ("srcid", "key");
ALTER TABLE "DataHubVersion"
  ADD CONSTRAINT "PK_DataHubVersion" PRIMARY KEY ("version");


-- ==========================================================================================
-- TYF-8321: Add missing indexes for findStuckActionForPool methods
-- ==========================================================================================
CREATE INDEX "IDX_PublicationAction_status" ON "PublicationAction" ("status");
CREATE INDEX "IDX_PublAction_modifiedTime" ON "PublicationAction" ("modifiedtime");
CREATE INDEX "IDX_CompAction_modifiedTime" ON "CompositionAction" ("modifiedtime");

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================
TRUNCATE TABLE "DataHubVersion";
INSERT INTO "DataHubVersion" values ('0.0.0');

TRUNCATE TABLE "DataHubVersion";
INSERT INTO "DataHubVersion" values ('18.8');
--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--
-- ==========================================================================================
-- Update DataHubVersion table to 6.5
-- ==========================================================================================
TRUNCATE TABLE `DataHubVersion`;

INSERT INTO `DataHubVersion` values ('6.5.0');


-- ==========================================================================================
-- Update DataHubVersion table to 6.6
-- ==========================================================================================

-- ==========================================================================================
-- Add new columns
-- ==========================================================================================

ALTER TABLE `DataHubPool` ADD (`deletable` INTEGER);

-- ==========================================================================================
-- Update column values
-- ==========================================================================================

UPDATE `DataHubPool` SET `deletable` = 1;

-- ==========================================================================================
-- Add index to the existing tables
-- ==========================================================================================

ALTER TABLE `RawItem` ADD INDEX `IDX_RawItem_DatapoolCreationtime` (`datapool`, `creationtime`);

ALTER TABLE `CanonicalItem` ADD INDEX `IDX_CanonIte_DatapoolCreationtime` (`datapool`, `creationtime`);

ALTER TABLE `TargetItem` ADD INDEX `IDX_TarItem_DatapoolCreationtime` (`datapool`, `creationtime`);

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE `DataHubVersion`;

INSERT INTO `DataHubVersion` values ('6.6.0');

-- ==========================================================================================
-- UPGRADE to 6.7
-- ==========================================================================================

-- ==========================================================================================
-- TYF-7643: Add missing PRIMARY KEY constraints / remove redundant indexes
-- ==========================================================================================

ALTER TABLE `TargetSysExportCode`
  MODIFY `destid` VARCHAR(255) DEFAULT '' NOT NULL,
  ADD CONSTRAINT PRIMARY KEY (`srcid`, `destid`);
ALTER TABLE `TargetItemMetadata_dependsOn`
  ADD CONSTRAINT PRIMARY KEY (`srcid`, `destid`);
ALTER TABLE `CanonicalItem_RawItem`
  ADD CONSTRAINT PRIMARY KEY (`srcid`, `destid`);
ALTER TABLE `DataHubFeed_Pool`
  ADD CONSTRAINT PRIMARY KEY (`srcid`, `destid`);
ALTER TABLE `ManagedTarItem_expCodeAttrMap`
  ADD CONSTRAINT PRIMARY KEY (`srcid`, `key`);
ALTER TABLE `DataHubVersion`
  ADD CONSTRAINT PRIMARY KEY (`version`);

DROP INDEX `PK_TargetItemMetadata_dependsOn_` ON `TargetItemMetadata_dependsOn`;
DROP INDEX `PK_CanonicalItem_RawItem_` ON `CanonicalItem_RawItem`;
DROP INDEX `PK_DataHubFeed_Pool_` ON `DataHubFeed_Pool`;
DROP INDEX `PK_ManagedTarItem_expCodeAttrMap_` ON `ManagedTarItem_expCodeAttrMap`;


-- ==========================================================================================
-- TYF-8321: Add missing indexes for findStuckActionForPool methods
-- ==========================================================================================
CREATE INDEX `IDX_PublicationAction_status` ON `PublicationAction` (`status`);
CREATE INDEX `IDX_PublAction_modifiedTime` ON `PublicationAction` (`modifiedtime`);
CREATE INDEX `IDX_CompAction_modifiedTime` ON `CompositionAction` (`modifiedtime`);

-- ==========================================================================================
-- Update DataHubVersion table
-- ==========================================================================================

TRUNCATE TABLE `DataHubVersion`;
INSERT INTO `DataHubVersion` values ('0.0.0');

TRUNCATE TABLE `DataHubVersion`;
INSERT INTO `DataHubVersion` values ('18.8');



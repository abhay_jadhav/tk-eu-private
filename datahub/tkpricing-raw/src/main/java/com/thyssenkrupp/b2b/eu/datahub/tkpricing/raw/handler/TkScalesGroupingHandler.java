package com.thyssenkrupp.b2b.eu.datahub.tkpricing.raw.handler;
import com.hybris.datahub.grouping.TargetItemCreationContext;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.sappricing.grouping.impl.ScalesGroupingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.UnmodifiableIterator;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
public class TkScalesGroupingHandler extends ScalesGroupingHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScalesGroupingHandler.class);
	private static final String PAC_DISCOUNT_TYPE = "Y2MA";
	private static final String DEFAULT_KSTBM_VALUE = "0.0";
	@Override
	public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> cg) {
		assert this.getOrder() < 0;
		int i = 0;
		String oldRowId = null;
		List<RawItem> newList = new ArrayList();
		UnmodifiableIterator var5 = cg.getItems().iterator();
		List<String> groupingDefaultRowCheck = new ArrayList();
		while (var5.hasNext()) {
			RawItem item = (RawItem) var5.next();
			String conditionId = (String) item.getField("E1KOMG-E1KONH-KNUMH");
			String discountCondition = (String) item.getField("E1KOMG-E1KONH-E1KONP-KSCHL");
			String rowId = conditionId + '_' + item.getField("E1KOMG-E1KONH-E1KONP-KSCHL")
					+ String.format("%09d", item.getDataLoadingAction().getActionId());
			String scaleAmount = (String) item.getField("E1KOMG-E1KONH-E1KONP-E1KONM-KBETR");
			String baseConditionId = (StringUtils.isNotEmpty(conditionId) && conditionId.contains("_")?StringUtils.substringBefore(conditionId, "_"):conditionId);
			LOGGER.debug("conditionId  -> " + conditionId + " " + rowId + " " + scaleAmount +" BaseconstionId "+baseConditionId);

			i = rowId.equals(oldRowId) ? i + 1 : 0;
			oldRowId = rowId;
			if (i == 0) {
				if (PAC_DISCOUNT_TYPE.equals(discountCondition) && !(groupingDefaultRowCheck.contains(baseConditionId))) {
					RawItem newItem = this.rawItemService.clone(item);
					String defaultScaleAmount = (String) item.getField("E1KOMG-E1KONH-E1KONP-KBETR");
					item.setField("E1KOMG-E1KONH-E1KONP-E1KONM-KSTBM", DEFAULT_KSTBM_VALUE);
					item.setField("E1KOMG-E1KONH-E1KONP-KBETR", defaultScaleAmount);
					newList.add(item);
					LOGGER.debug("Captured Default Condition Key for Base Scale  -> " + conditionId + " "
							+ defaultScaleAmount);
					if (scaleAmount != null) {
						newItem.setField("E1KOMG-E1KONH-KNUMH", conditionId + "_1");
						newItem.setField("E1KOMG-E1KONH-E1KONP-KBETR", scaleAmount);
						newList.add(newItem);
						rowId = conditionId + "_1" + '_' + discountCondition
								+ String.format("%09d", newItem.getDataLoadingAction().getActionId());
						oldRowId = rowId;
					}
					groupingDefaultRowCheck.add(baseConditionId);
				} else {
					newList.add(item);
				}
			} else {
				RawItem newItem = this.rawItemService.clone(item);
				newItem.setField("E1KOMG-E1KONH-KNUMH", conditionId + "_" + (i + 1));
				newItem.setField("E1KOMG-E1KONH-E1KONP-KBETR", scaleAmount);
				newList.add(newItem);
			}
		}
		return Collections.singletonList(new CompositionGroup(newList, cg.getAttributes(), cg.getPool()));
	}
}
package com.thyssenkrupp.b2b.eu.datahub;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TkInvoiceIdocFilterGroupingHandler extends AbstractInvoiceFilterGroupingHandler {

    private static final String ADDRESS_TYPE      = "E1EDKA1-PARVW";
    private static final String ADDRESS_CANONICAL = "CanonicalInvoiceAddress";

    private List<String> validAddressTypes;

    protected TkInvoiceIdocFilterGroupingHandler(String rawItem, int order) {
        super(rawItem, order);
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }

        boolean matchesAddressType = Boolean.TRUE;
        if (ADDRESS_CANONICAL.equals(compositionGroup.getCanonicalItemMetadata().getItemType())) {
            matchesAddressType = Boolean.FALSE;
            Stream filteredAddressType = getValueStreamForField(compositionGroup, ADDRESS_TYPE);
            matchesAddressType = containsAny(validAddressTypes, filteredAddressType);
        }

        return !(matchesAddressType);
    }

    public void setValidAddressTypes(List<String> validAddressTypes) {
        this.validAddressTypes = validAddressTypes;
    }
}


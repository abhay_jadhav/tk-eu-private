package com.thyssenkrupp.b2b.eu.datahub;

import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvoiceLogicalSystemFilterGroupingHandler extends AbstractInvoiceFilterGroupingHandler {
	private static final Logger         LOGGER                     = LoggerFactory.getLogger(InvoiceLogicalSystemFilterGroupingHandler.class);
	private static final String IDOC_SEGMENT = "EDI_DC40-IDOCTYP";
    private static final String  RAW_ITEM_INVOICE   = "RawInvoice";
    private final        String       rawItemName;
    private String       idocType;
    private String       logicalSystem;
    private String       logicalSystemSegment;
    private String       distributionChannelMissingSegment;
    private List<String> distributionChannel;
    private String  offlineInvoiceDistributionChannelValue;


	protected InvoiceLogicalSystemFilterGroupingHandler(String rawItem, int order) {
        super(rawItem, order);
        rawItemName = rawItem;
    }

    @Override
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        return Collections.emptyList();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        if (!super.isApplicable(compositionGroup)) {
            return false;
        }
        Stream idocTypes = getValueStreamForField(compositionGroup, IDOC_SEGMENT);
        Stream filteredSystems = getValueStreamForField(compositionGroup, logicalSystemSegment);

        boolean isCorrectSystem = isMatchingAll(logicalSystem, filteredSystems);
        boolean isValidIdocStatus = true;
        boolean matchesIdocType = isMatchingAny(idocType, idocTypes);
        LOGGER.info("idocTypes : " +idocTypes+ "filteredSystems : " +filteredSystems+ "isCorrectSystem : " +isCorrectSystem+ "isValidIdocStatus : "+isValidIdocStatus+"matchesIdocType : "+matchesIdocType);
        Stream filteredDistributionChannels = getValueStreamForField(compositionGroup, distributionChannelMissingSegment);
        boolean isCorrectDistributionChannel = containsAny(distributionChannel, filteredDistributionChannels);
        LOGGER.info("filteredDistributionChannels : " +filteredDistributionChannels+ "isCorrectDistributionChannel : " +isCorrectDistributionChannel);

        if (isOfflineInvoiceItem(compositionGroup)) {
            isCorrectSystem = true;
        }
        return !(matchesIdocType && isCorrectSystem && isCorrectDistributionChannel && isValidIdocStatus);
    }

    private <T extends RawItem> boolean isOfflineInvoiceItem(CompositionGroup<T> compositionGroup) {
    	 if (RAW_ITEM_INVOICE.equals(rawItemName) && compositionGroup != null && compositionGroup.getItems() != null) {
             return compositionGroup.getItems().stream().map(item -> (String) item.getField(distributionChannelMissingSegment))
                 .anyMatch(s -> isNotEmpty(s) && s.equals(offlineInvoiceDistributionChannelValue));
         }
         return false;
	}

	public void setDistributionChannelMissingSegment(String distributionChannelMissingSegment) {
        this.distributionChannelMissingSegment = distributionChannelMissingSegment;
    }

    public void setIdocType(String idocType) {
        this.idocType = idocType;
    }

    public void setLogicalSystem(String logicalSystem) {
        this.logicalSystem = logicalSystem;
    }

    public void setLogicalSystemSegment(String logicalSystemSegment) {
        this.logicalSystemSegment = logicalSystemSegment;
    }

    public void setDistributionChannel(List<String> distributionChannel) {
        this.distributionChannel = distributionChannel;
    }
    public String getOfflineInvoiceDistributionChannelValue() {
		return offlineInvoiceDistributionChannelValue;
	}

	public void setOfflineInvoiceDistributionChannelValue(String offlineInvoiceDistributionChannelValue) {
		this.offlineInvoiceDistributionChannelValue = offlineInvoiceDistributionChannelValue;
	}

}

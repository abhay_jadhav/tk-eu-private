package com.thyssenkrupp.b2b.eu.datahub;

import com.google.common.collect.ImmutableList;
import com.hybris.datahub.domain.RawItemStatusType;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.runtime.domain.DataLoadingAction;
import com.hybris.datahub.service.DataHubFeedService;
import com.hybris.datahub.service.RawItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class InvoiceMissingValuesGroupingHandler extends AbstractInvoiceFilterGroupingHandler {
    private static final int    RAW_ITEM_LIMIT = 1000;
    private String             logicalSystemSegment;
    private String             logicalSystemKeySegment;
    private String             logicalSystemKey;
    private String             distributionChannelKeySegment;
    private String             distributionChannelKey;
    private String             invoiceSegment;
    private String             invoiceMissingSegment;
    private DataHubFeedService feedService;
    private String             poolName;
    private RawItemService     rawItemService;
    private List<String>       canonicalItems;
    private String             distributionChannelSegment;
    private String             distributionChannelMissingSegment;


    protected InvoiceMissingValuesGroupingHandler(String rawItem, int order) {
        super(rawItem, order);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends RawItem> List<CompositionGroup<T>> group(CompositionGroup<T> compositionGroup) {
        DataLoadingAction dataLoadingAction = compositionGroup.getItems().get(0).getDataLoadingAction();
        List<? extends RawItem> rawItems = getRawItems(dataLoadingAction);

        Stream logicalSystems = getItemsForKeyAndValue(rawItems.stream(), logicalSystemKeySegment, logicalSystemKey);
        Stream filteredSystems = getValueStreamForField(logicalSystems, logicalSystemSegment);
        final String logicalSystem = getFirstOrEmptyString(filteredSystems);
        if (isNotEmpty(logicalSystem)) {
            addLogicalSystemToItems(compositionGroup.getItems(), logicalSystem);
        }

        final String distributionChannel = getDistributionChannelFromRawItems(rawItems);
        updateDistributionChannelToItems(compositionGroup.getItems(), distributionChannel);
        setInvoiceToEntries(rawItems,compositionGroup);
        return Collections.singletonList(compositionGroup);
    }

    private <T extends RawItem> void setInvoiceToEntries(List<? extends RawItem> rawItems, CompositionGroup<T> compositionGroup) {
        Stream filteredSystems = getValueStreamForField(rawItems.stream(), invoiceSegment);
        final String invoice = getFirstOrEmptyString(filteredSystems);
        if (isNotEmpty(invoice)) {
            addInvoiceToItems(compositionGroup.getItems(), invoice);
        }
    }

    private void addInvoiceToItems(List<? extends RawItem> items, String logicalSystem) {
        items.forEach(item -> {
            item.setField(invoiceMissingSegment, logicalSystem);
        });
    }

    @SuppressWarnings("unchecked")
    private String getDistributionChannelFromRawItems(List<? extends RawItem> rawItems) {
        Stream<RawItem> itemsWithDistributionChannelKey = getItemsForKeyAndValue(rawItems.stream(), distributionChannelKeySegment, distributionChannelKey);
        Stream<String> distributionChannels = itemsWithDistributionChannelKey.map(o -> (String) o.getField(distributionChannelSegment)).filter(StringUtils::isNotEmpty);
        return getFirstOrEmptyString(distributionChannels);
    }

    private void updateDistributionChannelToItems(List<? extends RawItem> items, final String distributionChannel) {
        if (isNotEmpty(distributionChannel)) {
            items.forEach(item -> item.setField(distributionChannelMissingSegment, distributionChannel));
        }
    }

    private String getFirstOrEmptyString(Stream<String> stream) {
        return stream.map(Optional::ofNullable).findFirst().flatMap(identity()).orElse(EMPTY);
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> compositionGroup) {
        String itemType = compositionGroup.getAttributes().get(0).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata().getItemType();
        return isMatchingCanonical(itemType);
    }

    private boolean isMatchingCanonical(String itemType) {
        if (canonicalItems != null && !canonicalItems.isEmpty() && !canonicalItems.contains(itemType)) {
            return false;
        }
        return true;
    }

    private List<? extends RawItem> getRawItems(DataLoadingAction dataLoadingAction) {
        DataHubPool configPool = this.feedService.findPoolByName(poolName);
        return rawItemService.findInPool(configPool, getRawItem(), RAW_ITEM_LIMIT, Collections.singletonList(dataLoadingAction), new HashMap<>(), RawItemStatusType.PROCESSED, RawItemStatusType.PENDING);
    }

    private void addLogicalSystemToItems(List<? extends RawItem> items, String logicalSystem) {
        items.forEach(item -> {
            item.setField(logicalSystemSegment, logicalSystem);
        });
    }

    @Required
    public void setDistributionChannelMissingSegment(String distributionChannelMissingSegment) {
        this.distributionChannelMissingSegment = distributionChannelMissingSegment;
    }

    @Required
    public void setDistributionChannelKeySegment(String distributionChannelKeySegment) {
        this.distributionChannelKeySegment = distributionChannelKeySegment;
    }

    @Required
    public void setDistributionChannelKey(String distributionChannelKey) {
        this.distributionChannelKey = distributionChannelKey;
    }

    @Required
    public void setFeedService(DataHubFeedService feedService) {
        this.feedService = feedService;
    }

    @Required
    public void setRawItemService(RawItemService rawItemService) {
        this.rawItemService = rawItemService;
    }

    @Required
    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    @Required
    public void setCanonicalItems(List<String> canonicalItems) {
        this.canonicalItems = canonicalItems;
    }

    @Required
    public void setLogicalSystemSegment(String logicalSystemSegment) {
        this.logicalSystemSegment = logicalSystemSegment;
    }

    @Required
    public void setDistributionChannelSegment(String distributionChannelSegment) {
        this.distributionChannelSegment = distributionChannelSegment;
    }

    public void setInvoiceSegment(String invoiceSegment) {
        this.invoiceSegment = invoiceSegment;
    }

    public void setInvoiceMissingSegment(String invoiceMissingSegment) {
        this.invoiceMissingSegment = invoiceMissingSegment;
    }

	public String getLogicalSystemKeySegment() {
		return logicalSystemKeySegment;
	}

	@Required
	public void setLogicalSystemKeySegment(String logicalSystemKeySegment) {
		this.logicalSystemKeySegment = logicalSystemKeySegment;
	}

	public String getLogicalSystemKey() {
		return logicalSystemKey;
	}

	@Required
	public void setLogicalSystemKey(String logicalSystemKey) {
		this.logicalSystemKey = logicalSystemKey;
	}
}

import io.gatling.recorder.GatlingRecorder
import io.gatling.recorder.config.RecorderPropertiesBuilder

object Recorder extends App {

    val props = new RecorderPropertiesBuilder
    props.simulationOutputFolder(IdePathHelper.recorderOutputDirectory.toString)
    props.simulationPackage("computerdatabase")
    props.bodiesFolder(IdePathHelper.bodiesDirectory.toString)

    GatlingRecorder.fromMap(props.build, Some(IdePathHelper.recorderConfigFile))
}

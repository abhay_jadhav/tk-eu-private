import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder

object Engine extends App {

    val props = new GatlingPropertiesBuilder
    props.dataDirectory(IdePathHelper.dataDirectory.toString)
    props.resultsDirectory(IdePathHelper.resultsDirectory.toString)
    props.bodiesDirectory(IdePathHelper.bodiesDirectory.toString)
    props.binariesDirectory(IdePathHelper.mavenBinariesDirectory.toString)

    Gatling.fromMap(props.build)
}

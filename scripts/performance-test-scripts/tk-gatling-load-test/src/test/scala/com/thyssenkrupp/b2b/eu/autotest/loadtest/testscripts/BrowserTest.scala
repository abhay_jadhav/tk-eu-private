package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Properties

class BrowserTest extends Simulation {

    val url = csv("testConfig.csv").records.get(0).get("url").get
    val httpProtocolNormalUser = http.baseURL(url)

    val noOfUsers = Properties.propOrElse("users", "5")
    val durationOfTest = Properties.propOrElse("duration", "62")

    val user = csv("user.csv").circular
    val categoryUrl = csv("main_category.csv").random

    val scenario2 = scenario("Scenario 2 - Browse").feed(user).feed(categoryUrl).during(durationOfTest.toInt minute) {
        group("HomePage") {
            exec(HomePage.homePage)
        }.group("LoginPage") {
            exec(LoginPage.userLoginPage)
        }.group("Perform Successfull Login") {
            exec(LogIn.userLogIn)
        }.group("Main Category Page") {
            exec(MainCategory.mainCategoryPage)
        }.group("Product Details Page") {
            exec(UserPDP.userPDP)
        }.group("Profile Details Page") {
            exec(ProfileDetails.profileDetailsPage)
        }.group("Logout from the site") {
            exec(LogOut.userLogOut)
        }
    }

    setUp(scenario2.inject(atOnceUsers(noOfUsers.toInt))
    ).protocols(httpProtocolNormalUser)

}

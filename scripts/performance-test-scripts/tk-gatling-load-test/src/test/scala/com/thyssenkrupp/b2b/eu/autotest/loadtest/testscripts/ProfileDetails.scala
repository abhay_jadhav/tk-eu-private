package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

object ProfileDetails {

    val profileDetailsPage = exec(http("ProfileDetails")
            .get("/tkSchulteSite/de/my-account/update-profile")
            .headers(Headers.mainCategoryHeader))
}
package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.structure.ConditionalStatements
import io.gatling.core.session._


object UserPDP {

    val userPDP = exec(http("Pdp Page")
            .get("${pdpUrl}")
            .headers(Headers.pdpPageHeaders_0))

}
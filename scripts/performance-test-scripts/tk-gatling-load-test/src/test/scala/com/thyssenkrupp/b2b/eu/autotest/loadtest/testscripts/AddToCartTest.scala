package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Properties

class AddToCartTest extends Simulation {

    val url = csv("testConfig.csv").records.get(0).get("url").get
    val httpProtocolNormalUser = http.baseURL(url)

    val noOfUsers = Properties.propOrElse("users", "5")
    val durationOfTest = Properties.propOrElse("duration", "62")

    val user = csv("user.csv").circular
    val searchTerm = csv("search.csv").circular
    val products = csv("productIds.csv").circular

    val scenario1 = scenario("Scenario 1 - Add to Cart").feed(user).feed(searchTerm).feed(products).during(durationOfTest.toInt minute) {
        group("HomePage") {
            exec(HomePage.homePage)
        }.group("LoginPage") {
            exec(LoginPage.userLoginPage)
        }.group("Perform Successfull Login") {
            exec(LogIn.userLogIn)
        }.group("Search For Product") {
            exec(Search.searchProduct)
        }.group("Navigate to pdp") {
            exec(UserPDP.userPDP)
        }.group("Add To Cart") {
            exec(UserCart.addToCart)
        }.group("Navigate To Cart") {
            exec(UserCart.cartPage)
        }.group("Logout from the site") {
            exec(LogOut.userLogOut)
        }
    }

    setUp(scenario1.inject(atOnceUsers(noOfUsers.toInt))).protocols(httpProtocolNormalUser)
}
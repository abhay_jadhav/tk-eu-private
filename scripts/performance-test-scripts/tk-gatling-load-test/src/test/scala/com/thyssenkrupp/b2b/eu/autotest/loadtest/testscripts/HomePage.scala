package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

object HomePage {

    val homePage = exec(flushHttpCache)
            .exec(http("HomePage")
                    .get("/")
                    .headers(Headers.homePageHeaders_0))
}
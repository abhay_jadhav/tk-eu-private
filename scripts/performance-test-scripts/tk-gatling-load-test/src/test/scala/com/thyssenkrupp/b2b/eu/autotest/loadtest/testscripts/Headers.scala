package com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

object Headers {
    final val CHROME_AGENT_STRING = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"

    //HomePage
    val homePageHeaders_0 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)

    //Login
    val userLoginHeaders_0 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)

    val userLoginHeaders_1 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Cache-Control" -> "max-age=0",
        "Connection" -> "keep-alive",
        "Origin" -> "https://tk-eu-dev-tks.mindcurv.com",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)
    val userLoginHeaders_2 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Cache-Control" -> "max-age=0",
        "Connection" -> "keep-alive",
        "Origin" -> "https://tk-eu-dev-tks.mindcurv.com",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)
    //Search Pages
    val searchPageHeaders_0 = Map(
        "Accept" -> "application/json, text/javascript, */*; q=0.01",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "User-Agent" -> CHROME_AGENT_STRING,
        "X-Requested-With" -> "XMLHttpRequest")
    val searchPageHeaders_1 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)
    //Pdp Pages
    val pdpPageHeaders_0 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)
    //Cart Pages
    val cartPageHeaders_0 = Map(
        "Accept" -> "*/*",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
        "Origin" -> "https://tk-eu-dev-tks.mindcurv.com",
        "User-Agent" -> CHROME_AGENT_STRING,
        "X-Requested-With" -> "XMLHttpRequest")
    val cartPageHeaders_1 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, sdch, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Upgrade-Insecure-Requests" -> "1",
        "User-Agent" -> CHROME_AGENT_STRING)
    //category pages
    val mainCategoryHeader = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.8",
        "Connection" -> "keep-alive",
        "Referer" -> "https://tk-eu-dev-tks.mindcurv.com/tkSchulteSite/de/",
        "User-Agent" -> CHROME_AGENT_STRING)
}
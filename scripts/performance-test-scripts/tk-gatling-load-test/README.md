# Gatling Load Tests

Load test script created using Gatling. 

## Execution Commands

**Specifying the test cases:**

	mvn gatling:execute -Dgatling.simulationClass=com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts.BrowserTest
	mvn gatling:execute -Dgatling.simulationClass=com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts.AddToCartTest

**Specifying number of users and test duration**

	mvn gatling:execute -Dgatling.simulationClass=com.thyssenkrupp.b2b.eu.autotest.loadtest.testscripts.BrowserTest -Dusers=10 -Dduration=240

The test duration can be specified using the veritable **duration** (minute)and number of users for the test can be specified using the veritable **users**. If the variable **duration and users** were **not defined**, then the test will be executed for the **one hour with 5 users**.


## Scripts

**Add to cart** and **Browse page** are the two scenarios are executing as the part of load test

## Steps
Following steps are executing as the part of the load test.

### Add to cart
```
1) Open the site
2) login
3) Search for  a product
4) Product details page
5) Add to cart
6) Navigate to cart
7) logout
```
### Browse
``` 
1) Open the site
2) Login
3) Main category page
4) Product details page
4) profile details page
6) Logout
```

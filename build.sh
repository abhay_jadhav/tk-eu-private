#!/bin/bash

touch hybris/bin/platform/lib/dbdriver/.lastupdate

#Download MySQL Java Connector
if [ ! -f hybris/bin/platform/lib/dbdriver/mysql-connector-java-5.1.40.jar ]; then
  mvn dependency:get -DrepoUrl=http://repo1.maven.org/maven2 -Dartifact=mysql:mysql-connector-java:5.1.40:jar -Dtransitive=false
  mvn dependency:copy -Dartifact=mysql:mysql-connector-java:5.1.40:jar -DoutputDirectory=hybris/bin/platform/lib/dbdriver/
else
  echo MySql Java Connector already downloaded.
fi

if [ ! -f .hybris-built ]; then
cd hybris/bin/platform/ &&
  . ./setantenv.sh &&
  ant addoninstall -Daddonnames='sapinvoiceaddon,thyssenkruppwishlistaddon,thyssenkruppb2bacceleratoraddon,thyssenkruppconfigurableproductsaddon,thyssenkrupplocalizationaddon,b2bacceleratoraddon,commerceorgaddon,commerceorgsamplesaddon,promotionenginesamplesaddon,textfieldconfiguratortemplateaddon,smarteditaddon,thyssenkruppassistedservicestorefront,customerticketingaddon,liveeditaddon,orderselfserviceaddon,thyssenkruppeustorefrontaddon,secureportaladdon' -DaddonStorefront.yacceleratorstorefront='thyssenkruppstorefront' &&
  ant customize &&
  ant clean all &&
  cd ../../../ && touch .hybris-built
else
  echo Hybris Initial build already complete.
fi

if ! grep -q thyssenkrupp /etc/hosts; then
  echo Adding entry to host file.
  echo "127.0.0.1       thyssenkrupp-plastics.local" | sudo tee --append /etc/hosts
  echo "127.0.0.1       thyssenkrupp-schulte.local"  | sudo tee --append /etc/hosts
else
  echo Host entry already present.
fi

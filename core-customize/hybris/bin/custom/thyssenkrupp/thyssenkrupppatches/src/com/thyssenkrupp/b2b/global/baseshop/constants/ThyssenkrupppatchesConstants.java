package com.thyssenkrupp.b2b.global.baseshop.constants;

/**
 * Global class for all thyssenkrupppatches constants. You can add global constants for your
 * extension into this class.
 */
public final class ThyssenkrupppatchesConstants extends GeneratedthyssenkrupppatchesConstants {
    public static final String EXTENSIONNAME = "thyssenkrupppatches";

    private ThyssenkrupppatchesConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "thyssenkrupppatchesPlatformLogo";
}

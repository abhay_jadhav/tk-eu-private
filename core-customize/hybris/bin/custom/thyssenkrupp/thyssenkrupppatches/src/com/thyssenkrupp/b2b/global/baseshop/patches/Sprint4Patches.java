package com.thyssenkrupp.b2b.global.baseshop.patches;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.global.baseshop.constants.ThyssenkrupppatchesConstants;

/**
 *
 */
@SystemSetup(extension = ThyssenkrupppatchesConstants.EXTENSIONNAME, patch = true)
public class Sprint4Patches extends AbstractSystemSetup {

    private static final Logger LOG = Logger.getLogger(Sprint4Patches.class);

    @SystemSetup(name = "Sprint-4 - Import ASM CMS Data", process = SystemSetup.Process.ALL, type = SystemSetup.Type.ALL)
    public void importAsmCmsData() {
        importImpexFile("/thyssenkrupppatches/Sprint-4/asm-module-data.impex", true);
    }

    @SystemSetup(name = "Sprint-4 -Import ASM User Data", process = SystemSetup.Process.ALL, type = SystemSetup.Type.ALL)
    public void importAsmUserData() {
        importImpexFile("/thyssenkrupppatches/Sprint-4/asm-module-userdata.impex", true);
    }

    public void importImpexFile(final String file, final boolean errorIfMissing) {
        getSetupImpexService().importImpexFile(file, errorIfMissing);
    }

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }

}

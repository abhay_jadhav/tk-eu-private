package com.thyssenkrupp.b2b.global.baseshop.setup;

import com.thyssenkrupp.b2b.global.baseshop.constants.ThyssenkrupppatchesConstants;
import com.thyssenkrupp.b2b.eu.core.service.TkSynchronizationService;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkShopOrganisation;

import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.patches.AbstractPatchesSystemSetup;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SystemSetup(extension = ThyssenkrupppatchesConstants.EXTENSIONNAME)
public class ThyssenkrupppatchesSystemSetup extends AbstractPatchesSystemSetup {

    private TkSynchronizationService synchronizationService;
    private SetupImpexService setupImpexService;

    @Override
    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
    public void createEssentialData(final SystemSetupContext setupContext) {
        super.createEssentialData(setupContext);
        getSetupImpexService().importImpexFile("/thyssenkrupppatches/releases/release_4/patch_sprint_4_20_00/global/rsprint_4_20_00_essentialData.impex", true);
        getSetupImpexService().importImpexFile("/thyssenkrupppatches/releases/release_global/patch_essential_solr_config.impex", true);
    }

    @Override
    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext setupContext) {
        super.createProjectData(setupContext);
        final List<String> shopNameList = Stream.of(TkShopOrganisation.values()).map(TkShopOrganisation::getCode).collect(Collectors.toList());
        getSynchronizationService().syncCatalogAndIndexForShops(shopNameList);
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        return super.getInitializationOptions();
    }


    public TkSynchronizationService getSynchronizationService() {
        return synchronizationService;
    }

    @Required
    public void setSynchronizationService(TkSynchronizationService synchronizationService) {
        this.synchronizationService = synchronizationService;
    }

    public SetupImpexService getSetupImpexService() {
        return setupImpexService;
    }

    public void setSetupImpexService(SetupImpexService setupImpexService) {
        this.setupImpexService = setupImpexService;
    }
    
}

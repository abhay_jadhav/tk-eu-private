package com.thyssenkrupp.b2b.global.baseshop.patches.releases.release3;

import com.thyssenkrupp.b2b.global.baseshop.patches.releases.AbstractTkPatch;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class Sprint18Patch extends AbstractTkPatch {

    private static final String PATH_ID = "sprint_3_18_00";

    public Sprint18Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, Collections.singletonList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_18_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_18_00_001-globalData.impex", languages, updateLanguagesOnly);
    }
}

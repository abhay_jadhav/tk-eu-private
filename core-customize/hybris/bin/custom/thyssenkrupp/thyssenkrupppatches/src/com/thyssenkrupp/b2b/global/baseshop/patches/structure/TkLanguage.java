package com.thyssenkrupp.b2b.global.baseshop.patches.structure;

import de.hybris.platform.patches.organisation.ImportLanguage;

public enum TkLanguage implements ImportLanguage {

    EN_US("en_US"), DE_DE("de_DE");

    private String code;

    TkLanguage(final String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }

}

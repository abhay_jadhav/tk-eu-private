package com.thyssenkrupp.b2b.global.baseshop.patches.structure;

public enum TkEnvironment {

    DEV("DEV"), QA("QA"), UAT("UAT"), PROD("PROD"), ALL("ALL");

    private String code;

    TkEnvironment(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.patches.releases.release4;

import com.thyssenkrupp.b2b.global.baseshop.patches.releases.AbstractTkPatch;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class Sprint23Patch extends AbstractTkPatch {

    private static final String PATH_ID = "sprint_4_23_00";

    public Sprint23Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, Collections.singletonList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        //empty implementation
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_4_23_00_001-cart-LocalizedMessages.impex", languages, updateLanguagesOnly);
    }
}

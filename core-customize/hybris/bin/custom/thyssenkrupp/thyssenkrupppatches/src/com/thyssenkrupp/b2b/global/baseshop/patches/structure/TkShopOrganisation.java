package com.thyssenkrupp.b2b.global.baseshop.patches.structure;

import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.ImportOrganisationUnit;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collection;

public enum TkShopOrganisation implements ImportOrganisationUnit<TkShopOrganisation, TkShopOrganisation> {

    TKS("tkSample", "TK Sample", new ImportLanguage[] { TkLanguage.DE_DE, TkLanguage.EN_US }, TkStructureState.V1);

    private static final String COMMON_FOLDER_NAME = Config.getString("patches.shops.common.folder.name","_commonShops");
    private static final String FOLDER_NAME        = Config.getString("patches.shops.folder.name","shops");
    private String                                                 code;
    private String                                                 name;
    private Collection<ImportLanguage>                             languages;
    private de.hybris.platform.patches.organisation.StructureState structureState;

    TkShopOrganisation(String code, String name, ImportLanguage[] languages, TkStructureState structureState) {
        this.code = code;
        this.name = name;
        this.languages = Arrays.asList(languages);
        this.structureState = structureState;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getFolderName() {
        return FOLDER_NAME;
    }

    @Override
    public String getCommonFolderName() {
        return COMMON_FOLDER_NAME;
    }

    @Override
    public Collection<TkShopOrganisation> getChildren() {
        return null;
    }

    @Override
    public Collection<ImportLanguage> getLanguages() {
        return this.languages;
    }

    @Override
    public TkShopOrganisation getParent() {
        return null;
    }

    @Override
    public void setParent(TkShopOrganisation tkShopOrganisation) {

    }

    @Override
    public de.hybris.platform.patches.organisation.StructureState getStructureState() {
        return this.structureState;
    }
}

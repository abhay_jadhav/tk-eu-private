package com.thyssenkrupp.b2b.global.baseshop.patches.releases;

import com.thyssenkrupp.b2b.global.baseshop.patches.structure.TkShopOrganisation;
import de.hybris.platform.patches.Patch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

public interface TkPatch extends Patch {
    void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly);

    void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly);
}

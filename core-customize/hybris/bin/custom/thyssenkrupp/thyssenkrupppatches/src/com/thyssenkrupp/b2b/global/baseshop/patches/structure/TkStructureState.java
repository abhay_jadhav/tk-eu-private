package com.thyssenkrupp.b2b.global.baseshop.patches.structure;

import de.hybris.platform.patches.organisation.StructureState;

public enum TkStructureState implements StructureState {
    V1, V2, V3, LAST;

    @Override
    public boolean isAfter(final StructureState structureState) {
        if (this == structureState) {
            return false;
        }
        for (final StructureState state : values()) {
            if (structureState.equals(state)) {
                return true;
            }
            if (this.equals(state)) {
                return false;
            }
        }
        return false;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.patches.structure;


public enum TkRelease implements de.hybris.platform.patches.Release {

    R3("3"), R4("4");

    private String releaseId;

    TkRelease(final String releaseId) {
        this.releaseId = releaseId;
    }

    @Override
    public String getReleaseId() {
        return this.releaseId;
    }

}

package com.thyssenkrupp.b2b.global.baseshop.service.impl;

import javax.annotation.Resource;

import org.junit.Before;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;

/**
 * This is an example of how the integration test should look like. {@link ServicelayerBaseTest}
 * bootstraps platform so you have an access to all Spring beans as well as database connection. It
 * also ensures proper cleaning out of items created during the test after it finishes. You can
 * inject any Spring service using {@link Resource} annotation. Keep in mind that by default it
 * assumes that annotated field name matches the Spring Bean ID.
 */
@IntegrationTest
public class DefaultThyssenkrupppatchesServiceIntegrationTest extends ServicelayerBaseTest {

    @Before
    public void setUp() throws Exception {
        // implement here code executed before each test
    }
}

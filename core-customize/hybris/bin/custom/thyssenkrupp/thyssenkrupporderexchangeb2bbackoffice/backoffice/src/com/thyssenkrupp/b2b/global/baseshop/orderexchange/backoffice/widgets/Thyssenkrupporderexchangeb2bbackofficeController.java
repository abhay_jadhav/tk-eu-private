
package com.thyssenkrupp.b2b.global.baseshop.orderexchange.backoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.backoffice.services.Thyssenkrupporderexchangeb2bbackofficeService;

public class Thyssenkrupporderexchangeb2bbackofficeController extends DefaultWidgetController {
    private static final long  serialVersionUID = 1L;
    private              Label label;

    @WireVariable
    private transient Thyssenkrupporderexchangeb2bbackofficeService thyssenkrupporderexchangeb2bbackofficeService;

    @Override
    public void initialize(final Component comp) {
        super.initialize(comp);
        label.setValue(thyssenkrupporderexchangeb2bbackofficeService.getHello() + " Thyssenkrupporderexchangeb2bbackofficeController");
    }
}

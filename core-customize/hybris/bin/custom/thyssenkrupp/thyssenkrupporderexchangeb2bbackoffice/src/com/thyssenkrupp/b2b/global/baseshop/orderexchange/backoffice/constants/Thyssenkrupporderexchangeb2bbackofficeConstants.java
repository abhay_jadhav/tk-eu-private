
package com.thyssenkrupp.b2b.global.baseshop.orderexchange.backoffice.constants;

public final class Thyssenkrupporderexchangeb2bbackofficeConstants extends GeneratedThyssenkrupporderexchangeb2bbackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkrupporderexchangeb2bbackoffice";

    private Thyssenkrupporderexchangeb2bbackofficeConstants() {

    }
}

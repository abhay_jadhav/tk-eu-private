package com.thyssenkrupp.b2b.global.baseshop.initialdata.constants;

/**
 * Global class for all ThyssenkruppInitialData constants.
 */
public final class ThyssenkruppInitialDataConstants extends GeneratedThyssenkruppInitialDataConstants {
    public static final String EXTENSIONNAME = "thyssenkruppinitialdata";

    private ThyssenkruppInitialDataConstants() {
        // empty
    }
}

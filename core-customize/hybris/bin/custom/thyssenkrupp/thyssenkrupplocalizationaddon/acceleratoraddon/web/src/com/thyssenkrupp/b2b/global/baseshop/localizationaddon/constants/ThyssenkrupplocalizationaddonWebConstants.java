package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.constants;

public final class ThyssenkrupplocalizationaddonWebConstants {
    private ThyssenkrupplocalizationaddonWebConstants() {
        //empty to avoid instantiating this constant class
    }
}

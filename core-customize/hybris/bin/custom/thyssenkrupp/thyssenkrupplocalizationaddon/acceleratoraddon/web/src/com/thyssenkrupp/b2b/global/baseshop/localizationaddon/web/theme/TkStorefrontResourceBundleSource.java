package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.web.theme;

import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.web.message.source.DatabaseMessageSource;
import com.thyssenkrupp.b2b.global.baseshop.storefront.web.theme.StorefrontResourceBundleSource;
import org.springframework.context.MessageSource;
import org.springframework.ui.context.Theme;
import org.springframework.ui.context.support.SimpleTheme;

public class TkStorefrontResourceBundleSource extends StorefrontResourceBundleSource {

    private boolean dbMessagesEnabled;
    private DatabaseMessageSource dataBaseMessageSource;

    protected Theme computeThemeForGivenKey(final String themeName) {
        Theme theme = super.computeThemeForGivenKey(themeName);
        if (isDbMessagesEnabled()) {
            MessageSource parentMessageSource = theme.getMessageSource();
            getDataBaseMessageSource().setParentMessageSource(parentMessageSource);
            return new SimpleTheme(themeName, getDataBaseMessageSource());
        }
        return theme;
    }

    public DatabaseMessageSource getDataBaseMessageSource() {
        return dataBaseMessageSource;
    }

    public void setDataBaseMessageSource(DatabaseMessageSource dataBaseMessageSource) {
        this.dataBaseMessageSource = dataBaseMessageSource;
    }

    public boolean isDbMessagesEnabled() {
        return dbMessagesEnabled;
    }

    public void setDbMessagesEnabled(boolean dbMessagesEnabled) {
        this.dbMessagesEnabled = dbMessagesEnabled;
    }
}

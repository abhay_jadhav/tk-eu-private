package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.web.message.source;

import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.cache.LocalizedMessageCacheService;
import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.LocalizedMessageService;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Optional;

public class DatabaseMessageSource extends AbstractMessageSource {

    private static final Logger LOG = Logger.getLogger(DatabaseMessageSource.class);
    private LocalizedMessageService      messageService;
    private BaseStoreService             baseStoreService;
    private LocalizedMessageCacheService messageCacheService;

    @Override
    protected MessageFormat resolveCode(final String key,final Locale locale) {
        BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
        CacheKey cacheKey = getMessageCacheService().getKey(key, currentBaseStore.getUid(), locale);
        Optional<String> cachedValue = getMessageCacheService().get(cacheKey);
        if (cachedValue.isPresent()) {
            LOG.debug(String.format("No Cached LocalizedMessage found cache for key:%s Locale:%s Store:%s", key, locale, currentBaseStore.getUid()));
            return createMessageFormat(cachedValue.get(), locale);
        } else {
            Optional<String> dbValue = getMessageService().getMessageValueByKeyAndLocale(key, currentBaseStore, locale);
            if (dbValue.isPresent()) {
                LOG.debug(String.format("Adding LocalizedMessage to cache for key:%s Locale:%s Store:%s", key, locale, currentBaseStore.getUid()));
                getMessageCacheService().put(cacheKey, dbValue.get());
                return createMessageFormat(dbValue.get(), locale);
            }
            return null;
        }
    }

    public LocalizedMessageService getMessageService() {
        return messageService;
    }

    @Required
    public void setMessageService(LocalizedMessageService messageService) {
        this.messageService = messageService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public LocalizedMessageCacheService getMessageCacheService() {
        return messageCacheService;
    }

    @Required
    public void setMessageCacheService(LocalizedMessageCacheService messageCacheService) {
        this.messageCacheService = messageCacheService;
    }
}

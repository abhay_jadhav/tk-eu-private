package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.cache;

import de.hybris.platform.regioncache.key.CacheKey;

import java.util.Locale;
import java.util.Optional;

public interface LocalizedMessageCacheService {

    Optional<String> get(CacheKey key);

    void put(CacheKey key, String content);

    boolean useCache();

    CacheKey getKey(String key, String storeId, Locale locale);
}

package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.constants;

public final class ThyssenkrupplocalizationaddonConstants extends GeneratedThyssenkrupplocalizationaddonConstants {
    public static final String EXTENSIONNAME = "thyssenkrupplocalizationaddon";

    private ThyssenkrupplocalizationaddonConstants() {
        // empty to avoid instantiating this constant class
    }
}

package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.cache.impl;

import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.cache.LocalizedMessageCacheService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Locale;
import java.util.Optional;

public class DefaultLocalizedMessageCacheService implements LocalizedMessageCacheService {

    private static final String CACHE_ENABLED_KEY = "thyssenkrupplocalizationaddon.localizedMessage.useCache.enabled";

    private ConfigurationService configurationService;

    private CacheController cacheController;

    @Override
    public Optional<String> get(CacheKey key) {
        return Optional.ofNullable(getCacheController().get(key));
    }

    @Override
    public void put(CacheKey key, String content) {
        getCacheController().getWithLoader(key, cacheKey -> content);
    }

    @Override
    public boolean useCache() {
        return getConfigurationService().getConfiguration().getBoolean(CACHE_ENABLED_KEY);
    }

    @Override
    public CacheKey getKey(String key, String storeId, Locale locale) {
        return new LocalizedMessageCacheKey(key, storeId, Registry.getCurrentTenant().getTenantID(), locale);
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public CacheController getCacheController() {
        return cacheController;
    }

    public void setCacheController(CacheController cacheController) {
        this.cacheController = cacheController;
    }
}

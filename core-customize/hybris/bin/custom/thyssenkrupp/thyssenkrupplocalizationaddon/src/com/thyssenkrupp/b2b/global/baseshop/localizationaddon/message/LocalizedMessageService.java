/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message;

import java.util.Locale;
import java.util.Optional;

import de.hybris.platform.store.BaseStoreModel;

public interface LocalizedMessageService {

    Optional<String> getMessageValueByKeyAndLocale(String key,BaseStoreModel baseStoreModel,Locale locale);

}

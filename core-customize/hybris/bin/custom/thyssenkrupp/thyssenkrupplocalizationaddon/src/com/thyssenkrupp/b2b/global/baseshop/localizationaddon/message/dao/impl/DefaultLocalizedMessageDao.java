package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.dao.impl;

import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.dao.LocalizedMessageDao;
import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.model.TkLocalizedMessageModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.thyssenkrupp.b2b.global.baseshop.localizationaddon.model.TkLocalizedMessageModel.VALUE;
import static com.thyssenkrupp.b2b.global.baseshop.localizationaddon.model.TkLocalizedMessageModel._TYPECODE;
import static com.thyssenkrupp.b2b.global.baseshop.localizationaddon.model.TkLocalizedMessageModel.KEY;
import static com.thyssenkrupp.b2b.global.baseshop.localizationaddon.model.TkLocalizedMessageModel.STORE;

public class DefaultLocalizedMessageDao extends DefaultGenericDao<TkLocalizedMessageModel> implements LocalizedMessageDao {

    public DefaultLocalizedMessageDao() {
        super(_TYPECODE);
    }

    public List<String> findMessageValueByKey(final String key,final BaseStoreModel baseStoreModel,final Locale locale){
        String queryString = String.format("SELECT {%s[%s]} FROM {%s} WHERE {%s}= ?key AND {%s}= ?store", VALUE, locale.toString(), _TYPECODE, KEY, STORE);
        final Map<String, Object> params = new HashMap<>(2);
        params.put("key", key);
        params.put("store",baseStoreModel);
        return doSearch(queryString,params,String.class);
    }

    private <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class<T> resultClass) {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        if (params != null) {
            searchQuery.addQueryParameters(params);
        }
        searchQuery.setResultClassList(Collections.singletonList(resultClass));
        final SearchResult<T> searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }
}

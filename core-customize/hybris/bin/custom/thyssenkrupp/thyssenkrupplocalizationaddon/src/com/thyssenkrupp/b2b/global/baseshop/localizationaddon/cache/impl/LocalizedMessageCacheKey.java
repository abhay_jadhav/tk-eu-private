package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.cache.impl;

import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;

import java.util.Locale;

public class LocalizedMessageCacheKey implements CacheKey {

    private static final String LOCALIZED_MESSAGE_CACHE_UNIT_CODE = "__LOCALIZED_MESSAGE_CACHE__";
    private final String key;
    private final String tenantId;
    private final Locale locale;
    private final String storeId;

    public LocalizedMessageCacheKey(String key, String storeId, String tenantId, Locale locale) {
        this.key = key;
        this.storeId = storeId;
        this.tenantId = tenantId;
        this.locale = locale;
    }

    @Override
    public CacheUnitValueType getCacheValueType() {
        return CacheUnitValueType.SERIALIZABLE;
    }

    @Override
    public Object getTypeCode() {
        return LOCALIZED_MESSAGE_CACHE_UNIT_CODE;
    }

    @Override
    public String getTenantId() {
        return tenantId;
    }

    @Override
    public boolean equals(Object cacheObject) {
        // START OF GENERATED CODE
        if (this == cacheObject) {
            return true;
        }
        if (cacheObject == null || getClass() != cacheObject.getClass()) {
            return false;
        }
        LocalizedMessageCacheKey that = (LocalizedMessageCacheKey) cacheObject;
        if (!key.equals(that.key)) {
            return false;
        }
        if (!tenantId.equals(that.tenantId)) {
            return false;
        }
        if (!locale.equals(that.locale)) {
            return false;
        }
        // END OF GENERATED CODE
        return storeId.equals(that.storeId);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + tenantId.hashCode();
        result = 31 * result + locale.hashCode();
        result = 31 * result + storeId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("LocalizedMessageCacheKey{key='%s', storeId='%s', locale=%s}", key, storeId, locale);
    }
}

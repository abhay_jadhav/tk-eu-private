package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.impl;

import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.LocalizedMessageService;
import com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.dao.LocalizedMessageDao;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class DefaultLocalizedMessageService implements LocalizedMessageService {

    private static final Logger LOG = Logger.getLogger(DefaultLocalizedMessageService.class);

    private LocalizedMessageDao messageDao;
    private CommonI18NService   commonI18NService;

    @Override
    public Optional<String> getMessageValueByKeyAndLocale(final String key, final BaseStoreModel baseStoreModel, final Locale locale) {
        validateParameterNotNullStandardMessage("key", key);
        validateParameterNotNullStandardMessage("BaseStore", baseStoreModel);
        validateParameterNotNullStandardMessage("locale", locale);
        List<String> messageValues = new ArrayList<>();
        Predicate<List> isEmptyList = List::isEmpty;
        try {
            messageValues = getMessageDao().findMessageValueByKey(key, baseStoreModel, locale);
            if(messageValues.isEmpty() || !messageValues.stream().filter(Objects::nonNull).findFirst().isPresent()){
                messageValues = getFallbackLocaleList(locale).stream().map(localeFromList -> getMessageDao().findMessageValueByKey(key, baseStoreModel, localeFromList))
                  .filter(isEmptyList.negate()).findFirst().orElse(Collections.emptyList());
            }
            LOG.debug("Message count: for '" + messageValues.size() + "' for  key [" + key + "], locale [" + locale + "]");
        } catch (Exception ex) {
            LOG.error("Could not find localized message for key [" + key + "], locale [" + locale + "]");
        }
        return messageValues.stream().filter(Objects::nonNull).findFirst();
    }

    private List<Locale> getFallbackLocaleList(Locale locale) {
        return getFallbackLanguagesFromLocale(locale).stream().map(getCommonI18NService()::getLocaleForLanguage).collect(Collectors.toList());
    }

    private List<LanguageModel> getFallbackLanguagesFromLocale(Locale locale) {
        if(locale != null) {
            List fallbackLanguageList = getCommonI18NService().getLanguage(locale.toString()).getFallbackLanguages();
            return !fallbackLanguageList.isEmpty() ? fallbackLanguageList : getDefaultLanguage();
        }
        return getDefaultLanguage();
    }

    private List<LanguageModel> getDefaultLanguage() {
        return Collections.singletonList(getCommonI18NService().getLanguage(Locale.getDefault().getLanguage()));
    }

    public LocalizedMessageDao getMessageDao() {
        return messageDao;
    }

    public void setMessageDao(LocalizedMessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }
}

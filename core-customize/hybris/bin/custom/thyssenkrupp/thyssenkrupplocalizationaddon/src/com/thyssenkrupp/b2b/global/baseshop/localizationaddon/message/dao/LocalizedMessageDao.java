package com.thyssenkrupp.b2b.global.baseshop.localizationaddon.message.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Locale;

public interface LocalizedMessageDao extends Dao {
    List<String> findMessageValueByKey(String key, BaseStoreModel baseStoreModel, Locale locale);
}

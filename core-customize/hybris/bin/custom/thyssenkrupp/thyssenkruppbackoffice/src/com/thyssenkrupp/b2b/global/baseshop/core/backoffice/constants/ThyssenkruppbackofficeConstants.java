
package com.thyssenkrupp.b2b.global.baseshop.core.backoffice.constants;

public final class ThyssenkruppbackofficeConstants extends GeneratedThyssenkruppbackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkruppbackoffice";

    private ThyssenkruppbackofficeConstants() {

    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.junit.Before;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.service.Thyssenkrupporderexchangeb2bService;

@IntegrationTest
public class DefaultThyssenkrupporderexchangeb2bServiceIntegrationTest extends ServicelayerBaseTest {
    @Resource
    private Thyssenkrupporderexchangeb2bService thyssenkrupporderexchangeb2bService;
    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Before
    public void setUp() throws Exception {
        // implement here code executed before each test
    }
}

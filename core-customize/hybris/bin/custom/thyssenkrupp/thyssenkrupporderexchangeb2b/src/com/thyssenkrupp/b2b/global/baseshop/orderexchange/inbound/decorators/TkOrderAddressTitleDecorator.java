package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.decorators;

import java.util.Map;

import de.hybris.platform.core.model.user.TitleModel;
import org.apache.commons.lang.StringUtils;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.util.CSVCellDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TkOrderAddressTitleDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkOrderAddressTitleDecorator.class);
    private static final String IGNORE = "<ignore>";

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry
        .getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        LOG.debug("Entering Order AddressTitle Decorator.");
        final String impexContent = impexLine.get(Integer.valueOf(position));

        String titleName;
        try {
            titleName = impexContent;

            LOG.debug("TkOrderAddressTitleDecorator title name=\'{}\'.", titleName);

            if(StringUtils.isNotEmpty(titleName)
                && !IGNORE.equalsIgnoreCase(titleName)) {
                final TitleModel title = tKDataHubInboundOrderHelper.getTitleByName(titleName);
                LOG.debug("Exiting order AddressOwner Decorator with title code.");
                return title.getCode();
            }
        } catch (Exception e) {
            LOG.error("Error in processing address title"+ e.getMessage());
            titleName="";
        }
        LOG.debug("Exiting order AddressOwner Decorator.");
        return titleName;
    }

}

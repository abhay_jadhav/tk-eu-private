package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.AbstractRawItemBuilder;
import org.apache.log4j.Logger;

public class TkSapRawHybrisOrderBuilder extends AbstractRawItemBuilder<OrderModel> {
    private static final Logger LOG = Logger.getLogger(TkSapRawHybrisOrderBuilder.class);
    @Override
    protected Logger getLogger() {
        return LOG;
    }
}

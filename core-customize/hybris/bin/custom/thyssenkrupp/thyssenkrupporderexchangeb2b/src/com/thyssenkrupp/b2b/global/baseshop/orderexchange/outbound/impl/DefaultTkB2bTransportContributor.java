package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.MapUtils;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;

public class DefaultTkB2bTransportContributor implements RawItemContributor<OrderModel> {
    private static final String ORDER_CONFIGURATION_CODE     = "orderConfigurationCode";
    private static final String TRANSPORT_FLAG_CODE          = "BAPE_VBAK";
    private static final String TRANSPORT_FLAG_RELEVANT_CODE = "BAPE_VBAKX";

    @Override
    public Set<String> getColumns() {
        final Set<String> columns = new HashSet<>();
        columns.add(ORDER_CONFIGURATION_CODE);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel model) {
        final List<Map<String, Object>> rows = new ArrayList<>();
        return enhanceRowsByOrderConfigFields(model, rows);
    }

    private List<Map<String,Object>> enhanceRowsByOrderConfigFields(OrderModel model, List<Map<String, Object>> rows) {
        final Map<String, Object> transportFlagRow = createRowForCode(model, TRANSPORT_FLAG_CODE);
        final Map<String, Object> transportFlagRelevantRow = createRowForCode(model, TRANSPORT_FLAG_RELEVANT_CODE);

        if (!MapUtils.isEmpty(transportFlagRow)) {
            rows.add(transportFlagRow);
        }
        if (!MapUtils.isEmpty(transportFlagRelevantRow)) {
            rows.add(transportFlagRelevantRow);
        }

        return rows;
    }


    private Map<String, Object> createRowForCode(final OrderModel model, String code) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, model.getCode());
        row.put(ORDER_CONFIGURATION_CODE, code);
        return row;
    }
}

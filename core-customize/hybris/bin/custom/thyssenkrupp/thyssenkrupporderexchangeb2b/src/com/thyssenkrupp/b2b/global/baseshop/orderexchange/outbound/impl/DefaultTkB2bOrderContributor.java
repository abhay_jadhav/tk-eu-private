/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import static com.thyssenkrupp.b2b.global.baseshop.orderexchange.constants.Thyssenkrupporderexchangeb2bConstants.LANG_DE;
import static com.thyssenkrupp.b2b.global.baseshop.orderexchange.constants.Thyssenkrupporderexchangeb2bConstants.LANG_EN;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Splitter;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchangeb2b.outbound.impl.DefaultB2BOrderContributor;

public class DefaultTkB2bOrderContributor extends DefaultB2BOrderContributor {
     
    public static final String LANGUAGE_ISO_CODE = "languageCode";
    private static final Logger LOG = Logger.getLogger(DefaultTkB2bOrderContributor.class);
    private static final String SALES_OFFICE                = "salesOffice";
    private static final String ORDER_NAME                  = "orderName";
    private static final String SHIPPING_NOTE               = "shippingNote";
    private static final int    SHIPPINGNOTE_LENGTH         = 132;
    private static final String NEW_LINE_REGEX              = "\\r?\\n";
    private static final String SHIPPING_NOTE_FORMATTER_COL = "formatCol";
    private static final String SHIPPING_NOTE_FORMATTER_VAL = "/";
    private static final String SHIPPING_SEQNO              = "sequenceNo";

    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

    @Override
    public Set<String> getColumns() {
        final Set<String> columns = super.getColumns();
        columns.add(SALES_OFFICE);
        columns.add(ORDER_NAME);
        columns.add(SHIPPING_NOTE);
        columns.add(LANGUAGE_ISO_CODE);
        columns.add(OrderCsvColumns.CHANNEL);
        columns.add(OrderCsvColumns.PURCHASE_ORDER_NUMBER);
        columns.add(SHIPPING_NOTE_FORMATTER_COL);
        columns.add(SHIPPING_SEQNO);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel model) {

        if (StringUtils.isNotEmpty(model.getCustomerShippingNote())
            && getFormattedShippingNote(model.getCustomerShippingNote()).length() > SHIPPINGNOTE_LENGTH) {
            String formattedNote = getFormattedShippingNote(model.getCustomerShippingNote());
            List<Map<String, Object>> enhancedRows = new ArrayList<Map<String, Object>>();
            String[] splitNote = formattedNote.split(NEW_LINE_REGEX); // Split the shippingNote by
                                                                      // new line
            boolean isFormatColNeeded = false;
            for (int i = 0; i < splitNote.length; i++) {
                updateEnhancedRows(model, enhancedRows, splitNote[i], isFormatColNeeded);
                isFormatColNeeded = true;
            }

            return addSequenceNoToRows(enhancedRows, model.getCode());
        } else {
            final List<Map<String, Object>> rows = super.createRows(model);
            return addSequenceNoToRows(enhanceRowsByB2BFields(model, rows), model.getCode());
        }
    }

    private List<Map<String, Object>> addSequenceNoToRows(List<Map<String, Object>> enhancedRows, String orderId) {
        int i = 0;
        String seqNo = "SN_" + StringUtils.substringAfter(orderId, "-") + "_";
        for (Map<String, Object> rows : enhancedRows) {
            ++i;
            rows.put(SHIPPING_SEQNO, seqNo + i);
        }

        return enhancedRows;
    }

    private List<Map<String, Object>> updateEnhancedRows(final OrderModel model, final List<Map<String, Object>> enhancedRows, final String shippingNote, boolean isFormatColNeeded) {
        boolean isFormatColNeededTmp=isFormatColNeeded;
        if (!isShippingLengthOver(shippingNote)) {
            enhancedRows.addAll(createNewRows(model, shippingNote, isFormatColNeededTmp));
        } else {
            Iterable<String> iterable = Splitter.fixedLength(SHIPPINGNOTE_LENGTH).split(shippingNote); // Split the shippingNote by character length of 132
            for (String splitNote : iterable) {
                enhancedRows.addAll(createNewRows(model, splitNote, isFormatColNeededTmp));
                isFormatColNeededTmp = true;
            }
        }
        return enhancedRows;
    }
    
    private List<Map<String, Object>> createNewRows(final OrderModel model, final String shippingNote, boolean isFormatColNeeded) {
        List<Map<String, Object>> rows = super.createRows(model);
        return enhanceRowsForShippingNotes(model, rows, shippingNote, isFormatColNeeded);
    }

    private boolean isShippingLengthOver(String shippingNote) {
        return shippingNote.length() > SHIPPINGNOTE_LENGTH;
    }

    private List<Map<String, Object>> enhanceRowsForShippingNotes(OrderModel model, List<Map<String, Object>> rows, String splitNote, boolean isFormatColNeeded) {
        final Map<String, Object> row = rows.get(0);
        String salesOffice=getSalesOfficeForOrder(model);

        row.put(SALES_OFFICE, salesOffice);
        row.put(ORDER_NAME, model.getName());
        row.put(SHIPPING_NOTE, splitNote);
        if (isFormatColNeeded) {
            row.put(SHIPPING_NOTE_FORMATTER_COL, SHIPPING_NOTE_FORMATTER_VAL);
        }
        row.put(OrderCsvColumns.CHANNEL, model.getSite().getChannel());
        row.put(OrderCsvColumns.PURCHASE_ORDER_NUMBER, model.getPurchaseOrderNumber());

        String language = model.getLanguage().getIsocode();
        if (language.isEmpty() || language.equals(LANG_DE) || language.equals(LANG_EN)) {
            final List<LanguageModel> fallbackLanguages = model.getLanguage().getFallbackLanguages();
            if (!fallbackLanguages.isEmpty()) {
                language = fallbackLanguages.get(0).getIsocode();
            }
        }
        row.put(LANGUAGE_ISO_CODE, language);
        return rows;
    }

    private String getSalesOfficeForOrder(OrderModel model) {
        String salesOffice = null;

        try {
            Optional<AddressModel> contactAddress = model.getUser().getAddresses().stream()
                    .filter(address -> address.getContactAddress()).findFirst();
            if (contactAddress.isPresent()) {
                salesOffice = contactAddress.get().getDepartment();
            }
        } catch (Exception e) {
            LOG.error("Exception caught during retrieving contact address: " + e);
        }

        if (StringUtils.isEmpty(salesOffice)) {
            salesOffice = model.getUnit().getSalesOffice();
            if (StringUtils.isEmpty(salesOffice)) {
                B2BUnitModel parentUnit = b2bUnitService.getParent(model.getUnit());
                salesOffice = parentUnit != null ? parentUnit.getSalesOffice() : null;
            }
        }

        return salesOffice;
    }

    private String getFormattedShippingNote(String customerShippingNote) {
        return customerShippingNote.trim().replaceAll("(?m)(^ *| +(?= |$))", "").replaceAll("(?m)^$([\r\n]+?)(^$[\r\n]+?^)+", "$1").replaceAll("(?m)^\\s*$[\n\r]{1,}", "");
    }

    @Override
    protected List<Map<String, Object>> enhanceRowsByB2BFields(final OrderModel model, final List<Map<String, Object>> rows) {
        final Map<String, Object> row = rows.get(0);
        String salesOffice=getSalesOfficeForOrder(model);
        
        row.put(SALES_OFFICE, salesOffice);
        row.put(ORDER_NAME, model.getName());
        row.put(SHIPPING_NOTE, StringUtils.isNotEmpty(model.getCustomerShippingNote()) ? getFormattedShippingNote(model.getCustomerShippingNote()) : "NA");
        row.put(OrderCsvColumns.CHANNEL, model.getSite().getChannel());
        row.put(OrderCsvColumns.PURCHASE_ORDER_NUMBER, model.getPurchaseOrderNumber());

        String language = model.getLanguage().getIsocode();
        if (language.isEmpty() || language.equals(LANG_DE) || language.equals(LANG_EN)) {
            final List<LanguageModel> fallbackLanguages = model.getLanguage().getFallbackLanguages();
            if (!fallbackLanguages.isEmpty()) {
                language = fallbackLanguages.get(0).getIsocode();
            }
        }
        row.put(LANGUAGE_ISO_CODE, language);
        return rows;
    }

    /**
     * @return B2BUnitService
     */
    public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService() {
        return b2bUnitService;
    }

    /**
     * set the B2B unit service
     *
     * @param b2bUnitService
     */
    public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;

public class TkOrderRemoveReferencedItemsTranslator extends AbstractSpecialValueTranslator {
    private static final Logger LOG                 = Logger.getLogger(TkOrderRemoveReferencedItemsTranslator.class.getName());
    private static final String DH_HELPER_BEAN_NAME = "tKDataHubInboundOrderHelper";

    private ModelService                modelService;
    private TKDataHubInboundOrderHelper inboundOrderHelper;

    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) {
        if (inboundOrderHelper == null) {
            inboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean(DH_HELPER_BEAN_NAME);
        }
        modelService = inboundOrderHelper.getModelService();
    }

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        String orderCode = getOrderCode(processedItem);
        OrderModel orderModel = inboundOrderHelper.findOrder(orderCode);
        if (shouldBeImported(cellValue, orderModel.getABSerialisationKey())) {
            LOG.info(String.format("Removing reference items of order \'%s\'.", orderCode));
            removeOrderCertificates(orderModel);
            removeOrderEntries(orderModel);
            removeOrderAddresses(orderModel);
        }
    }

    private String getOrderCode(Item processedItem) throws ImpExException {
        String orderCode;
        try {
            orderCode = processedItem.getAttribute(DataHubInboundConstants.CODE).toString();
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        }
        return orderCode;
    }

    private boolean shouldBeImported(String importSerializationKey, String existingSerializationKey) {
        return Long.parseLong(importSerializationKey) >= Long.parseLong(existingSerializationKey);
    }

    private void removeOrderCertificates(OrderModel order) {
        order.getEntries().forEach(entry -> modelService.removeAll(entry.getProductInfos()));
    }

    private void removeOrderEntries(OrderModel order) {
        modelService.removeAll(order.getEntries());
    }

    private void removeOrderAddresses(OrderModel order) {
        AddressModel deliveryAddress = order.getDeliveryAddress();
        AddressModel paymentAddress = order.getPaymentAddress();
        if (paymentAddress != null) {
            modelService.remove(paymentAddress);
        }
        if (deliveryAddress != null) {
            modelService.remove(deliveryAddress);
        }
    }
}

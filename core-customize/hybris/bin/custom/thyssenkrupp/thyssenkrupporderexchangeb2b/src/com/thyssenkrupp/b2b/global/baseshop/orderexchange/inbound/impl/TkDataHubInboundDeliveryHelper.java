/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.datahub.inbound.impl.DefaultDataHubInboundDeliveryHelper;

public class TkDataHubInboundDeliveryHelper extends DefaultDataHubInboundDeliveryHelper {

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper;

    @Override
    public void processDeliveryAndGoodsIssue(final String orderCode, final String warehouseId, final String goodsIssueDate) {

        final OrderModel order = readOrder(orderCode);

        final OrderModel snapshot = gettKDataHubInboundOrderHelper().createOrderSnapShot(order);
        gettKDataHubInboundOrderHelper().createOrderHistory(order, snapshot);
        if (goodsIssueDate == null) {
            processDeliveryCreation(warehouseId, snapshot);
        } else {
            processDeliveryGoodsIssue(warehouseId, goodsIssueDate, snapshot);
        }
    }

    public TKDataHubInboundOrderHelper gettKDataHubInboundOrderHelper() {
        return tKDataHubInboundOrderHelper;
    }

    public void settKDataHubInboundOrderHelper(TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper) {
        this.tKDataHubInboundOrderHelper = tKDataHubInboundOrderHelper;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.constants;

@SuppressWarnings("deprecation")
public final class Thyssenkrupporderexchangeb2bConstants extends GeneratedThyssenkrupporderexchangeb2bConstants {
    public static final String EXTENSIONNAME = "thyssenkrupporderexchangeb2b";
    
    public static final String LANG_DE = "de_DE";
    public static final String LANG_EN = "en_US";

    private Thyssenkrupporderexchangeb2bConstants() {
    }
}

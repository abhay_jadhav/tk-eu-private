
package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.sap.orderexchange.datahub.inbound.impl.DefaultDataHubInboundOrderHelper;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static de.hybris.platform.core.model.order.AbstractOrderModel.CODE;
import static de.hybris.platform.core.model.order.OrderModel.VERSIONID;
import static de.hybris.platform.core.model.order.OrderModel._TYPECODE;

/**
 * Default Data Hub Order Inbound helper for order related notifications
 */
public class TKDataHubInboundOrderHelper extends DefaultDataHubInboundOrderHelper {
    private static final String SAP_ORDER_ID = "sapOrderId";

    private OrderHistoryService   orderHistoryService;
    private String                versionId;
    private FlexibleSearchService flexibleSearchService;

    public OrderModel createOrderHistory(final String orderNumber) {

        final OrderModel order = readOrder(orderNumber);
        if (order == null) {
            throw new UnknownIdentifierException("Order with code " + orderNumber + " not found for current user in current  BaseStore");
        }
        OrderModel snapshot = createOrderSnapShot(order);
        createOrderHistory(order, snapshot);
        versionId = snapshot.getVersionID();
        return snapshot;
    }

    public OrderHistoryEntryModel createOrderHistory(OrderModel order, OrderModel snapshot) {
        final OrderHistoryEntryModel historyEntry = getModelService().create(OrderHistoryEntryModel.class);
        historyEntry.setOrder(order);
        historyEntry.setPreviousOrderVersion(snapshot);
        historyEntry.setDescription("Updated by SAP");
        historyEntry.setTimestamp(new Date());
        getModelService().save(historyEntry);
        return historyEntry;
    }

    public OrderModel createOrderSnapShot(OrderModel order) {
        final OrderModel snapshot = getOrderHistoryService().createHistorySnapshot(order);
        getOrderHistoryService().saveHistorySnapshot(snapshot);
        return snapshot;
    }

    public Optional<OrderModel> getOrderDetails(final String code) {
        Optional<OrderModel> orderBySapId = findOrderByAttributeAndValue(code, SAP_ORDER_ID);
        if (orderBySapId.isPresent()) {
            return orderBySapId;
        } else {
            return findOrderByAttributeAndValue(code, CODE);
        }
    }

    private Optional<OrderModel> findOrderByAttributeAndValue(String code, String fieldName) {
        final String orderCodeQuery = "select {pk} from {" + _TYPECODE + "} where {" + fieldName + "} = ?code AND {" + VERSIONID + "} is null ";
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(orderCodeQuery);

        fQuery.addQueryParameter("code", code);
        final SearchResult<OrderModel> result = getFlexibleSearchService().search(fQuery);

        if (result != null && result.getCount() > 0) {
            return Optional.of(result.getResult().get(0));
        }
        return Optional.empty();
    }

    public OrderModel findOrder(String code) {
        return readOrder(code);
    }

    public OrderEntryModel findOrderEntry(final String orderCode, final String entryNumber) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
            "SELECT {entry.pk} FROM {"
                + "OrderEntry AS entry JOIN Order as o ON {entry.order} = {o.pk}"
                + "} WHERE {o.code} = ?code AND {o.versionID} IS NULL AND {entry.entryNumber} = ?number");

        flexibleSearchQuery.addQueryParameter("code", orderCode);
        flexibleSearchQuery.addQueryParameter("number", entryNumber);

        final OrderEntryModel orderEntry = getFlexibleSearchService().searchUnique(flexibleSearchQuery);

        if (orderEntry == null) {
            throw new IllegalArgumentException(String.format(
                "Error while processing inbound IDoc with order code [%s] and entryNo [%s] that does not exist!", orderCode, entryNumber));
        }

        return orderEntry;
    }

    public OrderEntryModel findOrderEntryFromSapOrderIdAndPosition(final String sapOrderId, final String entryNumber) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
            "SELECT {entry.pk} FROM {"
                + "OrderEntry AS entry JOIN Order as o ON {entry.order} = {o.pk}"
                + "} WHERE {o.sapOrderId} = ?sapOrderId AND {o.versionID} IS NULL AND {entry.entryNumber} = ?number");

        flexibleSearchQuery.addQueryParameter("sapOrderId", sapOrderId);
        flexibleSearchQuery.addQueryParameter("number", entryNumber);

        final OrderEntryModel orderEntry = getFlexibleSearchService().searchUnique(flexibleSearchQuery);

        if (orderEntry == null) {
            throw new IllegalArgumentException(String.format(
                "Error while processing inbound IDoc with sapOrder Id [%s] and entryNo [%s] that does not exist!", sapOrderId, entryNumber));
        }
        return orderEntry;
    }

    public Optional<ProductModel> findProduct(String code) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
            "SELECT {p.pk} FROM {"
                + "Product AS p JOIN Catalog AS c "
                + "ON {p.catalog} = {c.pk} "
                + "JOIN CatalogVersion AS cv "
                + "ON {p.catalogVersion} = {cv.pk} "
                + "} WHERE {p.code} = ?code AND {cv.active} = ?activeFlag");

        flexibleSearchQuery.addQueryParameter("code", code);
        flexibleSearchQuery.addQueryParameter("activeFlag", "1");
        final SearchResult searchResult = getFlexibleSearchService().search(flexibleSearchQuery);

        if (searchResult.getResult().isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable((ProductModel) searchResult.getResult().get(0));
    }

    public OrderModel findInitialOrder(String orderCode) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
                "SELECT {o:pk} FROM {Order AS o} WHERE  {o.code} = ?code AND {o.initialOrderVersion} = 1");
        flexibleSearchQuery.addQueryParameter("code", orderCode);
        final OrderModel order = getFlexibleSearchService().searchUnique(flexibleSearchQuery);
        if (order == null) {
            throw new IllegalArgumentException(String.format(
                    "Error while processing inbound IDoc with order code [%s] that does not exist!", orderCode));
        }
        return order;
    }


    public void setOrderStatusAsConfirmed(OrderModel order) {
        if (Objects.nonNull(order)) {
            order.setStatus(OrderStatus.CONFIRMED);
            getModelService().save(order);
        }
    }

    public TitleModel getTitleByName(String name) {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
            "SELECT {pk} FROM {Title} WHERE  {name[de]} like ?name");
        flexibleSearchQuery.addQueryParameter("name", name);
        final TitleModel titleModel = getFlexibleSearchService().searchUnique(flexibleSearchQuery);
        if(titleModel == null) {
            throw new IllegalArgumentException(String.format(
                "Error while processing inbound IDoc for address with title name [%s] that does not exist!", name));
        }
        return titleModel;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    protected OrderHistoryService getOrderHistoryService() {
        return orderHistoryService;
    }

    @Required
    public void setOrderHistoryService(OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

}

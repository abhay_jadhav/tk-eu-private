/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.thyssenkrupp.b2b.global.baseshop.core.model.TkSapProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.security.JaloSecurityException;

public class TkOrderEntryProductAssignmentTranslator extends AbstractSpecialValueTranslator {
    private static final String DH_HELPER_BEAN_NAME     = "tKDataHubInboundOrderHelper";

    private TKDataHubInboundOrderHelper inboundOrderHelper;

    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        if (inboundOrderHelper == null) {
            inboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean(DH_HELPER_BEAN_NAME);
        }
    }

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        //productId#productName
        final String[] components = StringUtils.split(cellValue, "#");

        if(components.length != 2){
            throw new ImpExException(String.format("Passed value \'%s\' has too many/less parts. Should only contain 2 parts separated by \'#\'", cellValue));
        }

        String productCode = components[0];
        String productName = components[1];

        String orderCode = getOrderCode(processedItem);
        String entryNumber = getEntryNumber(processedItem);

        OrderEntryModel orderEntry = inboundOrderHelper.findOrderEntry(orderCode, entryNumber);
        Optional<ProductModel> product = inboundOrderHelper.findProduct(productCode);

        if (product.isPresent()) {
            orderEntry.setProduct(product.get());
            inboundOrderHelper.getModelService().save(orderEntry);
        } else {
            //if dummy product is used
            createProductInfo(orderEntry, productCode, productName);
        }
    }

    private void createProductInfo(OrderEntryModel entry, String productCode, String productName) {
        TkSapProductInfoModel productInfo = inboundOrderHelper.getModelService().create(TkSapProductInfoModel.class);
        productInfo.setOrderEntry(entry);
        productInfo.setProductCode(productCode);
        productInfo.setProductName(productName);
        productInfo.setProductName(productName, Locale.GERMAN);
        productInfo.setConfiguratorType(ConfiguratorType.SAPPRODUCTINFO);
        inboundOrderHelper.getModelService().save(productInfo);
    }

    private String getOrderCode(Item processedItem) throws ImpExException {
        String orderCode;
        try {
            Order order = (Order) processedItem.getAttribute("order");
            orderCode = order.getCode();
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        } catch (ClassCastException e) {
            throw new ImpExException("Cannot cast to Order jalo to retrieve order code.");
        }
        return orderCode;
    }

    private String getEntryNumber(Item processedItem) throws ImpExException {
        String entryNumber;
        try {
            entryNumber = processedItem.getAttribute("entryNumber").toString();
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        }
        return entryNumber;
    }
}

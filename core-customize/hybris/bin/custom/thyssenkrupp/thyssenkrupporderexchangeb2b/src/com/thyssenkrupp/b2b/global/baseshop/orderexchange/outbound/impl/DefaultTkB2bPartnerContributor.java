package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.PartnerCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.PartnerRoles;
import de.hybris.platform.sap.orderexchangeb2b.outbound.impl.DefaultB2BPartnerContributor;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Strings;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

public class DefaultTkB2bPartnerContributor extends DefaultB2BPartnerContributor {

    private static final String      AS_MANAGER_AGENT_GROUP_UID = "asagentsalesmanagergroup";
    private              String      partnerRoleYcCode;
    private              String      partnerRoleYcAddressNumber;
    private              String      partnerRoleYcString="YC";
    private              boolean     fetchFromRootUnit;
    private              UserService userService;

    @Override
    protected List<Map<String, Object>> createB2BRows(OrderModel order) {
        final List<Map<String, Object>> b2BRows = super.createB2BRows(order);
        createPartnerRowForAsmOrder(order).ifPresent(b2BRows::add);
        final Optional<AddressModel> orderConfirmationRecipientAddress = getOrderConfirmationRecipientAddress(getB2BUnit(order));
        if (orderConfirmationRecipientAddress.isPresent()) {
            final AddressModel addressModel = orderConfirmationRecipientAddress.get();
            final Map<String, Object> addressRow = createOrderConfirmationRecipientAddressRow(order, addressModel);
            b2BRows.add(addressRow);
        }
        return b2BRows;
    }
    @Override
    protected Map<String, Object> createAddressRow(final OrderModel order, final PartnerRoles partnerRole,
            final String addressNumber){
        final AddressModel address = addressForPartnerRole(order, partnerRole);
        Map<String, Object> row = null;
        if (address != null){
            row = new HashMap<>();
            row.put(OrderCsvColumns.ORDER_ID, order.getCode());
            row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, partnerRole.getCode());
            final String sapCustomer = address.getSapCustomerID();
            if (sapCustomer == null || sapCustomer.isEmpty() || StringUtils.equalsIgnoreCase(sapCustomer,"null")){
                row.put(PartnerCsvColumns.PARTNER_CODE, soldToFromOrder(order));
                row.put(PartnerCsvColumns.DOCUMENT_ADDRESS_ID, addressNumber);
                mapAddressData(order, address, row);
            }else{
                row.put(PartnerCsvColumns.PARTNER_CODE, sapCustomer);
                row.put(PartnerCsvColumns.DOCUMENT_ADDRESS_ID, "");
            }
        }
        return row;
    }
    
    private Optional<Map<String, Object>> createPartnerRowForAsmOrder(final OrderModel order) {
        final Map<String, Object> row = new HashMap<>(3);
        getPlacedByEmployee(order).filter(o -> isNotEmpty(o.getSapId())).ifPresent(employee -> getAsmAgentManagerGroup(employee).ifPresent(userGroup -> {
            row.put(OrderCsvColumns.ORDER_ID, order.getCode());
            row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, userGroup.getSapPartnerType());
            row.put(PartnerCsvColumns.PARTNER_CODE, employee.getSapId());
        }));
        return MapUtils.isNotEmpty(row) ? Optional.of(row) : Optional.empty();
    }

    private Optional<UserGroupModel> getAsmAgentManagerGroup(final EmployeeModel employeeModel) {
        final UserGroupModel asManagerGroup = getUserService().getUserGroupForUID(AS_MANAGER_AGENT_GROUP_UID);
        if (asManagerGroup != null && getUserService().isMemberOfGroup(employeeModel, asManagerGroup)) {
            return Optional.of(asManagerGroup);
        }
        return Optional.empty();
    }

    private Optional<EmployeeModel> getPlacedByEmployee(final OrderModel order) {
        final UserModel userModel = order.getPlacedBy();
        if (userModel instanceof EmployeeModel) {
            return Optional.of(EmployeeModel.class.cast(userModel));
        }
        return Optional.empty();
    }

    private Map<String, Object> createOrderConfirmationRecipientAddressRow(final OrderModel order, final AddressModel address) {
        Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, getPartnerRoleYcCode());
        String sapCustomer = address.getSapCustomerID();
        if (StringUtils.isEmpty(sapCustomer)) {
            sapCustomer = soldToFromOrder(order);
        }
        row.put(PartnerCsvColumns.PARTNER_CODE, sapCustomer);
        row.put(PartnerCsvColumns.DOCUMENT_ADDRESS_ID, getPartnerRoleYcAddressNumber());
        mapAddressData(order, address, row);

        Optional<String> customerEmail = getEmailOfCustomer(order);
        customerEmail.ifPresent(email -> row.replace(PartnerCsvColumns.EMAIL, email));
        return row;
    }

    private Optional<String> getEmailOfCustomer(final OrderModel order) {
        if (order.getUser() instanceof B2BCustomerModel) {
            B2BCustomerModel b2bCustomer = (B2BCustomerModel) order.getUser();
            return Optional.of(b2bCustomer.getContactEmail());
        }
        return Optional.empty();
    }

    private B2BUnitModel getB2BUnit(final OrderModel order) {
        if (isFetchFromRootUnit()) {
            return getB2bUnitService().getRootUnit(order.getUnit());
        }
        return order.getUnit();
    }

    private Optional<AddressModel> getOrderConfirmationRecipientAddress(B2BUnitModel b2BUnitModel) {
        final Collection<AddressModel> b2bAddress = b2BUnitModel.getAddresses();
        return emptyIfNull(b2bAddress).stream().filter(AddressModel::getOrderConfirmationRecipient).findFirst();
    }

    @Override
    protected void mapAddressData(final OrderModel order, final AddressModel address, final Map<String, Object> row) {
        super.mapAddressData(order, address, row);
        String languageSapCode = order.getLanguage().getSapCode();
        if (languageSapCode != null && !languageSapCode.isEmpty()) {
            row.replace(PartnerCsvColumns.LANGUAGE_ISO_CODE, languageSapCode);
        }
        if(!Strings.isNullOrEmpty(createCompanyName(order, address))) {
            row.put(PartnerCsvColumns.LAST_NAME, createCompanyName(order, address));
            row.put(PartnerCsvColumns.FIRST_NAME, createName(address, order));
         }else {
             row.put(PartnerCsvColumns.LAST_NAME, createName(address, order));
             row.put(PartnerCsvColumns.FIRST_NAME,StringUtils.EMPTY);
             }
        row.put(PartnerCsvColumns.TITLE, StringUtils.EMPTY);
    }

    private String createName(AddressModel address, OrderModel order) {
        if(partnerRoleYcString.equals(address.getSapAddressUsage()) && !Strings.isNullOrEmpty(order.getUser().getName())){
            return order.getUser().getName();
        }
        if(!Strings.isNullOrEmpty(address.getFirstname()) && !Strings.isNullOrEmpty(address.getLastname())) {
            return address.getFirstname()+" "+address.getLastname();
        } else if(Strings.isNullOrEmpty(address.getLastname())) {
            return address.getFirstname();
        } else if(Strings.isNullOrEmpty(address.getFirstname())) {
            return address.getLastname();
        }
        return StringUtils.EMPTY;
    }

    private String createCompanyName(OrderModel order, AddressModel address) {
        if(!Strings.isNullOrEmpty(address.getCompany())) {
            return address.getCompany();
        }else if(!Strings.isNullOrEmpty(order.getUnit().getLocName())) {
            return order.getUnit().getLocName();
        }
        return StringUtils.EMPTY;
    }

    public boolean isFetchFromRootUnit() {
        return fetchFromRootUnit;
    }

    public void setFetchFromRootUnit(boolean fetchFromRootUnit) {
        this.fetchFromRootUnit = fetchFromRootUnit;
    }

    public String getPartnerRoleYcCode() {
        return partnerRoleYcCode;
    }

    public void setPartnerRoleYcCode(String partnerRoleYcCode) {
        this.partnerRoleYcCode = partnerRoleYcCode;
    }

    public String getPartnerRoleYcAddressNumber() {
        return partnerRoleYcAddressNumber;
    }

    public void setPartnerRoleYcAddressNumber(String partnerRoleYcAddressNumber) {
        this.partnerRoleYcAddressNumber = partnerRoleYcAddressNumber;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class TkOrderRejectOlderSerialisationKeyTranslator extends AbstractValueTranslator {
    private static final Logger LOG             = LoggerFactory.getLogger(TkOrderRejectOlderSerialisationKeyTranslator.class);
    private static final String LOG_WARNING_MSG = "Impex line for orderId \'{}\' and serialisationKey \'{}\' will not be imported!";
    private static final String ERROR_MSG       = "Rejecting ImpEx line cause serialization key of \'%s\' iDOC is lesser (%d < %d)!";
    private static final String AB_STATUS       = "AB";
    private static final String BILLING_ADDRESS_TYPE = "RE";
    private static final String SHIPPING_ADDRESS_TYPE = "WE";

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {
        final String[] components = StringUtils.split(valueExpr, "#");
    
        if(components.length < 4){
            LOG.error("Component length is not matching given length of 4 or more than 4 strings separated by #. ImpEx will be rejected!");
            throw new RuntimeException(String.format("Component length %d doesn't match the required length of 4!", components.length));
        }

        String typeSpecificAttributeVal = components[0];
        final String orderNumber = components[1];
        final String variantType = components[2];
        final String serialisationKey = components[3];
        String addressType="";
        if(components.length == 5) {
         addressType=components[4];
         LOG.info("Component length is 5, addressType attribute is also added");
         }
        
        final Optional<OrderModel> orderResult = tKDataHubInboundOrderHelper.getOrderDetails(orderNumber);

        if (orderResult.isPresent()) {
            OrderModel orderModel = orderResult.get();
            String nonABSerialisationKey = orderModel.getNonABSerialisationKey();
            String abSerialisationKey = orderModel.getABSerialisationKey();
            LOG.info("value of nonABSerialisationKey="+nonABSerialisationKey+"and abSerialisationKey="+abSerialisationKey+"and serialisationKey="+serialisationKey);
            if (StringUtils.isEmpty(nonABSerialisationKey)) {
                nonABSerialisationKey = "0";
            }
            if (StringUtils.isEmpty(abSerialisationKey)) {
                abSerialisationKey = "0";
            }

            Long nonABSerialisationKeyL = Long.parseLong(nonABSerialisationKey);
            Long abSerialisationKeyL = Long.parseLong(abSerialisationKey);
            Long serialisationKeyL = Long.parseLong(serialisationKey);

            validateABKey(variantType,nonABSerialisationKeyL,serialisationKeyL,abSerialisationKeyL,orderNumber,serialisationKey);            
            
            if(!validateSapCustomerId(typeSpecificAttributeVal) && StringUtils.isNotEmpty(addressType)) {
                LOG.debug("Invalid SAPCustomer Id for :"+orderNumber+" AddressType:" +addressType +" SapCustomer Id:"+typeSpecificAttributeVal);
                return "";
            }
            
        
        } else {
            LOG.warn("No order was found for sapOrderId or hybrisOrderCode \'{}\'", orderNumber);
        }
        return typeSpecificAttributeVal;
    }

    private void validateABKey(String variantType, Long nonABSerialisationKeyL,Long serialisationKeyL,Long abSerialisationKeyL ,String orderNumber,String serialisationKey) {
        if (!AB_STATUS.equals(variantType) && serialisationKeyL < nonABSerialisationKeyL) {
            LOG.warn(LOG_WARNING_MSG, orderNumber, serialisationKey);
            throw new RuntimeException(String.format(ERROR_MSG, variantType, serialisationKeyL, nonABSerialisationKeyL));
        }

        if (AB_STATUS.equals(variantType) && serialisationKeyL < abSerialisationKeyL) {
            LOG.warn(LOG_WARNING_MSG, orderNumber, serialisationKey);
            throw new RuntimeException(String.format(ERROR_MSG, variantType, serialisationKeyL, abSerialisationKeyL));
        }
        
    }
    
    private boolean validateSapCustomerId(String sapCustomerId) {
        if(StringUtils.isNotEmpty(sapCustomerId) && !StringUtils.equalsIgnoreCase(sapCustomerId,"null")){
            return true;
        }
        return false;
    }

     @Override
    public String exportValue(Object value) throws JaloInvalidParameterException {
        return value == null ? "" : value.toString();
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.delivery.inbound.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.order.dao.TkConsignmentDao;

public class TkDeliveryRejectOlderSerialisationKeyTranslator extends AbstractValueTranslator {
    private static final Logger LOG = LoggerFactory.getLogger(TkDeliveryRejectOlderSerialisationKeyTranslator.class);
    private static final String LOG_WARNING_MSG = "Impex line for Consignment ID \'{}\' and serialisationKey \'{}\' will not be imported!";
    private static final String ERROR_MSG = "Rejecting ImpEx line cause serialization key of iDOC is lesser (%d < %d)!";

    private TkConsignmentDao consignmentDao = (TkConsignmentDao) Registry.getApplicationContext().getBean("consignmentDao");

    @Override
    public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {
        final String[] components = StringUtils.split(valueExpr, "#");

        if (components.length != 3) {
            LOG.error("Component length is not matching given length of 3 strings separated by #. ImpEx will be rejected!");
            throw new RuntimeException(String.format("Component length %d doesn't match the required length of 3!", components.length));
        }

        final String typeSpecificAttributeVal = components[0];
        final String deliveryId = components[1];
        final String serialisationKey = components[2];

        final ConsignmentModel consignmentModel = consignmentDao.findConsignment(deliveryId);

        if (consignmentModel != null) {
            String currentDeliverySerKey = consignmentModel.getDeliverySerialisationKey();
            String deliverySerialisationKey = currentDeliverySerKey == null ? "0" : currentDeliverySerKey;

            Long deliverySerialisationKeyL = Long.parseLong(deliverySerialisationKey);
            Long serialisationKeyL = Long.parseLong(serialisationKey);

            if (serialisationKeyL < deliverySerialisationKeyL) {
                LOG.warn(LOG_WARNING_MSG, consignmentModel.getCode(), serialisationKey);
                throw new RuntimeException(String.format(ERROR_MSG, serialisationKeyL, deliverySerialisationKeyL));
            }

        } else {
            LOG.info("No Consignment was found for deliveryId %s.", deliveryId);
        }
        return typeSpecificAttributeVal;
    }

    @Override
    public String exportValue(Object value) throws JaloInvalidParameterException {
        return value == null ? "" : value.toString();
    }
}

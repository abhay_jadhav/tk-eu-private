/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.invoice.translator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.servicelayer.model.ModelService;

public class TkInvoiceAddressAssignmentTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG                  = LoggerFactory.getLogger(TkInvoiceAddressAssignmentTranslator.class);
    private static final String BILLING_ADDRESS_TYPE = "RE";
    private static final String SOLD_TO_ADDRESS_TYPE = "AG";
    private static final String PAYER_ADDRESS_TYPE   = "RG";

    private GenericDao<InvoiceModel> invoiceModelGenericDao;
    private ModelService             modelService;

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        final String[] components = StringUtils.split(cellValue, "#");

        if (components.length != 2) {
            throw new ImpExException(String.format(
              "Passed value \'%s\' has too many/less parts. Should only contain 2 parts separated by \'#\'",
              cellValue));
        }
        String invoiceCode = components[0];
        String addressType = components[1];
        final Optional<InvoiceModel> invoiceModel = getInvoiceByCode(invoiceCode);

        if (invoiceModel.isPresent()) {
            fetchAndUpdateAddressModel(processedItem, addressType, invoiceModel.get(), invoiceCode);
        }
    }

    protected void fetchAndUpdateAddressModel(Item processedItem, String addressType, InvoiceModel invoiceModel, String invoiceCode) throws ImpExException {
        AddressModel addressModel = getModelService().get(PK.parse(getAddressPk(processedItem)));
        if (addressModel != null) {
            updateInvoiceWithAddress(addressModel, invoiceModel, addressType);
        } else {
            LOG.error("No invoice found for code \'{}\'!", invoiceCode);
        }
    }

    protected void updateInvoiceWithAddress(AddressModel addressModel, InvoiceModel invoiceModel, String addressType) {
        if (BILLING_ADDRESS_TYPE.equals(addressType)) {
            invoiceModel.setBillToAddress(addressModel);
        } else if (SOLD_TO_ADDRESS_TYPE.equals(addressType)) {
            invoiceModel.setSoldToAddress(addressModel);
        } else if (PAYER_ADDRESS_TYPE.equals(addressType)) {
            invoiceModel.setPayerAddress(addressModel);
        }
        getModelService().save(invoiceModel);
        LOG.debug("Successfully updated address for Invoice");
    }

    protected Optional<InvoiceModel> getInvoiceByCode(String invoiceCode) {
        final Map<String, Object> paramMap = new HashMap();
        paramMap.put(InvoiceModel.CODE, invoiceCode);
        final List<InvoiceModel> invoices = getInvoiceModelGenericDao().find(paramMap);
        if (CollectionUtils.isNotEmpty(invoices)) {
            return Optional.ofNullable(invoices.get(0));
        }
        return Optional.empty();
    }

    private String getAddressPk(Item processedItem) throws ImpExException {
        String addressPk;
        try {
            addressPk = processedItem.getAttribute(AddressModel.PK).toString();
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        }
        return addressPk;
    }

    public ModelService getModelService() {
        if(modelService == null) {
            this.modelService = (ModelService) Registry.getApplicationContext().getBean("modelService");
        }
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;

    }

    public GenericDao<InvoiceModel> getInvoiceModelGenericDao() {
        if(invoiceModelGenericDao == null) {
            this.invoiceModelGenericDao = (GenericDao) Registry.getApplicationContext().getBean("invoiceGenericDao");
        }
        return invoiceModelGenericDao;
    }

    @Required
    public void setInvoiceModelGenericDao(GenericDao<InvoiceModel> invoiceModelGenericDao) {
        this.invoiceModelGenericDao = invoiceModelGenericDao;

    }
}

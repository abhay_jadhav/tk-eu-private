package com.thyssenkrupp.b2b.global.baseshop.orderexchange.setup;


import de.hybris.platform.core.initialization.SystemSetup;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.constants.Thyssenkrupporderexchangeb2bConstants;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.service.Thyssenkrupporderexchangeb2bService;

@SystemSetup(extension = Thyssenkrupporderexchangeb2bConstants.EXTENSIONNAME)
public class Thyssenkrupporderexchangeb2bSystemSetup {
    private final Thyssenkrupporderexchangeb2bService thyssenkrupporderexchangeb2bService;

    public Thyssenkrupporderexchangeb2bSystemSetup(final Thyssenkrupporderexchangeb2bService thyssenkrupporderexchangeb2bService) {
        this.thyssenkrupporderexchangeb2bService = thyssenkrupporderexchangeb2bService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
        //
    }

    public Thyssenkrupporderexchangeb2bService getThyssenkrupporderexchangeb2bService() {
        return thyssenkrupporderexchangeb2bService;
    }

}

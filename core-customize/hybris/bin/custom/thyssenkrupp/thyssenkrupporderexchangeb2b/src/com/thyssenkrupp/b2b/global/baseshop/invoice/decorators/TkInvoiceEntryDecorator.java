package com.thyssenkrupp.b2b.global.baseshop.invoice.decorators;

import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import de.hybris.platform.core.Registry;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.CSVCellDecorator;

public class TkInvoiceEntryDecorator implements CSVCellDecorator {
    private static final Logger LOG = Logger.getLogger(TkInvoiceEntryDecorator.class);

    private FlexibleSearchService flexibleSearchService = (FlexibleSearchService) Registry.getApplicationContext()
            .getBean("flexibleSearchService");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {

        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, ",");
        StringBuilder builder = new StringBuilder();
        String consignmentEntries = "";
        try {
            for (String s : components) {
                final String[] consignmentKey = StringUtils.split(s, ":");
                final String consignmentCode = consignmentKey[0];
                final String batchGroupNumber = consignmentKey[1];
                final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("select {entry." + ConsignmentEntryModel.PK
                        + "} from" + "{ConsignmentEntry AS entry JOIN Consignment as c ON " + "{entry."
                        + ConsignmentEntryModel.CONSIGNMENT + "}={c." + ConsignmentModel.PK + "}} WHERE {c."
                        + ConsignmentModel.CODE + "}=?code AND " + "{entry." + ConsignmentEntryModel.BATCHGROUPNUMBER
                        + "}=?batchgroupnumber");

                fQuery.addQueryParameter("code", consignmentCode);
                fQuery.addQueryParameter("batchgroupnumber", batchGroupNumber);
                final SearchResult<ConsignmentEntryModel> result = getFlexibleSearchService().search(fQuery);
                LOG.debug("size of consignmentEntry list=" + result.getResult().size());
                if (null != result && CollectionUtils.isNotEmpty(result.getResult())) {
                    LOG.debug("size of consignmentEntry list=" + result.getResult().size());
                    result.getResult().forEach(con -> {
                        builder.append(con.getPk());
                        builder.append(",");
                    });
                    consignmentEntries = builder.toString();
                }
            }
            LOG.debug("consignmentEntries= " + consignmentEntries);
            consignmentEntries = consignmentEntries.replaceAll(",$", "");
        } catch (Exception e) {
            LOG.error("Exception while fetching consignment entries from consignmentCode and batchGroupNumber" + e);
        }
        return consignmentEntries;
    }

    /**
     * @return the flexibleSearchService
     */
    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}

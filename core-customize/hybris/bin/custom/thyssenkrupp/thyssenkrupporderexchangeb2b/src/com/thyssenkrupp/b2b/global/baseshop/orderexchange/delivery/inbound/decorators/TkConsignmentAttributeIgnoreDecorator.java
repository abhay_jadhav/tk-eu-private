package com.thyssenkrupp.b2b.global.baseshop.orderexchange.delivery.inbound.decorators;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.order.dao.TkConsignmentDao;
import de.hybris.platform.core.Registry;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.CSVCellDecorator;

public class TkConsignmentAttributeIgnoreDecorator implements CSVCellDecorator {
    
    private static final String IGNORE = "<ignore>";
    private static final Logger LOG = LoggerFactory.getLogger(TkConsignmentAttributeIgnoreDecorator.class);

    private TkConsignmentDao consignmentDao = (TkConsignmentDao) Registry.getApplicationContext().getBean("consignmentDao");
    
    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, "#");
        if (components.length != 2) {
            LOG.info("Component length is not matching given length of 2 strings separated by #.Returning " + impexContent);
            return impexContent;
        }

        final String consignmentId = components[0];
        final String itemValToReturn = components[1];
        ConsignmentModel consignment = consignmentDao.findConsignment(consignmentId);
        if (consignment != null) {

            return IGNORE;

        }

        return itemValToReturn;
    }

}

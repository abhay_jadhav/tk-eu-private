/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.decorators;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.CSVCellDecorator;

public class TkOrderAddressOwnerDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkOrderAddressOwnerDecorator.class);

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry
            .getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        LOG.debug("Entering Order AddressOwner Decorator.");
        final String impexContent = impexLine.get(Integer.valueOf(position));

        final String[] components = StringUtils.split(impexContent, "#");
        final String sapOrderId = components[0];

        String orderPk = null;
        LOG.debug("TkOrderAddressOwnerDecorator sapOrderId=\'{}\'.", sapOrderId);

        final Optional<OrderModel> orderResult = tKDataHubInboundOrderHelper.getOrderDetails(sapOrderId);
        if (orderResult.isPresent()) {
            OrderModel orderModel = orderResult.get();
            orderPk = orderModel.getPk().toString();
        } else {
            LOG.error("No order found for sapOrderId \'{}\'!", sapOrderId);
        }
        LOG.debug("Exiting order AddressOwner Decorator.");
        return orderPk;
    }

}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.decorators;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.util.CSVCellDecorator;

public class TkOrderEntryFromSapOrderIdDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkOrderEntryFromSapOrderIdDecorator.class);
    private static final long ITEM_NUM_STEP = 10L;

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        String orderEntryPk = null;
        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, "#");
        if (components.length != 2) {
            LOG.info("Component length is not matching given length of 2 strings separated by #.Returning " + impexContent);
            return impexContent;
        }
        final String sapOrderId = components[0];
        final String orderEntryPosNo = components[1];
        final String orderEntryNum = getOrderEntryNumber(orderEntryPosNo);
        OrderEntryModel orderEntry = tKDataHubInboundOrderHelper.findOrderEntryFromSapOrderIdAndPosition(sapOrderId, orderEntryNum);
        if (orderEntry != null) {
            orderEntryPk = orderEntry.getPk().toString();
        } else {
            LOG.warn("Failed to find the order entry for order '%s' and position %s.", sapOrderId, orderEntryNum);
        }
        return orderEntryPk;
    }

    private String getOrderEntryNumber(final String orderEntryPosNo) {
        String orderEntry = null;
        orderEntry = Long.toString((Long.parseLong((String) orderEntryPosNo)) - 1);
        LOG.debug("Returning Order entry number \'{}\' from order TkOrderEntryFromSapOrderIdDecorator.", orderEntry);
        return orderEntry;
    }

}

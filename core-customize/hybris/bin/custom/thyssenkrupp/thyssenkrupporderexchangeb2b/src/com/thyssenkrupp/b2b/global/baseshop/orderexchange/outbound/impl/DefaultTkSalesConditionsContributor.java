/*
 * Put your copyright text here
 */
package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.ABSOLUTE;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_CODE;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_COUNTER;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_CURRENCY_ISO_CODE;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_ENTRY_NUMBER;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_PRICE_QUANTITY;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_UNIT_CODE;
import static de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns.CONDITION_VALUE;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.SaporderexchangeConstants;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultSalesConditionsContributor;
import de.hybris.platform.util.DiscountValue;

public class DefaultTkSalesConditionsContributor extends DefaultSalesConditionsContributor {

    private static final String CERTIFICATE_COSTS = "Certificate";
    private static final String PACKAGING_COSTS = "Packaging";
    private static final String ASM_SERVICE_FEE = "AsmServiceFee";
    private static final String HANDLING_COSTS = "Handling";
    private static final String HANDLING_COSTS_ABS = "HandlingAbs";
    private static final String ATZ_COSTS = "Atz";
    private static final String DH_BATCH_ID = "dh_batchId";
    private static final String ITEM_PRICE = "ItemPrice";
    private static final String ZILLIANT = "ZILLIANT";
    private static final String SAWING = "Sawing";
    private static final String SAWING_UNIT_CODE = "PCE";
    private static final String ONE = "1";
    private static final String MINIMUM_ORDER_SURCHARGE = "MinimumOrderSurcharge";
    private static final String CUSTOMER_SPECIFIC_PRICE = "CSP";
    private static final String PAC = "PAC";
    private static final String PROS = "PROS";

    private static final int CONDITION_COUNTER_CUSTOMER_SPECIFIC_PRICE = 4;
    private static final Logger LOG = Logger.getLogger(DefaultTkSalesConditionsContributor.class);
    private static final String PROMOTION_ORDER_DISCOUNT_CODE = "OrderDiscount";

    private int conditionCounterCustomerSpecificPrice = CONDITION_COUNTER_CUSTOMER_SPECIFIC_PRICE;

    @Override
    public List<Map<String, Object>> createRows(final OrderModel order) {
        final List<Map<String, Object>> rows = super.createRows(order);
        final List<AbstractOrderEntryModel> entries = order.getEntries();
        if (syncPricingInactive(entries)) {
            createAtzCostRow(order, rows);
            for (final AbstractOrderEntryModel entry : entries) {
                createCustomerSpecificPriceRow(order, rows, entry);
            }
        }
        return rows;
    }

    @Override
    protected void createProductDiscountRows(final OrderModel order, List<Map<String, Object>> result,
            final AbstractOrderEntryModel entry) {
        final List<DiscountValue> discountList = entry.getDiscountValues();
        int conditionCounter = getConditionCounterStartProductDiscount();
        for (final DiscountValue disVal : emptyIfNull(discountList)) {
            if (!StringUtils.equals(SAWING, disVal.getCode()) && !StringUtils.equals(HANDLING_COSTS, disVal.getCode())
                    && !StringUtils.equals(HANDLING_COSTS_ABS, disVal.getCode())
                    && !disVal.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)) {
                addProductDiscountRow(order, result, entry, disVal, conditionCounter++);
            }
        }
        // addHandlingCost(order, result, entry, conditionCounter++);
        // handling cost at entry level
        addEntryHandlingCost(order, result, entry, conditionCounter++);
        if (getCutToLength(entry).isPresent()) {
            addSawingCost(order, result, entry, conditionCounter++);
        }

        addEntrypackagingCost(result, entry, order, conditionCounter++);

        addOrderEntryPromotionDiscountRow(order, result, conditionCounter, entry);

    }

    private void addEntrypackagingCost(List<Map<String, Object>> result, AbstractOrderEntryModel entry,
            OrderModel order, int conditionCounter) {
        List<DiscountValue> globalDiscountValue = order.getGlobalDiscountValues();
        if (CollectionUtils.isNotEmpty(globalDiscountValue) && entry.getEntryNumber() == 0) {
            updateIfPackagingCost(result, order, globalDiscountValue, entry, conditionCounter);
        }

    }

    private Optional<TkCutToLengthConfiguredProductInfoModel> getCutToLength(AbstractOrderEntryModel entry) {
        return ListUtils.emptyIfNull(entry.getProductInfos()).stream()
                .filter(TkCutToLengthConfiguredProductInfoModel.class::isInstance)
                .map(TkCutToLengthConfiguredProductInfoModel.class::cast).findFirst();
    }

    private void addSawingCost(OrderModel order, List<Map<String, Object>> result, AbstractOrderEntryModel entry,
            int conditionCounter) {
        double sawingCharge = 0;
        Optional<DiscountValue> sawingCosts = findSawingCosts(ListUtils.emptyIfNull(entry.getDiscountValues()));

        if (sawingCosts.isPresent() && entry.getQuantity() != 0) {
            sawingCharge = Math.abs(sawingCosts.get().getValue() / entry.getQuantity());
        }

        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
        row.put(CONDITION_COUNTER, conditionCounter);

        final String typeCode = getConditionTypeCode(order, SAWING);
        row.put(CONDITION_CODE, typeCode);
        row.put(CONDITION_VALUE, Math.abs(sawingCharge));
        row.put(CONDITION_UNIT_CODE, SAWING_UNIT_CODE);
        row.put(CONDITION_PRICE_QUANTITY, ONE);
        row.put(CONDITION_CURRENCY_ISO_CODE, entry.getOrder().getCurrency().getIsocode());

        getBatchIdAttributes().forEach(row::putIfAbsent);
        row.put(DH_BATCH_ID, order.getCode());
        result.add(row);
    }

    private Optional<DiscountValue> findSawingCosts(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> SAWING.equals(discountValue.getCode()))
                .findFirst();
    }

    /*
     * private void addHandlingCost(OrderModel order, List<Map<String, Object>>
     * result, AbstractOrderEntryModel entry, int conditionCounter) {
     * List<AbstractOrderEntryProductInfoModel> entryProductInfoModelList =
     * entry.getProductInfos().stream().filter(info ->
     * info.getConfiguratorType().equals(ConfiguratorType.COSTCONFIGURATION)).
     * collect(Collectors. toList()); for (AbstractOrderEntryProductInfoModel
     * entryProductInfoModel : entryProductInfoModelList) { if
     * (entryProductInfoModel != null && entryProductInfoModel instanceof
     * CostConfigurationProductInfoModel) { CostConfigurationProductInfoModel
     * costConfiguration = (CostConfigurationProductInfoModel)
     * entryProductInfoModel; if (HANDLING.equals(costConfiguration.getLabel())) {
     * final Map<String, Object> row = new HashMap<>();
     * row.put(OrderCsvColumns.ORDER_ID, order.getCode());
     * row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
     * row.put(CONDITION_COUNTER, conditionCounter);
     *
     * final String typeCode = getConditionTypeCode(order, HANDLING_COSTS);
     * row.put(CONDITION_CODE, typeCode); row.put(CONDITION_VALUE,
     * Math.abs(costConfiguration.getValue())); row.put(CONDITION_UNIT_CODE,
     * costConfiguration.getUnit().getCode()); row.put(CONDITION_PRICE_QUANTITY,
     * costConfiguration.getQuantity()); row.put(CONDITION_CURRENCY_ISO_CODE,
     * costConfiguration.getCurrency().getIsocode());
     *
     * getBatchIdAttributes().forEach(row::putIfAbsent); row.put(DH_BATCH_ID,
     * order.getCode()); result.add(row); } } } }
     */

    private void addEntryHandlingCost(OrderModel order, List<Map<String, Object>> result, AbstractOrderEntryModel entry,
            int conditionCounter) {
        List<AbstractOrderEntryProductInfoModel> entryProductInfoModelList = entry.getProductInfos().stream()
                .filter(info -> info.getConfiguratorType().equals(ConfiguratorType.COSTCONFIGURATION))
                .collect(Collectors.toList());
        for (AbstractOrderEntryProductInfoModel entryProductInfoModel : entryProductInfoModelList) {
            if (entryProductInfoModel != null && entryProductInfoModel instanceof CostConfigurationProductInfoModel) {
                CostConfigurationProductInfoModel costConfiguration = (CostConfigurationProductInfoModel) entryProductInfoModel;
                if (HANDLING_COSTS.equals(costConfiguration.getLabel())) {
                    final Map<String, Object> row = new HashMap<>();
                    row.put(OrderCsvColumns.ORDER_ID, order.getCode());
                    row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
                    row.put(CONDITION_COUNTER, conditionCounter);

                    final String typeCode = getConditionTypeCode(order, HANDLING_COSTS);
                    row.put(CONDITION_CODE, typeCode);
                    row.put(CONDITION_VALUE, Math.abs(costConfiguration.getValue()));
                    row.put(CONDITION_UNIT_CODE, costConfiguration.getUnit().getCode());
                    row.put(CONDITION_PRICE_QUANTITY, costConfiguration.getQuantity());
                    row.put(CONDITION_CURRENCY_ISO_CODE, costConfiguration.getCurrency().getIsocode());

                    getBatchIdAttributes().forEach(row::putIfAbsent);
                    row.put(DH_BATCH_ID, order.getCode());
                    result.add(row);
                } else if (HANDLING_COSTS_ABS.equals(costConfiguration.getLabel())) {
                    final Map<String, Object> row = new HashMap<>();
                    row.put(OrderCsvColumns.ORDER_ID, order.getCode());
                    row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
                    row.put(CONDITION_COUNTER, conditionCounter);

                    final String typeCode = getConditionTypeCode(order, HANDLING_COSTS_ABS);
                    row.put(CONDITION_CODE, typeCode);
                    row.put(CONDITION_VALUE, Math.abs(costConfiguration.getValue()));
                    // row.put(CONDITION_UNIT_CODE, costConfiguration.getUnit().getCode());
                    row.put(CONDITION_PRICE_QUANTITY, costConfiguration.getQuantity());
                    row.put(CONDITION_CURRENCY_ISO_CODE, costConfiguration.getCurrency().getIsocode());

                    getBatchIdAttributes().forEach(row::putIfAbsent);
                    row.put(DH_BATCH_ID, order.getCode());
                    result.add(row);
                }
            }
        }
    }

    @Override
    protected void createOrderDiscountRows(final OrderModel order, List<Map<String, Object>> result) {
        List<DiscountValue> discounts = order.getGlobalDiscountValues();
        int conditionCounter = getConditionCounterStartOrderDiscount();
        for (final DiscountValue discountValue : emptyIfNull(discounts)) {
            if (!discountValue.getCode().equals(HANDLING_COSTS) && !discountValue.getCode().equals(PACKAGING_COSTS)
                    && !discountValue.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)) {
                addOrderDiscountRow(order, result, discountValue, conditionCounter++);
            }
        }

        addOrderPromotionDiscountRow(order, result, conditionCounter);

    }

    private void addOrderPromotionDiscountRow(OrderModel order, List<Map<String, Object>> result,
            int conditionCounter) {

        List<DiscountValue> globalDiscountValue = order.getGlobalDiscountValues();
        addAbsoluteOrRelativeDiscount(globalDiscountValue, order, result, conditionCounter, null);
    }

    private void addOrderEntryPromotionDiscountRow(OrderModel order, List<Map<String, Object>> result,
            int conditionCounter, AbstractOrderEntryModel entry) {

        List<DiscountValue> entryLevelDiscountValueList = entry.getDiscountValues();
        addAbsoluteOrRelativeDiscount(entryLevelDiscountValueList, order, result, conditionCounter, entry);

    }

    private void addAbsoluteOrRelativeDiscount(List<DiscountValue> entryLevelDiscountValueList, OrderModel order,
            List<Map<String, Object>> result, int conditionCounter, AbstractOrderEntryModel entry) {
        List<DiscountValue> promotionsAbsoluteDiscounts = entryLevelDiscountValueList.stream()
                .filter(orderDiscountValue -> (orderDiscountValue.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)
                        && orderDiscountValue.isAbsolute()))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(promotionsAbsoluteDiscounts)) {

            addPromotionDiscountRow(order, result, promotionsAbsoluteDiscounts, true, conditionCounter++,
                    entry != null ? entry.getEntryNumber().toString() : SaporderexchangeConstants.HEADER_ENTRY);
        }
        List<DiscountValue> promotionsRelativeDiscounts = entryLevelDiscountValueList.stream()
                .filter(orderDiscountValue -> (orderDiscountValue.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)
                        && !orderDiscountValue.isAbsolute()))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(promotionsRelativeDiscounts)) {
            addPromotionDiscountRow(order, result, promotionsRelativeDiscounts, false, conditionCounter,
                    entry != null ? entry.getEntryNumber().toString() : SaporderexchangeConstants.HEADER_ENTRY);
        }

    }

    private void addPromotionDiscountRow(OrderModel order, List<Map<String, Object>> result,
            List<DiscountValue> promotionsAbsoluteDiscounts, boolean isAbsoluteDiscount, int conditionCounter,
            String entryNumber) {
        Map<String, Object> row = createPromotionDiscountRow(order, result, promotionsAbsoluteDiscounts,
                isAbsoluteDiscount, conditionCounter, entryNumber);
        getBatchIdAttributes().forEach(row::putIfAbsent);
        row.put(DH_BATCH_ID, order.getCode());
        result.add(row);

    }

    private Map<String, Object> createPromotionDiscountRow(OrderModel order, List<Map<String, Object>> result,
            List<DiscountValue> promotionDiscountList, boolean isAbsoluteDiscount, int conditionCounter,
            String entryNumber) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(CONDITION_ENTRY_NUMBER, entryNumber);

        boolean isProductDiscount = SaporderexchangeConstants.HEADER_ENTRY.equalsIgnoreCase(entryNumber) ? false : true;
        double totalOrderDiscountValue = 0.0d;

        for (DiscountValue discount : promotionDiscountList) {
            totalOrderDiscountValue = totalOrderDiscountValue + discount.getValue();
        }
        LOG.debug("totalOrderDiscountValue= " + totalOrderDiscountValue);
        totalOrderDiscountValue = roundPriceForERP(totalOrderDiscountValue);
        LOG.debug("totalOrderDiscountValue after round off = " + totalOrderDiscountValue);
        String orderDiscount = negatePriceForERP(totalOrderDiscountValue);
        LOG.debug("totalOrderDiscountValue after negate as prefix= " + orderDiscount);

        row.put(CONDITION_VALUE, orderDiscount);
        totalOrderDiscountValue = 0.0d;
        row.put(CONDITION_COUNTER, conditionCounter);

        if (isAbsoluteDiscount) {
            row.put(ABSOLUTE, Boolean.TRUE);
            row.put(CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
            LOG.debug("promotion discount order ABSOLUTE discount ");
        } else {
            row.put(ABSOLUTE, Boolean.FALSE);
            LOG.debug("promotion discount order RELATIVE discount ");
        }
        row.put(CONDITION_CODE, getConditionTypeCodeForPromotion(order, isAbsoluteDiscount, isProductDiscount));
        return row;
    }

    private void addOrderDiscountRow(OrderModel order, List<Map<String, Object>> result, DiscountValue disVal,
            int conditionCounter) {
        Map<String, Object> row = createOrderDiscountRow(order, disVal, conditionCounter);
        updateIfAsmServiceFee(order, row, disVal);
        getBatchIdAttributes().forEach(row::putIfAbsent);
        row.put(DH_BATCH_ID, order.getCode());
        result.add(row);
    }

    private Map<String, Object> createOrderDiscountRow(OrderModel order, DiscountValue discountValue,
            int conditionCounter) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(CONDITION_ENTRY_NUMBER, SaporderexchangeConstants.HEADER_ENTRY);

        row.put(CONDITION_VALUE, discountValue.getValue() * -1);
        row.put(CONDITION_COUNTER, conditionCounter);

        if (discountValue.isAbsolute()) {
            row.put(ABSOLUTE, Boolean.TRUE);
            row.put(CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
        } else {
            row.put(ABSOLUTE, Boolean.FALSE);
        }

        if (MINIMUM_ORDER_SURCHARGE.equals(discountValue.getCode())) {
            row.put(CONDITION_CODE, getConditionTypeCode(order, discountValue.getCode()));
        } else {
            row.put(CONDITION_CODE, discountValue.getCode());
        }
        return row;
    }

    private void addProductDiscountRow(OrderModel order, List<Map<String, Object>> result,
            AbstractOrderEntryModel entry, DiscountValue disVal, int conditionCounter) {
        Map<String, Object> row = createProductDiscountRow(order, entry, disVal, conditionCounter);
        updateIfCertificateCost(order, row, disVal);

        // handling cost discount row at entry level
        // updateIfHandlingCost(order, row, disVal);

        getBatchIdAttributes().forEach(row::putIfAbsent);
        row.put(DH_BATCH_ID, order.getCode());
        result.add(row);
    }

    private Map<String, Object> createProductDiscountRow(OrderModel order, AbstractOrderEntryModel entry,
            DiscountValue disVal, int conditionCounter) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
        row.put(CONDITION_COUNTER, conditionCounter);

        if (disVal.isAbsolute()) {
            row.put(ABSOLUTE, Boolean.TRUE);
            row.put(CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
            if (!CERTIFICATE_COSTS.equals(disVal.getCode())) {
                row.put(CONDITION_UNIT_CODE, entry.getUnit().getCode());
                row.put(CONDITION_PRICE_QUANTITY, entry.getQuantity());
            }
            row.put(CONDITION_VALUE, disVal.getValue() * entry.getQuantity() * -1);
        } else {
            row.put(ABSOLUTE, Boolean.FALSE);
            row.put(CONDITION_VALUE, disVal.getValue() * -1);
        }

        if (disVal.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)) {
            row.put(CONDITION_CODE, determinePromotionDiscountCode(order, disVal));
        } else {
            row.put(CONDITION_CODE, disVal.getCode());
        }
        return row;
    }

    private void updateIfCertificateCost(OrderModel order, Map<String, Object> row, DiscountValue disVal) {
        if (CERTIFICATE_COSTS.equals(disVal.getCode())) {
            final String typeCode = getConditionTypeCode(order, CERTIFICATE_COSTS);
            row.put(CONDITION_CODE, typeCode);
            row.put(CONDITION_VALUE, Math.abs(disVal.getValue()));
        }
    }

    private void updateIfAsmServiceFee(OrderModel order, Map<String, Object> row, DiscountValue disVal) {
        if (ASM_SERVICE_FEE.equals(disVal.getCode())) {
            final String typeCode = getConditionTypeCode(order, ASM_SERVICE_FEE);
            row.put(CONDITION_CODE, typeCode);
            row.put(CONDITION_VALUE, Math.abs(disVal.getValue()));
        }
    }

    private void updateIfPackagingCost(List<Map<String, Object>> result, OrderModel order,
            List<DiscountValue> globalDiscountValue, AbstractOrderEntryModel entry, int conditionCounter) {
        Optional<DiscountValue> disValue = globalDiscountValue.stream()
                .filter(discountValue -> PACKAGING_COSTS.equals(discountValue.getCode())).findFirst();
        final String typeCode = getConditionTypeCode(order, PACKAGING_COSTS);
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
        row.put(CONDITION_CODE, typeCode);
        row.put(CONDITION_COUNTER, conditionCounter);
        if (null != order.getCurrency()) {
            row.put(CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
        }
        if (disValue.isPresent()) {
            row.put(CONDITION_VALUE, (Math.abs(disValue.get().getValue())));
        }
        result.add(row);
    }
    //
    // private void updateIfHandlingCost(OrderModel order, Map<String, Object> row,
    // DiscountValue disVal) {
    // if (HANDLING_COSTS.equals(disVal.getCode())) {
    // final String typeCode = getConditionTypeCode(order, HANDLING_COSTS);
    // row.put(CONDITION_CODE, typeCode);
    // row.put(CONDITION_VALUE, Math.abs(disVal.getValue()));
    // }
    // if (HANDLING_COSTS_ABS.equals(disVal.getCode())) {
    // final String typeCode = getConditionTypeCode(order, HANDLING_COSTS_ABS);
    // row.put(CONDITION_CODE, typeCode);
    // row.put(CONDITION_VALUE, Math.abs(disVal.getValue()));
    // }
    // }

    private void createAtzCostRow(final OrderModel order, final List<Map<String, Object>> result) {
        final String typeCode = getConditionTypeCode(order, ATZ_COSTS);
        if (StringUtils.isNotEmpty(typeCode)) {
            final Map<String, Object> row = new HashMap<>();
            row.put(OrderCsvColumns.ORDER_ID, order.getCode());
            row.put(CONDITION_ENTRY_NUMBER, SaporderexchangeConstants.HEADER_ENTRY);
            row.put(CONDITION_CODE, typeCode);
            row.put(CONDITION_VALUE, Math.abs(order.getDeliveryCost()));
            row.put(CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
            row.put(CONDITION_COUNTER, getConditionCounterDeliveryCost());
            row.put(ABSOLUTE, Boolean.TRUE);
            getBatchIdAttributes().forEach(row::putIfAbsent);
            row.put(DH_BATCH_ID, order.getCode());
            result.add(row);
        }
    }

    private String getConditionTypeCode(final OrderModel order, String code) {
        final SAPConfigurationModel sapConfiguration = order.getStore().getSAPConfiguration();

        String result = null;
        if (sapConfiguration != null) {
            if (CERTIFICATE_COSTS.equals(code)) {
                result = sapConfiguration.getSaporderexchange_certificatePriceConditionType();
            } else if (PACKAGING_COSTS.equals(code)) {
                result = sapConfiguration.getSaporderexchange_packagingPriceConditionType();
            } else if (HANDLING_COSTS.equals(code)) {
                result = sapConfiguration.getSaporderexchange_handlingPriceConditionType();
            } else if (HANDLING_COSTS_ABS.equals(code)) {
                result = sapConfiguration.getSaporderexchange_abshandlingPriceConditionType();
            } else if (ATZ_COSTS.equals(code)) {
                result = sapConfiguration.getSaporderexchange_atzPriceConditionType();
            } else if (ITEM_PRICE.equals(code)) {
                result = sapConfiguration.getSaporderexchange_itemPriceConditionType();
            } else if (ASM_SERVICE_FEE.equals(code)) {
                result = sapConfiguration.getSaporderexchange_asmPriceConditionType();
            } else if (SAWING.equals(code)) {
                result = sapConfiguration.getSaporderexchange_cuttolengthPriceConditionType();
            } else if (MINIMUM_ORDER_SURCHARGE.equals(code)) {
                result = sapConfiguration.getSaporderexchange_minimumOrderValuePriceConditionType();
            } else if (CUSTOMER_SPECIFIC_PRICE.equals(code)) {
                result = sapConfiguration.getSaporderexchange_customerSpecificPriceConditionType();
            } else if (PAC.equals(code)) {// fetch Y2MA
                result = sapConfiguration.getSaporderexchange_itemPacPriceConditionType();
            }
        }
        return result;
    }

    private Object getConditionTypeCodeForPromotion(OrderModel order, Boolean isAbsolute, boolean isProductDiscount) {

        final SAPConfigurationModel sapConfiguration = order.getStore().getSAPConfiguration();
        String result = null;
        if (sapConfiguration != null) {
            if (!isProductDiscount && Boolean.TRUE.equals(isAbsolute)) {
                LOG.info("promotion Discount order Absolute and isabsolute= " + isAbsolute);
                result = sapConfiguration.getSaporderexchange_orderPromotionAbsoluteDiscountConditionType();
            } else if (!isProductDiscount && Boolean.FALSE.equals(isAbsolute)) {
                return sapConfiguration.getSaporderexchange_orderPromotionRelativeDiscountConditionType();
            } else if (isProductDiscount && Boolean.TRUE.equals(isAbsolute)) {
                return sapConfiguration.getSaporderexchange_productPromotionAbsoluteDiscountConditionType();
            } else {
                return sapConfiguration.getSaporderexchange_productPromotionRelativeDiscountConditionType();
            }
        }
        return result;
    }

    @Override
    protected void createGrossPriceRow(final OrderModel order, final List<Map<String, Object>> result,
            final AbstractOrderEntryModel entry) {
        final Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(SalesConditionCsvColumns.CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
        row.put(SalesConditionCsvColumns.CONDITION_CODE, super.getGrossPrice());

        row.put(SalesConditionCsvColumns.CONDITION_VALUE, entry.getBasePrice());
        row.put(SalesConditionCsvColumns.CONDITION_UNIT_CODE, entry.getUnit().getCode());
        row.put(SalesConditionCsvColumns.CONDITION_PRICE_QUANTITY, entry.getProduct().getPriceQuantity());

        final String typeCode = getConditionTypeCode(order, ITEM_PRICE);
        final String typeCodePac = getConditionTypeCode(order, PAC);
        if (StringUtils.isNotEmpty(typeCode)) {

            List<AbstractOrderEntryProductInfoModel> entryProductInfoModelList = entry.getProductInfos().stream()
                    .filter(info -> info.getConfiguratorType().equals(ConfiguratorType.COSTCONFIGURATION))
                    .collect(Collectors.toList());

            for (AbstractOrderEntryProductInfoModel entryProductInfoModel : entryProductInfoModelList) {
                if (entryProductInfoModel != null
                        && entryProductInfoModel instanceof CostConfigurationProductInfoModel) {
                    CostConfigurationProductInfoModel costConfiguration = (CostConfigurationProductInfoModel) entryProductInfoModel;
                    if (PAC.equals(costConfiguration.getLabel()) && StringUtils.isNotEmpty(typeCodePac)) {
                        row.put(SalesConditionCsvColumns.CONDITION_VALUE, costConfiguration.getValue());
                        row.put(SalesConditionCsvColumns.CONDITION_UNIT_CODE, costConfiguration.getUnit().getCode());
                        row.put(SalesConditionCsvColumns.CONDITION_PRICE_QUANTITY, costConfiguration.getQuantity());
                        row.put(SalesConditionCsvColumns.CONDITION_CODE, typeCodePac);
                        row.put(CONDITION_CURRENCY_ISO_CODE, costConfiguration.getCurrency().getIsocode());
                    } else if ((ZILLIANT.equals(costConfiguration.getLabel())
                            || PROS.equalsIgnoreCase(costConfiguration.getLabel()))
                            && StringUtils.isNotEmpty(typeCode)) {
                        row.put(SalesConditionCsvColumns.CONDITION_VALUE,
                                roundPriceForERP(costConfiguration.getValue()));
                        row.put(SalesConditionCsvColumns.CONDITION_UNIT_CODE, costConfiguration.getUnit().getCode());
                        row.put(SalesConditionCsvColumns.CONDITION_PRICE_QUANTITY, costConfiguration.getQuantity());
                        row.put(SalesConditionCsvColumns.CONDITION_CODE, typeCode);
                        row.put(CONDITION_CURRENCY_ISO_CODE, costConfiguration.getCurrency().getIsocode());

                    }
                }
            }
        }

        row.put(SalesConditionCsvColumns.CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
        row.put(SalesConditionCsvColumns.ABSOLUTE, Boolean.TRUE);
        row.put(SalesConditionCsvColumns.CONDITION_COUNTER, getConditionCounterGrossPrice());
        getBatchIdAttributes().forEach(row::putIfAbsent);
        row.put("dh_batchId", order.getCode());
        result.add(row);
    }

    private double roundPriceForERP(Double price) {
        if (null != price) {
            BigDecimal roundedPrice = new BigDecimal(price).setScale(2, BigDecimal.ROUND_HALF_UP);
            return roundedPrice.doubleValue();
        }
        return 0.0;
    }

    private String negatePriceForERP(Double price) {
        if (null != price) {
            return price.toString() + "-";
        }
        return "";
    }

    private void createCustomerSpecificPriceRow(final OrderModel order, final List<Map<String, Object>> result,
            final AbstractOrderEntryModel entry) {
        final Map<String, Object> row = new HashMap<>();
        final String typeCode = getConditionTypeCode(order, CUSTOMER_SPECIFIC_PRICE);
        if (StringUtils.isNotEmpty(typeCode)) {

            List<AbstractOrderEntryProductInfoModel> entryProductInfoModelList = entry.getProductInfos().stream()
                    .filter(info -> info.getConfiguratorType().equals(ConfiguratorType.COSTCONFIGURATION))
                    .collect(Collectors.toList());

            for (AbstractOrderEntryProductInfoModel entryProductInfoModel : entryProductInfoModelList) {
                if (entryProductInfoModel != null
                        && entryProductInfoModel instanceof CostConfigurationProductInfoModel) {
                    CostConfigurationProductInfoModel costConfiguration = (CostConfigurationProductInfoModel) entryProductInfoModel;
                    if (CUSTOMER_SPECIFIC_PRICE.equals(costConfiguration.getLabel())) {
                        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
                        row.put(SalesConditionCsvColumns.CONDITION_ENTRY_NUMBER, entry.getEntryNumber());
                        row.put(SalesConditionCsvColumns.ABSOLUTE, Boolean.TRUE);

                        row.put(SalesConditionCsvColumns.CONDITION_VALUE, costConfiguration.getValue());
                        row.put(SalesConditionCsvColumns.CONDITION_UNIT_CODE, costConfiguration.getUnit().getCode());
                        row.put(SalesConditionCsvColumns.CONDITION_PRICE_QUANTITY, costConfiguration.getQuantity());
                        row.put(SalesConditionCsvColumns.CONDITION_CURRENCY_ISO_CODE,
                                costConfiguration.getCurrency().getIsocode());

                        row.put(SalesConditionCsvColumns.CONDITION_CODE, typeCode);
                        row.put(SalesConditionCsvColumns.CONDITION_COUNTER, getConditionCounterCustomerSpecificPrice());

                        getBatchIdAttributes().forEach(row::putIfAbsent);
                        row.put("dh_batchId", order.getCode());
                        result.add(row);
                    }
                }
            }
        }
    }

    public int getConditionCounterCustomerSpecificPrice() {
        return conditionCounterCustomerSpecificPrice;
    }

    public void setConditionCounterCustomerSpecificPrice(int conditionCounterCustomerSpecificPrice) {
        this.conditionCounterCustomerSpecificPrice = conditionCounterCustomerSpecificPrice;
    }

}
package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import de.hybris.platform.servicelayer.model.ModelService;
import java.util.Optional;
import org.apache.commons.lang.StringUtils;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.core.PK;

public class TkOrderAddressAssignmentTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(TkOrderAddressAssignmentTranslator.class);
    private static final String DH_HELPER_BEAN_NAME = "tKDataHubInboundOrderHelper";
    private static final String BILLING_ADDR_TYPE = "RE";
    private static final String SHIPPING_ADDR_TYPE = "WE";

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper;
    private ModelService modelService;

    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        if (tKDataHubInboundOrderHelper == null) {
            tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext()
                    .getBean(DH_HELPER_BEAN_NAME);
            modelService = tKDataHubInboundOrderHelper.getModelService();
        }
    }

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        // orderId + "#" + addressType
        final String[] components = StringUtils.split(cellValue, "#");

        if (components.length != 2) {
            throw new ImpExException(String.format(
                    "Passed value \'%s\' has too many/less parts. Should only contain 2 parts separated by \'#\'",
                    cellValue));
        }
        String sapOrderId = components[0];
        String addressType = components[1];
        final Optional<OrderModel> orderResult = tKDataHubInboundOrderHelper.getOrderDetails(sapOrderId);
        AddressModel addressModel = (AddressModel) modelService.get(PK.parse(getAddressPk(processedItem)));

        if (orderResult.isPresent() && addressModel != null) {
            updateOrder(addressModel, orderResult.get(), addressType);
        } else {
            LOG.error("No order found for sapOrderId \'{}\'!");
        }

    }

    private void updateOrder(AddressModel addressModel, OrderModel orderModel, String addressType) {
        if (BILLING_ADDR_TYPE.equals(addressType)) {
            orderModel.setPaymentAddress(addressModel);
        }
        if (SHIPPING_ADDR_TYPE.equals(addressType)) {
            orderModel.setDeliveryAddress(addressModel);
        }
        modelService.save(orderModel);
        LOG.debug("Successfully updated address for Order");
    }

    private String getAddressPk(Item processedItem) throws ImpExException {
        String addressPk;
        try {
            addressPk = processedItem.getAttribute("pk").toString();
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        }
        return addressPk;
    }
}

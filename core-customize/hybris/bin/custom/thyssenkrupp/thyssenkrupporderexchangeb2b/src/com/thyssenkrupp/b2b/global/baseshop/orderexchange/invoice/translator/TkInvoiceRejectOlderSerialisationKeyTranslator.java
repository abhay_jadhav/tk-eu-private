/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.invoice.translator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

public class TkInvoiceRejectOlderSerialisationKeyTranslator extends AbstractValueTranslator {
    private static final Logger LOG = LoggerFactory.getLogger(TkInvoiceRejectOlderSerialisationKeyTranslator.class);

    private GenericDao<InvoiceModel> invoiceModelGenericDao;

    @Override
    public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {
        final String[] components = StringUtils.split(valueExpr, "#");

        if (components.length != 3) {
            throw new RuntimeException(String.format("Component length %d doesn't match the required length of 3! ImpEx will be rejected!", components.length));
        }

        final String typeSpecificAttributeVal = components[0];
        final String invoiceCode = components[1];
        final String serialisationKey = components[2];

        final Optional<InvoiceModel> invoiceModel = getInvoiceByCode(invoiceCode);

        if (invoiceModel.isPresent()) {
            compareSequenceId(serialisationKey, invoiceModel.get());
        } else {
            LOG.info("Invoice was not found for code \'{}\'.", invoiceCode);
        }
        return typeSpecificAttributeVal;
    }

    private void compareSequenceId(String serialisationKey, @NotNull InvoiceModel invoice) {
        String sequenceId = StringUtils.defaultIfEmpty(invoice.getSequenceId(), "0");

        long sequenceIdL = Long.parseLong(sequenceId);
        long serialisationKeyL = Long.parseLong(serialisationKey);

        if (serialisationKeyL < sequenceIdL) {
            LOG.warn("Impex line for Invoice code \'{}\' and sequence ID \'{}\' will not be imported!", invoice.getCode(), serialisationKey);
            throw new RuntimeException(String.format("Impex line was rejected due to smaller serialization key in iDOC (%d < %d)!", serialisationKeyL, sequenceIdL));
        }
    }

    @Override
    public String exportValue(Object value) throws JaloInvalidParameterException {
        return value == null ? "" : value.toString();
    }

    protected Optional<InvoiceModel> getInvoiceByCode(String invoiceCode) {
        final Map<String, Object> paramMap = new HashMap();
        paramMap.put(InvoiceModel.CODE, invoiceCode);
        final List<InvoiceModel> invoices = getInvoiceModelGenericDao().find(paramMap);
        if (CollectionUtils.isNotEmpty(invoices)) {
            return Optional.ofNullable(invoices.get(0));
        }
        return Optional.empty();
    }

    public GenericDao<InvoiceModel> getInvoiceModelGenericDao() {
        if (invoiceModelGenericDao == null) {
            this.invoiceModelGenericDao = (GenericDao) Registry.getApplicationContext().getBean("invoiceGenericDao");
        }
        return invoiceModelGenericDao;
    }

    @Required
    public void setInvoiceModelGenericDao(GenericDao<InvoiceModel> invoiceModelGenericDao) {
        this.invoiceModelGenericDao = invoiceModelGenericDao;

    }
}

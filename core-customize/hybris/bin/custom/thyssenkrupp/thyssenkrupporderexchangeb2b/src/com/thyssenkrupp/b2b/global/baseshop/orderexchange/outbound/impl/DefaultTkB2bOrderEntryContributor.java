/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderEntryContributor;

public class DefaultTkB2bOrderEntryContributor extends DefaultOrderEntryContributor {

    public static final String ENTRY_NUMBER = "entryNumber";
    
    public static final String CERT_CODE = "ZGNIS";
    public static final String CHARC_CODE = "prodConfigurationCode";
    public static final String CHARC_VAL = "prodConfigurationValue";
    private static final String SHIPPING_NOTE = "orderEntryShippingNote";

    @Override
    public Set<String> getColumns() {
        final Set<String> columns = super.getColumns();
        columns.add(SHIPPING_NOTE);
        
        columns.add(CHARC_VAL);
        columns.add(CHARC_CODE);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel model) {
        final List<Map<String, Object>> rows = super.createRows(model);
        return enhanceRowsByB2BFields(model, rows);
    }

    protected List<Map<String, Object>> enhanceRowsByB2BFields(final OrderModel model, final List<Map<String, Object>> rows) {
        final List<AbstractOrderEntryModel> entries = model.getEntries();
        for (final AbstractOrderEntryModel entry : entries) {
            for (Map<String, Object> row : rows) {
                Integer entryNum = (Integer) row.get(ENTRY_NUMBER);
                if (entryNum.equals(entry.getEntryNumber())) {
                    row.put(SHIPPING_NOTE, entry.getOrderEntryShippingNote());
                    
                    String certificate = getCertificate(entry.getProductInfos());
                    row.put(CHARC_CODE, CERT_CODE);
                    row.put(CHARC_VAL, certificate);      
                }
            }
        }
        return rows;
    }
    
    
    private String getCertificate(List<AbstractOrderEntryProductInfoModel> productInfoList) {
        String certificate = "";
        for (final AbstractOrderEntryProductInfoModel entry : productInfoList) {
            ConfiguratorType configuratorType = entry.getConfiguratorType();
            if (ConfiguratorType.CERTIFICATE.getCode().equals(configuratorType.getCode())) {
                CertificateConfiguredProductInfoModel certificateModel = (CertificateConfiguredProductInfoModel) entry;
                if (Boolean.TRUE.equals(certificateModel.isChecked())) {
                    certificate = certificateModel.getCertificateId();
                }
            }
        }
        return certificate;
    }
    
    
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.decorators;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.CSVCellDecorator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

public class TkOrderSapOrderIdDecorator implements CSVCellDecorator {

    private static final Logger LOG       = LoggerFactory.getLogger(TkOrderSapOrderIdDecorator.class);
    private static final String AB_STATUS = "AB";

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        LOG.debug("Entering order Decorator");
        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, "#");
        String returnSapOrderId = null;

        if (components.length != 4) {
            return null;
        }
        final String orderNumber = components[0];
        final String sapOrderId = components[1];
        final String variantType = components[2];
        final String serialisationKey = components[3];
        if (StringUtils.isNotEmpty(sapOrderId)) {
            returnSapOrderId = sapOrderId;
        }
        LOG.debug("OrderDecorator orderNumber={}, sapOrderId={}, variantType={}, serialisationKey={}.", orderNumber, sapOrderId, variantType, serialisationKey);
        final Optional<OrderModel> orderResult = tKDataHubInboundOrderHelper.getOrderDetails(orderNumber);

        if (orderResult.isPresent()) {
            OrderModel orderModel = orderResult.get();
            String nonABSerialisationKey = orderModel.getNonABSerialisationKey();
            String abSerialisationKey = orderModel.getABSerialisationKey();
            String hybrisSapOrderId = orderModel.getSapOrderId();
            returnSapOrderId = hybrisSapOrderId;
            LOG.debug("OrderDecorator nonABSerialisationKey={}, abSerialisationKey={}, hybrisSapOrderId={}.", nonABSerialisationKey, abSerialisationKey, hybrisSapOrderId);
          
            if(StringUtils.isEmpty(nonABSerialisationKey)){
                nonABSerialisationKey = "0";
            }
            if(StringUtils.isEmpty(abSerialisationKey)){
                abSerialisationKey = "0";
            }
            
            Long nonABSerialisationKeyL = Long.parseLong(nonABSerialisationKey);
            Long abSerialisationKeyL = Long.parseLong(abSerialisationKey);
            Long serialisationKeyL = Long.parseLong(serialisationKey);
         
            
            if (serialisationKeyL > nonABSerialisationKeyL) {
                returnSapOrderId = sapOrderId;
            }
            if (AB_STATUS.equals(variantType) && serialisationKeyL > abSerialisationKeyL) {
                final OrderModel snapshot = tKDataHubInboundOrderHelper.createOrderHistory(orderNumber);
                tKDataHubInboundOrderHelper.setOrderStatusAsConfirmed(snapshot);
            }
        }
        LOG.debug("Exiting order Decorator for order={} and returnSapOrderId={}.", orderNumber, returnSapOrderId);
        return returnSapOrderId;
    }
}

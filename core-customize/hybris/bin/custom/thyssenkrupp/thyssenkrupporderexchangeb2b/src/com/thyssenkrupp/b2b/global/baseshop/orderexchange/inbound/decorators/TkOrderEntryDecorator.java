package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.decorators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.CSVCellDecorator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TkOrderEntryDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkOrderEntryDecorator.class);
    private static final String AB_STATUS = "AB";
    private static final String VERSION = ":<null>";
    private static final String DATE_FORMAT = "yyyyMMdd";

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        LOG.debug("Entering order Entry Decorator.");
        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, "#");
        if (components.length != 4) {
            return null;
        }
        final String sapOrderId = components[0];
        final String variantType = components[1];
        final String serialisationKey = components[2];
        final String deliveryDate = components[3];
        String orderNumber = null;

        LOG.debug("OrderEntryDecorator sapOrderId=\'{}\' variantType=\'{}\' serialisationKey\'{}\'.", sapOrderId, variantType, serialisationKey);

        final Optional<OrderModel> orderResult = tKDataHubInboundOrderHelper.getOrderDetails(sapOrderId);

        if (orderResult.isPresent()) {
            orderNumber = getOrderNumberForOrderEntry(variantType, serialisationKey, orderResult.get());
            updateOrderDeliveryDate(deliveryDate, orderResult.get());

        } else {
            LOG.error("No order found for sapOrderId \'{}\'!");
        }
        LOG.debug("Exiting order Entry Decorator.");
        return orderNumber + VERSION;
    }

    private void updateOrderDeliveryDate(String deliveryDate, OrderModel orderModel) {
        Date currentDeliveryDate = orderModel.getDeliveryDate();
        final SimpleDateFormat srcFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        Date orderEntryDeliveryDate = null;
        try {
            orderEntryDeliveryDate = srcFormatter.parse(deliveryDate);
            LOG.debug("order entry delivery date is " + orderEntryDeliveryDate);
        } catch (ParseException e) {
            LOG.error("Could not convert deliverydate" + deliveryDate + " to date format of yyyyMMdd");
        }
        currentDeliveryDate = currentDeliveryDate == null ? orderEntryDeliveryDate : currentDeliveryDate;
        if (currentDeliveryDate != null && orderEntryDeliveryDate != null && currentDeliveryDate.compareTo(orderEntryDeliveryDate) <= 0) {
            orderModel.setDeliveryDate(orderEntryDeliveryDate);
            ModelService modelService = tKDataHubInboundOrderHelper.getModelService();
            modelService.save(orderModel);
            LOG.debug("Successfully set order  delivery date as " + orderEntryDeliveryDate);
        }

    }

    private String getOrderNumberForOrderEntry(final String variantType, final String serialisationKey, OrderModel orderModel) {
        String orderNumber;
        String abSerialisationKey = orderModel.getABSerialisationKey();
        orderNumber = orderModel.getCode();

        if (StringUtils.isEmpty(abSerialisationKey)) {
            abSerialisationKey = "0";
        }

        if (AB_STATUS.equals(variantType) && Long.compare(Long.parseLong(serialisationKey), Long.parseLong(abSerialisationKey)) == 0) {
            LOG.debug("Returning Hybris orderNumber \'{}\' from orderEntry Decorator.", orderNumber);
        }
        return orderNumber;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.servicelayer.model.ModelService;

public class TkDataHubOrderCancelTranslator extends AbstractSpecialValueTranslator {//DataHubTranslator<DataHubInboundOrderHelper> {

    public static final String HELPER_BEAN = "tKDataHubInboundOrderHelper";
    
    private TKDataHubInboundOrderHelper inboundOrderHelper;

    private ModelService modelService;

    public TkDataHubOrderCancelTranslator() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        if (getInboundOrderHelper() == null) {
            setInboundOrderHelper((TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean(HELPER_BEAN));
        }

        if (modelService == null) {
            setModelService((ModelService) Registry.getApplicationContext().getBean("modelService"));
        }
    }

    @Override
    public void performImport(final String orderInformation, final Item processedItem) throws ImpExException {
        final String orderCode = getOrderCode(processedItem);
        cancelConfirmedOrder(orderCode, orderInformation);
        cancelInitialOrder(orderCode, orderInformation);
    }

    private void cancelConfirmedOrder(String orderCode, String orderInformation) throws ImpExException {
        OrderModel confirmedOrderModel = getInboundOrderHelper().findOrder(orderCode);
        if (confirmedOrderModel != null) {
            getInboundOrderHelper().cancelOrder(orderInformation, confirmedOrderModel.getCode());
        }
    }

    private void cancelInitialOrder(String orderCode, String orderInformation) throws ImpExException {
        OrderModel initialOrderModel = getInboundOrderHelper().findInitialOrder(orderCode);
        initialOrderModel.setStatus(OrderStatus.CANCELLED);
        modelService.save(initialOrderModel);
    }

    private String getOrderCode(final Item processedItem) throws ImpExException {
        String orderCode;

        try {
            orderCode = processedItem.getAttribute(DataHubInboundConstants.CODE).toString();
        } catch (final JaloSecurityException e) {
            throw new ImpExException(e);
        }
        return orderCode;
    }

    public TKDataHubInboundOrderHelper getInboundOrderHelper() {
        return inboundOrderHelper;
    }

    public void setInboundOrderHelper(TKDataHubInboundOrderHelper inboundOrderHelper) {
        this.inboundOrderHelper = inboundOrderHelper;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}


package com.thyssenkrupp.b2b.global.baseshop.orderexchange.delivery.inbound.translators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.order.dao.TkConsignmentDao;
import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

public class TkConsignmentEntryRemoveTranslator extends AbstractSpecialValueTranslator {
    private static final String DH_HELPER_BEAN_NAME = "tKDataHubInboundOrderHelper";
    private static final Logger LOG = LoggerFactory.getLogger(TkConsignmentEntryRemoveTranslator.class);
    private ModelService modelService;
    private TKDataHubInboundOrderHelper inboundOrderHelper;
    private TkConsignmentDao consignmentDao;

    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        if (inboundOrderHelper == null) {
            inboundOrderHelper = (TKDataHubInboundOrderHelper) Registry.getApplicationContext().getBean(DH_HELPER_BEAN_NAME);
        }
        if (consignmentDao == null) {
            consignmentDao = (TkConsignmentDao) Registry.getApplicationContext().getBean("consignmentDao");
        }
        modelService = inboundOrderHelper.getModelService();
    }

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {

        String consignmentId = cellValue;
        ConsignmentModel consignmentModel = findConsignmentModel(consignmentId);
        if (consignmentModel != null) {
            removeConsignmentEntries(consignmentModel);
            LOG.info("Removed all consignment entries successfully for consignment:" + consignmentId);
            } else {
            LOG.info("No Consignment found for  ID :" + consignmentId);
        }
    }

    private void removeConsignmentEntries(ConsignmentModel consignmentModel) {
        modelService.removeAll(consignmentModel.getConsignmentEntries());

    }

    private ConsignmentModel findConsignmentModel(String consignmentId) {
        return consignmentDao.findConsignment(consignmentId);

    }
}

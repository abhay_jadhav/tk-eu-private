package com.thyssenkrupp.b2b.global.baseshop.orderexchange.outbound.impl;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class DefaultTkB2bProdConfigContributor implements RawItemContributor<OrderModel> {

    public static final String TRADELEN_CODE                  = "LA_INT_ALPHA";
    public static final String LONGPROD_CODE                  = "LA";
    public static final String LONGPROD_VAL                   = "HL";
    public static final String ORDER_ID                       = "orderId";
    public static final String ENTRY_NUMBER                   = "entryNumber";
    public static final String CHARC_CODE                     = "prodConfigurationCode";
    public static final String CHARC_VAL                      = "prodConfigurationValue";
    public static final String PRODUCT_CODE                   = "productCode";
    public static final String CUT_TO_LENGTH_PROCESSING_CODE  = "KZANA";
    public static final String CUT_TO_LENGTH_PROCESSING_VALUE = "FS";
    public static final String TYPE_OF_LENGTH_CODE            = "LA";
    public static final String TYPE_OF_LENGTH_VALUE           = "FL";
    public static final String SAWING_LENGTH_CODE             = "FX_1";
    public static final String NO_OF_PIECES_CODE              = "FX_1_ST";
    public static final String TOLLARENCE_CODE                = "LTOL_ALPHA";
    public static final String NO_OF_CUTS_CODE                = "XX_AZSCH";
    public static final String TKLEN_CODE                     = "XX_LENGTH";

    @Override
    public Set<String> getColumns() {
        final Set<String> columns = new HashSet<>();
        columns.add(CHARC_VAL);
        columns.add(CHARC_CODE);
        columns.add(PRODUCT_CODE);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel model) {
        final List<Map<String, Object>> rows = new ArrayList<>();
        return enhanceRowsByProdConfigFields(model, rows);
    }

    protected List<Map<String, Object>> enhanceRowsByProdConfigFields(final OrderModel model, final List<Map<String, Object>> result) {
        final List<AbstractOrderEntryModel> entries = model.getEntries();
        for (final AbstractOrderEntryModel entry : entries) {
            String tradeLength = getTradeLength(entry.getProductInfos());
            if (!tradeLength.isEmpty()) {
                updateRow(result, entry, TRADELEN_CODE, tradeLength, model);
                updateRow(result, entry, LONGPROD_CODE, LONGPROD_VAL, model);
            }
            Optional<AbstractOrderEntryProductInfoModel> tkLengthConfiguredProductInfo = getTkLength(entry.getProductInfos());
            if (tkLengthConfiguredProductInfo.isPresent()) {
                TkLengthConfiguredProductInfoModel tkLengthConfiguredProductInfoModel =  (TkLengthConfiguredProductInfoModel) tkLengthConfiguredProductInfo.get();
                updateRow(result, entry, TKLEN_CODE, tkLengthConfiguredProductInfoModel.getLengthValue().toString(), model);
            }
            TkCutToLengthConfiguredProductInfoModel cutToLengthConfiguredProductInfoModel = getCutToLength(entry);
            if (cutToLengthConfiguredProductInfoModel != null) {
                updateRow(result, entry, CUT_TO_LENGTH_PROCESSING_CODE, CUT_TO_LENGTH_PROCESSING_VALUE, model);
                updateRow(result, entry, TYPE_OF_LENGTH_CODE, TYPE_OF_LENGTH_VALUE, model);
                updateRow(result, entry, SAWING_LENGTH_CODE, cutToLengthConfiguredProductInfoModel.getValue().toString(), model);
                updateRow(result, entry, NO_OF_PIECES_CODE, entry.getQuantity().toString(), model);
                updateRow(result, entry, TOLLARENCE_CODE, cutToLengthConfiguredProductInfoModel.getSapExportValue(), model);
                updateRow(result, entry, NO_OF_CUTS_CODE, entry.getQuantity().toString(), model);
            }
        }
        return result;
    }

    private Optional<AbstractOrderEntryProductInfoModel> getTkLength(List<AbstractOrderEntryProductInfoModel> productInfos) {
        return productInfos.stream().filter(abstractOrderEntryProductInfoModel -> ConfiguratorType.TKCCLENGTH == abstractOrderEntryProductInfoModel.getConfiguratorType()).findFirst();
    }

    private void updateRow(List<Map<String, Object>> result, AbstractOrderEntryModel entry, String code, String value, OrderModel model) {
        final Map<String, Object> row = new HashMap<>();
        row.put(ORDER_ID, model.getCode());
        row.put(ENTRY_NUMBER, entry.getEntryNumber());
        row.put(CHARC_CODE, code);
        row.put(CHARC_VAL, value);
        row.put(PRODUCT_CODE, entry.getProduct().getCode());
        result.add(row);
    }

    private TkCutToLengthConfiguredProductInfoModel getCutToLength(AbstractOrderEntryModel entry) {

        Optional<AbstractOrderEntryProductInfoModel> productInfoModel = entry.getProductInfos().stream().filter(orderEntryProductInfoModel -> ConfiguratorType.CUTTOLENGTH_PRODINFO == orderEntryProductInfoModel.getConfiguratorType()).findFirst();
        if (productInfoModel.isPresent()) {
            return (TkCutToLengthConfiguredProductInfoModel) productInfoModel.get();
        }
        return null;
    }

    private String getTradeLength(List<AbstractOrderEntryProductInfoModel> productInfoList) {
        String tradeLength = "";
        for (final AbstractOrderEntryProductInfoModel entry : productInfoList) {
            ConfiguratorType configuratorType = entry.getConfiguratorType();
            if (ConfiguratorType.TKTRADELENGTH.getCode().equals(configuratorType.getCode())) {
                TkTradeLengthConfiguredProductInfoModel tradeLengthModel = (TkTradeLengthConfiguredProductInfoModel) entry;
                if (Boolean.TRUE.equals(tradeLengthModel.isChecked())) {
                    Double length = tradeLengthModel.getLengthValue();
                    Double conversion = tradeLengthModel.getUnit().getConversion();
                    Double tkTradeLength = length * conversion;
                    tradeLength = tkTradeLength.toString();
                }
            }
        }
        return tradeLength;
    }
}

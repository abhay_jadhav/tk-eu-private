package com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.translators;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang.math.NumberUtils.toLong;
import static org.apache.commons.lang3.ArrayUtils.getLength;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.split;

public class TkUpdateOrderTranslator extends AbstractSpecialValueTranslator {

    private static final Logger                      LOG                                = Logger.getLogger(TkUpdateOrderTranslator.class);
    private static final String                      DH_HELPER_BEAN_NAME                = "tKDataHubInboundOrderHelper";
    private static final String                      BASE_STORE_SERVICE_BEAN_NAME       = "baseStoreService";
    private static final String                      OFFLINE_ORDER_DISTRIBUTION_CHANNEL = "01";
    private              BaseStoreService            baseStoreService;
    private              TKDataHubInboundOrderHelper inboundOrderHelper;

    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        super.init(columnDescriptor);
        if (getInboundOrderHelper() == null) {
            setInboundOrderHelper(Registry.getApplicationContext().getBean(DH_HELPER_BEAN_NAME, TKDataHubInboundOrderHelper.class));
        }
        if (getBaseStoreService() == null) {
            setBaseStoreService(Registry.getApplicationContext().getBean(BASE_STORE_SERVICE_BEAN_NAME, BaseStoreService.class));
        }
    }

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        if (processedItem != null && !equalsIgnoreCase(DataHubInboundConstants.IGNORE, cellValue)) {
            final String[] components = split(cellValue, "#");
            if (getLength(components) != 3) {
                String errorMessage = String.format("Passed cellValue:%s has too many/less parts. Should contain only 3 parts separated by #", cellValue);
                LOG.error(errorMessage);
                throw new ImpExException(errorMessage);
            }
            final String hybrisOrderId = components[0];
            final String serialisationKey = components[1];
            final String distributionChannel = components[2];
            updateOfflineOrder(hybrisOrderId, distributionChannel, serialisationKey);
        }
    }

    private void updateOfflineOrder(String hybrisOrderId, String distributionChannel, String serialisationKey) throws ImpExException {
        if (StringUtils.equals(OFFLINE_ORDER_DISTRIBUTION_CHANNEL, distributionChannel) && isNotEmpty(hybrisOrderId) && isNotEmpty(serialisationKey)) {
            OrderModel orderModel = getInboundOrderHelper().findOrder(hybrisOrderId);
            if (orderModel != null) {
                if (!isValidSerializationKey(serialisationKey, orderModel.getABSerialisationKey())) {
                    LOG.warn(String.format("Skipping update of offline orderId:%s, Incompatible serialisationKey. NewKey:%s ExistingKey:%s", hybrisOrderId, serialisationKey, orderModel.getABSerialisationKey()));
                    return;
                }
                final String salesOrganization = orderModel.getSalesOrganization();
                final Optional<BaseStoreModel> maybeBaseStore = getBaseStoreForSalesOrganization(salesOrganization);
                if (maybeBaseStore.isPresent()) {
                    updateOrderModel(orderModel, maybeBaseStore.get());
                } else {
                    String errorMessage = String.format("No BaseStore model found for salesOrganization:%s", salesOrganization);
                    LOG.error(errorMessage);
                    throw new ImpExException(errorMessage);
                }
            }
        }
    }

    private void updateOrderModel(OrderModel orderModel, BaseStoreModel baseStoreModel) {
        orderModel.setStore(baseStoreModel);
        ofNullable(baseStoreModel.getDefaultLanguage()).ifPresent(orderModel::setLanguage);
        ofNullable(baseStoreModel.getDefaultCurrency()).ifPresent(orderModel::setCurrency);
        orderModel.setNet(baseStoreModel.isNet());
        emptyIfNull(baseStoreModel.getCmsSites()).stream().findFirst().ifPresent(orderModel::setSite);
        getInboundOrderHelper().getModelService().save(orderModel);
    }

    private boolean isValidSerializationKey(String importSerializationKey, String existingSerializationKey) {
        return toLong(importSerializationKey) >= toLong(existingSerializationKey);
    }

    private Optional<BaseStoreModel> getBaseStoreForSalesOrganization(final String salesOrgValue) {
        if (isNotEmpty(salesOrgValue)) {
            return emptyIfNull(getBaseStoreService().getAllBaseStores()).stream().filter(baseStoreModel -> baseStoreModel.getSAPConfiguration() != null)
                .filter(baseStoreModel -> StringUtils.equals(salesOrgValue, baseStoreModel.getSAPConfiguration().getSapcommon_salesOrganization())).findFirst();
        }
        return Optional.empty();
    }

    private BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    private TKDataHubInboundOrderHelper getInboundOrderHelper() {
        return inboundOrderHelper;
    }

    public void setInboundOrderHelper(TKDataHubInboundOrderHelper inboundOrderHelper) {
        this.inboundOrderHelper = inboundOrderHelper;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.orderexchange.delivery.inbound.decorators;



import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.global.baseshop.orderexchange.inbound.impl.TKDataHubInboundOrderHelper;
import com.thyssenkrupp.b2b.eu.core.order.dao.TkConsignmentDao;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.CSVCellDecorator;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

public class TkConsignmentDeliveryModeAllocator  implements CSVCellDecorator {

    private static final Logger LOG = LoggerFactory.getLogger(TkConsignmentAddressAllocationDecorator.class);

    private static final String IGNORE = "<ignore>";
    
    private TkConsignmentDao consignmentDao = (TkConsignmentDao) Registry.getApplicationContext()
            .getBean("consignmentDao");

    private TKDataHubInboundOrderHelper tKDataHubInboundOrderHelper = (TKDataHubInboundOrderHelper) Registry
            .getApplicationContext().getBean("tKDataHubInboundOrderHelper");

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        String deliveryMode = null;

        final String impexContent = impexLine.get(Integer.valueOf(position));
        final String[] components = StringUtils.split(impexContent, "#");
        if (components.length != 2) {
            LOG.info("Component length is not matching given length of 2 strings separated by #.Returning "
                    + impexContent);
            return impexContent;
        }
        final String consignmentId = components[0];
        final String sapOrderId = components[1];

        ConsignmentModel consignment = consignmentDao.findConsignment(consignmentId);
        if (consignment != null) {
            return IGNORE;
        }

        Optional<OrderModel> order = tKDataHubInboundOrderHelper.getOrderDetails(sapOrderId);
        if (order.isPresent()) {
            deliveryMode = order.get().getDeliveryMode()!= null ? order.get().getDeliveryMode().getCode() : null;
        } else {
            LOG.info("No order could be found for sap order id: " + sapOrderId
                    + " Failed to assign DeliveryMode to the consignment");
        }
        return deliveryMode;
    }

}


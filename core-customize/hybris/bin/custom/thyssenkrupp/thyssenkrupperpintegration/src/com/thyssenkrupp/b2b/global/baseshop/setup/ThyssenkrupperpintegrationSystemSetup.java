
package com.thyssenkrupp.b2b.global.baseshop.setup;

import static com.thyssenkrupp.b2b.global.baseshop.constants.ThyssenkrupperpintegrationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.thyssenkrupp.b2b.global.baseshop.constants.ThyssenkrupperpintegrationConstants;
import com.thyssenkrupp.b2b.global.baseshop.service.ThyssenkrupperpintegrationService;

@SystemSetup(extension = ThyssenkrupperpintegrationConstants.EXTENSIONNAME)
public class ThyssenkrupperpintegrationSystemSetup {
    private final ThyssenkrupperpintegrationService thyssenkrupperpintegrationService;

    public ThyssenkrupperpintegrationSystemSetup(final ThyssenkrupperpintegrationService thyssenkrupperpintegrationService) {
        this.thyssenkrupperpintegrationService = thyssenkrupperpintegrationService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
       
    }

}

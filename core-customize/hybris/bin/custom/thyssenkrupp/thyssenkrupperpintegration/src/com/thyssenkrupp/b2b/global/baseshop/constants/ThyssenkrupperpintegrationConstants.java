
package com.thyssenkrupp.b2b.global.baseshop.constants;

public final class ThyssenkrupperpintegrationConstants extends GeneratedThyssenkrupperpintegrationConstants {
    public static final String EXTENSIONNAME = "thyssenkrupperpintegration";

    private ThyssenkrupperpintegrationConstants() {
     
    }

   
    public static final String PLATFORM_LOGO_CODE = "thyssenkrupperpintegrationPlatformLogo";
}

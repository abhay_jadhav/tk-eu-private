
package com.thyssenkrupp.b2b.global.baseshop.service;

public interface ThyssenkrupperpintegrationService {
    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);
}

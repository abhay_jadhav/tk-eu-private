package com.thyssenkrupp.b2b.global.baseshop.product.inbound;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultTkProductCategoryDeletionService {
    private ModelService modelService;
    private ProductDao productDao;
    private static final Logger LOG = Logger.getLogger(DefaultTkProductCategoryDeletionService.class.getName());

    public void removeProductSuperCategory(final String productID, Item processedItem) {
        CatalogVersion catalogVersion;
        try {
            catalogVersion = (CatalogVersion) processedItem.getAttribute("catalogVersion");
            CatalogVersionModel catalogVersionModel = modelService.get(catalogVersion.getPK());
            final List<ProductModel> productModel = productDao.findProductsByCode(catalogVersionModel, productID);
            final Iterator<ProductModel> productIterator = productModel.iterator();
            while (productIterator.hasNext()) {

                final ProductModel product = productIterator.next();
                removeClassificationCategoryForProduct(product);
            }
        } catch (JaloInvalidParameterException | JaloSecurityException e) {
            LOG.error(String.format("Something went wrong while removing classification class for the product [%s]!", productID) + e.getMessage());
        }

    }

    private void removeClassificationCategoryForProduct(final ProductModel product) {
        final ArrayList<CategoryModel> categoryList = new ArrayList<CategoryModel>(product.getSupercategories());
        final ArrayList<CategoryModel> allClassificationCats = (ArrayList<CategoryModel>) categoryList.stream().filter(category -> category instanceof ClassificationClassModel).collect(Collectors.toList());
        final ArrayList<CategoryModel> distinctClassficationCategories = (ArrayList<CategoryModel>) allClassificationCats.stream().distinct().collect(Collectors.toList());
        categoryList.removeAll(allClassificationCats);
        categoryList.addAll(distinctClassficationCategories);
        product.setSupercategories(categoryList);
        modelService.save(product);

    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setProductDao(final ProductDao productDao) {
        this.productDao = productDao;
    }

    protected ProductDao getProductDao() {
        return productDao;
    }
}

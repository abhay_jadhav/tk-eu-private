package com.thyssenkrupp.b2b.global.baseshop.product.inbound;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;

public class DefaultTkProductCategoryTranslator extends AbstractSpecialValueTranslator  {
   
    final private static String BEAN_NAME = "tkProductCategoryDeletionService";
    private  DefaultTkProductCategoryDeletionService tkProductCategoryDeletionService;
    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        tkProductCategoryDeletionService.removeProductSuperCategory(cellValue,processedItem);
    }

    
    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException {
        if (tkProductCategoryDeletionService == null) {
            tkProductCategoryDeletionService = (DefaultTkProductCategoryDeletionService) Registry.getApplicationContext().getBean(BEAN_NAME);
        }
    }
   

}

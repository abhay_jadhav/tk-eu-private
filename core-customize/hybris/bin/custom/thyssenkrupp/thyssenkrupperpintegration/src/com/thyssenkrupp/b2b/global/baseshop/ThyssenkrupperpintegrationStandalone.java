
package com.thyssenkrupp.b2b.global.baseshop;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.RedeployUtilities;

public class ThyssenkrupperpintegrationStandalone {
    public static void main(final String[] args) {
        new ThyssenkrupperpintegrationStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        RedeployUtilities.shutdown();
    }
}

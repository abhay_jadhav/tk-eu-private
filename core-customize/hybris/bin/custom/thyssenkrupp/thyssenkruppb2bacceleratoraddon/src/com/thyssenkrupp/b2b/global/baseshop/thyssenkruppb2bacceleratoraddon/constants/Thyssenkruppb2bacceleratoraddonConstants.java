package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.constants;

/**
 * Global class for all Thyssenkruppb2bacceleratoraddon constants. You can add global constants for
 * your extension into this class.
 */
public final class Thyssenkruppb2bacceleratoraddonConstants extends GeneratedThyssenkruppb2bacceleratoraddonConstants {
    public static final String EXTENSIONNAME = "thyssenkruppb2bacceleratoraddon";

    private Thyssenkruppb2bacceleratoraddonConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}

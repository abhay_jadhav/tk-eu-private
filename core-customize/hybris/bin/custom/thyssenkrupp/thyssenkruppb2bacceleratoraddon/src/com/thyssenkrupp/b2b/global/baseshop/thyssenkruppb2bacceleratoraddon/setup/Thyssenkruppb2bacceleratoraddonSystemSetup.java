package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.constants.Thyssenkruppb2bacceleratoraddonConstants.EXTENSIONNAME;
import static de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService.IMPORT_CORE_DATA;
import static de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService.IMPORT_SAMPLE_DATA;


@SystemSetup(extension = EXTENSIONNAME)
public class Thyssenkruppb2bacceleratoraddonSystemSetup extends AbstractSystemSetup {

    private static final String TK_SAMPLE = "tkSample";
    private CoreDataImportService coreDataImportService;
    private SampleDataImportService sampleDataImportService;
    private boolean sampleDataImport;
    private boolean coreDataImport;

    @Override
    @SystemSetupParameterMethod(extension = EXTENSIONNAME)
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", isCoreDataImport()));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", isSampleDataImport()));
        return params;
    }


    @SystemSetup(type = Type.PROJECT, process = Process.ALL, extension = EXTENSIONNAME)
    public void createProjectData(final SystemSetupContext context) {

        final List<ImportData> eventData = new ArrayList<>();
        List<String> sites = Collections.singletonList(TK_SAMPLE);
        sites.forEach(s -> {
            final ImportData importData = new ImportData();
            importData.setProductCatalogName(s);
            importData.setContentCatalogNames(Collections.singletonList(s));
            importData.setStoreNames(Collections.singletonList(s));
            eventData.add(importData);
        });
        getCoreDataImportService().execute(this, context, eventData);
        getSampleDataImportService().execute(this, context, eventData);

        final boolean importCoreData = getBooleanSystemSetupParameter(context, IMPORT_CORE_DATA);

        if(importCoreData) {
            importImpexFile(context, "/thyssenkruppb2bacceleratoraddon/import/coredata/common/cms-action.impex", true);
        }

    }


    public void setCoreDataImportService(CoreDataImportService coreDataImportService) {
        this.coreDataImportService = coreDataImportService;
    }

    public void setSampleDataImportService(SampleDataImportService sampleDataImportService) {
        this.sampleDataImportService = sampleDataImportService;
    }

    public CoreDataImportService getCoreDataImportService() {
        return coreDataImportService;
    }

    public SampleDataImportService getSampleDataImportService() {
        return sampleDataImportService;
    }

    public boolean isSampleDataImport() {
        return sampleDataImport;
    }

    public void setSampleDataImport(boolean sampleDataImport) {
        this.sampleDataImport = sampleDataImport;
    }

    public boolean isCoreDataImport() {
        return coreDataImport;
    }

    public void setCoreDataImport(boolean coreDataImport) {
        this.coreDataImport = coreDataImport;
    }
}

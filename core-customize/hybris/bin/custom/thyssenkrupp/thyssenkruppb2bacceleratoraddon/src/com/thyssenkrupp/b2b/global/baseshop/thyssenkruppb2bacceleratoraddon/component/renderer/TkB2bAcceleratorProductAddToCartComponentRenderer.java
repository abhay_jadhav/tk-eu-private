package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.component.renderer;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.constants.Thyssenkruppb2bacceleratoraddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.model.TkProductAddToCartComponentModel;

public class TkB2bAcceleratorProductAddToCartComponentRenderer<C extends TkProductAddToCartComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    @Override
    protected String getAddonUiExtensionName(final C component) {
        return Thyssenkruppb2bacceleratoraddonConstants.EXTENSIONNAME;
    }
}

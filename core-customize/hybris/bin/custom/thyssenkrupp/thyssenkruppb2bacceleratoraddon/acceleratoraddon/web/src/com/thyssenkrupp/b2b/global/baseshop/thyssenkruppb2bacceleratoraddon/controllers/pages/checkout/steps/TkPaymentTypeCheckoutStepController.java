package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.b2bacceleratoraddon.controllers.pages.checkout.steps.PaymentTypeCheckoutStepController;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.data.B2BCostCenterData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.controllers.Thyssenkruppb2bacceleratoraddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.forms.TkPaymentTypeForm;

@RequestMapping(value = "/checkout/multi/payment-type")
public class TkPaymentTypeCheckoutStepController extends PaymentTypeCheckoutStepController {

    private static final String PAYMENT_TYPE = "payment-type";

    @Resource(name = "paymentTypeFormValidator")
    private de.hybris.platform.b2bacceleratoraddon.forms.validation.PaymentTypeFormValidator paymentTypeFormValidator;

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade b2bCheckoutFacade;

    @Override
    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateQuoteCheckoutStep
    @PreValidateCheckoutStep(checkoutStep = PAYMENT_TYPE)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, CommerceCartModificationException {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        model.addAttribute("cartData", cartData);
        model.addAttribute("paymentTypeForm", prepareTkPaymentTypeForm(cartData));
        model.addAttribute("billingAddresses", b2bCheckoutFacade.getB2bUnitBillingAddress());
        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setCheckoutStepLinksForModel(model, getCheckoutStep());

        return Thyssenkruppb2bacceleratoraddonControllerConstants.Views.Pages.MultiStepCheckout.ChoosePaymentTypePage;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RequireHardLogIn
    public String choose(@ModelAttribute final TkPaymentTypeForm paymentTypeForm, final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException, CommerceCartModificationException {
        paymentTypeFormValidator.validate(paymentTypeForm, bindingResult);

        if (bindingResult.hasErrors()) {
            de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.addErrorMessage(model, "checkout.error.paymenttype.formentry.invalid");
            model.addAttribute("paymentTypeForm", paymentTypeForm);
            prepareDataForPage(model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
            setCheckoutStepLinksForModel(model, getCheckoutStep());
            return Thyssenkruppb2bacceleratoraddonControllerConstants.Views.Pages.MultiStepCheckout.ChoosePaymentTypePage;
        }

        updateCheckoutCart(paymentTypeForm);

        checkAndSelectDeliveryAddress(paymentTypeForm);

        setSelectedBillingAddress(paymentTypeForm);

        return getCheckoutStep().nextStep();
    }

    protected void updateCheckoutCart(final TkPaymentTypeForm paymentTypeForm) {
        final CartData cartData = new CartData();
        // set payment type
        final B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();
        paymentTypeData.setCode(paymentTypeForm.getPaymentType());
        cartData.setPaymentType(paymentTypeData);
        // set cost center
        if (CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode())) {
            final B2BCostCenterData costCenter = new B2BCostCenterData();
            costCenter.setCode(paymentTypeForm.getCostCenterId());
            cartData.setCostCenter(costCenter);
        }
        // set purchase order number
        cartData.setPurchaseOrderNumber(paymentTypeForm.getPurchaseOrderNumber());

        cartData.setName(paymentTypeForm.getOrderName());
        cartData.setDescription(paymentTypeForm.getOrderDescription());
        b2bCheckoutFacade.updateCheckoutCart(cartData);
    }

    protected TkPaymentTypeForm prepareTkPaymentTypeForm(final CartData cartData) {
        final TkPaymentTypeForm paymentTypeForm = new TkPaymentTypeForm();
        // set payment type
        if (cartData.getPaymentType() != null && StringUtils.isNotBlank(cartData.getPaymentType().getCode())) {
            paymentTypeForm.setPaymentType(cartData.getPaymentType().getCode());
        } else {
            paymentTypeForm.setPaymentType(CheckoutPaymentType.ACCOUNT.getCode());
        }
        // set cost center
        if (cartData.getCostCenter() != null && StringUtils.isNotBlank(cartData.getCostCenter().getCode())) {
            paymentTypeForm.setCostCenterId(cartData.getCostCenter().getCode());
        } else if (!CollectionUtils.isEmpty(getVisibleActiveCostCenters()) && getVisibleActiveCostCenters().size() == 1) {
            paymentTypeForm.setCostCenterId(getVisibleActiveCostCenters().get(0).getCode());
        }
        // set purchase order number
        paymentTypeForm.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
        //
        paymentTypeForm.setOrderName(cartData.getName());
        paymentTypeForm.setOrderDescription(cartData.getDescription());
        return paymentTypeForm;
    }

    protected void setSelectedBillingAddress(final TkPaymentTypeForm paymentTypeForm) {
        if (CheckoutPaymentType.ACCOUNT.getCode().equals(paymentTypeForm.getPaymentType()) && StringUtils.isNotEmpty(paymentTypeForm.getAddressCode())) {
            b2bCheckoutFacade.setBillingAddress(paymentTypeForm.getAddressCode());
        }
    }
}

ACC.thyssenkruppb2baccelerator = {
    _autoload: [
        "bindResetPasswordCopyPaste",
        "bindResetPasswordBtnEnable",
        ["bindOrderCartFormOperations", $("#AddToCartOrderForm").length != 0]
    ],

     bindOrderCartFormOperations: function () {

        var currentTotalPrice = $('.js-total-price-value').val();

            $('body').on('blur', '.item__quantity [id^=cartEntries]', function(){
                    var quantity = $(this).val();
                    var price = $(this).parent().find('[id^=productPrice]').html();
                    $(this).parent().next().find(".item__total-value").html(price * quantity);

            });
            $('li.item__list--item').each(function(i){
                var quantity =  $(this).find('.item__quantity [id^=cartEntries][id$=quantity]').val();
                var price =  $(this).find('.item__quantity [id^=productPrice]').attr('data-variant-price');
                $(this).find('.item__total .item__total-value').html(price * quantity);
            });
     },

     bindResetPasswordCopyPaste: function() {
        $('#password, #confirm-password').on("cut copy paste",function(e) {
            e.preventDefault();
        });
    },

    bindResetPasswordBtnEnable: function() {

        var passwordId = $('.js-password-rule-check').attr('id');
        var passwordName = $('.js-password-rule-check').attr('name');
        var confirmPasswordId = $('.js-confirm-password-rule-check').attr('id');
        var confirmPasswordName = $('.js-confirm-password-rule-check').attr('name');

        $('.js-password-rule-check').on("focus", function() {
            $(".js-password-rule-check").valid();
            $('div[data-js-password-instruction="'+ passwordId +'"]').show();
        }).on("blur", function() {
            $('div[data-js-password-instruction="'+ passwordId +'"]').hide();
        });

        $('.js-confirm-password-rule-check').on("focus", function() {
            $(".js-confirm-password-rule-check").valid();
            $('div[data-js-password-instruction="'+ confirmPasswordId +'"]').show();
        }).on("blur", function() {
            $('div[data-js-password-instruction="'+ confirmPasswordId +'"]').hide();
        });

        $.validator.addMethod("atLeastOneDigitLowerUpperSpecialLetter", function (value, element) {

            var currentPass = value;
            var minRulePassed = 0;

            if(currentPass.length == 0) {
                ACC.thyssenkruppb2baccelerator.passwordRuleCheckReset();
                return false;
            }
            if(ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((currentPass.search(/(?=.*\d)/) >= 0), $('.has_number')))
                minRulePassed++;

            if(ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((currentPass.search(/(?=.*[A-Z])/) >= 0), $('.has_uppercase')))
                minRulePassed++;

            if(ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((currentPass.search(/(?=.*[a-z])/) >= 0), $('.has_lowercase')))
                minRulePassed++;

            if(ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((currentPass.search(/(?=.*[`~!°§@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/])/) >= 0), $('.has_special_char')))
                minRulePassed++;

            if (ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((currentPass.length >= 8), $('.has_min_length')) && minRulePassed >= 3)
                return true;
            else
                return false;
        }, "");

        $.validator.addMethod("samePassword", function (value, element) {

            var confirmPass = value;

            if(confirmPass.length == 0) {
                ACC.thyssenkruppb2baccelerator.passwordRuleCheckReset();
                return false;
            }

            if(ACC.thyssenkruppb2baccelerator.passwordRuleCheckStatus((confirmPass === $('.js-password-rule-check').val()), $('.has_same_password')))
                return true;
            else
                return false;
        }, "");

        var $updateForm = $("#update-pwd-form, #updatePasswordForm");
        var $submitButton = $updateForm.find("button[type='submit']");
        $submitButton.attr("disabled", true);

        var rules = {};
        rules[passwordName] = {
            atLeastOneDigitLowerUpperSpecialLetter: true
        };

        rules[confirmPasswordName] = {
            samePassword: true
        };

        if($updateForm.attr('id') === 'updatePasswordForm') {
            rules['currentPassword'] = {
                required: true
            };
        }

        $updateForm.validate({
            rules: rules,
            showErrors: function (errorMap, errorList) {


            },
        });

        $(document).on("input", '#currentPassword, #'+ passwordId +', #'+ confirmPasswordId, function () {
            if( $("#update-pwd-form, #updatePasswordForm").valid()) {
                $submitButton.attr("disabled", false);
            }
            else {
                $submitButton.attr("disabled", true);
            }
        });
    },

    passwordRuleCheckStatus: function (isPass, $el) {
        var bool = false;
        if(isPass) {
            $el.removeClass('password-rules__list__item--fail').addClass('password-rules__list__item--pass');
            bool = true;
        } else {
            $el.removeClass('password-rules__list__item--pass').addClass('password-rules__list__item--fail');
            bool = false;
        }

        return bool;
    },

    passwordRuleCheckReset: function () {
        $('.password-rules__list__item').removeClass('password-rules__list__item--fail password-rules__list__item--pass');
    }

};

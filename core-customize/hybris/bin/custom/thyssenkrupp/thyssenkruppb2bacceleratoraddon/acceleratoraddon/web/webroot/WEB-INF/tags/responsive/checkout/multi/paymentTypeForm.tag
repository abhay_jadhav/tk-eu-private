<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<form:form id="selectPaymentTypeForm" commandName="paymentTypeForm" action="${request.contextPath}/checkout/multi/payment-type/add" method="post">
    <div class="step-body-form">
        <div class="radiobuttons_paymentselection">
            <c:forEach items="${paymentTypes}" var="paymentType">
                <form:radiobutton path="paymentType" id="PaymentTypeSelection_${paymentType.code}" value="${paymentType.code}" label="${paymentType.displayName}" />
                <br>
            </c:forEach>
        </div>
        <formElement:formInputBox idKey="OrderName" labelKey="checkout.multi.orderName.label" path="orderName" inputCSS="text" />
        <formElement:formInputBox idKey="PurchaseOrderNumber" labelKey="checkout.multi.purchaseOrderNumber.label" path="purchaseOrderNumber" inputCSS="text" />
        <div id="costCenter" class="visible-xs">
            <formElement:formSelectBox idKey="costCenterSelect" labelKey="checkout.multi.costCenter.label" path="costCenterId" skipBlank="false" skipBlankMessageKey="checkout.multi.costCenter.title.pleaseSelect" itemValue="code" itemLabel="name" items="${costCenters}" mandatory="true" selectCSSClass="form-control"/>
        </div>
        <formElement:formInputBox idKey="selectedBillingAddressId" labelKey="checkout.multi.paymentMethod.addPaymentDetails.billingAddress" path="addressCode" inputCSS="visible-xs" />
        <div>
            <c:choose>
                <c:when test="${fn:length(billingAddresses) gt 1}">
                    <select  data-qa-id="checkout-billing-address-drop-down" id="selectBillingAddress" class="form-control">
                        <c:forEach var="billingAddress" items="${billingAddresses}">
                            <option value="${billingAddress.id}">
                                <order:tkAddressItem address="${billingAddress}"/>
                            </option>
                        </c:forEach>
                    </select>
                </c:when>
                <c:otherwise>
                    <c:forEach var="billingAddress" items="${billingAddresses}">
                        <input type=hidden id="selectBillingAddress" name="selectedBillingAddressId" value="${billingAddress.id}">
                        <div  data-qa-id="checkout-billing-address" class="control-label checkout-billing-address address-one-line">
                            <order:tkAddressItem address="${billingAddress}"/>
                        </div>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <button id="choosePaymentType_continue_button" type="submit" class="btn btn-primary btn-block checkout-next">
        <spring:theme code="checkout.multi.paymentType.continue"/>
    </button>
</form:form>

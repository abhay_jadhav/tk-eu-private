package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.constants;

/**
 * Global class for all Thyssenkruppb2bacceleratoraddon web constants. You can add global constants
 * for your extension into this class.
 */
public final class Thyssenkruppb2bacceleratoraddonWebConstants {// NOSONAR
    private Thyssenkruppb2bacceleratoraddonWebConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}

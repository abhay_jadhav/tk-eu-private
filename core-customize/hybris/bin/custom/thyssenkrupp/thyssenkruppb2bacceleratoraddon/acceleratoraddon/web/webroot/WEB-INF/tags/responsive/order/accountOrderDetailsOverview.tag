<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="row">
    <div class="col-sm-12 col-md-9 col-no-padding">
        <div class="row">
            <div class="col-sm-4 item-wrapper">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderID_label">
                        <span class="item-label" data-qa-id="order-details-order-number"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                        <span class="item-value">${fn:escapeXml(orderData.code)}</span>
                    </ycommerce:testId>
                </div>
                <div class="item-group">
                    <c:if test="${not empty orderData.name}">
                        <ycommerce:testId code="orderDetail_overviewOrderName_label">
                            <span class="item-label"><spring:theme code="text.account.order.orderDetails.OrderName"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.name)}</span>
                        </ycommerce:testId>
                    </c:if>
                </div>
                <div class="item-group">
                    <c:if test="${orderData.paymentType.code=='ACCOUNT' and not empty orderData.purchaseOrderNumber}">
                        <ycommerce:testId code="orderDetail_overviewPurchaseOrderNumber_label">
                            <span class="item-label" data-qa-id="order-details-po-number"><spring:theme code="text.account.order.orderDetails.purchaseOrderNumber"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.purchaseOrderNumber)}</span>
                        </ycommerce:testId>
                    </c:if>
                </div>
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderStatus_label">
                        <span class="item-label" data-qa-id="order-details-order-status"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <c:if test="${not empty orderData.statusDisplay}">
                            <span class="item-value"><spring:theme code="text.account.order.status.display.${fn:escapeXml(orderData.statusDisplay)}"/></span>
                        </c:if>
                    </ycommerce:testId>
                </div>
            </div>
            <div class="col-sm-4 item-wrapper">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewStatusDate_label">
                        <span class="item-label" data-qa-id="order-details-date-placed"><spring:theme code="text.account.orderHistory.dateSubmitted"/></span>
                        <span class="item-value"><fmt:formatDate value="${order.created}" dateStyle="medium" timeStyle="short" type="both"/></span>
                    </ycommerce:testId>
                </div>
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewPlacedBy_label">
                        <span class="item-label" data-qa-id="order-details-placed-by"><spring:theme code="text.account.orderHistory.orderSubmittedBy"/></span>
                        <span class="item-value"><spring:theme code="text.company.user.${fn:escapeXml(order.b2bCustomerData.titleCode)}.name" text=""/>&nbsp;${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</span>
                    </ycommerce:testId>
                </div>
                <c:if test="${not empty orderData.incoTerms}">
                    <div class="item-group">
                        <ycommerce:testId code="orderDetail_incoTerms_label">
                            <span class="item-label"><spring:theme code="text.account.orderHistory.incoTerms"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.incoTerms)}</span>
                        </ycommerce:testId>
                    </div>
                </c:if>
                <div class="item-group">
                    <c:if test="${orderData.paymentType.code=='ACCOUNT'}">
                        <ycommerce:testId code="orderDetail_overviewParentBusinessUnit_label">
                            <span class="item-label" data-qa-id="order-details-cost-center"><spring:theme code="text.account.orderHistory.organizationalUnit"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.costCenter.unit.name)}</span>
                        </ycommerce:testId>
                    </c:if>
                </div>
            </div>
            <div class="col-sm-4 item-wrapper">
                <c:if test="${not empty orderData.deliveryDate}">
                   <div class="item-group">
                        <ycommerce:testId code="orderDetail_deliveryDate_label">
                            <span class="item-label" data-qa-id="order-details-delivery-date"><spring:theme code="text.account.orderHistory.deliveryDate"/></span>
                            <span class="item-value"><fmt:formatDate value="${orderData.deliveryDate}" dateStyle="medium" timeStyle="short" type="both"/></span>
                        </ycommerce:testId>
                    </div>
                </c:if>
                <c:if test="${not empty orderData.paymentMode}">
                   <div class="item-group">
                        <ycommerce:testId code="orderDetail_paymentType_label">
                            <span class="item-label" data-qa-id="order-details-payment-type"><spring:theme code="text.account.orderHistory.paymentType"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.paymentMode.name)}</span>
                        </ycommerce:testId>
                    </div>
                </c:if>
                <c:if test="${not empty orderData.totalWeight}">
                   <div class="item-group">
                        <ycommerce:testId code="orderDetail_totalWeight_label">
                            <span class="item-label" data-qa-id="order-details-total-weight"><spring:theme code="text.account.orderHistory.totalWeight"/></span>
                            <span class="item-value"><fmt:formatNumber value="${orderData.totalWeight}" maxFractionDigits="2" /></span>
                        </ycommerce:testId>
                    </div>
                </c:if>
                <div class="item-group">
                    <c:if test="${orderData.paymentType.code=='ACCOUNT'}">
                        <ycommerce:testId code="orderDetail_overviewCostCenter_label">
                            <span class="item-label"><spring:theme code="text.account.order.orderDetails.CostCenter"/></span>
                            <span class="item-value">${fn:escapeXml(orderData.costCenter.name)}</span>
                        </ycommerce:testId>
                    </c:if>
                </div>
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                        <span class="item-label" data-qa-id="order-details-total-price"><spring:theme code="text.account.orderHistory.totalPrice"/></span>
                        <span class="item-value"><format:price priceData="${order.totalPriceWithTax}"/></span>
                    </ycommerce:testId>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 item-action">
        <c:set var="orderCode" value="${orderData.code}" scope="request"/>
        <action:actions element="div" parentComponent="${component}"/>
    </div>
</div>


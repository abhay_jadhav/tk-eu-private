
package com.thyssenkrupp.b2b.global.baseshop.localization.backoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.thyssenkrupp.b2b.global.baseshop.localization.backoffice.services.ThyssenkrupplocalizationaddonbackofficeService;

public class ThyssenkrupplocalizationaddonbackofficeController extends DefaultWidgetController {
    private static final long  serialVersionUID = 1L;
    private              Label label;

    @WireVariable
    private transient ThyssenkrupplocalizationaddonbackofficeService thyssenkrupplocalizationaddonbackofficeService;

    @Override
    public void initialize(final Component comp) {
        super.initialize(comp);
        label.setValue(thyssenkrupplocalizationaddonbackofficeService.getHello() + " ThyssenkrupplocalizationaddonbackofficeController");
    }
}


package com.thyssenkrupp.b2b.global.baseshop.localization.backoffice.constants;

public final class ThyssenkrupplocalizationaddonbackofficeConstants extends GeneratedThyssenkrupplocalizationaddonbackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkrupplocalizationaddonbackoffice";

    private ThyssenkrupplocalizationaddonbackofficeConstants() {

    }
}

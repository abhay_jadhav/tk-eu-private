package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.constants;

@SuppressWarnings({ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public class ThyssenkruppassistedservicestorefrontConstants extends GeneratedThyssenkruppassistedservicestorefrontConstants {
    public static final String EXTENSIONNAME = "thyssenkruppassistedservicestorefront";

    private ThyssenkruppassistedservicestorefrontConstants() {
        //empty
    }
}

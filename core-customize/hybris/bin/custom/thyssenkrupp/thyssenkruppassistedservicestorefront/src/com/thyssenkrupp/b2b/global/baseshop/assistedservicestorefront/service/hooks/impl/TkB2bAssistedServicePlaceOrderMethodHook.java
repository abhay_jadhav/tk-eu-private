package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.service.hooks.impl;

import de.hybris.platform.assistedservicefacades.hook.AssistedServicePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.order.InvalidCartException;

public class TkB2bAssistedServicePlaceOrderMethodHook extends AssistedServicePlaceOrderMethodHook {

    @Override
    public void afterPlaceOrder(CommerceCheckoutParameter parameter, CommerceOrderResult orderModel) throws InvalidCartException {
        if (getAssistedServiceFacade().isAssistedServiceAgentLoggedIn()) {
            orderModel.getOrder().setSalesApplication(SalesApplication.ASM);
        }
        super.afterPlaceOrder(parameter, orderModel);
    }
}

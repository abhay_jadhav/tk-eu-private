package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.customer360.provider;

import de.hybris.platform.assistedservicefacades.customer360.FragmentModelProvider;
import de.hybris.platform.assistedservicestorefront.customer360.FavoriteColorsData;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

/**
 * Model provider implementation for Customer favorite colors fragment.
 */
public class CustomerFavoriteColorsProvider implements FragmentModelProvider<FavoriteColorsData> {

    private UserService userService;

    @Override
    public FavoriteColorsData getModel(final Map<String, String> parameters) {
        final FavoriteColorsData customerFavoriteColors = new FavoriteColorsData();
        customerFavoriteColors.setName(getUserService().getCurrentUser().getName().split(" ")[0]);
        return customerFavoriteColors;
    }

    protected UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }
}

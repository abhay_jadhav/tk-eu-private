package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.restrictions;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.model.restrictions.AssistedServiceSessionRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import org.springframework.beans.factory.annotation.Required;

/**
 * Evaluates an ASM agent through session.
 * <p/>
 */
public class AssistedServiceSessionRestrictionEvaluator implements
    CMSRestrictionEvaluator<AssistedServiceSessionRestrictionModel> {
    private AssistedServiceFacade defaultAssistedServiceFacade;

    @Override
    public boolean evaluate(final AssistedServiceSessionRestrictionModel amsSessionRestriction, final RestrictionData context) {
        return defaultAssistedServiceFacade.isAssistedServiceAgentLoggedIn();
    }

    @Required
    public void setDefaultAssistedServiceFacade(final AssistedServiceFacade defaultAssistedServiceFacade) {
        this.defaultAssistedServiceFacade = defaultAssistedServiceFacade;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.restrictions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Path restriction implementation for all Assisted Service Agents. Paths from provided list are restricted.
 */
public class AssitedServiceAllAgentsPathRestriction extends AssistedServicePathRestriction {
    @Override
    public boolean evaluate(final HttpServletRequest httpservletrequest, final HttpServletResponse httpservletresponse) {
        if (pathMatches(httpservletrequest) && getAssistedServiceFacade().isAssistedServiceAgentLoggedIn()) {
            sendRedirectToPreviousPage(httpservletrequest, httpservletresponse);
            return false;
        }
        return true;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.restrictions;

import de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Path restriction implementation for Assisted Service Sales Group. Paths from provided list are restricted.
 */
public class AssistedServiceSalesGroupPathRestriction extends AssistedServicePathRestriction {
    @Override
    public boolean evaluate(final HttpServletRequest httpservletrequest, final HttpServletResponse httpservletresponse) {
        if (pathMatches(httpservletrequest) && getAssistedServiceFacade().isAssistedServiceAgentLoggedIn()) {
            final UserGroupModel managerGroup = getUserService().getUserGroupForUID(
                AssistedserviceservicesConstants.AS_MANAGER_AGENT_GROUP_UID);
            final UserModel agent = getAssistedServiceFacade().getAsmSession().getAgent();
            // restrict access in case agent is not in manager group
            if (!getUserService().isMemberOfGroup(agent, managerGroup)) {
                sendRedirectToPreviousPage(httpservletrequest, httpservletresponse);
                return false;
            }
        }
        return true;
    }
}

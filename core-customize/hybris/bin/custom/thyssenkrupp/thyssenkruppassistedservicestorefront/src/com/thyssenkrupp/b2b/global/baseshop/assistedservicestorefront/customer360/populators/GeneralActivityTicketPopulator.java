package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.customer360.populators;

import static de.hybris.platform.assistedservicefacades.constants.AssistedservicefacadesConstants.TICKET_TEXT;

import de.hybris.platform.assistedservicefacades.util.AssistedServiceUtils;
import de.hybris.platform.assistedservicestorefront.customer360.GeneralActivityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.ticket.model.CsTicketModel;

import org.springframework.beans.factory.annotation.Required;

/**
 * CustomerTicketModel -> GeenralActivityTicket populator
 */
public class GeneralActivityTicketPopulator implements Populator<CsTicketModel, GeneralActivityData> {
    private BaseSiteService baseSiteService;

    @Override
    public void populate(final CsTicketModel ticketModel, final GeneralActivityData ticketData) throws ConversionException {
        ticketData.setType(TICKET_TEXT);
        ticketData.setId(ticketModel.getTicketID());
        ticketData.setStatus(ticketModel.getState().getCode());
        ticketData.setCreated(ticketModel.getCreationtime());
        ticketData.setUpdated(ticketModel.getModifiedtime());
        ticketData.setDescription(ticketModel.getHeadline());
        ticketData.setUrl(AssistedServiceUtils.populateTicketUrl(ticketModel, getBaseSiteService().getCurrentBaseSite()));
        ticketData.setCategory(ticketModel.getCategory() == null ? "---" : ticketModel.getCategory().toString());
    }

    protected BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}

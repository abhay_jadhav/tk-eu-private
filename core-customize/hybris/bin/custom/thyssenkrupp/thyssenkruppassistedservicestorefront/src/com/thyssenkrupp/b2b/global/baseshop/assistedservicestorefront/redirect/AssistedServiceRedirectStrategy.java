package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.redirect;

/**
 * Redirect strategy that used in ASM when user starts to be emulated.
 */
public interface AssistedServiceRedirectStrategy {
    /**
     * Returns redirect path after emulation is called.
     */
    String getRedirectPath();

    /**
     * Returns redirect path after emulation is called with exceptions.
     */
    String getErrorRedirectPath();
}

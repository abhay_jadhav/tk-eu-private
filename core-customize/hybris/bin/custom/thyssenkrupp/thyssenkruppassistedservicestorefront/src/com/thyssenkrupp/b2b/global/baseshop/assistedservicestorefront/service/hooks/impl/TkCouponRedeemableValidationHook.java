package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.service.hooks.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.hooks.CartValidationHook;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponservices.service.data.CouponResponse;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Chitransh Mathur
 *
 */

public class TkCouponRedeemableValidationHook implements CartValidationHook{

   private static final String COUPONNOTVALID = "couponNotValid";
    private CouponService couponService;

    public void beforeValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications){
    }

    public void afterValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications){
        ServicesUtil.validateParameterNotNullStandardMessage("cart", parameter.getCart());
        final CartModel cartModel = parameter.getCart();
        if (CollectionUtils.isNotEmpty(cartModel.getAppliedCouponCodes())){
            final Iterator var5 = cartModel.getAppliedCouponCodes().iterator();
            final CommerceCartModification cartModificationData = new CommerceCartModification();
            final List<String> invalidCouponList = new ArrayList<>();
            while (var5.hasNext()){
                final String couponCode = (String) var5.next();
                final CouponResponse response = this.getCouponService().verifyCouponCode(couponCode, cartModel);
                if (BooleanUtils.isFalse(response.getSuccess())){
                    invalidCouponList.add(couponCode);
                    this.getCouponService().releaseCouponCode(couponCode, cartModel);
                }
            }
            if (CollectionUtils.isNotEmpty(invalidCouponList)){
                cartModificationData.setStatusCode(COUPONNOTVALID);
                cartModificationData.setInvalidCouponList(invalidCouponList);
                modifications.add(cartModificationData);
            }
        }

    }

    protected CouponService getCouponService(){
        return this.couponService;
    }

    @Required
    public void setCouponService(final CouponService couponService){
        this.couponService = couponService;
    }


}

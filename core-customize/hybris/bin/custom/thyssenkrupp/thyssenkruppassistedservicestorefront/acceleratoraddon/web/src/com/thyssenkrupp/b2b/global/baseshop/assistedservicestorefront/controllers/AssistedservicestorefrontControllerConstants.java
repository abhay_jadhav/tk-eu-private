package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.controllers;

import com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.model.AsmDevicesUsedComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.model.AsmFavoriteColorsComponentModel;

public interface AssistedservicestorefrontControllerConstants {
    String ADDON_PREFIX = "addon:/thyssenkruppassistedservicestorefront/";

    // implement here controller constants used by this extension

    interface Views {

        interface Fragments {

            interface CustomerListComponent {
                String ASMCustomerListPopup = ADDON_PREFIX + "fragments/asmCustomerListComponent";
                String ASMCustomerListTable = ADDON_PREFIX + "fragments/asmCustomerListTable";
            }
        }
    }

    interface Actions {
        interface Cms {
            String _Prefix = "/view/"; // NOSONAR
            String _Suffix = "Controller"; // NOSONAR

            String AsmDevicesUsedComponent    = _Prefix + AsmDevicesUsedComponentModel._TYPECODE + _Suffix; // NOSONAR
            String AsmFavoriteColorsComponent = _Prefix + AsmFavoriteColorsComponentModel._TYPECODE + _Suffix; // NOSONAR
        }
    }
}

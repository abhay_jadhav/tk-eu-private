package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.security.impl;

import java.io.Serializable;

/**
 * Represents Assisted Service Agent principal.
 */
public class AssistedServiceAgentPrincipal implements Serializable {
    private final String name;

    public AssistedServiceAgentPrincipal(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}

package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.security;

/**
 * Handles asagent roles, to enable changing authorities dynamically.
 */
public interface AssistedServiceAgentAuthoritiesManager {

    /**
     * Add authorities (roles) from a customer to the current agent. It also adds the initial agent authorities to the
     * session, so it can be restored later.
     *
     * @param customerId The id of the user the roles will be merged to the agent.
     */
    void addCustomerAuthoritiesToAgent(String customerId);

    /**
     * Restore agent initial authorities.
     */
    void restoreInitialAuthorities();
}

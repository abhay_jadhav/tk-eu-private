package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.security.impl;

import de.hybris.platform.servicelayer.security.spring.HybrisSessionFixationProtectionStrategy;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;

public class AssistedServiceAgentLogoutStrategy {
    private HybrisSessionFixationProtectionStrategy sessionFixationStrategy;

    public void logout(final HttpServletRequest request) {
        sessionFixationStrategy.onAuthentication(null, request, null);
    }

    protected HybrisSessionFixationProtectionStrategy getSessionFixationStrategy() {
        return sessionFixationStrategy;
    }

    @Required
    public void setSessionFixationStrategy(final HybrisSessionFixationProtectionStrategy sessionFixationStrategy) {
        this.sessionFixationStrategy = sessionFixationStrategy;
    }
}

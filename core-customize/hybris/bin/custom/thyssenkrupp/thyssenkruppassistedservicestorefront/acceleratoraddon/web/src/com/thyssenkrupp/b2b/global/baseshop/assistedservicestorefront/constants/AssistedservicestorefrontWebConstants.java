package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.constants;

/**
 * Global class for all Assistedservicestorefront web constants. You can add global constants for your extension into
 * this class.
 */
public final class AssistedservicestorefrontWebConstants {         // NOSONAR
    private AssistedservicestorefrontWebConstants() {
        //empty to avoid instantiating this constant class
    }
}

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${not empty validationData and empty hasOutOfStockProducts }">
    <c:choose>
        <c:when test="${reorderCart eq 'true'}">
            <div class="alert neutral">
                <spring:theme code="basket.validation.reorder.unavailable" text="The following items are no longer available and have not been added to your shopping cart:" htmlEscape="false"/>
                <ul>
                    <c:forEach items="${validationData}" var="modification">
                   <c:if test="${modification.statusCode ne 'couponNotValid'}">
                    <li>${modification.entry.product.name}</li>
                    </c:if>
                    
                    <c:if test="${modification.statusCode eq 'couponNotValid'}">
                   <c:forEach items="${modification.invalidCouponList}" var="invalidCoupon">
                    <li>${invalidCoupon}</li>
                    </c:forEach>
                    </c:if>
                    
                    </c:forEach>
                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <c:set var="productLinkValidationTextDecoration" value="style=\"text-decoration: underline\""/>
            <c:forEach items="${validationData}" var="modification">
                <div class="alert neutral">
                    <c:url value="${modification.entry.product.url}" var="entryUrl"/>
                    <spring:theme code="basket.validation.${modification.statusCode}"
                                  arguments="${fn:escapeXml(modification.entry.product.name)}###${entryUrl}###${modification.quantity}###
                                    ${modification.quantityAdded}###${productLinkValidationTextDecoration}" argumentSeparator="###" htmlEscape="false"/>
                    <br>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</c:if>

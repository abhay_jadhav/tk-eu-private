<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="product-documents-downloads">
    <c:if test="${not empty product.mediaData}">
        <ul class="list-unstyled media-download">
            <c:forEach items="${product.mediaData}" var="mediaData">
                <li class="item">
                    <a href="${mediaData.downloadUrl}"><img class="thumbnail" src="${mediaData.mediaIconUrl}" data-qa-id="pdp-media-lnk"/> <span>${mediaData.documentTitle}</span></a>
                </li>
            </c:forEach>
        </ul>
    </c:if>
</div>

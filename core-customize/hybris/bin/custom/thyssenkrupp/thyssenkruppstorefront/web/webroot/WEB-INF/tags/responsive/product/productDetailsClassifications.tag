<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="product-classifications">
    <c:if test="${not empty product.classifications}">
        <c:forEach items="${product.classifications}" var="classification">
            <div class="headline">${fn:escapeXml(classification.name)}</div>
                <table class="table">
                    <tbody>
                        <c:forEach items="${classification.features}" var="feature">
                            <tr>
                                <td class="attrib">${fn:escapeXml(feature.name)}</td>
                                <td>
                                    <c:forEach items="${feature.featureValues}" var="value" varStatus="status">
                                        ${fn:escapeXml(value.value)}
                                        <c:choose>
                                            <c:when test="${feature.range}">
                                                ${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
                                            </c:when>
                                            <c:otherwise>
                                                ${fn:escapeXml(feature.featureUnit.symbol)}
                                                ${not status.last ? '<br/>' : ''}
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
        </c:forEach>
    </c:if>
    <table class="table">
        <tbody>
            <tr>
                <td class="attrib">
                    <spring:theme code="text.product.spec.processingOptions"/>
                </td>
                <td>
                       <c:if test="${not empty product.processingOptions}">
                        <c:forEach items="${product.processingOptions}" var="value" varStatus="loop">
                            ${value}
                            <c:if test="${!loop.last}">,&nbsp;</c:if>
                        </c:forEach>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td class="attrib">
                    <spring:theme code="text.product.spec.seoKeywords"/>
                </td>
                <td>
                    <c:if test="${not empty product.seoKeywords}">
                        ${product.seoKeywords}
                    </c:if>
                </td>
            </tr>
            <tr>
                <td class="attrib">
                    <spring:theme code="text.product.spec.serpSnippet"/>
                </td>
                <td>
                    <c:if test="${not empty product.serpSnippet}">
                        ${product.serpSnippet}
                    </c:if>
                </td>
            </tr>
            <tr>
                <td class="attrib">
                    <spring:theme code="text.product.spec.additionalSearchKeywords"/>
                </td>
                <td>
                    <c:if test="${not empty product.additionalSearchKeywords}">
                        ${product.additionalSearchKeywords}
                    </c:if>
                </td>
            </tr>
        </tbody>
    </table>
</div>

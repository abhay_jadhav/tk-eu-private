<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="category" required="true" type="de.hybris.platform.commercefacades.product.data.CategoryData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="${category.url}" var="categoryUrl"/>
<c:set value="${category.picture}" var="categoryImage"/>

<div class="category-item">
    <ycommerce:testId code="category_wholeCategory">
        <a class="thumb" href="${categoryUrl}" title="${fn:escapeXml(category.name)}">
            <c:choose>
                <c:when test="${not empty categoryImage}">
                    <img src="${categoryImage.url}" alt="${fn:escapeXml(category.name)}" title="${fn:escapeXml(category.name)}"/>
                </c:when>
                <c:otherwise>
                    <theme:image code="img.missingProductImage.responsive.thumbnail" alt="${fn:escapeXml(category.name)}" title="${fn:escapeXml(category.name)}"/>
                </c:otherwise>
            </c:choose>
        </a>

        <div class="details">
           <ycommerce:testId code="category_categoryName">
              <a class="name" href="${categoryUrl}">
                 <c:choose>
                    <c:when test="${fn:length(category.name) > 30}">
                       <c:out value="${fn:substring(category.name, 0, 30)}..."/>
                    </c:when>
                    <c:otherwise>
                       <c:out value="${category.name}" />
                    </c:otherwise>
                 </c:choose>
              </a>
           </ycommerce:testId>
           <br/>
           <c:if test="${not empty category.description}">
                <div>${fn:escapeXml(category.description)}</div>
           </c:if>
        </div>

        <c:set var="category" value="${category}" scope="request"/>
        <c:set var="isGrid" value="true" scope="request"/>
    </ycommerce:testId>
</div>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="account-section-header ${noBorder}">
    <spring:theme code="text.account.paymentDetails" />
</div>
<div class="account-addressbook account-list">
    <div class="row payment_row">
        <div class="payment_subheading">
            <h4 data-qa-id="payment-header"> <spring:theme code="account.payment.termsOfPayment"/></h4>
        </div>

        <div class="payment_content" data-qa-id="payment-details" >
            <c:choose>
                <c:when test="${not empty termsOfPayment}">
                    <p>${fn:escapeXml(termsOfPayment)}</p>
                </c:when>
                <c:otherwise>
                    <spring:theme code="account.payment.termsOfPayment.empty"/>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="row payment_bill_row">
        <div class="payment_bill_subheading">
            <h4 data-qa-id="billing-payment-header"> <spring:theme code="account.payment.billingAddress"/> </h4>
        </div>
        <c:choose>
            <c:when test="${not empty billingAddresses}">
                <div class="account-cards card-select" data-qa-id="billing-payment-details">
                    <div class="row">
                        <c:forEach var="billingAddress" items="${billingAddresses}">
                            <div class="payment_card">
                                <ul class="pull-left payment_ul">
                                    <li>
                                        <strong>  ${billingAddress.firstName}&nbsp;${billingAddress.lastName}&nbsp; </strong>
                                    </li>
                                    <c:if test="${not empty billingAddress.line1}">
                                        <li>  ${billingAddress.line1} </li>
                                    </c:if>
                                    <c:if test="${not empty billingAddress.line2}">
                                        <li> ${billingAddress.line2} </li>
                                    </c:if>
                                    <c:if test="${not empty billingAddress.town}">
                                        <li> ${billingAddress.town} </li>
                                    </c:if>
                                    <c:if test="${not empty billingAddress.region.name}">
                                        <li> ${billingAddress.region.name} </li>
                                    </c:if>
                                    <li>
                                        ${fn:escapeXml(billingAddress.country.name)} &nbsp; ${fn:escapeXml(billingAddress.postalCode)}
                                    </li>
                                </ul>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <spring:theme code="account.payment.billingAddress.empty"/>
            </c:otherwise>
        </c:choose>
    </div>

</div>

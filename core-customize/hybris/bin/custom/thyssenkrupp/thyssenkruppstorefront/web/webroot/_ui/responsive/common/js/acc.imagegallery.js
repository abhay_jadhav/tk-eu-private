ACC.imagegallery = {

    _autoload: [
        "bindImageGallery",
        "bindProductComparison"
    ],

    bindImageGallery: function () {

        $(".js-gallery").each(function(){
            var navigationFlag = false;
            var $image = $(this).find(".js-gallery-image");
            var $carousel = $(this).find(".js-gallery-carousel")
            var imageTimeout;

            var slideCount = $($image).find('.item').length;
            if (slideCount > 3) {
                navigationFlag = true
            }

            $image.owlCarousel({
                singleItem : true,
                pagination:true,
                navigation:navigationFlag,
                lazyLoad:true,
                navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
                afterAction : function(){
                    ACC.imagegallery.syncPosition($image,$carousel,this.currentItem)
                    $image.data("zoomEnable",true)
                },
                startDragging: function(){

                    $image.data("zoomEnable",false)
                },
                afterLazyLoad:function(e){

                    var b = $image.data("owlCarousel") || {}
                    if(!b.currentItem){
                        b.currentItem = 0
                    }

                    var $e=$($image.find("img.lazyOwl")[b.currentItem]);
                    startZoom($e.parent())
                }
            });


            $carousel.owlCarousel({
                navigation:false,
                navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
                pagination:false,
                items:4,
                itemsDesktop : [5000,4],
                itemsDesktopSmall : [1200,4],
                itemsTablet: [768,4],
                itemsMobile : [480,3],
                lazyLoad:true,
                afterAction : function(){

                },
            });

            ACC.imagegallery.highlightCarouselItem(0, $carousel);

            $carousel.on("click","a.item",function(e){
                e.preventDefault();
                $(".owl-item").each(function(){
                    var $this = $(this);
                    $this.css({"border": "none"})
                });
                $(this).parent(".owl-item").css({"border": "solid","border-color": "#0068b3"});
                $image.trigger("owl.goTo",$(this).parent(".owl-item").data("owlItem"));
            })


            function startZoom(e){



                $(e).zoom({
                    url: $(e).find("img.lazyOwl").data("zoomImage"),
                    touch: true,
                    on: "grab",
                    touchduration:300,

                    onZoomIn:function(){

                    },

                    onZoomOut:function(){
                        var owl = $image.data('owlCarousel');
                        owl.dragging(true)
                        $image.data("zoomEnable",true)
                    },

                    zoomEnableCallBack:function(){
                        var bool = $image.data("zoomEnable")

                        var owl = $image.data('owlCarousel');
                        if(bool==false){
                            owl.dragging(true)
                        } else {
                             owl.dragging(false)
                        }
                        return bool;
                    }
                });
            }
        });
    },

    bindProductComparison: function () {
        var navigationFlag = false;
        var $products = $('.js-product-comparison');
        var productCount = $($products).find('.item-inner').length;

        if (productCount > 2) {
            navigationFlag = true
        }
        $products.owlCarousel({
            navigation: navigationFlag,
            navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
            pagination: false,
            items: 3,
            itemsDesktop : [5000, 3],
            itemsDesktopSmall : [1200, 3],
            itemsTablet: [768, 2],
            itemsMobile : [480, 1],
            lazyLoad:true,
            rewindNav : false,
            afterAction : function(){
                var b = $products.data("owlCarousel") || {}
                if (!b.currentItem) {
                    b.currentItem = 0;
                }
                console.log(productCount +' > '+b.currentItem);
                if(productCount > 2) {
                    var prev = $('.js-product-comparison .owl-prev');
                    var next = $('.js-product-comparison .owl-next');

                    if(b.currentItem == 0){
                        $(prev).hide();
                    } else {
                        $(prev).show();
                    }

                    if(b.currentItem == (productCount - 3)){
                        $(next).hide();
                    } else {
                        $(next).show();
                    }
                }
            },
        });
    },

    syncPosition: function($image,$carousel,currentItem){
        $carousel.trigger("owl.goTo",currentItem);
        $carousel.find('.owl-item').css('border', 'none');
        ACC.imagegallery.highlightCarouselItem(currentItem, $carousel);
    },

    highlightCarouselItem: function($owl_item, $carousel){
        $carousel.find('.owl-item:eq('+ $owl_item +')').css({"border": "solid","border-color": "#0068b3"});
    }
};

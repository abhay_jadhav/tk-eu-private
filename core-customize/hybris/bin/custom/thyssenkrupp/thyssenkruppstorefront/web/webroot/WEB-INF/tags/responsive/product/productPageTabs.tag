<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<div class="tabs js-tabs tabs-responsive">
    <div class="tabhead">
        <a href="#"><spring:theme code="product.product.details" /></a>
    </div>
    <div class="tabbody">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="tab-container" data-qa-id="pdp-item-detail-section">
                        <product:productDetailsTab product="${product}" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabhead">
        <a href="#"><spring:theme code="product.product.spec" /></a>
    </div>
    <div class="tabbody">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="tab-container" data-qa-id="pdp-item-specs-section">
                        <product:productDetailsClassifications product="${product}" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tabreview" class="tabhead">
        <a href="#" data-qa-id="pdp-reviews-tab-btn"><spring:theme code="review.reviews" /></a>
    </div>
    <div class="tabbody">
        <div class="container-lg">
            <div class="row">
                <div class=" col-md-6 col-lg-4">
                    <div class="tab-container" data-qa-id="pdp-review-section">
                        <product:productPageReviewsTab product="${product}" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabhead">
        <a href="#"><spring:theme code="product.delivery.tab" /></a>
    </div>
    <div class="tabbody">
       <cms:pageSlot position="Tabs" var="tabs">
          <cms:component component="${tabs}" />
      </cms:pageSlot>
    </div>

    <div class="tabhead">
        <a href="#"><spring:theme code="product.documents.downloads" /></a>
    </div>
    <div class="tabbody">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="tab-container" data-qa-id="pdp-downloads-section">
                        <product:productDocumentsDownloadTab product="${product}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

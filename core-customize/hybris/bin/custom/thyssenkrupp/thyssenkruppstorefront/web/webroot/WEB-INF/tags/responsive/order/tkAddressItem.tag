<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order/locale" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<c:choose>
    <c:when test="${fn:toUpperCase(address.country.isocode) eq 'US'}">
        <order:tkAddressItem_us address="${address}"/>
    </c:when>
    <c:otherwise>
        <order:tkAddressItem_de address="${address}"/>
    </c:otherwise>
</c:choose>

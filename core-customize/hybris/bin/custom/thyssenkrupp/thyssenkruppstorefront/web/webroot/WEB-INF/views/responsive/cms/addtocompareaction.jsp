<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="disabled" value="" />

<c:if test="${product.compareListAdded eq false && product.remainingCount <= 0}">
    <c:set var="disabled" value="disabled" />
</c:if>

<form class="add" action="${url}" method="post">
    <div class="checkbox">
        <label>
            <input data-qa-id="plp-add-to-compare-checkbox" type="checkbox" ${disabled} class="js-add-to-compare-action"
                name="addToCompare" ${(product.compareListAdded) ? "checked=checked" : ""} />
            <c:choose>
                <c:when test="${product.compareListAdded eq true}">
                    <cms:pageSlot position="CompareProductLink" var="feature">
                       <cms:component component="${feature}" element="span" data-qa-id="plp-add-to-compare-lnk"/>
                    </cms:pageSlot>
                    (${product.currentListCount})
                </c:when>
                <c:when test="${product.compareListAdded eq false}">
                    <spring:theme code="product.add.to.compare" text="Add to Compare"/>
                </c:when>
            </c:choose>
        </label>
    </div>
    <input type="hidden" name="productCode" value="${fn:escapeXml(product.code)}" />
</form>

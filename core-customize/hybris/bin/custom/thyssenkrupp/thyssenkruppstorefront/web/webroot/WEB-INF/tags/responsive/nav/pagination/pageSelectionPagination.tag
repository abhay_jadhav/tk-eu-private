<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ attribute name="themeMsgKey" required="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="hasPreviousPage" value="${searchPageData.pagination.currentPage > 0}"/>
<c:set var="hasNextPage" value="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"/>

<spring:url value="${searchUrl}" var="firstPageUrl" htmlEscape="true">
    <spring:param name="page" value="0"/>
</spring:url>

<spring:url value="${searchUrl}" var="lastPageUrl" htmlEscape="true">
    <spring:param name="page" value="${searchPageData.pagination.numberOfPages - 1}"/>
</spring:url>

<c:if test="${(searchPageData.pagination.numberOfPages > 1)}">
    <ul class="pagination" data-qa-id="plp-pagination-section">
        <c:if test="${hasPreviousPage}">
            <c:set var="previousPageNumber" value="${searchPageData.pagination.currentPage}"/>
            <li class="pagination-prev" data-qa-id="plp-pagination-prev-btn">
                <spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
                    <spring:param name="page" value="${previousPageNumber - 1}"/>
                </spring:url>
                <ycommerce:testId code="searchResults_previousPage_link">
                    <a href="${previousPageUrl}" rel="prev" class="glyphicon glyphicon-chevron-left"></a>
                </ycommerce:testId>
            </li>
        </c:if>

        <c:if test="${!hasPreviousPage}">
            <li class="pagination-prev hide" data-qa-id="plp-pagination-prev-btn"><span class="glyphicon glyphicon-chevron-left"></span></li>
        </c:if>

        <c:set var="linkClass" value="hidden-xs"/>

        <c:if test="${hasPreviousPage}">
            <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${firstPageUrl}">1</a></li>
            <c:choose>
                <c:when test="${searchPageData.pagination.currentPage - 3 gt 0}">
                    <li><span>&hellip;</span></li>
                    <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${previousPageUrl}">${previousPageNumber}</a></li>
                </c:when>
                <c:otherwise>
                    <c:forEach begin="2" end="${searchPageData.pagination.currentPage}" var="pageNumber">
                        <spring:url value="${searchUrl}" var="pageNumberUrl" htmlEscape="true">
                            <spring:param name="page" value="${pageNumber - 1}"/>
                        </spring:url>
                        <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${pageNumberUrl}">${pageNumber}</a></li>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </c:if>

        <li class="active"><span>${searchPageData.pagination.currentPage + 1} <span class="sr-only">(current)</span></span></li>

        <c:if test="${hasNextPage}">
            <spring:url value="${searchUrl}" var="nextPageUrl" htmlEscape="true">
                <spring:param name="page" value="${searchPageData.pagination.currentPage + 1}"/>
            </spring:url>
            <c:choose>
                <c:when test="${searchPageData.pagination.currentPage + 2 eq searchPageData.pagination.numberOfPages}">
                    <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${lastPageUrl}">${searchPageData.pagination.numberOfPages}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${nextPageUrl}">${searchPageData.pagination.currentPage + 2}</a></li>
                    <c:choose>
                        <c:when test="${searchPageData.pagination.currentPage + 4 lt searchPageData.pagination.numberOfPages}">
                            <li><span>&hellip;</span></li>
                            <li><a class="${linkClass}" data-qa-id="plp-pagination-btn"  href="${lastPageUrl}">${searchPageData.pagination.numberOfPages}</a></li>
                        </c:when>
                        <c:otherwise>
                            <c:forEach begin="${searchPageData.pagination.currentPage + 3}" end="${searchPageData.pagination.numberOfPages}" var="pageNumber">
                                <spring:url value="${searchUrl}" var="pageNumberUrl" htmlEscape="true">
                                    <spring:param name="page" value="${pageNumber - 1}"/>
                                </spring:url>
                                <li><a class="${linkClass}" data-qa-id="plp-pagination-btn" href="${pageNumberUrl}">${pageNumber}</a></li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </c:if>

        <c:if test="${hasNextPage}">
            <li class="pagination-next" data-qa-id="plp-pagination-next-btn">
                <ycommerce:testId code="searchResults_nextPage_link">
                    <a href="${nextPageUrl}" rel="next" class="glyphicon glyphicon-chevron-right"></a>
                </ycommerce:testId>
            </li>
        </c:if>

        <c:if test="${!hasNextPage}">
            <li class="pagination-next hide" data-qa-id="plp-pagination-next-btn"><span class="glyphicon glyphicon-chevron-right"></span></li>
        </c:if>
    </ul>
</c:if>

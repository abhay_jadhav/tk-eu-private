<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>



<c:forEach items="${childItems}" var="childLevelItem">
    <c:choose>
        <c:when test="${not empty childLevelItem.entries}">
            <c:forEach items="${childLevelItem.entries}" var="childlink">
                <li class="nav__link--secondary js-enquire-has-sub">
                    <div class="title js_nav__link hidden-xs">
                        <cms:component component="${childlink.item}" evaluateRestriction="true" class="nav__link js_nav__link"  />
                        <c:if test="${not empty childLevelItem.children}">
                            <b class="caret-right hidden-xs"></b>
                        </c:if>
                    </div>
                    <cms:component component="${childlink.item}" evaluateRestriction="true" class="nav__link js_nav__link visible-xs" element="span"  />

                    <c:if test="${not empty childLevelItem.children}">
                        <span class="glyphicon  glyphicon-chevron-right hidden-md hidden-lg nav__link--drill__down js_nav__link_secondary--drill__down"></span>
                        <div class="sub-navigation-section col-md-12">
                            <a class="sm-back js-enquire-sub-secondary-close hidden-md hidden-lg" href="#">Back</a>
                            <ul class="sub-navigation-list">
                              <c:set var="childItems" value="${childLevelItem.children}" scope="request" />
                              <jsp:include page="megamenu.jsp" />
                            </ul>
                        </div>

                    </c:if>
                 </li>
            </c:forEach>
        </c:when>
        <c:otherwise>
          <c:set var="childItems" value="${childLevelItem.children}" scope="request" />
          <jsp:include page="megamenu.jsp" />
        </c:otherwise>
    </c:choose>
</c:forEach>

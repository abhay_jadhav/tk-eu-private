<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
    <c:when test="${isPopUp eq 'true'}">
        <div class="forgotten-password">
            <div class="description forgotten-password__description" data-qa-id="reset-explanation" ><spring:theme code="forgottenPwd.description"/></div>
            <form:form method="post" commandName="forgottenPwdForm">
                <div class="control-group m-popup__body">
                    <formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" placeholder="forgottenPwd.email" qaAttribute="login-forgot-email" path="email" mandatory="true"/>
                    <div class="m-popup__body__actions">
                        <div class="m-popup__body__actions__item">
                            <a data-qa-id="cancel-btn" id="forgotten-password-cancel-button">
                                <spring:theme code="text.button.cancel"/>
                            </a>
                        </div>
                        <div class="m-popup__body__actions__item">
                            <button class="btn btn-default"  data-qa-id="reset-btn" type="submit">
                                <spring:theme code="forgottenPwd.title"/>
                            </button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </c:when>
    <c:otherwise>
        <div class="account-section">
            <div class="account-section-content">
                <div class="account-section-content">
                    <div class="container-lg">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                 <div class="forgotten-password">
                                    <div class="description"><spring:theme code="forgottenPwd.description"/></div>
                                    <form:form method="post" class="form-horizontal" commandName="forgottenPwdForm">
                                        <div class="control-group">
                                            <formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email" mandatory="true" />
                                            <button class="btn btn-primary" type="submit">
                                                <spring:theme code="forgottenPwd.title"/>
                                            </button>
                                        </div>
                                    </form:form>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>
<%@ attribute name="favouriteFacetData" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty pageData.breadcrumbs || favouriteFacetData.selected}">

    <div class="facet js-facet">
        <div class="facet__name js-facet-name">
            <span class="glyphicon facet__arrow"></span>
            <spring:theme code="search.nav.applied.facets"/>
        </div>
        <div class="facet__values js-facet-values">
            <ul class="facet__list">
                <c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
                    <li>
                        <c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
                        ${fn:escapeXml(breadcrumb.facetValueName)}&nbsp;<a href="${removeQueryUrl}&amp;favourites=${favouriteFacetData.selected}" ><span class="glyphicon glyphicon-remove"></span></a>
                    </li>
                </c:forEach>

                <c:if test="${favouriteFacetData.selected}">
                    <li>
                        <spring:theme code="search.nav.applied.favourites" text="Favourites"/>&nbsp;
                        <c:url value="${favouriteFacetData.removeUrl}" var="removeQueryUrl"/>
                        <a href="${removeQueryUrl}" ><span class="glyphicon glyphicon-remove"></span></a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>

</c:if>

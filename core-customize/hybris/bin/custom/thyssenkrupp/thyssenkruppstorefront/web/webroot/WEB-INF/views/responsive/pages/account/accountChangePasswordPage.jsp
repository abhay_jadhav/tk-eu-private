<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-6">
            <spring:theme code="text.account.profile.updatePasswordForm"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-lg col-md-7">
        <div class="account-section-content">
            <div class="account-section-form">
                <form:form action="${action}" method="post" commandName="updatePasswordForm">
                    <div class="row">
                        <div class="col-md-6">
                            <formElement:formPasswordBox idKey="currentPassword"
                                 labelKey="profile.currentPassword" path="currentPassword" inputCSS="form-control"
                                 mandatory="true" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <formElement:formPasswordBox idKey="newPassword"
                                 labelKey="profile.newPassword" path="newPassword" inputCSS="form-control js-password-rule-check"
                                 mandatory="true" />
                        </div>
                        <div class="col-md-6 password-rules--wrapper">
                            <div class="form-group">
                                <div class="password-rules display-none" data-js-password-instruction="newPassword">
                                    <ul class="password-rules__list">
                                        <li class="password-rules__list__item has_min_length"> <spring:theme code="account.password.validation.minlength"/> </li>
                                        <li class="password-rules__list__item password-rules__list__item--text"> <spring:theme code="account.password.validation.text"/> </li>
                                        <li class="password-rules__list__item has_uppercase"> <spring:theme code="account.password.validation.uppercase"/> </li>
                                        <li class="password-rules__list__item has_lowercase"> <spring:theme code="account.password.validation.lowercase"/> </li>
                                        <li class="password-rules__list__item has_number"> <spring:theme code="account.password.validation.number"/> </li>
                                        <li class="password-rules__list__item has_special_char"> <spring:theme code="account.password.validation.specialchar"/> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <formElement:formPasswordBox idKey="checkNewPassword"
                                     labelKey="profile.checkNewPassword" path="checkNewPassword" inputCSS="form-control js-confirm-password-rule-check"
                                     mandatory="true" />
                        </div>
                        <div class="col-md-6 password-rules--wrapper">
                            <div class="form-group">
                                <div class="password-rules display-none" data-js-password-instruction="checkNewPassword">
                                    <ul class="password-rules__list">
                                        <li class="password-rules__list__item has_same_password"> <spring:theme code="account.password.validation.passwordmatch"/> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6">
                            <div class="accountActions">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <spring:theme code="updatePwd.submit" text="Update Password" />
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6">
                            <div class="accountActions">
                                <button type="button" class="btn btn-default btn-block backToHome">
                                        <spring:theme code="text.button.cancel" text="Cancel" />
                                </button>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

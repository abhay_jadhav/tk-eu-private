ACC.cartitem = {

    _autoload: [
        "bindCartItem"
    ],

    submitTriggered: false,

    bindCartItem: function ()
    {

        $('.js-execute-entry-action-button').on("click", function ()
        {
            var entryAction = $(this).data("entryAction");
            var entryActionUrl =  $(this).data("entryActionUrl");
            var entryProductCode =  $(this).data("entryProductCode");
            var entryInitialQuantity =  $(this).data("entryInitialQuantity");
            var actionEntryNumbers =  $(this).data("actionEntryNumbers");

            if(entryAction == 'REMOVE')
            {
                ACC.track.trackRemoveFromCart(entryProductCode, entryInitialQuantity);
            }

            var cartEntryActionForm = $("#cartEntryActionForm");
            var entryNumbers = actionEntryNumbers.toString().split(';');
            entryNumbers.forEach(function(entryNumber) {
                var entryNumbersInput = $("<input>").attr("type", "hidden").attr("name", "entryNumbers").val(entryNumber);
                cartEntryActionForm.append($(entryNumbersInput));
            });
            cartEntryActionForm.attr('action', entryActionUrl).submit();
        });

        $('.js-remove-entry-button').on("click", function ()
        {
            var entryNumber = $(this).attr('id').split("_")
            var form = $('#updateCartForm' + entryNumber[1]);

            var productCode = form.find('input[name=productCode]').val();
            var initialCartQuantity = form.find('input[name=initialQuantity]');
            var cartQuantity = form.find('input[name=quantity]');

            ACC.track.trackRemoveFromCart(productCode, initialCartQuantity.val());

            cartQuantity.val(0);
            initialCartQuantity.val(0);
            form.submit();
        });

        $('.js-update-entry-quantity-input').on("blur", function (e)
        {
            ACC.cartitem.handleUpdateQuantity(this, e);

        }).on("keyup", function (e)
        {
            return ACC.cartitem.handleKeyEvent(this, e);
        }
        ).on("keydown", function (e)
        {
            return ACC.cartitem.handleKeyEvent(this, e);
        }
        );

        ACC.cartitem.bindWishDeliveryDatepicker();
    },

    handleKeyEvent: function (elementRef, event)
    {

        if (event.which == 13 && !ACC.cartitem.submitTriggered)
        {
            ACC.cartitem.submitTriggered = ACC.cartitem.handleUpdateQuantity(elementRef, event);
            return false;
        }
        else
        {
            if (ACC.cartitem.submitTriggered)
            {
                return false;
            }
        }

        return true;
    },

    handleUpdateQuantity: function (elementRef, event)
    {

        var form = $(elementRef).closest('form');

        var productCode = form.find('input[name=productCode]').val();
        var initialCartQuantity = form.find('input[name=initialQuantity]').val();
        var newCartQuantity = form.find('input[name=quantity]').val();

        if(newCartQuantity != '')
        {
            if(initialCartQuantity != newCartQuantity)
            {
                ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
                form.submit();

                return true;
            }
        }
        else
        {
        	form.find('input[name=quantity]').val(initialCartQuantity);
        }
        return false;
    },

    bindWishDeliveryDatepicker: function()
    {

        $(".item__wishdeliverydate div.form-group .datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0,
            onSelect: function(dateText, inst) {
                var date = $(this).val();
                var form = $(this).closest('form');
                $(form).css('opacity', 0.3);
                $.post($(form).attr('action'), $(form).serialize())
                .done(function() {
                    $(form).css('opacity', 1);
                });
            }
        });

        $("form.updateDeliveryDate").on('submit',function(e){
            e.preventDefault();
        });
    },
};

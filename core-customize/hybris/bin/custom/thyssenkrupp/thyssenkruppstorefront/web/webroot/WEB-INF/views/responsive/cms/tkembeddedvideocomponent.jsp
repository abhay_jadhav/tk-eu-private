<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="videoCSSClass" value="col-md-12" />
<c:set var="textCSSClass" value="col-md-12" />
<c:url var="url" value="" />
<c:if test="${component.platform eq 'YOUTUBE'}">
    <c:set var="url" value="https://www.youtube.com/embed/${component.videoIdentifier}" />
</c:if>

<c:choose>
    <c:when test="${component.rendering eq 'WITHOUT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-12" />
    </c:when>
    <c:when test="${component.rendering eq 'RIGHT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-6" />
        <c:set var="textCSSClass" value="col-md-6" />
    </c:when>
    <c:when test="${component.rendering eq 'LEFT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-6 pull-right" />
        <c:set var="textCSSClass" value="col-md-6" />
    </c:when>
    <c:otherwise>
        <c:set var="videoCSSClass" value="col-md-12" />
        <c:set var="textCSSClass" value="col-md-12" />
    </c:otherwise>
</c:choose>

<div class="video__component ${(component.rendering eq 'LEFT_TEXT' or component.rendering eq 'RIGHT_TEXT' ? 'video__component--has-text' : '')}">
    <div class="row">
        <div class="${videoCSSClass}">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="${url}" frameborder="0" allow="autoplay; encrypted-media"></iframe>
            </div>
        </div>
        <c:if test="${component.rendering ne 'WITHOUT_TEXT'}">
            <div class="${textCSSClass}">${component.text}</div>
        </c:if>
    </div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="favouriteFacetData" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<div class="facet js-facet">
    <div class="facet__name js-facet-name">
        <span class="glyphicon facet__arrow"></span>
        <spring:theme code="search.nav.applied.shopbyfavorites" text="Shop by Favourites"/>
    </div>

    <div class="facet__values js-facet-values js-facet-form">
        <ul class="facet__list js-facet-list ">
            <li>
                <form action="#" method="get">
                     <input type="hidden" name="q" value="${favouriteFacetData.actionUrl}">
                     <input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
                     <input type="hidden" name="favourites" value="${!favouriteFacetData.selected}"/>
                    <label>
                        <input type="checkbox" class="facet__list__checkbox js-facet-checkbox sr-only"  ${favouriteFacetData.selected ? 'checked="checked"' : ''} />
                        <span class="facet__list__label">
                            <span class="facet__list__mark"></span>
                            <span class="facet__list__text">
                                <spring:theme code="search.nav.applied.favourites" text="Products"/>&nbsp;
                                <span class="facet__value__count">(${favouriteFacetData.count})</span>
                            </span>
                        </span>
                    </label>
                </form>
            </li>
        </ul>
    </div>
</div>

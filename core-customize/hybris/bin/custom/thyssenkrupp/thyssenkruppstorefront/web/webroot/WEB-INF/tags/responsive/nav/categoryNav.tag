<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData" %>
<%@ attribute name="favoriteFacetData" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${showCategoriesOnly}">
        <nav:categoryNavLinks categories="${pageData.subCategories}" favoriteFacetData="${favoriteFacetData}"/>
    </c:when>

    <c:otherwise>
        <nav:facetNavAppliedFilters pageData="${pageData}"/>
        <nav:facetNavRefinements pageData="${pageData}"/>
    </c:otherwise>
</c:choose>

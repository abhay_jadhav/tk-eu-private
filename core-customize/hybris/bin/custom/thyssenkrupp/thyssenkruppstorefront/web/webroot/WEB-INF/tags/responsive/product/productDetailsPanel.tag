<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/responsive/gtm" %>

<div class="product-details page-title">
    <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
        <div class="name" data-qa-id="pdp-item-name">${fn:escapeXml(product.name)}<span class="sku">ID</span><span class="code" data-qa-id="pdp-item-code">${fn:escapeXml(product.code)}</span></div>
    </ycommerce:testId>
    <product:productReviewSummary product="${product}" showLinks="true"/>
</div>
<div class="row">
    <div class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-lg-4">
        <product:productImagePanel galleryImages="${galleryImages}"/>
    </div>
    <div class="clearfix hidden-sm hidden-md hidden-lg"></div>
    <div class="col-sm-6 col-lg-8">
        <div class="product-main-info">

            <div class="row">
                <div class="col-lg-6">
                    <div class="product-details">
                        <product:productPromotionSection product="${product}"/>
                        <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                            <product:productPricePanel product="${product}"/>
                        </ycommerce:testId>
                        <div class="description" data-qa-id="pdp-item-description">${ycommerce:sanitizeHTML(product.summary)}</div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-6">
                    <cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
                        <cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
                    </cms:pageSlot>
                    <cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select clearfix">
                        <cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
                    </cms:pageSlot>
                    <div class="row">
                        <hr/>
                        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                            <div class="col-lg-12 col-sm-12">
                                <cms:pageSlot position="AddToWishList" var="component" element="div">
                                    <cms:component component="${component}" element="div"/>
                                </cms:pageSlot>
                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <cms:pageSlot position="AddToFavourites" var="component" element="div">
                                    <cms:component component="${component}" element="div"/>
                                </cms:pageSlot>
                            </div>
                        </sec:authorize>
                        <div class="col-lg-12 col-sm-12">
                            <cms:pageSlot position="TkSocialShare" var="component" >
                                <cms:component component="${component}" />
                            </cms:pageSlot>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<storepickup:pickupStorePopup />
<gtm:productObjectGTM product="${product}" />
<gtm:productDetailGTM product="${product}" />

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="productCount" required="true" type="java.lang.Integer" %>

<div class="product_add_to_compare_widget">
    <ul class="list-inline">
        <li>
            <strong>
                <cms:pageSlot position="CompareProductLink" var="feature">
                    <cms:component component="${feature}" element="span" data-qa-id="plp-compare-compare-lnk" />
                    <c:if test="${productCount > 0}">
                        (${productCount})
                    </c:if>
                </cms:pageSlot>
            </strong>
        </li>
        <c:if test="${productCount > 0}">
            <li>
                <strong>
                    <cms:pageSlot position="CompareProductClearAllLink" var="feature">
                      <cms:component component="${feature}" element="span" data-qa-id="plp-compare-clearall-lnk"/>
                    </cms:pageSlot>
                </strong>
            </li>
        </c:if>
    </ul>
</div>

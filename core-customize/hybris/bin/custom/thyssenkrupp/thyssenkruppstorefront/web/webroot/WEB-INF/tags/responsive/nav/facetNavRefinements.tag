<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>
<%@ attribute name="favouriteFacetData" required="false" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

 <c:if test="${favouriteFacetData.enabled eq true}">
    <nav:facetFavouriteRefinement favouriteFacetData="${favouriteFacetData}"/>
 </c:if>

<c:forEach items="${pageData.facets}" var="facet">
    <c:choose>
        <c:when test="${facet.code eq 'availableInStores'}">
            <nav:facetNavRefinementStoresFacet facetData="${facet}" userLocation="${userLocation}"/>
        </c:when>
        <c:otherwise>
            <nav:facetNavRefinementFacet facetData="${facet}" favouriteFacetData="${favouriteFacetData}"/>
        </c:otherwise>
    </c:choose>
</c:forEach>

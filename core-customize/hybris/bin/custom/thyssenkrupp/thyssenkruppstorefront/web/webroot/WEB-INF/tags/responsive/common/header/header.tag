<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
    <cms:component component="${component}" />
</cms:pageSlot>
<cms:pageSlot position="GoogleTagManagerContentSlot" var="component" element="div" class="container">
    <cms:component component="${component}" />
</cms:pageSlot>
<header class="js-mainHeader">
    <nav class="navigation navigation--top hidden-xs hidden-sm">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="nav__left js-site-logo">
                    <cms:pageSlot position="SiteLogo" var="logo" limit="1">
                        <cms:component component="${logo}" element="div" class="yComponentWrapper"/>
                    </cms:pageSlot>
                </div>
            </div>
            <div class="col-sm-12 col-md-8">
                <div class="nav__right">
                    <ul class="nav__links nav__links--account">
                        <c:if test="${empty hideHeaderLinks}">
                            <c:if test="${uiExperienceOverride}">
                                <li class="backToMobileLink"><c:url
                                        value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
                                    <a href="${backToMobileStoreUrl}"> <spring:theme
                                            code="text.backToMobileStore" />
                                    </a>
                                </li>
                            </c:if>

                        <cms:pageSlot position="JspHeader" var="component">
                            <cms:component component="${component}"/>
                        </cms:pageSlot>

                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <%-- a hook for the my account links in desktop/wide desktop--%>
    <div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
        <ul class="nav__links">

        </ul>
    </div>
    <div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
        <ul class="nav__links js-nav__links">

        </ul>
    </div>
    <nav class="navigation navigation--middle js-navigation--middle">
        <div class="container-fluid">
            <div class="row">
                <div class="mobile__nav__row mobile__nav__row--table">
                    <div class="mobile__nav__row--table-group">
                        <div class="mobile__nav__row--table-row">
                            <div class="mobile__nav__row--table-cell visible-xs hidden-sm">
                                <button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation"
                                        type="button">
                                    <span class="glyphicon glyphicon-align-justify"></span>
                                </button>
                            </div>

                            <div class="mobile__nav__row--table-cell visible-xs mobile__nav__row--seperator">
                                <ycommerce:testId code="header_search_activation_button">
                                    <button    class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-sm hidden-md hidden-lg" type="button">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </ycommerce:testId>
                            </div>

                            <c:if test="${empty hideHeaderLinks}">
                                <ycommerce:testId code="header_StoreFinder_link">
                                    <div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator">
                                        <a href="<c:url value="/store-finder"/>" class="mobile__nav__row--btn mobile__nav__row--btn-location btn">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </a>
                                    </div>
                                </ycommerce:testId>
                            </c:if>

                            <cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg">
                                <cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
                            </cms:pageSlot>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row desktop__nav">
                <div class="nav__left col-xs-12 col-md-4 col-sm-2">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs visible-sm mobile-menu">
                            <button class="btn js-toggle-sm-navigation" type="button">
                                <span class="glyphicon glyphicon-align-justify"></span>
                            </button>
                        </div>
                        <div class="col-sm-10 col-md-12 hide-breadcrumb">
                            <nav:topNavigation />
                        </div>

                    </div>
                </div>
                <div class="nav__right col-xs-12 col-md-8 col-sm-10 text-right">

                    <div class="site-search">
                        <cms:pageSlot position="SearchBox" var="component">
                            <cms:component component="${component}" element="div"/>
                        </cms:pageSlot>
                    </div>
                    <ul class="nav__links nav__links--shop_info hidden-xs">
                        <li>
                            <c:if test="${empty hideHeaderLinks}">
                                <ycommerce:testId code="header_StoreFinder_link">
                                    <div class="nav-location hidden-xs">
                                        <a href="<c:url value="/store-finder"/>" class="btn" data-qa-id="header-store-locator-btn">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </a>
                                    </div>
                                </ycommerce:testId>
                            </c:if>

                        </li>

                        <li>
                            <cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">

                                <cms:component component="${cart}" element="div"/>

                            </cms:pageSlot>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
     <div class="show-breadcrumb">
        <nav:topNavigation />
      </div>
    <a id="skiptonavigation"></a>
</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"    class="container-fluid">
    <cms:component component="${component}" />
</cms:pageSlot>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/responsive/productCategory" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<c:if test="${not empty subCategories}">
    <div class="category__listing category__list">
        <c:forEach items="${subCategories}" var="subCategory" varStatus="status">
            <category:categoryListerItem category="${subCategory}" />
        </c:forEach>
    </div>
</c:if>


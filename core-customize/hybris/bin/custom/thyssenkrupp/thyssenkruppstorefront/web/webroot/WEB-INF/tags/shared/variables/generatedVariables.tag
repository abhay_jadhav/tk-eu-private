<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="screenXs" value="320px" scope="session"/>
<c:set var="screenSm" value="646px" scope="session"/>
<c:set var="screenMd" value="971px" scope="session"/>
<c:set var="screenLg" value="1170px" scope="session"/>

<c:set var="screenXsMin" value="320px" scope="session"/>
<c:set var="screenSmMin" value="646px" scope="session"/>
<c:set var="screenMdMin" value="971px" scope="session"/>
<c:set var="screenLgMin" value="1170px" scope="session"/>

<c:set var="screenXsMax" value="645px" scope="session"/>
<c:set var="screenSmMax" value="970px" scope="session"/>
<c:set var="screenMdMax" value="1169px" scope="session"/>

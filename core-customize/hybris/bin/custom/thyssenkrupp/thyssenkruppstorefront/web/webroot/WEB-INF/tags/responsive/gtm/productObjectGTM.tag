<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="price" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="quantity" required="false" type="java.lang.Double"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="basePrice" value="${product.price.value}" />

<script>
    var products = products || [];

    <c:if test="${not empty price}">
        <c:set var="basePrice" value="${price.value}" />
    </c:if>

    products.push( {
        'name': '${product.name}',
        'id': '${product.code}',
        <c:if test="${not empty basePrice}">
            'price': '${basePrice}',
        </c:if>
        <c:if test="${product.multidimensional}">
            'variant': [
                <c:forEach var="unit" items="${product.categories}" varStatus="loop">
                    '${unit.name}',
                </c:forEach>
            ],
        </c:if>
        <c:if test="${not empty quantity}">
            'quantity': '${quantity}',
        </c:if>
        'category': [
            <c:forEach var="category" items="${product.categories}" varStatus="loop">
                '${category.code}',
            </c:forEach>
        ],
    });
</script>

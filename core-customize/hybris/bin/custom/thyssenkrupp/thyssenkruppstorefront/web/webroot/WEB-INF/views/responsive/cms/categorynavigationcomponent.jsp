<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:if test="${component.visible}">
    <c:set var="rootNode" value="${component.navigationNode}"/>
    <c:set var="childNodes" value="${rootNode.children}"/>
    <ul class="nav navbar-nav m-header__primary-nav__menu-left js-menu-desktop visible-md visible-lg">
        <c:forEach items="${component.navigationNode.children}" var="childLevel1">
            <li class="dropdown yamm-fullwidth">
                <c:forEach items="${childLevel1.entries}" var="childlink1">
                    <cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="dropdown-toggle m-header__main-nav__link" data-toggle="dropdown"/>
                </c:forEach>

                <c:set var="hasSubChild" value="false"/>
                <c:forEach items="${childLevel1.children}" var="childLevel2">
                    <c:if test="${not empty childLevel2.children}">
                        <c:set var="hasSubChild" value="true"/>
                    </c:if>
                </c:forEach>

                <div class="dropdown-menu">
                    <div class="container">
                        <div class="row">
                            <c:choose>
                                <c:when test="${hasSubChild}">
                                    <c:forEach items="${childLevel1.children}" var="childLevel2">
                                        <c:choose>
                                            <c:when test="${childLevel2.children[0].entries[0] != null && childLevel2.children[0].entries[0].item.itemtype eq 'TkBannerComponent'}">
                                                 <c:forEach items="${childLevel2.children}" var="childLevel3" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                                     <c:forEach items="${childLevel3.entries}" var="childlink3">
                                                         <c:set value="${childlink3.item.renderOption}" var="renderOption"/>
                                                         <c:choose>
                                                              <c:when test="${renderOption == 'NAVIGATION'}">
                                                                  <c:set value="${childlink3.item.rightOffset}" var="rightOffest"/>
                                                                  <c:set value="${childlink3.item.leftOffset}" var="leftOffest"/>
                                                              </c:when>
                                                              <c:otherwise>
                                                                   <c:set value="0" var="rightOffest"/>
                                                                   <c:set value="0" var="leftOffest"/>
                                                              </c:otherwise>
                                                          </c:choose>
                                                         <cms:component component="${childlink3.item}" evaluateRestriction="true" element="div" class="col-md-3 col-md-offset-${leftOffest} col-md-offset-right-${rightOffest}"/>
                                                     </c:forEach>
                                                 </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="col-md-2">
                                                    <div class="m-mega-menu__child-menus">
                                                        <p class="m-mega-menu__child-menus--category-label">${fn:escapeXml(childLevel2.title)}</p>
                                                        <ul class="list-unstyled">
                                                            <c:forEach items="${childLevel2.children}" var="childLevel3" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                                                <c:forEach items="${childLevel3.entries}" var="childlink3">
                                                                    <cms:component component="${childlink3.item}" evaluateRestriction="true" element="li" class="m-nav-node__container__list__item__link"/>
                                                                </c:forEach>
                                                            </c:forEach>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:when>
                            </c:choose>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-mega-menu__close js-close-mega-menu">
                                    <hr />
                                    <p class="m-mega-menu__close__text"><spring:theme code="menu.closeMenu" /> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </c:forEach>
    </ul>
    <div id="js-sidebar" class="m-sidebar js-sidebar visible-xs visible-sm out">
        <div class="m-sidebar__heading">
            <span class="m-sidebar__heading__label">
                <c:if test="${not empty component.navigationNode.entries[0].item.linkName}">
                    ${component.navigationNode.entries[0].item.linkName}
                </c:if>
            </span>
            <a href="javascript:void(0)" class="m-sidebar__heading__close js-menu-toggle">
                <i class="u-icon-tk u-icon-tk--close-single"> </i>
            </a>
        </div>
        <ul id="sidebar-links" class="nav m-sidebar__links visible-xs visible-sm">
            <c:forEach items="${component.navigationNode.children}" var="childLevel1" varStatus="loop">
                <c:set var="hasSubChild" value="false"/>
                <c:forEach items="${childLevel1.children}" var="childLevel2">
                    <c:if test="${not empty childLevel2.children}">
                        <c:set var="hasSubChild" value="true"/>
                    </c:if>
                </c:forEach>
                <li class="m-sidebar__links__item m-sidebar__links__item--first-level ${hasSubChild ? 'm-sidebar__links__item--has-child' : ''}">
                    <c:forEach items="${childLevel1.entries}" var="childlink1">
                        <cms:component component="${childlink1.item}" evaluateRestriction="true"
                        element="span" data-target="#submenu-${loop.index}" class="collapsed" data-toggle="collapse" aria-expanded="true" data-parent="#sidebar-links"/>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${hasSubChild}">
                            <c:set var="secondSubMenuId" value="submenu-${loop.index}"/>
                            <ul id="${secondSubMenuId}" class="list-unstyled collapse" role="menu">
                                <c:forEach items="${childLevel1.children}" var="childLevel2" varStatus="loop1">
                                    <c:if test="${childLevel2.children[0].entries[0] != null && childLevel2.children[0].entries[0].item.itemtype ne 'TkBannerComponent'}">
                                        <li class="m-sidebar__links__item m-sidebar__links__item--second-level ${fn:length(childLevel2.children) gt 0 ? 'm-sidebar__links__item--has-child' : ''}">
                                            <c:set var="thirdSubMenuId" value="submenu-${loop.index}${loop1.index}"/>
                                            <span data-toggle="collapse" data-target="#${thirdSubMenuId}" aria-expanded="true" class="collapsed" data-parent="${secondSubMenuId}">
                                                <a href="#">${fn:escapeXml(childLevel2.title)}</a>
                                            </span>
                                            <ul id="${thirdSubMenuId}" class="list-unstyled collapse" role="menu">
                                                <c:forEach items="${childLevel2.children}" var="childLevel3" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                                    <c:forEach items="${childLevel3.entries}" var="childlink3">
                                                        <cms:component component="${childlink3.item}" evaluateRestriction="true" element="li" class="m-sidebar__links__item m-sidebar__links__item--third-level"/>
                                                    </c:forEach>
                                                </c:forEach>
                                            </ul>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </c:when>
                    </c:choose>
                </li>
            </c:forEach>
        </ul>
        <div class="m-sidebar__bottom visible-xs js-sidebar-bottom-links">
        </div>
    </div>
</c:if>

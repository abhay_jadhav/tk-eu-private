<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
    <nav:facetNavAppliedFilters pageData="${searchPageData}" favouriteFacetData="${favouriteFacetData}"/>
    <nav:facetNavRefinements pageData="${searchPageData}" favouriteFacetData="${favouriteFacetData}"/>
</div>

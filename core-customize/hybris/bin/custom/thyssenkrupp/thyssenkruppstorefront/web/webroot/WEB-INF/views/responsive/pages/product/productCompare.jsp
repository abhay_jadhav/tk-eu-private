<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<template:page>
    <div class="l-product-comparison">
        <cms:pageSlot position="SocialShareSlotComparison" var="social" >
            <cms:component component="${social}" />
        </cms:pageSlot>
        <div class="container">
            <div class="row">
                <div class="col-md-12" data-qa-id="comparison-zero-products"><h3 class="l-product-comparison__header"><spring:theme code="text.product.compare" text="Compare" /></h3></div>
            </div>
            <c:choose>
                <c:when test="${isEmpty eq 'true'}">
                    <cms:pageSlot position="CompareProductsAttributesSlot" var="feature">
                        <cms:component component="${feature}" element="div" class="alert alert-info" />
                    </cms:pageSlot>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="l-product-comparison__attributes">
                                <c:forEach var="columnHeader" items="${listData}" end="0">
                                    <ul class="list-unstyled text-right l-product-comparison__product__attributes">
                                        <c:forEach var="columnValue" items="${columnHeader.value}" varStatus="loop">
                                            <c:if test="${loop.count eq 1}">
                                              <li class="l-product-comparison__product__head"></li>
                                            </c:if>
                                            <li class="l-product-comparison__product__attribute">
                                                <span class="text-uppercase" data-qa-attribute="comparison-item-label">${columnValue}</span>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="carousel js-product-comparison">
                                <c:forEach var="columnHeader" items="${listData}">
                                    <c:if test="${columnHeader.key ne 'attributes'}">
                                        <div class="item-inner l-product-comparison__product">
                                            <ul class="list-unstyled l-product-comparison__product__properties">
                                                <c:forEach var="columnValue" items="${columnHeader.value}" varStatus="loopCount">
                                                    <c:choose>
                                                        <c:when test="${loopCount.count eq 1}">
                                                            <li class="item l-product-comparison__product__head">
                                                                <c:url value="${columnValue.url}" var="productUrl" />
                                                                <form class="add clearfix" action="/product-comparison/modifyList" method="post">
                                                                    <button type="submit" name="Remove" class="btn btn-link pull-right" data-qa-attribute="comparison-item-remove-btn"><span class="fa fa-times"></span></button>
                                                                    <input type="hidden" name="productCode" value="${fn:escapeXml(columnValue.code)}" />
                                                                </form>
                                                                <a href="${productUrl}" class="l-product-comparison__product__link">
                                                                    <product:productPrimaryImage product="${columnValue}" format="product" />
                                                                    <div class="l-product-comparison__product__description" data-qa-attribute="comparison-item-name"><c:out value="${columnValue.name}" /></div>
                                                                </a>
                                                            </li>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <li class="l-product-comparison__product__property" data-qa-attribute="comparison-item-values">
                                                                <c:choose>
                                                                    <c:when test = "${not empty columnValue}" >
                                                                        ${columnValue}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        -
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </li>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</template:page>

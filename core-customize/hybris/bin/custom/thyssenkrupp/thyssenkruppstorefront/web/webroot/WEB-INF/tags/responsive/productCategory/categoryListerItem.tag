<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="category" required="true" type="de.hybris.platform.commercefacades.product.data.CategoryData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:url value="${category.url}" var="categoryUrl"/>
<c:set value="${category.picture}" var="categoryImage"/>

<li class="category__list--item">
    <ycommerce:testId code="test_searchPage_wholeCategory">

        <a class="category__list--thumb" href="${categoryUrl}" title="${fn:escapeXml(category.name)}">
            <c:choose>
                <c:when test="${not empty categoryImage}">
                    <img src="${categoryImage.url}" alt="${fn:escapeXml(category.name)}" title="${fn:escapeXml(category.name)}"/>
                </c:when>
                <c:otherwise>
                    <theme:image code="img.missingProductImage.responsive.thumbnail" alt="${fn:escapeXml(category.name)}" title="${fn:escapeXml(category.name)}"/>
                </c:otherwise>
            </c:choose>
        </a>

        <ycommerce:testId code="searchPage_categoryName_link_${category.code}">
            <a class="category__list--name" href="${categoryUrl}">${fn:escapeXml(category.name)}</a>
        </ycommerce:testId>

        <c:if test="${not empty category.description}">
            <div class="category__listing--description">${fn:escapeXml(category.description)}</div>
        </c:if>

        <c:set var="category" value="${category}" scope="request"/>
    </ycommerce:testId>
</li>

<%@ tag import="de.hybris.platform.util.Config" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:theme code='<%=Config.getParameter("thyssenkrupp.schulte.gtmid")%>' var="googleTagmanagerId"/>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=${googleTagmanagerId}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="canonical" href="${request.scheme}://${request.serverName}:${request.serverPort}/"/>

<c:set var="currentQuery" value="" />
<c:if test="${not empty searchPageData}">
    <c:set var="currentQuery">
        <c:catch var="exception">${searchPageData.currentQuery}</c:catch>
    </c:set>
</c:if>
<c:if test="${not empty searchPageData && not empty currentQuery}">
    <c:set var="hasPreviousPage" value="${searchPageData.pagination.currentPage > 0}"/>
    <c:set var="hasNextPage" value="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"/>
    <c:set var="currentQueryUrl" value="${searchPageData.currentQuery.url}"/>
    <c:if test="${hasPreviousPage && not empty currentQueryUrl}">
        <spring:url value="${currentQueryUrl}" var="previousPageUrl" htmlEscape="true">
            <spring:param name="page" value="${searchPageData.pagination.currentPage - 1}"/>
        </spring:url>
        <link rel="prev" href="${previousPageUrl}"/>
    </c:if>
    <c:if test="${hasNextPage && not empty currentQueryUrl}">
        <spring:url value="${currentQueryUrl}" var="nextPageUrl" htmlEscape="true">
            <spring:param name="page" value="${searchPageData.pagination.currentPage + 1}"/>
        </spring:url>
        <link rel="next" href="${nextPageUrl}"/>
    </c:if>
</c:if>

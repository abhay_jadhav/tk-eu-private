<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
    type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<c:if test="${not empty cartData.billingAddress}">
<ul class="checkout-order-summary-list">
    <li class="checkout-order-summary-list-heading">
        <div class="title"><spring:theme code="checkout.multi.billing.to" />:</div>
        <div class="address" data-qa-id="order-summary-billing-address">
           <order:tkAddressItem address="${cartData.billingAddress}"/>
        </div>
    </li>
    </ul>
</c:if>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>


<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
    <jsp:include page="../../../../../common/fragments/checkout/multi/hostedOrderPostForm.jsp" />
</template:page>

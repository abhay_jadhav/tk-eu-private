<%@ attribute name="commentText" required="true" type="java.lang.String" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="row">
    <div class="col-sm-12">
        <div class="checkout-comments">
            <div class="checkout-comments-text hide-temp">
                <label>Comment(s): </label>
                <p>${commentText}</p>

            </div>
            <div class="checkout-comments-form">
                <c:url value="/checkout/multi/delivery-method/updateShippingNotes" var="shippingNotesUpdateAction"/>
                <div class="row">
                    <div class="col-sm-12">
                            <textarea class="form-control"
                                      rows="1" id="customerShippingNotes"
                                      name="customerShippingNotes"
                                      placeholder="Please add any additional comments for this order here...">${commentText}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

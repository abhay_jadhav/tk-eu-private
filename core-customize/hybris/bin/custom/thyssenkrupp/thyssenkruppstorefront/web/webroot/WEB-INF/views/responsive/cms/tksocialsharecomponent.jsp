<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${not empty component.socialShares}">
    <a class="a2a_dd share" data-qa-id="pdp-item-share" href="https://www.addtoany.com/share_save" />
        <h5><i class="fa fa-share-alt"></i>&nbsp;${component.linkText}</h5>
    </a>
    <c:set var = "services" value = ""/>
    <c:forEach items="${component.socialShares}" var="service" varStatus="loop">
        <c:choose>
            <c:when test="${loop.index gt 0 }">
                <c:set var="services" value="${services}, \"${fn:toLowerCase(service)}\"" />
            </c:when>
            <c:otherwise>
                <c:set var="services" value="\"${fn:toLowerCase(service)}\"" />
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <script type="text/javascript">
        var a2a_config = a2a_config || {};
        a2a_config.num_services = ${fn:length(component.socialShares)};
        a2a_config.prioritize = [${services}];
    </script>
    <script type="text/javascript" src="https://static.addtoany.com/menu/page.js"></script>
</c:if>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tkcategory" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/productCategory" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<c:if test="${not empty subCategories}">
    <div class="m-category-list m-category-list--grid">
        <div class="row">
            <c:forEach items="${subCategories}" var="subCategory" varStatus="status">
                <div class="col-md-2">
                    <tkcategory:categoryListerGridItem category="${subCategory}" />
                </div>
            </c:forEach>
        </div>
    </div>
</c:if>

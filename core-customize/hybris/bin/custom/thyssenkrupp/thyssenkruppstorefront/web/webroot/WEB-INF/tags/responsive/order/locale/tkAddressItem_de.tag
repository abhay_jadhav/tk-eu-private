<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<ul class="address-tag">
     <c:if test="${not empty fn:escapeXml(address.companyName)}">
         <li> ${fn:escapeXml(address.companyName)}</li>
     </c:if>
    <c:if test="${not empty fn:escapeXml(address.firstName)}">
        <li>${fn:escapeXml(address.department)} ${fn:escapeXml(address.title)} ${address.firstName}&#x20; ${address.lastName}</li>
    </c:if>
    <c:if test="${not empty address.line1}">
        <li>${address.line1} ${address.line2}</li>
    </c:if>
    <li>${fn:escapeXml(address.postalCode)}
        <c:if test="${not empty address.town}">
            ${fn:escapeXml(address.town)}
        </c:if>
    </li>
    <li>${fn:escapeXml(address.country.name)}</li>
</ul>

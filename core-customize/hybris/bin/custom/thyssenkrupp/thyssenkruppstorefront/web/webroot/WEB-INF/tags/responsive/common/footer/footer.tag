<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <cms:pageSlot position="Footer" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
            <div class="col-sm-12 col-md-4">
                <cms:pageSlot position="FooterAddress" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>

        </div>
    </div>
</footer>


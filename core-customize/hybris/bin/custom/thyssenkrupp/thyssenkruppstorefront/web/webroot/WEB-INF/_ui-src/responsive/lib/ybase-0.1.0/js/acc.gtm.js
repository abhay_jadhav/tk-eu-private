/*global window, ACC*/
ACC.gtm = {

    _autoload: [
        "fnInitializeGtmVariables",
        "fnBindCartOperations"
    ],

    fnInitializeGtmVariables: function () {
        window.products = window.products || [];
        window.dataLayer = window.dataLayer || [];
    },

    fnBindCartOperations: function () {
        if (window.mediator !== undefined){
        window.mediator.subscribe('trackAddToCart', function (data) {
            for (var i = 0; i < products.length; i++) {
                if (data.productCode === products[i].id) {
                    products[i].quantity = data.quantity;
                    dataLayer.push( {
                        'event': 'addToCart',
                        'ecommerce': {
                            'add': {
                                'products': [ products[i] ],
                            }
                        }
                    });
                }
            }
        });
    }
        if (window.mediator !== undefined) {
        window.mediator.subscribe('trackRemoveFromCart', function (data) {
            ACC.gtm.fnRemoveCartItemGtmEvent(data);
        } );
        }

        if (window.mediator !== undefined) {
        window.mediator.subscribe('trackUpdateCart', function (data) {
            if (data.newCartQuantity == 0) {
                ACC.gtm.fnRemoveCartItemGtmEvent(data);
            }
        });
        }
    },

    fnRemoveCartItemGtmEvent: function (data) {
        for (var i = 0; i < products.length; i++) {
            if (data.productCode === products[i].id) {
                products[i].quantity = data.initialCartQuantity;
                dataLayer.push( {
                    'event': 'removeFromCart',
                    'ecommerce': {
                        'remove': {
                            'products': [ products[i] ],
                        }
                    }
                });
            }
        }
    }
}
<%@ page trimDirectiveWhitespaces="true" %>
<div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="en-block" data-qa-id="cms-slot">${emptyMessageText}</div>
            </div>
        </div>
    </div>
</div>

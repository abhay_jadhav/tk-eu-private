<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<div class="row">
    <div class="col-sm-6">
        <div class="checkout-headline">
            <span class="glyphicon glyphicon-lock"></span>
            <spring:theme code="checkout.multi.secure.checkout"/>
        </div>
        <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
            <jsp:body>
                <c:if test="${not empty paymentFormUrl}">
                    <div class="checkout-paymentmethod">
                        <div class="checkout-indent">
                            <div class="headline"><spring:theme code="checkout.multi.paymentMethod"/></div>
                            <c:if test="${not empty paymentFormUrl}">
                                <ycommerce:testId code="paymentDetailsForm">
                                    <form:form id="silentOrderPostForm" name="silentOrderPostForm" commandName="sopPaymentDetailsForm" action="${paymentFormUrl}" method="POST">
                                        <jsp:include page="../../../../../common/fragments/checkout/silentOrderPostForm.jsp" />
                                        <input type="hidden" value="${fn:escapeXml(silentOrderPageData.parameters['billTo_email'])}" class="text" name="billTo_email" id="billTo_email">
                                        <address:billAddressFormSelector supportedCountries="${countries}" regions="${regions}" tabindex="12"/>
                                        <p class="help-block"><spring:theme code="checkout.multi.paymentMethod.seeOrderSummaryForMoreInformation"/></p>
                                    </form:form>
                                </ycommerce:testId>
                            </c:if>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-block submit_silentOrderPostForm checkout-next"><spring:theme code="checkout.multi.paymentMethod.continue"/></button>
                </c:if>
                <c:if test="${not empty paymentInfos}">
                    <div id="savedpayments">
                        <div id="savedpaymentstitle">
                            <div class="headline">
                                <span class="headline-text"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard"/></span>
                            </div>
                        </div>
                        <div id="savedpaymentsbody">
                            <c:forEach items="${paymentInfos}" var="paymentInfo" varStatus="status">
                                <div class="saved-payment-entry">
                                    <form action="${request.contextPath}/checkout/multi/payment-method/choose" method="GET">
                                        <input type="hidden" name="selectedPaymentMethodId" value="${fn:escapeXml(paymentInfo.id)}"/>
                                            <ul>
                                                <strong>${fn:escapeXml(paymentInfo.billingAddress.firstName)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.lastName)}</strong><br/>
                                                ${fn:escapeXml(paymentInfo.cardTypeData.name)}<br/>
                                                ${fn:escapeXml(paymentInfo.cardNumber)}<br/>
                                                <spring:theme code="checkout.multi.paymentMethod.paymentDetails.expires" arguments="${fn:escapeXml(paymentInfo.expiryMonth)},${fn:escapeXml(paymentInfo.expiryYear)}"/><br/>
                                                ${fn:escapeXml(paymentInfo.billingAddress.line1)}<br/>
                                                ${fn:escapeXml(paymentInfo.billingAddress.town)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.region.isocodeShort)}<br/>
                                                ${fn:escapeXml(paymentInfo.billingAddress.postalCode)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.country.isocode)}<br/>
                                            </ul>
                                            <button type="submit" class="btn btn-primary btn-block" tabindex="${(status.count * 2) - 1}"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails"/></button>
                                    </form>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </c:if>
           </jsp:body>
        </multiCheckout:checkoutSteps>
    </div>
    <div class="col-sm-6 hidden-xs">
        <multiCheckout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>
    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>
</template:page>

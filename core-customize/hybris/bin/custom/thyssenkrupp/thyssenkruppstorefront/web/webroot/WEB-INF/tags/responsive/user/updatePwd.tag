<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="account-section">
    <div class="account-section-header no-border"><spring:theme code="text.account.profile.resetPassword"/></div>
    <div class="account-section-content">
        <div class="row">
            <div class="col-md-8">
                <form:form method="post" commandName="updatePwdForm" id="update-pwd-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <formElement:formPasswordBox idKey="password" labelKey="updatePwd.pwd" path="pwd"
                                                             inputCSS="form-control js-password-rule-check" mandatory="true"/>
                            </div>
                        </div>
                        <div class="col-md-6 password-rules--wrapper">
                            <div class="form-group">
                                <div class="password-rules display-none" data-js-password-instruction="password">
                                    <ul class="password-rules__list">
                                        <li class="password-rules__list__item has_min_length"> <spring:theme code="account.password.validation.minlength"/> </li>
                                        <li class="password-rules__list__item password-rules__list__item--text"> <spring:theme code="account.password.validation.text"/> </li>
                                        <li class="password-rules__list__item has_uppercase"> <spring:theme code="account.password.validation.uppercase"/> </li>
                                        <li class="password-rules__list__item has_lowercase"> <spring:theme code="account.password.validation.lowercase"/> </li>
                                        <li class="password-rules__list__item has_number"> <spring:theme code="account.password.validation.number"/> </li>
                                        <li class="password-rules__list__item has_special_char"> <spring:theme code="account.password.validation.specialchar"/> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <formElement:formPasswordBox idKey="confirm-password" labelKey="updatePwd.checkPwd"
                                                             path="checkPwd" inputCSS="form-control js-confirm-password-rule-check" mandatory="true"/>
                            </div>
                        </div>
                        <div class="col-md-6 password-rules--wrapper">
                            <div class="form-group">
                                <div class="password-rules display-none" data-js-password-instruction="confirm-password">
                                    <ul class="password-rules__list">
                                        <li class="password-rules__list__item has_same_password"> <spring:theme code="account.password.validation.passwordmatch"/> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row login-form-action">
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <spring:theme code="updatePwd.submit"/>
                                    </button>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-default btn-block backToHome">
                                        <spring:theme code="text.button.cancel"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.UpdatePasswordFormValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CartRestorationStrategy;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.core.exception.TokenExpiredException;
import com.thyssenkrupp.b2b.eu.facades.account.impl.DefaultTkEuB2bMyAccountFacade;
import com.thyssenkrupp.b2b.eu.facades.forgotpassword.ForgotPasswordFacade;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;

@Controller
@RequestMapping(value = "/login/pw")
public class PasswordResetPageController extends AbstractPageController {
    private static final String FORGOTTEN_PWD_TITLE = "forgottenPwd.title";

    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(PasswordResetPageController.class);

    private static final String REDIRECT_PWD_REQ_CONF = "redirect:/login/pw/request/external/conf";
    private static final String REDIRECT_LOGIN        = "redirect:/login";
    private static final String REDIRECT_HOME         = "redirect:/";
    private static final String UPDATE_PWD_CMS_PAGE   = "updatePassword";
    private static final String REDIRECT_RESEND_EMAIL = "redirect:/login/pw/request/external";

    @Resource(name = "cartRestorationStrategy")
    private CartRestorationStrategy cartRestorationStrategy;

    @Resource(name = "autoLoginStrategy")
    private AutoLoginStrategy autoLoginStrategy;

    @Resource(name = "secureTokenService")
    private SecureTokenService secureTokenService;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "updatePasswordFormValidator")
    private UpdatePasswordFormValidator updatePasswordFormValidator;

    @Resource(name = "tkEuB2bMyAccountFacade")
    private DefaultTkEuB2bMyAccountFacade tkEuB2bMyAccountFacade;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "b2bCustomerFacade")
    private DefaultB2BCustomerFacade b2bCustomerFacade;

    @Resource(name = "forgotPasswordFacade")
    private ForgotPasswordFacade forgotPasswordFacade;

    @RequestMapping(value = "/request", method = RequestMethod.GET)
    public String getPasswordRequest(final Model model) throws CMSItemNotFoundException {
        model.addAttribute(new ForgottenPwdForm());
        model.addAttribute("isPopUp", true);
        return ControllerConstants.Views.Fragments.Password.PasswordResetRequestPopup;
    }

    @RequestMapping(value = "/request", method = RequestMethod.POST)
    public String passwordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException {
        model.addAttribute("isPopUp", true);
        if (bindingResult.hasErrors()) {
            return ControllerConstants.Views.Fragments.Password.PasswordResetRequestPopup;
        } else {
            try {
                forgotPasswordFacade.forgottenPassword(form.getEmail());
            } catch (final UnknownIdentifierException unknownIdentifierException) {
                LOG.warn("Email: " + form.getEmail() + " does not exist in the database.");
            }

            return ControllerConstants.Views.Fragments.Password.ForgotPasswordValidationMessage;
        }
    }

    @RequestMapping(value = "/request/external", method = RequestMethod.GET)
    public String getExternalPasswordRequest(final Model model) throws CMSItemNotFoundException {
        model.addAttribute(new ForgottenPwdForm());
        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));
        return ControllerConstants.Views.Pages.Password.PasswordResetRequest;
    }

    @RequestMapping(value = "/request/external/conf", method = RequestMethod.GET)
    public String getExternalPasswordRequestConf(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));
        return ControllerConstants.Views.Pages.Password.PasswordResetRequestConfirmation;
    }

    @RequestMapping(value = "/request/external", method = RequestMethod.POST)
    public String externalPasswordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult, final Model model,
      final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(FORGOTTEN_PWD_TITLE));

        if (bindingResult.hasErrors()) {
            return ControllerConstants.Views.Pages.Password.PasswordResetRequest;
        } else {
            try {
                forgotPasswordFacade.forgottenPassword(form.getEmail());
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.forgotten.password.link.sent");
            } catch (final UnknownIdentifierException unknownIdentifierException) {
                LOG.warn("Email: " + form.getEmail() + " does not exist in the database.");
            }
            return REDIRECT_PWD_REQ_CONF;
        }
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public String getChangePassword(@RequestParam(required = false) final String token, final Model model) throws CMSItemNotFoundException, TokenInvalidatedException, TokenExpiredException {
        if (StringUtils.isNotBlank(token)) {
            try {
                final SecureToken secureToken = secureTokenService.decryptData(token);
                validateToken(token, secureToken);
            } catch (TokenInvalidatedException | IllegalArgumentException | TokenExpiredException ex) {
                throw ex;
            } catch (final RuntimeException exception) {
                LOG.error("Invalid token ", exception);
                return REDIRECT_PREFIX + ROOT;
            }
        } else {
            return REDIRECT_HOME;
        }
        final UpdatePwdForm form = new UpdatePwdForm();
        form.setToken(token);
        model.addAttribute(form);
        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
        return ControllerConstants.Views.Pages.Password.PasswordResetChangePage;
    }

    private void validateToken(final String token, final SecureToken data) throws TokenInvalidatedException, TokenExpiredException {

        final boolean isValidToken = tkEuB2bMyAccountFacade.validatePasswordToken(data);

        if (!isValidToken) {
            throw new TokenExpiredException("Token Expired");
        }

        final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);

        if (customer != null && !token.equals(customer.getToken())) {
            throw new TokenInvalidatedException("Invalid Token");
        }
    }

    @ExceptionHandler(TokenInvalidatedException.class)
    public String handleTokenInvalidatedException(final TokenInvalidatedException invalidTokenException) {
        LOG.error("The token does not match with user token , the token has been updated. ", invalidTokenException);
        return REDIRECT_PREFIX + ROOT;
    }

    @ExceptionHandler(TokenExpiredException.class)
    public String handleTokenExpiredException(final TokenExpiredException expiredTokenException) {
        LOG.error("Token Expired. ", expiredTokenException);
        return REDIRECT_RESEND_EMAIL;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalArgumentException(final IllegalArgumentException illegalArgumentException) {
        LOG.error("Token expired ", illegalArgumentException);
        return REDIRECT_HOME;
    }

    @RequestMapping(value = "/change", method = RequestMethod.POST)
    public String changePassword(@Valid final UpdatePwdForm form, final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel,
      final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException {
        getUpdatePasswordFormValidator().validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "form.global.error");
            return ControllerConstants.Views.Pages.Password.PasswordResetChangePage;
        }
        if (!StringUtils.isBlank(form.getToken())) {
            try {
                b2bCustomerFacade.updatePassword(form.getToken(), form.getPwd());
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.password.updated");

                final SecureToken secureToken = secureTokenService.decryptData(form.getToken());
                getAutoLoginStrategy().login(secureToken.getData(), form.getPwd(), request, response);
                cartRestorationStrategy.restoreCart(request);
            } catch (final TokenInvalidatedException e) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalidated");
            } catch (final RuntimeException e) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(e);
                }
                prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "updatePwd.pwd.invalid");
                return ControllerConstants.Views.Pages.Password.PasswordResetChangePage;
            }
        }
        return REDIRECT_HOME;
    }

    protected AutoLoginStrategy getAutoLoginStrategy() {
        return autoLoginStrategy;
    }

    protected void prepareErrorMessage(final Model model, final String page, final String messageKey) throws CMSItemNotFoundException {
        GlobalMessages.addErrorMessage(model, messageKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(page));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
    }

    public UpdatePasswordFormValidator getUpdatePasswordFormValidator() {
        return updatePasswordFormValidator;
    }

    public UserService getUserService() {
        return userService;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.cms;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.CategoryGridComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;

@Controller("CategoryGridComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CategoryGridComponent)
public class CategoryGridComponentController extends AbstractAcceleratorCMSComponentController<CategoryGridComponentModel> {

    private static final String SUB_CATEGORIES = "subCategories";

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final CategoryGridComponentModel component) {
        final SearchPageData searchPageData = getRequestContextData(request).getSearch();
        if (searchPageData instanceof ProductCategorySearchPageData) {
            final ProductCategorySearchPageData<?, ?, CategoryData> productCategorySearchPageData = (ProductCategorySearchPageData<?, ?, CategoryData>) searchPageData;
            model.addAttribute(SUB_CATEGORIES, productCategorySearchPageData.getSubCategories());
        }
    }
}


package com.thyssenkrupp.b2b.global.baseshop.storefront.security.evaluator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SecurityTraitEvaluator {
    /**
     * Evaluates a security trait.
     *
     * @return true if security trait needs to be enforced.
     */
    boolean evaluate(HttpServletRequest request, HttpServletResponse response);
}

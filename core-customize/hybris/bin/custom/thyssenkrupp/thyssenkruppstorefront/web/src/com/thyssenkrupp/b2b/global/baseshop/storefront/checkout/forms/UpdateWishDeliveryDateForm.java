/**
 *
 */
package com.thyssenkrupp.b2b.global.baseshop.storefront.checkout.forms;

import javax.validation.constraints.NotNull;

public class UpdateWishDeliveryDateForm {
    @NotNull(message = "The customer Specific Delivery Day should not be Null!")
    // private java.time.LocalDate wishDeliveryDate;
    private String wishDeliveryDate;

    /**
     * @return the wishDeliveryDate
     */
    public String getWishDeliveryDate() {
        return wishDeliveryDate;
    }

    /**
     * @param wishDeliveryDate
     *            the wishDeliveryDate to set
     */
    public void setWishDeliveryDate(final String wishDeliveryDate) {
        this.wishDeliveryDate = wishDeliveryDate;
    }

}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.tkmetadata;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapAdapter extends XmlAdapter<MapWrapper, Map<String, Object>> {
    @Override
    public Map<String, Object> unmarshal(MapWrapper v) throws Exception {
        throw new OperationNotSupportedException();
    }

    @Override
    public MapWrapper marshal(Map<String, Object> mapToMarshall) throws Exception {
        final MapWrapper xmlMapWrapper = new MapWrapper();
        List<Object> elements = new ArrayList<>();
        for (Map.Entry<String, Object> property : mapToMarshall.entrySet()) {
            elements.add(new JAXBElement<>(new QName(property.getKey()), String.class, property.getValue().toString()));
        }
        xmlMapWrapper.setElements(elements);
        return xmlMapWrapper;
    }
}

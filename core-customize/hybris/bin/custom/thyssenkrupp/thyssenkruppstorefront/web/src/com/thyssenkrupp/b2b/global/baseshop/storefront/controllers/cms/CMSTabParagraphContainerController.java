package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;

/**
 * Controller for CMSTabParagraphContainer. This controller is used for displaying the container own
 * page
 */
@Controller("CMSTabParagraphContainerController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMSTabParagraphContainer)
public class CMSTabParagraphContainerController extends AbstractAcceleratorCMSComponentController<CMSTabParagraphContainerModel> {
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final CMSTabParagraphContainerModel component) {
        model.addAttribute("components", component.getSimpleCMSComponents());
    }
}

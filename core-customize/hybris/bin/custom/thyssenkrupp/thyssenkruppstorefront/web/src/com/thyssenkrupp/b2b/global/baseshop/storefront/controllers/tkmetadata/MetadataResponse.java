package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.tkmetadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Properties;

@XmlRootElement(name = "metadata")
public class MetadataResponse {

    private Properties buildProperties;

    @XmlJavaTypeAdapter(MapAdapter.class)
    @XmlElement(name = "build.properties")
    public Properties getBuildProperties() {
        return buildProperties;
    }

    public void setBuildProperties(Properties buildProperties) {
        this.buildProperties = buildProperties;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductComparisonFacade;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.ProductCompareComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;

@Controller
@RequestMapping(value = "/product-comparison")
public class ProductComparisonController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(ProductComparisonController.class);
    private static final String COMPAREPRODUCTLISTSIZE = "thyssenkrupp.compare.productlist.size";
    private static final String PRODUCT_COMPARISON_CMS_PAGE = "ComparisonPage";

    @Resource(name = "sessionService")
    private SessionService sessionService;
    @Resource(name = "siteConfigService")
    private SiteConfigService siteConfigService;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "tkEuProductComparisonFacade")
    private TkEuProductComparisonFacade tkEuProductComparisonFacade;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String compareProduct(final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException, UnsupportedEncodingException {

        storeCmsPageInModel(model, getContentPageForLabelOrId(PRODUCT_COMPARISON_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PRODUCT_COMPARISON_CMS_PAGE));

        final ProductCompareComponentModel component = (ProductCompareComponentModel) cmsComponentService.getSimpleCMSComponent("ProductCompare");
        final String attribute = component.getProductAttributes();

        if (attribute != null) {
            final String[] attributeList = attribute.split(",");
            if (sessionService.getAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES) == null) {
                model.addAttribute("isEmpty", true);
            } else {
                final List<String> comparisonProductList = new LinkedList<String>();
                comparisonProductList.addAll(sessionService.getAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES));

                if (comparisonProductList.isEmpty()) {
                    model.addAttribute("isEmpty", true);
                }

                final Map valueMap = tkEuProductComparisonFacade.getComparisonData(comparisonProductList, attributeList);
                model.addAttribute("listData", valueMap);
            }
        } else {
            model.addAttribute("isEmpty", true);
        }
        return ControllerConstants.Views.Fragments.Product.ProductComparePage;
    }

    @RequestMapping(value = "/modifyList", method = RequestMethod.POST)
    public String modifyProductComparisonList(@RequestParam("productCode") final String productCode, final HttpServletRequest request) throws CMSItemNotFoundException, UnsupportedEncodingException {

        final List<String> comparisonProductList = new LinkedList<>();
        final int comparisonListSize = Integer.parseInt(siteConfigService.getProperty(COMPAREPRODUCTLISTSIZE));
        if (sessionService.getAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES) != null) {
            comparisonProductList.addAll(sessionService.getAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES));
            modifyCompareProductList(comparisonProductList, productCode.toUpperCase(), comparisonListSize);
        } else {
            comparisonProductList.add(productCode.toUpperCase());
            sessionService.setAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES, comparisonProductList);
        }
        return REDIRECT_PREFIX + request.getHeader("Referer");
    }

    protected void modifyCompareProductList(final List<String> comparisonProductList, final String productCode, final int comparisonListSize) {
        if (!comparisonProductList.contains(productCode) && comparisonProductList.size() < comparisonListSize) {
            comparisonProductList.add(productCode);
        } else if (comparisonProductList.contains(productCode)) {
            comparisonProductList.remove(productCode);
        }
        sessionService.setAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES, comparisonProductList);
    }

    @RequestMapping(value = "/clearList", method = RequestMethod.GET)
    public String clearProductComparisonList(final HttpServletRequest request) throws CMSItemNotFoundException, UnsupportedEncodingException {

        if (sessionService.getAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES) != null) {
            sessionService.setAttribute(ControllerConstants.SESSION_COMPARISON_PRODUCT_CODES, new LinkedList<String>());
        }
        return REDIRECT_PREFIX + request.getHeader("Referer");
    }
}

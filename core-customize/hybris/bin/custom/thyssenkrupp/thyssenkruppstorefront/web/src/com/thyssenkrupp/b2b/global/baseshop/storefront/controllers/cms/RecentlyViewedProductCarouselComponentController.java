package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.cms;

import static com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants.Actions.Cms.RecentlyViewedProductCarouselComponent;
import static com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants.Views.Cms.ComponentPrefix;
import static de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel._TYPECODE;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuLastViewedProductsFacade;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.RecentlyViewedProductCarouselComponentModel;

@Controller("RecentlyViewedProductCarouselComponentController")
@RequestMapping(value = RecentlyViewedProductCarouselComponent)
public class RecentlyViewedProductCarouselComponentController extends AbstractAcceleratorCMSComponentController<RecentlyViewedProductCarouselComponentModel> {

    private static final Logger LOG = Logger.getLogger(RecentlyViewedProductCarouselComponentController.class);

    @Resource(name = "tkEuLastViewedProductsFacade")
    private TkEuLastViewedProductsFacade tkEuLastViewedProductsFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final RecentlyViewedProductCarouselComponentModel component) {
        LOG.debug("Started populating products for RecentlyViewedProductCarouselComponent");

        final List<ProductData> lastViewedProducts = tkEuLastViewedProductsFacade.loadViewedProducts();

        model.addAttribute("title", component.getTitle());
        model.addAttribute("productData", lastViewedProducts);

    }

    @Override
    protected String getView(final RecentlyViewedProductCarouselComponentModel component) {
        return ComponentPrefix + StringUtils.lowerCase(_TYPECODE);
    }

    public TkEuLastViewedProductsFacade getTkEuLastViewedProductsFacade() {
        return tkEuLastViewedProductsFacade;
    }

    public void setTkEuLastViewedProductsFacade(final TkEuLastViewedProductsFacade lastViewedProductsFacade) {
        this.tkEuLastViewedProductsFacade = lastViewedProductsFacade;
    }
}

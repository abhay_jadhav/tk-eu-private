package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.enums.NavigationBarMenuLayout;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;

/**
 *
 */
@Controller("NavigationBarComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.NavigationBarComponent)
public class NavigationBarComponentController extends AbstractAcceleratorCMSComponentController<NavigationBarComponentModel> {
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final NavigationBarComponentModel component) {
        if (component.getDropDownLayout() != null) {
            model.addAttribute("dropDownLayout", component.getDropDownLayout().getCode().toLowerCase());
        } else if (component.getNavigationNode() != null && component.getNavigationNode().getChildren() != null && !component.getNavigationNode().getChildren().isEmpty()) {
            // Component has children but not drop down layout, default to auto
            model.addAttribute("dropDownLayout", NavigationBarMenuLayout.AUTO.getCode().toLowerCase());
        }
    }
}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages;


import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.impl.TkEuSolrProductSearchFacade;
import com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData;

@Controller
@RequestMapping(value = "/**/c")
public class CategoryPageController extends AbstractCategoryPageController {

    @Resource(name = "tkEuSolrProductSearchFacade")
    private TkEuSolrProductSearchFacade<ProductData> productSearchFacade;

    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String category(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
        @RequestParam(value = "q", required = false) final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
        @RequestParam(value = "sort", required = false) final String sortCode, @RequestParam(value = "favourites", defaultValue = "false") final boolean favourites, final Model model, final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {
        return performSearchAndGetResultsPage(categoryCode, searchQuery, page, showMode, sortCode, favourites, request, response, model);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode, @RequestParam(value = "q", required = false) final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode, @RequestParam(value = "q", required = false) final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetResultsData(categoryCode, searchQuery, page, showMode, sortCode);
    }

    protected String performSearchAndGetResultsPage(final String categoryCode, final String searchQuery, final int page, final ShowMode showMode, final String sortCode, final boolean favourites, final HttpServletRequest request, final HttpServletResponse response, final Model model) throws UnsupportedEncodingException {
        final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
        final String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        final CategoryPageModel categoryPage = getCategoryPage(category);
        final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode, sortCode, categoryPage);

        final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
        final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData =
            prepareSearchPageData(model, pageableData, favourites, categoryCode, searchQuery, categorySearch);

        prepareSiteData(showMode, request, model, category, categorySearch, searchPageData);

        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getSearchBreadcrumbBuilder().getBreadcrumbs(categoryCode, searchPageData));
        if (page == 0) {
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
        }

        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(category.getKeywords().toString());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);

        return getViewPage(categorySearch.getCategoryPage());
    }

    private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> prepareSearchPageData(final Model model, final PageableData pageableData, final boolean isFavouriteFacet, final String categoryCode, final String searchQuery, final CategorySearchEvaluator categorySearch) {
        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = null;
        final SearchStateData searchState = productSearchFacade.getSearchState(searchQuery);
        FavouriteFacetData favouriteFacetData = new FavouriteFacetData();
        try {
            if (isFavouriteFacet) {
                searchPageData = productSearchFacade.favouriteProductsCategorySearch(categoryCode, searchState, pageableData);
                favouriteFacetData = productSearchFacade.populateFavouriteFacetData(searchPageData);
            } else {
                categorySearch.doSearch();
                searchPageData = categorySearch.getSearchPageData();
                final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> favSearchPageData = getFavouriteFacetSearchPageData(categoryCode, searchPageData, searchState, pageableData);
                if (favSearchPageData != null && favSearchPageData.getPagination().getTotalNumberOfResults() > 0) {
                    favouriteFacetData = productSearchFacade.populateFavouriteFacetData(favSearchPageData);
                }
            }
            favouriteFacetData.setSelected(isFavouriteFacet);
            model.addAttribute("favouriteFacetData", favouriteFacetData);
        } catch (final ConversionException e) {
            searchPageData = createEmptySearchResult(categoryCode);
        }

        return searchPageData;
    }

    private void prepareSiteData(final ShowMode showMode, final HttpServletRequest request, final Model model, final CategoryModel category, final CategorySearchEvaluator categorySearch, final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData) {
        storeCmsPageInModel(model, categorySearch.getCategoryPage());
        storeContinueUrl(request);
        populateModel(model, searchPageData, showMode, categorySearch, category);
        updatePageTitle(category, model);
        setRequestContext(request, category, searchPageData);
    }

    private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getFavouriteFacetSearchPageData(final String categoryCode, final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData, final SearchStateData searchState, final PageableData pageableData) {
        if (productSearchFacade.shouldExecuteFavProductsSearch(searchPageData)) {
            return productSearchFacade.favouriteProductsCategorySearch(categoryCode, searchState, pageableData);
        }
        return null;
    }

    protected void populateModel(final Model model, final SearchPageData searchPageData, final ShowMode showMode, final CategorySearchEvaluator categorySearch, final CategoryModel category) {
        super.populateModel(model, searchPageData, showMode);
        final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("pageType", PageType.CATEGORY.name());
        model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());
    }

    private void setRequestContext(final HttpServletRequest request, final CategoryModel category, final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData) {
        final RequestContextData requestContextData = getRequestContextData(request);
        requestContextData.setCategory(category);
        requestContextData.setSearch(searchPageData);
    }
}

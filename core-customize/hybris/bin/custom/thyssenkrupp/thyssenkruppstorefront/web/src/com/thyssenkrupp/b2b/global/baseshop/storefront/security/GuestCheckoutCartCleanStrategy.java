package com.thyssenkrupp.b2b.global.baseshop.storefront.security;

import javax.servlet.http.HttpServletRequest;

/**
 * A strategy for clearing unwanted saved data from the cart for guest checkout.
 *
 */
public interface GuestCheckoutCartCleanStrategy {

    /**
     * Checks whether the request's page is checkout URL.
     *
     */
    boolean checkWhetherURLContainsCheckoutPattern(HttpServletRequest request);

    /**
     * Removes the delivery address, delivery mode, payment info from the session cart, if the guest
     * user moves away from checkout pages.
     *
     */
    void cleanGuestCart(HttpServletRequest request);
}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.tkmetadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "health")
public class HealthCheckResponse {
    private Boolean allOk;
    private String databaseConnection;
    private Boolean databaseConnectionOk;
    private Boolean solrConnectionOk;

    @XmlElement
    public Boolean getAllOk() {
        return allOk;
    }

    public void setAllOk(Boolean allOk) {
        this.allOk = allOk;
    }

    @XmlElement
    public String getDatabaseConnection() {
        return databaseConnection;
    }

    public void setDatabaseConnection(String databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @XmlElement
    public Boolean getDatabaseConnectionOk() {
        return databaseConnectionOk;
    }

    public void setDatabaseConnectionOk(Boolean databaseConnectionOk) {
        this.databaseConnectionOk = databaseConnectionOk;
    }

    @XmlElement
    public Boolean getSolrConnectionOk() {
        return solrConnectionOk;
    }

    public void setSolrConnectionOk(Boolean solrConnectionOk) {
        this.solrConnectionOk = solrConnectionOk;
    }
}

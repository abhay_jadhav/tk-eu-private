package com.thyssenkrupp.b2b.global.baseshop.storefront.checkout.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;

import java.util.Arrays;

public class TkAddToCartForm extends AddToCartForm {
    private String[] processingOptionsIds;

    public String[] getProcessingOptionsIds() {
        return processingOptionsIds;
    }

    public void setProcessingOptionsIds(final String[] processingOptionsIds) {
        this.processingOptionsIds = Arrays.copyOf(processingOptionsIds, processingOptionsIds.length);
    }

}

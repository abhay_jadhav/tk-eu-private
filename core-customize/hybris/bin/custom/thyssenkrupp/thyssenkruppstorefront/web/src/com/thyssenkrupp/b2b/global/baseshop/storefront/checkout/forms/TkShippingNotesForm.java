package com.thyssenkrupp.b2b.global.baseshop.storefront.checkout.forms;

public class TkShippingNotesForm {
    private String customerShippingNotes = "";

    public String getCustomerShippingNotes() {
        return customerShippingNotes;
    }

    public void setCustomerShippingNotes(final String customerShippingNotes) {
        this.customerShippingNotes = customerShippingNotes;
    }
}

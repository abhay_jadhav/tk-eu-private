package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.tkmetadata;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.healthcheck.DatabaseHealthCheck;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.health.HealthCheck.Result;
import com.thyssenkrupp.b2b.eu.core.healthcheck.SolrHealthCheck;

@RestController
@RequestMapping("/internal-api")
public class InternalApiController {

    private static final Logger LOG = Logger.getLogger(InternalApiController.class);

    @Resource(name = "siteConfigService")
    private SiteConfigService siteConfigService;
    @Resource(name = "databaseHealthCheck")
    private DatabaseHealthCheck dbHealthCheck;
    @Resource(name = "solrHealthCheck")
    private SolrHealthCheck solrhealthCheck;
    @Resource(name = "jdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    @Resource
    private ServletContext servletContext;
    private Properties metadataProperties;

    @RequestMapping(value = "/metadata", method = { RequestMethod.GET })
    public MetadataResponse getMetadata() {
        final MetadataResponse metadata = new MetadataResponse();
        try {
            if (metadataProperties == null) {
                initMetadata();
            }
            metadata.setBuildProperties(metadataProperties);
        } catch (final Exception ex) {
            LOG.error("Failed to build metadata properties XML.", ex);
        }
        return metadata;
    }

    private void initMetadata() throws IOException {
        final String filePath = siteConfigService.getProperty("thyssenkrupp.metadata.file.path");
        final InputStream propertiesFile = servletContext.getResourceAsStream(filePath);
        metadataProperties = new Properties();
        metadataProperties.load(propertiesFile);
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public HealthCheckResponse getHealthStatus() {
        final HealthCheckResponse healthStatus = new HealthCheckResponse();

        try {
            final Result dbHealth = dbHealthCheck.execute();

            healthStatus.setDatabaseConnectionOk(dbHealth.isHealthy());

            final Result solrHealth = solrhealthCheck.execute();
            healthStatus.setSolrConnectionOk(solrHealth.isHealthy());

            final String jdbcUrl = jdbcTemplate.getDataSource().getConnection().getMetaData().getConnection().getMetaData().getURL();
            healthStatus.setDatabaseConnection(jdbcUrl);

            healthStatus.setAllOk(healthStatus.getDatabaseConnectionOk() && healthStatus.getSolrConnectionOk());
        } catch (final Exception ex) {
            LOG.error("Failed to determine environment health.", ex);
            healthStatus.setAllOk(false);
        }

        return healthStatus;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages;

import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sap.security.core.server.csi.XSSEncoder;
import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.impl.TkEuSolrProductSearchFacade;
import com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData;

@Controller
@RequestMapping("/search")
public class SearchPageController extends AbstractSearchPageController {

    private static final Logger LOG                                 = Logger.getLogger(SearchPageController.class);
    private static final String SEARCH_META_DESCRIPTION_ON          = "search.meta.description.on";
    private static final String SEARCH_META_DESCRIPTION_RESULTS     = "search.meta.description.results";
    private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";
    private static final String FACET_SEPARATOR                     = ":";
    private static final String SEARCH_CMS_PAGE_ID                  = "search";
    private static final String NO_RESULTS_CMS_PAGE_ID              = "searchEmpty";

    @Resource(name = "tkEuSolrProductSearchFacade")
    private TkEuSolrProductSearchFacade<ProductData> productSearchFacade;

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "customerLocationService")
    private CustomerLocationService customerLocationService;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @RequestMapping(method = RequestMethod.GET, params = "!q")
    public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText, final HttpServletRequest request, final Model model, @RequestParam(value = "favourites", defaultValue = "false") final boolean isFavourite) throws CMSItemNotFoundException {
        if (StringUtils.isNotBlank(searchText)) {
            final String redirect = doTextSearch(searchText, request, model, isFavourite);
            if (StringUtils.isNotEmpty(redirect)) {
                return redirect;
            }
        } else {
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
        }
        model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null, SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " " + getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON, getI18nService().getCurrentLocale()) + " " + getSiteName());
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
        setUpMetaData(model, metaKeywords, metaDescription);
        return getViewForPage(model);
    }

    private String checkForMultipleSpace(final String searchText) {
        final String[] splitted = StringUtils.split(searchText);
        return splitted.length > 2 ? "\"" + searchText + "\"" : searchText;

    }
    private String doTextSearch(final String searchText, final HttpServletRequest request, final Model model, final boolean isFavourite) throws CMSItemNotFoundException {
        final PageableData pageableData = createPageableData(0, getSearchPageSize(), null, ShowMode.Page);
        String encodedSearchText = XSSFilterUtil.filter(searchText);
        encodedSearchText=checkForMultipleSpace(encodedSearchText);
        final SearchStateData searchState = productSearchFacade.getSearchState(encodedSearchText);
        ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
        searchPageData = prepareSearchPageData(model, isFavourite, pageableData, searchState, searchPageData);
        final String searchString = productSearchFacade.unescapeHtml(searchText);
        productSearchFacade.setSearchStringToSearchPageData(searchPageData, searchString);

        if (searchPageData == null) {
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
        } else if (searchPageData.getKeywordRedirectUrl() != null) {
            searchPageData.setFreeTextSearch(searchText);
            // if the search engine returns a redirect, just
            return "redirect:" + searchPageData.getKeywordRedirectUrl();
        } else {
            searchPageData.setFreeTextSearch(searchText);
            preparePageContent(request, model, encodedSearchText, searchPageData);
        }

        model.addAttribute("userLocation", customerLocationService.getUserLocation());
        getRequestContextData(request).setSearch(searchPageData);
        if (searchPageData != null) {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchString, CollectionUtils.isEmpty(searchPageData.getBreadcrumbs())));
        }
        return null;
    }

    private void preparePageContent(final HttpServletRequest request, final Model model, final String encodedSearchText, final ProductSearchPageData<SearchStateData, ProductData> searchPageData) throws CMSItemNotFoundException {
        if (searchPageData.getPagination().getTotalNumberOfResults() == 0) {
            model.addAttribute("searchPageData", searchPageData);
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
            updatePageTitle(encodedSearchText, model);
        } else {
            storeContinueUrl(request);
            populateModel(model, searchPageData, ShowMode.Page);
            storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
            updatePageTitle(encodedSearchText, model);
        }
    }

    private ProductSearchPageData<SearchStateData, ProductData> prepareSearchPageData(final Model model, final boolean isFavourite, final PageableData pageableData, final SearchStateData searchState, final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        FavouriteFacetData favouriteFacetData = new FavouriteFacetData();
        ProductSearchPageData<SearchStateData, ProductData> searchData = null;
        try {
            if (isFavourite) {
                final ProductSearchPageData<SearchStateData, ProductData> favProductsSearchPageData = getFavouriteProductsSearchPageData(searchState, pageableData);
                searchData = encodeSearchPageData(favProductsSearchPageData);
                favouriteFacetData = productSearchFacade.populateFavouriteFacetData(searchData);
            } else {
                searchData = getEncodedSearchPageData(searchState, pageableData);
                if (productSearchFacade.shouldExecuteFavProductsSearch(searchData)) {
                    final ProductSearchPageData<SearchStateData, ProductData> favProductsSearchPageData = getFavouriteProductsSearchPageData(searchState, pageableData);
                    if (favProductsSearchPageData != null && favProductsSearchPageData.getPagination().getTotalNumberOfResults() > 0) {
                        favouriteFacetData = productSearchFacade.populateFavouriteFacetData(favProductsSearchPageData);
                    }
                }
            }
            favouriteFacetData.setSelected(isFavourite);
            model.addAttribute("favouriteFacetData", favouriteFacetData);
        } catch (final ConversionException ex) {
            LOG.warn("ConversionException in SearchPageController", ex);
        }
        return searchData;
    }

    @RequestMapping(method = RequestMethod.GET, params = "q")
    public String refineSearch(@RequestParam("q") final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode, @RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request, final Model model, @RequestParam(value = "favourites", defaultValue = "false") final boolean isFavourite) throws CMSItemNotFoundException {
        final FavouriteFacetData favouriteFacetData = new FavouriteFacetData();
        final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
        final SearchStateData searchState = productSearchFacade.getSearchState(searchQuery);
        ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
        searchPageData = prepareSearchPageData(model, isFavourite, pageableData, searchState, searchPageData);
        populateModel(model, searchPageData, showMode);
        model.addAttribute("userLocation", customerLocationService.getUserLocation());

        if (productSearchFacade.getTotalNumResults(searchPageData) == 0) {
            updatePageTitle(searchPageData.getFreeTextSearch(), model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
        } else {
            storeContinueUrl(request);
            updatePageTitle(searchPageData.getFreeTextSearch(), model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
        }
        if (page == 0) {
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
        }
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));
        model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null, SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " " + getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON, getI18nService().getCurrentLocale()) + " " + getSiteName());
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
        setUpMetaData(model, metaKeywords, metaDescription);
        return getViewForPage(model);
    }

    protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page, final ShowMode showMode, final String sortCode, final int pageSize) {
        final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);
        final SearchStateData searchState = productSearchFacade.getSearchState(searchQuery);
        return getEncodedSearchPageData(searchState, pageableData);
    }

    @ResponseBody
    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> jsonSearchResults(@RequestParam("q") final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException {
        final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode, sortCode, getSearchPageSize());
        final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
        searchResultsData.setResults(searchPageData.getResults());
        searchResultsData.setPagination(searchPageData.getPagination());
        return searchResultsData;
    }

    @ResponseBody
    @RequestMapping(value = "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery, @RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException {
        final SearchStateData searchState = productSearchFacade.getSearchState(searchQuery);
        ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
        final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
        searchPageData = getSearchPageData(searchState, pageableData);
        final Map<String, FacetData<SearchStateData>> selectedFacets = convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs());
        final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(), selectedFacets);
        final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
        setRefinementData(refinement, searchPageData, facets);
        return refinement;
    }

    @ResponseBody
    @RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid, @RequestParam("term") final String term) throws CMSItemNotFoundException {
        final AutocompleteResultData resultData = new AutocompleteResultData();
        final SearchBoxComponentModel component = (SearchBoxComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);
        if (component.isDisplaySuggestions()) {
            resultData.setSuggestions(subList(productSearchFacade.getAutocompleteSuggestions(term), component.getMaxSuggestions()));
        }
        if (component.isDisplayProducts()) {
            resultData.setProducts(subList(productSearchFacade.textSearch(term, SearchQueryContext.SUGGESTIONS).getResults(), component.getMaxProducts()));
        }
        return resultData;
    }

    protected <E> List<E> subList(final List<E> list, final int maxElements) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        if (list.size() > maxElements) {
            return list.subList(0, maxElements);
        }
        return list;
    }

    protected void updatePageTitle(final String searchText, final Model model) {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(getMessageSource().getMessage("search.meta.title", null, "search.meta.title", getI18nService().getCurrentLocale()) + " " + searchText));
    }

    @Override
    protected ProductSearchPageData<SearchStateData, ProductData> encodeSearchPageData(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        final SearchStateData currentQuery = searchPageData.getCurrentQuery();

        if (currentQuery != null) {
            try {
                final SearchQueryData query = currentQuery.getQuery();
                final String encodedQueryValue = XSSEncoder.encodeHTML(query.getValue());
                query.setValue(encodedQueryValue);
                currentQuery.setQuery(query);
                searchPageData.setCurrentQuery(currentQuery);
                searchPageData.setFreeTextSearch(XSSEncoder.encodeHTML(searchPageData.getFreeTextSearch()));

                final List<FacetData<SearchStateData>> facets = searchPageData.getFacets();
                if (CollectionUtils.isNotEmpty(facets)) {
                    processFacetData(facets);
                }
            } catch (final UnsupportedEncodingException ex) {
                LOG.error("Error occured during Encoding the Search Page data values", ex);
            }
        }
        return searchPageData;
    }

    @Override
    protected void processFacetData(final List<FacetData<SearchStateData>> facets) throws UnsupportedEncodingException {
        for (final FacetData<SearchStateData> facetData : facets) {
            final List<FacetValueData<SearchStateData>> topFacetValueDatas = facetData.getTopValues();
            if (CollectionUtils.isNotEmpty(topFacetValueDatas)) {
                processFacetDatas(topFacetValueDatas);
            }
            final List<FacetValueData<SearchStateData>> facetValueDatas = facetData.getValues();
            if (CollectionUtils.isNotEmpty(facetValueDatas)) {
                processFacetDatas(facetValueDatas);
            }
        }
    }

    @Override
    protected void processFacetDatas(final List<FacetValueData<SearchStateData>> facetValueDatas) throws UnsupportedEncodingException {
        for (final FacetValueData<SearchStateData> facetValueData : facetValueDatas) {
            final SearchStateData facetQuery = facetValueData.getQuery();
            final SearchQueryData queryData = facetQuery.getQuery();
            final String queryValue = queryData.getValue();
            if (StringUtils.isNotBlank(queryValue)) {
                final String[] queryValues = queryValue.split(FACET_SEPARATOR);
                final StringBuilder queryValueBuilder = new StringBuilder();
                queryValueBuilder.append(XSSEncoder.encodeHTML(queryValues[0]));
                for (int i = 1; i < queryValues.length; i++) {
                    queryValueBuilder.append(FACET_SEPARATOR).append(queryValues[i]);
                }
                queryData.setValue(queryValueBuilder.toString());
            }
        }
    }

    protected ProductSearchPageData<SearchStateData, ProductData> getEncodedSearchPageData(final SearchStateData searchState, final PageableData pageableData) {
        return encodeSearchPageData(getSearchPageData(searchState, pageableData));
    }

    protected ProductSearchPageData<SearchStateData, ProductData> getSearchPageData(final SearchStateData searchState, final PageableData pageableData) {
        return productSearchFacade.textSearch(searchState, pageableData);
    }

    protected ProductSearchPageData<SearchStateData, ProductData> getFavouriteProductsSearchPageData(final SearchStateData searchState, final PageableData pageableData) {
        return productSearchFacade.favouriteProductsSearch(searchState, pageableData);
    }

    protected void setRefinementData(final FacetRefinement<SearchStateData> refinement, final ProductSearchPageData<SearchStateData, ProductData> searchPageData, final List<FacetData<SearchStateData>> facets) {
        refinement.setFacets(facets);
        refinement.setCount(productSearchFacade.getTotalNumResults(searchPageData));
        refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
    }
}

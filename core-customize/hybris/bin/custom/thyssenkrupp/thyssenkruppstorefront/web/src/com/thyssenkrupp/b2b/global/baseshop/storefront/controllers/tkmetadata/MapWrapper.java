package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.tkmetadata;

import javax.xml.bind.annotation.XmlAnyElement;
import java.util.List;

class MapWrapper {
    private List<Object> elements;

    @XmlAnyElement
    public List<Object> getElements() {
        return elements;
    }

    public void setElements(List<Object> elements) {
        this.elements = elements;
    }
}

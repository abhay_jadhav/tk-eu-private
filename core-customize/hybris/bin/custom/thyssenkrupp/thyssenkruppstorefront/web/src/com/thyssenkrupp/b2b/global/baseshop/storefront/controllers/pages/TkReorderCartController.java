package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages;

import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerHelper;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.api.cart.CheckoutFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

@Controller
@RequestMapping(value = "/cart")
public class TkReorderCartController extends AbstractCartPageController {

    private static final Logger LOGGER              = Logger.getLogger(TkReorderCartController.class);
    private static final String ORDER_LIST_URL      = "/my-account/orders/";
    private static final String CART_URL            = "/cart";
    private static final String ORDER_REORDER_ERROR = "order.reorder.error";

    @Resource(name = "b2bCheckoutFacade")
    private CheckoutFacade b2bCheckoutFacade;

    @Resource(name = "controllerHelper")
    private ControllerHelper controllerHelper;

    @RequestMapping(value = "/reorder", method = { RequestMethod.PUT, RequestMethod.POST })
    @RequireHardLogIn
    public String reorder(@RequestParam(value = "orderCode") final String orderCode, final RedirectAttributes redirectModel)
        throws CMSItemNotFoundException, InvalidCartException, ParseException, CommerceCartModificationException {
        try {
            b2bCheckoutFacade.createCartFromOrder(orderCode);
        } catch (final IllegalArgumentException ex) {
            LOGGER.error(String.format("Unable to reorder %s", orderCode), ex);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ORDER_REORDER_ERROR, null);
            return REDIRECT_PREFIX + ORDER_LIST_URL;
        }
        redirectModel.addFlashAttribute("reorderCart", "true");
        final boolean validateCart = validateCart(redirectModel);
        final List<String> outOfStockProducts = controllerHelper.getOutOfStockProductsFromCartModificationData(redirectModel.getFlashAttributes());
        if (validateCart && CollectionUtils.isNotEmpty(outOfStockProducts)) {
            redirectModel.addFlashAttribute("hasOutOfStockProducts", "true");
            controllerHelper.setOutOfStockGlobalFlashMessage(redirectModel, outOfStockProducts);
        }
        return REDIRECT_PREFIX + CART_URL;
    }
}

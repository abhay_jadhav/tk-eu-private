package com.thyssenkrupp.b2b.global.baseshop.storefront.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static de.hybris.platform.commerceservices.order.CommerceCartModificationStatus.NO_STOCK;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;

public class ControllerHelper {

    @SuppressWarnings("unchecked")
    public List<String> getOutOfStockProductsFromCartModificationData(final Map<String, ?> flashAttributesMap) {
        if (isNotEmpty(flashAttributesMap) && flashAttributesMap.containsKey("validationData")) {
            List<CartModificationData> validationDataList = (List<CartModificationData>) flashAttributesMap.get("validationData");
            return emptyIfNull(validationDataList).stream().filter(isNoStockModificationData()).map(s -> s.getEntry().getProduct().getName()).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public void setOutOfStockGlobalFlashMessage(final RedirectAttributes redirectModel, final List<String> outOfStockProducts) {
        if (isNotEmpty(outOfStockProducts)) {
            final String messageKey = outOfStockProducts.size() > 1 ? "cart.availability.positions.removed" : "cart.availability.position.removed";
            final String attribute = String.join(", ", outOfStockProducts);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, messageKey, new String[] { attribute });
        }
    }

    private Predicate<CartModificationData> isNoStockModificationData() {
        return s -> StringUtils.equals(NO_STOCK, s.getStatusCode()) && nonNull(s.getEntry()) && nonNull(s.getEntry().getProduct());
    }
}

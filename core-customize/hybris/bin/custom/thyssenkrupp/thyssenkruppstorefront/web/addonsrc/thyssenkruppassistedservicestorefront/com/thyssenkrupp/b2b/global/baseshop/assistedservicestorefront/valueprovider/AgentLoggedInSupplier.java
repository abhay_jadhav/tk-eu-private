package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.valueprovider;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;

import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Required;

/**
 * Boolean Supplier returning {@link Boolean#TRUE} if an assisted service agent is logged and {@link Boolean#FALSE}
 * otherwise.
 */
public class AgentLoggedInSupplier implements Supplier<Boolean> {
    private AssistedServiceFacade assistedServiceFacade;

    @Override
    public Boolean get() {
        return Boolean.valueOf(getAssistedServiceFacade().isAssistedServiceAgentLoggedIn());
    }

    protected AssistedServiceFacade getAssistedServiceFacade() {
        return assistedServiceFacade;
    }

    @Required
    public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade) {
        this.assistedServiceFacade = assistedServiceFacade;
    }
}

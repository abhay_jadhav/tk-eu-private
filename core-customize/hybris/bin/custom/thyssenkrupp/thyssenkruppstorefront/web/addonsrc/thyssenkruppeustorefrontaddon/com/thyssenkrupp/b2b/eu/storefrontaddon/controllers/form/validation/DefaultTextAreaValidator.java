package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;

public class DefaultTextAreaValidator {

    private ConfigurationService configurationService;

    public boolean validateTextAreaPattern(String fieldValue) {
        if (getConfigurationService() != null) {
            final String textAreaPattern = configurationService.getConfiguration().getString("textarea.validationPattern");
            if (StringUtils.isNotBlank(fieldValue) && StringUtils.isNotBlank(textAreaPattern)) {
                return fieldValue.matches(textAreaPattern);
            }
        }
        return true;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}

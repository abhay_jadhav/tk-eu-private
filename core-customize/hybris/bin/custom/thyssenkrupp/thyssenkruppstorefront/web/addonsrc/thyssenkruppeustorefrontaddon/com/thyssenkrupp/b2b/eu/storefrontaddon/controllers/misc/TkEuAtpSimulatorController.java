package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.misc;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.facades.atp.TkEuAtpFacade;
import com.thyssenkrupp.b2b.eu.facades.order.data.AtpViewData;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.AtpRequestDataHelper;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.core.utils.TkEuAtpDateUtils.convertDateToStringDateMonthYear;

@RestController
@RequestMapping("/api/atp-internal/simulator")
public class TkEuAtpSimulatorController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(TkEuAtpSimulatorController.class);

    @Resource(name = "atpRequestDataHelper")
    private AtpRequestDataHelper atpRequestDataHelper;

    @Resource(name = "atpFacade")
    private TkEuAtpFacade atpFacade;

    @RequestMapping(value = "/success", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> simulateAtpSuccess(){
        LOG.info("Inside simulateAtpSuccess");
        final Optional<AtpRequestData> atpRequestData = atpRequestDataHelper.buildAtpRequestData();
        if (atpRequestData.isPresent()) {
            final AtpViewData atpViewData = atpFacade.doAtpCheckAndBuildViewResponse(atpRequestData.get());
            updateAvailabilityMessage(atpViewData);
            String[] messageArgs = { atpViewData.getConsolidatedDate() };
            String consolidateDateLabel = getMessageSource().getMessage("cart.availability.position.expectedDate", messageArgs, "cart.availability.position.expectedDate", getI18nService().getCurrentLocale());
            atpViewData.setConsolidatedDateLabel(consolidateDateLabel);
            return new ResponseEntity<>(atpViewData, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/error", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> simulateAtpError() throws Exception{
        LOG.info("Inside simulateAtpError");
        throw new RuntimeException("Failing SAP Simulation");
    }

    @RequestMapping(value = "/timeout", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> simulateAtpTimeout() {
        LOG.info("Inside simulateAtpTimeout");
        try{
            Thread.sleep(6000);
        }catch(Exception e) {
            LOG.error("Error while thread sleep " +e.getMessage());
        }
        AtpViewData atpViewData = null;
        return new ResponseEntity<>(atpViewData, HttpStatus.OK);
    }

    @RequestMapping(value = "/maxthread", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> simulateAtpThreadpoolRejection() {
        LOG.info("Inside simulateAtpThreadpoolRejection");
        try{
            Thread.sleep(3800);
        }catch(Exception e) {
            LOG.error("Error while thread sleep " +e.getMessage());
        }
        AtpViewData atpViewData = null;
        return new ResponseEntity<>(atpViewData, HttpStatus.OK);
    }

    private void updateAvailabilityMessage(AtpViewData atpViewData) {
        atpViewData.getEntries().forEach(e -> {
            String availabilityMsg = e.getAvailabilityMessage();
            if (e.getDeliveryDate() != null) {
                String[] messageArgs = { convertDateToStringDateMonthYear(e.getDeliveryDate()) };
                e.setAvailabilityMessage(getMessageSource().getMessage(availabilityMsg, messageArgs, availabilityMsg, getI18nService().getCurrentLocale()));

            } else {
                e.setAvailabilityMessage(getMessageSource().getMessage(availabilityMsg, null, availabilityMsg, getI18nService().getCurrentLocale()));
            }

        });

    }
}

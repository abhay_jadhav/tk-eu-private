package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkBannerComponentModel;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

public class TkBannerComponentRenderer<C extends TkBannerComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    
    private ResponsiveMediaFacade responsiveMediaFacade;
    private CommerceCommonI18NService commerceCommonI18NService;

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        final List<ImageData> mediaDataList = getResponsiveMediaFacade().getImagesFromMediaContainer(component.getMedia(getCommerceCommonI18NService().getCurrentLocale()));
        model.put("medias", mediaDataList);
        model.put("links", component.getLinks());
        return model;
    }

    public ResponsiveMediaFacade getResponsiveMediaFacade() {
        return responsiveMediaFacade;
    }

    public void setResponsiveMediaFacade(ResponsiveMediaFacade responsiveMediaFacade) {
        this.responsiveMediaFacade = responsiveMediaFacade;
    }

    public CommerceCommonI18NService getCommerceCommonI18NService() {
        return commerceCommonI18NService;
    }

    public void setCommerceCommonI18NService(CommerceCommonI18NService commerceCommonI18NService) {
        this.commerceCommonI18NService = commerceCommonI18NService;
    }

}


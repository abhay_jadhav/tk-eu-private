/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.ContactConfirmationPageComponentModel;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

@Controller("ContactConfirmationPageComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_CONTACTCONFIRMATIONCOMPONENT)
public class ContactConfirmationPageComponentController extends AbstractCMSComponentController<ContactConfirmationPageComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, ContactConfirmationPageComponentModel component) {

        model.addAttribute("headline", component.getHeadline());
        model.addAttribute("explanationText", component.getExplanationText());
        model.addAttribute("buttonUrl", component.getButtonUrl());
    }

    @Override
    protected String getView(ContactConfirmationPageComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
            StringUtils.lowerCase(ContactConfirmationPageComponentModel._TYPECODE);
    }
}

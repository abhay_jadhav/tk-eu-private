package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusResponse;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuDocumentDownloadFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/document")
public class TkEuDocumentDownloadController {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuDocumentDownloadController.class);

    @Resource(name = "documentDownloadFacade")
    private TkEuDocumentDownloadFacade documentDownloadFacade;

    @RequestMapping(value = "/exists", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetDocumentStatusResponse documentExistsCheck(@RequestBody DocumentDownloadRequest document) throws Exception {
        return documentDownloadFacade.getDocumentStatus(document);
    }

    @RequestMapping(value = "/download", method = RequestMethod.POST)
    @ResponseBody
    public GetDocumentContentResponse documentdownload(@RequestBody DocumentDownloadRequest documentDownloadRequest, HttpServletResponse response) throws Exception {
        GetDocumentContentResponse document = null;
        try {
            document = documentDownloadFacade.getDocument(documentDownloadRequest);
        } catch (Exception e) {
            LOG.error("Exception during document download: " + e.getMessage());
        }

        return document;
    }
}


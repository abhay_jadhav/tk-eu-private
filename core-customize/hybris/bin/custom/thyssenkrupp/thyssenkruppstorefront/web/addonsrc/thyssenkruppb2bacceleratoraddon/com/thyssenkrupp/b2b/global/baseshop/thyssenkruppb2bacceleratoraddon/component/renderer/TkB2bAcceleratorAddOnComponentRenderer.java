package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.component.renderer;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.constants.Thyssenkruppb2bacceleratoraddonConstants;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

public class TkB2bAcceleratorAddOnComponentRenderer<C extends AbstractCMSComponentModel> extends DefaultAddOnCMSComponentRenderer {

    @Override
    protected String getAddonUiExtensionName(AbstractCMSComponentModel component) {
        return Thyssenkruppb2bacceleratoraddonConstants.EXTENSIONNAME;
    }
}


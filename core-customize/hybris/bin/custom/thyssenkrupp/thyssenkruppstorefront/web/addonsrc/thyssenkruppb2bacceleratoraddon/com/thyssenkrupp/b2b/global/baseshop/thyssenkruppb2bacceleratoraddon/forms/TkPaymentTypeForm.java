package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.forms;

public class TkPaymentTypeForm extends de.hybris.platform.b2bacceleratoraddon.forms.PaymentTypeForm {
    private String orderName;
    private String orderDescription;
    private String addressCode;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(final String orderName) {
        this.orderName = orderName;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(final String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(final String addressCode) {
        this.addressCode = addressCode;
    }
}

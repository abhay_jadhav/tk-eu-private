package com.thyssenkrupp.b2b.eu.storefrontaddon.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.impl.DefaultBruteForceAttackCounter;

public class TkDefaultBruteForceAttackCounter extends DefaultBruteForceAttackCounter {

    public TkDefaultBruteForceAttackCounter(Integer maxFailedLogins, Integer cacheExpiration, Integer cacheSizeLimit) {
        super(maxFailedLogins, cacheExpiration, cacheSizeLimit);
    }

    public boolean isAttack(final String userUid) {
        //This is a deprecated feature in tk schulte which is kept for backward compatibility
        return false;
    }

    public void resetUserCounter(final String userUid) {
        //This is a deprecated feature in tk schulte which is kept for backward compatibility
    }
}

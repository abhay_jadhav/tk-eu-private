package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.component.renderer;

import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonWebConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.components.ProductAddToWishListComponentModel;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

public class ThyssenkruppwishlistaddonProductAddToWishListComponentRenderer<C extends ProductAddToWishListComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    private static final String COMPONENT = "component";

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        model.put(COMPONENT, component);
        return model;
    }

    @Override
    protected String getAddonUiExtensionName(final C component) {
        return ThyssenkruppwishlistaddonWebConstants.EXTENSIONNAME;
    }
}

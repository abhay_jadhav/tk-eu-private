package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;


import com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.component.renderer.AssistedServiceComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class TkEuAssistedServiceComponentRenderer<C extends AbstractCMSComponentModel> extends AssistedServiceComponentRenderer<C> {

    private static final Logger LOG = Logger.getLogger(TkEuAssistedServiceComponentRenderer.class);

    private SessionService sessionService;
    private BaseSiteService baseSiteService;

    @Override
    public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException {

        String currentDomain = sessionService.getCurrentSession().getAttribute("asm_domain");
        List<String> asmSupportedDomainPatterns = (List<String>) baseSiteService.getCurrentBaseSite().getAsmSupportedDomainPatterns();
        Optional<String> queryResult = asmSupportedDomainPatterns.stream()
                .filter(value -> value != null)
                .filter(value -> value.equalsIgnoreCase(currentDomain))
                .findFirst();
        if (queryResult.isPresent()) {
            super.renderComponent(pageContext, component);
        } else {
            LOG.error("Could not find " + currentDomain + " in list , aborting loading of ASM Component.");
        }


    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}

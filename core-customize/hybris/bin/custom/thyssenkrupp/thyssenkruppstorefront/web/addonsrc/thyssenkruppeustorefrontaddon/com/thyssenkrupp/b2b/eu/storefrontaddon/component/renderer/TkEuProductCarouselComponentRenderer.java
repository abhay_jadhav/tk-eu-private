package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.TkEuProductSearchFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonConstants;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import javax.servlet.jsp.PageContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static de.hybris.platform.cms2.misc.CMSFilter.PREVIEW_TICKET_ID_PARAM;
import static java.util.stream.Collectors.toList;

public class TkEuProductCarouselComponentRenderer<C extends ProductCarouselComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    private static final Logger              LOG             = Logger.getLogger(TkEuProductCarouselComponentRenderer.class);
    private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC);

    private ProductFacade productFacade;

    private TkEuProductSearchFacade<ProductData> productSearchFacade;

    private SessionService sessionService;

    private CatalogVersionService catalogVersionService;

    private ProductService productService;

    private SearchRestrictionService searchRestrictionService;

    protected String getView(final C component) {
        return "/WEB-INF/views/addons/" + ThyssenkruppeustorefrontaddonConstants.EXTENSIONNAME + "/" + getUIExperienceFolder() + "/"
          + getCmsComponentFolder() + "/" + getViewResourceName(component) + ".jsp";
    }

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {

        final List<ProductData> products = new ArrayList<>();
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        products.addAll(collectLinkedProducts(component));
        products.addAll(collectSearchProducts(component));

        model.put("title", component.getTitle());
        model.put("productData", products);

        return model;
    }

    @SuppressWarnings("rawtypes")
    protected List<ProductData> collectLinkedProducts(final ProductCarouselComponentModel component) {
        final List<ProductData> products = new ArrayList<>();
        List<String> productCodeList = new ArrayList<String>();

        for (final ProductModel productModel : !isPreview() ? component.getProducts() : retrieveProductsForVersionsInSession(getUnfilterdListOfProducts(component))) {
            try {
                products.add(getProductFacade().getProductForCodeAndOptions(productModel.getCode(), PRODUCT_OPTIONS));
            } catch (Exception ex) {
                LOG.error("Exception occured for the product " + productModel.getCode(), ex);
            }
            productCodeList.add(productModel.getCode());
        }

        for (final CategoryModel categoryModel : component.getCategories()) {
            for (final ProductModel productModel : !isPreview() ? categoryModel.getProducts() : retrieveProductsForVersionsInSession(getUnfilterdListOfProducts(categoryModel))) {
                try {
                    products.add(getProductFacade().getProductForCodeAndOptions(productModel.getCode(), PRODUCT_OPTIONS));
                } catch (Exception ex) {
                    LOG.error("Exception occured for the product " + productModel.getCode(), ex);
                }
                productCodeList.add(productModel.getCode());
            }
        }
        ProductSearchPageData solrResponse = getProductCarouselAttributeFromSolr(productCodeList);
        setProductVarientRangeAttributes(solrResponse, products);
        return products;
    }

    @SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
    private void setProductVarientRangeAttributes(ProductSearchPageData solrResponse, List<ProductData> productDatas) {
        if (solrResponse != null && CollectionUtils.isNotEmpty(solrResponse.getResults())) {
            final List<ProductData> searchResults = solrResponse.getResults();
            for (ProductData productdata : productDatas) {
                for (ProductData searchResult : searchResults) {
                    if (productdata.getCode().equalsIgnoreCase(searchResult.getCode())) {
                        productdata.setTkEuPLPDisplayList(searchResult.getTkEuPLPDisplayList());
                    }
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
    private ProductSearchPageData getProductCarouselAttributeFromSolr(final List<String> productCodeList) {
        return productSearchFacade.searchByProductCodes(productCodeList);
    }

    protected List<ProductData> collectSearchProducts(final ProductCarouselComponentModel component) {
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchQueryData.setValue(component.getSearchQuery());
        final String categoryCode = component.getCategoryCode();

        if (searchQueryData.getValue() != null && categoryCode != null) {
            final SearchStateData searchState = new SearchStateData();
            searchState.setQuery(searchQueryData);

            final PageableData pageableData = new PageableData();
            pageableData.setPageSize(100); // Limit to 100 matching results

            return getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData).getResults();
        }

        return Collections.emptyList();
    }

    protected List<ProductModel> retrieveProductsForVersionsInSession(final List<ProductModel> persistentProducts) {

        if (!isPreview()) {
            return persistentProducts;
        } else {
            return getSessionService().executeInLocalView(new SessionExecutionBody() {

                @Override
                public Object execute() {
                    try {
                        getSearchRestrictionService().disableSearchRestrictions();

                        return persistentProducts.stream().map(productModel -> {

                            final String code = productModel.getCode();
                            final CatalogModel catalog = productModel.getCatalogVersion().getCatalog();
                            final CatalogVersionModel sessionCatalogVersionForCatalog = getCatalogVersionService().getSessionCatalogVersionForCatalog(catalog.getId());
                            try {
                                return getProductService().getProductForCode(sessionCatalogVersionForCatalog, code);
                            } catch (final UnknownIdentifierException e) {
                                return productModel;
                            }
                        }).collect(toList());
                    } finally {
                        getSearchRestrictionService().enableSearchRestrictions();
                    }
                }
            });
        }
    }

    protected List<ProductModel> getUnfilterdListOfProducts(final ProductCarouselComponentModel component) {

        return getSessionService().executeInLocalView(new SessionExecutionBody() {

            @Override
            public Object execute() {
                try {
                    getSearchRestrictionService().disableSearchRestrictions();
                    final ProductCarouselComponentModel refreshed = (ProductCarouselComponentModel) getModelService().get(component.getPk());
                    return refreshed.getProducts();
                } finally {
                    getSearchRestrictionService().enableSearchRestrictions();
                }
            }
        });
    }

    protected List<ProductModel> getUnfilterdListOfProducts(final CategoryModel category) {

        return getSessionService().executeInLocalView(new SessionExecutionBody() {

            @Override
            public Object execute() {
                try {
                    getSearchRestrictionService().disableSearchRestrictions();
                    final CategoryModel refreshed = (CategoryModel) getModelService().get(category.getPk());
                    return refreshed.getProducts();
                } finally {
                    getSearchRestrictionService().enableSearchRestrictions();
                }
            }
        });
    }

    protected boolean isPreview() {
        return sessionService.getAttribute(PREVIEW_TICKET_ID_PARAM) != null;
    }

    public ProductFacade getProductFacade() {
        return productFacade;
    }

    public void setProductFacade(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public TkEuProductSearchFacade<ProductData> getProductSearchFacade() {
        return productSearchFacade;
    }

    public void setProductSearchFacade(TkEuProductSearchFacade<ProductData> productSearchFacade) {
        this.productSearchFacade = productSearchFacade;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public SearchRestrictionService getSearchRestrictionService() {
        return searchRestrictionService;
    }

    public void setSearchRestrictionService(SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }
}


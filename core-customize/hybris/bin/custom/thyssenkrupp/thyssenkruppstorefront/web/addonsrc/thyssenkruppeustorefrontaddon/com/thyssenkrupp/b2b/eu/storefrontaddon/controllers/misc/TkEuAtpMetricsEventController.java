package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.misc;

import com.thyssenkrupp.b2b.eu.core.atp.event.FetchAtpMerticsEvent;
import de.hybris.platform.servicelayer.event.EventService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/atp-internal")
public class TkEuAtpMetricsEventController {

    @Resource(name = "eventService")
    private EventService eventService;

    @RequestMapping(value = "/metrics", method = RequestMethod.GET)
    public void getAtpMetrics() {
        FetchAtpMerticsEvent atpEvent = new FetchAtpMerticsEvent();
        getEventService().publishEvent(atpEvent);
    }

    public EventService getEventService() {
        return eventService;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}

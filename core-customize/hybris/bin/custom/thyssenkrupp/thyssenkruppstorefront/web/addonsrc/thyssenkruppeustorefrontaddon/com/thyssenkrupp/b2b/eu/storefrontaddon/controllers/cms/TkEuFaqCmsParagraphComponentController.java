package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.components.TkEuFaqCmsParagraphComponentModel;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller("TkEuFaqCmsParagraphComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_FAQCMSPARAGRAPHCOMPONENT)
public class TkEuFaqCmsParagraphComponentController extends AbstractCMSComponentController<TkEuFaqCmsParagraphComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, TkEuFaqCmsParagraphComponentModel component) {
        if (component != null) {
            model.addAttribute("hashedUrl", component.getHashedUrl());
            model.addAttribute("content", component.getContent());
            model.addAttribute("isExpanded", component.isIsExpanded());
            model.addAttribute("title", component.getTitle());
        }
    }

    @Override
    protected String getView(TkEuFaqCmsParagraphComponentModel component) {
        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
          StringUtils.lowerCase(TkEuFaqCmsParagraphComponentModel._TYPECODE);
    }
}

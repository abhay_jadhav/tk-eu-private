package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppb2bacceleratoraddon.controllers;

//CHECKSTYLE.OFF: ConstantName
public interface Thyssenkruppb2bacceleratoraddonControllerConstants {
    static final String ADDON_PREFIX = "addon:/thyssenkruppb2bacceleratoraddon/";
    static final String STOREFRONT_PREFIX = "/";

    interface Views {
        interface Pages {
            interface MultiStepCheckout {
                String ChoosePaymentTypePage = ADDON_PREFIX + "pages/checkout/multi/choosePaymentTypePage";
                String CheckoutSummaryPage = ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
            }

            interface Checkout {
                String CheckoutLoginPage = STOREFRONT_PREFIX + "pages/checkout/checkoutLoginPage";
                String ReadOnlyExpandedOrderForm = STOREFRONT_PREFIX + "fragments/checkout/readOnlyExpandedOrderForm";
            }

        }

        interface Fragments {

            interface Product {
                String ProductLister = ADDON_PREFIX + "fragments/product/productLister";
            }

        }
    }
}
// CHECKSTYLE.ON: ConstantName

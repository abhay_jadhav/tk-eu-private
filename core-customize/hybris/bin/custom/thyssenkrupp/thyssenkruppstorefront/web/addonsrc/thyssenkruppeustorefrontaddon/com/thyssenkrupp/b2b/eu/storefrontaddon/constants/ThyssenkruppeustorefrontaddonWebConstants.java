package com.thyssenkrupp.b2b.eu.storefrontaddon.constants;

public final class ThyssenkruppeustorefrontaddonWebConstants {

    public static final String ORDER_LIST_SORT_CODE = "orderListSortCode";
    public static final String ORDER_LIST_PAGE_NUMBER = "orderListPageNumber";
    public static final String INIT_SORT_CODE_PARAMETER = "init";
    public static final String ORDER_LIST_PAGE_PARAMETER = "page";

    private ThyssenkruppeustorefrontaddonWebConstants() {
        // empty
    }
}

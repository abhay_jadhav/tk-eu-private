package com.thyssenkrupp.b2b.eu.storefrontaddon.interceptors;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCacheBusterService;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CacheBusterViewHandler implements BeforeViewHandler {

    private TkEuCacheBusterService tkEuCacheBusterService;

    @Override
    public void beforeView(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) throws Exception {
        modelAndView.getModel().put("buildTimestamp",tkEuCacheBusterService.getBuildTimestamp());
    }

    public TkEuCacheBusterService getTkEuCacheBusterService() {
        return tkEuCacheBusterService;
    }

    public void setTkEuCacheBusterService(TkEuCacheBusterService tkEuCacheBusterService) {
        this.tkEuCacheBusterService = tkEuCacheBusterService;
    }
}

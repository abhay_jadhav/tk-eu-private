package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages.checkout.steps;

import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;

import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratoraddon.controllers.pages.checkout.steps.SummaryCheckoutStepController;
import de.hybris.platform.b2bacceleratoraddon.forms.PlaceOrderForm;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BReplenishmentRecurrenceEnum;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.cronjob.enums.DayOfWeek;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.session.SessionService;

@RequestMapping(value = "/checkout/multi/summary")
public class TkSummaryCheckoutStepController extends SummaryCheckoutStepController {
    private static final Logger LOG     = Logger.getLogger(TkSummaryCheckoutStepController.class);
    private static final String SUMMARY = "summary";

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade tkEuCheckoutFacade;

    @Resource(name = "b2bUnitFacade")
    private B2BUnitFacade b2bUnitFacade;

    @Resource
    private SessionService sessionService;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    @PreValidateQuoteCheckoutStep
    @PreValidateCheckoutStep(checkoutStep = SUMMARY)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, CommerceCartModificationException {
        final CartData cartData = getCartData();

        if (getTkEuCheckoutFacade().getNewShippingAddress().getNewShippingAddress() == null && cartData.getDeliveryAddress() == null) {

            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.deliveryAddress.notSelected");
            return REDIRECT_PREFIX + "/checkout/multi/delivery-address/choose";
        } else if (cartData.getBillingAddress() == null) {

            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.billing.address.notSelected");
            return REDIRECT_PREFIX + "/checkout/multi/billing-address/choose";
        }
        preparePlaceOrderForm(model);
        sessionService.getCurrentSession().setAttribute("cartModifiedTime",cartData.getModifiedTime());
        prepareModel(model, cartData);
        setUpModelAttributes(model, cartData);
        setUpDeliveryData(cartData);
        populateCommonModelAttributes(model, cartData, new TkShippingAddressForm());
        setTradeLengthAttributesOnOrder(cartData);
        setSawingAttributesOnOrder(cartData);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setCheckoutStepLinksForModel(model, getCheckoutStep());
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SUMMARY_PAGE;
    }

    protected void setTradeLengthAttributesOnOrder(CartData cartData) {
        for (OrderEntryData orderEntryData : cartData.getEntries()) {
            tkEuCheckoutFacade.setTradeLengthAttributes(orderEntryData);
        }
    }
    protected void setSawingAttributesOnOrder(CartData cartData) {
        for (OrderEntryData orderEntryData : cartData.getEntries()) {
            tkEuCheckoutFacade.setSawingAttributes(orderEntryData);
        }
    }

    protected void setUpModelAttributes(final Model model, final CartData cartData) throws CMSItemNotFoundException {
        setUpPaymentAndBillingData(model, cartData);
        setUpBaseModelData(model);
        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setCheckoutStepLinksForModel(model, getCheckoutStep());
    }

    private void setUpPaymentAndBillingData(final Model model, final CartData cartData) {
        model.addAttribute("billingAddresses", getTkEuCheckoutFacade().getB2bUnitBillingAddress());
        model.addAttribute("cartData", cartData);
        model.addAttribute("termsOfPayment", tkEuCheckoutFacade.checkPaymentTerms(cartData));
        model.addAttribute("incoterms", tkEuCheckoutFacade.checkIncoterms(cartData));
        model.addAttribute("poNumber", cartData.getPurchaseOrderNumber());
        model.addAttribute("orderName", cartData.getName());
        model.addAttribute("selectedBillingAddress", cartData.getBillingAddress());
    }

    protected void populateCommonModelAttributes(final Model model, final CartData cartData, final TkShippingAddressForm tkShippingAddressForm) throws CMSItemNotFoundException {

        final CartData newCartData = getTkEuCheckoutFacade().getNewShippingAddress();
        model.addAttribute("newDeliveryAddress", newCartData.getNewShippingAddress());

        model.addAttribute("deliveryAddresses", getTkEuCheckoutFacade().getB2bUnitShippingAddress());
        model.addAttribute("shippingTerms", tkEuCheckoutFacade.checkShippingTerms(cartData));
        model.addAttribute("comment", cartData.getCustomerShippingNotes());
        model.addAttribute("selectedDeliveryAddress", cartData.getDeliveryAddress());
        model.addAttribute("tkShippingAddressForm", tkShippingAddressForm);
        model.addAttribute("newAddressStatus", sessionService.getCurrentSession().getAttribute("newAddressStatus"));

        if(sessionService.getCurrentSession().getAttribute("newShippingAddressIsSavedStatus") != null){
            boolean status=sessionService.getCurrentSession().getAttribute("newShippingAddressIsSavedStatus");
            if(status){
                sessionService.getCurrentSession().setAttribute("newAddressStatus", true);
                sessionService.getCurrentSession().setAttribute("addAddressButton", true);
            }else{
                sessionService.getCurrentSession().setAttribute("newAddressStatus", false);
                sessionService.getCurrentSession().setAttribute("addAddressButton", false);
            }
        }
    }

    private void setUpBaseModelData(final Model model) {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(""));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
    }

    private void setUpPaymentData(final CartData cartData) {
        getTkEuCheckoutFacade().setDefaultPaymentInfoForCheckout();
        getTkEuCheckoutFacade().updateCheckoutCart(cartData);
    }

    private void setUpDeliveryData(final CartData cartData) {
        setUpPaymentData(cartData);
        getCheckoutFacade().setDeliveryAddressIfAvailable();
        getCheckoutFacade().setDeliveryModeIfAvailable();
    }

    private void prepareModel(Model model, CartData cartData) {
        model.addAttribute("cartData", cartData);
        model.addAttribute("allItems", cartData.getEntries());
        model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
        model.addAttribute("deliveryMode", cartData.getDeliveryMode());
        model.addAttribute("paymentInfo", cartData.getPaymentInfo());
        // TODO:Make configuration hMC-driven rather than hard coding in controller
        model.addAttribute("nDays", getNumberRange(1, 30));
        model.addAttribute("nthDayOfMonth", getNumberRange(1, 31));
        model.addAttribute("nthWeek", getNumberRange(1, 12));
        model.addAttribute("daysOfWeek", getB2BCheckoutFacade().getDaysOfWeekForReplenishmentCheckoutSummary());

        // Only request the security code if the SubscriptionPciOption is set to Default.
        final boolean requestSecurityCode = (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption()));
        model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(""));
        model.addAttribute("metaRobots", "noindex,nofollow");
    }

    private CartData getCartData() {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        if (cartData.getEntries() != null && !cartData.getEntries().isEmpty()) {
            for (final OrderEntryData entry : cartData.getEntries()) {
                final String productCode = entry.getProduct().getCode();
                final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
                    ProductOption.VARIANT_MATRIX_BASE));
                final String productUrl = entry.getProduct().getUrl();
                if (StringUtils.isNotEmpty(productUrl)) {
                    product.setUrl(productUrl);
                }
                entry.setProduct(product);
            }
        }
        return cartData;
    }

    public TkEuCheckoutFacade getTkEuCheckoutFacade() {
        return tkEuCheckoutFacade;
    }

    public B2BUnitFacade getB2bUnitFacade() {
        return b2bUnitFacade;
    }

    private void preparePlaceOrderForm(Model model) {
        if (!model.containsAttribute("placeOrderForm")) {
            final PlaceOrderForm placeOrderForm = new PlaceOrderForm();
            // TODO: Make setting of default recurrence enum value hMC-driven rather than hard coding in controller
            placeOrderForm.setReplenishmentRecurrence(B2BReplenishmentRecurrenceEnum.MONTHLY);
            placeOrderForm.setnDays("14");
            final List<DayOfWeek> daysOfWeek = new ArrayList<DayOfWeek>();
            daysOfWeek.add(DayOfWeek.MONDAY);
            placeOrderForm.setnDaysOfWeek(daysOfWeek);
            model.addAttribute("placeOrderForm", placeOrderForm);
        }
    }

    @Override
    @RequestMapping(value = "/placeOrder")
    @PreValidateQuoteCheckoutStep
    @RequireHardLogIn
    public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
      final RedirectAttributes redirectModel)
      throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException {
        if (checkCartUpdated(redirectModel))
            return REDIRECT_PREFIX + "/checkout/multi/summary/view";

        if (validateOrderForm(placeOrderForm, model)) {
            return enterStep(model, redirectModel);
        }
        // authorize, if failure occurs don't allow to place the order
        boolean isPaymentAuthorized = false;
        try {
            isPaymentAuthorized = getCheckoutFacade().authorizePayment(placeOrderForm.getSecurityCode());
        } catch (final AdapterException ae) {
            // handle a case where a wrong paymentProvider configurations on the store see getCommerceCheckoutService().getPaymentProvider()
            LOG.error(ae.getMessage(), ae);
        }
        if (!isPaymentAuthorized) {
            GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
            return enterStep(model, redirectModel);
        }
        final PlaceOrderData placeOrderData = getPlaceOrderData(placeOrderForm);

        final AbstractOrderData orderData;
        try {
            orderData = tkEuCheckoutFacade.placeOrder(placeOrderData);
        } catch (final EntityValidationException e) {
            LOG.error("Failed to place Order", e);
            return entityValidationError(placeOrderForm, model, redirectModel, e);
        } catch (final Exception e) {
            LOG.error("Failed to place Order", e);
            GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
            return enterStep(model, redirectModel);
        }

        return redirectToOrderConfirmationPage(placeOrderData, orderData);
    }

    private boolean checkCartUpdated(RedirectAttributes redirectModel) {
        if(nonNull(sessionService.getCurrentSession().getAttribute("cartModifiedTime"))){
            final Date cartModifiedTime=sessionService.getCurrentSession().getAttribute("cartModifiedTime");
            sessionService.getCurrentSession().removeAttribute("cartModifiedTime");
            final CartData cartData = tkEuCheckoutFacade.getCheckoutCart();
            if(!cartModifiedTime.equals(cartData.getModifiedTime())){
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "checkout.cart.cartUpdatedNotification");
                return true;
            }
        }
        return false;
    }

    private String entityValidationError(@ModelAttribute("placeOrderForm") PlaceOrderForm placeOrderForm, Model model, RedirectAttributes redirectModel,
      EntityValidationException e) throws CMSItemNotFoundException, CommerceCartModificationException {
        GlobalMessages.addErrorMessage(model, e.getLocalizedMessage());

        placeOrderForm.setTermsCheck(false);
        model.addAttribute(placeOrderForm);

        return enterStep(model, redirectModel);
    }

    private PlaceOrderData getPlaceOrderData(@ModelAttribute("placeOrderForm") PlaceOrderForm placeOrderForm) {
        final PlaceOrderData placeOrderData = new PlaceOrderData();
        placeOrderData.setNDays(placeOrderForm.getnDays());
        placeOrderData.setNDaysOfWeek(placeOrderForm.getnDaysOfWeek());
        placeOrderData.setNthDayOfMonth(placeOrderForm.getNthDayOfMonth());
        placeOrderData.setNWeeks(placeOrderForm.getnWeeks());
        placeOrderData.setReplenishmentOrder(placeOrderForm.isReplenishmentOrder());
        placeOrderData.setReplenishmentRecurrence(placeOrderForm.getReplenishmentRecurrence());
        placeOrderData.setReplenishmentStartDate(placeOrderForm.getReplenishmentStartDate());
        placeOrderData.setSecurityCode(placeOrderForm.getSecurityCode());
        placeOrderData.setTermsCheck(placeOrderForm.isTermsCheck());
        return placeOrderData;
    }

    protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model) {
        final String securityCode = placeOrderForm.getSecurityCode();
        boolean invalid = isInValid(model, securityCode);

        if (!placeOrderForm.isTermsCheck()) {
            GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
            return true;
        }
        final CartData cartData = tkEuCheckoutFacade.getCheckoutCart();

        if (!getCheckoutFacade().containsTaxValues()) {
            LOG.error(String.format(
              "Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
              cartData.getCode()));
            GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
            invalid = true;
        }
        if (!cartData.isCalculated()) {
            LOG.error(
              String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
            GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
            invalid = true;
        }

        return invalid;
    }

    private boolean isInValid(Model model, String securityCode) {
        if (tkEuCheckoutFacade.hasNoDeliveryAddress()) {
            GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
            return true;
        }
        if (getCheckoutFlowFacade().hasNoDeliveryMode()) {
            GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
            return true;
        }
        if (getCheckoutFlowFacade().hasNoPaymentInfo()) {
            GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
            return true;
        } else {
            // Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
            if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
              && StringUtils.isBlank(securityCode)) {
                GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
                return true;
            }
        }
        return false;
    }
}

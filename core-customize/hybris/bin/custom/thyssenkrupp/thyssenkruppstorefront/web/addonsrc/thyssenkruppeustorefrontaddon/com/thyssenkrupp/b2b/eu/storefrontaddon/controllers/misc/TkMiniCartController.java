package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.misc;

import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MINI_CART_POPUP;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuCartFacade;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.misc.MiniCartController;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

public class TkMiniCartController extends MiniCartController {

    private static final String TOTAL_DISPLAY_PATH_VARIABLE_PATTERN = "{totalDisplay:.*}";
    private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";

    @Resource(name = "cartFacade")
    private TkEuCartFacade cartFacade;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @RequestMapping(value = "/cart/rollover/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String rolloverMiniCartPopup(@PathVariable final String componentUid, final Model model) throws CMSItemNotFoundException {
        
        cartFacade.removeInactiveProductsFromCart();
        final CartData cartData = cartFacade.getSessionCart();
        model.addAttribute("cartData", cartData);

        final MiniCartComponentModel component = (MiniCartComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

        final List entries = cartData.getEntries();
        if (entries != null) {
            Collections.reverse(entries);
            model.addAttribute("isProductAdded",model.asMap().get("isProductAdded"));
            model.addAttribute("minicartClosingTime",model.asMap().get("minicartClosingTime"));
            model.addAttribute("entries", entries);
            model.addAttribute("numberItemsInCart", Integer.valueOf(entries.size()));
            model.addAttribute("forceEnableCheckout", CollectionUtils.isNotEmpty(cartData.getRootGroups()));
            if (entries.size() < component.getShownProductCount()) {
                model.addAttribute("numberShowing", Integer.valueOf(entries.size()));
            } else {
                model.addAttribute("numberShowing", Integer.valueOf(component.getShownProductCount()));
            }
        }
        model.addAttribute("lightboxBannerComponent", component.getLightboxBannerComponent());
        addAnalyticsData(model, cartData);
        return VIEWS_PAGES_MINI_CART_POPUP;
    }

    private void addAnalyticsData(final Model model, CartData cartData) {
        model.addAttribute("cartCode", cartData.getCode());
        Optional<OrderEntryData> entryData = getRecentlyUpdatedOrderEntry(model);
        if (entryData.isPresent()) {
            model.addAttribute("entry", entryData.get());
            model.addAttribute("product", entryData.get().getProduct());
        }
    }

    private Optional<OrderEntryData> getRecentlyUpdatedOrderEntry(final Model model) {
        Object orderEntryObject = model.asMap().get("entry");
        if (orderEntryObject instanceof OrderEntryData) {
            return Optional.of((OrderEntryData) orderEntryObject);
        }
        return Optional.empty();
    }

    @Override
    @RequestMapping(value = "/cart/miniCart/" + TOTAL_DISPLAY_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String getMiniCart(@PathVariable final String totalDisplay, final Model model) {
        String miniCartView = super.getMiniCart(totalDisplay, model);
        if (model != null && cartFacade.getMiniCart() != null) {
            model.addAttribute("totalItems", Integer.valueOf(cartFacade.getMiniCart().getTotalItems()));
        }
        return miniCartView;
    }
}

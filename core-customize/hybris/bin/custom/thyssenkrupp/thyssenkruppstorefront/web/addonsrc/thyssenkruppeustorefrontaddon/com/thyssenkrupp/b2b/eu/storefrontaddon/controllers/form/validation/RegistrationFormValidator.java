package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.CompleteRegistrationForm;

@Component("registrationFormValidator")
public class RegistrationFormValidator implements Validator {

    @Resource(name = "passwordPattern")
    private String passwordPattern;

    @Override
    public boolean supports(Class<?> aClass) {
        return CompleteRegistrationForm.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final CompleteRegistrationForm completeRegistrationForm = (CompleteRegistrationForm) object;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "token", "validation.registration.token.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "validation.registration.password.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "validation.registration.confirmPassword.empty");
        validateTermsAndConditions(errors, completeRegistrationForm.getTermsAndConditions());
        if (StringUtils.isNotBlank(completeRegistrationForm.getPassword()) && StringUtils.isNotBlank(completeRegistrationForm.getConfirmPassword())) {
            comparePasswords(errors, completeRegistrationForm.getPassword(), completeRegistrationForm.getConfirmPassword());
            validatePassword(errors, completeRegistrationForm.getPassword());
        }
    }

    private void validateTermsAndConditions(Errors errors, Boolean termsAndConditions) {
        if (BooleanUtils.isFalse(termsAndConditions)) {
            errors.rejectValue("termsAndConditions", "validation.registration.termsAndConditions.checked");
        }
    }

    protected void comparePasswords(final Errors errors, final String password, final String confirmPassword) {
        if (!StringUtils.equals(password, confirmPassword)) {
            errors.rejectValue("confirmPassword", "validation.registration.confirmPassword.equals");
        }
    }

    public void validatePassword(Errors errors, final String password) {
        if (StringUtils.isNotBlank(getPasswordPattern()) && !password.matches(getPasswordPattern())) {
            errors.rejectValue("password", "validation.registration.password.pattern");
        }
    }

    public String getPasswordPattern() {
        return passwordPattern;
    }

    public void setPasswordPattern(String passwordPattern) {
        this.passwordPattern = passwordPattern;
    }
}


/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages.TkEuAccountPageController.BREADCRUMBS_ATTR;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductFacade;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;
import com.thyssenkrupp.b2b.eu.storefrontaddon.builders.TkEuEnumTypeDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkEuContactForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.TkEuContactFormValidator;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.VariantAttributeSelectionHelper;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.customerticketingfacades.TicketFacade;
import de.hybris.platform.customerticketingfacades.data.TicketData;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

@Controller
public class TkEuContactFormController extends AbstractPageController {

    private static final String PRODUCT_ENQUIRY                    = "PRODUCT_ENQUIRY";
    private static final String CONTACT_FORM_CMS_PAGE              = "contact";
    private static final String CONTACT_CONFIRMATION_FORM_CMS_PAGE = "contactConfirmation";
    private static final String PAGE_UID                           = "contactDataProtection";
    private static final String OPEN_PARENTHESIS  = "(";
    private static final String CLOSE_PARENTHESIS = ")";


    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "defaultTicketFacade")
    private TicketFacade ticketFacade;

    @Resource(name = "b2bCustomerFacade")
    private TkEuB2bCustomerFacade b2bCustomerFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "tkEuContactFormValidator")
    private TkEuContactFormValidator tkEuContactFormValidator;

    @Resource(name = "b2bUnitFacade")
    private B2BUnitFacade b2bUnitFacade;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "variantAttributeSelectionHelper")
    private VariantAttributeSelectionHelper attributeSelectionHelper;

    @Resource(name = "enumUtils")
    private TkEnumUtils tkEnumUtils;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "productVariantFacade")
    private TkEuProductFacade productFacade;

    @Resource(name = "unitService")
    private UnitService unitService;

    @Resource
    private BaseSiteService baseSiteService;

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String showCompleteRegistrationPage(final Model model, final TkEuContactForm tkEuContactForm) throws CMSItemNotFoundException {

        setContactForm(tkEuContactForm);

        getCsTicketCategoryTopics(model);
        getAllTitles(model);

        setModelAttributes(model, tkEuContactForm);

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_CONTACT_FORM;
    }

    @RequestMapping(value = "/productAvailabilityRequest", method = RequestMethod.POST)
    public String requestProductAvailability(final Model model, final TkEuContactForm tkEuContactForm) throws CMSItemNotFoundException {

        final String productCode = tkEuContactForm.getProductCode();

        final ProductModel productModel = productService.getProductForCode(productCode);
        String productIdentifierString = createProductIdentifierString(productModel);
        String productVariantString = createSelectedVariantsString(productModel);
        String productInfoString = createProductInfoString(productCode, tkEuContactForm);
        String message = createMessage(productIdentifierString, productVariantString, productInfoString);

        tkEuContactForm.setMessage(message);

        tkEuContactForm.setTopic(CsTicketCategory.valueOf(PRODUCT_ENQUIRY).toString());
        setContactForm(tkEuContactForm);
        getCsTicketCategoryTopics(model);
        getAllTitles(model);

        setModelAttributes(model, tkEuContactForm);

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_CONTACT_FORM;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public String submitContactDetails(final Model model, final TkEuContactForm tkEuContactForm, final BindingResult bindingResult) throws CMSItemNotFoundException, JaloSecurityException {

        tkEuContactFormValidator.validate(tkEuContactForm, bindingResult);
        if (bindingResult.hasErrors()) {

            GlobalMessages.addErrorMessage(model, "contact.error.formentry.invalid");
            model.addAttribute("tkEuContactForm", tkEuContactForm);
            storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));
            setContactForm(tkEuContactForm);
            getAllTitles(model);
            getCsTicketCategoryTopics(model);
            createBreadcrumb(model);

            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_CONTACT_FORM;
        }

        final CsTicketModel csTicketModel = new CsTicketModel();
        final TicketData ticketData = setTicketData(tkEuContactForm);
        if (isNotBlank(tkEuContactForm.getTitle())) {
            setTitleCode(tkEuContactForm, csTicketModel);
        }
        ticketFacade.createTicket(ticketData);
        storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_CONFIRMATION_FORM_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_CONFIRMATION_FORM_CMS_PAGE));
        createBreadcrumb(model);

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_CONTACT_CONFIRMATION_FORM;
    }

    private void setModelAttributes(Model model, TkEuContactForm tkEuContactForm) throws CMSItemNotFoundException {
        model.addAttribute("tkEuContactForm", tkEuContactForm);
        storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));

        createBreadcrumb(model);
    }

    private String createMessage(String productIdentifier, String productDetail, String productInfoString) {
        String formatedProductDetail = String.join("\n", productIdentifier, productDetail, productInfoString);
        String[] localisedMessageArray = { generateLocalisedMessage("request.availability.message.salutation", null),
            generateLocalisedMessage("request.availability.message.content1", null),
            generateLocalisedMessage("request.availability.message.productdetails", new Object[] { formatedProductDetail }),
            generateLocalisedMessage("request.availability.message.content2", null) };
        return join(localisedMessageArray);
    }

    private String createProductInfoString(String productCode, TkEuContactForm tkEuContactForm) {

        final String certOption = tkEuContactForm.getCertOption();
        final String salesUom = tkEuContactForm.getSalesUom();
        final String selectedTradeLength = tkEuContactForm.getSelectedTradeLength();
        final long qty = tkEuContactForm.getQty();

        final List<ConfigurationInfoData> configurations = productFacade.getConfiguratorSettingsForCode(productCode);
        final Locale locale = getI18nService().getCurrentLocale();
        Map<String, String> productInfoMap = new HashMap<>();
        if (StringUtils.isNotEmpty(selectedTradeLength)) {
            attributeSelectionHelper.constructLocalizedTradeLengthCategoryString(selectedTradeLength, locale).ifPresent(s -> productInfoMap.put(s.getKey(), s.getValue()));
        }
        getLocalizedCutToLengthSelection(tkEuContactForm, locale).ifPresent(s -> {
            final String uomString = " " + OPEN_PARENTHESIS + tkEuContactForm.getSelectedCutToLengthRangeUnit().toLowerCase() + CLOSE_PARENTHESIS;
            productInfoMap.put(generateLocalisedMessage("request.availability.message.tradelength", null) + uomString, s);
        });

        Optional<String> certLabel = configurations.stream()
            .filter(configuration -> configuration.getUniqueId().equalsIgnoreCase(certOption))
            .map(ConfigurationInfoData::getConfigurationLabel).findFirst();

        certLabel.ifPresent(s -> productInfoMap.put(generateLocalisedMessage("request.availability.message.certificate", null), s));

        String productInfoString;
        if (StringUtils.isNotEmpty(salesUom)) {
            UnitModel unit = unitService.getUnitForCode(salesUom);
            productInfoMap.put(generateLocalisedMessage("request.availability.message.quantity", null), Long.toString(qty).concat(" "+unit.getName()));
            productInfoString = productInfoMap.entrySet().stream().map(entry -> entry.getKey() + " : " + entry.getValue()).collect(Collectors.joining("\n"));
        } else {
            productInfoMap.put(generateLocalisedMessage("request.availability.message.quantity", null), "");
            productInfoString = productInfoMap.entrySet().stream().map(entry -> entry.getKey() + " : " + entry.getValue()).collect(Collectors.joining("\n"));
        }
        return productInfoString;
    }

    private Optional<String> getLocalizedCutToLengthSelection(final TkEuContactForm form,final Locale locale){
        return attributeSelectionHelper.constructLocalizedCutToLengthCategoryString(form.getSelectedCutToLengthRange(), form.getSelectedCutToLengthTolerance(),
            form.getSelectedCutToLengthRangeUnit(), locale);
    }

    private String createSelectedVariantsString(ProductModel productModel) {
        final List<VariantValueCategoryModel> variantValueCategories = attributeSelectionHelper.getAllNonTradeLengthVariantValueCategories(productModel);
        return attributeSelectionHelper.constructLocalizedVariantValueCategoryString(variantValueCategories, getI18nService().getCurrentLocale());
    }

    private String createProductIdentifierString(ProductModel productModel) {
        List<String> valuesList = Arrays.asList(productModel.getName(getI18nService().getCurrentLocale()), " ", OPEN_PARENTHESIS, productModel.getCode(), CLOSE_PARENTHESIS);
        return String.join("", valuesList);
    }

    private String generateLocalisedMessage(String messageCode, Object[] objects) {
        return getMessageSource().getMessage(messageCode, objects, getI18nService().getCurrentLocale());
    }

    public String join(String[] arrayOfString) {
        return String.join("\n\n", arrayOfString);
    }

    private void getCsTicketCategoryTopics(Model model) {

        List<TkEuEnumTypeDto> tkEuTicketCategoryTypes = new ArrayList<>();

        baseSiteService.getCurrentBaseSite().getCsTicketCategories().forEach(csTicketCategory -> {
            TkEuEnumTypeDto tkEuEnumTypeDTO = new TkEuEnumTypeDto();
            tkEuEnumTypeDTO.setCode(csTicketCategory.getCode());
            tkEuEnumTypeDTO.setName(tkEnumUtils.getNameFromEnumCode(csTicketCategory));
            tkEuTicketCategoryTypes.add(tkEuEnumTypeDTO);
        });
        model.addAttribute("topics", tkEuTicketCategoryTypes);
    }

    private void createBreadcrumb(Model model) {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("contact.page", null, getI18nService().getCurrentLocale()), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    private TicketData setTicketData(TkEuContactForm tkEuContactForm) {
        final TicketData ticketData = new TicketData();
        ticketData.setTitle(tkEuContactForm.getTitle());
        ticketData.setFirstname(tkEuContactForm.getFirstname());
        ticketData.setLastname(tkEuContactForm.getLastname());
        ticketData.setCompany(tkEuContactForm.getCompany());
        ticketData.setEmail(tkEuContactForm.getEmail());
        ticketData.setCsTicketCategory(tkEnumUtils.getEnumerationService().getEnumerationValue(CsTicketCategory.class, tkEuContactForm.getTopic()));
        ticketData.setMessage(tkEuContactForm.getMessage());
        ticketData.setSubject(tkEnumUtils.getNameFromEnumCode(CsTicketCategory.valueOf(tkEuContactForm.getTopic())));
        ticketData.setCustomerId(StringUtils.trim(tkEuContactForm.getEmail()));
        return ticketData;
    }

    private void setContactForm(TkEuContactForm tkEuContactForm) {
        final CustomerData customerData = b2bCustomerFacade.getCurrentB2bCustomer();
        if (customerData != null) {
            tkEuContactForm.setTitle(customerData.getTitleCode());
            tkEuContactForm.setFirstname(customerData.getFirstName());
            tkEuContactForm.setLastname(customerData.getLastName());
            tkEuContactForm.setEmail(customerData.getEmail());
            setCompanyName(tkEuContactForm);
        }
    }

    private void setCompanyName(TkEuContactForm tkEuContactForm) {
        final List<AddressData> addressBooks = b2bUnitFacade.getParentUnit().getAddresses();
        if (addressBooks != null) {
            tkEuContactForm.setCompany(addressBooks.iterator().next().getCompanyName());
        }
    }

    private void getAllTitles(Model model) {
        final List<TitleData> titles = userFacade.getTitles();
        if (titles != null) {
            model.addAttribute("titles", titles);
        }
    }

    private void setTitleCode(TkEuContactForm tkEuContactForm, CsTicketModel csTicketModel) {

        final TitleModel title = new TitleModel();
        title.setCode(tkEuContactForm.getTitle());
        csTicketModel.setTitle(flexibleSearchService.getModelByExample(title));
    }

    @RequestMapping(value = "/contact/data-protection", method = RequestMethod.GET)
    public String getContactDataProtection(final Model model) throws CMSItemNotFoundException {

        final ContentPageModel pageForRequest = getContentPageForLabelOrId(PAGE_UID);
        storeCmsPageInModel(model, pageForRequest);
        setUpMetaDataForContentPage(model, pageForRequest);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CONTACT_PAGES_POPUP;
    }

    public ContentPageBreadcrumbBuilder getContentPageBreadcrumbBuilder() {
        return contentPageBreadcrumbBuilder;
    }
}

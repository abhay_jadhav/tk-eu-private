/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;

import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkGenericBannerComponentModel;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

public class TkEuGenericBannerComponentRenderer<C extends TkGenericBannerComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {

    private static final String HERO_BANNER_PREFIX = "HERO";
    private ResponsiveMediaFacade responsiveMediaFacade;
    private CommerceCommonI18NService commerceCommonI18NService;

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        final List<ImageData> mediaDataList = getResponsiveMediaFacade().getImagesFromMediaContainer(component.getMedia(getCommerceCommonI18NService().getCurrentLocale()));
        model.put("medias", mediaDataList);
        if (StringUtils.isNotEmpty(component.getRenderOption().getCode()) && component.getRenderOption().getCode().startsWith(HERO_BANNER_PREFIX))
            model.put("isHeroBanner", true);
        else
            model.put("isHeroBanner", false);
        return model;
    }

    public ResponsiveMediaFacade getResponsiveMediaFacade() {
        return responsiveMediaFacade;
    }

    public void setResponsiveMediaFacade(ResponsiveMediaFacade responsiveMediaFacade) {
        this.responsiveMediaFacade = responsiveMediaFacade;
    }

    public CommerceCommonI18NService getCommerceCommonI18NService() {
        return commerceCommonI18NService;
    }

    public void setCommerceCommonI18NService(CommerceCommonI18NService commerceCommonI18NService) {
        this.commerceCommonI18NService = commerceCommonI18NService;
    }

}


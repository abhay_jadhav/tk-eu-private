package com.thyssenkrupp.b2b.eu.storefrontaddon.forms;

import java.util.Map;

public class VariantSelectionForm {
    private String selectedVariantCategory;
    private String selectedVariantValueCategory;
    private Map<String, String> variantSelectionMap;
    private String selectedTradeLength;
    private boolean mapResetFlag;
    private String selectedCertificateId;
    private String selectedCutToLengthRange;
    private String selectedCutToLengthRangeUnit;
    private String selectedCutToLengthTolerance;
    private boolean submitTriggeredBySawing;
    private String sawingType;
    private boolean selectedCutToLengthRangeInvalid;
    private boolean selectionIgnore;

    public String getSelectedTradeLength() {
        return selectedTradeLength;
    }

    public void setSelectedTradeLength(String selectedTradeLength) {
        this.selectedTradeLength = selectedTradeLength;
    }

    public boolean isMapResetFlag() {
        return mapResetFlag;
    }

    public void setMapResetFlag(boolean mapResetFlag) {
        this.mapResetFlag = mapResetFlag;
    }

    public String getSelectedVariantCategory() {
        return selectedVariantCategory;
    }

    public void setSelectedVariantCategory(String selectedVariantCategory) {
        this.selectedVariantCategory = selectedVariantCategory;
    }

    public String getSelectedVariantValueCategory() {
        return selectedVariantValueCategory;
    }

    public void setSelectedVariantValueCategory(String selectedVariantValueCategory) {
        this.selectedVariantValueCategory = selectedVariantValueCategory;
    }

    public Map<String, String> getVariantSelectionMap() {
        return variantSelectionMap;
    }

    public void setVariantSelectionMap(Map<String, String> variantSelectionMap) {
        this.variantSelectionMap = variantSelectionMap;
    }

    public String getSelectedCertificateId() {
        return selectedCertificateId;
    }

    public void setSelectedCertificateId(String selectedCertificateId) {
        this.selectedCertificateId = selectedCertificateId;
    }

    public String getSelectedCutToLengthRange() {
        return selectedCutToLengthRange;
    }

    public void setSelectedCutToLengthRange(String selectedCutToLengthRange) {
        this.selectedCutToLengthRange = selectedCutToLengthRange;
    }

    public String getSelectedCutToLengthTolerance() {
        return selectedCutToLengthTolerance;
    }

    public void setSelectedCutToLengthTolerance(String selectedCutToLengthTolerance) {
        this.selectedCutToLengthTolerance = selectedCutToLengthTolerance;
    }

    public boolean isSubmitTriggeredBySawing() {
        return submitTriggeredBySawing;
    }

    public void setSubmitTriggeredBySawing(boolean submitTriggeredBySawing) {
        this.submitTriggeredBySawing = submitTriggeredBySawing;
    }

    public String getSawingType() {
        return sawingType;
    }

    public void setSawingType(String sawingType) {
        this.sawingType = sawingType;
    }

    public String getSelectedCutToLengthRangeUnit() {
        return selectedCutToLengthRangeUnit;
    }

    public void setSelectedCutToLengthRangeUnit(String selectedCutToLengthRangeUnit) {
        this.selectedCutToLengthRangeUnit = selectedCutToLengthRangeUnit;
    }

    public boolean isSelectedCutToLengthRangeInvalid() {
        return selectedCutToLengthRangeInvalid;
    }

    public void setSelectedCutToLengthRangeInvalid(boolean selectedCutToLengthRangeInvalid) {
        this.selectedCutToLengthRangeInvalid = selectedCutToLengthRangeInvalid;
    }

    public boolean isSelectionIgnore() {
        return selectionIgnore;
    }

    public void setSelectionIgnore(boolean selectionIgnore) {
        this.selectionIgnore = selectionIgnore;
    }

}

package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import java.util.Collections;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuUserActivationPendingException;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.LoginPageController;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

public class TkEuLoginPageController extends LoginPageController {

    private static final String SECURE_LOGINPAGE = "secureLoginPage";

    @Resource
    private UserService userService;
    @Resource
    private SessionService sessionService;

    @Override
    protected String getDefaultLoginPage(final boolean loginError, final HttpSession session, final Model model) throws CMSItemNotFoundException {
        final LoginForm loginForm = new LoginForm();
        model.addAttribute(loginForm);
        model.addAttribute(new RegisterForm());
        model.addAttribute(new GuestForm());

        final String username = (String) session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
        if (username != null) {
            session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);
        }

        loginForm.setJ_username(username);
        storeCmsPageInModel(model, getContentPageForLabelOrId(SECURE_LOGINPAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SECURE_LOGINPAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);

        final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.login", null, "header.link.login", getI18nService().getCurrentLocale()), null);
        model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));

        if (loginError) {
            if (isUserPendingActivation()) {
                GlobalMessages.addErrorMessage(model, "login.inactivatedUser.message");
                return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_SECURE_LOGIN_PAGE;
            }
            try {
                userService.getUserForUID(StringUtils.lowerCase(username));
            } catch (final AmbiguousIdentifierException | UnknownIdentifierException e) {
                model.addAttribute("loginError", Boolean.valueOf(loginError));
                GlobalMessages.addErrorMessage(model, "login.error.account.not.found.customer");
                return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_SECURE_LOGIN_PAGE;
            }
            model.addAttribute("loginError", Boolean.valueOf(loginError));
            GlobalMessages.addErrorMessage(model, "login.error.account.not.found.title");
        }
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_SECURE_LOGIN_PAGE;

    }

    private boolean isUserPendingActivation() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        TkEuUserActivationPendingException exception = null;
        boolean isUserPendingActivation = false;
        if (request.getAttribute("SPRING_SECURITY_LAST_EXCEPTION") != null && request.getAttribute("SPRING_SECURITY_LAST_EXCEPTION") instanceof TkEuUserActivationPendingException) {
            isUserPendingActivation = true;
        } else {
            HttpSession session = request.getSession();
            if (session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION") != null && session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION") instanceof TkEuUserActivationPendingException) {
                isUserPendingActivation = true;
            }
        }

        return isUserPendingActivation;
    }
}

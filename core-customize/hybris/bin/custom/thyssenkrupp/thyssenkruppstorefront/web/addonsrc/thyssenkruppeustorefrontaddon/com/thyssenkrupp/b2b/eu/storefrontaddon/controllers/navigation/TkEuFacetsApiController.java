package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.navigation;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/facets")
public class TkEuFacetsApiController extends AbstractCategoryPageController {

    private static final Logger LOG = Logger.getLogger(TkEuFacetsApiController.class);

    @RequestMapping(value = "/**/c" + CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public List<FacetData<SearchStateData>> getFacet(@PathVariable("categoryCode") final String categoryCode, @RequestParam(value = "facetName", required = false) final String facetName) throws UnsupportedEncodingException {

        final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> categoryPageData = populateSearchPageData(categoryCode, null, 0, ShowMode.Page, null);
        if (CollectionUtils.isNotEmpty(categoryPageData.getFacets()) && isValidFacetName(facetName)) {
            return categoryPageData.getFacets().stream().filter(searchStateDataFacetData -> searchStateDataFacetData.getCode().equals(facetName)).collect(Collectors.toList());
        }
        LOG.debug("Response is empty or facetName :" + facetName + " is not valid");
        return Collections.emptyList();
    }

    private boolean isValidFacetName(String facetName) {
        return StringUtils.isBlank(facetName) ? false : true;
    }
}


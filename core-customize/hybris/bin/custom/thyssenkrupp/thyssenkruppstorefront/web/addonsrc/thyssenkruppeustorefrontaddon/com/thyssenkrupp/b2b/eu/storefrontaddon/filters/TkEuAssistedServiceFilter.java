package com.thyssenkrupp.b2b.eu.storefrontaddon.filters;

import com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.filter.AssistedServiceFilter;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TkEuAssistedServiceFilter extends AssistedServiceFilter {

    private SessionService sessionService;

    @Override
    protected void doFilterInternal(final HttpServletRequest httpservletrequest, final HttpServletResponse httpservletresponse,
                                    final FilterChain filterchain) throws ServletException, IOException {

        String domain = httpservletrequest.getServerName();
        sessionService.setAttribute("asm_domain", domain);
        super.doFilterInternal(httpservletrequest, httpservletresponse, filterchain);

    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }
}

/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.ExpiredTokenComponentModel;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

@Controller("ExpiredTokenComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_EXPIREDTOKENCOMPONENT)
public class ExpiredTokenComponentController extends AbstractCMSComponentController<ExpiredTokenComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, ExpiredTokenComponentModel component) {

        model.addAttribute("headerText", component.getHeaderText());
        model.addAttribute("headerDetailText", component.getHeaderDetailText());
        model.addAttribute("button", component.getButton());
        model.addAttribute("resendEmaiUrl", component.getResendEmaiUrl());
        model.addAttribute("confirmationMessage", component.getConfirmationMessage());
    }

    @Override
    protected String getView(ExpiredTokenComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX + StringUtils.lowerCase(ExpiredTokenComponentModel._TYPECODE);
    }
}


/**
 *
 */
package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonWebConstants;

public class TkEuMyAccountPageController extends AbstractSearchPageController {

    protected static final String BREADCRUMBS_ATTR = "breadcrumbs";
    private static final String MYACCOUNT_LANDING_PAGE_TITLE = "myaccount.landingpage.title";
    private static final String MY_ACCOUNT_DASHBOARD_URL = "/my-account/dashboard";

    @Resource(name = "accountBreadcrumbBuilder")
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "baseStoreService")
    protected BaseStoreService baseStoreService;

    protected void createBreadcrumb(Model model, String cmsPage) throws CMSItemNotFoundException {

        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage(MYACCOUNT_LANDING_PAGE_TITLE, null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getContentPageForLabelOrId(cmsPage).getTitle(), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    protected PageableData getPageableData(final int page, final ShowMode showMode, final String sortCode) {
        initSessionStoredInfo();

        String storedSortCode = loadStoredSortCode(sortCode);
        if (StringUtils.isEmpty(storedSortCode)) {
            storedSortCode = sortCode;
        }
        int storedPage = page;
        Integer storedPageNumber = loadStoredPage();
        if (storedPageNumber != null) {
            storedPage = storedPageNumber.intValue();
        }

        final PageableData pageableData = createPageableData(storedPage, loadPageSize(), storedSortCode, showMode);
        return pageableData;
    }

    protected HttpServletRequest retrieveRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getRequest();
    }

    protected int loadPageSize() {
        int pageSize = 10;
        BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
        if (baseStoreModel != null && baseStoreModel.getNumberOfOrdersPerPage() != null) {
            pageSize = baseStoreModel.getNumberOfOrdersPerPage();
        }
        return pageSize;
    }

    protected void initSessionStoredInfo() {
        HttpServletRequest request = retrieveRequest();
        if (StringUtils.isNotEmpty(request.getParameter(ThyssenkruppeustorefrontaddonWebConstants.INIT_SORT_CODE_PARAMETER))) {
            getSessionService().removeAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE);
            getSessionService().removeAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER);
        }
    }

    protected String loadStoredSortCode(final String sortCode) {
        String sessionSortCode = getSessionService().getAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE);

        if (StringUtils.isNotEmpty(sessionSortCode) && sortCode == null) {
            return sessionSortCode;
        }

        if (StringUtils.isNotEmpty(sortCode)) {
            getSessionService().setAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE, sortCode);
        }

        return null;
    }

    protected Integer loadStoredPage() {
        Integer page = retrievePageParameterValue();
        Integer sessionPage = getSessionService().getAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER);

        if (sessionPage != null && page == null) {
            return sessionPage;
        }
        if (page != null) {
            getSessionService().setAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER, page);
        }

        return null;
    }

    protected void createDetailPageBreadcrumb(Model model, String cmsPage, String inputValue, String listingPageUrl, String messageKey) throws CMSItemNotFoundException {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        String[] strArray = { inputValue };
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage(MYACCOUNT_LANDING_PAGE_TITLE, null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb(listingPageUrl, getContentPageForLabelOrId(cmsPage).getTitle(), null));
        breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage(messageKey, strArray, getI18nService().getCurrentLocale()), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);

    }

    private Integer retrievePageParameterValue() {
        HttpServletRequest request = retrieveRequest();
        String page = request.getParameter(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_PARAMETER);
        if (page != null) {
            return new Integer(page);
        }

        return null;
    }

}

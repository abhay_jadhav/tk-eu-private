package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.core.model.process.SetYourPasswordComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

@Controller("SetYourPasswordComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_SETYOURPASSWORDCOMPONENT)
public class SetYourPasswordComponentController extends AbstractCMSComponentController<SetYourPasswordComponentModel> {


    @Override
    protected void fillModel(HttpServletRequest request, Model model, SetYourPasswordComponentModel component) {

        model.addAttribute("headerText", component.getHeaderText());
        model.addAttribute("headerDetailText", component.getHeaderDetailText());
        model.addAttribute("setPassword", component.getSetPassword());
        model.addAttribute("confirmPassword", component.getConfirmPassword());
        model.addAttribute("passwordValidationMessage", component.getPasswordValidationMessage());
        model.addAttribute("changePasswordButton", component.getChangePasswordButton());
    }

    @Override
    protected String getView(SetYourPasswordComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
                StringUtils.lowerCase(SetYourPasswordComponentModel._TYPECODE);
    }
}

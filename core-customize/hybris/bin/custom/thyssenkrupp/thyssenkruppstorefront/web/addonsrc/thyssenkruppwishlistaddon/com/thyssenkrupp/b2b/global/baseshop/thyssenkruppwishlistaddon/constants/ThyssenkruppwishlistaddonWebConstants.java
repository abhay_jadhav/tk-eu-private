/*
 */
package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants;

/**
 * Global class for all Thyssenkruppwishlistaddon web constants. You can add global constants for
 * your extension into this class.
 */
public final class ThyssenkruppwishlistaddonWebConstants{ // NOSONAR


    public static final String EXTENSIONNAME = "thyssenkruppwishlistaddon";
    public static final String ADDON_PREFIX = "addon:/thyssenkruppwishlistaddon";
    public static final String ADDTOWISHLISTPOPUP = "addon:/thyssenkruppwishlistaddon/fragments/addToWishListPopup";

    public static final String MY_WISH_LIST_URL = "/my-account/my-wishlist";
    private ThyssenkruppwishlistaddonWebConstants() {
        // empty to avoid instantiating this constant class
    }

}

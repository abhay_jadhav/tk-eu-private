package com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.servicelayer.security.spring.HybrisSessionFixationProtectionStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

/**
 * Implementation of login strategy for assisted service agent.
 */
public class AssistedServiceAgentLoginStrategy {
    private GUIDCookieStrategy                      guidCookieStrategy;
    private UserDetailsService                      userDetailsService;
    private HybrisSessionFixationProtectionStrategy sessionFixationStrategy;

    public void login(final String username, final HttpServletRequest request, final HttpServletResponse response) {
        final UserDetails userDetails = getUserDetailsService().loadUserByUsername(username);
        final AssistedServiceAuthenticationToken token = new AssistedServiceAuthenticationToken(
            new AssistedServiceAgentPrincipal(username), userDetails.getAuthorities());
        token.setDetails(new WebAuthenticationDetails(request));
        SecurityContextHolder.getContext().setAuthentication(token);
        getGuidCookieStrategy().setCookie(request, response);
        sessionFixationStrategy.onAuthentication(token, request, response);
    }

    protected GUIDCookieStrategy getGuidCookieStrategy() {
        return guidCookieStrategy;
    }

    @Required
    public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy) {
        this.guidCookieStrategy = guidCookieStrategy;
    }

    protected UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    @Required
    public void setUserDetailsService(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    protected HybrisSessionFixationProtectionStrategy getSessionFixationStrategy() {
        return sessionFixationStrategy;
    }

    @Required
    public void setSessionFixationStrategy(final HybrisSessionFixationProtectionStrategy sessionFixationStrategy) {
        this.sessionFixationStrategy = sessionFixationStrategy;
    }
}

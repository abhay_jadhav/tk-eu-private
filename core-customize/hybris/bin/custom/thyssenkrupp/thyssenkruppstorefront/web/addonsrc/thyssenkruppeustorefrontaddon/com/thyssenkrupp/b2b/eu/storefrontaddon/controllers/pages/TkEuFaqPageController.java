package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.core.model.TkEuFaqTitleComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import de.hybris.platform.acceleratorcms.component.slot.CMSPageSlotComponentService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class TkEuFaqPageController extends AbstractPageController {

    protected static final String BREADCRUMBS_ATTR      = "breadcrumbs";
    private static final   String CONTACT_FORM_CMS_PAGE = "faqPage";

    @Resource(name = "accountBreadcrumbBuilder")
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "cmsPageSlotComponentService")
    private CMSPageSlotComponentService cmsPageSlotComponentService;

    @RequestMapping(value = "/faq", method = RequestMethod.GET)
    public String showFaqPage(final Model model, HttpServletRequest request) throws CMSItemNotFoundException {

        final LinkedHashMap<String, String> componentDetails = new LinkedHashMap<>();

        final List<AbstractCMSComponentModel> contentSlotComponents = cmsPageSlotComponentService.getContentSlotForId("TkEuFaqSection2ContentSlot").getCmsComponents();
        for (final AbstractCMSComponentModel components : contentSlotComponents) {
            if (components.getItemtype().equals("TkEuFaqTitleComponent")) {
                componentDetails.put(components.getUid(), ((TkEuFaqTitleComponentModel) components).getContent());
            }
        }
        model.addAttribute("navComponent", componentDetails);
        createBreadcrumb(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE));

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_FAQ_PAGE;
    }

    private void createBreadcrumb(Model model) throws CMSItemNotFoundException {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("#",getContentPageForLabelOrId(CONTACT_FORM_CMS_PAGE).getTitle(),null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }
}

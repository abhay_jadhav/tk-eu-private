package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.misc;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.form.AddToWishListForm;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.TkWishlistFacade;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonWebConstants.MY_WISH_LIST_URL;

@Controller
@RequestMapping(value = "/wishlist")
public class WishListController extends AbstractPageController {

    @Resource(name = "tkWishListFacade")
    private TkWishlistFacade tkWishListFacade;

    @Resource(name = "accProductFacade")
    private ProductFacade productFacade;

    @Resource(name = "userService")
    private UserService userService;

    @RequireHardLogIn
    @RequestMapping(value = "/addWishListEntry", method = RequestMethod.POST, produces = "application/json")
    public String addToWishList(@RequestParam("productCodePost") final String code, @ModelAttribute final AddToWishListForm addToWishListForm, final RedirectAttributes model) {
        final UserModel user = userService.getCurrentUser();
        if (tkWishListFacade.hasEntryInWishList(addToWishListForm.getWishListName(), code)) {
            model.addFlashAttribute("addtoWishListResult", getMessageFromCurrentLocale("info.wishlist.product.exists"));
        } else {
            tkWishListFacade.addToWishList(addToWishListForm.getWishListName(), code, 1);
            model.addFlashAttribute("addtoWishListResult", getMessageFromCurrentLocale("info.wishlist.product.added"));
        }
        model.addFlashAttribute("wishListName", addToWishListForm.getWishListName());
        return REDIRECT_PREFIX + MY_WISH_LIST_URL;
    }

    @RequireHardLogIn
    @RequestMapping(value = "/createAndAddWishListEntry", method = RequestMethod.POST)
    public String createAndAddToWishList(@RequestParam("productCodePost") final String code, @ModelAttribute @Valid final AddToWishListForm addToWishListForm, final RedirectAttributes model) {
        final UserModel user = userService.getCurrentUser();
        tkWishListFacade.createWishList(user, addToWishListForm.getWishListName(), addToWishListForm.getWishListDescription());
        tkWishListFacade.addToWishList(addToWishListForm.getWishListName(), code, 1);
        model.addFlashAttribute("addtoWishListResult", getMessageFromCurrentLocale("info.wishlist.product.added"));
        model.addFlashAttribute("wishListName", addToWishListForm.getWishListName());
        return REDIRECT_PREFIX + MY_WISH_LIST_URL;
    }

    @RequireHardLogIn
    @RequestMapping(value = "/createModal", method = RequestMethod.POST, produces = "application/json")
    public String addToWishListModal(@RequestParam("productCodePost") final String code, final Model model, @ModelAttribute final AddToWishListForm addToWishListForm, final BindingResult bindingErrors) {
        final UserModel user = userService.getCurrentUser();
        model.addAttribute("wishListData", tkWishListFacade.getAllWishLists(user));
        model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));
        return ThyssenkruppwishlistaddonConstants.ADDTOWISHLISTPOPUP;
    }

    private String getMessageFromCurrentLocale(final @NotNull String messageKey) {
        return getMessageSource().getMessage(messageKey, null, getI18nService().getCurrentLocale());
    }
}

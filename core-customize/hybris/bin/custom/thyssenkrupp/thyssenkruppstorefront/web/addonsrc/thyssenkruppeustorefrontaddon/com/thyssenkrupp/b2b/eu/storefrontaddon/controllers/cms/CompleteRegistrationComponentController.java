/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.CompleteRegistrationComponentModel;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

@Controller("CompleteRegistrationComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_REGISTRATIONCOMPONENT)
public class CompleteRegistrationComponentController extends AbstractCMSComponentController<CompleteRegistrationComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, CompleteRegistrationComponentModel component) {

        model.addAttribute("headerText", component.getHeaderText());
        model.addAttribute("headerDetailText", component.getHeaderDetailText());
        model.addAttribute("choosePassword", component.getChoosePassword());
        model.addAttribute("confirmPassword", component.getConfirmPassword());
        model.addAttribute("termsAndConditions", component.getTermsAndConditions());
        model.addAttribute("passwordValidation", component.getPasswordValidation());
        model.addAttribute("completeRegistrationButton", component.getCompleteRegistrationButton());
    }

    @Override
    protected String getView(CompleteRegistrationComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
            StringUtils.lowerCase(CompleteRegistrationComponentModel._TYPECODE);
    }
    
}

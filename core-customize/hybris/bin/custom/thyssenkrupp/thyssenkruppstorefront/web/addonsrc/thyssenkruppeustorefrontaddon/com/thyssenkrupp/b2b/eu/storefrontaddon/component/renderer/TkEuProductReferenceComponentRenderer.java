package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonConstants;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TkEuProductReferenceComponentRenderer<C extends ProductReferencesComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC);

    private ProductFacade productVariantFacade;

    protected String getView(final C component) {
        return "/WEB-INF/views/addons/" + ThyssenkruppeustorefrontaddonConstants.EXTENSIONNAME + "/" + getUIExperienceFolder() + "/"
          + getCmsComponentFolder() + "/" + getViewResourceName(component) + ".jsp";
    }

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        final ProductModel currentProduct = getRequestContextData(request).getProduct();
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        if (currentProduct != null) {
            final List<ProductReferenceData> productReferences = getProductVariantFacade().getProductReferencesForCode(currentProduct.getCode(), component.getProductReferenceTypes(), PRODUCT_OPTIONS, component.getMaximumNumberProducts());
            model.put("component", component);
            model.put("title", component.getTitle());
            model.put("productReferences", productReferences);
        }
        return model;
    }

    public ProductFacade getProductVariantFacade() {
        return productVariantFacade;
    }

    public void setProductVariantFacade(ProductFacade productVariantFacade) {
        this.productVariantFacade = productVariantFacade;
    }
}


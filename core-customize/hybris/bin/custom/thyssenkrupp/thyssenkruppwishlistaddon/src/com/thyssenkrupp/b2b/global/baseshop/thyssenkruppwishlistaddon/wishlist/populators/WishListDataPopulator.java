package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.populators;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class WishListDataPopulator implements Populator<Wishlist2Model, WishListData> {

    private Converter<Wishlist2EntryModel, WishListEntryData> wishlistEntryConverter;

    @Override
    public void populate(Wishlist2Model wishlist2Model, WishListData wishListData) throws ConversionException {
        wishListData.setName(wishlist2Model.getName());
        wishListData.setDescription(wishlist2Model.getDescription());
        addEntries(wishlist2Model, wishListData);
    }

    protected void addEntries(final Wishlist2Model source, final WishListData target) {
        target.setEntries(getWishlistEntryConverter().convertAll(source.getEntries()));
    }

    public Converter<Wishlist2EntryModel, WishListEntryData> getWishlistEntryConverter() {
        return wishlistEntryConverter;
    }

    public void setWishlistEntryConverter(Converter<Wishlist2EntryModel, WishListEntryData> wishlistEntryConverter) {
        this.wishlistEntryConverter = wishlistEntryConverter;
    }

}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.dao.impl;

import javax.annotation.Resource;

import de.hybris.platform.wishlist2.impl.daos.impl.DefaultWishlist2Dao;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.dao.TkWishListDao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;

public class DefaultTkWishListDao extends DefaultWishlist2Dao implements TkWishListDao {

    private static final Logger LOG                   = Logger.getLogger(DefaultTkWishListDao.class.getName());
    private static final String SELECT_CLAUSE         = "SELECT {" + Wishlist2Model.PK + "} FROM {" + Wishlist2Model._TYPECODE + "} ";
    private static final String FIND_BY_USER          = " WHERE {" + Wishlist2Model.USER + "} = ?user";
    private static final String FIND_BY_USER_AND_NAME = " WHERE {" + Wishlist2Model.USER + "} = ?user AND {" + Wishlist2Model.NAME + "} = ?wishListName";
    private static final String ORDER_BY_CLAUSE       = " ORDER BY {" + Wishlist2Model.MODIFIEDTIME + "} DESC";

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Override
    public Wishlist2Model getWishListByName(UserModel user, String wishListName) {
        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SELECT_CLAUSE + FIND_BY_USER_AND_NAME);
        fQuery.addQueryParameter("user", user);
        fQuery.addQueryParameter("wishListName", wishListName);

        SearchResult result = getFlexibleSearchService().search(fQuery);
        if (result.getCount() > 1) {

            LOG.warn("More than one wishlist with Same Name defined for user " + user.getName() + ". Returning first!");
        }
        return result.getCount() > 0 ? (Wishlist2Model) result.getResult().iterator().next() : null;
    }

    @Override
    public List<Wishlist2Model> findAllWishlists(UserModel user) {
        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SELECT_CLAUSE + FIND_BY_USER + ORDER_BY_CLAUSE);
        fQuery.addQueryParameter("user", user);
        SearchResult<Wishlist2Model> result = getFlexibleSearchService().search(fQuery);
        return result.getResult();
    }

    @Override
    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Override
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}

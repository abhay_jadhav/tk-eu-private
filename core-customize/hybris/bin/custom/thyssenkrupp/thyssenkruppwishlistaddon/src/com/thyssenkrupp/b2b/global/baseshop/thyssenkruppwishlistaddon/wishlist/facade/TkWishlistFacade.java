package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade;

import java.util.List;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface TkWishlistFacade {

    void addToWishList(String wishListName, String code, long quantity);

    List<WishListData> getAllWishLists(UserModel user);

    WishListData getWishListByName(String wishListName, UserModel user);

    void createWishList(UserModel user, String wishlistName, String wishlistDescription);

    Wishlist2EntryModel getWishListEntry(ProductModel product, Wishlist2Model wishList);

    boolean hasEntryInWishList(String wishListName, String productCode);

    boolean isWishListExists(UserModel user, String wishlistName);

    boolean deleteWishList(String wishListName);

    boolean deleteEntryByProduct(String wishListName, String productCode);

    void updateWishList(String wishListIdentifier, String name, String description);

    boolean deleteAllEntries(String wishListName, List<String> unAvailableProducts);

    List<WishListEntryData> removeAllUnAvailableProducts(String wishListName, List<WishListEntryData> wishListEntryData);

    boolean isLimitExceeds();
}

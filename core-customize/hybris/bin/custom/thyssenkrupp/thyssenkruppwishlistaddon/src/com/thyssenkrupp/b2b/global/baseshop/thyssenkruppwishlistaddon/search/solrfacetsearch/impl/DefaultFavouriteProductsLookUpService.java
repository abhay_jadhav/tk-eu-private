/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.search.solrfacetsearch.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.FavouriteProductsHook;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.facade.TkFavouriteFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.impl.DefaultTkWishListFacade;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

public class DefaultFavouriteProductsLookUpService implements FavouriteProductsHook {

    private static final Logger LOG = Logger.getLogger(DefaultFavouriteProductsLookUpService.class);

    private UserService             userService;
    private TkFavouriteFacade       tkFavouriteFacade;
    private DefaultTkWishListFacade tkWishListFacade;

    @Override
    public List<String> getFavouriteProducts() {
        final UserModel user = getUserService().getCurrentUser();
        LOG.debug("***** User ID  : " + user.getUid() + " *****");
        if (checkFavourite(user) != null) {
            final WishListData favouriteWishList = getTkWishListFacade().getWishListByName(ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME, user);
            return getFavoiteProductCodes(favouriteWishList);
        }
        return Collections.emptyList();
    }

    public List<String> getFavoiteProductCodes(final WishListData favoriteWishList) {
        List<String> favoriteProductCodes = new ArrayList<>();
        if (favoriteWishList != null) {
            final List<WishListEntryData> favWishListEntries = favoriteWishList.getEntries();
            for (WishListEntryData favWishListEntry : favWishListEntries) {
                if (favWishListEntry.getProduct() != null) {
                    favoriteProductCodes.add(favWishListEntry.getProduct().getCode().toLowerCase());
                }
            }
        }
        LOG.debug(CollectionUtils.isEmpty(favoriteProductCodes) ? "User does not have favoriteProducts" : "User has favorite Products");
        return favoriteProductCodes;
    }

    private TkFavouriteModel checkFavourite(final UserModel user) {
        return getTkFavouriteFacade().checkFavourite(user);
    }

    protected UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected TkFavouriteFacade getTkFavouriteFacade() {
        return tkFavouriteFacade;
    }

    @Required
    public void setTkFavouriteFacade(TkFavouriteFacade tkFavouriteFacade) {
        this.tkFavouriteFacade = tkFavouriteFacade;
    }

    protected DefaultTkWishListFacade getTkWishListFacade() {
        return tkWishListFacade;
    }

    public void setTkWishListFacade(DefaultTkWishListFacade tkWishListFacade) {
        this.tkWishListFacade = tkWishListFacade;
    }
}

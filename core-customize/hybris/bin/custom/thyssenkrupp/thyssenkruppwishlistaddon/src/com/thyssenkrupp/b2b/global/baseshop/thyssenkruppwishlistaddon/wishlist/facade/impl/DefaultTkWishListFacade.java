package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.TkWishlistFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.service.TkWishListService;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class DefaultTkWishListFacade implements TkWishlistFacade {

    protected static final Logger LOG = Logger.getLogger(DefaultTkWishListFacade.class);

    @Resource(name = "tkWishListService")
    private TkWishListService tkWishListService;

    private CatalogVersionService catalogVersionService;

    private ProductService productService;

    private UserService userService;

    private Converter<Wishlist2Model, WishListData> wishlistConverter;
    private Converter<Wishlist2Model, WishListData> wishlistDataConverter;
    private int                                     wishListLimitPerUser;

    @Override
    public void addToWishList(String wishListName, final String code, final long quantity) {
        final ProductModel product = getProductService().getProductForCode(code);
        final UserModel user = userService.getCurrentUser();
        tkWishListService.addToWishlist(wishListName, user, product, 1, Wishlist2EntryPriority.MEDIUM, "");
    }

    @Override
    public List<WishListData> getAllWishLists(UserModel user) {

        List<Wishlist2Model> wishlists = tkWishListService.getWishListsWithoutFavourite(user);

        if (CollectionUtils.isNotEmpty(wishlists)) {
            return getWishlistConverter().convertAll(wishlists);
        }
        return Collections.emptyList();
    }

    @Override
    public WishListData getWishListByName(String wishListName, UserModel user) {
        Wishlist2Model wishList = getTkWishListService().getWishListByName(wishListName, user);
        return getWishlistDataConverter().convert(wishList);
    }

    @Override
    public Wishlist2EntryModel getWishListEntry(ProductModel product, Wishlist2Model wishList) {
        return getTkWishListService().getWishListEntry(product, wishList);
    }

    @Override
    public boolean hasEntryInWishList(String wishListName, String productCode) {
        final ProductModel product = getProductService().getProductForCode(productCode);
        final Wishlist2Model wishList = getWishListModelByName(wishListName);
        return tkWishListService.checkEntryExistsInWishList(product, wishList);
    }

    @Override
    public void createWishList(UserModel user, String wishListName, String wishListDescription) {
        tkWishListService.createWishList(user, wishListName, wishListDescription);
    }

    @Override
    public boolean deleteWishList(final String wishListName) {
        final Wishlist2Model wishList = getWishListModelByName(wishListName);
        if (wishList != null) {
            return getTkWishListService().deleteWishList(wishList);
        }
        return false;
    }

    @Override
    public boolean deleteEntryByProduct(final String wishListName, final String productCode) {
        final ProductModel product = getProductService().getProductForCode(productCode);
        final Wishlist2Model wishList = getWishListModelByName(wishListName);
        return getTkWishListService().deleteEntryByProduct(product, wishList);
    }

    @Override
    public void updateWishList(final String wishListIdentifier, final String name, final String description) {
        final Wishlist2Model wishList = getWishListModelByName(wishListIdentifier);
        if (wishList != null) {
            wishList.setName(name);
            wishList.setDescription(description);
            getTkWishListService().updateWishList(wishList);
        }
    }

    @Override
    public boolean deleteAllEntries(final String wishListName, final List<String> unAvailableProducts) {
        final Wishlist2Model wishList = getWishListModelByName(wishListName);
        wishList.getEntries().forEach(wishlist2EntryModel -> {
            if (unAvailableProducts.contains(wishlist2EntryModel.getProduct().getCode())) {
                getTkWishListService().removeWishListEntry(wishList, wishlist2EntryModel);
            }
        });
        return false;
    }

    @Override
    public List<WishListEntryData> removeAllUnAvailableProducts(final String wishListName, final @NotNull List<WishListEntryData> wishListEntryData) {
        final List<WishListEntryData> unAvailableEntries = new ArrayList<>();
        wishListEntryData.forEach(wishListEntry -> {
            String productCode = wishListEntry.getProduct().getCode();
            try {
                ProductModel productModel = getProductService().getProductForCode(productCode);
                CatalogVersionModel catalogVersion = productModel.getCatalogVersion();
                getCatalogVersionService().getSessionCatalogVersionForCatalog(catalogVersion.getCatalog().getId());
            } catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
                LOG.warn(String.format("Product \"%s\" is no longer available in the current catalog version and removing from wishlist \"%s\"", productCode, wishListName));
                unAvailableEntries.add(wishListEntry);
            } catch (Exception ex) {
                LOG.warn(String.format("Product \"%s\" is no longer available and removing from wishlist \"%s\"", productCode, wishListName));
                unAvailableEntries.add(wishListEntry);
            }
        });
        List<String> products = unAvailableEntries.stream().map(wishListStreamData -> wishListStreamData.getProduct().getCode()).collect(Collectors.toList());
        deleteAllEntries(wishListName, products);
        return unAvailableEntries;
    }

    @Override
    public boolean isLimitExceeds() {
        final UserModel user = getUserService().getCurrentUser();
        List<Wishlist2Model> wishLists = getTkWishListService().getWishLists(user);
        if (CollectionUtils.isNotEmpty(wishLists) && wishLists.size() >= getWishListLimitPerUser()) {
            return true;
        }
        return false;
    }

    private Wishlist2Model getWishListModelByName(final String wishListName) {
        final UserModel user = getUserService().getCurrentUser();
        return getTkWishListService().getWishListByName(wishListName, user);
    }

    public int getWishListLimitPerUser() {
        return wishListLimitPerUser;
    }

    public void setWishListLimitPerUser(int wishListLimitPerUser) {
        this.wishListLimitPerUser = wishListLimitPerUser;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    protected ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    @Override
    public boolean isWishListExists(UserModel user, String wishlistName) {
        return tkWishListService.isWishListExists(user, wishlistName);
    }

    public Converter<Wishlist2Model, WishListData> getWishlistConverter() {
        return wishlistConverter;
    }

    @Required
    public void setWishlistConverter(Converter<Wishlist2Model, WishListData> wishlistConverter) {
        this.wishlistConverter = wishlistConverter;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Converter<Wishlist2Model, WishListData> getWishlistDataConverter() {
        return wishlistDataConverter;
    }

    public void setWishlistDataConverter(Converter<Wishlist2Model, WishListData> wishlistDataConverter) {
        this.wishlistDataConverter = wishlistDataConverter;
    }

    public TkWishListService getTkWishListService() {
        return tkWishListService;
    }

    public void setTkWishListService(TkWishListService tkWishListService) {
        this.tkWishListService = tkWishListService;
    }
}

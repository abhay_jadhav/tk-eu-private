package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.service;

import java.util.List;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface TkWishListService {

    List<Wishlist2Model> getWishLists(UserModel user);

    void createWishList(UserModel user, String wishlistName, String wishlistDescription);

    void addToWishlist(String wishListName, UserModel user, ProductModel product, Integer desiredQuantity, Wishlist2EntryPriority priority, String comment);

    boolean checkEntryExistsInWishList(ProductModel product, Wishlist2Model wishList);

    Wishlist2Model getWishListByName(String wishListName, UserModel user);

    Wishlist2EntryModel getWishListEntry(ProductModel product, Wishlist2Model wishList);

    boolean isWishListExists(UserModel user, String wishListName);

    List<Wishlist2Model> getWishListsWithoutFavourite(UserModel user);

    boolean deleteWishList(Wishlist2Model wishlist2Model);

    boolean deleteEntryByProduct(ProductModel product, Wishlist2Model wishlist2Model);

    void updateWishList(Wishlist2Model wishList);

    boolean removeWishListEntry(Wishlist2Model wishList, Wishlist2EntryModel wishlist2EntryModel);
}

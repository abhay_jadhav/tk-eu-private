package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.service;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface TkFavouriteService {

    TkFavouriteModel checkFavourite(UserModel user);
    void createFavourite(UserModel user, String favourite);
    boolean checkEntryExistsInFavourite(ProductModel product, Wishlist2Model wishList); 
    void removeFavouriteEntryForProduct(ProductModel product, Wishlist2Model wishlist);
}

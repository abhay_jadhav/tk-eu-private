package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.dao.TkFavouriteDao;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.service.TkFavouriteService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.Constants.USER;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class DefaultTkFavouriteService implements TkFavouriteService {

    @Resource(name = "tkFavouriteDao")
    private TkFavouriteDao tkFavouriteDao;

    @Resource(name = "modelService")
    private ModelService modelService;

    protected boolean checkUser(UserModel user) {
        if (user == null) {
            return false;
        } else {
            boolean anonymous = USER.ANONYMOUS_CUSTOMER.equals(user.getUid());
            return !anonymous;
        }
    }

    @Override
    public TkFavouriteModel checkFavourite(UserModel user) {
        return tkFavouriteDao.checkFavourite(user);
    }

    @Override
    public void createFavourite(UserModel user, String favourite) {
        TkFavouriteModel tkFavModel = new TkFavouriteModel();
        tkFavModel.setName(favourite);
        tkFavModel.setFavourite(Boolean.TRUE);
        tkFavModel.setUser(user);
        if (checkUser(user)) {
            modelService.save(tkFavModel);
        }
    }

    public boolean checkEntryExistsInFavourite(ProductModel product, Wishlist2Model wishList) {
        boolean isEntryExists = true;
        Wishlist2EntryModel entryModel = getFavouriteEntryForProduct(product, wishList);
        if (entryModel == null) {
            isEntryExists = false;
        }
        return isEntryExists;
    }

    public Wishlist2EntryModel getFavouriteEntryForProduct(ProductModel product, Wishlist2Model wishlist) {
        ServicesUtil.validateParameterNotNull(product, "Parameter \'product\' was null.");
        ServicesUtil.validateParameterNotNull(wishlist, "Parameter \'wishlist\' was null.");
        List<Wishlist2EntryModel> entries = tkFavouriteDao.findFavouriteEntryByProduct(product, wishlist);
        if (!entries.isEmpty()) {
            return (Wishlist2EntryModel) entries.iterator().next();
        }
        return null;
    }

    public void removeFavouriteEntryForProduct(ProductModel product, Wishlist2Model wishlist) {
        Wishlist2EntryModel entry = getFavouriteEntryForProduct(product, wishlist);
        this.removeFavouriteEntry(wishlist, entry);
    }

    public void removeFavouriteEntry(Wishlist2Model wishlist, Wishlist2EntryModel entry) {
        ArrayList<Wishlist2EntryModel> entries = new ArrayList<>(wishlist.getEntries());
        entries.remove(entry);
        wishlist.setEntries(entries);
        if (this.checkUser(wishlist.getUser())) {
            modelService.save(wishlist);
        }
    }

}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.dao.TkWishListDao;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.service.TkWishListService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import javax.validation.constraints.NotNull;

public class DefaultTkWishListService implements TkWishListService {

    private static final Logger LOG = Logger.getLogger(DefaultTkWishListService.class);

    private Wishlist2Service wishlistService;

    @Resource(name = "tkWishListDao")
    private TkWishListDao tkWishListDao;

    @Resource(name = "modelService")
    private ModelService modelService;

    @Override
    public List<Wishlist2Model> getWishLists(UserModel user) {
        return getTkWishListDao().findAllWishlists(user);
    }

    public List<Wishlist2Model> getWishListsWithoutFavourite(UserModel user) {
        List<Wishlist2Model> wishLists = getWishLists(user);
        return removeFavourite(wishLists);
    }

    @Override
    public void createWishList(UserModel user, String wishlistName, String wishlistDescription) {
        wishlistService.createWishlist(user, wishlistName, wishlistDescription);
    }

    @Override
    public void addToWishlist(String wishListName, UserModel user, ProductModel product, Integer desiredQuantity, Wishlist2EntryPriority priority, String comment) {
        Wishlist2Model wishlist2Model = this.getWishListByName(wishListName, user);
        wishlistService.addWishlistEntry(wishlist2Model, product, desiredQuantity, priority, comment);
        modelService.save(wishlist2Model);
    }

    @Override
    public boolean checkEntryExistsInWishList(ProductModel product, Wishlist2Model wishList) {
        boolean isEntryExists = true;
        try {
            wishlistService.getWishlistEntryForProduct(product, wishList);
        } catch (UnknownIdentifierException uie) {
            LOG.error("Duplicate WishList Entry Found ", uie);
            isEntryExists = false;
        } catch (AmbiguousIdentifierException aie) {
            LOG.error("Ambiguous WishList Entry Found ", aie);
        }
        return isEntryExists;
    }

    @Override
    public Wishlist2EntryModel getWishListEntry(ProductModel product, Wishlist2Model wishList) {
        return wishlistService.getWishlistEntryForProduct(product, wishList);
    }

    @Override
    public Wishlist2Model getWishListByName(String wishListName, UserModel user) {

        final Wishlist2Model wishlistModel = tkWishListDao.getWishListByName(user, wishListName);

        if (CollectionUtils.isNotEmpty(wishlistModel.getEntries())) {
            final List<Wishlist2EntryModel> sortedEntries = wishlistModel.getEntries().stream().sorted((entry1, entry2) -> entry2.getModifiedtime().compareTo(entry1.getModifiedtime())).collect(Collectors.toList());
            wishlistModel.setEntries(sortedEntries);
        }

        return wishlistModel;
    }

    @Override
    public boolean deleteWishList(@NotNull final Wishlist2Model wishlist2Model) {
        modelService.remove(wishlist2Model);
        return modelService.isRemoved(wishlist2Model);
    }

    @Override
    public boolean deleteEntryByProduct(final ProductModel product, final Wishlist2Model wishlist2Model) {
        getWishlistService().removeWishlistEntryForProduct(product, wishlist2Model);
        return true;
    }

    @Override
    public void updateWishList(final Wishlist2Model wishList) {
        getModelService().save(wishList);
        getModelService().refresh(wishList);
    }

    @Override
    public boolean removeWishListEntry(final Wishlist2Model wishlist2Model, final Wishlist2EntryModel wishlist2EntryModel) {
        getWishlistService().removeWishlistEntry(wishlist2Model, wishlist2EntryModel);
        return true;
    }

    public Wishlist2Service getWishlistService() {
        return wishlistService;
    }

    public void setWishlistService(Wishlist2Service wishlistService) {
        this.wishlistService = wishlistService;
    }

    public TkWishListDao getTkWishListDao() {
        return tkWishListDao;
    }

    public void setTkWishListDao(TkWishListDao tkWishListDao) {
        this.tkWishListDao = tkWishListDao;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public boolean isWishListExists(UserModel user, String wishListName) {
        List<Wishlist2Model> wishLists = getWishLists(user);

        if (CollectionUtils.isNotEmpty(wishLists)) {

            return wishLists.stream().filter(wishlist2Model -> wishlist2Model.getName().equalsIgnoreCase(wishListName)).findAny().isPresent();
        }

        return false;
    }

    public List<Wishlist2Model> removeFavourite(List<Wishlist2Model> wishLists) {
        if (CollectionUtils.isNotEmpty(wishLists)) {

            return wishLists.stream().filter(wishlist2Model -> !wishlist2Model.getName().equalsIgnoreCase(ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME)).collect(Collectors.toList());
        }
        return null;
    }
}

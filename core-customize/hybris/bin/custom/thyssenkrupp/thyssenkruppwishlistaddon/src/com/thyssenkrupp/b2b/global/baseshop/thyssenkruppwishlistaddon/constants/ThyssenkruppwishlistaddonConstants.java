package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants;

public final class ThyssenkruppwishlistaddonConstants extends GeneratedThyssenkruppwishlistaddonConstants {

    public static final String EXTENSIONNAME = "thyssenkruppwishlistaddon";
    public static final String ADDTOWISHLISTPOPUP = "addon:/thyssenkruppwishlistaddon/fragments/addToWishListPopup";
    public static final String DEFAULTFAVOURITENAME= "Default Favourite";
    
    private ThyssenkruppwishlistaddonConstants() {
        // empty to avoid instantiating this constant class
    }

}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.dao.TkFavouriteDao;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class DefaultTkFavouriteDao implements TkFavouriteDao {

    private static final String DEFAULT_FAVOURITE_NAME = ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME;

    @Resource
    private FlexibleSearchService flexibleSearchService;
    
    @Override
    public TkFavouriteModel checkFavourite(UserModel user) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {fav." + TkFavouriteModel.PK + "} " + "FROM {" + TkFavouriteModel._TYPECODE + " AS fav} "
                + "WHERE {fav." + TkFavouriteModel.USER + "} = ?user " + "AND {fav." + TkFavouriteModel.FAVOURITE + "} = ?favourite " + "AND {fav." + TkFavouriteModel.NAME + "} = ?name");
        fQuery.addQueryParameter("user", user);
        fQuery.addQueryParameter("name", DEFAULT_FAVOURITE_NAME);
        fQuery.addQueryParameter("favourite", Boolean.TRUE);
        SearchResult<TkFavouriteModel> result = flexibleSearchService.search(fQuery);
        return result.getCount() > 0 ? (TkFavouriteModel) result.getResult().iterator().next() : null;
    }
    
    public List<Wishlist2EntryModel> findFavouriteEntryByProduct(ProductModel product, Wishlist2Model wishlist) {
        StringBuilder query = new StringBuilder("SELECT {pk} FROM {Wishlist2Entry} WHERE {");
        query.append("product} = ?product AND {");
        query.append("wishlist} = ?wishlist");
        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
        fQuery.addQueryParameter("product", product);
        fQuery.addQueryParameter("wishlist", wishlist);
        SearchResult<Wishlist2EntryModel> result = flexibleSearchService.search(fQuery);
        List<Wishlist2EntryModel> entries = result.getResult();
        return entries;
    }
}

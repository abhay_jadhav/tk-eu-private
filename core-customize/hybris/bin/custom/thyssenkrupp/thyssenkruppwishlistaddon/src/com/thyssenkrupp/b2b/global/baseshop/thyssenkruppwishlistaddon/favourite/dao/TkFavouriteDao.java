package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.dao;

import java.util.List;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface TkFavouriteDao {

    TkFavouriteModel checkFavourite(UserModel user);
    List<Wishlist2EntryModel> findFavouriteEntryByProduct(ProductModel product, Wishlist2Model wishlist); 

}

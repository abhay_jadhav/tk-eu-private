package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.search.solrfacetsearch.impl;

import com.google.common.collect.ImmutableSet;
import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.SolrAppendToSearchQueryMethodHook;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Set;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class TkFavouriteProductsSolrQueryService implements SolrAppendToSearchQueryMethodHook {

    private static final Logger LOG  = Logger.getLogger(TkFavouriteProductsSolrQueryService.class);

    private DefaultFavouriteProductsLookUpService favouriteProductsLookUpService;
    private String productIdentifier;

    public SolrSearchRequest appendToSearchQuery(final SolrSearchRequest solrSearchRequest) {
        validateParameterNotNull(solrSearchRequest, "solrSearchRequest cannot be null");
        final List<String> favouriteProducts = getFavouriteProductCodesToQuery();
        if (CollectionUtils.isNotEmpty(favouriteProducts)) {
            LOG.debug("*****************  favouriteProducts appendFilterQuery Start ************");
            final Set<String> favProducts = ImmutableSet.copyOf(favouriteProducts);
            final SearchQuery searchQuery = (SearchQuery) solrSearchRequest.getSearchQuery();
            searchQuery.addFilterQuery(getProductIdentifier(), SearchQuery.Operator.OR, favProducts);
            solrSearchRequest.setSearchQuery(searchQuery);
            LOG.debug("*****************  favouriteProducts appendFilterQuery End ************");
        }
        return solrSearchRequest;
    }

    private List<String> getFavouriteProductCodesToQuery() {
        return getFavouriteProductsLookUpService().getFavouriteProducts();
    }

    public DefaultFavouriteProductsLookUpService getFavouriteProductsLookUpService() {
        return favouriteProductsLookUpService;
    }

    public void setFavouriteProductsLookUpService(DefaultFavouriteProductsLookUpService favouriteProductsLookUpService) {
        this.favouriteProductsLookUpService = favouriteProductsLookUpService;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }
}

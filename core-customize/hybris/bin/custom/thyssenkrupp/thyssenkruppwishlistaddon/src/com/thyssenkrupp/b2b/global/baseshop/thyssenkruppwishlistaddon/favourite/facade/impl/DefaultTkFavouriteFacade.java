package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.facade.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.facade.TkFavouriteFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.service.TkFavouriteService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class DefaultTkFavouriteFacade implements TkFavouriteFacade {

    protected static final Logger LOG = Logger.getLogger(DefaultTkFavouriteFacade.class);

    @Resource(name = "tkFavouriteService")
    private TkFavouriteService tkFavouriteService;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "userService")
    private UserService userService;

    @Override
    public TkFavouriteModel checkFavourite(UserModel user) {
        return tkFavouriteService.checkFavourite(user);
    }

    @Override
    public void createFavourite(final UserModel user,final String favourite) {
        tkFavouriteService.createFavourite(user, favourite);
    }

    @Override
    public boolean hasEntryInFavourites(String productCode) {
        final ProductModel product = productService.getProductForCode(productCode);
        final UserModel user = userService.getCurrentUser();
        Wishlist2Model wishlist = tkFavouriteService.checkFavourite(user);
        return tkFavouriteService.checkEntryExistsInFavourite(product, wishlist);
    }
    @Override
    public void removeFavouriteEntryForProduct(String productCode) {
        final ProductModel product = productService.getProductForCode(productCode);
        final UserModel user = userService.getCurrentUser();
        Wishlist2Model wishlist = tkFavouriteService.checkFavourite(user);
        tkFavouriteService.removeFavouriteEntryForProduct(product, wishlist);
        
    }

    public TkFavouriteService getTkFavouriteService() {
        return tkFavouriteService;
    }

    public void setTkFavouriteService(TkFavouriteService tkFavouriteService) {
        this.tkFavouriteService = tkFavouriteService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}

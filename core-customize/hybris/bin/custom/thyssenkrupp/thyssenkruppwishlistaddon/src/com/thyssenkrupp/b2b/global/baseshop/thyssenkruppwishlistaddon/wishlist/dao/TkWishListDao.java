package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.impl.daos.Wishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface TkWishListDao extends Wishlist2Dao {

    Wishlist2Model getWishListByName(UserModel user, String wishlistName);
}

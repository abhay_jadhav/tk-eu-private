package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.facade;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;

import de.hybris.platform.core.model.user.UserModel;

public interface TkFavouriteFacade {

    TkFavouriteModel checkFavourite(UserModel user);
    void createFavourite(UserModel user, String favourite);
    boolean hasEntryInFavourites(String productCode);
    void removeFavouriteEntryForProduct(String productCode);
}

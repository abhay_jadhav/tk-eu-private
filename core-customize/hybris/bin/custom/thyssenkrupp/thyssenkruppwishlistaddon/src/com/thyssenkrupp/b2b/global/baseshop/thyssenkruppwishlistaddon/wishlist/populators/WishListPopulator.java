package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.populators;

import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class WishListPopulator implements Populator<Wishlist2Model, WishListData> {

    @Override
    public void populate(Wishlist2Model wishlist2Model, WishListData wishListData) throws ConversionException {
        wishListData.setName(wishlist2Model.getName());
        wishListData.setDescription(wishlist2Model.getDescription());
        wishListData.setModifiedDate(wishlist2Model.getModifiedtime());
    }
}

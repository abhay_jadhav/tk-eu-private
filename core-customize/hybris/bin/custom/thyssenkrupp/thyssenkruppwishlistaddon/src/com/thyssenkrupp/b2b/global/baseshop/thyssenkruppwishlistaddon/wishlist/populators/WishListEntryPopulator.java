package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.populators;


import org.apache.http.client.utils.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

public class WishListEntryPopulator implements Populator<Wishlist2EntryModel, WishListEntryData> {

    private static final Logger LOG = Logger.getLogger(WishListEntryPopulator.class);
    private Converter<ProductModel, ProductData> productConverter;

    @Override
    public void populate(Wishlist2EntryModel source, WishListEntryData target) throws ConversionException {

        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        populateWishListEntry(source, target);
        populateWishListDetails(source, target);
    }

    protected void populateWishListDetails(final Wishlist2EntryModel modelEntry, final WishListEntryData dataEntry) {
        dataEntry.setComment(modelEntry.getComment());
        dataEntry.setAddedDate(DateUtils.formatDate(modelEntry.getAddedDate(), "dd-MM-yyyy"));
    }

    protected void populateWishListEntry(final Wishlist2EntryModel orderEntry, final WishListEntryData entry) {
        entry.setProduct(getProductConverter().convert(orderEntry.getProduct()));
    }

    public Converter<ProductModel, ProductData> getProductConverter() {
        return productConverter;
    }

    public void setProductConverter(Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }
}

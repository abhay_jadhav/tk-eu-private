/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.pages;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonConstants;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.favourite.facade.TkFavouriteFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.model.TkFavouriteModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.TkWishlistFacade;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

@Controller
@RequestMapping("/my-account")
public class FavouritesPageController extends AbstractCartPageController {

    private static final Logger LOG = Logger.getLogger(FavouritesPageController.class.getName());
    private static final String BREADCRUMBS_ATTR = "breadcrumbs";
    private static final String FAVORITES_CMS_PAGE              = "favourite";
    private static final String TEXT_ACCOUNT_FAVORITE           = "text.account.favourites";
    private static final String MY_FAVOURITE_URL                 = "/my-account/favorites";
    private static final String REDIRECT_TO_FAVOURITE_PAGE       = REDIRECT_PREFIX + MY_FAVOURITE_URL;

    @Resource(name="userService")
    private UserService userService;

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "tkFavouriteFacade")
    private TkFavouriteFacade tkFavouriteFacade;

    @Resource(name = "tkWishListFacade")
    private TkWishlistFacade tkWishListFacade;

    @RequestMapping(value = "/favorites", method = RequestMethod.GET)
    @RequireHardLogIn
    public String getFavourite(final Model model) throws CMSItemNotFoundException {
        try {
            final UserModel user = userService.getCurrentUser();
            if (checkFavourite(user) != null) {
                model.addAttribute("favouriteData", tkWishListFacade.getWishListByName(ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME, user));
            }
            storeCmsPageInModel(model, getContentPageForLabelOrId(FAVORITES_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FAVORITES_CMS_PAGE));
            model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_FAVORITE));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        } catch (Exception ex) {
            LOG.error("Error in getfavourite:"+ex.getMessage());
            return REDIRECT_TO_FAVOURITE_PAGE;
        }
        return getViewForPage(model);
    }

    @RequestMapping(value = "/add-favourite", method = RequestMethod.POST)
    @RequireHardLogIn
    public String addFavourite(final RedirectAttributes redirectModel,@RequestParam("productCode") String productCode, final HttpServletRequest request) throws CMSItemNotFoundException {
        try {
            final UserModel user = userService.getCurrentUser();
            if (checkFavourite(user) != null) {
                addtToFavourite(productCode, redirectModel);
            } else {
                createFavourite(user);
                addtToFavourite(productCode, redirectModel);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        return REDIRECT_PREFIX + request.getHeader("Referer");
    }

    @RequestMapping(value = "/remove-favourite", method = RequestMethod.POST)
    @RequireHardLogIn
    public String removeFavourite(@RequestParam("productCode") String productCode, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        try {
            if (tkFavouriteFacade.hasEntryInFavourites(productCode)) {
                removeFromFavourite(productCode, redirectModel);
            } else {
                GlobalMessages.addFlashMessage(redirectModel,GlobalMessages.ERROR_MESSAGES_HOLDER,"favorites.product.notfound");
            }
        } catch (Exception ex) {
            LOG.error("Error in Remove favourite:"+ex.getMessage());
            return REDIRECT_TO_FAVOURITE_PAGE;
        }
        return REDIRECT_TO_FAVOURITE_PAGE;
    }

    private TkFavouriteModel checkFavourite(final UserModel user) {
        return tkFavouriteFacade.checkFavourite(user);
    }

    private void createFavourite(final UserModel user) {
        tkFavouriteFacade.createFavourite(user,ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME);
    }

    private void addtToFavourite(final String productCode, final RedirectAttributes redirectModel) {
        if (!tkFavouriteFacade.hasEntryInFavourites(productCode)) {
            tkWishListFacade.addToWishList(ThyssenkruppwishlistaddonConstants.DEFAULTFAVOURITENAME, productCode, 1);
            GlobalMessages.addFlashMessage(redirectModel,GlobalMessages.INFO_MESSAGES_HOLDER,"favorites.product.added");
        } else {
            GlobalMessages.addFlashMessage(redirectModel,GlobalMessages.INFO_MESSAGES_HOLDER,"favorites.product.already.added");
        }
    }

    private void removeFromFavourite(final String productCode, final RedirectAttributes redirectModel) {
        tkFavouriteFacade.removeFavouriteEntryForProduct(productCode);
        GlobalMessages.addFlashMessage(redirectModel,GlobalMessages.INFO_MESSAGES_HOLDER,"favorites.product.removed");
    }

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div>
    <c:url value="${url}" var="addToWishListActionUrl"/>
    <c:url value="/login" var="loginUrl"/>
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        <c:choose>
            <c:when test="${not empty sessionScope.acceleratorSecureGUID}">
                <div class="add-to-wishlist">
                <form:form method="post" id="addToWishListModalForm" class="add_to_wishlist_form span-5" action="${addToWishListActionUrl}">
                    <input type="hidden" name="productCodePost" value="${fn:toUpperCase(product.code)}"/>
                    <c:choose>
                        <c:when test="${!product.multidimensional}">
                            <button id="addToWishListButton" type="submit" class="btn btn-primary btn-block" data-popup-title="<spring:theme code="text.addToWishList"/>">
                                 <i class="fa fa-list"></i>
                            </button>
                        </c:when>
                    </c:choose>
                </form:form>
              </div>
            </c:when>
            <c:otherwise>
                <div class="add-to-wishlist">
                    <a id="addToWishListButton" href="${loginUrl}" class="btn btn-primary btn-block">
                       <i class="fa fa-list"></i>
                    </a>
                </div>
            </c:otherwise>
        </c:choose>
    </sec:authorize>
</div>

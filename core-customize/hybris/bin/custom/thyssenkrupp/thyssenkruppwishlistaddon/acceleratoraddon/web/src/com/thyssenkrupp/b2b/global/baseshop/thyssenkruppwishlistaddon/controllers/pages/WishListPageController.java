package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.pages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData;
import com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.form.AddToWishListForm;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.wishlist.facade.TkWishlistFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.constants.ThyssenkruppwishlistaddonWebConstants.MY_WISH_LIST_URL;

@Controller
@RequestMapping("/my-account")
public class WishListPageController extends AbstractPageController {

    private static final Logger LOGGER = Logger.getLogger(WishListPageController.class);

    private static final String BREADCRUMBS_ATTR                = "breadcrumbs";
    private static final String CREATE_WISHLIST_CMS_PAGE        = "wishlist";
    private static final String TEXT_ACCOUNT_WISHLIST           = "text.account.wishlist";
    private static final String CREATE_WISHLIST_DETAIL_CMS_PAGE = "wishlistDetailsPage";
    private static final String WISHLIST_NAME_VARIABLE_PATTERN  = "{wishListName:.*}";
    private static final String PRODUCT_CODE_VARIABLE_PATTERN   = "{productCode:.*}";
    private static final String REDIRECT_TO_WISHLIST_PAGE       = REDIRECT_PREFIX + MY_WISH_LIST_URL;
    private static final String TEXT_ACCOUNT_WISHLIST_DETAILS   = "text.account.wishlistDetails";

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "tkWishListFacade")
    private TkWishlistFacade tkWishListFacade;

    @RequestMapping(value = "/my-wishlist", method = { RequestMethod.GET, RequestMethod.POST })
    @RequireHardLogIn
    public String myWishList(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        final UserModel user = userService.getCurrentUser();
        getAllWishLists(user, model);
        final AddToWishListForm addToWishListForm = new AddToWishListForm();
        model.addAttribute("addToWishListForm", addToWishListForm);
        addRedirectAttributes(model, redirectAttributes);
        storeCmsPageInModel(model, getContentPageForLabelOrId(CREATE_WISHLIST_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CREATE_WISHLIST_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_WISHLIST));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    private void addRedirectAttributes(Model model, RedirectAttributes redirectAttributes) {
        if (redirectAttributes.containsAttribute("addtoWishListResult")) {
            model.addAttribute("addtoWishListResult", redirectAttributes.getFlashAttributes().get("addtoWishListResult"));
        }
        if (redirectAttributes.containsAttribute("wishListName")) {
            model.addAttribute("wishListName", redirectAttributes.getFlashAttributes().get("wishListName"));
        }
    }

    @ResponseBody
    @RequestMapping(value = "/create-Wishlist", method = RequestMethod.POST)
    @RequireHardLogIn
    public ObjectNode createWishList(@Valid final AddToWishListForm addToWishListForm) throws CMSItemNotFoundException {
        final UserModel user = userService.getCurrentUser();
        ObjectNode modelData = new ObjectMapper().createObjectNode();
        createWishList(user, addToWishListForm, modelData);
        return modelData;
    }

    @RequestMapping(value = "/wishlist-details/" + WISHLIST_NAME_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String wishListDetails(@PathVariable("wishListName") final String wishListName, final Model model) throws CMSItemNotFoundException {
        try {
            final UserModel user = userService.getCurrentUser();
            getWishListDetails(wishListName, user, model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(CREATE_WISHLIST_DETAIL_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CREATE_WISHLIST_DETAIL_CMS_PAGE));
            model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_WISHLIST_DETAILS));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        } catch (final Exception ex) {
            LOGGER.error("Error while retrieving wishlist entries", ex);
            return REDIRECT_TO_WISHLIST_PAGE;
        }

        return getViewForPage(model);
    }

    @ResponseBody
    @RequestMapping(value = "/wishlist/" + WISHLIST_NAME_VARIABLE_PATTERN, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequireHardLogIn
    public ObjectNode deleteWishList(@PathVariable("wishListName") final String wishListName) throws JsonProcessingException {
        ObjectNode modelData = new ObjectMapper().createObjectNode();
        ArrayNode errorMessages = modelData.putArray("errorMessages");
        try {
            boolean isWishListDeleted = tkWishListFacade.deleteWishList(wishListName);
            modelData.put("wishListDeleted", isWishListDeleted);
            checkForError(errorMessages, isWishListDeleted, getMessageFromCurrentLocale("error.wishlist.delete"));
        } catch (Exception ex) {
            errorMessages.add(getMessageFromCurrentLocale("error.wishlist.delete"));
            LOGGER.error("Error while removing wishlist \"" + wishListName + "\"", ex);
        }

        modelData.put("wishList", wishListName);
        modelData.putPOJO("wishLists", tkWishListFacade.getAllWishLists(userService.getCurrentUser()));
        return modelData;
    }

    @ResponseBody
    @RequestMapping(value = "/wishlist/" + WISHLIST_NAME_VARIABLE_PATTERN, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequireHardLogIn
    public ObjectNode updateWishList(@PathVariable("wishListName") final String wishListName, @Valid @RequestBody final AddToWishListForm addToWishListForm) {
        ObjectNode modelData = new ObjectMapper().createObjectNode();
        String updatedWishListName = addToWishListForm.getWishListName().trim();
        updateWishList(wishListName, addToWishListForm, modelData, updatedWishListName);
        modelData.put("wishList", wishListName);
        modelData.put("updatedWishList", updatedWishListName);
        modelData.putPOJO("wishLists", tkWishListFacade.getAllWishLists(userService.getCurrentUser()));
        return modelData;
    }

    private void updateWishList(final String wishListName, final AddToWishListForm addToWishListForm, final ObjectNode modelData, final String updatedWishListName) {
        ArrayNode errorMessages = modelData.putArray("errorMessages");
        boolean isWishListExists = wishListName.equalsIgnoreCase(updatedWishListName) ? false : tkWishListFacade.isWishListExists(userService.getCurrentUser(), updatedWishListName);
        if (isWishListExists) {
            errorMessages.add(getMessageFromCurrentLocale("error.wishlist.name.duplicate"));
        } else {
            tkWishListFacade.updateWishList(wishListName, updatedWishListName, addToWishListForm.getWishListDescription());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/wishlist/" + WISHLIST_NAME_VARIABLE_PATTERN + "/product/" + PRODUCT_CODE_VARIABLE_PATTERN, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequireHardLogIn
    public ObjectNode deleteWishListEntry(@PathVariable("wishListName") final String wishListName, @PathVariable("productCode") final String productCode) throws JsonProcessingException {
        ObjectNode modelData = new ObjectMapper().createObjectNode();
        ArrayNode errorMessages = modelData.putArray("errorMessages");
        try {
            boolean isWishListEntryDeleted = tkWishListFacade.deleteEntryByProduct(wishListName, productCode);
            modelData.put("wishListEntryDeleted", isWishListEntryDeleted);
            checkForError(errorMessages, isWishListEntryDeleted, getMessageFromCurrentLocale("error.wishlist.wishlistentry.delete"));
        } catch (Exception ex) {
            errorMessages.add(getMessageFromCurrentLocale("error.wishlist.wishlistentry.delete"));
            LOGGER.error("Error while removing wishlist", ex);
        }
        modelData.put("wishList", wishListName);
        modelData.put("productCode", productCode);
        modelData.putPOJO("wishListEntry", tkWishListFacade.getWishListByName(wishListName, userService.getCurrentUser()));
        return modelData;
    }

    private void checkForError(final ArrayNode errorMessages, final boolean isValid, final @NotNull String errorMessage) {
        if (!isValid) {
            errorMessages.add(errorMessage);
        }
    }

    private void createWishList(final UserModel user, final AddToWishListForm addToWishListForm, final ObjectNode modelData) {
        ArrayNode errorMessages = modelData.putArray("errorMessages");
        final String wishListName = addToWishListForm.getWishListName().trim();
        final String wishListDescription = addToWishListForm.getWishListDescription();
        if (tkWishListFacade.isLimitExceeds()) {
            errorMessages.add(getMessageFromCurrentLocale("error.wishlist.user.maximum"));
        } else if (tkWishListFacade.isWishListExists(user, wishListName)) {
            errorMessages.add(getMessageFromCurrentLocale("error.wishlist.name.duplicate"));
        } else {
            tkWishListFacade.createWishList(user, wishListName, wishListDescription);
        }
    }

    private String getMessageFromCurrentLocale(final @NotNull String messageKey) {
        return getMessageSource().getMessage(messageKey, null, getI18nService().getCurrentLocale());
    }

    public void getAllWishLists(final UserModel user, Model model) {
        model.addAttribute("wishLists", tkWishListFacade.getAllWishLists(user));
    }

    public void getWishListDetails(final String wishListName, final UserModel user, Model model) {
        WishListData wishListData = tkWishListFacade.getWishListByName(wishListName, user);
        model.addAttribute("wishListData", wishListData);
        if (wishListData.getEntries() != null) {
            removeUnAvailableProducts(wishListName, user, model, wishListData.getEntries());
        }
        model.addAttribute("wishListData", tkWishListFacade.getWishListByName(wishListName, user));
    }

    private void removeUnAvailableProducts(final String wishListName, final UserModel user, final Model model, final @NotNull List<WishListEntryData> wishListEntryData) {
        List<WishListEntryData> unAvailableEntries = tkWishListFacade.removeAllUnAvailableProducts(wishListName, wishListEntryData);
        if (CollectionUtils.isNotEmpty(unAvailableEntries)) {
            final List<String> unAvailableProducts = unAvailableEntries.stream().map(wishListStreamData ->
                "[" + wishListStreamData.getProduct().getName() + " : " + wishListStreamData.getProduct().getCode() + "]").collect(Collectors.toList());
            GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "error.wishlist.product.unavailable", new String[] { String.join(",", unAvailableProducts) });
            model.addAttribute("unAvailableProducts", unAvailableProducts);
            model.addAttribute("wishListData", tkWishListFacade.getWishListByName(wishListName, user));
        }
    }
}

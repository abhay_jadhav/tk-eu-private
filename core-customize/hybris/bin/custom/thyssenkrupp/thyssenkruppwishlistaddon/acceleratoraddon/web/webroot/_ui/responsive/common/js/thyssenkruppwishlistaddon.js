ACC.thyssenkruppwishlist = {
    _autoload: [
        "bindToAddToWishListModalForm",
        "bindToAddToWishListSucessForm",
        ["bindWishListForm", $("#addToWishListForm").length != 0],
        ['bindCreateAndAddToWishListForm', $("#addToWishListModalForm").length != 0],
        ["bindWishListOperations", $(".wishlist-section").length != 0],
        ["bindWishListDetailPageOperations", $("table.wish_list_entry").length != 0],
        "removeProductFavoriteBtn",
    ],
    bindToAddToWishListModalForm: function () {
        var AddToWishListModalForm = $('.add_to_wishlist_form');
        AddToWishListModalForm.ajaxForm({
            beforeSubmit:ACC.thyssenkruppwishlist.showRequest,
            success: ACC.thyssenkruppwishlist.displayAddToWishListPopup
        });
        setTimeout(function(){
            $ajaxCallEvent  = true;
        }, 2000);
    },
    bindToAddToWishListSucessForm: function () {
        var addToWishListSuccessForm = $('#addtoWishListModel');
        addToWishListSuccessForm.ajaxForm({
            beforeSubmit:ACC.thyssenkruppwishlist.showRequest,
            success: ACC.thyssenkruppwishlist.displayAddToWishListPopup
        });
        setTimeout(function(){
            $ajaxCallEvent  = true;
        }, 2000);
    },
    showRequest: function(arr, $form, options) {
        if($ajaxCallEvent) {
            $ajaxCallEvent = false;
            return true;
        }
        return false;
    },

    displayAddToWishListPopup: function (wishListForm, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
        $('#addToWishListLayer').remove();
        var titleHeader = $('.add_to_wishlist_form button[type="submit"]').attr('data-popup-title');

        ACC.colorbox.open(titleHeader, {
            html: wishListForm.addToWishListLayer,
            width: "460px",
            onComplete: function () {
                $('.selectpicker').selectpicker({
                    style: 'btn-default',
                    size: 4
                });
            }
        });

        $('#cboxContent').on('change', '#wishListName', function(){
            $('#addToWishListFormSubmit').prop('disabled', this.value.trim() == "" ? true : false);
        });

        $('#cboxContent').on('click', '.js-add-wishlist-close-button', function(e){
            e.preventDefault();
            ACC.colorbox.close()
        });
    },

    bindWishListForm: function () {
        $('#wishListName').on('blur',function() {

            $('#saveWishListForm').prop('disabled', this.value.trim() == "" ? true : false);

        });

        $('li.add-wishlist-form form').submit(function(e) {
            e.preventDefault();
            var form = $(this);
            highLightForm(false);
            var wishListName = $(form).find("input[name='wishListName']").val().trim();
            if (validateWishlistForm(wishListName) == false) {
                var errorMsg = $('.error.error-message').text();
                showErrorMessage(errorMsg);
            } else {
                $.post($(form).attr('action'), $(form).serialize())
                    .done(function(data) {
                        highLightForm(true);

                        if (data.errorMessages.length > 0) {
                            showErrorMessage(data.errorMessages.join('<br/>'));
                        } else {
                            location.reload();
                        }

                    });
              }
            });


        $('#saveWishListForm').prop('disabled', $('#wishListName').val().trim() == "" ? true : false);
    },

    removeProductFavoriteBtn: function () {
        $(document).on('click', '.js-remove-favorite-item', function(e){
            e.preventDefault();
            var favoriteProductId = $(this).attr('data-product-id');
            var favoriteProductName = $(this).attr('data-product-name');
            var popupTitle = $(this).attr('data-popup-title');
            var form = $(this).closest('form');
            ACC.thyssenkruppwishlist.removeProductFavoritePopup(favoriteProductId,favoriteProductName, popupTitle, form);
       });
    },

    removeProductFavoritePopup: function (favoriteProductId,favoriteProductName, popupTitle, form) {
        if(popupTitle === 'undefined') {
            popupTitle = '';
        }

        var favoriteProductId = favoriteProductId;
        var confirm = '<p> '+$("input[name='favouriteDeleteSummary']").val()+'</p>'+
        '<div class="modal-details row">'+
            '<span class="col-xs-6">'+$("input[name='productNameLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+favoriteProductName+'</b></span>'+
            '<span class="col-xs-6">'+$("input[name='productCodeLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+favoriteProductId+'</b></span>'+
        '</div>'+
        '<div class="modal-actions">'+
            '<div class="row">'+
                '<div class="col-xs-12 col-sm-6 col-sm-push-6">'+
                    '<button id="confirmDeleteYes" type="button" data-qa-id="favourites-delete-button" class="js-savedcart_delete_confirm btn btn-primary btn-block">'+
                        $("input[name='deleteLabel']").val()+
                    '</button>'+
                '</div>'+
                '<div class="col-xs-12 col-sm-6 col-sm-pull-6">'+
                    '<button id="confirmDeleteNo" type="button" data-qa-id="remove-favourites-cancel" class="js-savedcart_delete_confirm_cancel btn btn-default btn-block">'+
                        $("input[name='cancelLabel']").val()+
                    '</button>'+
                '</div>'+
            '</div>'+
        '</div>';

        ACC.colorbox.open(popupTitle, {
            html: confirm,
            className: "js-wishlist_remove_confirm_modal",
            width: '500px',
            onComplete: function () {
                $('#confirmDeleteYes').click(function(){
                    form.submit();
                })

                $('#confirmDeleteNo').click(function(){
                    ACC.colorbox.close();
                });
            }
        });
    },

    bindCreateAndAddToWishListForm: function () {

         $(document).on('submit','#createAndAddToWishListModal', function(){

            var form = $(this);
            var wishListName = $(form).find("input[name='wishListName']").val().trim();

            if (validateWishlistForm(wishListName) == false) {
                var errorMsg = $('.error.error-message').text();

                showErrorMessage(errorMsg, true);
                return false;

            }

            return true;
        });

    },

    bindWishListOperations: function () {
        $(document).on('click', '.js-delete-wishlist', function(e){
            e.preventDefault();

            var wishListObj = $(this).data('wishlist');
            var popupTitle = $(this).data('delete-popup-title');
            ACC.thyssenkruppwishlist.displayDeleteWishListPopup(wishListObj, popupTitle);

        });

        $(document).on('click', '.js-edit-wishlist-toggle-on', function(e){
            e.preventDefault();
            var wishListObj = $(this).data('wishlist');
            var form = $(this).closest('form');
            $(form).find("input[name='wishListName']").val(wishListObj.wishListName);
            $(form).find("textarea[name='wishListDescription']").val(wishListObj.wishListDescription);
            toggleFormWidget(form);
        });

        $(document).on('click', '.js-edit-wishlist-toggle-off', function(e){
            e.preventDefault();
            var form = $(this).closest('form');
            toggleFormWidget(form);
        });

        $(document).on('click', '.js-edit-wishlist', function(e){
            e.preventDefault();
            $(this).closest('form').submit();

        });

        $('.edit-wishlist-form').submit(function (e) {
            e.preventDefault();

            var form = $(this);
            var jsonInputArgs = {"wishListName": $(form).find("input[name='wishListName']").val().trim(), "wishListDescription": $(form).find("textarea[name='wishListDescription']").val()};

            var wishListName = $(form).find("input[name='wishListName']").val();
            if (validateWishlistForm(wishListName) == false) {
                var errorMsg = $('.error.error-message').text();
                showErrorMessage(errorMsg);
            } else {
                $.ajax({
                     url: $(form).attr('action'),
                     type: 'PUT',
                     contentType : "application/json",
                     data: JSON.stringify(jsonInputArgs),
                     success: function(data) {
                         if (data.errorMessages.length > 0) {
                             showErrorMessage(data.errorMessages.join('<br/>'));
                         } else {
                             location.reload();
                         }
                     }
                });
            }

        });
    },

    displayDeleteWishListPopup: function (wishListObj, popupTitle) {
        if(popupTitle === 'undefined') {
            popupTitle = '';
        }
        var wishListId = wishListObj.wishListName;
        var confirm = '<p>  '+$("input[name='deleteSummary']").val()+'  </p>'+
        '<div class="modal-details row">'+
            '<span class="col-xs-6">'+$("input[name='nameLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+wishListObj.wishListName+'</b></span>'+
            '<span class="col-xs-6">'+$("input[name='descriptionLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+wishListObj.wishListDescription+'</b></span>'+
        '</div>'+
        '<div class="modal-actions">'+
            '<div class="row">'+
                '<div class="col-xs-12 col-sm-6 col-sm-push-6">'+
                    '<button id="confirmDeleteYes" type="button" class="js-savedcart_delete_confirm btn btn-primary btn-block">'+$("input[name='deleteLabel']").val()+
                    '</button>'+
                '</div>'+
                '<div class="col-xs-12 col-sm-6 col-sm-pull-6">'+
                    '<button id="confirmDeleteNo" type="button" class="js-savedcart_delete_confirm_cancel btn btn-default btn-block">'+
                       $("input[name='cancelLabel']").val()+
                    '</button>'+
                '</div>'+
            '</div>'+
        '</div>';

        ACC.colorbox.open(popupTitle, {
            html: confirm,
            className: "js-wishlist_delete_confirm_modal",
            width: '500px',
            onComplete: function () {
                $('#confirmDeleteYes').click(function(){
                    $.ajax({
                        url: '/my-account/wishlist//'+wishListId,
                        type: 'DELETE',
                        success: function(data) {
                            ACC.colorbox.close();
                            if (data.errorMessages.length > 0) {
                                showErrorMessage(data.errorMessages.join('<br/>'));
                            } else {
                                location.reload();
                            }
                        }
                    });
                })

                $('#confirmDeleteNo').click(function(){
                    ACC.colorbox.close();
                });
            }
        });
    },

    bindWishListDetailPageOperations: function () {
        $(document).on('click', '.js-remove-wishlist-item', function(e){
            e.preventDefault();

            var wishListObj = $(this).data('wishlist-item');
            var popupTitle = $(this).data('remove-popup-title');
            ACC.thyssenkruppwishlist.displayRemoveProductFromWishListPopup(wishListObj, popupTitle);

        });
    },

    displayRemoveProductFromWishListPopup: function (wishListObj, popupTitle) {
        if(popupTitle === 'undefined') {
            popupTitle = '';
        }
        var wishListId = wishListObj.wishListName;
        var confirm = '<p> '+$("input[name='deleteSummary']").val()+'</p>'+
        '<div class="modal-details row">'+
            '<span class="col-xs-6">'+$("input[name='nameLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+wishListObj.wishListName+'</b></span>'+
            '<span class="col-xs-6">'+$("input[name='productNameLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+wishListObj.productName+'</b></span>'+
            '<span class="col-xs-6">'+$("input[name='productCodeLabel']").val()+':</span>'+
            '<span class="col-xs-6"><b>'+wishListObj.productCode+'</b></span>'+
        '</div>'+
        '<div class="modal-actions">'+
            '<div class="row">'+
                '<div class="col-xs-12 col-sm-6 col-sm-push-6">'+
                    '<button id="confirmDeleteYes" type="button" class="js-savedcart_delete_confirm btn btn-primary btn-block">'+$("input[name='deleteLabel']").val()+
                    '</button>'+
                '</div>'+
                '<div class="col-xs-12 col-sm-6 col-sm-pull-6">'+
                    '<button id="confirmDeleteNo" type="button" class="js-savedcart_delete_confirm_cancel btn btn-default btn-block">'+
                         $("input[name='cancelLabel']").val()+
                    '</button>'+
                '</div>'+
            '</div>'+
        '</div>';


        ACC.colorbox.open(popupTitle, {
            html: confirm,
            className: "js-wishlist_remove_confirm_modal",
            width: '500px',
            onComplete: function () {
                $('#confirmDeleteYes').click(function(){
                    $.ajax({
                        url: '/my-account/wishlist/'+wishListId+'/product/'+wishListObj.productCode,
                        type: 'DELETE',
                        success: function(data) {
                            ACC.colorbox.close();

                            if (data.errorMessages.length > 0) {
                                showErrorMessage(data.errorMessages.join('<br/>'));
                            } else {
                                location.reload();
                            }
                        }
                    });
                })

                $('#confirmDeleteNo').click(function(){
                    ACC.colorbox.close();
                });
            }
        });
    },

};

function validateWishlistForm(wishListName){
    return /^[A-zÀ-ÿ\/\. 0-9]+$/.test(wishListName);
}

function toggleFormWidget(form){
    if($(form).find('.wishlist-value').hasClass('show-widget')){
        $(form).find('.wishlist-value').removeClass('show-widget').addClass('hide-widget');

        $(form).find('.wishlist-edit').removeClass('hide-widget').addClass('show-widget');
    } else {
        $(form).find('.wishlist-value').removeClass('hide-widget').addClass('show-widget');

        $(form).find('.wishlist-edit').removeClass('show-widget').addClass('hide-widget');
    }
}

function showErrorMessage(errorMsg, isModalForm){

    if(isModalForm){
        if ($('#cboxLoadedContent .alert.alert-danger').length > 0) {
            $('#cboxLoadedContent .alert.alert-danger').remove();
        }
        $('#cboxLoadedContent').prepend(renderErrorTemplate(errorMsg) );

    } else {

        if ($('.alert.alert-danger').length > 0) {
            $('.alert.alert-danger').remove();
        }
        $('.account-section').prepend(renderErrorTemplate(errorMsg) );
        highLightForm(true);
    }

}

function renderErrorTemplate(errorMsg){
    return '<div class="alert alert-danger alert-dismissable">' +errorMsg+ ' <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>';
}

function highLightForm(flag){
    var form = $('#addToWishListForm');
    if(flag)
         $(form).css('opacity', 1);
    else
        $(form).css('opacity', 0.3);
}

<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<spring:message code="text.wishlistName" var="wishlistNameLabel"/>

{"cartData": {},
"addToWishListLayer":"<spring:escapeBody javaScriptEscape="true">
<c:choose>
    <c:when test ="${not empty wishListData}">
        <c:url value="/wishlist/addWishListEntry" var="wishListUrl" />
        <c:url value="addtoWishListModal" var="formId" />
    </c:when>
    <c:otherwise>
         <c:url value="/wishlist/createAndAddWishListEntry" var="wishListUrl" />
         <c:url value="createAndAddToWishListModal" var="formId" />
    </c:otherwise>
</c:choose>

<form:form method="post" id="${formId}" class="add_to_wishlist_sucess_form" action="${wishListUrl}"
    commandName="addToWishListForm">
    <input type="hidden" name="productCodePost" value="${product.code}"/>
    <div class="row">
        <div class="col-xs-4">
            <product:productPrimaryImage product="${product}" format="thumbnail"/>
        </div>
        <div class="col-xs-8">
            <div class="form-group">
                <strong>${fn:escapeXml(product.name)}</strong>
            </div>
            <div class="form-group">
                <c:choose>
                    <c:when test ="${not empty wishListData}">

                        <form:select path="wishListName" class="form-control select-text-case">
                            <option value="" disabled selected><spring:theme code="Select Wishlist"/></option>
                            <c:forEach var="wishlist" items="${wishListData}">
                                <form:option value="${wishlist.name}"> </form:option>
                            </c:forEach>
                        </form:select>
                    </c:when>
                    <c:otherwise>
                        <spring:theme code="wishlist.message.create.new" />
                        <div class="form-group">
                            <form:input idKey="wishListName" class="form-control" path="wishListName" placeholder="${wishlistNameLabel}" mandatory="true" value="${fn:escapeXml(wishlist.name)}" maxlength="100"/>
                            <div class="error error-message hide" id="alertMsg">
                               <spring:theme code="wishlist.specialCharacter.validation.message"/>
                            </div>
                        </div>
                     </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <div id="addToWishListLayer" class="add-to-cart">
        <c:choose>
            <c:when test="${not empty wishListData}">
                <button id="addToWishListFormSubmit" type="submit" class="btn btn-primary btn-block" disabled="true">
                    <spring:theme code="text.addToWishList" />
                </button>
            </c:when>
            <c:otherwise>
                <button id="addToWishListFormSubmit" type="submit" class="btn btn-primary btn-block" disabled="true">
                    <spring:theme code="text.createaandaddtowishlist" />
                </button>
            </c:otherwise>
        </c:choose>
        <a href="#" class="btn btn-default btn-block js-add-wishlist-close-button">
            <spring:theme code="text.cancel.label" />
        </a>
    </div>
    </form:form>
    </spring:escapeBody>"
}

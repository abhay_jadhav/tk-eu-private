<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="favouriteEntry" required="true"
    type="com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url value="${favouriteEntry.product.url}" var="productUrl" />
<c:url value="/my-account/remove-favourite" var="removeFavourite" />

<div class="item__image" data-qa-id="favourites-product-image">
    <a href="${productUrl}"> <product:productPrimaryImage product="${favouriteEntry.product}" format="thumbnail" /></a>
</div>

<div class="item__info">
    <a href="${favouriteEntry.product.purchasable ? productUrl : ''}">
        <span class="item__name" data-qa-id="favourites-product-name">${fn:escapeXml(favouriteEntry.product.name)} </span>
     </a>
    <div class="item__code" data-qa-id="favourites-product-identifier">${fn:escapeXml(favouriteEntry.product.code)}</div>
</div>

<div class="item__wishdeliverydate">
    <div class="favoriteDate" data-qa-id="favourites-added-date"> ${favouriteEntry.addedDate} </div>
</div>

<div class="item__remove">
    <form:form method="post" id="removeFavForm"  class="favoriteRemove" action="${removeFavourite}">
        <input type="hidden" name="productCode" value="${favouriteEntry.product.code}"/>
        <button class="btn favoriteCloseBtn js-remove-favorite-item" data-qa-id="favourites-remove-button"
            data-product-id="${fn:escapeXml(favouriteEntry.product.code)}"  data-product-name="${fn:escapeXml(favouriteEntry.product.name)}"
            data-popup-title="<spring:theme code="favourite.delete.title" text="Delete Favourites" /> " id="addToFavButton"  >
                <span class="glyphicon glyphicon-remove"></span>
        </button>
    </form:form>
</div>

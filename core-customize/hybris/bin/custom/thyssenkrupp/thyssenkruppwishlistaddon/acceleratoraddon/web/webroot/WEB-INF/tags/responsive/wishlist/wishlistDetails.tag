<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="wishlist" required="true"
    type="com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListData"%>
<%@ attribute name="wishlistEntry" required="true"
    type="com.thyssenkrupp.b2b.global.baseshop.facades.wishlist.data.WishListEntryData"%>
<%@ attribute name="consignmentEntry" required="false"
    type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData"%>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer"%>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="wishlist-entry"
    tagdir="/WEB-INF/tags/addons/thyssenkruppwishlistaddon/responsive/wishlist"%>


<c:url value="${wishlistEntry.product.url}" var="productUrl" />
<tr>
    <%-- product image --%>
    <td class="item__image" >
        <a href="${productUrl}" data-qa-id="wishlist-details-product-image"> <product:productPrimaryImage
                product="${wishlistEntry.product}" format="thumbnail" />
        </a>

    </td>

    <td class="item__info">
        <a href="${wishlistEntry.product.purchasable ? productUrl : ''}" data-qa-id="wishlist-details-product-name"><span
            class="item__name">${fn:escapeXml(wishlistEntry.product.name)}</span></a>

        <div class="item__code" data-qa-id="wishlist-details-product-id">
            ${fn:escapeXml(wishlistEntry.product.code)}</div>
    </td>
    <td class="item__info" data-qa-id="wishlist-details-product-date">${wishlistEntry.addedDate}</td>
    <td class="item_actions">
        <a data-qa-id="    wishlist-details-product-remove" href="#" class="js-remove-wishlist-item edit-item-link item-link" data-wishlist-item='{"wishListName": "${fn:escapeXml(fn:trim(wishlist.name))}", "productName": "${fn:escapeXml(wishlistEntry.product.name)}", "productCode": "${fn:escapeXml(wishlistEntry.product.code)}"}' data-remove-popup-title="Remove Product">
            <span class="glyphicon glyphicon-remove"></span>
        </a>
    </td>

</tr>
<input type="hidden" name="deleteSummary" value="<spring:theme code="text.delete.summary" text="The following product will be deleted from the wishlist"/>">
<input type="hidden" name="deleteLabel" value="<spring:theme code="text.delete" text="Delete"/>">
<input type="hidden" name="cancelLabel" value="<spring:theme code="text.cancel" text="Cancel"/>">
<input type="hidden" name="nameLabel" value="<spring:theme code="text.wishlistName" />">
<input type="hidden" name="descriptionLabel" value="<spring:theme code="text.wishlistDescription" />">
<input type="hidden" name="productNameLabel" value="<spring:theme code="text.productName" text="Product Name"/>">
<input type="hidden" name="productCodeLabel" value="<spring:theme code="text.productLabel" text="Product Code" />">

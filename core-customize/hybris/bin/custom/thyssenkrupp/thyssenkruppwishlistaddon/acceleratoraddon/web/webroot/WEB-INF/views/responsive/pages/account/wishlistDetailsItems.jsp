<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="wishlist-entry" tagdir="/WEB-INF/tags/addons/thyssenkruppwishlistaddon/responsive/wishlist" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-header" data-qa-id="wishlist-details-name">${fn:escapeXml(wishListData.name)}</div>


<table class="table item__list wish_list_entry" data-qa-id="wishlist-details-list">
    <c:if test="${empty wishListData.entries}">
        <div class="alert alert-danger alert-dismissable"><spring:theme code="wishlist.empty.validation.message"/>
             <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
        </div>
    </c:if>
    <c:if test="${not empty wishListData.entries}">
        <thead>
             <th ></th>
             <th ><spring:theme code="wishlist.page.product.label"/></th>
             <th ><spring:theme code="wishlist.page.product.addeddate.label"/></th>
             <th ><spring:theme code="wishlist.page.product.actions.label" text="Actions"/></th>
        </thead>
        <tbody>
             <c:forEach items="${wishListData.entries}" var="entry" varStatus="loop">
                  <wishlist-entry:wishlistDetails wishlistEntry="${entry}" wishlist="${wishListData}" itemIndex="${loop.index}" showStock="false"/>
             </c:forEach>
        </tbody>
    </c:if>
</table>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/wishlist-details/" var="wishlistDetailsLink" htmlEscape="false"/>
<spring:message code="text.wishlistName" var="wishlistNameLabel"/>
<spring:message code="text.wishlistDescription" var="wishlistDescriptionLabel"/>

<div class="account-section-content">
    <div class="account-section-header"> <spring:theme code="text.wishlist.myWishlist" text="My WishList" /></div>
    <c:if test="${not empty addtoWishListResult && not empty wishListName}">
        <div class="alert alert-info" data-qa-id="wishlist-msg">
            <span class="alert-text">${addtoWishListResult}&nbsp;${wishListName}</span>
        </div>
    </c:if>
    <c:if test="${not empty errorMessage}">
        <div class="alert alert-danger" data-qa-id="wishlist-msg"> ${errorMessage}</div>
    </c:if>

    <div class="wishlist-section">
        <div class="row">
            <div class="col-xs-12">
                <div class="add-wishlist-form">
                    <ul class="item__list item__list__cart js-ul-container">
                        <li class="item__list--item js-li-container add-wishlist-form">
                            <form:form action="create-Wishlist" method="post" commandName="addToWishListForm">
                                <div class="item-attributes item__count hidden-xs"><span class="fa fa-plus fa-lg"></span></div>
                                <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong>${wishlistNameLabel}</strong></div>
                                <div class="item-attributes item__name">
                                   <form:input idKey="wishListName" class="form-control" path="wishListName" placeholder="${wishlistNameLabel}" mandatory="true" maxlength="100" data-qa-id="wishlist-name-textbox"/>
                                   <div class="error error-message hide" id="alertMsg">
                                       <spring:theme code="wishlist.specialCharacter.validation.message"/>
                                   </div>
                               </div>
                                <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong><spring:theme code="text.wishlistDescription"/></strong></div>
                                <div class="item-attributes item__info"><form:textarea data-qa-id="wishlist-description-textbox" idKey="wishListDescription" path="wishListDescription" class="form-control" rows="1" cols="50" maxlength="254" placeholder="${wishlistDescriptionLabel}"/></div>

                                <div class="item-attributes item__date"></div>
                                <div class="item-attributes item__actions">
                                    <button id="saveWishListForm" type="submit" disabled="true" class="btn btn-primary btn-block" data-qa-id="create-wishlist-btn">
                                        <span class="fa fa-plus-circle"></span> <spring:theme code="text.account.wishlist" text="Create WishList"/>
                                    </button>
                               </div>

                           </form:form>
                       </li>
                   </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="item__list item__list__cart js-ul-container" data-qa-id="wishlist-list">
                    <li class="hidden-xs hidden-sm">
                        <ul class="item__list--header">
                            <li class="item__count">#</li>
                            <li class="item__name"><spring:theme code="text.wishlistName" text="Name"/></li>
                            <li class="item__info"><spring:theme code="text.wishlistDescription" text="Description"/></li>
                            <li class="item__date"><spring:theme code="text.date" text="Date"/></li>
                            <li class="item__actions"><spring:theme code="text.wishlist.actions" text="Actions"/></li>
                        </ul>
                    </li>

                    <c:if test="${not empty wishLists}">
                        <c:forEach items="${wishLists}" var="wishlist" varStatus="loop">
                            <li class="item__list--item js-li-container">
                                <form:form class="edit-wishlist-form" action="/my-account/wishlist/${ycommerce:encodeUrl(wishlist.name)}" method="POST" commandName="addToWishListForm">
                                    <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong>#</strong></div>
                                    <div class="item-attributes item__count">${loop.index+1}</div>
                                    <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong><spring:theme code="text.wishlistName" text="Name"/></strong></div>
                                    <div class="item-attributes item__name">
                                        <div class="wishlist-value show-widget">
                                            <a href="${wishlistDetailsLink}${ycommerce:encodeUrl(wishlist.name)}"
                                            class="responsive-table-link" data-qa-id="wishlist-name">
                                                ${fn:escapeXml(wishlist.name)} </a>
                                        </div>
                                        <div class="wishlist-edit hide-widget">
                                            <form:input idKey="wishListName" class="form-control" path="wishListName" placeholder="${wishlistNameLabel}" mandatory="true" value="${fn:escapeXml(wishlist.name)}" maxlength="100"/>
                                        </div>
                                    </div>
                                    <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong><spring:theme code="text.wishlistDescription" text="Description"/></strong></div>
                                    <div class="item-attributes item__info"><div class="wishlist-value show-widget" data-qa-id="wishlist-description">${wishlist.description}</div>
                                        <div class="wishlist-edit hide-widget">
                                            <form:textarea idKey="wishListDescription" path="wishListDescription" class="form-control" rows="1" cols="50" maxlength="254" placeholder="${wishlistDescriptionLabel}"/>
                                        </div>
                                    </div>
                                    <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong><spring:theme code="text.date" text="Date"/></strong></div>
                                    <div class="item-attributes item__date" data-qa-id="wishlist-date">
                                        <fmt:formatDate value="${wishlist.modifiedDate}" dateStyle="medium" timeStyle="short" type="both" />
                                    </div>
                                    <div class="hidden-sm hidden-md hidden-lg item-attributes"><strong><spring:theme code="text.actions" text="Actions"/></strong></div>
                                    <div class="item-attributes item__actions">
                                        <div class="wishlist-value show-widget">
                                            <a href="#" class="js-edit-wishlist-toggle-on edit-item-link item-link" data-wishlist='{"wishListName": "${fn:escapeXml(fn:trim(wishlist.name))}", "wishListDescription": "${wishlist.description}"}' data-edit-popup-title="Edit Wishlist">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </a>
                                            <a data-qa-id="wishlist-remove-btn" href="#" class="js-delete-wishlist delete-item-link item-link" data-wishlist='{"wishListName": "${fn:escapeXml(fn:trim(wishlist.name))}", "wishListDescription": "${wishlist.description}"}' data-delete-popup-title="<spring:theme code="wishlist.page.delete.wishlist" text="Delete Wishlist"/>">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </div>
                                        <div class="wishlist-edit hide-widget">
                                            <a href="#" class="js-edit-wishlist edit-item-link item-link" data-wishlist='{"wishListName": "${fn:escapeXml(fn:trim(wishlist.name))}", "wishListDescription": "${wishlist.description}"}' data-edit-popup-title="Edit Wishlist">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </a>
                                            <a href="#" class="js-edit-wishlist-toggle-off edit-item-link item-link" >
                                                <span class="glyphicon glyphicon-repeat"></span>
                                            </a>
                                        </div>
                                    </div>

                                </form:form>
                            </li>
                         </c:forEach>
                     </c:if>
                </ul>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="deleteSummary" value="<spring:theme code="wishlist.page.delete.summary" text="The following wishlist will be deleted"/>">
<input type="hidden" name="deleteLabel" value="<spring:theme code="wishlist.page.delete.label" text="Delete"/>">
<input type="hidden" name="cancelLabel" value="<spring:theme code="text.cancel.label" text="Cancel"/>">
<input type="hidden" name="nameLabel" value="<spring:theme code="text.wishlistName"/>">
<input type="hidden" name="descriptionLabel" value="<spring:theme code="text.wishlistDescription"/>">

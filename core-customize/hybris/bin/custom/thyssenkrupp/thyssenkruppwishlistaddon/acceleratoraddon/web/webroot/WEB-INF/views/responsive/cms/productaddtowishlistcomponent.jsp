<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url value="/wishlist/createModal" var="wishlistPopupAction"/>
<c:url value="/login" var="loginUrl"/>
<c:choose>
    <c:when test="${not empty sessionScope.acceleratorSecureGUID}">
        <form:form method="post" id="addToWishListModalForm" class="add_to_wishlist_form span-5" action="${wishlistPopupAction}">
            <input type="hidden" name="productCodePost" value="${product.code}"/>
            <button id="addToWishListButton" type="submit" class="btn btn-link" data-qa-id="pdp-add-to-wishlist-btn" data-popup-title="<spring:theme code="text.addToWishList"/>" >
                <i class="fa fa-list"></i><spring:theme code="text.addToWishList"/>
            </button>
        </form:form>
    </c:when>
    <c:otherwise>
        <a id="addToWishListButton" href="${loginUrl}" class="btn btn-link">
            <i class="fa fa-list"></i><spring:theme code="text.addToWishList"/>
        </a>
    </c:otherwise>
</c:choose>

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppwishlistaddon.controllers.form;

import org.hibernate.validator.constraints.NotBlank;

public class AddToWishListForm {

    @NotBlank(message = "{error.wishlist.name.notBlank}")
    private String wishListName;
    private String wishListDescription;

    public String getWishListName() {
        return wishListName;
    }

    public void setWishListName(String wishListName) {
        this.wishListName = wishListName;
    }

    public String getWishListDescription() {
        return wishListDescription;
    }

    public void setWishListDescription(String wishListDescription) {
        this.wishListDescription = wishListDescription;
    }
}

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="favourite-entry"
    tagdir="/WEB-INF/tags/addons/thyssenkruppwishlistaddon/responsive/favourites"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<div class="account-section-header">
    <spring:theme code="text.account.favorite" text="Favourites" />
</div>

<jsp:useBean id="dummyDate" class="java.util.Date" scope="page" />

<c:if test="${ empty favouriteData.entries}">
    <div class="alert alert-danger alert-dismissable" data-qa-id="favourite-remove-success">
        <spring:theme code="favourite.empty.validation.message" />
        <button class="close" aria-hidden="true" data-dismiss="alert"
            type="button">x</button>
    </div>
</c:if>
<c:if test="${ not empty favouriteData.entries}">
<ul class="item__list item__list__cart" data-qa-id="favourites-list">
    <li class="hidden-xs hidden-sm">
        <ul class="item__list--header">
            <li class="item__toggle"></li>
            <li class="item__image"></li>
            <li class="item__info"> <spring:theme code="favourites.table.column.product_description" text="Product" /> </li>
            <li class="item__wishdeliverydate"><spring:theme code="favourites.table.column.addeddate" text="Added date" /></li>
            <li class="item__remove"></li>
        </ul>
    </li>

    <c:forEach items="${favouriteData.entries}" var="entry" varStatus="loop">
        <li class="item__list--item section-print favoriteNoborder">
            <favourite-entry:favouriteDetails favouriteEntry="${entry}"/>
        </li>
    </c:forEach>

</ul>
</c:if>
<input type="hidden" name="favouriteDeleteSummary" value="<spring:theme code="favourite.message.delete.summary" text="The following product will be deleted from Favourites"/>">
<input type="hidden" name="productNameLabel" value="<spring:theme code="text.productName" text="Product Name "/>">
<input type="hidden" name="productCodeLabel" value="<spring:theme code="text.productCode" text="Product Code"/>">
<input type="hidden" name="deleteLabel" value="<spring:theme code="text.delete.label" text="Delete"/>">
<input type="hidden" name="cancelLabel" value="<spring:theme code="text.cancel.label" text="Cancel"/>">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<c:url value="/my-account/add-favourite" var="favouriteAddAction"/>
<c:url value="/login" var="loginUrl"/>

<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
    <c:choose>
        <c:when test="${not empty sessionScope.acceleratorSecureGUID }">
            <c:if test="${! product.multidimensional}" >
                <div class="add-to-favorite">
                    <form:form method="post" id="addToFavForm" class="" action="${favouriteAddAction}">
                        <input type="hidden" name="productCode" value="${fn:toUpperCase(product.code)}"/>
                        <button id="addToFavButton" data-qa-id="plp-add-to-favourite-btn" type="submit" class="btn btn-primary btn-block ">
                            <i class="fa fa-heart"></i>
                        </button>
                    </form:form>
                </div>
            </c:if>
        </c:when>
        <c:otherwise>
            <c:if test="${! product.multidimensional}">
                <div class="add-to-favorite">
                    <a id="addToFavouritesButton" href="${loginUrl}" class="btn btn-primary btn-block">
                        <i class="fa fa-heart"></i>
                    </a>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
</sec:authorize>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url value="/my-account/add-favourite" var="favouriteAddAction"/>
<c:url value="/login" var="loginUrl"/>
<c:choose>
    <c:when test="${not empty sessionScope.acceleratorSecureGUID}">
        <form:form method="post" id="addToFavForm" class="" action="${favouriteAddAction}">
            <input type="hidden" name="productCode" value="${product.code}"/>
            <button id="addToFavButton" data-qa-id="pdp-add-to-favourite-btn" type="submit" class="btn btn-link">
                <i class="fa fa-heart-o"></i><spring:theme code="text.favourites.add"/>
            </button>
        </form:form>
    </c:when>
    <c:otherwise>
        <a id="addToWishListButton" href="${loginUrl}" class="btn btn-link">
            <i class="fa fa-heart-o"></i><spring:theme code="text.favourites.add"/>
        </a>
    </c:otherwise>
</c:choose>

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bConfiguratorSettingsService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.commerceservices.order.ProductConfigurationHandler;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.sap.sapmodel.services.SAPUnitService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil.stripTrailingZeros;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.lowerCase;

public class TkCutToLengthProductInfoConfigurationHandler implements ProductConfigurationHandler {

    private ModelService                     modelService;
    private TkB2bConfiguratorSettingsService configuratorSettingsService;
    private SAPUnitService                   unitService;

    @Override
    public List<AbstractOrderEntryProductInfoModel> createProductInfo(AbstractConfiguratorSettingModel productSettings) {
        return emptyList();
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> convert(final Collection<ProductConfigurationItem> items, final AbstractOrderEntryModel entry) {
        return emptyIfNull(items).stream().filter(o -> CUTTOLENGTH_PRODINFO == o.getConfiguratorType()).map(o -> generateProductInfoModel(o, entry))
            .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
    }

    protected Optional<AbstractOrderEntryProductInfoModel> generateProductInfoModel(final ProductConfigurationItem item, final AbstractOrderEntryModel entry) {
        final Optional<AbstractConfiguratorSettingModel> maybeSettingModel = getConfiguratorSettingsService().getConfiguratorSettingById(item.getCutToLengthTolerance());
        if (maybeSettingModel.filter(TkCutToLengthConfiguratorSettingsModel.class::isInstance).isPresent()) {
            final TkCutToLengthConfiguredProductInfoModel c2lProductInfoModel = createCutToLengthConfiguredProductInfoModel(entry);
            populateProductInfo(item, c2lProductInfoModel, TkCutToLengthConfiguratorSettingsModel.class.cast(maybeSettingModel.get()));
            return Optional.ofNullable(c2lProductInfoModel);
        }
        return Optional.empty();
    }

    protected TkCutToLengthConfiguredProductInfoModel createCutToLengthConfiguredProductInfoModel(final AbstractOrderEntryModel entry) {
        return getModelService().create(TkCutToLengthConfiguredProductInfoModel.class);
    }

    protected void populateProductInfo(ProductConfigurationItem item, TkCutToLengthConfiguredProductInfoModel tkCutToLengthConfiguredProductInfoModel, TkCutToLengthConfiguratorSettingsModel tkCutToLengthConfiguratorSettings) {
        final String toleranceDisplayValue = tkCutToLengthConfiguratorSettings.getToleranceDisplayValue();
        final String label = createCartLabelFrom(item, toleranceDisplayValue);

        tkCutToLengthConfiguredProductInfoModel.setChecked(true);
        tkCutToLengthConfiguredProductInfoModel.setLabel(label);
        tkCutToLengthConfiguredProductInfoModel.setConfiguratorType(CUTTOLENGTH_PRODINFO);
        final String rangeUnit = item.getCutToLengthRangeUnit();
        if (isNotEmpty(rangeUnit)) {
            tkCutToLengthConfiguredProductInfoModel.setUnit(getUnitService().getUnitForSAPCode(rangeUnit));
        }
        tkCutToLengthConfiguredProductInfoModel.setValue(item.getCutToLengthRange());
        tkCutToLengthConfiguredProductInfoModel.setTolerance(toleranceDisplayValue);
        tkCutToLengthConfiguredProductInfoModel.setToleranceValue(item.getCutToLengthTolerance());
        tkCutToLengthConfiguredProductInfoModel.setSapExportValue(tkCutToLengthConfiguratorSettings.getSapExportValue());
    }

    protected String createCartLabelFrom(ProductConfigurationItem item, String toleranceDisplayValue) {
        return stripTrailingZeros(item.getCutToLengthRange()) + lowerCase(item.getCutToLengthRangeUnit()) + " " + toleranceDisplayValue;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public TkB2bConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkB2bConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public SAPUnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(SAPUnitService unitService) {
        this.unitService = unitService;
    }
}

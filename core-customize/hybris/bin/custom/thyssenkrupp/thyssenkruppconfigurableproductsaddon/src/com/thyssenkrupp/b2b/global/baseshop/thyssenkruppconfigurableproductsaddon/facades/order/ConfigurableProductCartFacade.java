package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.order;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.List;
import java.util.Map;

public interface ConfigurableProductCartFacade extends CartFacade {

    CartModificationData addToCart(String code, long quantity, List<ConfigurationInfoData> configurationInfoData) throws CommerceCartModificationException;

    List<ConfigurationInfoData> getConfigurationInfoData(Map<ConfiguratorType, Map<String, String>> form);
}

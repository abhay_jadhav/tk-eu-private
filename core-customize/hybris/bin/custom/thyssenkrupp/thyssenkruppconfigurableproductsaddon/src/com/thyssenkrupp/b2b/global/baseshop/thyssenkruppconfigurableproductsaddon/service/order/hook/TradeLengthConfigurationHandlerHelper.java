/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TRADE_LENGTH_CATEGORY_CODE;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

public class TradeLengthConfigurationHandlerHelper {

    private TkB2bCategoryService categoryService;
    private CommonI18NService    commonI18NService;

    public String generateTradeLengthCodeFromProductInfo(TkTradeLengthConfiguredProductInfoModel productInfo, AbstractOrderEntryModel entry) {
        return getCategoryService().buildVariantValueCategoryCode(generateVariantValueCodePattern(productInfo), getTradeLengthCategoryModel(entry));
    }

    private String generateVariantValueCodePattern(TkTradeLengthConfiguredProductInfoModel productInfo) {
        return getCategoryService().getVariantValueCodePatternForTradeLength(productInfo);
    }

    private VariantCategoryModel getTradeLengthCategoryModel(AbstractOrderEntryModel entry) {
        Optional<VariantValueCategoryModel> vvcModel = entry.getProduct().getSupercategories().stream()
            .filter(category -> VariantValueCategoryModel._TYPECODE.equals(category.getItemtype()))
            .map(category -> (VariantValueCategoryModel) category)
            .filter(category -> category.getCode().contains(TRADE_LENGTH_CATEGORY_CODE))
            .findFirst();

        return (VariantCategoryModel) vvcModel.map(VariantValueCategoryModel::getSupercategories).get().stream().findFirst().orElse(null);
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public TkB2bCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(TkB2bCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

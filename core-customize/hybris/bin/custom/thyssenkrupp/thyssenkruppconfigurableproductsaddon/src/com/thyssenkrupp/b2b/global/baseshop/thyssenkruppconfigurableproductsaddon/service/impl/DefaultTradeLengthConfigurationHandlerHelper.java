package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.TradeLengthConfigurationHandlerHelper;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Optional;

public class DefaultTradeLengthConfigurationHandlerHelper implements TradeLengthConfigurationHandlerHelper {

    private static final String TRADE_LENGTH = "Trade_Length";
    private TkB2bCategoryService categoryService;

    @Override
    public String generateTradeLengthCodeFromProductInfo(TkTradeLengthConfiguredProductInfoModel productInfo, AbstractOrderEntryModel entry) {
        return getCategoryService().buildVariantValueCategoryCode(generateVariantValueCodePattern(productInfo), getTradeLengthCategoryModel(entry));
    }

    private String generateVariantValueCodePattern(TkTradeLengthConfiguredProductInfoModel productInfo) {
        return getCategoryService().getVariantValueCodePatternForTradeLength(productInfo);
    }

    private VariantCategoryModel getTradeLengthCategoryModel(AbstractOrderEntryModel entry) {
        Optional<VariantValueCategoryModel> vvcModel = entry.getProduct().getSupercategories().stream()
          .filter(category -> VariantValueCategoryModel._TYPECODE.equals(category.getItemtype()))
          .map(category -> (VariantValueCategoryModel) category)
          .filter(category -> category.getCode().contains(TRADE_LENGTH))
          .findFirst();

        return (VariantCategoryModel) vvcModel.map(VariantValueCategoryModel::getSupercategories).get().stream().findFirst().orElse(null);

    }

    public TkB2bCategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(TkB2bCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

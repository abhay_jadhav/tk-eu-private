package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.populators;

import java.util.List;

import com.thyssenkrupp.b2b.eu.facades.product.data.UnitData;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkTradeLengthConfigurationsPopulator<T extends AbstractOrderEntryProductInfoModel> implements Populator<T, List<ConfigurationInfoData>> {

    private Converter<UnitModel, UnitData> productUnitConverter;

    public Converter<UnitModel, UnitData> getProductUnitConverter() {
        return productUnitConverter;
    }

    public void setProductUnitConverter(Converter<UnitModel, UnitData> productUnitConverter) {
        this.productUnitConverter = productUnitConverter;
    }

    @Override
    public void populate(T source, List<ConfigurationInfoData> target) throws ConversionException {
        if (source.getConfiguratorType() == ConfiguratorType.TKTRADELENGTH) {
            if (!(source instanceof TkTradeLengthConfiguredProductInfoModel)) {
                throw new ConversionException("Instance with type " + source.getConfiguratorType() + " is of class" + source.getClass().getName() + " which is not convertible to " + TkTradeLengthConfiguredProductInfoModel.class.getName());
            }

            final ConfigurationInfoData item = new ConfigurationInfoData();
            final TkTradeLengthConfiguredProductInfoModel model = (TkTradeLengthConfiguredProductInfoModel) source;
            item.setUniqueId(model.getLabel());
            item.setConfigurationLabel(model.getDisplayValue());
            item.setStatus(source.getProductInfoStatus());
            item.setConfiguratorType(model.getConfiguratorType());
            item.setConfigurationValue(String.valueOf(model.isChecked()));
            item.setLengthValue(String.valueOf(model.getLengthValue()));
            item.setUnit(getProductUnitConverter().convert(model.getUnit()));
            target.add(item);

        }
    }

}

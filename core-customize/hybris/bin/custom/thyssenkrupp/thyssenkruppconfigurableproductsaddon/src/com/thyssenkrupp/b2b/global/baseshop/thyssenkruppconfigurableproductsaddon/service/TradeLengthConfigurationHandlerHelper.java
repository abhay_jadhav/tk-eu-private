package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public interface TradeLengthConfigurationHandlerHelper {
     String generateTradeLengthCodeFromProductInfo(TkTradeLengthConfiguredProductInfoModel productInfo, AbstractOrderEntryModel entry);
}

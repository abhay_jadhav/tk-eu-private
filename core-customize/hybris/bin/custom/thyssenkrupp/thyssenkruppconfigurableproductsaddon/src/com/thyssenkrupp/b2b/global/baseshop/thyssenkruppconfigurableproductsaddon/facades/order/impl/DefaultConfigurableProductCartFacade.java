package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.order.impl;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.order.ConfigurableProductCartFacade;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CERTIFICATE_CONFIG_LABEL_ID_SEPARATOR;

public class DefaultConfigurableProductCartFacade extends DefaultCartFacade implements ConfigurableProductCartFacade {
    @Override
    public CartModificationData addToCart(String code, long quantity, List<ConfigurationInfoData> configurationInfoData) throws CommerceCartModificationException {
        final AddToCartParams params = new AddToCartParams();
        params.setProductCode(code);
        params.setQuantity(quantity);
        params.setProductConfiguration(configurationInfoData);
        return addToCart(params);
    }

    @Override
    public List<ConfigurationInfoData> getConfigurationInfoData(Map<ConfiguratorType, Map<String, String>> form) {
        final List<ConfigurationInfoData> configurationInfoDataList = new ArrayList<>();
        if (form != null) {
            for (final Map.Entry<ConfiguratorType, Map<String, String>> item : form.entrySet()) {
                item.getValue().entrySet().stream().map(formEntry -> prepareConfigurationInfoData(item, formEntry)).forEach(configurationInfoDataList::add);
            }
        }
        return configurationInfoDataList;
    }

    protected ConfigurationInfoData prepareConfigurationInfoData(final Map.Entry<ConfiguratorType, Map<String, String>> item, final Map.Entry<String, String> formEntry) {
        ConfigurationInfoData configurationInfoData = new ConfigurationInfoData();
        configurationInfoData.setConfigurationLabel(formEntry.getKey().split(CERTIFICATE_CONFIG_LABEL_ID_SEPARATOR)[0]);
        configurationInfoData.setConfigurationValue(formEntry.getValue());
        configurationInfoData.setUniqueId(formEntry.getKey().split(CERTIFICATE_CONFIG_LABEL_ID_SEPARATOR)[1]);
        configurationInfoData.setConfiguratorType(item.getKey());
        configurationInfoData.setStatus(ProductInfoStatus.SUCCESS);
        return configurationInfoData;
    }

    @Override
    protected ProductConfigurationItem configurationInfoToProductConfiguration(final ConfigurationInfoData configurationInfoData) {
        final ProductConfigurationItem productConfigurationItem = new ProductConfigurationItem();
        productConfigurationItem.setKey(configurationInfoData.getConfigurationLabel());
        productConfigurationItem.setUniqueId(configurationInfoData.getUniqueId());
        productConfigurationItem.setValue(configurationInfoData.getConfigurationValue());
        productConfigurationItem.setStatus(configurationInfoData.getStatus());
        productConfigurationItem.setConfiguratorType(configurationInfoData.getConfiguratorType());

        return productConfigurationItem;
    }
}

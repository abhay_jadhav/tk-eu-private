package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import de.hybris.platform.commerceservices.order.ProductConfigurationHandler;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;

import java.util.Collection;

public interface TkProductConfigurationHandler extends ProductConfigurationHandler {

    void updateOrderEntryWithProductInfo(Collection<ProductConfigurationItem> productConfigurationItems, AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel, AbstractOrderEntryModel entry);
}

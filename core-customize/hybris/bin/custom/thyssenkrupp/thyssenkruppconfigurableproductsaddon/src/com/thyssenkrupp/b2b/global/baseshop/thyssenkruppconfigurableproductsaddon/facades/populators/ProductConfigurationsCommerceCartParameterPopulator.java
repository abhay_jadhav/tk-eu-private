package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.populators;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ProductConfigurationsCommerceCartParameterPopulator implements Populator<AddToCartParams, CommerceCartParameter> {
    @Override
    public void populate(AddToCartParams addToCartParams, CommerceCartParameter commerceCartParameter) throws ConversionException {
        Collection<ConfigurationInfoData> productConfiguration = addToCartParams.getProductConfiguration();

        if(CollectionUtils.isNotEmpty(productConfiguration)) {
            List<ProductConfigurationItem> productConfigurationItems = productConfiguration.stream().map(configurationInfoData -> {
                final ProductConfigurationItem productConfigurationItem = new ProductConfigurationItem();
                productConfigurationItem.setKey(configurationInfoData.getConfigurationLabel());
                productConfigurationItem.setUniqueId(configurationInfoData.getUniqueId());
                productConfigurationItem.setValue(configurationInfoData.getConfigurationValue());
                productConfigurationItem.setStatus(configurationInfoData.getStatus());
                productConfigurationItem.setConfiguratorType(configurationInfoData.getConfiguratorType());
                productConfigurationItem.setCutToLengthRange(configurationInfoData.getCutToLengthRange());
                productConfigurationItem.setCutToLengthRangeUnit(configurationInfoData.getCutToLengthRangeUnit());
                productConfigurationItem.setCutToLengthTolerance(configurationInfoData.getCutToLengthTolerance());
                return productConfigurationItem;
            }).collect(Collectors.toList());

            commerceCartParameter.setProductConfiguration(productConfigurationItems);
        }
    }
}

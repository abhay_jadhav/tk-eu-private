package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TkLengthConfigurationHandler implements TkProductConfigurationHandler {

    private ModelService modelService;

    @Override
    public void updateOrderEntryWithProductInfo(Collection<ProductConfigurationItem> productConfigurationItems, AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel, AbstractOrderEntryModel entry) {
        //empty implementation
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> createProductInfo(AbstractConfiguratorSettingModel productSettings) {
        if (!(productSettings instanceof TkLengthConfiguratorSettingsModel)) {
            throw new IllegalArgumentException("Argument must be type of TkLengthConfiguratorSettingsModel");
        }
        final TkLengthConfiguratorSettingsModel settingsModel = (TkLengthConfiguratorSettingsModel) productSettings;
        final TkLengthConfiguredProductInfoModel tkLengthConfiguredProductInfoModel = modelService.create(TkLengthConfiguredProductInfoModel.class);
        tkLengthConfiguredProductInfoModel.setLabel(settingsModel.getLabel());
        tkLengthConfiguredProductInfoModel.setLengthValue(settingsModel.getLengthValue());
        tkLengthConfiguredProductInfoModel.setConfiguratorType(ConfiguratorType.TKCCLENGTH);
        tkLengthConfiguredProductInfoModel.setUnit(settingsModel.getUnit());
        tkLengthConfiguredProductInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        tkLengthConfiguredProductInfoModel.setChecked(settingsModel.isChecked());
        return Collections.singletonList(tkLengthConfiguredProductInfoModel);
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> convert(Collection<ProductConfigurationItem> items, AbstractOrderEntryModel entry) {
        return Collections.emptyList();
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}

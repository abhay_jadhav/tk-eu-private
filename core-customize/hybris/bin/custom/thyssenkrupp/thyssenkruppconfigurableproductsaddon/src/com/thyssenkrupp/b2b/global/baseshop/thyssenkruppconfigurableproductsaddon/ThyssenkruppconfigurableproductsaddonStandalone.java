package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppconfigurableproductsaddonStandalone {

    public static void main(final String[] args) {
        new ThyssenkruppconfigurableproductsaddonStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        System.out.println("Session ID: " + jaloSession.getSessionID()); //NOPMD
        System.out.println("User: " + jaloSession.getUser()); //NOPMD
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}

/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CHECKBOX_CHECKED_VALUE;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.model.ModelService;

public class CertificateConfigurationHandler implements TkProductConfigurationHandler {

    private ModelService modelService;

    @Override
    public List<AbstractOrderEntryProductInfoModel> createProductInfo(AbstractConfiguratorSettingModel productSettings) {

        if (!(productSettings instanceof CertificateConfiguratorSettingsModel)) {
            throw new IllegalArgumentException("Argument must be type of CertificateConfiguratorSettingModel");
        }

        final CertificateConfiguratorSettingsModel certificateConfiguratorSettingsModel = (CertificateConfiguratorSettingsModel) productSettings;
        final CertificateConfiguredProductInfoModel certificateConfiguredProductInfoModel = modelService.create(CertificateConfiguredProductInfoModel.class);
        certificateConfiguredProductInfoModel.setCertificateId(certificateConfiguratorSettingsModel.getCertificateId());
        certificateConfiguredProductInfoModel.setName(certificateConfiguratorSettingsModel.getName());
        certificateConfiguredProductInfoModel.setConfiguratorType(ConfiguratorType.CERTIFICATE);
        certificateConfiguredProductInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        certificateConfiguredProductInfoModel.setChecked(certificateConfiguratorSettingsModel.isChecked());
        certificateConfiguredProductInfoModel.setDescription(certificateConfiguratorSettingsModel.getDescription());
        return Collections.singletonList(certificateConfiguredProductInfoModel);
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> convert(Collection<ProductConfigurationItem> items, AbstractOrderEntryModel entry) {
        return items.stream().map(item -> {
            final CertificateConfiguredProductInfoModel certificateConfiguredProductInfoModel = modelService.create(CertificateConfiguredProductInfoModel.class);
            certificateConfiguredProductInfoModel.setName(item.getKey());
            certificateConfiguredProductInfoModel.setCertificateId(item.getUniqueId());
            certificateConfiguredProductInfoModel.setConfiguratorType(ConfiguratorType.CERTIFICATE);
            certificateConfiguredProductInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
            certificateConfiguredProductInfoModel.setChecked(Boolean.valueOf(item.getValue().toString()));
            return certificateConfiguredProductInfoModel;
        }).collect(Collectors.toList());
    }

    @Override
    public void updateOrderEntryWithProductInfo(@NotNull Collection<ProductConfigurationItem> productConfigurationItems, @NotNull AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel, AbstractOrderEntryModel entry) {
        CertificateConfiguredProductInfoModel certificateConfiguredProductInfoModel = (CertificateConfiguredProductInfoModel) abstractOrderEntryProductInfoModel;
        productConfigurationItems.stream().filter(productConfigurationItem -> productConfigurationItem.getConfiguratorType() == ConfiguratorType.CERTIFICATE)
          .filter(productConfigurationItem -> productConfigurationItem.getUniqueId().equals(certificateConfiguredProductInfoModel.getCertificateId()))
          .forEach(productConfigurationItem -> {
              certificateConfiguredProductInfoModel.setChecked(CHECKBOX_CHECKED_VALUE.equals(productConfigurationItem.getValue()));
              getModelService().save(certificateConfiguredProductInfoModel);
          });
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}

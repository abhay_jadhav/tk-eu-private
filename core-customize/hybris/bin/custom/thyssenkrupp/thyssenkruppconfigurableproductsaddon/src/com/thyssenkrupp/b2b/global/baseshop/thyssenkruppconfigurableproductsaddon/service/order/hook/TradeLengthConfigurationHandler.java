package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CHECKBOX_CHECKED_VALUE;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKTRADELENGTH;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class TradeLengthConfigurationHandler implements TkProductConfigurationHandler {

    protected I18NService                           i18nService;
    private   ModelService                          modelService;
    private   CommonI18NService                     commonI18NService;
    private   TradeLengthConfigurationHandlerHelper tradeLengthConfigurationHandlerHelper;

    @Override
    public List<AbstractOrderEntryProductInfoModel> createProductInfo(AbstractConfiguratorSettingModel productSettings) {

        if (!(productSettings instanceof TkTradeLengthConfiguratorSettingsModel)) {
            throw new IllegalArgumentException("Argument must be type of TkTradeLengthConfiguratorSettingModel");
        }

        final TkTradeLengthConfiguratorSettingsModel tkTradeLengthConfiguratorSettingsModel = (TkTradeLengthConfiguratorSettingsModel) productSettings;
        final TkTradeLengthConfiguredProductInfoModel tkTradeLengthConfiguredProductInfoModel = modelService.create(TkTradeLengthConfiguredProductInfoModel.class);
        tkTradeLengthConfiguredProductInfoModel.setLabel(tkTradeLengthConfiguratorSettingsModel.getLabel());
        setProductInfoDisplayValue(tkTradeLengthConfiguratorSettingsModel, tkTradeLengthConfiguredProductInfoModel);
        tkTradeLengthConfiguredProductInfoModel.setLengthValue(tkTradeLengthConfiguratorSettingsModel.getLengthValue());
        tkTradeLengthConfiguredProductInfoModel.setConfiguratorType(TKTRADELENGTH);
        tkTradeLengthConfiguredProductInfoModel.setUnit(tkTradeLengthConfiguratorSettingsModel.getUnit());
        tkTradeLengthConfiguredProductInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        tkTradeLengthConfiguredProductInfoModel.setChecked(tkTradeLengthConfiguratorSettingsModel.isChecked());
        return Collections.singletonList(tkTradeLengthConfiguredProductInfoModel);
    }

    private void setProductInfoDisplayValue(TkTradeLengthConfiguratorSettingsModel tkTradeLengthConfiguratorSettingsModel, TkTradeLengthConfiguredProductInfoModel tkTradeLengthConfiguredProductInfoModel) {
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            final String displayName = tkTradeLengthConfiguratorSettingsModel.getDisplayValue(locale);
            if (isNotBlank(displayName)) {
                tkTradeLengthConfiguredProductInfoModel.setDisplayValue(displayName, locale);
            }
        }
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> convert(Collection<ProductConfigurationItem> items, AbstractOrderEntryModel entry) {
        return items.stream().map(item -> {
            final TkTradeLengthConfiguredProductInfoModel tkTradeLengthConfiguredProductInfoModel = modelService.create(TkTradeLengthConfiguredProductInfoModel.class);
            tkTradeLengthConfiguredProductInfoModel.setLabel(item.getKey());
            tkTradeLengthConfiguredProductInfoModel.setDisplayValue(item.getUniqueId());
            tkTradeLengthConfiguredProductInfoModel.setConfiguratorType(TKTRADELENGTH);
            tkTradeLengthConfiguredProductInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
            tkTradeLengthConfiguredProductInfoModel.setChecked(Boolean.valueOf(item.getValue().toString()));
            return tkTradeLengthConfiguredProductInfoModel;
        }).collect(Collectors.toList());
    }

    @Override
    public void updateOrderEntryWithProductInfo(Collection<ProductConfigurationItem> productConfigurationItems, AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel, AbstractOrderEntryModel entry) {
        TkTradeLengthConfiguredProductInfoModel tkTradeLengthConfiguredProductInfoModel = (TkTradeLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel;
        emptyIfNull(productConfigurationItems).stream()
            .filter(productConfigurationItem -> productConfigurationItem.getConfiguratorType() == TKTRADELENGTH)
            .filter(productConfigurationItem -> productConfigurationItem.getUniqueId().equals(generateTradeLengthCodeFromProductInfo(tkTradeLengthConfiguredProductInfoModel, entry)))
            .forEach(productConfigurationItem -> tkTradeLengthConfiguredProductInfoModel.setChecked(CHECKBOX_CHECKED_VALUE.equals(productConfigurationItem.getValue())));
        getModelService().save(tkTradeLengthConfiguredProductInfoModel);
    }

    private String generateTradeLengthCodeFromProductInfo(TkTradeLengthConfiguredProductInfoModel productInfo, AbstractOrderEntryModel entry) {
        return getTradeLengthConfigurationHandlerHelper().generateTradeLengthCodeFromProductInfo(productInfo, entry);
    }

    public TradeLengthConfigurationHandlerHelper getTradeLengthConfigurationHandlerHelper() {
        return tradeLengthConfigurationHandlerHelper;
    }

    @Required
    public void setTradeLengthConfigurationHandlerHelper(TradeLengthConfigurationHandlerHelper tradeLengthConfigurationHandlerHelper) {
        this.tradeLengthConfigurationHandlerHelper = tradeLengthConfigurationHandlerHelper;
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }
}

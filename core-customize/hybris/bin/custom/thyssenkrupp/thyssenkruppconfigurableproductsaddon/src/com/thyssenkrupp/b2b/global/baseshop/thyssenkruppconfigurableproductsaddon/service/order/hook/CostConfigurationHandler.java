package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CostConfigurationHandler implements TkProductConfigurationHandler {
    @Override
    public void updateOrderEntryWithProductInfo(Collection<ProductConfigurationItem> productConfigurationItems, AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel, AbstractOrderEntryModel entry) {
        //not used since value is not changing
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> createProductInfo(AbstractConfiguratorSettingModel productSettings) {
        return Collections.emptyList();
    }

    @Override
    public List<AbstractOrderEntryProductInfoModel> convert(Collection<ProductConfigurationItem> items, AbstractOrderEntryModel entry) {
        return Collections.emptyList();
    }
}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.impl.ConfigurableProductAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class TkConfigurableProductAddToCartMethodHook extends ConfigurableProductAddToCartMethodHook {

    @Override
    public void afterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result) throws CommerceCartModificationException {
        if (result.getQuantityAdded() > 0) {
            super.afterAddToCart(parameters, result);
            final Collection<ProductConfigurationItem> productConfiguration = parameters.getProductConfiguration();
            updateOrderEntryWithProductInfo(result, productConfiguration, tkProductHandlerPredicate(), castIntoTkProductConfigurationHandlerFuntion());
        }
    }

    public void updateOrderEntryWithProductInfo(final CommerceCartModification result, final Collection<ProductConfigurationItem> productConfiguration, final Predicate<AbstractOrderEntryProductInfoModel> tkProductHandler, final Function<AbstractOrderEntryProductInfoModel, TkProductConfigurationHandler> tkProductConfigurationHandler) {
        final List<AbstractOrderEntryProductInfoModel> productInfos = result.getEntry().getProductInfos();

        if (CollectionUtils.isNotEmpty(productConfiguration) && CollectionUtils.isNotEmpty(productInfos)) {
            productInfos.stream().filter(tkProductHandler).forEach(abstractOrderEntryProductInfoModel -> tkProductConfigurationHandler.apply(abstractOrderEntryProductInfoModel).updateOrderEntryWithProductInfo(productConfiguration, abstractOrderEntryProductInfoModel, result.getEntry()));
        }
    }

    protected Function<AbstractOrderEntryProductInfoModel, TkProductConfigurationHandler> castIntoTkProductConfigurationHandlerFuntion() {
        return abstractOrderEntryProductInfoModel -> (TkProductConfigurationHandler) getConfigurationFactory().handlerOf(abstractOrderEntryProductInfoModel.getConfiguratorType());
    }

    protected Predicate<AbstractOrderEntryProductInfoModel> tkProductHandlerPredicate() {
        return abstractOrderEntryProductInfoModel -> abstractOrderEntryProductInfoModel != null && getConfigurationFactory().handlerOf(abstractOrderEntryProductInfoModel.getConfiguratorType()) instanceof TkProductConfigurationHandler;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.populators;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

public class CertificateConfigurationsPopulator<T extends AbstractOrderEntryProductInfoModel> implements Populator<T, List<ConfigurationInfoData>> {
    @Override
    public void populate(T source, List<ConfigurationInfoData> target) throws ConversionException {
        if (source.getConfiguratorType() == ConfiguratorType.CERTIFICATE) {
            if (!(source instanceof CertificateConfiguredProductInfoModel)) {
                throw new ConversionException("Instance with type " + source.getConfiguratorType() + " is of class" + source.getClass().getName() + " which is not convertible to " + CertificateConfiguredProductInfoModel.class.getName());
            }

            final ConfigurationInfoData item = new ConfigurationInfoData();
            final CertificateConfiguredProductInfoModel model= (CertificateConfiguredProductInfoModel)source;
            item.setUniqueId(model.getCertificateId());
            item.setConfigurationLabel(model.getName());
            item.setDescription(model.getDescription());
            item.setStatus(source.getProductInfoStatus());
            item.setConfiguratorType(model.getConfiguratorType());
            item.setConfigurationValue(String.valueOf(model.isChecked()));

            target.add(item);

        }
    }
}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import de.hybris.platform.commerceservices.order.EntryMergeFilter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.stream.Collectors;

public class TkEntryMergeFilterConfigurableProduct implements EntryMergeFilter {
    @Override
    public Boolean apply(@Nonnull AbstractOrderEntryModel candidate, @Nonnull AbstractOrderEntryModel target) {
        final boolean candidateProcessingOptionsNotEmpty = CollectionUtils.isNotEmpty(candidate.getProductInfos());
        final boolean targetProcessingOptionsNotEmpty = CollectionUtils.isNotEmpty(target.getProductInfos());

        if (candidateProcessingOptionsNotEmpty && targetProcessingOptionsNotEmpty) {
            Map<String, Boolean> candidateMap = candidate.getProductInfos().stream().filter(abstractOrderEntryProductInfoModel -> abstractOrderEntryProductInfoModel instanceof CertificateConfiguredProductInfoModel).map(abstractOrderEntryProductInfoModel -> (CertificateConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).collect(Collectors.toMap(CertificateConfiguredProductInfoModel::getCertificateId, CertificateConfiguredProductInfoModel::isChecked));

            Map<String, Boolean> targetMap = target.getProductInfos().stream().filter(abstractOrderEntryProductInfoModel -> abstractOrderEntryProductInfoModel instanceof CertificateConfiguredProductInfoModel).map(abstractOrderEntryProductInfoModel -> (CertificateConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).collect(Collectors.toMap(CertificateConfiguredProductInfoModel::getCertificateId, CertificateConfiguredProductInfoModel::isChecked));

            return candidateMap.equals(targetMap);
        } else if ((!candidateProcessingOptionsNotEmpty && targetProcessingOptionsNotEmpty) || (candidateProcessingOptionsNotEmpty && !targetProcessingOptionsNotEmpty)) {
            return false;
        }
        return true;
    }
}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants;

public final class ThyssenkruppconfigurableproductsaddonConstants extends GeneratedThyssenkruppconfigurableproductsaddonConstants {
    public static final String EXTENSIONNAME          = "thyssenkruppconfigurableproductsaddon";
    public static final String CHECKBOX_CHECKED_VALUE = "on";
    public static final String CERTIFICATE_CONFIG_LABEL_ID_SEPARATOR = "##";

    private ThyssenkruppconfigurableproductsaddonConstants() {
    }

}

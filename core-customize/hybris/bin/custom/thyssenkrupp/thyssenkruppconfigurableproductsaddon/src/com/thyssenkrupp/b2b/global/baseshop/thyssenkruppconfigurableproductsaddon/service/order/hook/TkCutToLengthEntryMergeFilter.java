package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.commerceservices.order.EntryMergeFilter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.stream.Collectors;

public class TkCutToLengthEntryMergeFilter implements EntryMergeFilter {
    @Override
    public Boolean apply(@Nonnull AbstractOrderEntryModel source, @Nonnull AbstractOrderEntryModel target) {
        final boolean sourceProcessingOptionsNotEmpty = CollectionUtils.isNotEmpty(source.getProductInfos());
        final boolean targetProcessingOptionsNotEmpty = CollectionUtils.isNotEmpty(target.getProductInfos());

        if (sourceProcessingOptionsNotEmpty && targetProcessingOptionsNotEmpty) {
            Map<String, Double> sourceMap = collectAsMap(source);
            Map<String, Double> targetMap = collectAsMap(target);

            return sourceMap.equals(targetMap);
        } else if ((!sourceProcessingOptionsNotEmpty && targetProcessingOptionsNotEmpty) || (sourceProcessingOptionsNotEmpty && !targetProcessingOptionsNotEmpty)) {
            return false;
        }
        return true;
    }

    protected Map<String, Double> collectAsMap(@Nonnull AbstractOrderEntryModel orderEntryModel) {
        return orderEntryModel.getProductInfos().stream().filter(TkCutToLengthConfiguredProductInfoModel.class::isInstance).map(TkCutToLengthConfiguredProductInfoModel.class::cast).collect(Collectors.toMap(TkCutToLengthConfiguredProductInfoModel::getToleranceValue, TkCutToLengthConfiguredProductInfoModel::getValue));
    }
}

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.interceptor;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkRangeConfiguratorSettingsModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.springframework.beans.factory.annotation.Required;

public class TkRangeConfiguratorSettingsInterceptor implements PrepareInterceptor {

    private UnitService unitService;

    @Override
    public void onPrepare(Object object, InterceptorContext interceptorContext) throws InterceptorException {

        TkRangeConfiguratorSettingsModel tkRangeConfiguratorModel = (TkRangeConfiguratorSettingsModel) object;
        if (tkRangeConfiguratorModel.getUnit() == null) {

            tkRangeConfiguratorModel.setUnit(getUnitService().getUnitForCode("MTR"));
        }
        interceptorContext.registerElementFor(object, PersistenceOperation.SAVE);
    }

    @Required
    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }
}

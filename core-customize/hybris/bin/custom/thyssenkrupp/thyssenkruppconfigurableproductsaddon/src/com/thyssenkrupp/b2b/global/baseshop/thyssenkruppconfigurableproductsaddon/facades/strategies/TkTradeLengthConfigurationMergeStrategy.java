package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.strategies;

import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.strategies.merge.ProductConfigurationMergeStrategy;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CHECKBOX_CHECKED_VALUE;

public class TkTradeLengthConfigurationMergeStrategy implements ProductConfigurationMergeStrategy {
    @Nonnull
    @Override
    public List<ConfigurationInfoData> merge(@Nonnull final List<ConfigurationInfoData> firstConfiguration, @Nonnull final List<ConfigurationInfoData> secondConfiguration) {

        List<String> formLabels = firstConfiguration.stream()
            .peek(this::setConfigurationValueToBooleanString)
            .map(ConfigurationInfoData::getUniqueId)
            .collect(Collectors.toList());

        List<ConfigurationInfoData> checkboxConfiguration = secondConfiguration.stream()
            .filter(checkBoxConfiguration -> !formLabels.contains(checkBoxConfiguration.getUniqueId()))
            .collect(Collectors.toList());

        checkboxConfiguration.stream()
            .forEach(configuration -> configuration.setConfigurationValue(Boolean.FALSE.toString()));

        return checkboxConfiguration;
    }

    protected void setConfigurationValueToBooleanString(ConfigurationInfoData configurationInfoData) {
        configurationInfoData.setConfigurationValue(
            Boolean.valueOf(CHECKBOX_CHECKED_VALUE.equals(configurationInfoData.getConfigurationValue())).toString());
    }
}

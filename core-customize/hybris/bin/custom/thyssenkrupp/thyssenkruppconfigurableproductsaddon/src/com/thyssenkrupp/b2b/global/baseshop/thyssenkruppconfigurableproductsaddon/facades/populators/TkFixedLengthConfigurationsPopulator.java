package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.populators;

import java.util.List;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkFixedLengthConfigurationsPopulator<T extends AbstractOrderEntryProductInfoModel> implements Populator<T, List<ConfigurationInfoData>> {
    private TkEuB2bProductService b2bProductService;
    @Override
    public void populate(T source, List<ConfigurationInfoData> target) throws ConversionException {
        if (source.getConfiguratorType() == ConfiguratorType.TKCCLENGTH) {
            if (!(source instanceof TkLengthConfiguredProductInfoModel)) {
                throw new ConversionException("Instance with type " + source.getConfiguratorType() + " is of class" + source.getClass().getName() + " which is not convertible to " + TkLengthConfiguredProductInfoModel.class.getName());
            }

            final ConfigurationInfoData item = new ConfigurationInfoData();
            final TkLengthConfiguredProductInfoModel model = (TkLengthConfiguredProductInfoModel) source;
            item.setUniqueId(model.getLabel());
            item.setConfigurationLabel(model.getDisplayValue());
            item.setStatus(source.getProductInfoStatus());
            item.setConfiguratorType(model.getConfiguratorType());
            item.setConfigurationValue(String.valueOf(model.isChecked()));
            item.setLengthValue(String.valueOf(model.getLengthValue()));
            if (model.getOrderEntry() != null && model.getOrderEntry().getProduct() != null) {
                final Optional<FeatureValue> productWidthFeatureValue = getB2bProductService()
                        .getProductWidth(model.getOrderEntry().getProduct(), "XX_WIDTH");
                if (productWidthFeatureValue.isPresent()) {
                    item.setWidthValue(productWidthFeatureValue.get().getValue().toString());
                }
            }
            target.add(item);

        }
    }
    public TkEuB2bProductService getB2bProductService() {
        return b2bProductService;
    }
    public void setB2bProductService(TkEuB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }


}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppconfigurableproductsaddon/responsive/product" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url value="/cart/${entryNumber}/configuration/CERTIFICATE" var="cartPageUrl"/>

<template:page pageTitle="${pageTitle}">
    <jsp:body>
        <div class="container l-page__content">
            <div class="row">
                <div id="globalMessages">
                    <common:globalMessages/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form:form method="post" id="certificateConfigurationForm" action="${cartPageUrl}">

                        <input id="quantity" name="quantity" type="hidden" value="${qty}" hidden="hidden">
                        <div class="form-group">
                            <product:productConfiguratorTab configurations="${configurations}"/>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <button id="update" type="submit" class="btn btn-primary btn-block">
                                    <spring:theme code="configuration.page.update"/>
                                </button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </jsp:body>
</template:page>

package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.forms;

import de.hybris.platform.catalog.enums.ConfiguratorType;

import java.io.Serializable;
import java.util.Map;

public class CertificateConfigurationForm implements Serializable {

    private Long quantity;

    private Map<ConfiguratorType, Map<String, String>> configurationsKeyValueMap;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Map<ConfiguratorType, Map<String, String>> getConfigurationsKeyValueMap() {
        return configurationsKeyValueMap;
    }

    public void setConfigurationsKeyValueMap(Map<ConfiguratorType, Map<String, String>> configurationsKeyValueMap) {
        this.configurationsKeyValueMap = configurationsKeyValueMap;
    }
}

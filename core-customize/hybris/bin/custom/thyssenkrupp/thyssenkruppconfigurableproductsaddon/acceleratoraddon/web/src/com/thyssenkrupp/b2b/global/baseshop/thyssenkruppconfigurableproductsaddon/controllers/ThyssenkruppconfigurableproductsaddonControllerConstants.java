package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers;

public final class ThyssenkruppconfigurableproductsaddonControllerConstants {

    public static final String ADDON_PREFIX      = "addon:/thyssenkruppconfigurableproductsaddon/";
    public static final String STOREFRONT_PREFIX = "/";

    private ThyssenkruppconfigurableproductsaddonControllerConstants() {
        throw new UnsupportedOperationException("Class supports only static constants");
    }

    public static class Views {
        public static class Pages {
            public static final String PRODUCT_CONFIGURATOR_PAGE = ADDON_PREFIX + "pages/productConfiguratorPage";
            public static final String ENTRY_CONFIGURATOR_PAGE   = ADDON_PREFIX + "/pages/cartEntryConfiguratorPage";
            public static final String ENTRY_READ_ONLY_PAGE      = ADDON_PREFIX + "/pages/readOnlyEntryConfiguratorPage";
        }

        public static class Fragments {
            public static class Cart {
                public static final String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
            }
        }
    }
}

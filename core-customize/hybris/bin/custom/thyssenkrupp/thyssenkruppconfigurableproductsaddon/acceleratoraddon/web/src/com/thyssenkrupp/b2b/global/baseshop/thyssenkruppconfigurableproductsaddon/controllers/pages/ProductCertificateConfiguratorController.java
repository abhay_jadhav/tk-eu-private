package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.pages;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.order.ConfigurableProductCartFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ConfigureForm;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.forms.CertificateConfigurationForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.QuoteFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.ThyssenkruppconfigurableproductsaddonControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP;
import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.ThyssenkruppconfigurableproductsaddonControllerConstants.Views.Pages.ENTRY_CONFIGURATOR_PAGE;
import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.ThyssenkruppconfigurableproductsaddonControllerConstants.Views.Pages.ENTRY_READ_ONLY_PAGE;
import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.ThyssenkruppconfigurableproductsaddonControllerConstants.Views.Pages.PRODUCT_CONFIGURATOR_PAGE;
import static de.hybris.platform.commercefacades.product.ProductOption.*;

@Controller
public class ProductCertificateConfiguratorController extends AbstractPageController {

    public static final String CERTIFICATE_CONFIGURATOR_TYPE = "CERTIFICATE";
    public static final String PAGE_LABEL                    = "configure" + CERTIFICATE_CONFIGURATOR_TYPE;

    private static final String TYPE_MISMATCH_ERROR_CODE             = "typeMismatch";
    private static final String ERROR_MSG_TYPE                       = "errorMsg";
    private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
    private static final String MODEL_ATTR_DOCUMENT_CODE             = "documentCode";
    private static final String MODEL_ATTR_RETURN_DOCUMENT_TYPE      = "returnDocumentType";
    private static final String MODEL_ATTR_ENTRY_NUMBER              = "entryNumber";

    @Resource
    private ProductFacade productFacade;

    @Resource
    private QuoteFacade quoteFacade;

    @Resource
    private OrderFacade orderFacade;

    @Resource
    private SaveCartFacade saveCartFacade;

    @Resource
    private ConfigurableProductCartFacade configurableProductCartFacade;

    @Resource(name = "productBreadcrumbBuilder")
    private ProductBreadcrumbBuilder productBreadcrumbBuilder;

    @RequestMapping(value = "/**/p/{productCode}/configuratorPage/" + CERTIFICATE_CONFIGURATOR_TYPE, method = { RequestMethod.GET, RequestMethod.POST })
    public String productConfigurator(@PathVariable("productCode") final String productCode, final Model model,
        final ConfigureForm configureForm)
        throws CMSItemNotFoundException {
        storePageData(productCode, getProductFacade().getConfiguratorSettingsForCode(productCode), model);
        model.addAttribute("qty", configureForm.getQty());
        return PRODUCT_CONFIGURATOR_PAGE;
    }

    @RequestMapping(value = "/**/p/{productCode}/configure/" + CERTIFICATE_CONFIGURATOR_TYPE, method = RequestMethod.POST)
    public String addToCart(@PathVariable("productCode") final String productCode, final Model model,
        @ModelAttribute("certificateConfigurationForm") final CertificateConfigurationForm form, final BindingResult bindingErrors,
        final HttpServletRequest request, final RedirectAttributes redirectModel) {
        if (bindingErrors.hasErrors()) {
            return getViewWithBindingErrorMessages(model, bindingErrors);
        }

        boolean err = false;
        final long qty = form.getQuantity();
        err = validateCartQuantity(productCode, model, form, redirectModel, err, qty);
        if (err) {
            return getConfigurePageRedirectPath(productCode);
        }

        model.addAttribute("product", productFacade.getProductForCodeAndOptions(productCode, Collections.singletonList(BASIC)));
        return REDIRECT_PREFIX + "/cart";
    }

    private boolean validateCartQuantity(final String productCode, final Model model, final CertificateConfigurationForm form, final RedirectAttributes redirectModel, final boolean err, final long qty) {
        boolean error = err;
        if (qty <= 0) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.quantity.invalid");
            model.addAttribute("quantity", 0L);
            error = true;
        } else {
            error = validateAddToCart(productCode, model, form, redirectModel, error, qty);
        }
        return error;
    }

    private boolean validateAddToCart(final String productCode, final Model model, final CertificateConfigurationForm form, final RedirectAttributes redirectModel, final boolean err, final long qty) {
        boolean error = err;
        try {
            final CartModificationData cartModification = configurableProductCartFacade.addToCart(productCode, qty, configurableProductCartFacade.getConfigurationInfoData(form.getConfigurationsKeyValueMap()));

            if (cartModification == null) {
                throw new CommerceCartModificationException("Null cart modification");
            }
            if (cartModification.getQuantityAdded() > 0) {
                configurableProductCartFacade.updateCartEntry(getOrderEntryData(form, cartModification.getEntry()));
                model.addAttribute("quantity", cartModification.getQuantityAdded());
                model.addAttribute("entry", cartModification.getEntry());
            }

            if (cartModification.getQuantityAdded() == 0L) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                error = true;
            } else if (cartModification.getQuantityAdded() < qty) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
                error = true;
            }
        } catch (final CommerceCartModificationException ex) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.occurred");
            error = true;
        }
        return error;
    }

    @RequestMapping(value = "/cart/{entryNumber}/configuration/" + CERTIFICATE_CONFIGURATOR_TYPE)
    public String editConfigurationInEntry(@PathVariable(MODEL_ATTR_ENTRY_NUMBER) final int entryNumber, final Model model)
        throws CMSItemNotFoundException, CommerceCartModificationException {
        final CartData cart = configurableProductCartFacade.getSessionCart();
        final OrderEntryData entry = getOrderEntry(entryNumber, cart);
        model.addAttribute(MODEL_ATTR_ENTRY_NUMBER, entryNumber);
        storePageData(entry.getProduct().getCode(), entry.getConfigurationInfos(), model);
        return ENTRY_CONFIGURATOR_PAGE;
    }

    @RequestMapping(value = "/cart/{entryNumber}/configuration/" + CERTIFICATE_CONFIGURATOR_TYPE, method = RequestMethod.POST)
    public ModelAndView updateConfigurationInEntry(@PathVariable(MODEL_ATTR_ENTRY_NUMBER) final int entryNumber, final Model model,
        @ModelAttribute("certificateConfigurationForm") final CertificateConfigurationForm form)
        throws CommerceCartModificationException {
        final CartData cart = configurableProductCartFacade.getSessionCart();
        final OrderEntryData entry = getOrderEntry(entryNumber, cart);
        configurableProductCartFacade.updateCartEntry(getOrderEntryData(form, entry));
        setCartModelData(model, entry);
        return new ModelAndView(REDIRECT_PREFIX + "/cart");
    }

    @RequestMapping(value = "/my-account/my-quotes/{quoteCode}/{entryNumber}/configurationDisplay/" + CERTIFICATE_CONFIGURATOR_TYPE)
    public String displayConfigurationInQuoteEntry(@PathVariable("quoteCode") final String quoteCode,
        @PathVariable(MODEL_ATTR_ENTRY_NUMBER) final int entryNumber, final Model model)
        throws CMSItemNotFoundException, CommerceCartModificationException {
        final QuoteData quote = quoteFacade.getQuoteForCode(quoteCode);
        final OrderEntryData entry = getAbstractOrderEntry(entryNumber, quote);
        model.addAttribute(MODEL_ATTR_ENTRY_NUMBER, entryNumber);
        model.addAttribute(MODEL_ATTR_DOCUMENT_CODE, quoteCode);
        model.addAttribute(MODEL_ATTR_RETURN_DOCUMENT_TYPE, "my-quotes");
        storePageData(entry.getProduct().getCode(), entry.getConfigurationInfos(), model);
        return ENTRY_READ_ONLY_PAGE;
    }

    @RequestMapping(value = "/my-account/order/{orderCode}/{entryNumber}/configurationDisplay/" + CERTIFICATE_CONFIGURATOR_TYPE)
    public String displayConfigurationInOrderEntry(@PathVariable("orderCode") final String orderCode,
        @PathVariable(MODEL_ATTR_ENTRY_NUMBER) final int entryNumber, final Model model)
        throws CMSItemNotFoundException, CommerceCartModificationException {
        final OrderData order = orderFacade.getOrderDetailsForCode(orderCode);
        final OrderEntryData entry = getAbstractOrderEntry(entryNumber, order);
        model.addAttribute(MODEL_ATTR_ENTRY_NUMBER, entryNumber);
        model.addAttribute(MODEL_ATTR_DOCUMENT_CODE, orderCode);
        model.addAttribute(MODEL_ATTR_RETURN_DOCUMENT_TYPE, "order");
        storePageData(entry.getProduct().getCode(), entry.getConfigurationInfos(), model);
        return ENTRY_READ_ONLY_PAGE;
    }

    @RequestMapping(value = "/my-account/saved-carts/{cartCode}/{entryNumber}/configurationDisplay/" + CERTIFICATE_CONFIGURATOR_TYPE)
    public String displayConfigurationInSavedCartEntry(@PathVariable("cartCode") final String cartCode,
        @PathVariable(MODEL_ATTR_ENTRY_NUMBER) final int entryNumber, final Model model)
        throws CMSItemNotFoundException, CommerceCartModificationException, CommerceSaveCartException {
        final CommerceSaveCartParameterData parameters = new CommerceSaveCartParameterData();
        parameters.setCartId(cartCode);
        final CommerceSaveCartResultData commerceSaveCartResultData = saveCartFacade.getCartForCodeAndCurrentUser(parameters);
        final OrderEntryData entry = getAbstractOrderEntry(entryNumber, commerceSaveCartResultData.getSavedCartData());
        model.addAttribute(MODEL_ATTR_ENTRY_NUMBER, entryNumber);
        model.addAttribute(MODEL_ATTR_DOCUMENT_CODE, cartCode);
        model.addAttribute(MODEL_ATTR_RETURN_DOCUMENT_TYPE, "saved-carts");
        storePageData(entry.getProduct().getCode(), entry.getConfigurationInfos(), model);
        return ENTRY_READ_ONLY_PAGE;
    }

    protected void setCartModelData(final Model model, final OrderEntryData entry) {
        model.addAttribute("product", productFacade.getProductForCodeAndOptions(entry.getProduct().getCode(), Collections.singletonList(BASIC)));
        model.addAttribute("quantity", entry.getQuantity());
        model.addAttribute("entry", entry);
    }

    protected void storePageData(final String productCode, final List<ConfigurationInfoData> configuration, final Model model)
        throws CMSItemNotFoundException {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productCode));
        final Set<ProductOption> options = new HashSet<>(Arrays.asList(VARIANT_FIRST_VARIANT, BASIC, URL, PRICE, SUMMARY, DESCRIPTION, GALLERY,
            CATEGORIES, REVIEW, PROMOTIONS, CLASSIFICATION, VARIANT_FULL, STOCK, VOLUME_PRICES, PRICE_RANGE, DELIVERY_MODE_AVAILABILITY));

        final ProductData productData = getProductFacade().getProductForCodeAndOptions(productCode, options);
        model.addAttribute("product", productData);
        model.addAttribute("pageType", PageType.PRODUCT.name());
        final ContentPageModel pageModel = getContentPageForLabelOrId(PAGE_LABEL);
        storeCmsPageInModel(model, pageModel);
        model.addAttribute("configurations", configuration);
    }

    protected OrderEntryData getOrderEntry(final int entryNumber, final CartData cart) throws CommerceCartModificationException {
        final List<OrderEntryData> entries = cart.getEntries();
        return getOrderEntryData(entryNumber, entries);
    }

    protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors) {
        for (final ObjectError error : bindingErrors.getAllErrors()) {
            if (isTypeMismatchError(error)) {
                model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
            } else {
                model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
            }
        }
        return ADD_TO_CART_POPUP;
    }

    protected OrderEntryData getOrderEntryData(final CertificateConfigurationForm form, final OrderEntryData orderEntryData) {
        final List<ConfigurationInfoData> configurationInfoDataList = configurableProductCartFacade.getConfigurationInfoData(form.getConfigurationsKeyValueMap());
        orderEntryData.setConfigurationInfos(configurationInfoDataList);
        return orderEntryData;
    }

    protected OrderEntryData getAbstractOrderEntry(final int entryNumber, final AbstractOrderData abstractOrder) throws CommerceCartModificationException {
        final List<OrderEntryData> entries = abstractOrder.getEntries();
        return getOrderEntryData(entryNumber, entries);
    }

    protected OrderEntryData getOrderEntryData(int entryNumber, final List<OrderEntryData> entries) throws CommerceCartModificationException {
        if (entries == null) {
            throw new CommerceCartModificationException("Cart is empty");
        }
        try {
            return entries.stream()
                .filter(e -> e != null)
                .filter(e -> e.getEntryNumber() == entryNumber)
                .findAny().get();
        } catch (final NoSuchElementException e) {
            throw new CommerceCartModificationException("Cart entry #" + entryNumber + " does not exist");
        }
    }

    protected boolean isTypeMismatchError(final ObjectError error) {
        return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
    }

    protected String getConfigurePageRedirectPath(String productCode) {
        return String.format("%s/p/%s/configuratorPage/%s", REDIRECT_PREFIX, productCode, CERTIFICATE_CONFIGURATOR_TYPE);
    }

    protected ProductFacade getProductFacade() {
        return productFacade;
    }
}


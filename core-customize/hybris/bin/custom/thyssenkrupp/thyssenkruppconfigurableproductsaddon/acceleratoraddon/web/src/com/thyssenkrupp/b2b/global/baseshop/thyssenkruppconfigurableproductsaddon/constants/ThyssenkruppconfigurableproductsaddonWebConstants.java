package com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants;

public final class ThyssenkruppconfigurableproductsaddonWebConstants {
    private ThyssenkruppconfigurableproductsaddonWebConstants() {
        throw new UnsupportedOperationException("Class supports only static constants");
    }
}

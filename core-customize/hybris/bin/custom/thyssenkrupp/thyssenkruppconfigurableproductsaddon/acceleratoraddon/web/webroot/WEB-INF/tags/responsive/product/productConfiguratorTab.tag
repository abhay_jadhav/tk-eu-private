<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="configurations" required="true" type="java.util.ArrayList" %>
<%@ attribute name="readOnly" required="false"  type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:eval var="VALUE_SEPARATOR"
expression="T(com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants).CERTIFICATE_CONFIG_LABEL_ID_SEPARATOR"/>
<c:forEach var="configuration" items="${configurations}">
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <label for="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}]">
                ${configuration.configurationLabel}
            </label>
        </div>
        <div class="col-md-6">
            <c:choose>
                <c:when test="${configuration.configurationValue eq 'true'}">
                    <input id="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}]"
                           name="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}${VALUE_SEPARATOR}${configuration.uniqueId}]"
                           type="checkbox" checked="checked"
                    <c:if test="${readOnly}"> disabled</c:if>
                    >
                </c:when>
                <c:otherwise>
                    <input id="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}]"
                           name="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}${VALUE_SEPARATOR}${configuration.uniqueId}]"
                           type="checkbox"
                    <c:if test="${readOnly}"> disabled</c:if>
                    >
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</c:forEach>

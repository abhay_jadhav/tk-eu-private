package com.thyssenkrupp.b2b.global.baseshop.fulfillment.constants;

/**
 * Global class for all Thyssenkruppsaporderfulfillment constants. You can add global constants for
 * your extension into this class.
 */
public final class ThyssenkruppsaporderfulfillmentConstants extends GeneratedThyssenkruppsaporderfulfillmentConstants {
    @SuppressWarnings("javadoc")
    public static final String EXTENSIONNAME = "thyssenkruppsaporderfulfillment";

    @SuppressWarnings("javadoc")
    public static final String ORDER_PROCESS_NAME = "sap-order-process";

    private ThyssenkruppsaporderfulfillmentConstants() {
        // empty to avoid instantiating this constant class
    }

}


package com.thyssenkrupp.b2b.global.baseshop.fulfillment.setup;

import com.thyssenkrupp.b2b.global.baseshop.fulfillment.constants.ThyssenkruppsaporderfulfillmentConstants;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

@SystemSetup(extension = ThyssenkruppsaporderfulfillmentConstants.EXTENSIONNAME)
public class ThyssenkruppSapOrderFulfillmentSystemSetup extends AbstractSystemSetup {

    private static final String IMPORT_CORE_DATA = "importCoreData";

    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        final boolean importCoreData = getBooleanSystemSetupParameter(context, IMPORT_CORE_DATA);
        if (importCoreData) {
            importImpexFile(context, "/thyssenkruppsaporderfulfillment/import/coredata/common/cronJobs.impex", true);
        }
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
        return params;
    }
}

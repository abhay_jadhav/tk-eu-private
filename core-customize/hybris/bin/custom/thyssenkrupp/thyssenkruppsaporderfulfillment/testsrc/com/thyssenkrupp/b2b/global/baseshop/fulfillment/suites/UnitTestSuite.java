package com.thyssenkrupp.b2b.global.baseshop.fulfillment.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.thyssenkrupp.b2b.global.baseshop.fulfillment.actions.SendOrderToDataHubActionTest;
import com.thyssenkrupp.b2b.global.baseshop.fulfillment.actions.SetCompletionStatusActionTest;
import com.thyssenkrupp.b2b.global.baseshop.fulfillment.actions.SetConfirmationStatusActionTest;
import com.thyssenkrupp.b2b.global.baseshop.fulfillment.actions.UpdateERPOrderStatusActionTest;
import com.thyssenkrupp.b2b.global.baseshop.fulfillment.jobs.OrderCancelRepairJobTest;
import com.thyssenkrupp.b2b.global.baseshop.fulfillment.jobs.OrderExchangeRepairJobTest;

@SuppressWarnings("javadoc")
@RunWith(Suite.class)
@SuiteClasses({ UpdateERPOrderStatusActionTest.class, SetConfirmationStatusActionTest.class, SendOrderToDataHubActionTest.class, SetCompletionStatusActionTest.class, OrderExchangeRepairJobTest.class, OrderCancelRepairJobTest.class })
public class UnitTestSuite {
    // Intentionally left blank
}

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        watch: {
            less: {
                files: ["./web/webroot/_ui-src/less/*.less", "./web/webroot/_ui-src/less/module/*.less",
                    "./web/webroot/_ui-src/less/generic/*.less"],
                tasks: ["less", "cssmin"]
            },
            scripts: {
                files: ["./web/webroot/_ui-src/js/generic/*.js", "./web/webroot/_ui-src/js/module/*.js"],
                tasks: ["jshint", "uglify"]
            }
        },
        less: {
            dist: {
                files: [{
                    expand: true,
                    cwd: "./web/webroot/_ui-src/less/",
                    src: ["generic.less", "module.less"],
                    dest: "./web/webroot/_ui-src/css/",
                    ext: ".css"
                }]
            }
        },
        csslint: {
            options: {
                csslintrc: ".csslintrc"
            },
            strict: {
                options: {
                    import: 2
                },
                src: ["./web/webroot/source/css/*.css"]
            },
            lax: {
                options: {
                    import: false
                },
                src: ["./web/webroot/source/css/*.css"]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: "./web/webroot/_ui-src/css/",
                    src: ["*.css", "!*.min.css"],
                    dest: "./web/webroot/_ui/css/",
                    ext: ".min.css"
                },
                {
                    expand: true,
                    cwd: "./web/webroot/_ui-src/css/",
                    src: ["generic.css", "!*.min.css"],
                    dest: "../thyssenkruppeu/thyssenkruppeustorefrontaddon/acceleratoraddon/web/webroot/_ui/responsive/patternlibrary/css/",
                    ext: ".min.css"
                }]
            }
        },
        jshint: {
            src: ["./web/webroot/_ui-src/js/generic/*.js", "./web/webroot/_ui-src/js/module/*.js", "!./web/webroot/source/js/*.min.js"]
        },
        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: "./web/webroot/_ui-src/js/generic/",
                    src: ["*.js", "!*.min.js"],
                    dest: "../thyssenkruppeu/thyssenkruppeustorefrontaddon/acceleratoraddon/web/webroot/_ui/responsive/patternlibrary/js/",
                    ext: ".min.js"
                },
                {
                    expand: true,
                    cwd: "./web/webroot/_ui-src/js/module/",
                    src: ["*.js", "!*.min.js"],
                    dest: "./web/webroot/_ui/js/",
                    ext: ".min.js"
                }]
            }
        },
        copy: {
            css: {
                expand: true,
                cwd: "./web/webroot/_ui-src/css/lib/",
                src: ["*.css"],
                dest: "./web/webroot/_ui/css/lib/"
            },
            js: {
                expand: true,
                cwd: "./web/webroot/_ui-src/js/lib/",
                src: ["*.js"],
                dest: "./web/webroot/_ui/js/lib/"
            }
        }
    });
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-csslint");

    grunt.registerTask("default", ["less", "cssmin", "jshint", "uglify", "copy"]);
    grunt.registerTask("build", ["copy"]);
};

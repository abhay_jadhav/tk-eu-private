package com.thyssenkrupp.global.pali.constants;

/**
 * Global class for all Tkpatternlibrary constants. You can add global constants
 * for your extension into this class.
 */
public final class TkpatternlibraryConstants extends GeneratedTkpatternlibraryConstants {
    public static final String EXTENSIONNAME = "tkpatternlibrary";
    public static final String PLATFORM_LOGO_CODE = "tkpatternlibraryPlatformLogo";


    private TkpatternlibraryConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension

}

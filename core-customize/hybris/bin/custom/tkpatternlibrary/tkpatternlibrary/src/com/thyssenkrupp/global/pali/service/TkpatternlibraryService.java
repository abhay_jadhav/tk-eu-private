package com.thyssenkrupp.global.pali.service;

public interface TkpatternlibraryService {
    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);
}

package com.thyssenkrupp.global.pali.setup;

import static com.thyssenkrupp.global.pali.constants.TkpatternlibraryConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.thyssenkrupp.global.pali.constants.TkpatternlibraryConstants;
import com.thyssenkrupp.global.pali.service.TkpatternlibraryService;

@SystemSetup(extension = TkpatternlibraryConstants.EXTENSIONNAME)
public class TkpatternlibrarySystemSetup {
    private final TkpatternlibraryService tkpatternlibraryService;

    public TkpatternlibrarySystemSetup(final TkpatternlibraryService tkpatternlibraryService) {
        this.tkpatternlibraryService = tkpatternlibraryService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
        tkpatternlibraryService.createLogo(PLATFORM_LOGO_CODE);
    }

    private InputStream getImageStream() {
        return TkpatternlibrarySystemSetup.class.getResourceAsStream("/tkpatternlibrary/sap-hybris-platform.png");
    }
}

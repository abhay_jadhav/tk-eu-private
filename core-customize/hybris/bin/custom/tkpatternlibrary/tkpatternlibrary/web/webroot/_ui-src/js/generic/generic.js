// This line for Popup modal prevent scrolling for iOS browsers
(function () {
    var _overlay = document.getElementsByClassName('modal');
    var _clientY = null;

    var touchStart = function (event) {
        if (event.targetTouches.length === 1) {
            _clientY = event.targetTouches[0].clientY;
        }
    };

    var touchMove = function (event) {
        if (event.targetTouches.length === 1) {
            disableRubberBand(event);
        }
    };

    for (var i = 0; i < _overlay.length; i++) {
        _overlay[i].addEventListener('touchstart', touchStart, false);
        _overlay[i].addEventListener('touchmove', touchMove, false);
    }

    function disableRubberBand(event) {
        var clientY = event.targetTouches[0].clientY - _clientY;

        if (_overlay.scrollTop === 0 && clientY > 0) {
            event.preventDefault();
        }

        if (isOverlayTotallyScrolled() && clientY < 0) {
            event.preventDefault();
        }
    }

    function isOverlayTotallyScrolled() {
        return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
    }

    $('.dropdown-menu--validate a').on('click', function(){
        $(this).closest('.dropdown').find('.dropdown-toggle').removeClass('error').removeClass('has-error');
        $(this).closest('.dropdown').find('.js-size-alert').addClass('hide');
        $(this).closest('.dropdown').prev('label').removeClass('has-error');
        $(this).closest('.dropdown').prev('label').removeClass('has-error');
    });

    $('.dropdown-menu a').not('.js-custom-function').on('click', function(){
      var liItems = $(this).closest('.dropdown-menu').find("li");
        $(liItems).each(function(){
            $(this).removeClass("active");
        });

        $(this).parent("li").addClass("active");

        if($(liItems).last().hasClass("active")) {
            $(liItems).last().prev("li").find("a").css("border-bottom", 0);
        } else {
            $(liItems).last().prev("li").find("a").css("border-bottom", "");
        }

        $(".js-item-qty, .js-qty-lbl").removeClass("has-error");
        $(".js-qty-alert, .js-qty-validation").addClass("hide");
        $(".js-add-to-cart").removeAttr("disabled");
        $(this).closest(".s-dropdown").find('.dropdown-toggle').html($(this).html() + '<span class="dd-icon u-icon-tk u-icon-tk--arrow-down"></span>').addClass("selected");

        if($("#std").is(":visible")) {
          var selectedElem = $(this).closest(".s-dropdown").find(".dropdown-toggle.selected"),
              selectedVal = selectedElem.text(),
              selectedCode = $(this).parent("li").attr("data-code");
          if($("#productQtyUnit").length) {
            $("#productQtyUnit").val(selectedCode);
            $("#productQtyUnitName").val(selectedVal);
          }

          if($("#productQty").length) {
            $("#productQty").val("1");
          }

          selectedElem.val(selectedVal);
          if(ACC.productDetail){
            ACC.productDetail.validQuantity = true;
          }
        }
    });

    function tooltip () {
      $(".js-tooltip-icon").on("click", function(e) {
            var pos = $(this).offset(),
                ttText = $(this).data("text"),
                clientX = e.clientX,
                clientY = e.clientY,
                windowHeight = window.outerHeight,
                 windowWidth = $("body").width(),
                heightDiff = windowHeight - clientY,
                widthDiff = windowWidth - clientX,
                divDiff = $('body').offset().left;
            $(".js-tooltip-text").text(ttText);
            var tooltipHeight = $(".js-tooltip-box").outerHeight();
            if(heightDiff < 200 && widthDiff > 100 && clientX > 100) {
              if(windowWidth < 767) {
                $(".js-tooltip-box").removeClass("right").removeClass("left").removeClass("top-left").removeClass("top-right").addClass("top").css(
                          { "left": pos.left - 85, "top": (pos.top) - tooltipHeight - 155}).show();
              } else {
                $(".js-tooltip-box").removeClass("right").removeClass("left").removeClass("top-left").removeClass("top-right").addClass("top").css(
                            { "left": pos.left - 85, "top": (pos.top) - tooltipHeight - 20}).show();
              }

            } else if ((clientX < 100) && (heightDiff < 200) && windowWidth > 767) {
                $(".js-tooltip-box").removeClass("top").removeClass("right").removeClass("left").removeClass("top-right").addClass("top-left").css(
                        { "left": pos.left, "top": (pos.top) - 120 }).show();
            } else if ((widthDiff < 100) && (heightDiff < 200)) {
                $(".js-tooltip-box").removeClass("top").removeClass("right").removeClass("left").removeClass("top-left").addClass("top-right").css(
                        { "left": pos.left - 140, "top": (pos.top) - 120 }).show();
            } else if(widthDiff < 100){
                $(".js-tooltip-box").removeClass("top").removeClass("left").removeClass("top-left").removeClass("top-right").addClass("right").css(
                    { "left": pos.left - 210, "top": (pos.top) - 30 }).show();
            } else if ((windowWidth - widthDiff) < 100 && clientX < 100) {
                if(windowWidth < 767) {
                  $(".js-tooltip-box").removeClass("top").removeClass("right").removeClass("top-left").removeClass("top-right").addClass("left").css(
                            { "left": pos.left + 40, "top": (pos.top) - 155 }).show();
                } else {
                  $(".js-tooltip-box").removeClass("top").removeClass("right").removeClass("top-left").removeClass("top-right").addClass("left").css(
                            { "left": pos.left + 50, "top": (pos.top) - 30 }).show();
                }
            } else {
            if ($("body").hasClass("pali")){
                $(".js-tooltip-box").removeClass("right").removeClass("left").removeClass("top-left").removeClass("top-right").removeClass("top").css(
                    { "left": pos.left - divDiff - 77, "top": pos.top + 40 }).show();
                    }else
                    {
                      if(windowWidth < 767) {
                            $(".js-tooltip-box").removeClass("right").removeClass("left").removeClass("top-left").removeClass("top-right").removeClass("top").css(
                               { "left": pos.left - divDiff - 85, "top": pos.top - 80 }).show();
                       } else {
                            $(".js-tooltip-box").removeClass("right").removeClass("left").removeClass("top-left").removeClass("top-right").removeClass("top").css(
                               { "left": pos.left - divDiff - 85, "top": pos.top + 40 }).show();
                       }
                  }
            }
        });

        $(document).on("click", function(e) {
            var targetEle = $(e.target);
            if($(targetEle).hasClass("js-tooltip-icon-box") || $(targetEle).hasClass("js-tooltip-icon")) {
                return false;
            } else if ($(targetEle).hasClass("js-tooltip-box")) {
                return false;
            } else if( $(targetEle).closest(".js-tooltip-box").length > 0) {
                if($(targetEle).hasClass("close") || $(targetEle).parent().hasClass("close")) {
                    $(".js-tooltip-box").hide();
                } else {
                    return false;
                }
            } else {
                $(".js-tooltip-box").hide();
            }
        });
    }
    tooltip ();
    dropdownscroller();


}());

var currentObj;
function typeInDropdown() {
    $('.js-type-in-dd').selectize({
        onDelete: function (obj) {
            typeInOnDropdownOnDelete(obj);
        },
        onDropdownOpen: function (obj) {
            typeInOnDropdownOpen(obj);
        },
        onDropdownClose: function () {
            typeInOnDropdownClose();
        }
    });
}

typeInDropdown();

function dropdownscroller() {


  //Code for IE and Edge Browser Only
      if(document.documentMode || /Edge/.test(navigator.userAgent)) {

          $(".dropdown-menu").each(function() {
              if($(this).find("li").length > 8) {
                  $(this).addClass("ie-dropdown-scroller");
              } else {
                  $(this).removeClass("ie-dropdown-scroller");
              }
          });

      } else {
        $(".dropdown-menu").each(function() {
              if($(this).find("li").length > 8) {
                  $(this).css("overflow", "hidden");
                  $(this).mCustomScrollbar({
                      setHeight: 281,
                      theme: "inset-dark",
                      mouseWheel: { preventDefault:true }
                   });

               } else {
                   $(this).css('height', 'auto');
               }
          });
        $(".dropdown-menu").on("mouseup pointerup",function(e) {
            $(".dropdown-menu .mCSB_scrollTools").removeClass("mCSB_scrollTools_onDrag");
          }).on("click",function(e) {
            if($(e.target).parents(".mCSB_scrollTools").length || $(".dropdown-menu .mCSB_scrollTools").hasClass("mCSB_scrollTools_onDrag")) {
              e.stopPropagation();
            }
          });
      }


  }
function typeInOnDropdownOnDelete(obj) {
    currentObj.find('.selectize-dropdown-content .option').each(function () {
        $(this).removeClass('show-hide');
    });
}
function typeInOnDropdownOpen(obj) {
    typeindropdownScroller();
    $(".selectize-control").addClass("highlight-dropdown");
    currentObj = $(obj);
    var selectedText = $(obj).closest('.selectize-control').find(".selectize-input").find(".item").text();
    $(obj).find('.selectize-dropdown-content .option').each(function () {
        var allText = $(this).text();
        if (selectedText == allText) {
            $(this).addClass("show-hide").siblings().removeClass('show-hide');
        }
    });
}
function typeInOnDropdownClose() {
    $('.custom-scroll').mCustomScrollbar("disable", true);
    $(".selectize-control").removeClass("highlight-dropdown");
}
function typeindropdownScroller() {

   $(".selectize-dropdown-content").each(function(){
     if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {

       if ($(this).find('.option').length > 8){
           $(this).css("max-height", "276px");
           }
           else {
             $(this).css("height", "auto");

             }
       }

       else {
             if ($(this).find('.option').length > 8){
             if($(this).closest(".custom-scroll").length == 0){
               $(this).wrapAll("<div class='custom-scroll' style='max-height:276px'> </div>");
             }
           }
           else {

             if($(this).closest(".custom-scroll").length == 0){
                   $(this).wrapAll("<div class='custom-scroll' style='height:auto'> </div>");
                }
             }
       }
      });


    //Code for IE and Edge Browser Only
    if(document.documentMode || /Edge/.test(navigator.userAgent)) {
        $(".custom-scroll").each(function(){
            if($(this).find(".option").length > 8){
                $(this).addClass("type-in-dd-ie-scroll");
            } else {
                $(this).removeClass("type-in-dd-ie-scroll");
            }

        });

    } else {
      $(".custom-scroll").each(function() {
            if($(this).find(".option").length > 8){
                $(this).css("overflow", "hidden");
                $(this).mCustomScrollbar('destroy');
                 $(this).mCustomScrollbar({
                    theme: "inset-dark",
                    mouseWheel:{ preventDefault:true }
                 });

             }
            });
      $(".custom-scroll").on("mouseup pointerup",function(e) {
          $(".custom-scroll .mCSB_scrollTools").removeClass("mCSB_scrollTools_onDrag");
        }).on("click",function(e) {
          if($(e.target).parents(".mCSB_scrollTools").length || $(".dropdown-menu .mCSB_scrollTools").hasClass("mCSB_scrollTools_onDrag")){
            e.stopPropagation();
          }
        });
    }
  }

var backtoTop = (function (w) {
    "use strict";
    var scrollElement,
        isVisibleState = false,
        isScrolling = false,
        currentScrollY,
        lastScrollY,
        windowHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight),
        scrollTimeout, // Debounce the scroll event
        hideTimeout; // Hide after 4s timer
    function  scrollTopHide () {
        if (!scrollElement || !isVisibleState) {
            return false;
        }
        scrollElement.classList.add("scrollHide");
        isVisibleState = false;
        return true;
    }
    function isVisible () {
        return isVisibleState;
    }
    function shouldShow () {
        return !isVisible ();
    }
    function  createScrolldiv () {
        scrollElement = document.createElement("div");
        scrollElement.classList.add("scrollTop", "scrollHide", "icon-tk-arrow-top");
        document.body.appendChild(scrollElement);
        scrollElement.addEventListener("click", function() {
            $("html, body").animate({scrollTop : 0},300);
        });
    }
    function showScrollElement () {
        // Hide after 4s if not scrolling
        if (!!hideTimeout) {
            clearTimeout(hideTimeout);
        }
        hideTimeout = setTimeout(scrollTopHide, 4000);
        if (!shouldShow ()) {
            return false;
        }
        // Build the button on first scroll
        if (!scrollElement) {
            createScrolldiv ();
        }
        // Make it async for first animation
        setTimeout (function () {
            scrollElement.classList.remove("scrollHide");
        }, 0);
        // Cache visible state
        isVisibleState = true;
        return true;
    }
    /*scrolling up or down and save scroll Y values.*/
    function onScrollMove () {
        currentScrollY = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0);
        if (Math.abs( currentScrollY - lastScrollY) > 5) {
            // Only show if scrolled more than 2 * window height.
            if (lastScrollY > currentScrollY && windowHeight * 2 < currentScrollY) {
                // Scroll up.
                showScrollElement ();
            } else {
                // Scroll down.
                scrollTopHide ();
            }
        }
        // Save last scroll Y position.
        lastScrollY = currentScrollY;
        isScrolling = false;
    }
    /* Prepare and debounce the scrolling event. */
    function prepareScroll () {
        if (!isScrolling) {
            isScrolling = true;
            if (!scrollTimeout) {
                clearTimeout (scrollTimeout);
            }
            scrollTimeout = setTimeout (onScrollMove, 75);
        }
    }
    /* Start the scroll event. */
    function initScroll () {
        $(window).bind("touchstart click", function() {
            setTimeout (onScrollMove, 50);
        });
        $(window).bind("scroll", prepareScroll);
    }
    // Start in setTimeout to ignore anchor scrolling
    setTimeout(initScroll, 100);
})(window);


$(document).ready(function() {

   dropdownscroller();
});

$('#js-filterModal').on('shown.bs.modal', function () {
    if(window.outerHeight < $(this).find(".modal-dialog").height()) {
      $(this).find(".modal-dialog").removeClass("modal-dialog-centered");
    }
});

$(".cart-row").on("click", "a.inactive", function(){
   return false;
});


$(document).ajaxSend(function(event, request, settings) {
  if(settings.noLoader == undefined || !settings.noLoader) {
    if(settings.globalLoader != undefined && !settings.globalLoader) {
        $(".simple-loader").show();
      } else {
          $(".global-loader").show();
      }
  }

});

$(document).ajaxComplete(function(event, request, settings) {

  if(settings.noLoader == undefined || !settings.noLoader) {
    $(".global-loader, .simple-loader").hide();
  }
});

var ACC = ACC || {};

ACC.generic = {

      _autoload: [
          "validateEmail"
      ],

      validateEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

};

// "use strict";

$(function() {
    $(".collapse").on('hide.bs.collapse', function() {
        $(this).prev("ul").find(".collapse-icon .minus").hide();
        $(this).prev("ul").find(".collapse-icon .plus").show();
    }).on('show.bs.collapse', function() {
        $(this).prev("ul").find(".collapse-icon .minus").show();
        $(this).prev("ul").find(".collapse-icon .plus").hide();
    });

    $(".pali-editor a").on("click", function() {
        $(this).closest("ul").next(".collapse").find(".tab-content div").hide();
        $(this).closest("ul").find("li a").removeClass('active');
        $(this).addClass('active');
        $(this).closest("ul").next(".collapse").find("#" + $(this).attr("data-href")).show();
    });

    $('textarea').each(function() {
        $(this).val($.trim($(this).val()).replace(/\s*[\r\n]+\s*/g, '\n')
            .replace(/(<[^\/][^>]*>)\s*/g, '$1')
            .replace(/\s*(<\/[^>]+>)/g, '$1'));
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.js-show-global-loader').click(function() {
        $('.global-loader').show();
        $('.global-loader').delay( 2500 ).fadeOut();
    });
});

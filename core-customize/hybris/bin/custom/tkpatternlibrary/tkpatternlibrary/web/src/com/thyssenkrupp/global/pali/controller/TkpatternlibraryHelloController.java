package com.thyssenkrupp.global.pali.controller;

import static com.thyssenkrupp.global.pali.constants.TkpatternlibraryConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thyssenkrupp.global.pali.service.TkpatternlibraryService;

@Controller
public class TkpatternlibraryHelloController {
    @Autowired
    private TkpatternlibraryService tkpatternlibraryService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(final ModelMap model) {
        model.addAttribute("logoUrl", tkpatternlibraryService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
        return "welcome";
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public class TkCartUnitPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {
    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        addCartUnit(source, target);
    }

    protected void addCartUnit(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {

        if (orderEntry.getUnit() != null) {
            entry.setUnit(orderEntry.getUnit().getCode());
            // entry.setUnitName(orderEntry.getUnit().getName()); Was In Baseshop Code
        } else {
            entry.setUnit("");
        }
    }
}


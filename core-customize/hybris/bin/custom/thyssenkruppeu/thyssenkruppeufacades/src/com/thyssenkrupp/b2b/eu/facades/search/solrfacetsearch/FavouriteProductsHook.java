package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch;

import java.util.List;

public interface FavouriteProductsHook {

    List<String> getFavouriteProducts();
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;

import java.math.BigDecimal;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceEntryModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

import de.hybris.platform.commercefacades.order.data.TkEuInvoiceData;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuInvoicePopulator implements Populator<InvoiceModel, TkEuInvoiceData> {

    private static String DEFAULT_CURRENCY_CODE = "EUR";
    private Converter<AddressModel, AddressData> addressConverter;
    private Converter<InvoiceEntryModel, TkEuInvoiceEntryData> invoiceEntryConverter;
    private TkEuPriceDataFactory tkEuPriceDataFactory;

    public void populate(final InvoiceModel invoiceModel, final TkEuInvoiceData invoiceData) throws ConversionException {
        invoiceData.setInvoiceNumber(invoiceModel.getCode());
        invoiceData.setTotal(formatPriceAsPriceData(invoiceModel, invoiceModel.getTotalGross()));
        invoiceData.setDueDate(invoiceModel.getDueDate());
        invoiceData.setInvoiceDate(invoiceModel.getDate());
        if (invoiceModel.getBillToAddress() != null) {
            invoiceData.setBillingAddress(getAddressConverter().convert(invoiceModel.getBillToAddress()));
        }
        if (invoiceModel.getSoldToAddress() != null) {
            invoiceData.setSoldToAddress(getAddressConverter().convert(invoiceModel.getSoldToAddress()));
        }

        if (CollectionUtils.isNotEmpty(invoiceModel.getEntries())) {
            invoiceData.setInvoiceEntries(invoiceEntryConverter.convertAll(invoiceModel.getEntries()));
        }
        invoiceData.setTotalTax(formatPriceAsPriceData(invoiceModel, invoiceModel.getTotalTax()));
        invoiceData.setTotalNet(formatPriceAsPriceData(invoiceModel, invoiceModel.getTotalNet()));
        invoiceData.setPaymentTerms(invoiceModel.getPaymentTerms());
    }

    private PriceData formatPriceAsPriceData(InvoiceModel invoiceModel, Double priceValue) {
        String currencyIsoCode = invoiceModel.getCurrency() != null ? invoiceModel.getCurrency().getIsocode() : DEFAULT_CURRENCY_CODE;
        String uomName = invoiceModel.getUnit() != null ? invoiceModel.getUnit().getName() : null;
        return getTkEuPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currencyIsoCode, formatUom(uomName));

    }

    protected Converter<AddressModel, AddressData> getAddressConverter() {
        return addressConverter;
    }

    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    public Converter<InvoiceEntryModel, TkEuInvoiceEntryData> getInvoiceEntryConverter() {
        return invoiceEntryConverter;
    }

    public void setInvoiceEntryConverter(Converter<InvoiceEntryModel, TkEuInvoiceEntryData> invoiceEntryConverter) {
        this.invoiceEntryConverter = invoiceEntryConverter;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

}

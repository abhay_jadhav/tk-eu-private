package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;

public class TkEuOrderConfirmationDetailPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {

    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        if (source instanceof OrderEntryModel) {
            OrderEntryModel orderEntry = (OrderEntryModel) source;
            target.setCustomerMaterialNumber(orderEntry.getCustomerMaterialNumber());
        }
    }
}

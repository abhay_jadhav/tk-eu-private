package com.thyssenkrupp.b2b.eu.facades.product;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

public interface TkEuLastViewedProductsFacade {

    void storeViewedProduct(String productCode);

    List<ProductData> loadViewedProducts();
}

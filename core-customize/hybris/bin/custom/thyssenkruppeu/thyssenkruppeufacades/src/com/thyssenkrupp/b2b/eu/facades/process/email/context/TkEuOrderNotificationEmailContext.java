package com.thyssenkrupp.b2b.eu.facades.process.email.context;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.enums.PaymentKeyTerms;
import com.thyssenkrupp.b2b.eu.core.enums.ShippingCondition;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuOrderNotificationEmailContext extends AbstractEmailContext<OrderProcessModel> {

    private static final String BCC_EMAILS = "bccEmails";


    private Converter<OrderModel, OrderData> orderConverter;
    private OrderData                        orderData;
    private List<CouponData>                 giftCoupons;

    private ShippingCondition shippingConditions;
    private PaymentKeyTerms   termsOfPayment;
    private TkEnumUtils enumUtils;

    @Override
    public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel) {
        super.init(orderProcessModel, emailPageModel);
        OrderModel orderModel = orderProcessModel.getOrder();
        orderData = getOrderConverter().convert(orderModel);
        orderData.setOrderStatusName(getEnumUtils().getNameFromEnumCode(orderModel.getStatus()));
        giftCoupons = orderData.getAppliedOrderPromotions().stream().filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).flatMap(p -> p.getGiveAwayCouponCodes().stream()).collect(Collectors.toList());
        B2BUnitModel b2bUnit = orderModel.getUnit();
        setTermsOfPayment(b2bUnit.getPaymentterms());
        setShippingConditions(b2bUnit.getShippingcondition());
        setBccEmails(emailPageModel);
    }
    public String getSiteBaseUrl() throws UnsupportedEncodingException {
        boolean fetchSecureUrl = true;
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), fetchSecureUrl, "");
    }

    private void setBccEmails(EmailPageModel emailPageModel) {
        if (CollectionUtils.isNotEmpty(emailPageModel.getBccEmails())) {
            put(BCC_EMAILS, emailPageModel.getBccEmails());
        } else {
            put(BCC_EMAILS, CollectionUtils.EMPTY_COLLECTION);
        }

    }

    @Override
    protected BaseSiteModel getSite(final OrderProcessModel orderProcessModel) {
        return orderProcessModel.getOrder().getSite();
    }

    @Override
    protected CustomerModel getCustomer(final OrderProcessModel orderProcessModel) {
        return (CustomerModel) orderProcessModel.getOrder().getUser();
    }

    protected Converter<OrderModel, OrderData> getOrderConverter() {
        return orderConverter;
    }

    @Required
    public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter) {
        this.orderConverter = orderConverter;
    }

    public OrderData getOrder() {
        return orderData;
    }

    @Override
    protected LanguageModel getEmailLanguage(final OrderProcessModel orderProcessModel) {
        return orderProcessModel.getOrder().getLanguage();
    }

    public List<CouponData> getCoupons() {
        return giftCoupons;
    }

    public ShippingCondition getShippingConditions() {
        return shippingConditions;
    }

    public void setShippingConditions(ShippingCondition shippingConditions) {
        this.shippingConditions = shippingConditions;
    }

    public PaymentKeyTerms getTermsOfPayment() {
        return termsOfPayment;
    }

    public void setTermsOfPayment(PaymentKeyTerms termsOfPayment) {
        this.termsOfPayment = termsOfPayment;
    }

    public TkEnumUtils getEnumUtils() {
        return enumUtils;
    }

    public void setEnumUtils(TkEnumUtils enumUtils) {
        this.enumUtils = enumUtils;
    }
}


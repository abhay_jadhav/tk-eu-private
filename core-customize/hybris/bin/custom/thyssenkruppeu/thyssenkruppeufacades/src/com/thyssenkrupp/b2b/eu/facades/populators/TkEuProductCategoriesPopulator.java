package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductCategoriesPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuProductCategoriesPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends ProductCategoriesPopulator<SOURCE, TARGET> {

    @Override
    public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException {
        // Overriding to prevent OOTB category population.
        // Categories populated in TkEuProductVariantMatrixPopulator as per custom implementation.
    }
}

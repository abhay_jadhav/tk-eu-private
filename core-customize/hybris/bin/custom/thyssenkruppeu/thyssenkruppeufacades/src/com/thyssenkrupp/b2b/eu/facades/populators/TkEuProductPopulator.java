package com.thyssenkrupp.b2b.eu.facades.populators;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatMtkName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants;
import com.thyssenkrupp.b2b.eu.facades.product.data.UnitData;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkB2bProductService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.variants.model.VariantProductModel;

public class TkEuProductPopulator implements Populator<ProductModel, ProductData> {
    private static final Logger LOG = Logger.getLogger(TkEuProductPopulator.class);
    private DefaultTkB2bProductService productService;
    private TkEuConfiguratorSettingsService configuratorSettingsService;
    private I18NService i18nService;
    private TkEuStorefrontCategoryProviderService tkEuStorefrontCategoryProviderService;
    private Converter<CategoryModel, CategoryData> categoryConverter;


    @Override
    public void populate(ProductModel productModel, ProductData productData) throws ConversionException {
        populateBaseProductData(productModel, productData);
        UnitModel baseUnit = productModel.getBaseUnit();
        if (baseUnit != null) {
            productData.setBaseUnit(generateBaseUnitData(baseUnit));
        }
        filterNormalCategories(productModel,productData);
        populateAttributes(productModel, productData);
    }

    private void populateBaseProductData(ProductModel productModel, ProductData productData) {
        if (StringUtils.isNotEmpty(productModel.getTkProductInfo())) {
            productData.setTkProductInfo(productModel.getTkProductInfo());
            TkTradeLengthConfiguratorSettingsModel setting = getConfiguratorSettingsService().getSelectedTradeLengthConfigurationForProduct(productModel);
            List<Map<String, String>> varianValues = trimVariantValues(getProductService().fetchProductInfo(productModel), setting);
            if (CollectionUtils.isNotEmpty(varianValues)) {
                productData.setVariantValues(varianValues);
            }
        }
        if (productModel instanceof VariantProductModel) {
            final VariantProductModel variantProduct = (VariantProductModel) productModel;
            if (variantProduct.getBaseProduct() != null) {
                productData.setBaseProductName(variantProduct.getBaseProduct().getName());
            } else {
                LOG.debug("base product is not loaded for the variant \"" + variantProduct.getCode() + "\"");
            }
        }
    }

    private List<Map<String, String>> trimVariantValues(List<Map<String, String>> variantValues, TkTradeLengthConfiguratorSettingsModel setting) {
        if (CollectionUtils.isEmpty(variantValues)) {
            return null;
        }
        Locale currentLocale = i18nService.getCurrentLocale();
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        for (Map<String, String> variantValue : variantValues) {
            Map<String, String> trimmedMap = new HashMap<String, String>();
            for (Map.Entry<String, String> entry : variantValue.entrySet()) {
                trimmedMap.put(entry.getKey().trim(), entry.getValue().trim());
            }
            if (StringUtils.containsIgnoreCase(trimmedMap.get("code"), ThyssenkruppeufacadesConstants.TRADE_LENGTH_IDENTIFIER) && setting != null) {
                if (trimmedMap.get("value").equals(setting.getDisplayValue(currentLocale))) {
                    result.add(trimmedMap);
                }
            } else {
                result.add(trimmedMap);
            }
        }
        return result;
    }

    private UnitData generateBaseUnitData(UnitModel unitModel) {
        final UnitData data = new UnitData();
        data.setCode(unitModel.getCode());
        data.setName(formatMtkName(unitModel.getName()));
        data.setSapCode(unitModel.getSapCode());
        data.setUnitType(unitModel.getUnitType());
        data.setZilliantUnit(unitModel.getZilliantUnit());
        return data;
    }

    private void filterNormalCategories(ProductModel productModel, ProductData productData) {

        final ProductModel baseProductModel = (productModel instanceof VariantProductModel) ? ((VariantProductModel) productModel).getBaseProduct() : productModel;
        Collection<CategoryModel> normalCategories = tkEuStorefrontCategoryProviderService.filterStorefrontCategories(baseProductModel.getSupercategories());
        productData.setNormalCategories(categoryConverter.convertAll(normalCategories));
    }

    private void populateAttributes(final ProductModel source, final ProductData target) {
        target.setSeoKeywords(source.getSeoKeywords());
        target.setSerpSnippet(source.getSerpSnippet());
        target.setAdditionalSearchKeywords(source.getAdditionalSearchKeywords());
        target.setProcessingOptions(source.getProcessingOptions());
    }

    public DefaultTkB2bProductService getProductService() {
        return productService;
    }

    public void setProductService(DefaultTkB2bProductService productService) {
        this.productService = productService;
    }

    public TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public TkEuStorefrontCategoryProviderService getTkEuStorefrontCategoryProviderService() {
        return tkEuStorefrontCategoryProviderService;
    }

    public void setTkEuStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkEuStorefrontCategoryProviderService) {
        this.tkEuStorefrontCategoryProviderService = tkEuStorefrontCategoryProviderService;
    }

    public Converter<CategoryModel, CategoryData> getCategoryConverter() {
        return categoryConverter;
    }

    public void setCategoryConverter(Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }
}

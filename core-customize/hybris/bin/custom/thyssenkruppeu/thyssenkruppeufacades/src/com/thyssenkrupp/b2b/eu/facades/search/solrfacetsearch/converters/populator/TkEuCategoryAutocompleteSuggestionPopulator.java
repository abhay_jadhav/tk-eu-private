package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.converters.populator;

import com.thyssenkrupp.b2b.eu.core.search.data.TkEuCategorySuggestion;
import com.thyssenkrupp.b2b.eu.facades.search.data.TkEuCategorySuggestionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuCategoryAutocompleteSuggestionPopulator<SOURCE extends TkEuCategorySuggestion, TARGET extends TkEuCategorySuggestionData> implements Populator<SOURCE, TARGET> {

    @Override
    public void populate(final SOURCE categorySuggesstion, final TARGET categorySuggesstionData) throws ConversionException {
        categorySuggesstionData.setCode(categorySuggesstion.getCode());
        categorySuggesstionData.setDisplayName(categorySuggesstion.getDisplayName());
        categorySuggesstionData.setNumOfProducts(categorySuggesstion.getNumOfProducts());
        categorySuggesstionData.setSeoURI(categorySuggesstion.getSeoURI());
        categorySuggesstionData.setMediaURI(categorySuggesstion.getMediaURI());
    }
}

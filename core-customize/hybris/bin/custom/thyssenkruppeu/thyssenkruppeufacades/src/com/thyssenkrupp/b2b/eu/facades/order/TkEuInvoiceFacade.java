package com.thyssenkrupp.b2b.eu.facades.order;

import de.hybris.platform.commercefacades.order.data.TkEuInvoiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

public interface TkEuInvoiceFacade {

    SearchPageData<TkEuInvoiceData> invoiceSearchList(String searchKey, PageableData pageableData);

    int invoiceSearchListCount(String searchKey);

    TkEuInvoiceData fetchInvoiceByCode(String invoiceNo);
}

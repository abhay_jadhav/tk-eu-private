package com.thyssenkrupp.b2b.eu.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

public class OrderCancelledEmailContext extends TkAbstractEmailContext<OrderProcessModel> {

    @Override
    public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel) {
        getOrderDetails(orderProcessModel, emailPageModel);
    }
}

package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.converters.populator;

import com.thyssenkrupp.b2b.eu.facades.product.data.TkEuPLPDisplayAttributeMap;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class TkEuSolrSearchResultProductPopulator<SOURCE extends SearchResultValueData, TARGET extends ProductData> implements Populator<SOURCE, TARGET> {

    private String shapeToDimensionsPLPDisplay;

    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        final Optional<Map<String, Object>> fieldValues = Optional.ofNullable(source).map(SearchResultValueData::getValues);
        if (fieldValues.isPresent() && fieldValues.get().containsKey(getShapeToDimensionsPLPDisplay())) {
            setTkEuPLPDisplayList(fieldValues.get(), target);
        }
    }

    private void setTkEuPLPDisplayList(final Map<String, Object> fieldValues, final TARGET target) {
        final Object tkEuPLPDisplayList = fieldValues.get(getShapeToDimensionsPLPDisplay());
        @SuppressWarnings("unchecked") final List<String> values = tkEuPLPDisplayList instanceof List ? (ArrayList<String>) tkEuPLPDisplayList : Collections.emptyList();
        target.setTkEuPLPDisplayList(convertAsPLPDisplayAttributeMapList(values));
    }

    private List<TkEuPLPDisplayAttributeMap> convertAsPLPDisplayAttributeMapList(List<String> values) {
        List<TkEuPLPDisplayAttributeMap> plpAttributeList = new ArrayList<TkEuPLPDisplayAttributeMap>();
        if (CollectionUtils.isNotEmpty(values)) {
            values.forEach(value -> {
                if (hasValidAttribute(value)) {
                    String[] split = value.split(":");
                    addPlpDisplayAttributeMap(split, plpAttributeList);
                }
            });
        }
        return plpAttributeList;
    }

    private boolean hasValidAttribute(String keyValueString) {
        return StringUtils.isNotBlank(keyValueString) && keyValueString.contains(":") && keyValueString.split(":").length == 2;
    }

    private void addPlpDisplayAttributeMap(String[] split, List<TkEuPLPDisplayAttributeMap> plpAttributeList) {
        TkEuPLPDisplayAttributeMap tkEuPLPDisplayAttributeMap = new TkEuPLPDisplayAttributeMap();
        tkEuPLPDisplayAttributeMap.setKey(split[0]);
        tkEuPLPDisplayAttributeMap.setValue(split[1]);
        plpAttributeList.add(tkEuPLPDisplayAttributeMap);
    }

    public String getShapeToDimensionsPLPDisplay() {
        return shapeToDimensionsPLPDisplay;
    }

    public void setShapeToDimensionsPLPDisplay(String shapeToDimensionsPLPDisplay) {
        this.shapeToDimensionsPLPDisplay = shapeToDimensionsPLPDisplay;
    }
}

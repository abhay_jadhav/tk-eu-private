package com.thyssenkrupp.b2b.eu.facades.populators;

import java.util.LinkedList;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;

public class TkEuB2bProductComparisonPopulator implements Populator<ProductModel, ProductData> {

    private static final String COMPAREPRODUCTLISTSIZE = "thyssenkrupp.compare.productlist.size";
    private static final String PRODUCTCOMPARESESSIONKEY = "comparisonProductCodeList";

    private SessionService sessionService;
    private SiteConfigService siteConfigService;

    @Override
    public void populate(final ProductModel source, final ProductData target) {
        populateAttributes(source, target);
    }

    private void populateAttributes(final ProductModel source, final ProductData target) {

        final int comparisonListSize = Integer.parseInt(getSiteConfigService().getProperty(COMPAREPRODUCTLISTSIZE));
        if (sessionService.getAttribute(PRODUCTCOMPARESESSIONKEY) == null) {
            target.setCurrentListCount(0);
            target.setRemainingCount(comparisonListSize);
            target.setCompareListAdded(false);
        } else {
            checkProductExists(source, target, comparisonListSize);
        }
    }

    private void checkProductExists(final ProductModel source, final ProductData target, final int comparisonListSize) {
        final LinkedList<String> comparisonProductList = new LinkedList<>();
        comparisonProductList.addAll(sessionService.getAttribute(PRODUCTCOMPARESESSIONKEY));
        if (comparisonProductList.isEmpty()) {
            target.setCompareListAdded(false);
        } else if (!isListContainsProduct(comparisonProductList, source.getCode())) {
            target.setCompareListAdded(false);
        } else {
            target.setCompareListAdded(true);
        }
        target.setCurrentListCount(comparisonProductList.size());
        target.setRemainingCount(comparisonListSize - comparisonProductList.size());
    }

    private boolean isListContainsProduct(final LinkedList<String> comparisonProductList, final String productCode) {
        return comparisonProductList.stream().filter(s -> s.equalsIgnoreCase(productCode)).findFirst().isPresent();
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public SiteConfigService getSiteConfigService() {
        return siteConfigService;
    }

    public void setSiteConfigService(final SiteConfigService siteConfigService) {
        this.siteConfigService = siteConfigService;
    }

}

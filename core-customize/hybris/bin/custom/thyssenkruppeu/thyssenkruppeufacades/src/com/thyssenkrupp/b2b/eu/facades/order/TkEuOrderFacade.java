package com.thyssenkrupp.b2b.eu.facades.order;

import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;

public interface TkEuOrderFacade extends OrderFacade {
    SearchPageData<OrderHistoryData> orderAcknowledgementSearchList(String searchKey, PageableData pageableData, OrderStatus... statuses);

    Integer findOrderAcknowledgementListCount(String searchKey, OrderStatus... statuses);

    OrderData getOrderConfirmationDetailsForCode(String code);

    SearchPageData<OrderHistoryData> orderConfirmationSearchList(String searchKey, PageableData pageableData, OrderStatus... statuses);

    Integer findOrderConfirmationListCount(String searchKey, OrderStatus... statuses);

    boolean oderConfirmationExists(String orderCode);

    String getSapOrderIdFromConfirmedOrder(String orderCode);

    String getSapOrderIdFromCancelledOrder(String orderCode);
}

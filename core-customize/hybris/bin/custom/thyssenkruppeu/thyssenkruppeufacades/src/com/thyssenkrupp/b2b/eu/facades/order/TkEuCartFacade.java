package com.thyssenkrupp.b2b.eu.facades.order;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

public interface TkEuCartFacade extends CartFacade {
    CommerceCartModification addToCart(CommerceCartParameter parameter) throws CommerceCartModificationException;

    CartModel createInMemoryCart();

    CartData converterCart(CartModel cartModel);

    CartData converterInMemoryCart(CartModel cartModel);

    boolean updateAtpInformation(List<AtpResponseData> atpResponseData);

    boolean updateAtpConsolidatedDate(Date atpConsolidatedDate);

    CommerceCartModification inMemoryAddToCart(AddToCartParams addToCartParams, @NotNull CartModel cartModel) throws CommerceCartModificationException;

    CommerceCartModification updateCertificateInCartEntry(int cartEntryNumber, String certificateCode);
    
    boolean removeInactiveProductsFromCart();

    void setCustmerDeliveryDateToLineItem(int cartEntryNumber, String wishDeliveryDate);

    Collection<String> getNotRedeemedCouponsFromCart();

    void removeNotRedeemedCouponFromCart();

}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticketsystem.data.CsTicketParameter;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;

public class TkEuCsTicketPopulator<SOURCE extends CsTicketParameter, TARGET extends CsTicketModel> implements Populator<SOURCE, TARGET> {

    private UserService userService;

    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
        populateTitleIfPresent(source, target);
        target.setFirstname(source.getFirstname());
        target.setEmail(source.getEmail());
        target.setLastname(source.getLastname());
        target.setCompany(source.getCompany());
        target.setPhone(source.getPhone());
        target.setPriority(source.getPriority());
        if (source.isCustomerRegistrationTicket()) {
            target.setBusiness(source.getBusiness());
            target.setState(source.getState());
            target.setCustomerRegistrationTicket(source.isCustomerRegistrationTicket());
            target.setCustomerNumber(source.getCustomerNumber());
        } else {
            try {
                if (!Objects.isNull(getUserService().getCurrentUser())) {
                    B2BCustomerModel b2BCustomerModelObj = getUserService().getUserForUID(getUserService().getCurrentUser().getUid(), B2BCustomerModel.class);
                    if (!Objects.isNull(b2BCustomerModelObj) && !Objects.isNull(b2BCustomerModelObj.getDefaultB2BUnit())) {
                        target.setCustomerNumber(b2BCustomerModelObj.getDefaultB2BUnit().getUid());
                    }
                }
            } catch (Exception e) {
                System.out.println("Error in fetching customer number" + e.getMessage());
            }
        }
    }

    private void populateTitleIfPresent(SOURCE source, TARGET target) {
        final TitleData titleData = source.getTitle();
        if (titleData != null && StringUtils.isNotEmpty(titleData.getCode())) {
            final TitleModel titleModel = getUserService().getTitleForCode(titleData.getCode());
            target.setTitle(titleModel);
        }
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}

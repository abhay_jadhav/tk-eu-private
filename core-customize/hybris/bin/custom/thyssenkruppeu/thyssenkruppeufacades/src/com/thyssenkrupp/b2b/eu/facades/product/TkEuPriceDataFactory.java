package com.thyssenkrupp.b2b.eu.facades.product;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.math.BigDecimal;

public interface TkEuPriceDataFactory extends PriceDataFactory {

    PriceData create(PriceDataType priceType, BigDecimal value, String currencyIso, String positiveSuffix);

    PriceData create(PriceDataType priceType, BigDecimal value, CurrencyModel currency, String positiveSuffix);

    PriceData createWithoutCurrency(PriceDataType priceType, BigDecimal value, String positiveSuffix);
    
    PriceData create(PriceDataType priceType, BigDecimal value, String currencyIso,BigDecimal valueWithPriceFactor, String positiveSuffix);


}

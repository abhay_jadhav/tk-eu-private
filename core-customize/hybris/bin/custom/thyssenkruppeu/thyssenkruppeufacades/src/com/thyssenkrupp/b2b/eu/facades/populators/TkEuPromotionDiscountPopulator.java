package com.thyssenkrupp.b2b.eu.facades.populators;

import java.math.BigDecimal;
import java.util.List;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

public class TkEuPromotionDiscountPopulator extends AbstractOrderPopulator<AbstractOrderModel, AbstractOrderData> {
    private static final String PROMOTION_DISCOUNT_CODE_PREFIX = "Action";
    private PriceDataFactory priceDataFactory;


    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        addPromotions(source, target);
        populateSubTotalWithoutDiscounts(source, target);
    }

    @Override
    protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype) {
        addPromotions(source, getPromotionsService().getPromotionResults(source), prototype);
    }

    @Override
    protected double getProductsDiscountsAmount(final AbstractOrderModel source) {
        double discounts = 0.0d;

        final List<AbstractOrderEntryModel> entries = source.getEntries();
        if (entries != null) {
            for (final AbstractOrderEntryModel entry : entries) {
                final List<DiscountValue> discountValues = entry.getDiscountValues();
                if (discountValues != null) {
                    for (final DiscountValue dValue : discountValues) {
                        if (dValue.getAppliedValue() > 0
                                && dValue.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)) {

                            discounts += dValue.getAppliedValue();
                        }
                    }
                }
            }
        }
        return discounts;
    }
    
    private void populateSubTotalWithoutDiscounts(AbstractOrderModel source, AbstractOrderData target) {
        final double subTotal = source.getSubtotal().doubleValue();
        target.setSubTotalWithoutDiscounts(createPrice(source, Double.valueOf(subTotal)));
    }

    protected PriceData createPrice(final AbstractOrderModel source, final Double val) {
        if (source == null) {
            throw new IllegalArgumentException("source order must not be null");
        }

        final CurrencyModel currency = source.getCurrency();
        if (currency == null) {
            throw new IllegalArgumentException("source order currency must not be null");
        }

        // Get double value, handle null as zero
        final double priceValue = val != null ? val.doubleValue() : 0d;

        return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);

    }

    /**
     * @return the priceDataFactory
     */
    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}

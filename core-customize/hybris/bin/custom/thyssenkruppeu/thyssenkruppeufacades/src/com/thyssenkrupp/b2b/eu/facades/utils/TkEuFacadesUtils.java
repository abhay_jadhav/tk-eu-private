package com.thyssenkrupp.b2b.eu.facades.utils;

import static com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants.UOM_SEPARATOR;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

public final class TkEuFacadesUtils {

    private TkEuFacadesUtils() {
        throw new UnsupportedOperationException();
    }

    public static String formatUom(String... arg) {

        String formattedUom = null;
        if (arg != null) {
            if (arg.length > 1) {
                formattedUom = " " + String.join(UOM_SEPARATOR, arg);
            } else {
                formattedUom = (UOM_SEPARATOR + arg[0]);
            }
        }
        return formatMtkName(formattedUom);
    }

    public static String formatMtkName(String name) {

        return name == null ? "" : name.replaceAll("m2", "m<sup>2</sup>");
    }

    public static Optional<Date> parseDate(final String dateToParse) {
        Date result = null;
        // no format needed cause were expecting standard e.g. 2017-04-14
        try {
            final LocalDate localDate = LocalDate.parse(dateToParse);
            result = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (final Exception e) {
        }
        return Optional.ofNullable(result);
    }
    
    public static String formatUomWithPriceFactor(long pricefactor, String uomName) {

        String formattedUom = null;
        if (pricefactor > 1L) {
                formattedUom = (UOM_SEPARATOR + pricefactor + " "+ uomName);
            } else {
                formattedUom = (UOM_SEPARATOR + uomName);
            }
                return formatMtkName(formattedUom);
    }
}

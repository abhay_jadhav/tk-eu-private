package com.thyssenkrupp.b2b.eu.facades.forgotpassword;

import static com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus.ACTIVATED;

import org.springframework.util.Assert;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.I18NService;

public class TkEuForgotPasswordPasswordFacade extends DefaultCustomerFacade implements ForgotPasswordFacade {

    private TkEuB2bCustomerService b2bCustomerService;
    private I18NService            i18NService;

    @Override
    public void forgottenPassword(final String uid) {
        Assert.hasText(uid, "The field [uid] cannot be empty");
        final CustomerModel customerModel = (CustomerModel) getB2bCustomerService().getAllUserForUID(uid.toLowerCase(getI18NService().getCurrentLocale()), CustomerModel.class);
        boolean validUser = validateUserStatus(customerModel);
        if (validUser) {
            getCustomerAccountService().forgottenPassword(customerModel);
        }
    }

    public boolean validateUserStatus(CustomerModel customerModel) {

        B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) customerModel;
        boolean userValidity = true;

        if (!b2BCustomerModel.getActive()) {
            userValidity = false;
        }
        if (b2BCustomerModel.getRegistrationStatus() != null && !b2BCustomerModel.getRegistrationStatus().equals(ACTIVATED)) {
            userValidity = false;
        }
        return userValidity;
    }

    public TkEuB2bCustomerService getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(TkEuB2bCustomerService b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }

    public I18NService getI18NService() {
        return i18NService;
    }

    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }

}

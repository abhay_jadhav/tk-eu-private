package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class TkEuInMemoryCartPopulator extends AbstractOrderPopulator<CartModel, CartData> {
    @Override
    public void populate(CartModel cartModel, CartData cartData) throws ConversionException {
        validateParameterNotNull(cartData, "Parameter cartData cannot be null");
        if (cartModel != null) {
            addCommon(cartModel, cartData);
            addEntries(cartModel, cartData);
            addTotals(cartModel, cartData);
        }
    }
}

package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TkEuProductCategorySearchPagePopulator<SOURCE extends ProductCategorySearchPageData, TARGET extends ProductCategorySearchPageData> implements Populator<SOURCE, TARGET> {

    private Converter searchStateConverter;

    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        @SuppressWarnings("unchecked") final Optional<BreadcrumbData> firstBreadCrumb = source.getBreadcrumbs().stream().findFirst();
        firstBreadCrumb.ifPresent(presentFirstBreadCrumb -> {
            final SolrSearchQueryData solrSearchQueryData = (SolrSearchQueryData) presentFirstBreadCrumb.getRemoveQuery();
            final List<SolrSearchQueryTermData> emptyFilterTerms = Collections.emptyList();
            solrSearchQueryData.setFilterTerms(emptyFilterTerms);
            target.setResetAllUrl(getResetUri(solrSearchQueryData));
        });
    }

    private String getResetUri(final SolrSearchQueryData searchQueryData) {
        @SuppressWarnings("unchecked") final Optional<SearchStateData> queryData = Optional.ofNullable((SearchStateData) getSearchStateConverter().convert(searchQueryData));
        return queryData.map(SearchStateData::getUrl).orElse("");
    }

    protected Converter getSearchStateConverter() {
        return searchStateConverter;
    }

    public void setSearchStateConverter(final Converter searchStateConverter) {
        this.searchStateConverter = searchStateConverter;
    }
}

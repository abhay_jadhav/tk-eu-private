package com.thyssenkrupp.b2b.eu.facades.constants;

public class ThyssenkruppeufacadesConstants extends GeneratedThyssenkruppeufacadesConstants {

    public static final String EXTENSIONNAME = "thyssenkruppeufacades";
    public static final String TRADE_LENGTH_IDENTIFIER = "Trade";
    public static final String UOM_SEPARATOR = " / ";
    public static final String ORDER_ENTRY_PARAM = "e";
    public static final String CERTIFICATE = "CERTIFICATE";
    public static final String EXPRESS = "express";
    public static final String CET_TIMEZONE = "Europe/Berlin";
    public static final String CERTIFICATEEXISTS = "TRUE";

    public static final String DELIVERY_ADDRESS = "delivery-address";
    public static final String BILLING_ADDRESS = "billing-address";
    public static final String SAP_DUMMY_PRODUCT = "sap_dummy_product";

}

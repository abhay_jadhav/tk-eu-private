package com.thyssenkrupp.b2b.eu.facades.populators;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuOrderHistoryPopulator implements Populator<OrderModel, OrderHistoryData> {
    @Override
    public void populate(final OrderModel source, final OrderHistoryData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        target.setDeliveryDate(source.getDeliveryDate());
        populateMiscAttributes(source, target);
    }

    protected void populateMiscAttributes(final OrderModel source, final OrderHistoryData target) {
        if (source.getName() != null) {
            target.setName(source.getName());
        }
        if (StringUtils.isNotEmpty(source.getSapOrderId())) {
            target.setSapOrderId(source.getSapOrderId());
        }
    }

}

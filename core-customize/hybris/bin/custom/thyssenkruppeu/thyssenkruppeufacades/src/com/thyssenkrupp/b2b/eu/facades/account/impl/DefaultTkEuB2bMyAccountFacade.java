/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.facades.account.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.ObjectUtils;

import com.thyssenkrupp.b2b.eu.facades.account.TkEuB2bMyAccountFacade;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCustomerAccountService;

import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

public class DefaultTkEuB2bMyAccountFacade implements TkEuB2bMyAccountFacade {

    private ModelService             modelService;
    private CustomerAccountService   customerAccountService;
    private CheckoutCustomerStrategy checkoutCustomerStrategy;
    private B2BCustomerService       b2bCustomerService;
    private UserService              userService;
    private B2BCommerceUnitService   b2BCommerceUnitService;
    private CustomerFacade         customerFacade;
    private B2BCommerceUnitService b2bCommerceUnitService;
    private UserFacade userFacade;
    private TkB2bCustomerAccountService defaultTkB2BCustomerAccountService;


    @Override
    public void saveCustomerShippingAddress(AddressModel addressModel) {

        getModelService().save(addressModel);
        getModelService().refresh(addressModel);
    }

    @Override
    public void savePrimaryShippingAddress(AddressModel addressModel) {
        setPrimaryShippingAddress(addressModel);
    }

    @Override
    public void removePrimaryShippingAddress() {
        setPrimaryShippingAddress( null);
    }

    @Override
    public void setCustomerAddressAsPrimaryAddress(final AddressData addressData) {

        validateParameterNotNullStandardMessage("addressData", addressData);
        final CustomerModel currentCustomer = getCurrentUserForCheckout();
        final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());

        setPrimaryShippingAddress(addressModel);
    }

    private void setPrimaryShippingAddress(AddressModel addressModel) {

        final CustomerModel currentCustomer = getCurrentUserForCheckout();
        final B2BCustomerModel b2BCustomerModel = getUserService().getUserForUID(currentCustomer.getUid(), B2BCustomerModel.class);
        b2BCustomerModel.setPrimaryShippingAddress(addressModel);
        getModelService().save(b2BCustomerModel);
        getModelService().refresh(b2BCustomerModel);
    }

    @Override
    public void setB2BAddressAsPrimaryAddress(final AddressData addressData) {

        validateParameterNotNullStandardMessage("addressData", addressData);
        final AddressModel b2bAddressModel = getB2BCommerceUnitService().getAddressForCode(getCustomerB2BUnitModel(), addressData.getId());

        savePrimaryShippingAddress(b2bAddressModel);
    }

    @Override
    public void setB2BAddressAsPrimaryBillingAddress(final AddressData addressData) {

        validateParameterNotNullStandardMessage("addressData", addressData);
        final CustomerModel currentCustomer = getCurrentUserForCheckout();
        final AddressModel b2bAddressModel = getB2BCommerceUnitService().getAddressForCode(getCustomerB2BUnitModel(), addressData.getId());
        final B2BCustomerModel b2BCustomerModel = getUserService().getUserForUID(currentCustomer.getUid(), B2BCustomerModel.class);
        b2BCustomerModel.setPrimaryBillingAddress(b2bAddressModel);
        getModelService().save(b2BCustomerModel);
        getModelService().refresh(b2BCustomerModel);
    }

    protected B2BUnitModel getCustomerB2BUnitModel() {
        final CustomerData b2bCustomerData = getCustomerFacade().getCurrentCustomer();
        final B2BUnitData b2bUnitData = b2bCustomerData.getUnit();
        return getB2bCommerceUnitService().getUnitForUid(b2bUnitData.getUid());
    }

    @Override
    public void checkPrimaryShippingAddress(boolean isPrimary, AddressData newAddress) {
        if (isPrimary)
            setCustomerAddressAsPrimaryAddress(newAddress);
        else {
            final B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) getCurrentUserForCheckout();
            if (ObjectUtils.nullSafeEquals(b2BCustomerModel.getPrimaryShippingAddress(), getCustomerAccountService().getAddressForCode(b2BCustomerModel, newAddress.getId())))
                removePrimaryShippingAddress();
        }
    }

    public CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    public B2BCommerceUnitService getB2bCommerceUnitService() {
        return b2bCommerceUnitService;
    }

    public void setB2bCommerceUnitService(final B2BCommerceUnitService b2bCommerceUnitService) {
        this.b2bCommerceUnitService = b2bCommerceUnitService;
    }

    public B2BCommerceUnitService getB2BCommerceUnitService() {
        return b2BCommerceUnitService;
    }

    public void setB2BCommerceUnitService(B2BCommerceUnitService b2BCommerceUnitService) {
        this.b2BCommerceUnitService = b2BCommerceUnitService;
    }

    public CustomerAccountService getCustomerAccountService() {
        return customerAccountService;
    }

    public void setCustomerAccountService(CustomerAccountService customerAccountService) {
        this.customerAccountService = customerAccountService;
    }

    protected CustomerModel getCurrentUserForCheckout() {
        return getCheckoutCustomerStrategy().getCurrentUserForCheckout();
    }

    public CheckoutCustomerStrategy getCheckoutCustomerStrategy() {
        return checkoutCustomerStrategy;
    }

    @Required
    public void setCheckoutCustomerStrategy(CheckoutCustomerStrategy checkoutCustomerStrategy) {
        this.checkoutCustomerStrategy = checkoutCustomerStrategy;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public B2BCustomerService getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(B2BCustomerService b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }

    public UserFacade getUserFacade() {
        return userFacade;
    }

    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Override
    public boolean validatePasswordToken(SecureToken data) {
        return defaultTkB2BCustomerAccountService.validatePasswordToken(data);
    }

    public void setDefaultTkB2BCustomerAccountService(TkB2bCustomerAccountService defaultTkB2BCustomerAccountService) {
        this.defaultTkB2BCustomerAccountService = defaultTkB2BCustomerAccountService;
    }

}

package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static java.util.Objects.nonNull;

public class TkEuAtpRequestEntryDataPopulator implements Populator<OrderEntryData, AtpRequestEntryData> {

    @Override
    public void populate(OrderEntryData orderEntryData, AtpRequestEntryData atpRequestEntryData) throws ConversionException {
        atpRequestEntryData.setPosition(orderEntryData.getEntryNumber());
        atpRequestEntryData.setProductCode(orderEntryData.getProduct().getCode());
        atpRequestEntryData.setQuantity(orderEntryData.getQuantity());
        atpRequestEntryData.setSalesUnit(SAP_KGM_UNIT_CODE);
        Double getTotalWeightInKg = nonNull(orderEntryData.getTotalWeightInKg()) ? orderEntryData.getTotalWeightInKg().getValue().doubleValue() : 0.0;
        atpRequestEntryData.setQuantityInKg(getTotalWeightInKg);
        atpRequestEntryData.setConfigurationInfos(orderEntryData.getConfigurationInfos());
    }
}

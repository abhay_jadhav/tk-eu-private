package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.impl;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.thyssenkrupp.b2b.eu.core.search.data.TkEuCategorySuggestion;
import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.CategorySearchAutocompleteService;
import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.impl.TkEuSolrProductSearchService;
import com.thyssenkrupp.b2b.eu.facades.search.data.TkEuCategorySuggestionData;
import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.FavouriteProductsHook;
import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.TkEuProductSearchFacade;
import com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuSolrProductSearchFacade<ITEM extends ProductData> extends DefaultSolrProductSearchFacade implements TkEuProductSearchFacade {

    private static final int MAX_PAGE_LIMIT = 100;
    private static final Logger LOG = Logger.getLogger(TkEuSolrProductSearchFacade.class);

    private Converter<TkEuCategorySuggestion, TkEuCategorySuggestionData> categoryAutocompleteSuggestionConverter;
    private CategorySearchAutocompleteService<TkEuCategorySuggestion>     categoryAutoCompleteService;
    private TkEuSolrProductSearchService tkProductSearchService;
    private List<FavouriteProductsHook> favouriteProductsMethodHooks;

    @Override
    public List<TkEuCategorySuggestionData> getCategorySuggestions(final String input) {
        return getThreadContextService().executeInContext((ThreadContextService.Executor<List<TkEuCategorySuggestionData>, ThreadContextService.Nothing>) () ->
          Converters.convertAll(getCategoryAutoCompleteService().getCategoryAutocompleteSuggestion(input), getCategoryAutocompleteSuggestionConverter()));
    }

    @Override
    public ProductSearchPageData searchByProductCodes(final List productCodes) {
        String productCodeSearchQuery = convertProductCodesAsString(productCodes);
        final SearchStateData searchState = getSearchState(productCodeSearchQuery);
        return productCodeTextSearch(searchState, createPageableData());
    }

    private String convertProductCodesAsString(List<String> productCodes) {
        if (CollectionUtils.isNotEmpty(productCodes)) {
            return StringUtils.join(productCodes, " OR ");
        }
        return "";
    }

    private ProductSearchPageData<SearchStateData, ITEM> productCodeTextSearch(final SearchStateData searchState, final PageableData pageableData) {
        Assert.notNull(searchState, "SearchStateData must not be null.");

        return getThreadContextService().executeInContext(
          new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>() {
              @Override
              public ProductSearchPageData<SearchStateData, ITEM> execute() {
                        return getProductCategorySearchPageConverter().convert(getTkProductSearchService().productCodeSearch(decodeState(searchState, null), pageableData));
              }
          });
    }

    private PageableData createPageableData() {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(MAX_PAGE_LIMIT);
        return pageableData;
    }

    @Override
    public Converter<ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>, ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>> getProductCategorySearchPageConverter() {
        return super.getProductCategorySearchPageConverter();
    }

    protected CategorySearchAutocompleteService<TkEuCategorySuggestion> getCategoryAutoCompleteService() {
        return categoryAutoCompleteService;
    }

    @Required
    public void setCategoryAutoCompleteService(CategorySearchAutocompleteService<TkEuCategorySuggestion> categoryAutoCompleteService) {
        this.categoryAutoCompleteService = categoryAutoCompleteService;
    }

    protected Converter<TkEuCategorySuggestion, TkEuCategorySuggestionData> getCategoryAutocompleteSuggestionConverter() {
        return categoryAutocompleteSuggestionConverter;
    }

    public void setCategoryAutocompleteSuggestionConverter(Converter<TkEuCategorySuggestion, TkEuCategorySuggestionData> categoryAutocompleteSuggestionConverter) {
        this.categoryAutocompleteSuggestionConverter = categoryAutocompleteSuggestionConverter;
    }

    public ProductSearchPageData<SearchStateData, ITEM> favouriteProductsTextSearch(final String text) {
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>() {
            @Override
            public ProductSearchPageData<SearchStateData, ITEM> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsTextSearch(text, null, null));
            }
        });
    }

    public ProductSearchPageData<SearchStateData, ITEM> favouriteProductsTextSearch(final String text, final SearchQueryContext searchQueryContext) {
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>() {
            @Override
            public ProductSearchPageData<SearchStateData, ITEM> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsTextSearch(text, searchQueryContext, null));
            }
        });
    }

    public ProductSearchPageData<SearchStateData, ITEM> favouriteProductsTextSearch(final SearchStateData searchState, final PageableData pageableData) {

        Assert.notNull(searchState, "SearchStateData must not be null.");
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>() {
            @Override
            public ProductSearchPageData<SearchStateData, ITEM> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsSearch(decodeState(searchState, null), pageableData));
            }
        });
    }

    public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> favouriteProductsCategorySearch(final String categoryCode) {
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>, ThreadContextService.Nothing>() {
            @Override
            public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsCategorySearch(categoryCode, null, null));
            }
        });
    }

    public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> favouriteProductsCategorySearch(final String categoryCode, final SearchQueryContext searchQueryContext) {
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>, ThreadContextService.Nothing>() {
            @Override
            public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsCategorySearch(categoryCode, searchQueryContext, null));
            }
        });
    }

    public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> favouriteProductsCategorySearch(final String categoryCode, final SearchStateData searchState, final PageableData pageableData) {

        Assert.notNull(searchState, "SearchStateData must not be null.");
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>, ThreadContextService.Nothing>() {
            @Override
            public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsSearch(decodeState(searchState, categoryCode), pageableData));
            }
        });
    }

    public ProductSearchPageData<SearchStateData, ITEM> favouriteProductsSearch(final SearchStateData searchState, final PageableData pageableData) {
        Assert.notNull(searchState, "SearchStateData must not be null.");
        return getThreadContextService().executeInContext(new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>() {
            @Override
            public ProductSearchPageData<SearchStateData, ITEM> execute() {
                return getProductCategorySearchPageConverter().convert(getTkProductSearchService().favouriteProductsSearch(decodeState(searchState, null), pageableData));
            }
        });
    }

    public List<String> getFavouriteProducts() {
        if (CollectionUtils.isNotEmpty(getFavouriteProductsMethodHooks())) {
            FavouriteProductsHook favouriteProductsHook = getFavouriteProductsMethodHooks().get(0);
            if (favouriteProductsHook != null) {
                return favouriteProductsHook.getFavouriteProducts();
            }
        }
        return Collections.emptyList();
    }

    public FavouriteFacetData populateFavouriteFacetData(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        FavouriteFacetData favouriteFacetData = new FavouriteFacetData();
        if (searchPageData != null) {
            favouriteFacetData.setCount(searchPageData.getPagination().getTotalNumberOfResults());
            favouriteFacetData.setEnabled(true);
            final String encodedCurrentQuery = getEncodedCurrentQuery(searchPageData);
            favouriteFacetData.setActionUrl(encodedCurrentQuery);
            favouriteFacetData.setRemoveUrl(getFavSelectionRemoveUrl(searchPageData.getCurrentQuery().getUrl()));
            LOG.debug("********* encodedCurrentQueryValue : " + encodedCurrentQuery);
        }
        return favouriteFacetData;
    }

    private String getFavSelectionRemoveUrl(final String path) {
        if (StringUtils.isBlank(path)) {
            return path;
        }
        try {
            URIBuilder uriBuilder = new URIBuilder(path);
            uriBuilder.addParameter("favourites", "false");
            return uriBuilder.toString();
        } catch (URISyntaxException ex) {
            LOG.error("URISyntaxException in converting the url : " + path, ex);
        }
        return path;
    }

    protected String getEncodedCurrentQuery(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        final SearchStateData currentQuery = searchPageData.getCurrentQuery();
        if (currentQuery != null && currentQuery.getQuery() != null) {
            return currentQuery.getQuery().getValue();
        }
        return "";
    }

    public boolean shouldExecuteFavProductsSearch(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        final List<String> favoriteProducts = getFavouriteProducts();
        return getTotalNumResults(searchPageData) > 1 && CollectionUtils.isNotEmpty(favoriteProducts);
    }

    public long getTotalNumResults(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {
        long numRecs = 0;
        if (searchPageData != null) {
            numRecs = searchPageData.getPagination().getTotalNumberOfResults();
        }
        return numRecs;
    }

    public SearchStateData getSearchState(final String searchText) {
        final SearchStateData searchState = new SearchStateData();
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchQueryData.setValue(searchText);
        searchState.setQuery(searchQueryData);
        return searchState;
    }

    public String unescapeHtml(final String encodedSearchText) {
        if (StringUtils.isBlank(encodedSearchText)) {
            return encodedSearchText;
        }
        String searchText = encodedSearchText;
        searchText = searchText.replaceAll("&#40;", "\\(").replaceAll("&#41;", "\\)");
        searchText = searchText.replaceAll("&#39;", "'");
        return searchText;
    }

    public void setSearchStringToSearchPageData(final ProductSearchPageData<SearchStateData, ProductData> searchPageData, final String searchString) {
        if (StringUtils.isNotBlank(searchString)) {
            searchPageData.setFreeTextSearch(searchString);
        }
    }

    public List<FavouriteProductsHook> getFavouriteProductsMethodHooks() {
        return favouriteProductsMethodHooks;
    }

    public void setFavouriteProductsMethodHooks(List<FavouriteProductsHook> favouriteProductsMethodHooks) {
        this.favouriteProductsMethodHooks = favouriteProductsMethodHooks;
    }

    public TkEuSolrProductSearchService getTkProductSearchService() {
        return tkProductSearchService;
    }

    public void setTkProductSearchService(TkEuSolrProductSearchService tkProductSearchService) {
        this.tkProductSearchService = tkProductSearchService;
    }

}

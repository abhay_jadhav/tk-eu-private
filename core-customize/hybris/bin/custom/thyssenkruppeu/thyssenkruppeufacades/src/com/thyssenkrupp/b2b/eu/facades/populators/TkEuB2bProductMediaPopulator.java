package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkMediaService;

public class TkEuB2bProductMediaPopulator implements Populator<ProductModel, ProductData> {
    private DefaultTkMediaService mediaService;

    @Override
    public void populate(final ProductModel source, final ProductData target) throws ConversionException {
        if (source instanceof VariantProductModel) {
            populateMediaBasedOnMimeType(((VariantProductModel) source).getBaseProduct(), target);
        } else {
            populateMediaBasedOnMimeType(source, target);
        }
    }

    private void populateMediaBasedOnMimeType(final ProductModel source, final ProductData target) {
        if (CollectionUtils.isNotEmpty(source.getData_sheet())) {
            final List<MediaModel> productMediaList = (List<MediaModel>) source.getData_sheet();
            final List<MediaData> productMediaDataList = new ArrayList<>();
            for (final MediaModel productMedia : productMediaList) {
                final MediaData productMediaData = new MediaData();
                productMediaData.setDocumentTitle(productMedia.getDocumentTitle());
                productMediaData.setDownloadUrl(productMedia.getDownloadURL());
                final String mediaIconUrl = getMediaService().getMediaIconUrlFromMimeType(productMedia.getMime());
                productMediaData.setMediaIconUrl(mediaIconUrl);
                productMediaDataList.add(productMediaData);
            }
            target.setMediaData(productMediaDataList);
        }

    }

    public DefaultTkMediaService getMediaService() {
        return mediaService;
    }

    public void setMediaService(final DefaultTkMediaService mediaService) {
        this.mediaService = mediaService;
    }

}

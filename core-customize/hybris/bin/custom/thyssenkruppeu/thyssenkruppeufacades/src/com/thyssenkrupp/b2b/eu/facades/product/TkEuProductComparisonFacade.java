package com.thyssenkrupp.b2b.eu.facades.product;

import java.util.LinkedHashMap;
import java.util.List;

public interface TkEuProductComparisonFacade {

    LinkedHashMap<String, String> getComparisonData(List<String> comparisonProductList, String[] attributeList);
}

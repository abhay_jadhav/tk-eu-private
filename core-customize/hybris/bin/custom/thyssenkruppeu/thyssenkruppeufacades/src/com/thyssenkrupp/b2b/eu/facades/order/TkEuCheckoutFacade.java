package com.thyssenkrupp.b2b.eu.facades.order;

import java.util.List;

import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.InvalidCartException;

public interface TkEuCheckoutFacade extends CheckoutFacade {

    void setBillingAddress(String selectedAddressCode);

    void setBillingDetails(String orderName, String poNumber);

    void saveNewShippingAddress(AddressModel addressModel);

    CartData getNewShippingAddress();

    String checkShippingTerms(CartData cartData);

    String checkPaymentTerms(CartData cartData);

    String checkIncoterms(CartData cartData);

    boolean hasNoDeliveryAddress();

    <T extends AbstractOrderData> T placeOrder(PlaceOrderData placeOrderData) throws InvalidCartException;

    AddressData getPrimaryShippingAddress();

    List<AddressData> getCustomerB2bUnitShippingAddress();

    List<AddressData> getCustomerB2bUnitBillingAddress();

    List<AddressData> getB2bUnitContactAddress();

    List<AddressData> getB2bUnitCompanyAddress();

    void setTradeLengthAttributes(OrderEntryData orderEntryData);

    void setSawingAttributes(OrderEntryData target);

    void setCartPaymentType();

    void setCustomerShippingNotes(String shippingNotes);

    CartData updateCheckoutCart(CartData cartData);

    List<AddressData> getB2bUnitShippingAddress();

    List<AddressData> getB2bUnitBillingAddress();

}

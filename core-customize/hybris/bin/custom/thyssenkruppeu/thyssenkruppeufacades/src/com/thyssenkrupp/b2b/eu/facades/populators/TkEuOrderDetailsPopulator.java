package com.thyssenkrupp.b2b.eu.facades.populators;

import org.apache.commons.collections.CollectionUtils;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

public class TkEuOrderDetailsPopulator implements Populator<AbstractOrderModel, OrderData> {

    public void populate(final AbstractOrderModel source, final OrderData target) {
        target.setStatus(source.getStatus());
        target.setExportStatus(source.getExportStatus());
        if (source instanceof OrderModel) {
            OrderModel order = (OrderModel) source;
            target.setDeliveryDate(order.getDeliveryDate());
        }
        populateCancelledEntryCount(source,target);
    }

    private void populateCancelledEntryCount(AbstractOrderModel source, OrderData target) {

        try {
            if(CollectionUtils.isNotEmpty(source.getEntries())){
                target.setCancelledEntryCount((int)source.getEntries().stream().filter(entry -> OrderEntryStatus.DEAD.equals(entry.getQuantityStatus())).count());
            }else {
            target.setCancelledEntryCount(0);
            }
        }catch(Exception e) {
            target.setCancelledEntryCount(0);
        }
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuAddressPopulator implements Populator<AddressModel, AddressData> {

    @Override
    public void populate(AddressModel addressModel, AddressData addressData) throws ConversionException {

        Assert.notNull(addressModel, "Address model cannot be null");
        Assert.notNull(addressData, "AddressData cannot be null");

        addressData.setFax(addressModel.getFax());
        addressData.setPhone2(addressModel.getPhone2());
        addressData.setCellPhone(addressModel.getCellphone());
        addressData.setStreetName(addressModel.getLine1());
        addressData.setStreetNumber(addressModel.getLine2());
        if (StringUtils.isNotEmpty(addressModel.getDepartment())) {
            addressData.setDepartment(addressModel.getDepartment());
        }
    }
}

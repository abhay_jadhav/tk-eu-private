package com.thyssenkrupp.b2b.eu.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

public class OrderRefundEmailContext extends TkAbstractEmailContext<OrderProcessModel> {

    private PriceData refundAmount;

    @Override
    public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel) {
        getOrderDetails(orderProcessModel, emailPageModel);
        orderData = getOrderConverter().convert(orderProcessModel.getOrder());
        refundAmount = orderData.getTotalPrice();
    }

    public PriceData getRefundAmount() {
        return refundAmount;
    }
}

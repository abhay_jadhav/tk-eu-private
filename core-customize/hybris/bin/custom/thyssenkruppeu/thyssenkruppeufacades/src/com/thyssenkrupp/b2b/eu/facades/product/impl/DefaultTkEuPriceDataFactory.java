package com.thyssenkrupp.b2b.eu.facades.product.impl;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class DefaultTkEuPriceDataFactory extends DefaultPriceDataFactory implements TkEuPriceDataFactory {

    protected final ConcurrentMap<String, NumberFormat> conversionFormats = new ConcurrentHashMap<>();

    @Override
    public PriceData create(PriceDataType priceType, BigDecimal value, String currencyIso, String positiveSuffix) {
        final CurrencyModel currency = getCommonI18NService().getCurrency(currencyIso);
        PriceData priceData = create(priceType, value, currency, positiveSuffix);
        return priceData;
    }

    @Override
    public PriceData create(PriceDataType priceType, BigDecimal value, CurrencyModel currency, String positiveSuffix) {
        Assert.notNull(positiveSuffix, "Parameter positiveSuffix cannot be null.");
        PriceData priceData = create(priceType, value, currency);
        priceData.setFormattedValueWithUnit(formatPrice(value, currency, positiveSuffix));
        priceData.setFormattedUnit(positiveSuffix);
        return priceData;
    }

    @Override
    public PriceData createWithoutCurrency(PriceDataType priceType, BigDecimal value, String positiveSuffix) {
        Assert.notNull(priceType, "Parameter priceType cannot be null.");
        Assert.notNull(value, "Parameter value cannot be null.");
        Assert.notNull(positiveSuffix, "Parameter positiveSuffix cannot be null.");
        final PriceData priceData = createPriceData(priceType, value, positiveSuffix);
        return priceData;
    }

    protected PriceData createPriceData(PriceDataType priceType, BigDecimal value, String positiveSuffix) {
        final PriceData priceData = createPriceData();
        priceData.setPriceType(priceType);
        priceData.setValue(value);
        Locale locale = getCurrentLocale();
        DecimalFormat numberInstance = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        numberInstance.setPositiveSuffix(positiveSuffix);
        priceData.setFormattedValueWithUnit(numberInstance.format(value));
        priceData.setFormattedUnit(positiveSuffix);
        return priceData;
    }

    protected Locale getCurrentLocale() {
        final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
        Locale locale = getCommerceCommonI18NService().getLocaleForLanguage(currentLanguage);
        if (locale == null) {
            // Fallback to session locale
            locale = getI18NService().getCurrentLocale();
        }
        return locale;
    }

    protected String formatPrice(final BigDecimal value, final CurrencyModel currency, String positiveSuffix) {
        Locale locale = getCurrentLocale();
        final NumberFormat currencyFormat = createConversionFormat(locale, currency, positiveSuffix);
        return currencyFormat.format(value).concat(positiveSuffix);
    }

    protected NumberFormat createConversionFormat(final Locale locale, final CurrencyModel currency, String positiveSuffix) {
        final String key = locale.getISO3Country() + "_" + currency.getIsocode();

        NumberFormat numberFormat = conversionFormats.get(key);
        if (numberFormat == null) {
            final NumberFormat currencyFormat = createConversionFormat(locale, currency);
            numberFormat = conversionFormats.putIfAbsent(key, currencyFormat);
            if (numberFormat == null) {
                numberFormat = currencyFormat;
            }
        }
        // don't allow multiple references
        return (NumberFormat) numberFormat.clone();
    }

    protected NumberFormat createConversionFormat(final Locale locale, final CurrencyModel currency) {
        final DecimalFormat currencyFormat = (DecimalFormat) NumberFormat.getCurrencyInstance(locale);
        adjustConversionDigits(currencyFormat, currency);
        adjustSymbol(currencyFormat, currency);
        return currencyFormat;
    }

    protected DecimalFormat adjustConversionDigits(final DecimalFormat format, final CurrencyModel currencyModel) {
        final int tempDigits = currencyModel.getDigits() == null ? 0 : currencyModel.getDigits().intValue();
        final int tempConversionDigits = currencyModel.getDigits() == null ? 0 : currencyModel.getConversionDigits().intValue();
        final int digits = Math.max(0, tempDigits);
        final int conversionDigits = Math.max(digits, tempConversionDigits);

        format.setMaximumFractionDigits(conversionDigits);
        format.setMinimumFractionDigits(digits);
        if (digits == 0) {
            format.setDecimalSeparatorAlwaysShown(false);
        }

        return format;
    }

    @Override
    public PriceData create(PriceDataType priceType, BigDecimal value, String currencyIso,
            BigDecimal valueWithPriceFactor, String positiveSuffix) {
          Assert.notNull(positiveSuffix, "Parameter positiveSuffix cannot be null.");
          final CurrencyModel currency = getCommonI18NService().getCurrency(currencyIso);
            PriceData priceData = create(priceType, value, currency);
            priceData.setFormattedValueWithUnit(formatPrice(valueWithPriceFactor, currency, positiveSuffix));
            priceData.setFormattedUnit(positiveSuffix);
            return priceData;
    }
}

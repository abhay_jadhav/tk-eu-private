package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class TkEuProductUomConversionPopulator<S extends ProductModel, T extends ProductData> extends AbstractProductPopulator<S, T> {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuProductUomConversionPopulator.class);

    private TkEuUomConversionService uomConversionService;

    private Converter<List<TkUomConversionModel>, List<SalesUnitData>> tkEuSalesDataConverter;

    @Override
    public void populate(S s, T t) {
        List<TkUomConversionModel> supportedUomConversions = null;
        try {
            supportedUomConversions = getUomConversionService().getStoreSupportedUomConversions(s);
            List<SalesUnitData> salesUnitDataList = getTkEuSalesDataConverter().convert(supportedUomConversions);
            updateIsDefaultUnit(salesUnitDataList, s);
            t.setSupportedSalesUnits(salesUnitDataList);
        } catch (TkEuPriceServiceException ex) {
            LOG.error("Can not find SalesUnitData with following reason", ex);
        }
    }

    protected void updateIsDefaultUnit(List<SalesUnitData> salesUnitDataList, ProductModel productModel) {
        if (productModel.getUnit() != null && CollectionUtils.isNotEmpty(salesUnitDataList)) {
            salesUnitDataList.forEach(salesUnitData -> {
                if (productModel.getUnit().getCode().equals(salesUnitData.getCode())) {
                    salesUnitData.setIsDefaultUnit(true);
                }
            });
        }
    }

    public Converter<List<TkUomConversionModel>, List<SalesUnitData>> getTkEuSalesDataConverter() {
        return tkEuSalesDataConverter;
    }

    @Required
    public void setTkEuSalesDataConverter(Converter<List<TkUomConversionModel>, List<SalesUnitData>> tkEuSalesDataConverter) {
        this.tkEuSalesDataConverter = tkEuSalesDataConverter;
    }

    public TkEuUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    public void setUomConversionService(TkEuUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }
}

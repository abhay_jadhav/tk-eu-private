package com.thyssenkrupp.b2b.eu.facades.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Iterables;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCommerceCartService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCartFacade;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.promotionengineservices.model.AbstractRuleBasedPromotionActionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class DefaultTkEuCartFacade extends DefaultCartFacade implements TkEuCartFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCartFacade.class);
    private CartFactory inMemoryCartFactory;
    private Converter<CartModel, CartData> inMemoryCartConverter;
    private Converter<AtpResponseData, CommerceCartParameter> commerceAtpCartParameterConverter;
    private TkEuB2bCommerceCartService tkEuB2bCommerceCartService;
    private VoucherFacade voucherFacade;
    
    
    public VoucherFacade getVoucherFacade() {
        return voucherFacade;
    }

    public void setVoucherFacade(VoucherFacade voucherFacade) {
        this.voucherFacade = voucherFacade;
    }

    @Override
    public CommerceCartModification addToCart(CommerceCartParameter parameters)
            throws CommerceCartModificationException {
        validateParameterNotNull(parameters.getCart(), "Cart model cannot be null");
        validateParameterNotNull(parameters.getProduct(), "Product model cannot be null");
        validateParameterNotNull(parameters.getQuantity(), "Quantity cannot be null");
        return getCommerceCartService().addToCart(parameters);
    }

    @Override
    public CartModel createInMemoryCart() {

        return getInMemoryCartFactory().createCart();
    }

    @Override
    public CartData converterCart(CartModel cartModel) {
        return getCartConverter().convert(cartModel);
    }

    @Override
    public CartData converterInMemoryCart(CartModel cartModel) {
        return getInMemoryCartConverter().convert(cartModel);
        }

    @Override
    public boolean updateAtpInformation(List<AtpResponseData> atpResponseData) {
        final CartModel sessionCart = getCartService().getSessionCart();
        if (sessionCart != null && atpResponseData != null) {
            final List<CommerceCartParameter> commerceCartParameters = Converters.convertAll(atpResponseData,
                    commerceAtpCartParameterConverter);
            for (CommerceCartParameter commerceCartParameter : commerceCartParameters) {
                commerceCartParameter.setCart(sessionCart);
                getTkEuB2bCommerceCartService().updateAtpInformation(commerceCartParameter);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean updateAtpConsolidatedDate(Date atpConsolidatedDate) {
        final CartModel sessionCart = getCartService().getSessionCart();
        if (sessionCart != null && atpConsolidatedDate != null) {
            CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
            commerceCartParameter.setCart(sessionCart);
            commerceCartParameter.setAtpConsolidatedDate(atpConsolidatedDate);
            getTkEuB2bCommerceCartService().updateAtpConsolidatedDate(commerceCartParameter);
            return true;
        }
        return false;
    }

    @Override
    public CommerceCartModification inMemoryAddToCart(@NotNull AddToCartParams addToCartParams,
            @NotNull CartModel cartModel) throws CommerceCartModificationException {
        CommerceCartParameter commerceCartParameter = getCommerceCartParameterConverter().convert(addToCartParams);
        ProductModel productModel = commerceCartParameter.getProduct();
        commerceCartParameter.setProduct(getFirstVariantForProduct(productModel).orElse(productModel));
        commerceCartParameter.setCreateNewEntry(true);
        commerceCartParameter.setSkipPromotionCalculation(true);
        commerceCartParameter.setCart(cartModel);
        return addToCart(commerceCartParameter);
    }

    private Optional<ProductModel> getFirstVariantForProduct(ProductModel productModel) {
        if (productModel != null && productModel.getVariantType() != null
                && CollectionUtils.isNotEmpty(productModel.getVariants())) {
            return Optional.ofNullable(Iterables.get(productModel.getVariants(), 0));
        }
        return Optional.empty();
    }

    @Override
    public CartRestorationData restoreSavedCart(final String guid) throws CommerceCartRestorationException {
        if (!hasEntries() && !hasEntryGroups()) {
            getCartService().setSessionCart(null);
        }

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setEnableHooks(true);
        final CartModel cartForGuidAndSiteAndUser = getCommerceCartService().getCartForGuidAndSiteAndUser(guid,
                getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
        parameter.setCart(cartForGuidAndSiteAndUser);

        checkForCartCalculation(cartForGuidAndSiteAndUser, parameter);

        return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
    }

    @Override
    public CommerceCartModification updateCertificateInCartEntry(final int cartEntryNumber,
            final String certificateCode) {
        final CartModel cartModel = getCartService().getSessionCart();
        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cartModel);
        parameter.setEntryNumber(cartEntryNumber);
        parameter.setCertificateCode(certificateCode);
        return getTkEuB2bCommerceCartService().updateCertificateForCartEntry(parameter);
    }

    private void checkForCartCalculation(CartModel cartForGuidAndSiteAndUser, CommerceCartParameter parameter) {
        if (cartForGuidAndSiteAndUser == null) {
            return;
        }
        BaseSiteModel baseSite = getBaseSiteService().getCurrentBaseSite();
        Optional<Long> activePricingSequenceId = Optional.empty();
        if (baseSite != null) {
            CMSSiteModel site = (CMSSiteModel) baseSite;
            CatalogModel defaultCatalog = site.getDefaultCatalog();
            if (defaultCatalog != null && defaultCatalog.getActiveCatalogVersion() != null) {
                activePricingSequenceId = Optional
                        .ofNullable(defaultCatalog.getActiveCatalogVersion().getPricingSequenceId());
            }
        }

        Optional<Long> cartPricingSequenceId = Optional.ofNullable(cartForGuidAndSiteAndUser.getPricingSequenceId());

        if (activePricingSequenceId.isPresent() && cartPricingSequenceId.isPresent()
                && activePricingSequenceId.equals(cartPricingSequenceId)) {
            parameter.setSkipCartCalculation(true);
        } else if (activePricingSequenceId.isPresent()) {
            cartForGuidAndSiteAndUser.setPricingSequenceId(activePricingSequenceId.get());
        }
    }

    public TkEuB2bCommerceCartService getTkEuB2bCommerceCartService() {
        return tkEuB2bCommerceCartService;
    }

    @Required
    public void setTkEuB2bCommerceCartService(TkEuB2bCommerceCartService tkEuB2bCommerceCartService) {
        this.tkEuB2bCommerceCartService = tkEuB2bCommerceCartService;
    }

    public Converter<AtpResponseData, CommerceCartParameter> getCommerceAtpCartParameterConverter() {
        return commerceAtpCartParameterConverter;
    }

    @Required
    public void setCommerceAtpCartParameterConverter(
            Converter<AtpResponseData, CommerceCartParameter> commerceAtpCartParameterConverter) {
        this.commerceAtpCartParameterConverter = commerceAtpCartParameterConverter;
    }

    public CartFactory getInMemoryCartFactory() {
        return inMemoryCartFactory;
    }

    public void setInMemoryCartFactory(CartFactory inMemoryCartFactory) {
        this.inMemoryCartFactory = inMemoryCartFactory;
    }

    public Converter<CartModel, CartData> getInMemoryCartConverter() {
        return inMemoryCartConverter;
    }

    public void setInMemoryCartConverter(Converter<CartModel, CartData> inMemoryCartConverter) {
        this.inMemoryCartConverter = inMemoryCartConverter;
    }

    @Override
    public boolean removeInactiveProductsFromCart() {
        try {
            final CartModel sessionCart = getCartService().getSessionCart();
            boolean isCartUpdated = false;
            if (CollectionUtils.isNotEmpty(sessionCart.getEntries())) {
                List<AbstractOrderEntryModel> entriesToBeRemoved = sessionCart.getEntries().stream()
                        .filter(entry -> !(ArticleApprovalStatus.APPROVED.getCode()
                                .equalsIgnoreCase(entry.getProduct().getApprovalStatus().getCode())))
                        .collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(entriesToBeRemoved)) {
                    isCartUpdated = true;
                    entriesToBeRemoved.forEach(entry -> {
                        LOG.info("Entries to be Removed are -> " + entry.getProduct().getCode() + " Entry No:"
                                + entry.getEntryNumber());
                        try {
                            updateCartEntry(entry.getEntryNumber(), 0);
                        } catch (CommerceCartModificationException e) {
                            e.printStackTrace();
                        }
                    });
                }
                getModelService().refresh(sessionCart);
                getCartService().setSessionCart(sessionCart);
            }
            return isCartUpdated;

        } catch (Exception e) {
            LOG.error("Exception occurred while removing InactiveProducts from Cart", e);
        }
        return false;

    }

    @Override
    public void setCustmerDeliveryDateToLineItem(int cartEntryNumber, String wishDeliveryDate) {
        final CartModel cartModel = getCartService().getSessionCart();
        final CartEntryModel cartEntry = getCartService().getEntryForNumber(cartModel, cartEntryNumber);
        final Optional<Date> date = TkEuFacadesUtils.parseDate(wishDeliveryDate);

        if (date.isPresent()) {
            cartEntry.setCustomerSpecifiedDeliveryDate(date.get());
            getCartService().saveOrder(cartModel);

        }

    }

    /**
     * To get list of all coupons from cart which are not redeemed in promotion
     * 
     */
    @Override
    public Collection<String> getNotRedeemedCouponsFromCart() {

        final CartModel sessionCart = getCartService().getSessionCart();
        if (CollectionUtils.isNotEmpty(sessionCart.getAppliedCouponCodes())) {
            if (CollectionUtils.isNotEmpty(sessionCart.getAllPromotionResults())) {
                return getNotRedeemedCouponsFromPromotionsInCart(sessionCart);
            } else {
                return sessionCart.getAppliedCouponCodes();
            }
        }

        return Collections.emptyList();

    }

    private Collection<String> getNotRedeemedCouponsFromPromotionsInCart(CartModel sessionCart) {
        final List<String> couponCodesRedeemedInPromotion = getUsedCouponCodefromPromotions(sessionCart);

        if (couponCodesRedeemedInPromotion.isEmpty()) {
            return sessionCart.getAppliedCouponCodes();
        } else {
            return sessionCart.getAppliedCouponCodes().stream()
                    .collect(Collectors.partitioningBy(item -> couponCodesRedeemedInPromotion.contains((String) item)))
                    .get(false);
        }

    }

    private List<String> getUsedCouponCodefromPromotions(CartModel sessionCart) {
        final List<String> couponCodesRedeemedInPromotion = new ArrayList<>();

        sessionCart.getAllPromotionResults().forEach(each -> {
            each.getActions().forEach(action -> {
                try {
                    AbstractRuleBasedPromotionActionModel ruleAction = (AbstractRuleBasedPromotionActionModel) action;
                    if (CollectionUtils.isNotEmpty(ruleAction.getUsedCouponCodes()))
                        couponCodesRedeemedInPromotion.addAll(ruleAction.getUsedCouponCodes());
                } catch (Exception e) {
                    LOG.error("Exception in getting Used Coupon code from Promotion for cart/order:"
                            + each.getOrderCode(), e);
                }
            });
        });
        return couponCodesRedeemedInPromotion;
    }

    /**
     * To check and remove any coupons from cart which are not redeemed in promotion
     * 
     */
    @Override
    public void removeNotRedeemedCouponFromCart() {
        try {
            final Collection<String> notRedeemedCouponCodes = getNotRedeemedCouponsFromCart();

            if (CollectionUtils.isNotEmpty(notRedeemedCouponCodes)) {
                notRedeemedCouponCodes.forEach(coupon -> {
                    try {
                        LOG.debug("Releasing coupon from cart: " + coupon);
                        voucherFacade.releaseVoucher(coupon);
                        LOG.debug("Released coupon from cart: " + coupon);
                    } catch (final Exception e) {
                        LOG.error("Exception while removing not redeemed coupon from cart " + coupon, e);
                    }
                });
                LOG.info("Removed following not redeemed coupons : " + String.join(",", notRedeemedCouponCodes));

            }

        } catch (final Exception e) {
            LOG.error("Exception while removing not redeemed coupon from cart", e);
        }

    }

}

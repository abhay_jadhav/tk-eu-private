package com.thyssenkrupp.b2b.eu.facades.process.email.context;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bRegistrationProcessModel;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuB2bRegistrationEmailContext extends AbstractEmailContext<TkEuB2bRegistrationProcessModel> {

    private String token;
    private String registrationConfirmationPageUrl;
    private Converter<UserModel, CustomerData> customerConverter;
    private CustomerData customerData;

    @Override
    public void init(final TkEuB2bRegistrationProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        customerData = getCustomerConverter().convert(getCustomer(businessProcessModel));
        setToken(businessProcessModel.getToken());
    }

    @Override
    protected BaseSiteModel getSite(TkEuB2bRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getSite();
    }

    @Override
    protected CustomerModel getCustomer(TkEuB2bRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getCustomer();
    }

    @Override
    protected LanguageModel getEmailLanguage(TkEuB2bRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getLanguage();
    }

    public String getDisplayRegisterUrl() throws UnsupportedEncodingException {
        boolean fetchSecureUrl = true;
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), fetchSecureUrl, getRegistrationConfirmationPageUrl(), "token=" + getURLEncodedToken());
    }

    public String getSiteBaseUrl() throws UnsupportedEncodingException {
        boolean fetchSecureUrl = true;
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), fetchSecureUrl, "");
    }

    protected Converter<UserModel, CustomerData> getCustomerConverter() {
        return customerConverter;
    }

    @Required
    public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter) {
        this.customerConverter = customerConverter;
    }

    public CustomerData getCustomer() {
        return customerData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getURLEncodedToken() throws UnsupportedEncodingException {
        return URLEncoder.encode(token, "UTF-8");
    }

    public String getRegistrationConfirmationPageUrl() {
        return registrationConfirmationPageUrl;
    }

    public void setRegistrationConfirmationPageUrl(String registrationConfirmationPageUrl) {
        this.registrationConfirmationPageUrl = registrationConfirmationPageUrl;
    }
}


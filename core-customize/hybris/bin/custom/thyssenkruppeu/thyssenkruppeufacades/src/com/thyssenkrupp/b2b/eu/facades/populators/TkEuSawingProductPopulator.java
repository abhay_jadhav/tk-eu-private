package com.thyssenkrupp.b2b.eu.facades.populators;

import java.util.List;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;

public class TkEuSawingProductPopulator<T extends AbstractOrderEntryProductInfoModel> implements Populator<T, List<ConfigurationInfoData>> {
    @Override
    public void populate(T source, List<ConfigurationInfoData> target) throws ConversionException {
        if (source.getConfiguratorType() == CUTTOLENGTH_PRODINFO) {
            if (!(source instanceof TkCutToLengthConfiguredProductInfoModel)) {
                throw new ConversionException("Instance with type " + source.getConfiguratorType() + " is of class" + source.getClass().getName() + " which is not convertible to " + TkCutToLengthConfiguredProductInfoModel.class.getName());
            }

            final ConfigurationInfoData item = new ConfigurationInfoData();
            final TkCutToLengthConfiguredProductInfoModel model = (TkCutToLengthConfiguredProductInfoModel) source;
            item.setUniqueId(model.getLabel());
            item.setConfigurationLabel(model.getLabel());
            item.setStatus(source.getProductInfoStatus());
            item.setConfiguratorType(model.getConfiguratorType());
            item.setConfigurationValue(String.valueOf(model.getChecked()));
            item.setLengthValue(String.valueOf(model.getValue()));
            item.setSapExportValue(model.getSapExportValue());
            target.add(item);
        }
    }
}

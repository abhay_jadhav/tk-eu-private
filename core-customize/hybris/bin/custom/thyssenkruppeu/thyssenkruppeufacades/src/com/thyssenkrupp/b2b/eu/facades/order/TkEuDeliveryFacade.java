package com.thyssenkrupp.b2b.eu.facades.order;

import com.thyssenkrupp.b2b.eu.facades.order.data.MyAccountConsignmentDataWrapper;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

public interface TkEuDeliveryFacade {

    SearchPageData<ConsignmentData> orderDeliveriesSearchList(String searchKey, PageableData pageableData);

    int orderDeliveriesSearchListCount(String searchKey);

    MyAccountConsignmentDataWrapper fetchDeliveryByCode(String deliveryNo);
}

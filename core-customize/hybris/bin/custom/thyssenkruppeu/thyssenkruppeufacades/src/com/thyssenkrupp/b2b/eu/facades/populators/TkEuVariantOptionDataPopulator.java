package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.VariantOptionDataPopulator;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.variants.model.VariantAttributeDescriptorModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TkEuVariantOptionDataPopulator extends VariantOptionDataPopulator {

    @Override
    public void populate(final VariantProductModel source, final VariantOptionData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        if (source.getBaseProduct() != null) {
            final List<VariantAttributeDescriptorModel> descriptorModels = getVariantsService().getVariantAttributesForVariantType(
              source.getBaseProduct().getVariantType());

            final Collection<VariantOptionQualifierData> variantOptionQualifiers = new ArrayList<VariantOptionQualifierData>();
            for (final VariantAttributeDescriptorModel descriptorModel : descriptorModels) {
                final VariantOptionQualifierData variantOptionQualifier = new VariantOptionQualifierData();
                final String qualifier = descriptorModel.getQualifier();
                variantOptionQualifier.setQualifier(qualifier);
                variantOptionQualifier.setName(descriptorModel.getName());
                final Object variantAttributeValue = lookupVariantAttributeName(source, qualifier);
                variantOptionQualifier.setValue(variantAttributeValue == null ? "" : variantAttributeValue.toString());

                variantOptionQualifiers.add(variantOptionQualifier);
            }
            target.setVariantOptionQualifiers(variantOptionQualifiers);
            target.setCode(source.getCode());
            target.setUrl(getProductModelUrlResolver().resolve(source));
            /* Removed stock conversion code */

            /*Removed Price Calculation*/
        }
    }
}

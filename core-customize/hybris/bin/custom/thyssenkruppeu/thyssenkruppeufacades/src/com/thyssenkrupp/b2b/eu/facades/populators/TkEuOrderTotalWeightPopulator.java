package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;

public class TkEuOrderTotalWeightPopulator<SOURCE extends AbstractOrderModel, TARGET extends AbstractOrderData> implements Populator<SOURCE, TARGET> {

    private TkEuPriceDataFactory     tkEuPriceDataFactory;
    private TkEuUomConversionService uomConversionService;
    private UnitService              unitService;

    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
        if (source != null) {
            BigDecimal orderTotalWeightInKg = source.getTotalWeight() != null ? BigDecimal.valueOf(source.getTotalWeight()) : getUomConversionService().calculateOrderTotalWeightInKg(source);
            UnitModel kgUnit = source.getWeightUnit() != null ? source.getWeightUnit() : unitService.getUnitForCode(SAP_KGM_UNIT_CODE);
            PriceData priceDataTotalWeightInKg = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, orderTotalWeightInKg.setScale(2, RoundingMode.HALF_UP), " " + kgUnit.getName());
            target.setTotalWeightInkg(priceDataTotalWeightInKg);
        }
    }

    public TkEuUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    @Required
    public void setUomConversionService(TkEuUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    @Required
    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.servicelayer.type.TypeService;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.global.baseshop.facades.product.data.GenderData;

/**
 * Populates {@link GenderData} with name and code.
 */
public class TkEuGenderDataPopulator implements Populator<Gender, GenderData> {
    private TypeService typeService;

    protected TypeService getTypeService() {
        return typeService;
    }

    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    @Override
    public void populate(final Gender source, final GenderData target) {
        target.setCode(source.getCode());
        target.setName(getTypeService().getEnumerationValue(source).getName());
    }
}

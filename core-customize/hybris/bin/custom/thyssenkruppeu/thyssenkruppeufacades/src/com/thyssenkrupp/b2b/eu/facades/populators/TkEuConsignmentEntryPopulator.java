package com.thyssenkrupp.b2b.eu.facades.populators;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.thyssenkrupp.b2b.eu.facades.product.data.UnitData;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceEntryModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuConsignmentEntryPopulator extends ConsignmentEntryPopulator {

    private Converter<UnitModel, UnitData> productUnitConverter;

    @Override
    public void populate(ConsignmentEntryModel consignmentEntryModel, ConsignmentEntryData consignmentEntryData)
            throws ConversionException {

        Assert.notNull(consignmentEntryModel, "Parameter source cannot be null.");
        Assert.notNull(consignmentEntryData, "Parameter target cannot be null.");

        consignmentEntryData.setBatchNumber(consignmentEntryModel.getBatchNumber());
        consignmentEntryData.setSupplierLotNumber(consignmentEntryModel.getSupplierLotNumber());
        consignmentEntryData.setBatchGroupNumber(consignmentEntryModel.getBatchGroupNumber());
        consignmentEntryData.setEntryNo(consignmentEntryModel.getEntryNumber());

        if (CollectionUtils.isNotEmpty(consignmentEntryModel.getInvoiceEntries())) {
            Optional<InvoiceEntryModel> invoiceEntryModel = consignmentEntryModel.getInvoiceEntries().stream()
                    .findFirst();
            if (invoiceEntryModel.isPresent()) {
                consignmentEntryData.setInvoice(invoiceEntryModel.get().getInvoice().getCode());
                consignmentEntryData.setIsLinkAvailable(true);
            }

        }
        consignmentEntryData.setQuantity(consignmentEntryModel.getQuantity());
        consignmentEntryData.setShippedQuantity(consignmentEntryModel.getShippedQuantity());

        try {
            UnitModel shippedQuantityUnit = consignmentEntryModel.getShippedQuantityUnit();
            if (shippedQuantityUnit != null) {
                consignmentEntryData.setShippedQuantityUnit(getProductUnitConverter().convert(shippedQuantityUnit));
            }
        } catch (Exception e) {
            consignmentEntryData.setShippedQuantityUnit(null);
        }
        consignmentEntryData.setConsignmentNumber(consignmentEntryModel.getConsignment().getCode());
        populateProductParameter(consignmentEntryModel, consignmentEntryData);
    }

    private void populateProductParameter(ConsignmentEntryModel consignmentEntryModel,
            ConsignmentEntryData consignmentEntryData) {

        populateProductNameAndCode(consignmentEntryModel, consignmentEntryData);

        populateCertificateStatus(consignmentEntryModel, consignmentEntryData);

    }

    private void populateCertificateStatus(ConsignmentEntryModel consignmentEntryModel,
            ConsignmentEntryData consignmentEntryData) {
        boolean certificateStatus = false;
        if (StringUtils.isEmpty(consignmentEntryModel.getCertificateOrdered())
                && Objects.nonNull(consignmentEntryModel.getOrderEntry())
                && (CollectionUtils.isNotEmpty(consignmentEntryModel.getOrderEntry().getProductInfos()))) {
            List<AbstractOrderEntryProductInfoModel> productInfoModelList = consignmentEntryModel.getOrderEntry()
                    .getProductInfos();
            for (AbstractOrderEntryProductInfoModel infoModel : productInfoModelList) {
                if (infoModel instanceof CertificateConfiguredProductInfoModel
                        && ((CertificateConfiguredProductInfoModel) infoModel).isChecked()) {

                    certificateStatus = true;
                    break;

                }
            }
        } else if (StringUtils.isNotEmpty(consignmentEntryModel.getCertificateOrdered())) {
            certificateStatus = true;
            consignmentEntryData.setCertificateOrdered(consignmentEntryData.getCertificateOrdered());
        }
        consignmentEntryData.setIsCertificateExist(certificateStatus);
    }

    private void populateProductNameAndCode(ConsignmentEntryModel consignmentEntryModel,
            ConsignmentEntryData consignmentEntryData) {
        if (StringUtils.isNotEmpty(consignmentEntryModel.getProductName()))
            consignmentEntryData.setProductName(consignmentEntryModel.getProductName());
        else if (Objects.nonNull(consignmentEntryModel.getOrderEntry())
                && Objects.nonNull(consignmentEntryModel.getOrderEntry().getProduct()))
            consignmentEntryData.setProductName(consignmentEntryModel.getOrderEntry().getProduct().getName());

        if (StringUtils.isNotEmpty(consignmentEntryModel.getProductCode()))
            consignmentEntryData.setProductCode(consignmentEntryModel.getProductCode());
        else if (Objects.nonNull(consignmentEntryModel.getOrderEntry())
                && Objects.nonNull(consignmentEntryModel.getOrderEntry().getProduct()))
            consignmentEntryData.setProductCode(consignmentEntryModel.getOrderEntry().getProduct().getCode());
    }

    public Converter<UnitModel, UnitData> getProductUnitConverter() {
        return productUnitConverter;
    }

    @Required
    public void setProductUnitConverter(Converter<UnitModel, UnitData> productUnitConverter) {
        this.productUnitConverter = productUnitConverter;
    }
}

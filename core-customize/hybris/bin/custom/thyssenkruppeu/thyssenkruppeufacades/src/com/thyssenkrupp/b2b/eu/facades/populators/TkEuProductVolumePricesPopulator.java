package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils;

import de.hybris.platform.acceleratorfacades.product.converters.populator.ProductVolumePricesPopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUomWithPriceFactor;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;
import static de.hybris.platform.europe1.jalo.PriceRow.UNIT_FACTOR;;

public class TkEuProductVolumePricesPopulator extends ProductVolumePricesPopulator<ProductModel, ProductData> {

    private TkEuPriceDataFactory tkEuPriceDataFactory;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
        if (productData != null) {
            final List<PriceInformation> pricesInfos = getPriceService().getPriceInformationsForProduct(productModel);
            if ( CollectionUtils.isEmpty(pricesInfos) || (!pricesInfos.isEmpty() && pricesInfos.size() < 2 && (long)pricesInfos.get(0).getQualifiers().get(PriceRow.MINQTD)==0)){
                productData.setVolumePrices(Collections.<PriceData>emptyList());
            } else {
                final List<PriceData> volPrices = createPrices(productModel, pricesInfos);
                // Sort the list into quantity order
                Collections.sort(volPrices, VolumePriceComparator.INSTANCE);

                // Set the max quantities
                for (int i = 0; i < volPrices.size() - 1; i++) {
                    volPrices.get(i).setMaxQuantity(Long.valueOf(volPrices.get(i + 1).getMinQuantity().longValue() - 1));
                }
                volPrices.removeIf(e -> e.getValue().doubleValue()== 0.0d); //NOSONAR
                productData.setVolumePrices(volPrices);
                final UnitModel unitModel = (UnitModel) pricesInfos.get(0).getQualifiers().get("unit");
                productData.setVolumePriceRangeUnit(TkEuFacadesUtils.formatMtkName(unitModel.getName()));
            }
        }
    }

    @Override
    protected List<PriceData> createPrices(final ProductModel productModel, final List<PriceInformation> pricesInfos) {
        final List<PriceData> volPrices = new ArrayList<PriceData>();

        final PriceDataType priceType = getPriceType(productModel);

        for (final PriceInformation priceInfo : pricesInfos) {
            final Long minQuantity = getMinQuantity(priceInfo);
            if (minQuantity != null) {
                final PriceData volPrice = createPriceData(productModel, priceType, priceInfo);
                if (volPrice != null) {
                    volPrice.setMinQuantity(minQuantity);
                    volPrices.add(volPrice);
                }
            }
        }
        return volPrices;
    }

    protected PriceData createPriceData(ProductModel productModel, PriceDataType priceType, PriceInformation info) {
        if(info.getQualifiers().containsKey(UNIT_FACTOR)) {
            return getTkEuPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()), info.getPriceValue().getCurrencyIso(), formatUomWithPriceFactor((Long)info.getQualifiers().get(UNIT_FACTOR), productModel.getBaseUnit().getName()));
        } else {
            return getTkEuPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()), info.getPriceValue().getCurrencyIso(), formatUom(productModel.getBaseUnit().getName()));
        }
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

}

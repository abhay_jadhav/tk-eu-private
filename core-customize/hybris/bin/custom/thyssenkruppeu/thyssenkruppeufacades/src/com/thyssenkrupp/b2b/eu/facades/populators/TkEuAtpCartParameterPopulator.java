package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuAtpCartParameterPopulator implements Populator<AtpResponseData, CommerceCartParameter> {

    @Override
    public void populate(AtpResponseData atpResponseData, CommerceCartParameter commerceCartParameter) throws ConversionException {
        commerceCartParameter.setEntryNumber(atpResponseData.getPosition());
        commerceCartParameter.setHybrisAtpResult(atpResponseData.getHybrisAtpResult());
        commerceCartParameter.setSapAtpResult(atpResponseData.getSapAtpResult());
    }
}

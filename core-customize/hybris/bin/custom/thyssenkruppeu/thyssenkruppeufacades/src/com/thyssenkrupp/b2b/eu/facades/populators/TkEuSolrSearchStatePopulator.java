package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.converters.populator.SolrSearchStatePopulator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TkEuSolrSearchStatePopulator extends SolrSearchStatePopulator {

    private static final Logger LOG     = Logger.getLogger(TkEuSolrSearchStatePopulator.class);
    private static final String PARAM_Q = "?q=";

    @Override
    protected String buildUrlQueryString(final SolrSearchQueryData source, final SearchStateData target) {

        String searchQueryParam = target.getQuery().getValue();
        if (StringUtils.isNotBlank(searchQueryParam)) {
            try {
                searchQueryParam = StringEscapeUtils.unescapeHtml(searchQueryParam);
                return PARAM_Q + URLEncoder.encode(searchQueryParam, "UTF-8");
            } catch (final UnsupportedEncodingException e) {
                LOG.error("Unsupported encoding (UTF-8). Fallback to html escaping.", e);
                return PARAM_Q + StringEscapeUtils.escapeHtml(searchQueryParam);
            }
        }
        return "";
    }
}

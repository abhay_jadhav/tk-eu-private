package com.thyssenkrupp.b2b.eu.facades.process.email.context;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bAmbiguousRegistrationProcessModel;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

public class TkEuB2bAmbigiousRegistrationEmailContext extends AbstractEmailContext<TkEuB2bAmbiguousRegistrationProcessModel> {

    public static final String EMAIL = "email";
    public static final String DISPLAY_NAME = "displayName";
    public static final String CUSTOMERCARE_EMAIL_KEY = "tk.eu.customerservice.email";
    public static final String CUSTOMERCARE_EMAIL_DISPLAY_NAME = "tk.eu.customerservice.email.name";
    private List<CustomerModel> customerModelList;
    @Override
    public void init(final TkEuB2bAmbiguousRegistrationProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        setCustomerModelList(businessProcessModel.getAmbiguousCustomers());
        super.init(businessProcessModel, emailPageModel);
        final String customerServiceEmail = getConfigurationService().getConfiguration().getString(CUSTOMERCARE_EMAIL_KEY);
        final String customerServiceEmailDisplayName = getConfigurationService().getConfiguration().getString(CUSTOMERCARE_EMAIL_DISPLAY_NAME);
        if (!customerServiceEmail.isEmpty()) {
            put(EMAIL, customerServiceEmail);
            put(DISPLAY_NAME, customerServiceEmailDisplayName);
        }
    }
    public String getSiteBaseUrl() throws UnsupportedEncodingException {
        boolean fetchSecureUrl = true;
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), fetchSecureUrl, "");
    }
    @Override
    protected BaseSiteModel getSite(TkEuB2bAmbiguousRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getSite();
    }

    @Override
    protected CustomerModel getCustomer(TkEuB2bAmbiguousRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getCustomer();
    }

    @Override
    protected LanguageModel getEmailLanguage(TkEuB2bAmbiguousRegistrationProcessModel businessProcessModel) {
        return businessProcessModel.getLanguage();
    }

    public List<CustomerModel> getCustomerModelList() {
        return customerModelList;
    }

    public void setCustomerModelList(List<CustomerModel> customerModelList) {
        this.customerModelList = customerModelList;
    }
}

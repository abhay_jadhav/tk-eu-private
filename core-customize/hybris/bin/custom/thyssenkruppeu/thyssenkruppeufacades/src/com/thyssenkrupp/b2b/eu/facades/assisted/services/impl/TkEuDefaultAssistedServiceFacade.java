package com.thyssenkrupp.b2b.eu.facades.assisted.services.impl;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;
import com.thyssenkrupp.b2b.eu.core.service.TkEuLoginDetailsService;

import de.hybris.platform.assistedservicefacades.impl.DefaultAssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;

public class TkEuDefaultAssistedServiceFacade extends DefaultAssistedServiceFacade{
    private static final Logger LOG = Logger.getLogger(TkEuDefaultAssistedServiceFacade.class);
    private TkEuLoginDetailsService tkEuLoginDetailsService;

    @Override
    public void emulateCustomer(final String customerId, final String cartId) throws AssistedServiceException{
        emulateCustomer(customerId, cartId, null);
        
        if (getAsmSession() != null) {
            tkEuLoginDetailsService.setLoginDetails(customerId ,LoginType.ASM);
        }

    }

    /**
     * @return the tkEuLoginDetailsService
     */
    public TkEuLoginDetailsService getTkEuLoginDetailsService() {
        return tkEuLoginDetailsService;
    }

    /**
     * @param tkEuLoginDetailsService the tkEuLoginDetailsService to set
     */
    public void setTkEuLoginDetailsService(TkEuLoginDetailsService tkEuLoginDetailsService) {
        this.tkEuLoginDetailsService = tkEuLoginDetailsService;
    }

   
}

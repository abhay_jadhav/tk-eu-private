package com.thyssenkrupp.b2b.eu.facades.order.impl;

import com.thyssenkrupp.b2b.eu.core.pojo.CertificateEntry;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAP;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPSingleResponse;
import com.thyssenkrupp.b2b.eu.core.service.TkEuDocumentDownloadFactory;
import com.thyssenkrupp.b2b.eu.core.service.TkEuDocumentDownloadService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuDocumentDownloadFacade;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DefaultTkEuDocumentDownloadFacade implements TkEuDocumentDownloadFacade {

    private static final String ONE    = "1";
    private static final String ZEROES = "000000";

    private TkEuDocumentDownloadFactory tkEuDocumentDownloadFactory;

    public TkEuDocumentDownloadFactory getTkEuDocumentDownloadFactory() {
        return tkEuDocumentDownloadFactory;
    }

    public void setTkEuDocumentDownloadFactory(TkEuDocumentDownloadFactory tkEuDocumentDownloadFactory) {
        this.tkEuDocumentDownloadFactory = tkEuDocumentDownloadFactory;
    }

    @Override
    public boolean checkIfAuthorizedUser(DocumentDownloadRequest documents) throws Exception {
        // Call service and validate if user is authorized for a particular document
        TkEuDocumentDownloadService downloadService = tkEuDocumentDownloadFactory.getDocumentDownloadService();
        return downloadService.isAuthorizedUser(documents);
    }

    @Override
    public GetDocumentStatusResponse getDocumentStatus(DocumentDownloadRequest documents) throws Exception {
        GetDocumentStatusResponse docStatusResponse = new GetDocumentStatusResponse();
        List<CertificateEntry> certificates = new ArrayList<CertificateEntry>();
        TkEuDocumentDownloadService downloadService = tkEuDocumentDownloadFactory.getDocumentDownloadService();
        docStatusResponse.setDownloadServiceStatus(downloadService.getType().toString());
        if (documents != null
            && CollectionUtils.isNotEmpty(documents.getDocumentRequests())) {
            Object sapResponse = downloadService.getDocumentStatus(documents);
            if (sapResponse instanceof GetDocumentStatusSAPResponse) {
                GetDocumentStatusSAPResponse getDocumentStatusSAPResponse = (GetDocumentStatusSAPResponse) sapResponse;
                if (CollectionUtils.isNotEmpty(getDocumentStatusSAPResponse.getGetDocumentStatusSAP())) {
                    for (GetDocumentStatusSAP getDocumentStatusSAP : getDocumentStatusSAPResponse.getGetDocumentStatusSAP()) {
                        if (checkIfCertificate(getDocumentStatusSAP)) {
                            certificates.add(getCertificate(getDocumentStatusSAP));
                        } else {
                            docStatusResponse.setDocumentStatus(isDocumentExist(getDocumentStatusSAP.getDocumentExistsFlag()));
                            docStatusResponse.setDocumentNo(getDocumentStatusSAP.getDocumentNo());
                        }
                    }
                }
            } else if (sapResponse instanceof GetDocumentStatusSAPSingleResponse) {
                GetDocumentStatusSAPSingleResponse getDocumentStatusSAPSingleResponse = (GetDocumentStatusSAPSingleResponse) sapResponse;
                GetDocumentStatusSAP getDocumentStatusSAP = null;
                getDocumentStatusSAP = getDocumentStatusSAPSingleResponse.getGetDocumentStatusSAP();
                docStatusResponse.setDocumentNo(getDocumentStatusSAP != null ? getDocumentStatusSAP.getDocumentNo() : StringUtils.EMPTY);
                docStatusResponse.setDocumentStatus(getDocumentStatusSAP != null ? isDocumentExist(getDocumentStatusSAP.getDocumentExistsFlag()) : false);
                addSAPSingleResponseCert(getDocumentStatusSAP, certificates);
            }
        }

        docStatusResponse.setCertificates(certificates);
        return docStatusResponse;
    }

    private void addSAPSingleResponseCert(GetDocumentStatusSAP getDocumentStatusSAP, List<CertificateEntry> certificates) {
        if (getDocumentStatusSAP != null && checkIfCertificate(getDocumentStatusSAP)) {
            certificates.add(getCertificate(getDocumentStatusSAP));
        }
    }

    private CertificateEntry getCertificate(GetDocumentStatusSAP getDocumentStatusSAP) {
        CertificateEntry certificate = new CertificateEntry();
        certificate.setLineItemNo(getDocumentStatusSAP.getLineItemNumber());
        certificate.setStatus(isDocumentExist(getDocumentStatusSAP.getDocumentExistsFlag()));
        return certificate;
    }

    @Override
    public GetDocumentContentResponse getDocument(DocumentDownloadRequest documents) throws Exception {
        GetDocumentContentResponse getDocumentContentResponse = null;
        TkEuDocumentDownloadService downloadService = tkEuDocumentDownloadFactory.getDocumentDownloadService();
        if (documents != null
            && CollectionUtils.isNotEmpty(documents.getDocumentRequests())) {
            getDocumentContentResponse = downloadService.getDocument(documents);
        }

        return getDocumentContentResponse;
    }

    private boolean checkIfCertificate(GetDocumentStatusSAP getDocumentStatusSAP) {
        return !ZEROES.equals(getDocumentStatusSAP.getLineItemNumber());
    }

    private boolean isDocumentExist(String documentExistsFlag) {
        return ONE.equals(documentExistsFlag);
    }
}

package com.thyssenkrupp.b2b.eu.facades.atp;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.facades.order.data.AtpViewData;
import com.thyssenkrupp.b2b.eu.facades.product.ProductNameURLData;

import java.util.HashMap;
import java.util.List;

public interface TkEuAtpFacade {
    List<AtpResponseData> doAtpCheck(AtpRequestData request);

    AtpViewData doAtpCheckAndBuildViewResponse(AtpRequestData request);

    AtpViewData removeOutOfStockProductsAndGetConsolidateResult();

    HashMap<String, ProductNameURLData> removeOutOfStockItems();
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import static com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants.ORDER_ENTRY_PARAM;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatMtkName;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.CERTIFICATE_COSTS;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkEuCustomerAccountDeliveryService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.DiscountValue;

public class TkEuOrderEntryPopulator extends OrderEntryPopulator {
    private static final Logger LOG = Logger.getLogger(TkEuOrderEntryPopulator.class);
    private static final String PROMOTION_DISCOUNT_CODE_PREFIX = "Action";
    private TkEuPriceDataFactory       tkEuPriceDataFactory;
    private TkEuCheckoutFacade         b2bCheckoutFacade;
    private TkEuRoundingService              roundingService;
    private DefaultTkEuCustomerAccountDeliveryService             tkEuCustomerAccountDeliveryService;



    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        super.populate(source, target);
        addMisc(source, target);
        populateCertificateDiscounts(source, target);
        getB2bCheckoutFacade().setTradeLengthAttributes(target);
        addUnit(source, target);
        appendOrderEntryDetailsToProductDataUrl(source,target);
        populateConsignmentInfo(source, target);
        getB2bCheckoutFacade().setSawingAttributes(target);
        addDiscountdetails(source,target);
    }

    protected void addMisc(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        if (orderEntry.getEntryNumber() != null) {
            entry.setPositionNumber(Integer.valueOf((orderEntry.getEntryNumber().intValue() + 1)));
        }
        
        
        entry.setIsCancelledEntry(orderEntry.getQuantityStatus()!=null && OrderEntryStatus.DEAD.getCode().equals(orderEntry.getQuantityStatus().getCode()));
       
 
    }

    protected void populateConsignmentInfo(AbstractOrderEntryModel source, OrderEntryData target) {
        List<ConsignmentEntryModel> consignmentEntries = getConsignmentEntries(source); 
        if (consignmentEntries == null) {
            return;
        }
        List<ConsignmentData> consignmentsData =
            consignmentEntries.stream()
                .map(consignmentEntry -> consignmentEntry.getConsignment())
                .filter(distinctByKey(consignment -> consignment.getCode()))
                .map(consignment -> convertToConsignmentData(consignment)) //cannot use consignmentConverter as it leads to a recursive call
                .sorted(Comparator.comparing(ConsignmentData::getCode).reversed())
                .collect(Collectors.toList());

        target.setConsignments(consignmentsData);
    }

    private List<ConsignmentEntryModel> getConsignmentEntries(AbstractOrderEntryModel source) {
        List<ConsignmentEntryModel> consignmentEntriesList = new ArrayList<>();
        if (source.getOrder() instanceof OrderModel && StringUtils.isNotEmpty(((OrderModel) source.getOrder()).getSapOrderId())) {
            consignmentEntriesList.addAll(tkEuCustomerAccountDeliveryService.fetchDeliveriesForOrderEntry(
                    ((OrderModel) source.getOrder()).getSapOrderId(), source.getEntryNumber().intValue()));
        }
        if (CollectionUtils.isNotEmpty(source.getConsignmentEntries())) {
            consignmentEntriesList.addAll(source.getConsignmentEntries());
        }

        return consignmentEntriesList;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private ConsignmentData convertToConsignmentData(ConsignmentModel consignmentModel) {
        ConsignmentData consignmentData = new ConsignmentData();
        consignmentData.setCode(consignmentModel.getCode());
        consignmentData.setShippingDate(consignmentModel.getShippingDate());
        return consignmentData;
    }

    private void appendOrderEntryDetailsToProductDataUrl(final AbstractOrderEntryModel source, final OrderEntryData target) {
        try {
            if (target.getProduct() != null && isNotEmpty(target.getProduct().getUrl()) && source.getPk() != null) {
                URIBuilder uriBuilder = new URIBuilder(target.getProduct().getUrl());
                uriBuilder.addParameter(ORDER_ENTRY_PARAM, source.getPk().toString());
                target.getProduct().setUrl(uriBuilder.toString());
            }
        } catch (URISyntaxException e) {
            LOG.error("Exception in building product url for orderEntry: " + source.getPk(), e);
        }
    }

    protected void populateCertificateDiscounts(final AbstractOrderEntryModel source, final OrderEntryData target) {
        List<DiscountValue> discountValues = emptyIfNull(source.getDiscountValues());
        double certificatePrice = discountValues.stream().filter(discountValue -> discountValue.getCode().equals(CERTIFICATE_COSTS)).mapToDouble(discountValue -> discountValue.getValue()).sum();
        if(source.getBasePrice() != null) {
            target.setCertificatePrice(createPrice(source, Math.abs(certificatePrice)));
        }
    }

    protected void addUnit(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        if (orderEntry.getUnit() != null) {
            entry.setUnitName(formatMtkName(orderEntry.getUnit().getName()));
        }
    }

    @Override
    protected void addTotals(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        if (orderEntry.getBasePrice() != null) {
            entry.setBasePrice(createPriceWithUnit(orderEntry, orderEntry.getBasePrice()));
        }
        if (orderEntry.getTotalPrice() != null) {
            entry.setTotalPrice(createPrice(orderEntry, orderEntry.getTotalPrice()));
        }
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    @Required
    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

    protected PriceData createPriceWithUnit(final AbstractOrderEntryModel orderEntry, final Double val) {
        return getTkEuPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val), orderEntry.getOrder().getCurrency().getIsocode(), formatUom(orderEntry.getUnit().getName()));
    }

    public TkEuCheckoutFacade getB2bCheckoutFacade() {
        return b2bCheckoutFacade;
    }

    public void setB2bCheckoutFacade(TkEuCheckoutFacade b2bCheckoutFacade) {
        this.b2bCheckoutFacade = b2bCheckoutFacade;
    }

    protected void addDiscountdetails(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        final double entryDiscounts=calculateProductDiscounts(orderEntry);
        if(orderEntry.getTotalPrice()!=null) {
        final double totalPriceWithoutDiscount = orderEntry.getTotalPrice().doubleValue()+entryDiscounts;
           if (!(BigDecimal.valueOf(totalPriceWithoutDiscount).equals(entry.getTotalPrice().getValue()))) {
            entry.setEntryTotalPriceWithoutDiscount(createPrice(orderEntry, totalPriceWithoutDiscount));
            entry.setEntryTotalDiscount(
                    (createPrice(orderEntry,entryDiscounts)));
        }
    }
    }
    private double calculateProductDiscounts(AbstractOrderEntryModel orderEntry) {
        double discounts = 0.0d;

            final List<DiscountValue> discountValues = orderEntry.getDiscountValues();
        if (discountValues != null) {
            for (final DiscountValue dValue : discountValues) {
                if(dValue.getAppliedValue()> 0
                                && dValue.getCode().startsWith(PROMOTION_DISCOUNT_CODE_PREFIX)) {
                        discounts += dValue.getAppliedValue();
                        }
                    }
                }


        return discounts;
    }

    public TkEuRoundingService getRoundingService() {
        return roundingService;
    }

    public void setRoundingService(TkEuRoundingService roundingService) {
        this.roundingService = roundingService;
    }

    public DefaultTkEuCustomerAccountDeliveryService getTkEuCustomerAccountDeliveryService() {
        return tkEuCustomerAccountDeliveryService;
    }

    public void setTkEuCustomerAccountDeliveryService(DefaultTkEuCustomerAccountDeliveryService tkEuCustomerAccountDeliveryService) {
        this.tkEuCustomerAccountDeliveryService = tkEuCustomerAccountDeliveryService;
    }
}

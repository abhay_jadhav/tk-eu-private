package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

public class TkEuCutToLengthCategoryPopulator implements Populator<CategoryModel, CategoryData> {

    private TkB2bCategoryService categoryService;

    @Override
    public void populate(CategoryModel source, CategoryData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        if (getCategoryService().isTradeLengthVariantCategory(source)) {
            target.setIsTradeLengthCategory(true);
        }
    }

    public TkB2bCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(TkB2bCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

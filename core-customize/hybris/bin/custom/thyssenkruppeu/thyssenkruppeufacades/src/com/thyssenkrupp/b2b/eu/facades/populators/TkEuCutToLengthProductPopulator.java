package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Optional;

public class TkEuCutToLengthProductPopulator<S extends ProductModel, T extends ProductData> extends AbstractProductPopulator<S, T> {

    private TkEuProductFacade productFacade;

    @Override
    public void populate(S source, T target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        if (getProductFacade().getBaseProduct(source).flatMap(this::getFirstCutToLengthConfigurationCategory).isPresent()) {
            target.setBaseProductHasCutToLengthCategory(true);
        }
    }

    private Optional<CategoryModel> getFirstCutToLengthConfigurationCategory(ProductModel product) {
        return getProductFacade().getFirstCutToLengthConfigurationCategory(product);
    }

    private TkEuProductFacade getProductFacade() {
        return productFacade;
    }

    @Required
    public void setProductFacade(TkEuProductFacade productFacade) {
        this.productFacade = productFacade;
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuB2bUnitPopulator implements Populator<B2BUnitModel, B2BUnitData> {
    @Override
    public void populate(B2BUnitModel b2BUnitModel, B2BUnitData b2BUnitData) throws ConversionException {
        populateAdditionalAttributes(b2BUnitModel, b2BUnitData);
    }

    private void populateAdditionalAttributes(B2BUnitModel b2bUnitModel, B2BUnitData b2bUnitData) {
        b2bUnitData.setPaymentTerms(b2bUnitModel.getPaymentterms());
        b2bUnitData.setShippingCondition(b2bUnitModel.getShippingcondition());
        b2bUnitData.setIncoTerms(b2bUnitModel.getIncoterms());
    }
}

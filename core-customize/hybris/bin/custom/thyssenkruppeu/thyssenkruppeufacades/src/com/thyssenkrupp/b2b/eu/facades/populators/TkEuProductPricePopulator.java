package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;

public class TkEuProductPricePopulator extends ProductPricePopulator {

    private TkEuPriceDataFactory tkEuPriceDataFactory;
    private UserService          userService;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
        final PriceDataType priceType;
        final PriceInformation info;
        UserModel currentUser = getUserService().getCurrentUser();
        boolean isAnonymousUser = getUserService().isAnonymousUser(currentUser);
        if (isAnonymousUser) {
            productData.setPurchasable(Boolean.FALSE);
            return;
        }
        if (CollectionUtils.isEmpty(productModel.getVariants())) {
            priceType = PriceDataType.BUY;
            info = getCommercePriceService().getWebPriceForProduct(productModel);
        } else {
            priceType = PriceDataType.FROM;
            info = getCommercePriceService().getFromPriceForProduct(productModel);
        }

        if (info != null) {
            createPriceData(productModel, productData, priceType, info);
        } else {
            productData.setPurchasable(Boolean.FALSE);
        }
    }

    private void createPriceData(ProductModel productModel, ProductData productData, PriceDataType priceType, PriceInformation info) {
        final PriceData priceData = getTkEuPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()), info.getPriceValue().getCurrencyIso(), formatUom(productModel.getBaseUnit().getName()));
        productData.setPrice(priceData);
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    @Required
    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }
}

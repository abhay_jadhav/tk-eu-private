package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.converters.Populator;

public class TkEuFeaturePopulator implements Populator<Feature, FeatureData> {
    @Override
    public void populate(final Feature source, final FeatureData target) {
        final ClassAttributeAssignmentModel classAttributeAssignment = source.getClassAttributeAssignment();
        target.setVisibility(classAttributeAssignment.getVisibility());
        target.setPosition(classAttributeAssignment.getPosition());
    }
}

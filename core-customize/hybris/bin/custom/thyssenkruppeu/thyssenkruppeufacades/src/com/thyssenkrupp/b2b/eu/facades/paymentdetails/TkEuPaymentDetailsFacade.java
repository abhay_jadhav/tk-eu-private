package com.thyssenkrupp.b2b.eu.facades.paymentdetails;

import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

public interface TkEuPaymentDetailsFacade {
    List<AddressData> getB2bUnitBillingAddress();
}

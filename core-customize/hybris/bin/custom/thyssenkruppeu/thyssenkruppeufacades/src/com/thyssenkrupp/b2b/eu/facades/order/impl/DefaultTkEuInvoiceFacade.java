package com.thyssenkrupp.b2b.eu.facades.order.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountInvoiceService;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkEuB2BUnitService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuInvoiceFacade;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceData;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceEntryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

public class DefaultTkEuInvoiceFacade implements TkEuInvoiceFacade {

    private TkEuCustomerAccountInvoiceService invoiceService;
    private DefaultB2BCommerceUnitService defaultB2BCommerceUnitService;
    private Converter<InvoiceModel, TkEuInvoiceData> invoiceConverter;
    private UserService userService;
    private DefaultTkEuB2BUnitService b2bUnitService;

    @Override
    public SearchPageData<TkEuInvoiceData> invoiceSearchList(String searchKey, PageableData pageableData) {

        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        final SearchPageData<InvoiceModel> invoiceResults = getInvoiceService().orderInvoiceSearchList(searchKey, pageableData, b2bUnits);
        return convertPageData(invoiceResults, getInvoiceConverter());
    }

    @Override
    public int invoiceSearchListCount(String searchKey) {
        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        return getInvoiceService().orderInvoicesSearchListCount(searchKey,b2bUnits);
    }

    @Override
    public TkEuInvoiceData fetchInvoiceByCode(String invoiceNo) {
        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        List<InvoiceModel> invoiceModels = getInvoiceService().fetchInvoices(invoiceNo, b2bUnits);
        List<TkEuInvoiceData> invoices = getInvoiceConverter().convertAll(invoiceModels);
        if (CollectionUtils.isNotEmpty(invoices)) {
            invoices.get(0).setIsInvoiceDataValid(checkInvoiceData(invoices.get(0)));
            return invoices.get(0);
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    private boolean checkInvoiceData(TkEuInvoiceData tkEuInvoiceData) {
        BigDecimal total= new BigDecimal(0.0d);
        for(TkEuInvoiceEntryData entryData:tkEuInvoiceData.getInvoiceEntries()) {
            if(StringUtils.isBlank(entryData.getProductCode()) || StringUtils.isBlank(entryData.getProductName()) || StringUtils.isBlank(entryData.getQuantity())
                || Objects.isNull(entryData.getTotalNet()) || (!Objects.isNull(entryData) && entryData.getTotalNet().getValue().compareTo(BigDecimal.ZERO)==0)){
                return false;
            }
            total=total.add(entryData.getTotalNet().getValue());
        }
        if(tkEuInvoiceData.getTotalNet().getValue().compareTo(total)==0) {
            return true;
        }
        return false;
    }

    protected SearchPageData<TkEuInvoiceData> convertPageData(final SearchPageData<InvoiceModel> source, final Converter<InvoiceModel, TkEuInvoiceData> converter) {
        final SearchPageData<TkEuInvoiceData> result = new SearchPageData();
        result.setPagination(source.getPagination());
        result.setSorts(source.getSorts());
        result.setResults(Converters.convertAll(source.getResults(), converter));
        return result;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public Converter<InvoiceModel, TkEuInvoiceData> getInvoiceConverter() {
        return invoiceConverter;
    }

    @Required
    public void setInvoiceConverter(Converter<InvoiceModel, TkEuInvoiceData> invoiceConverter) {
        this.invoiceConverter = invoiceConverter;
    }

    public TkEuCustomerAccountInvoiceService getInvoiceService() {
        return invoiceService;
    }

    @Required
    public void setInvoiceService(TkEuCustomerAccountInvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}

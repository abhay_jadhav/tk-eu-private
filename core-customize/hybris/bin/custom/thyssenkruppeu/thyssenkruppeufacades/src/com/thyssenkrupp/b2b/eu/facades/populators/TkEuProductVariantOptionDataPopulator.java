package com.thyssenkrupp.b2b.eu.facades.populators;

import java.util.Collection;

import com.thyssenkrupp.b2b.eu.facades.utils.ProductModelUtils;

import de.hybris.platform.commercefacades.product.converters.populator.ProductVariantOptionDataPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

public class TkEuProductVariantOptionDataPopulator extends ProductVariantOptionDataPopulator {

    private ProductModelUtils productModelUtils;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {

        final Collection<VariantProductModel> variants = getProductModelUtils().getVariants(productModel);
        for (final VariantProductModel variant : variants) {
            populateNodes(productData.getVariantMatrix(), variant, productModel);
            populateNodes(productData.getUnselectedVariantMatrixList(), variant, productModel);
        }
        if (!(productModel instanceof ERPVariantProductModel)) {
            copyDataFromNodes(productData);
        }

    }

    public ProductModelUtils getProductModelUtils() {
        return productModelUtils;
    }

    public void setProductModelUtils(final ProductModelUtils productModelUtils) {
        this.productModelUtils = productModelUtils;
    }

}

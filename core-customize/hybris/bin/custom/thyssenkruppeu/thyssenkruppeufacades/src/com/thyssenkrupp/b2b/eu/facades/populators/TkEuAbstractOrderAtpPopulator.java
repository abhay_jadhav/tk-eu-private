package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuAbstractOrderAtpPopulator implements Populator<AbstractOrderModel, AbstractOrderData> {
    
    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        target.setOverallAtpDate(source.getOverallAtpDate());
    }

}

package com.thyssenkrupp.b2b.eu.facades.product.impl;

import java.util.LinkedHashMap;
import java.util.List;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductComparisonFacade;
import com.thyssenkrupp.b2b.eu.core.service.ProductComparisonService;

public class DefaultTkEuProductComparisonFacadeImpl implements TkEuProductComparisonFacade {

    private ProductComparisonService productComparisonService;

    @Override
    public LinkedHashMap<String, String> getComparisonData(final List<String> comparisonProductList, final String[] attributeList) {

        return productComparisonService.getComparisonData(comparisonProductList, attributeList);
    }

    public void setProductComparisonService(ProductComparisonService productComparisonService) {
        this.productComparisonService = productComparisonService;
    }
}

package com.thyssenkrupp.b2b.eu.facades.utils;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;

public final class ProductModelUtils {
    private ProductModelUtils() {
        super();
    }

    public Collection<VariantProductModel> getVariants(final ProductModel productModel) {
        Collection<VariantProductModel> variants = productModel.getVariants();
        if (productModel instanceof ERPVariantProductModel) {
            variants = ((ERPVariantProductModel) productModel).getBaseProduct().getVariants();
        }
        return CollectionUtils.isNotEmpty(variants) ? variants : Collections.<VariantProductModel>emptyList();
    }

}

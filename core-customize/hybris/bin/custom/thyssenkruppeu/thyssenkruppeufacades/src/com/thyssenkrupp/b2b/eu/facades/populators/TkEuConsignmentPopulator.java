package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class TkEuConsignmentPopulator implements Populator<ConsignmentModel, ConsignmentData> {

    @Override
    public void populate(ConsignmentModel consignmentModel, ConsignmentData consignmentData) throws ConversionException {
        consignmentData.setBatchId(consignmentModel.getBatchId());
        consignmentData.setDeliveryDate(consignmentModel.getTkNamedDeliveryDate());
        consignmentData.setShippingDate(consignmentModel.getShippingDate());
    }
}

package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch;

import com.thyssenkrupp.b2b.eu.facades.search.data.TkEuCategorySuggestionData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;

import java.util.List;

public interface TkEuProductSearchFacade<ITEM extends ProductData> extends ProductSearchFacade {

    List<TkEuCategorySuggestionData> getCategorySuggestions(String input);

    ProductSearchPageData<SearchStateData, ITEM> searchByProductCodes(List<String> productCodes);
}

/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.facades.account;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.AddressModel;

public interface TkEuB2bMyAccountFacade {

    void saveCustomerShippingAddress(AddressModel addressModel);

    void savePrimaryShippingAddress(AddressModel addressModel);

    void removePrimaryShippingAddress();

    void setCustomerAddressAsPrimaryAddress(AddressData addressData);

    void setB2BAddressAsPrimaryAddress(AddressData addressData);

    void setB2BAddressAsPrimaryBillingAddress(AddressData addressData);

    void checkPrimaryShippingAddress(boolean isPrimary, AddressData newAddress);

    boolean validatePasswordToken(SecureToken data);// Moved as part of Baseshop Removal

}

package com.thyssenkrupp.b2b.eu.facades.paymentdetails.impl;

import static com.thyssenkrupp.b2b.eu.facades.predicate.TkEuAddressModelPredicates.isBillingAddress;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.paymentdetails.TkEuPaymentDetailsFacade;

import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuDefaultPaymentDetailsFacade implements TkEuPaymentDetailsFacade {


    private B2BCommerceUnitService b2bCommerceUnitService;

    private Converter<AddressModel, AddressData> addressConverter;

    @Override
    public List<AddressData> getB2bUnitBillingAddress() {

        Collection<AddressModel> addressData = b2bCommerceUnitService.getParentUnit().getAddresses();
        if (addressData != null) {
            return getAddressConverter().convertAll(addressData.stream().filter(isBillingAddress()).collect(Collectors.toList()));
        }
        return Collections.EMPTY_LIST;
    }

    protected Converter<AddressModel, AddressData> getAddressConverter() {
        return addressConverter;
    }

    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    public B2BCommerceUnitService getB2bCommerceUnitService() {
        return b2bCommerceUnitService;
    }

    public void setB2bCommerceUnitService(B2BCommerceUnitService b2bCommerceUnitService) {
        this.b2bCommerceUnitService = b2bCommerceUnitService;
    }
}

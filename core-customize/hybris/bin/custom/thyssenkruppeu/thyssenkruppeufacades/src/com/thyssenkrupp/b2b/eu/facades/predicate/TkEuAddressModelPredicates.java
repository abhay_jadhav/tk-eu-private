package com.thyssenkrupp.b2b.eu.facades.predicate;

import static java.lang.Boolean.TRUE;

import java.util.function.Predicate;

import de.hybris.platform.core.model.user.AddressModel;

public final class TkEuAddressModelPredicates {

    private TkEuAddressModelPredicates() {
    }

    public static Predicate<AddressModel> isContactAddress() {
        return address -> TRUE.equals(address.getContactAddress());
    }

    public static Predicate<AddressModel> isBillingAddress() {
        return address -> TRUE.equals(address.getBillingAddress());
    }

    public static Predicate<AddressModel> isShippingAddress() {
        return address -> TRUE.equals(address.getShippingAddress());
    }
}

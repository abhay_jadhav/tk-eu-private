package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

public class TkEuVariantProductPopulator implements Populator<ProductModel, ProductData> {

    @Override
    public void populate(final ProductModel source, final ProductData target) {
        if (source instanceof VariantProductModel) {
            populateAttributes((VariantProductModel) source, target);
        }
    }

    private void populateAttributes(final VariantProductModel source, final ProductData target) {
        target.setPackagingType(source.getPackagingType());
        target.setPackingSize(source.getPackingSize());
        target.setTransportLocking(source.isTransportLocking());
        target.setMaterialAvailability(source.getMaterialAvailability());
        target.setSupplierLeadTime(source.getSupplierLeadTime());
        target.setSupplierMinimumQuantity(source.getSupplierMinimumQuantity());
        target.setSuggestedRetailPrice(source.getSuggestedRetailPrice());
    }
}

package com.thyssenkrupp.b2b.eu.facades.product;

import de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;
import java.util.Map;

public interface TkEuProductClassificationFacade {

    void extractVisibleClassificationFeatures(ProductData productData);

    Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> mergeClassificationFeatures(ProductData masterProduct, ProductData variantProduct);
}

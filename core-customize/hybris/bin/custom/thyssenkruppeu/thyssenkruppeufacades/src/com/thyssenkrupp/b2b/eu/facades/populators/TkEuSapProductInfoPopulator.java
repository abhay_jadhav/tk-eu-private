package com.thyssenkrupp.b2b.eu.facades.populators;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.List;

import com.thyssenkrupp.b2b.global.baseshop.core.model.TkSapProductInfoModel;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;

public class TkEuSapProductInfoPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        List<AbstractOrderEntryProductInfoModel> abstractOrderEntryProductInfoModels = emptyIfNull(source.getProductInfos());
        abstractOrderEntryProductInfoModels.stream()
          .filter(TkSapProductInfoModel.class::isInstance).map(TkSapProductInfoModel.class::cast).forEach(
          tkSapProductInfoModel -> {
              ProductData productData = target.getProduct();
              if (productData != null) {
                  productData.setName(tkSapProductInfoModel.getProductName());
                  productData.setCode(tkSapProductInfoModel.getProductCode());
              }
          }
        );
    }
}

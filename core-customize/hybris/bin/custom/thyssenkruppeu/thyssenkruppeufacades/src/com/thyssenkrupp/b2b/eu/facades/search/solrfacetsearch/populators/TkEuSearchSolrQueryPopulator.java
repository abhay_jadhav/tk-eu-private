package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchSolrQueryPopulator;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.StringUtils;

public class TkEuSearchSolrQueryPopulator extends SearchSolrQueryPopulator {

    @Override
    protected SearchQuery createSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
        final SearchQueryContext searchQueryContext, final String freeTextSearch) {
        final String queryTemplateName = getSearchQueryTemplateNameResolver().resolveTemplateName(facetSearchConfig, indexedType,
            searchQueryContext);
        final String modifiedFreeTextSearch = StringUtils.isBlank(freeTextSearch) ? freeTextSearch : freeTextSearch.toLowerCase();
        return getFacetSearchService().createFreeTextSearchQueryFromTemplate(facetSearchConfig, indexedType,
            queryTemplateName, modifiedFreeTextSearch);
    }
}

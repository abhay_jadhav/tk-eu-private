package com.thyssenkrupp.b2b.eu.facades.atp.impl;

import static com.thyssenkrupp.b2b.eu.core.utils.TkEuAtpDateUtils.convertDateToStringDateMonthYear;
import static com.thyssenkrupp.b2b.eu.core.utils.TkEuAtpDateUtils.findNextWorkingDayFrom;
import static java.util.Optional.ofNullable;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collections;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpService;
import com.thyssenkrupp.b2b.eu.core.model.process.InStockStatusToMaxAvailabilityDaysMappingModel;
import com.thyssenkrupp.b2b.eu.core.service.InStockStatusToAvailabilityMappingService;
import com.thyssenkrupp.b2b.eu.core.stock.service.TkEuStockService;
import com.thyssenkrupp.b2b.eu.facades.atp.TkEuAtpFacade;
import com.thyssenkrupp.b2b.eu.facades.atp.strategy.TkEuAtpAvailabilityStrategy;
import com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCartFacade;
import com.thyssenkrupp.b2b.eu.facades.order.data.AtpViewData;
import com.thyssenkrupp.b2b.eu.facades.order.data.AtpViewEntryData;
import com.thyssenkrupp.b2b.eu.facades.order.data.AvailabilityColor;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.services.BaseStoreService;
import com.thyssenkrupp.b2b.eu.facades.product.ProductNameURLData;

public class TkEuAtpFacadeImpl implements TkEuAtpFacade {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuAtpFacadeImpl.class);
    private AtpService atpService;
    private InStockStatusToAvailabilityMappingService inStockStatusToAvailabilityMappingService;
    private TkEuCartFacade cartFacade;
    private TkEuAtpAvailabilityStrategy tkEuAtpAvailabilityStrategy;
    private TkEuStockService stockService;
    private WarehouseSelectionStrategy warehouseSelectionStrategy;
    private BaseStoreService baseStoreService;
    private DefaultCartFacade defaultCartFacade;
    private ModelService modelService;
    private CartService cartService;
    private UrlResolver<ProductModel> productModelUrlResolver;

    @Override
    public List<AtpResponseData> doAtpCheck(AtpRequestData request) {
        return getAtpService().doAtpCheck(request);
    }

    @Override
    public AtpViewData doAtpCheckAndBuildViewResponse(AtpRequestData request) {
        final AtpViewData data = new AtpViewData();
        boolean isPdpRequest = request.getIsPdpRequest().booleanValue();
        final List<AtpResponseData> responseDataList = doAtpCheck(request);
        if (CollectionUtils.isNotEmpty(responseDataList)) {
            if (!isPdpRequest) {
                getCartFacade().updateAtpInformation(responseDataList);
            }
            final List<AtpViewEntryData> entries = new ArrayList<>(responseDataList.size());
            for (final AtpResponseData responseData : responseDataList) {
                final AtpViewEntryData entry = new AtpViewEntryData();
                createResponsedata(isPdpRequest, responseData, entry);
                getDeliveryDate(responseData).ifPresent(entry::setDeliveryDate);
                entry.setPosition(responseData.getPosition());
                entries.add(entry);
            }
            data.setEntries(entries);
        }
        return data;
    }

    @Override
    public AtpViewData removeOutOfStockProductsAndGetConsolidateResult() {
        final AtpViewData data = new AtpViewData();
        CartModel sessionCart = getCartService().getSessionCart();

        Map<String, ProductNameURLData> deletedProducts = removeNotAvailableItems(sessionCart);
        if (MapUtils.isNotEmpty(deletedProducts)) {
            data.setOutOfStockProducts(deletedProducts);
        }

        getModelService().refresh(sessionCart);
        getCartService().setSessionCart(sessionCart);

        List<Date> deliveryDateList = new ArrayList<Date>();
        sessionCart.getEntries().forEach(entry -> {
            Optional<Date> deliverDate = getDeliveryDate(entry);
            if (deliverDate.isPresent()) {
                deliveryDateList.add(deliverDate.get());
            }
        });

        final Date consolidatedDate = deliveryDateList.stream().filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder()).findFirst().orElse(null);
        getCartFacade().updateAtpConsolidatedDate(consolidatedDate);
        data.setConsolidatedDate(convertDateToStringDateMonthYear(consolidatedDate));

        return data;
    }

    @Override
    public HashMap<String, ProductNameURLData> removeOutOfStockItems() {
        return removeNotAvailableItems(getCartService().getSessionCart());
    }
    private HashMap<String, ProductNameURLData> removeNotAvailableItems(CartModel cartModel) {
        HashMap<String, ProductNameURLData> outOfStockItems = new HashMap<String, ProductNameURLData>();
        try {
            List<AbstractOrderEntryModel> entriesTobeRemoved = (List<AbstractOrderEntryModel>) cartModel.getEntries()
                    .stream()
                    .filter(responseEntry -> !Objects.isNull(responseEntry.getHybrisATPCheckResult())
                            && InStockStatus.NOTAVAILABLE.getCode()
                                    .equalsIgnoreCase(responseEntry.getHybrisATPCheckResult().getCode()))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(entriesTobeRemoved)) {
                Collections.reverse(entriesTobeRemoved);
                entriesTobeRemoved.forEach(entry -> {
                    ProductModel product = entry.getProduct();
                    ProductNameURLData productNameUrl = new ProductNameURLData();
                    productNameUrl.setName(product.getName(Locale.GERMANY));
                    productNameUrl.setUrl(getProductModelUrlResolver().resolve(product));
                    outOfStockItems.put(product.getCode(), productNameUrl);
                    LOG.info("Product removed from cart due to unavailability -> " + entry.getProduct().getCode());
                    try {
                        getDefaultCartFacade().updateCartEntry(entry.getEntryNumber(), 0);
                    } catch (CommerceCartModificationException e) {
                        LOG.error("Exception in method removeNotAvailableItems:"+e.getMessage(),e);
                    }
                });
            }
            getModelService().refresh(cartModel);
            getCartService().setSessionCart(cartModel);
        } catch (Exception e) {
            LOG.error("Exception in method removeNotAvailableItems:"+e.getMessage(),e);
        }
        return outOfStockItems;
    }


    private void createResponsedata(boolean isPdpRequest, final AtpResponseData responseData,
            final AtpViewEntryData entry) {
        if (responseData.getSapAtpResult() == null) {
            setHybrisResponseData(isPdpRequest, responseData, entry);
        } else {
            setSapResponseData(isPdpRequest, responseData, entry);
        }
    }

    private void setSapResponseData(boolean isPdpRequest, final AtpResponseData responseData,
            final AtpViewEntryData entry) {
        if (isPdpRequest) {
            setPdpSapAvailabilityData(responseData.getSapAtpResult(), entry);

        } else {
            getSapAvailabilityColor(responseData.getSapAtpResult()).ifPresent(entry::setAvailabilityColor);
            getSapAvailabilityMessage().ifPresent(entry::setAvailabilityMessage);

        }
    }

    private void setHybrisResponseData(boolean isPdpRequest, final AtpResponseData responseData,
            final AtpViewEntryData entry) {
        if (isPdpRequest) {

            setPdpHybrisAvailabilityData(responseData.getHybrisAtpResult(), entry);
        } else {
            getHybrisAvailabilityColor(responseData.getHybrisAtpResult()).ifPresent(entry::setAvailabilityColor);
            getHybrisAvailabilityMessage(responseData.getHybrisAtpResult()).ifPresent(entry::setAvailabilityMessage);
        }
    }

    private void setPdpSapAvailabilityData(Date date, AtpViewEntryData entry) {
        getPdpSapAvailabilityStatus(date).ifPresent(entry::setAvailabilityStatus);
        getPdpSapAvailabilityTooltip().ifPresent(entry::setAvailabilityTooltip);
        getPdpSapAvailabilityColor(date).ifPresent(entry::setAvailabilityColor);
        getPdpSapAvailabilityMessage().ifPresent(entry::setAvailabilityMessage);

    }

    private void setPdpHybrisAvailabilityData(InStockStatus inStockStatus, AtpViewEntryData entry) {
        getPdpHybrisAvailabilityStatus(inStockStatus).ifPresent(entry::setAvailabilityStatus);
        getPdpHybrisAvailabilityTooltip(inStockStatus).ifPresent(entry::setAvailabilityTooltip);
        getPdpHybrisAvailabilityColor(inStockStatus).ifPresent(entry::setAvailabilityColor);
        getPdpHybrisAvailabilityMessage(inStockStatus).ifPresent(entry::setAvailabilityMessage);

    }

    private Optional<Date> getDeliveryDate(AtpResponseData atpResponseData) {
        if (atpResponseData.getSapAtpResult() != null) {
            return Optional.of(atpResponseData.getSapAtpResult());
        }
        return getDeliveryDateFromHybrisAtpResult(atpResponseData.getHybrisAtpResult());
    }

    private Optional<Date> getDeliveryDate(AbstractOrderEntryModel entryModel) {
        if (entryModel.getSapATPCheckResult() != null) {
            return Optional.of(entryModel.getSapATPCheckResult());
        }
        return getDeliveryDateFromHybrisAtpResult(entryModel.getHybrisATPCheckResult());
    }

    private Optional<Date> getDeliveryDateFromHybrisAtpResult(@NotNull InStockStatus hybrisAtpResult) {
        final InStockStatusToMaxAvailabilityDaysMappingModel byInStockStatus = getInStockStatusToAvailabilityMappingService()
                .findByInStockStatus(hybrisAtpResult);
        if (byInStockStatus != null) {
            int maxAvailabilityDays = byInStockStatus.getInStockStatus().name().equalsIgnoreCase(
                    ThyssenkruppeufacadesConstants.EXPRESS) ? findAvailabilityDayCountForExpressStock(byInStockStatus)
                            : byInStockStatus.getMaxAvailabilityDays();
            return ofNullable(findNextWorkingDayFrom(maxAvailabilityDays));
        }
        return Optional.empty();
    }

    private int findAvailabilityDayCountForExpressStock(
            InStockStatusToMaxAvailabilityDaysMappingModel byInStockStatus) {
        LocalDateTime currentTimeCet = LocalDateTime.now(ZoneId.of(ThyssenkruppeufacadesConstants.CET_TIMEZONE));
        LocalDateTime midday = LocalDateTime.of(currentTimeCet.getYear(), currentTimeCet.getMonth(),
                currentTimeCet.getDayOfMonth(), 12, 0);
        return currentTimeCet.isBefore(midday) ? byInStockStatus.getMaxAvailabilityDays()
                : byInStockStatus.getMaxAvailabilityDays() + 1;
    }

    private Optional<AvailabilityColor> getHybrisAvailabilityColor(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getHybrisAtpAvailabilityColor(inStockStatus);
    }

    private Optional<String> getHybrisAvailabilityMessage(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getHybrisAtpAvailabilityMessage(inStockStatus);
    }

    private Optional<AvailabilityColor> getPdpHybrisAvailabilityColor(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getPdpHybrisAtpAvailabilityColor(inStockStatus);
    }

    private Optional<String> getPdpHybrisAvailabilityMessage(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getPdpHybrisAtpAvailabilityMessage(inStockStatus);
    }

    private Optional<String> getPdpHybrisAvailabilityStatus(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getPdpHybrisAtpAvailabilityStatus(inStockStatus);
    }

    private Optional<String> getPdpHybrisAvailabilityTooltip(final InStockStatus inStockStatus) {
        return getTkEuAtpAvailabilityStrategy().getPdpHybrisAtpAvailabilityToolTip(inStockStatus);
    }

    private Optional<AvailabilityColor> getSapAvailabilityColor(final Date date) {
        return getTkEuAtpAvailabilityStrategy().getSapAtpAvailabilityColor(date);
    }

    private Optional<String> getSapAvailabilityMessage() {
        return getTkEuAtpAvailabilityStrategy().getSapAtpAvailabilityMessage();
    }

    private Optional<AvailabilityColor> getPdpSapAvailabilityColor(final Date date) {
        return getTkEuAtpAvailabilityStrategy().getPdpSapAtpAvailabilityColor(date);
    }

    private Optional<String> getPdpSapAvailabilityMessage() {
        return getTkEuAtpAvailabilityStrategy().getPdpSapAtpAvailabilityMessage();
    }

    private Optional<String> getPdpSapAvailabilityStatus(final Date date) {
        return getTkEuAtpAvailabilityStrategy().getPdpSapAtpAvailabilityStatus(date);
    }

    private Optional<String> getPdpSapAvailabilityTooltip() {
        return getTkEuAtpAvailabilityStrategy().getPdpSapAtpAvailabilityToolTip();
    }

    public TkEuCartFacade getCartFacade() {
        return cartFacade;
    }

    @Required
    public void setCartFacade(TkEuCartFacade cartFacade) {
        this.cartFacade = cartFacade;
    }

    public AtpService getAtpService() {
        return atpService;
    }

    @Required
    public void setAtpService(AtpService atpService) {
        this.atpService = atpService;
    }

    public InStockStatusToAvailabilityMappingService getInStockStatusToAvailabilityMappingService() {
        return inStockStatusToAvailabilityMappingService;
    }

    @Required
    public void setInStockStatusToAvailabilityMappingService(
            InStockStatusToAvailabilityMappingService inStockStatusToAvailabilityMappingService) {
        this.inStockStatusToAvailabilityMappingService = inStockStatusToAvailabilityMappingService;
    }

    public TkEuAtpAvailabilityStrategy getTkEuAtpAvailabilityStrategy() {
        return tkEuAtpAvailabilityStrategy;
    }

    @Required
    public void setTkEuAtpAvailabilityStrategy(TkEuAtpAvailabilityStrategy tkEuAtpAvailabilityStrategy) {
        this.tkEuAtpAvailabilityStrategy = tkEuAtpAvailabilityStrategy;
    }

    public TkEuStockService getStockService() {
        return stockService;
    }

    public void setStockService(TkEuStockService stockService) {
        this.stockService = stockService;
    }

    public WarehouseSelectionStrategy getWarehouseSelectionStrategy() {
        return warehouseSelectionStrategy;
    }

    public void setWarehouseSelectionStrategy(WarehouseSelectionStrategy warehouseSelectionStrategy) {
        this.warehouseSelectionStrategy = warehouseSelectionStrategy;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public DefaultCartFacade getDefaultCartFacade() {
        return defaultCartFacade;
    }

    @Required
    public void setDefaultCartFacade(DefaultCartFacade defaultCartFacade) {
        this.defaultCartFacade = defaultCartFacade;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CartService getCartService() {
        return cartService;
    }

    @Required
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    public UrlResolver<ProductModel> getProductModelUrlResolver() {
        return productModelUrlResolver;
    }

    public void setProductModelUrlResolver(UrlResolver<ProductModel> productModelUrlResolver) {
        this.productModelUrlResolver = productModelUrlResolver;
    }
}

package com.thyssenkrupp.b2b.eu.facades.product.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuLastViewedProductsFacade;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;

public class DefaultTkEuLastViewedProductsFacadeImpl implements TkEuLastViewedProductsFacade {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuLastViewedProductsFacadeImpl.class);

    private static final int DEFAULT_LAST_VIEWED_PRODUCTS   = 10;
    private static final int DEFAULT_CURRENT_PRODUCT_INDEX  = 0;
    private static final int DEFAULT_CURRENT_PRODUCT_OFFSET = 1;

    private static final String ANONYMOUS_LAST_VISITED_PRODUCTS_LIST = "lastVisitedPruductsList";

    private SessionService                       sessionService;
    private UserService                          userService;
    private ModelService                         modelService;
    private BaseStoreService                     baseStoreService;
    private ProductService                       productService;
    private Converter<ProductModel, ProductData> productConverter;

    @Override
    public void storeViewedProduct(String productCode) {
        UserModel user = getUserService().getCurrentUser();
        boolean anonymousUser = getUserService().isAnonymousUser(user);
        int numberOfProductsForStore = getNumberOfProductsForStore();

        if (user instanceof CustomerModel && !anonymousUser) {
            saveToModel(productCode, (CustomerModel) user, numberOfProductsForStore);
        } else if (user instanceof CustomerModel && anonymousUser) {

            List<String> visitedProducts = sessionService.getAttribute(ANONYMOUS_LAST_VISITED_PRODUCTS_LIST);
            saveToSession(productCode, numberOfProductsForStore, visitedProducts);
        }
    }

    protected void saveToModel(String productCode, CustomerModel customerModel, int numberOfProductsForStore) {

        try {
            ProductModel actualVisitedProduct = productService.getProductForCode(productCode);

            List<ProductModel> lastVisitedProducts = customerModel.getLastViewedProducts() != null ? customerModel.getLastViewedProducts() : new ArrayList<>();

            List<ProductModel> myUniqueProducts = new ArrayList<>(numberOfProductsForStore);

            myUniqueProducts.add(DEFAULT_CURRENT_PRODUCT_INDEX, actualVisitedProduct);

            for (ProductModel viewedProduct : lastVisitedProducts) {
                if (myUniqueProducts.size() == numberOfProductsForStore) {
                    break;
                }
                if (!actualVisitedProduct.getCode().equalsIgnoreCase(viewedProduct.getCode()))
                    myUniqueProducts.add(viewedProduct);
            }

            customerModel.setLastViewedProducts(myUniqueProducts);
            modelService.save(customerModel);
        } catch (Exception exp) {
            LOG.error("Failed to update the last visited product: " + productCode + " for Customer: " + customerModel.getCustomerID());
        }
    }

    protected void saveToSession(String productCode, int numberOfProductsForStore, List<String> lastVisitedProducts) {
        List<String> listToSave = new ArrayList<>(numberOfProductsForStore);

        listToSave.add(DEFAULT_CURRENT_PRODUCT_INDEX, productCode);
        if (lastVisitedProducts != null) {

            for (String visitedProductCode : lastVisitedProducts) {
                if (listToSave.size() == numberOfProductsForStore) {
                    break;
                }
                if (!productCode.equalsIgnoreCase(visitedProductCode)) {
                    listToSave.add(visitedProductCode);
                }
            }
        }
        sessionService.getCurrentSession().setAttribute(ANONYMOUS_LAST_VISITED_PRODUCTS_LIST, listToSave);
    }

    @Override
    public List<ProductData> loadViewedProducts() {
        int numberOfProductsForStore = getNumberOfProductsForStore();

        List<ProductModel> result = new ArrayList<>(numberOfProductsForStore);
        UserModel user = getUserService().getCurrentUser();

        boolean anonymousUser = getUserService().isAnonymousUser(user);

        if (user instanceof CustomerModel && !anonymousUser) {
            result = loadProductsFromModel((CustomerModel) user, numberOfProductsForStore);
        } else if (user instanceof CustomerModel && anonymousUser) {
            result = loadProductsFromSession(numberOfProductsForStore);
        }

        result = removeCurrentProductFromResult(result);

        return productConverter.convertAllIgnoreExceptions(result);
    }

    private List<ProductModel> removeCurrentProductFromResult(List<ProductModel> result) {
        if (result.isEmpty()) {
            return result;
        } else {
            return result.subList(DEFAULT_CURRENT_PRODUCT_OFFSET, result.size());
        }
    }

    protected List<ProductModel> loadProductsFromModel(CustomerModel user, int numberOfProductsForStore) {
        CustomerModel customerModel = user;
        List<ProductModel> result = new ArrayList<>(numberOfProductsForStore);
        List<ProductModel> lastVisitedProducts = customerModel.getLastViewedProducts();

        if (lastVisitedProducts != null && !lastVisitedProducts.isEmpty()) {
            for (ProductModel product : lastVisitedProducts) {
                if (result.size() == numberOfProductsForStore) {
                    break;
                }
                result.add(product);
            }
        }
        return result;
    }

    protected List<ProductModel> loadProductsFromSession(int numberOfProductsForStore) {
        List<ProductModel> result = new ArrayList<>();
        List<String> visitedProducts = sessionService.getAttribute(ANONYMOUS_LAST_VISITED_PRODUCTS_LIST);
        if (visitedProducts != null && !visitedProducts.isEmpty()) {
            for (String visitedProductCode : visitedProducts) {
                if (result.size() == numberOfProductsForStore) {
                    break;
                }
                ProductModel prod = productService.getProductForCode(visitedProductCode);
                if (prod != null) {
                    result.add(prod);
                }
            }
        }
        return result;
    }

    private int getNumberOfProductsForStore() {
        Integer numberOfProductsForStore = baseStoreService.getCurrentBaseStore().getNumberOflastVisitedProducts();
        numberOfProductsForStore = numberOfProductsForStore != null ? numberOfProductsForStore : DEFAULT_LAST_VIEWED_PRODUCTS;
        numberOfProductsForStore += DEFAULT_CURRENT_PRODUCT_OFFSET;
        return numberOfProductsForStore.intValue();
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public Converter<ProductModel, ProductData> getProductConverter() {
        return productConverter;
    }

    @Required
    public void setProductConverter(Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }

    public ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserService getUserService() {
        return userService;
    }
}

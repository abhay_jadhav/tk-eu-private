package com.thyssenkrupp.b2b.eu.facades.atp.strategy.impl;

import static com.thyssenkrupp.b2b.eu.core.utils.TkEuAtpDateUtils.findNextWorkingDayFrom;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.facades.atp.strategy.TkEuAtpAvailabilityStrategy;
import com.thyssenkrupp.b2b.eu.facades.order.data.AvailabilityColor;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;

public class DefaultTkEuAtpAvailabilityStrategy implements TkEuAtpAvailabilityStrategy {
    private static final String SAP_AVAILABILITY_COLOR_DAYS_FACTOR = "tk.eu.product.sap.availabilityColorDaysFactor";
    private ConfigurationService configurationService;

    @Override
    public Optional<AvailabilityColor> getHybrisAtpAvailabilityColor(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of(AvailabilityColor.GREEN);
            case "available1":
                return Optional.of(AvailabilityColor.GREEN);
            case "available2":
                return Optional.of(AvailabilityColor.YELLOW);
            case "available3":
                return Optional.of(AvailabilityColor.YELLOW);
            case "onDemand":
                return Optional.of(AvailabilityColor.YELLOW);
            case "forceInStock":
                return Optional.of(AvailabilityColor.GREEN);
            case "notAvailable":
                return Optional.of(AvailabilityColor.RED);
            default:
                return Optional.empty();
        }
    }

    @Override
    public Optional<String> getHybrisAtpAvailabilityMessage(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of("cart.availability.express");
            case "available1":
                return Optional.of("cart.availability.available1");
            case "available2":
                return Optional.of("cart.availability.available2");
            case "available3":
                return Optional.of("cart.availability.available3");
            case "onDemand":
                return Optional.of("cart.availability.onDemand");
            case "forceInStock":
                return Optional.of("cart.availability.available1");
            case "notAvailable":
                return Optional.of("cart.availability.notavailable");
            default:
                return Optional.empty();
        }
    }

    @Override
    public Optional<AvailabilityColor> getSapAtpAvailabilityColor(Date date) {
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DAY_OF_MONTH, getConfigurationService().getConfiguration().getInt(SAP_AVAILABILITY_COLOR_DAYS_FACTOR));
        if (calender.getTime().after(date)) {
            return Optional.of(AvailabilityColor.GREEN);
        }
        return Optional.of(AvailabilityColor.YELLOW);
    }

    @Override
    public Optional<String> getSapAtpAvailabilityMessage() {
        return Optional.of("cart.availability.position.expectedDate");
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public Optional<String> getPdpSapAtpAvailabilityStatus(Date date) {
        return Optional.of("pdp.availability.atp");
    }

    @Override
    public Optional<String> getPdpSapAtpAvailabilityToolTip() {
        return Optional.of("pdp.availability.atp.tooltip");
    }

    @Override
    public Optional<AvailabilityColor> getPdpSapAtpAvailabilityColor(Date date) {
        final Date availableWorkingDay = findNextWorkingDayFrom(getConfigurationService().getConfiguration().getInt(SAP_AVAILABILITY_COLOR_DAYS_FACTOR));
        if (date.after(availableWorkingDay)) {
            return Optional.of(AvailabilityColor.YELLOW);
        }
        return Optional.of(AvailabilityColor.GREEN);
    }

    @Override
    public Optional<String> getPdpSapAtpAvailabilityMessage() {
        return Optional.of("pdp.availability.expectedDate");
    }

    @Override
    public Optional<String> getPdpHybrisAtpAvailabilityStatus(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of("product.stock.immediatelyAvailable");
            case "available1":
                return Optional.of("product.stock.immediatelyAvailable");
            case "available2":
                return Optional.of("pdp.availability.later");
            case "available3":
                return Optional.of("pdp.availability.later");
            case "onDemand":
                return Optional.of("product.stock.onDemand");
            case "notAvailable":
                return Optional.of("product.stock.notAvailable");
            case "forceInStock":
                return Optional.of("product.stock.immediatelyAvailable");
            default:
                return Optional.empty();
        }
    }

    @Override
    public Optional<String> getPdpHybrisAtpAvailabilityToolTip(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of("pdp.availability.express.tooltip");
            case "available1":
                return Optional.of("pdp.availability.express.tooltip");
            case "available2":
                return Optional.of("pdp.availability.later.tooltip");
            case "available3":
                return Optional.of("pdp.availability.later.tooltip");
            case "onDemand":
                return Optional.of("pdp.availability.later.tooltip");
            case "notAvailable":
                return Optional.of("pdp.availability.unavailable.tooltip");
            case "forceInStock":
                return Optional.of("pdp.availability.express.tooltip");
            default:
                return Optional.empty();
        }
    }

    @Override
    public Optional<AvailabilityColor> getPdpHybrisAtpAvailabilityColor(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of(AvailabilityColor.GREEN);
            case "available1":
                return Optional.of(AvailabilityColor.GREEN);
            case "available2":
                return Optional.of(AvailabilityColor.YELLOW);
            case "available3":
                return Optional.of(AvailabilityColor.YELLOW);
            case "onDemand":
                return Optional.of(AvailabilityColor.YELLOW);
            case "notAvailable":
                return Optional.of(AvailabilityColor.RED);
            case "forceInStock":
                return Optional.of(AvailabilityColor.GREEN);
            default:
                return Optional.empty();
        }
    }

    @Override
    public Optional<String> getPdpHybrisAtpAvailabilityMessage(InStockStatus inStockStatus) {
        switch (inStockStatus.getCode()) {
            case "express":
                return Optional.of("product.stock.availability.express");
            case "available1":
                return Optional.of("product.stock.availability.available1");
            case "available2":
                return Optional.of("product.stock.availability.available2");
            case "available3":
                return Optional.of("product.stock.availability.available3");
            case "onDemand":
                return Optional.of("product.stock.availability.onDemand");
            case "notAvailable":
                return Optional.of("product.stock.availability.notAvailable");
            case "forceInStock":
                return Optional.of("product.stock.availability.available1");
            default:
                return Optional.empty();
        }
    }
}

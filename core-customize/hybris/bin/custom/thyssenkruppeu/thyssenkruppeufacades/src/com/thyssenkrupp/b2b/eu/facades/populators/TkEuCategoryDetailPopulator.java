package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.core.model.media.MediaModel;

public class TkEuCategoryDetailPopulator extends CategoryPopulator {

    @Override
    public void populate(CategoryModel source, CategoryData target) {
        super.populate(source, target);
        populatePicture(source, target);
        target.setDescription(source.getDescription());
    }

    private void populatePicture(CategoryModel source, CategoryData target) {
        MediaModel picture = source.getPicture();
        if (picture != null) {
            target.setPicture(getImageConverter().convert(picture));
        }
    }
}

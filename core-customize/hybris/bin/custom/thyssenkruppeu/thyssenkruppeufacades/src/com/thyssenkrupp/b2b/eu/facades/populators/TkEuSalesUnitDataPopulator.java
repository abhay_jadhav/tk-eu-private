package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatMtkName;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;

public class TkEuSalesUnitDataPopulator<SOURCE extends List<TkUomConversionModel>, TARGET extends List<SalesUnitData>> implements Populator<SOURCE, TARGET> {

    private TkEuUomConversionService uomConversionService;
    private TkEuPriceDataFactory     tkEuPriceDataFactory;

    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
        Optional<TkUomConversionModel> weightedUomConversion = getUomConversionService().getFirstWeightedUomConversion(source);
        BigDecimal weightPerBaseUnit = weightedUomConversion.flatMap(getUomConversionService()::calculateWeightPerBaseUnit).orElse(BigDecimal.ONE);
        for (TkUomConversionModel conversionModel : source) {
            SalesUnitData salesUnitData = generateSalesUnitData(conversionModel.getSalesUnit());
            updateSalesUnitToBaseUnitFactor(conversionModel, salesUnitData);
            updateIsWeightedUnit(salesUnitData, conversionModel);
            salesUnitData.setWeightPerSalesUnit(1.0);
            if (weightedUomConversion.isPresent()) {
                BigDecimal weightPerSalesUnit = getUomConversionService().calculateWeightPerSalesUnit(weightedUomConversion.get(), conversionModel).orElse(BigDecimal.ONE);
                salesUnitData.setWeightPerSalesUnit(weightPerSalesUnit.doubleValue());
            }
            salesUnitData.setWeightPerBaseUnit(weightPerBaseUnit.doubleValue());
            target.add(salesUnitData);
        }
    }

    private void updateSalesUnitToBaseUnitFactor(TkUomConversionModel conversionModel, SalesUnitData salesUnitData) {
        getUomConversionService().calculateFactor(conversionModel.getBaseUnitFactor(), conversionModel.getSalesUnitFactor())
          .ifPresent(bigDecimal -> {
              BigDecimal applyCommercialWeight = bigDecimal;
              if (!getUomConversionService().isWeightedUomConversion(conversionModel)) {
                  applyCommercialWeight = getUomConversionService().applyCommercialWeight(conversionModel.getProductId(), conversionModel.getBaseUnit(), bigDecimal, getUomConversionService().getUomConversionByProductCode(conversionModel.getProductId()));
              }
              PriceData priceData = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, applyCommercialWeight, formatUom(conversionModel.getBaseUnit().getName(), conversionModel.getSalesUnit().getName()));
              salesUnitData.setSalesToBaseUnitFactor(priceData);
          });
    }

    private void updateIsWeightedUnit(SalesUnitData salesUnitData, TkUomConversionModel conversionModel) {
        if (getUomConversionService().isWeightedUomConversion(conversionModel)) {
            salesUnitData.setIsWeightedUnit(true);
        }
    }

    private SalesUnitData generateSalesUnitData(UnitModel unitModel) {
        final SalesUnitData data = new SalesUnitData();
        data.setCode(unitModel.getCode());
        data.setName(formatMtkName(unitModel.getName()));
        data.setSapCode(unitModel.getSapCode());
        data.setUnitType(unitModel.getUnitType());
        data.setZilliantUnit(unitModel.getZilliantUnit());
        return data;
    }

    public TkEuUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    @Required
    public void setUomConversionService(TkEuUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    @Required
    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }
}

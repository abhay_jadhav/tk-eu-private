package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.core.service.MaterialShape;
import com.thyssenkrupp.b2b.eu.core.service.ProductLengthConfigurationParam;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.core.service.impl.TradeLengthUomConversionHelper;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.math.RoundingMode;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_MMT_UNIT_TYPE_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_WEIGHTED_TYPE_CODE;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.PAC;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.PROS;
import static com.thyssenkrupp.b2b.eu.core.service.MaterialShape.LONG;
import static com.thyssenkrupp.b2b.eu.core.service.impl.TradeLengthUomConversionHelper.isValidTradeLengthSupportedUnit;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;
import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUomWithPriceFactor;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.valueOf;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.lowerCase;

public class TkEuOrderEntryUomConversionPopulator<SOURCE extends AbstractOrderEntryModel, TARGET extends OrderEntryData> implements Populator<SOURCE, TARGET> {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuOrderEntryUomConversionPopulator.class);

    private static String LENGTH_UNIT_DISPLAY = "mm";

    private TkEuUomConversionService            uomConversionService;
    private TkEuPriceDataFactory                tkEuPriceDataFactory;
    private UnitService                         unitService;
    private CostConfigurationProductInfoService costConfigurationProductInfoService;
    private TkEuConfiguratorSettingsService     configuratorSettingsService;
    private TradeLengthUomConversionHelper      tradeLengthUomConversionHelper;
    private TkEuB2bProductService               tkEuB2bProductService;

    private Converter<List<TkUomConversionModel>, List<SalesUnitData>> tkEuSalesDataConverter;

    @Override
    public void populate(SOURCE source, TARGET target) {
        if (source.getProduct() != null && !ThyssenkruppeufacadesConstants.SAP_DUMMY_PRODUCT.equalsIgnoreCase(source.getProduct().getCode())) {
            try {
                List<TkUomConversionModel> supportedUomConversions = getUomConversionService().getStoreSupportedUomConversions(source.getProduct());
                List<TkUomConversionModel> productUomConversions = getUomConversionService().getSupportedUomConversions(source.getProduct());
                List<SalesUnitData> salesUnitDataList = getTkEuSalesDataConverter().convert(productUomConversions);
                updateSalesUnitDataList(source, target, supportedUomConversions, salesUnitDataList);
                List<SalesUnitData> supportedSalesUnits = getSupportedSalesUnit(source, supportedUomConversions, salesUnitDataList);
                updateSupportedUoms(target, supportedUomConversions, productUomConversions, supportedSalesUnits);
            } catch (TkEuPriceServiceException ex) {
                LOG.error("Can not find SalesUnitData with following reason", ex);
            } catch (Exception e) {
                LOG.error("Exception in populating Product UOM: "+e.getMessage());
            }

        }
    }

    private List<SalesUnitData> getSupportedSalesUnit(SOURCE source, List<TkUomConversionModel> supportedUomConversions, List<SalesUnitData> salesUnitDataList) {
        List<SalesUnitData> supportedSalesUnits;
        if (emptyIfNull(source.getProductInfos()).stream().filter(productInfo -> productInfo.getConfiguratorType() == CUTTOLENGTH_PRODINFO).findFirst().isPresent()) {
            supportedSalesUnits = filterC2lSupportedSalesUnits(salesUnitDataList);
        } else {
            supportedSalesUnits = filterStoreSupportedSalesUnits(salesUnitDataList, supportedUomConversions);
        }
        return supportedSalesUnits;
    }

    private void updateSupportedUoms(TARGET target, List<TkUomConversionModel> supportedUomConversions, List<TkUomConversionModel> productUomConversions, @NotNull List<SalesUnitData> supportedSalesUnits) {
        target.setSupportedSalesUnits(supportedSalesUnits);
        target.setIsSupportedUomPresent(checkSupportedUom(supportedUomConversions, productUomConversions));
        SalesUnitData targetSalesUnitData = target.getSalesUnitData();
        if (targetSalesUnitData == null || (!supportedSalesUnits.contains(targetSalesUnitData) && isNotEmpty(supportedSalesUnits))) {
            target.setSalesUnitData(supportedSalesUnits.get(0));
        }
    }

    private void updateSalesUnitDataList(SOURCE source, TARGET target, List<TkUomConversionModel> supportedUomConversions, List<SalesUnitData> salesUnitDataList) {
        updateSalesUnitData(source, salesUnitDataList, target);
        updatePriceAndWeight(source, target, salesUnitDataList);
        updateSalesToBaseUnitFactorForTradeLength(source, salesUnitDataList, supportedUomConversions, target);
    }

    private boolean checkSupportedUom(List<TkUomConversionModel> supportedUomConversions, List<TkUomConversionModel> productUomConversions) {
        return productUomConversions.stream().anyMatch(tkUomConversionModel -> supportedUomConversions.contains(tkUomConversionModel));
    }

    private List<SalesUnitData> filterStoreSupportedSalesUnits(List<SalesUnitData> salesUnitDataList, List<TkUomConversionModel> supportedUomConversions) {
        ArrayList<SalesUnitData> salesUnitDatas = new ArrayList<>();
        for (SalesUnitData salesUnitData : emptyIfNull(salesUnitDataList)) {
            List<TkUomConversionModel> tkUomConversionModels = emptyIfNull(supportedUomConversions);
            if (tkUomConversionModels.stream().anyMatch(tkUomConversionModel -> tkUomConversionModel.getSalesUnit().getCode().equals(salesUnitData.getCode()))) {
                salesUnitDatas.add(salesUnitData);
            }
        }
        return salesUnitDatas;
    }

    private List<SalesUnitData> filterC2lSupportedSalesUnits(List<SalesUnitData> salesUnitDataList) {
        ArrayList<SalesUnitData> salesUnitDatas = new ArrayList<>();
        List<String> c2lSupportedUnitCodes = getTradeLengthUomConversionHelper().getC2lSupportedUnits();
        if (isNotEmpty(c2lSupportedUnitCodes)) {
            for (SalesUnitData salesUnitData : emptyIfNull(salesUnitDataList)) {
                if (c2lSupportedUnitCodes.stream().anyMatch(unit -> unit.equalsIgnoreCase(salesUnitData.getCode()))) {
                    salesUnitDatas.add(salesUnitData);
                }
            }
        }
        return salesUnitDatas;
    }

    protected void updatePriceAndWeight(SOURCE source, TARGET target, List<SalesUnitData> salesUnitDataList) {
        if (source.getQuantity() != null) {
            updateSubTotal(source, target);
            updatePricePerBaseUnit(source, target);
            updatePriceInWeight(source, target, salesUnitDataList);
        }
    }

    private Optional<Boolean> updatePricePerBaseUnit(SOURCE source, TARGET target) {
        List<AbstractOrderEntryProductInfoModel> productInfos = emptyIfNull(source.getProductInfos());
        Optional<CostConfigurationProductInfoModel> costConfigurationProductInfoModel = productInfos.stream()
                .filter(getCostConfigurationProductInfoService().isPacCostConfig())
                .map(abstractOrderEntryProductInfoModel -> (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel)
                .findFirst();
        if (!costConfigurationProductInfoModel.isPresent()) {
            costConfigurationProductInfoModel = productInfos.stream()
                    .filter(getCostConfigurationProductInfoService().isProsCostConfig())
                    .map(abstractOrderEntryProductInfoModel -> (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel)
                    .findFirst();
           
        }
        if (!costConfigurationProductInfoModel.isPresent()) {
            costConfigurationProductInfoModel = productInfos.stream()
                    .filter(getCostConfigurationProductInfoService().isZilliantCostConfig())
                    .map(abstractOrderEntryProductInfoModel -> (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel)
                    .findFirst();
        }
        return costConfigurationProductInfoModel.map(mapPricePerBaseUnitFromCostConfig(source, target));
    }

    private Function<CostConfigurationProductInfoModel, Boolean> mapPricePerBaseUnitFromCostConfig(final SOURCE source, final TARGET target) {
        return costConfigurationProductInfoModel -> {
            try {
                double pdpPrice = costConfigurationProductInfoModel.getValue();
                long pdpQuantity = costConfigurationProductInfoModel.getQuantity();

                List<AbstractOrderEntryProductInfoModel> productInfos = emptyIfNull(source.getProductInfos());

                Optional<CostConfigurationProductInfoModel> costConfigurationProductInfo = productInfos.stream()
                        .filter((getCostConfigurationProductInfoService().isPacCostConfig())
                                .or(getCostConfigurationProductInfoService().isProsCostConfig())
                                .or(getCostConfigurationProductInfoService().isZilliantCostConfig()))
                        .map(abstractOrderEntryProductInfoModel -> (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel)
                        .findFirst();
                
                
                Optional<CostConfigurationProductInfoModel> zilCuspriCostConfigurationProductInfoModel = productInfos
                        .stream().filter(getCostConfigurationProductInfoService().isZilCuspriCostConfig())
                        .map(abstractOrderEntryProductInfoModel -> (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel)
                        .findFirst();

                BigDecimal priceInBaseUOM = null;
                LOG.debug("costConfigurationProductInfo " + costConfigurationProductInfo);
                if (costConfigurationProductInfo.isPresent()) {
                    if (PAC.equals(costConfigurationProductInfo.get().getLabel())) {
                        priceInBaseUOM = getUomConversionService().calculatePriceInBaseUOM(source.getProduct(),
                                valueOf(pdpPrice / pdpQuantity), costConfigurationProductInfo.get().getUnit());
                    } else if (PROS.equals(costConfigurationProductInfo.get().getLabel())&& TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeupricedataConstants.PROS_PRICING_ENABLED, true) ) {
                        priceInBaseUOM = getUomConversionService().calculatePriceInBaseUOM(source.getProduct(),
                                valueOf(pdpPrice / pdpQuantity), costConfigurationProductInfo.get().getUnit());
                    } else {
                        if (zilCuspriCostConfigurationProductInfoModel.isPresent()) {
                            pdpPrice = pdpPrice + zilCuspriCostConfigurationProductInfoModel.get().getValue();
                        }
                        priceInBaseUOM = getUomConversionService().calculatePriceInBaseUOM(source.getProduct(),
                                valueOf(pdpPrice / pdpQuantity), costConfigurationProductInfo.get().getUnit());
                }
                String baseUnitName = source.getProduct().getBaseUnit().getName();

                PriceData priceDataPerBaseUnit = getTkEuPriceDataFactory().create(PriceDataType.BUY, priceInBaseUOM, source.getOrder().getCurrency().getIsocode(), BigDecimal.valueOf(pdpPrice), formatUomWithPriceFactor(pdpQuantity,baseUnitName));
                target.setPricePerBaseUnit(priceDataPerBaseUnit);
                }
                boolean isFlatMaterial = isTypeOf(source, LONG);
                if (isFlatMaterial && isValidTradeLengthSupportedUnit(source.getUnit().getCode())) {
                    setProductTotalLength(source, target);
                }
                return TRUE;
            } catch (TkEuPriceServiceException ex) {
                LOG.error("Can not price in base unit with following reason", ex);
                return FALSE;
            }
        };
    }

    protected boolean isTypeOf(AbstractOrderEntryModel abstractOrderEntryModel, final @NotNull MaterialShape shape) {
        Optional<MaterialShape> productShape = getTkEuB2bProductService().getProductShape(abstractOrderEntryModel.getProduct());
        return productShape.equals(Optional.of(shape));
    }

    protected void setProductTotalLength(AbstractOrderEntryModel abstractOrderEntryModel, TARGET target) {
        Optional<ProductLengthConfigurationParam> productLengthConfigurationParam = getConfiguratorSettingsService().getProductLengthInfoForOrderEntry(abstractOrderEntryModel);
        if (productLengthConfigurationParam.isPresent()) {
            UnitModel mmUnit = unitService.getUnitForCode(SAP_MMT_UNIT_TYPE_CODE);
            if (mmUnit.getConversion() != null) {
                double productLength = convertToMillimeter(mmUnit.getConversion(), productLengthConfigurationParam.get().getLengthValue());
                PriceData totalProductLength = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY,  calculateTotalProductLength(productLength, abstractOrderEntryModel.getQuantity()), " " + LENGTH_UNIT_DISPLAY);
                target.setProductTotalLength(totalProductLength);
            }
        }
    }

    protected BigDecimal calculateTotalProductLength(double productLength, Long productLengthValue) {
        return BigDecimal.valueOf(productLength * productLengthValue);
    }

    protected double convertToMillimeter(Double unitConversion, Double productLengthValue) {
        return (productLengthValue / unitConversion);
    }

    protected void updateSubTotal(SOURCE source, TARGET target) {
        BigDecimal subTotal = valueOf(source.getBasePrice() * source.getQuantity().doubleValue());
        PriceData subTotalInSalesUnit = getTkEuPriceDataFactory().create(PriceDataType.BUY, subTotal, source.getOrder().getCurrency().getIsocode(), formatUom(source.getUnit().getName()));
        target.setSubTotal(subTotalInSalesUnit);
    }

    protected void updateSalesUnitData(SOURCE source, List<SalesUnitData> salesUnitDataList, TARGET target) {
        String salesUnitCode = source.getUnit() != null ? source.getUnit().getCode() : target.getUnit();
        if (salesUnitCode != null && isNotEmpty(salesUnitDataList)) {
            salesUnitDataList.forEach(salesUnitData -> {
                if (salesUnitCode.equals(salesUnitData.getCode())) {
                    target.setSalesUnitData(salesUnitData);
                    salesUnitData.setIsDefaultUnit(true);
                } else {
                    salesUnitData.setIsDefaultUnit(false);
                }
            });
        }
    }

    protected void setWeightPerUnitInKg(TARGET target, BigDecimal totalWeightForOrderEntryInKg, UnitModel kgUnit) {
        BigDecimal kgPerSalesUnit = getUomConversionService().calculateFactor(totalWeightForOrderEntryInKg.doubleValue(), target.getQuantity().doubleValue()).orElse(ONE);
        PriceData kgPerSalesUnitPrice = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, kgPerSalesUnit.setScale(2, RoundingMode.HALF_UP), formatUom(kgUnit.getName(), target.getUnitName()));
        target.setKgPerSalesUnit(kgPerSalesUnitPrice);
    }

    protected void updatePriceInWeight(SOURCE source, TARGET target, List<SalesUnitData> salesUnitDataList) {
        Optional<SalesUnitData> firstWeightedUnit = salesUnitDataList.stream().filter(salesUnitData -> salesUnitData.isIsWeightedUnit()).findFirst();
        firstWeightedUnit.ifPresent(weightedUnit -> {
            SalesUnitData salesUnitData = target.getSalesUnitData();
            if (salesUnitData.isIsWeightedUnit()) {
                updateWeightedSalesUnit(source, target, salesUnitData);
            } else {
                updateNonWeightedSalesUnit(source, target);
            }
        });
    }

    protected void updateWeightedSalesUnit(SOURCE source, TARGET target, SalesUnitData weightedUnit) {
        PriceData totalWeightPriceData = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, valueOf(source.getQuantity()), " " + lowerCase(weightedUnit.getCode()));
        UnitModel weightInHybrisUnit = getUnitService().getUnitForCode(weightedUnit.getCode());
        double totalWeightInKg = totalWeightPriceData.getValue().doubleValue() * weightInHybrisUnit.getConversion();
        UnitModel kgUnit = unitService.getUnitForCode(SAP_KGM_UNIT_CODE);
        PriceData priceDataTotalWeightInKg = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, valueOf(totalWeightInKg), " " + kgUnit.getName());
        target.setTotalWeightInKg(priceDataTotalWeightInKg);
    }

    protected void updateNonWeightedSalesUnit(SOURCE source, TARGET target) {
        BigDecimal totalWeightForOrderEntryInKg = BigDecimal.ZERO;
        try {
            totalWeightForOrderEntryInKg = source.getPositionWeight() != null ? valueOf(source.getPositionWeight()) : getUomConversionService().findTotalWeightForOrderEntryInKg(source);
        } catch (TkEuPriceServiceException e) {
            LOG.error("Can not find SalesUnitData with following reason", totalWeightForOrderEntryInKg);
        }

        UnitModel kgUnit = unitService.getUnitForCode(SAP_KGM_UNIT_CODE);
        setWeightPerUnitInKg(target, totalWeightForOrderEntryInKg, kgUnit);
        PriceData totalWeightInKgPriceData = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, totalWeightForOrderEntryInKg.setScale(2, RoundingMode.HALF_UP), " " + kgUnit.getName());
        target.setTotalWeightInKg(totalWeightInKgPriceData);
    }

    protected void updateSalesToBaseUnitFactorForTradeLength(AbstractOrderEntryModel abstractOrderEntryModel, List<SalesUnitData> salesUnitDataList, List<TkUomConversionModel> supportedUomConversions, TARGET target) {
        Optional<ProductLengthConfigurationParam> productLengthConfigurationParam = getConfiguratorSettingsService().getProductLengthInfoForOrderEntry(abstractOrderEntryModel);
        UnitModel salesUnitModel = abstractOrderEntryModel.getUnit();
        LOG.debug("pricePerBaseUnit= "+target.getPricePerBaseUnit()+" subtotal= "+target.getSubTotal());
        if (SAP_KGM_UNIT_CODE.equals(abstractOrderEntryModel.getProduct().getBaseUnit().getCode()) && salesUnitModel != null && !SAP_WEIGHTED_TYPE_CODE.equals(salesUnitModel.getUnitType())) {
            updateSalesToBaseUnitFactor(abstractOrderEntryModel, salesUnitDataList, target.getKgPerSalesUnit().getValue(), salesUnitModel.getCode());
        } else if (productLengthConfigurationParam.isPresent()) {
            updateSalesUnitDataFromProductLengthConfiguration(abstractOrderEntryModel, salesUnitDataList, target);
        }
    }

    private void updateSalesUnitDataFromProductLengthConfiguration(AbstractOrderEntryModel abstractOrderEntryModel, List<SalesUnitData> salesUnitDataList, TARGET target) {
        if(target.getPricePerBaseUnit()!=null && target.getSubTotal()!=null) {
        final BigDecimal pricePerBaseUnit = target.getPricePerBaseUnit().getValue();
        final BigDecimal subTotal = target.getSubTotal().getValue();
        LOG.debug("pricePerBaseUnit= "+pricePerBaseUnit+" subtotal= "+subTotal);
        BigDecimal salesToBaseUnitFactor = getUomConversionService().calculateFactor(subTotal.doubleValue(), pricePerBaseUnit.doubleValue() * target.getQuantity()).orElse(ONE);
        updateSalesToBaseUnitFactor(abstractOrderEntryModel, salesUnitDataList, salesToBaseUnitFactor, abstractOrderEntryModel.getUnit().getCode());
        }else {
            LOG.debug("Either PricePerBaseUnit or SubTotal is empty");
        }
    }

    private void updateSalesToBaseUnitFactor(AbstractOrderEntryModel abstractOrderEntryModel, List<SalesUnitData> salesUnitDataList, BigDecimal salesToBaseUnitFactor, String selectedUnit) {
        emptyIfNull(salesUnitDataList).stream().filter(salesUnitData -> StringUtils.equals(selectedUnit, salesUnitData.getCode()))
          .forEach(salesUnitData -> {
              PriceData salesToBaseUnitFactorData = getTkEuPriceDataFactory().createWithoutCurrency(PriceDataType.BUY, salesToBaseUnitFactor.setScale(2, RoundingMode.HALF_UP), formatUom(abstractOrderEntryModel.getProduct().getBaseUnit().getName(), salesUnitData.getName()));
              salesUnitData.setSalesToBaseUnitFactor(salesToBaseUnitFactorData);
          });
    }

    public TradeLengthUomConversionHelper getTradeLengthUomConversionHelper() {
        return tradeLengthUomConversionHelper;
    }

    @Required
    public void setTradeLengthUomConversionHelper(TradeLengthUomConversionHelper tradeLengthUomConversionHelper) {
        this.tradeLengthUomConversionHelper = tradeLengthUomConversionHelper;
    }

    public TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public Converter<List<TkUomConversionModel>, List<SalesUnitData>> getTkEuSalesDataConverter() {
        return tkEuSalesDataConverter;
    }

    @Required
    public void setTkEuSalesDataConverter(Converter<List<TkUomConversionModel>, List<SalesUnitData>> tkEuSalesDataConverter) {
        this.tkEuSalesDataConverter = tkEuSalesDataConverter;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    @Required
    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

    public TkEuUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    public void setUomConversionService(TkEuUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    public CostConfigurationProductInfoService getCostConfigurationProductInfoService() {
        return costConfigurationProductInfoService;
    }

    @Required
    public void setCostConfigurationProductInfoService(CostConfigurationProductInfoService costConfigurationProductInfoService) {
        this.costConfigurationProductInfoService = costConfigurationProductInfoService;
    }

    public TkEuB2bProductService getTkEuB2bProductService() {
        return tkEuB2bProductService;
    }

    public void setTkEuB2bProductService(TkEuB2bProductService tkEuB2bProductService) {
        this.tkEuB2bProductService = tkEuB2bProductService;
    }
}

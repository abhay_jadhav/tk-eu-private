package com.thyssenkrupp.b2b.eu.facades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

public interface TkEuB2bCustomerFacade extends CustomerFacade {

    boolean completeCustomerRegistration(String uId);

    boolean isTokenExpired(String token);

    boolean isTokenInvalid(String token);

    String getEmailIdFromToken(String token);

    CustomerData getCurrentB2bCustomer();

    AddressData getPrimaryShippingAddressOfCurrentUser();
}


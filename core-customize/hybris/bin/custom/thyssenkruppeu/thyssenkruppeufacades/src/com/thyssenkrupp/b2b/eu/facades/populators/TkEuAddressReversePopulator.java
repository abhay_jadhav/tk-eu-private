package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static org.apache.commons.lang.StringUtils.isBlank;

public class TkEuAddressReversePopulator extends AddressReversePopulator implements Populator<AddressData, AddressModel> {

    @Override
    public void populate(AddressData addressData, AddressModel addressModel) throws ConversionException {
        if(isBlank(addressData.getTitleCode())) {
            addressData.setTitleCode(null);
            addressModel.setTitle(null);
        }
        super.populate(addressData, addressModel);
        addressModel.setDepartment(addressData.getDepartment());
    }
}

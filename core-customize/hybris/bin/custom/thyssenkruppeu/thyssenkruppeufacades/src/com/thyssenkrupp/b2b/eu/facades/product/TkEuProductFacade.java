package com.thyssenkrupp.b2b.eu.facades.product;

import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface TkEuProductFacade extends ProductFacade {

    boolean isBaseProduct(String productCode);

    boolean isPurchasableProduct(String productCode);

    boolean isElementStatusActive(ProductData productData, String name);

    void populateActiveVariantCategoryValues(List<VariantProductModel> variants, ProductData productData);

    List<VariantProductModel> getAllVariantsFilteredBySelectedVvc(ProductModel productModel, Map<String, String> selectedVvcMap);

    String getBaseProductCodeForVariant(String productCode);

    boolean isVariantSelected(Map<String, String> variantSelectionMap, int variantCategoryCount);

    int getVariantValueCategoriesCount(String productCode);

    SalesUnitData findSelectedSalasUnitData(List<SalesUnitData> salesUnits, String salesUom);

    Optional<CategoryModel> getFirstVariantCategoryForVariantValueCategory(VariantValueCategoryModel variantValueCategory);

    Optional<CategoryModel> getFirstCutToLengthConfigurationCategory(ProductModel productModel);

    Optional<ProductModel> getBaseProductForCode(String productCode);

    Optional<ProductModel> getBaseProduct(ProductModel product);

    List<VariantValueCategoryModel> getAllVariantValueCategoryFromProduct(ProductModel productModel);

    Optional<VariantProductModel> singleVariantProduct(String baseProductCode);
}

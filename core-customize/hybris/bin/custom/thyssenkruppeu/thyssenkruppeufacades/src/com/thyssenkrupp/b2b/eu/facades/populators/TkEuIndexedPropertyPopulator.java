package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;

public class TkEuIndexedPropertyPopulator implements Populator<SolrIndexedPropertyModel, IndexedProperty> {
    
    @Override
    public void populate(final SolrIndexedPropertyModel source, final IndexedProperty target) throws ConversionException {
        if(null != source.getFacetSearchAndScroll() && source.getFacetSearchAndScroll()) {
        target.setFacetSearchAndScroll(source.getFacetSearchAndScroll());
        } else {
            target.setFacetSearchAndScroll(false);    
        }
    }
}

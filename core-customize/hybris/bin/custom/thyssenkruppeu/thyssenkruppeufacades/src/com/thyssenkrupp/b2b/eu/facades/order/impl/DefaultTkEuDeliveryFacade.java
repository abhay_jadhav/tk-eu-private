package com.thyssenkrupp.b2b.eu.facades.order.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountDeliveryService;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkEuB2BUnitService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuDeliveryFacade;
import com.thyssenkrupp.b2b.eu.facades.order.data.MyAccountConsignmentDataWrapper;
import com.thyssenkrupp.b2b.eu.facades.order.data.MyAccountConsignmentEntryDataWrapper;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

public class DefaultTkEuDeliveryFacade implements TkEuDeliveryFacade {

    private TkEuCustomerAccountDeliveryService customerAccountDeliveryService;
    private DefaultB2BCommerceUnitService defaultB2BCommerceUnitService;
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;
    private UserService userService;
    private DefaultTkEuB2BUnitService b2bUnitService;

    @Override
    public SearchPageData<ConsignmentData> orderDeliveriesSearchList(String searchKey, PageableData pageableData) {
        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        final SearchPageData<ConsignmentModel> orderResults = getCustomerAccountDeliveryService().orderDeliveriesSearchList(searchKey, pageableData, b2bUnits);
        return convertPageData(orderResults, getConsignmentConverter());
    }

    @Override
    public int orderDeliveriesSearchListCount(String searchKey) {
        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        return getCustomerAccountDeliveryService().orderDeliveriesSearchListCount(searchKey, b2bUnits);
    }

    @Override
    @Nullable
    public MyAccountConsignmentDataWrapper fetchDeliveryByCode(String deliveryNo) {
        Set<B2BUnitModel> b2bUnits =getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel)getUserService().getCurrentUser());
        List<ConsignmentModel> consignmentModels = getCustomerAccountDeliveryService().fetchDeliveries(deliveryNo, b2bUnits);
        List<ConsignmentData> consignments = getConsignmentConverter().convertAll(consignmentModels);
        if (CollectionUtils.isNotEmpty(consignments)) {
            ConsignmentData consignmentData = consignments.get(0);
            return wrapConsignments(consignmentData);
        }
        return null;
    }

    protected B2BUnitModel getParentUnit() {
        UserModel currentUser = getUserService().getCurrentUser();
        if (currentUser instanceof B2BCustomerModel) {
            B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) currentUser;
            B2BUnitModel defaultB2BUnit = b2BCustomerModel.getDefaultB2BUnit();
            if (defaultB2BUnit != null) {
                return defaultB2BUnit;
            }
        }
        return getDefaultB2BCommerceUnitService().getRootUnit();
    }

    protected MyAccountConsignmentDataWrapper wrapConsignments(ConsignmentData consignmentData) {
        MyAccountConsignmentDataWrapper consignmentDataWrapper = new MyAccountConsignmentDataWrapper();
        List<MyAccountConsignmentEntryDataWrapper> consignmentEntriesWrapper = new ArrayList<>();
        consignmentDataWrapper.setConsignmentData(consignmentData);
        if (consignmentData != null) {
            prepareConsignmentWrapperData(consignmentData, consignmentDataWrapper, consignmentEntriesWrapper);
        }

        return consignmentDataWrapper;
    }

    private void prepareConsignmentWrapperData(ConsignmentData consignmentData, MyAccountConsignmentDataWrapper consignmentDataWrapper, List<MyAccountConsignmentEntryDataWrapper> consignmentEntriesWrapper) {
        List<ConsignmentEntryData> consignmentEntries = ListUtils.emptyIfNull(consignmentData.getEntries());
        consignmentEntries.sort(Comparator.comparing(ConsignmentEntryData::getBatchGroupNumber));
        for (ConsignmentEntryData consignmentEntryData : consignmentEntries) {
            MyAccountConsignmentEntryDataWrapper consignmentEntryDataWrapper = consignmentEntriesWrapper.stream().filter(myAccountConsignmentEntryDataWrapper -> StringUtils.equals(myAccountConsignmentEntryDataWrapper.getBatchGroupNumber(), consignmentEntryData.getBatchGroupNumber())).findFirst().orElse(initializeMyAccountConsignmentEntryDataWrapper(consignmentEntryData));

            updateConsignmentEntryDataWrapper(consignmentEntriesWrapper, consignmentEntryData, consignmentEntryDataWrapper);
        }
        consignmentDataWrapper.setConsignmentEntriesWrapper(consignmentEntriesWrapper);
    }

    private void updateConsignmentEntryDataWrapper(List<MyAccountConsignmentEntryDataWrapper> consignmentEntriesWrapper, ConsignmentEntryData consignmentEntryData, MyAccountConsignmentEntryDataWrapper consignmentEntryDataWrapper) {
        if (consignmentEntryDataWrapper.getBatchQty() != null && consignmentEntryData.getShippedQuantity() != null) {
            Long batchQty = consignmentEntryDataWrapper.getBatchQty() + consignmentEntryData.getShippedQuantity();
            consignmentEntryDataWrapper.setBatchQty(batchQty);
            if (!consignmentEntriesWrapper.contains(consignmentEntryDataWrapper)) {
                List<ConsignmentEntryData> consignmentEntries = new ArrayList();
                consignmentEntries.add(consignmentEntryData);
                consignmentEntryDataWrapper.setConsignmentEntry(consignmentEntries);
                consignmentEntriesWrapper.add(consignmentEntryDataWrapper);
            } else {
                consignmentEntryDataWrapper.getConsignmentEntry().add(consignmentEntryData);
            }
        }
    }

    protected MyAccountConsignmentEntryDataWrapper initializeMyAccountConsignmentEntryDataWrapper(ConsignmentEntryData consignmentEntryData) {
        MyAccountConsignmentEntryDataWrapper myAccountConsignmentEntryDataWrapper = new MyAccountConsignmentEntryDataWrapper();
        myAccountConsignmentEntryDataWrapper.setBatchGroupNumber(consignmentEntryData.getBatchGroupNumber());
        myAccountConsignmentEntryDataWrapper.setOrderEntry(consignmentEntryData.getOrderEntry());
        myAccountConsignmentEntryDataWrapper.setBatchQty(0L);
        myAccountConsignmentEntryDataWrapper.setBatchUnit(consignmentEntryData.getShippedQuantityUnit());

        return myAccountConsignmentEntryDataWrapper;
    }

    protected SearchPageData<ConsignmentData> convertPageData(final SearchPageData<ConsignmentModel> source, final Converter<ConsignmentModel, ConsignmentData> converter) {
        final SearchPageData<ConsignmentData> result = new SearchPageData();
        result.setPagination(source.getPagination());
        result.setSorts(source.getSorts());
        result.setResults(Converters.convertAll(source.getResults(), converter));
        return result;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public Converter<ConsignmentModel, ConsignmentData> getConsignmentConverter() {
        return consignmentConverter;
    }

    @Required
    public void setConsignmentConverter(Converter<ConsignmentModel, ConsignmentData> consignmentConverter) {
        this.consignmentConverter = consignmentConverter;
    }

    public TkEuCustomerAccountDeliveryService getCustomerAccountDeliveryService() {
        return customerAccountDeliveryService;
    }

    @Required
    public void setCustomerAccountDeliveryService(TkEuCustomerAccountDeliveryService customerAccountDeliveryService) {
        this.customerAccountDeliveryService = customerAccountDeliveryService;
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    @Required
    public void setB2bUnitService(DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;

public class TkEuOrderAcknowledgementDetailsPopulator implements Populator<OrderModel, OrderData> {

    public void populate(final OrderModel source, final OrderData target) {
        target.setSapOrderId(source.getSapOrderId());
    }
}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.converters.Populator;

public class TkEuCartModificationPopulator implements Populator<CommerceCartModification, CartModificationData> {

    @Override
    public void populate(final CommerceCartModification source, final CartModificationData target){
        target.setInvalidCouponList(source.getInvalidCouponList());
    }
}

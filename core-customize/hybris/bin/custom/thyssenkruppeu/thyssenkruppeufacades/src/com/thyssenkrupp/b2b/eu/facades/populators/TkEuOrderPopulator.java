package com.thyssenkrupp.b2b.eu.facades.populators;

import java.text.DateFormat;
import java.util.Locale;

import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;

public class TkEuOrderPopulator implements Populator<AbstractOrderModel, OrderData> {

    private Converter<AddressModel, AddressData> addressConverter;
    private TkEnumUtils enumUtils;
    private I18NService i18nService;

    @Override
    public void populate(final AbstractOrderModel source, final OrderData target) {
        populateFormattedAttributes(source, target);
        addShippingNotes(source, target);
        addBillingAddress(source, target);
        addB2bUnitTerms(source, target);
    }


    

    protected void addBillingAddress(final AbstractOrderModel orderModel, final OrderData cartData) {
        AddressModel billingAddress = orderModel.getPaymentAddress();
        if (billingAddress == null && orderModel.getPaymentInfo() != null) {
            billingAddress = orderModel.getPaymentInfo().getBillingAddress();
        }
        if (billingAddress != null) {
            cartData.setBillingAddress(getAddressConverter().convert(billingAddress));
        }
    }

    protected void addShippingNotes(final AbstractOrderModel orderModel, final OrderData orderData) {
        String notes = "";
        if (orderModel != null && orderModel.getCustomerShippingNote() != null) {
            notes = orderModel.getCustomerShippingNote();
        }
        orderData.setCustomerShippingNotes(notes);
    }

    protected void addB2bUnitTerms(final AbstractOrderModel orderModel, final OrderData orderData) {
        if (orderModel != null) {
            orderData.setShippingCondition(getEnumUtils().getNameFromEnumCode(orderModel.getShippingcondition()));
            orderData.setPaymentTerms(getEnumUtils().getNameFromEnumCode(orderModel.getPaymentterms()));
            orderData.setIncoTerms(getEnumUtils().getNameFromEnumCode(orderModel.getIncoterms()));
        }
    }

    private void populateFormattedAttributes(final AbstractOrderModel source, final OrderData target) {
        if (source.getDate() != null) {

            DateFormat dateFormatter;
            Locale currentLocale = i18nService.getCurrentLocale();
            dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, currentLocale);
            String formattedDate = dateFormatter.format(source.getDate());
            target.setFormattedSubmittedDate(formattedDate);
        }
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public Converter<AddressModel, AddressData> getAddressConverter() {
        return addressConverter;
    }

    public void setAddressConverter(Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    public TkEnumUtils getEnumUtils() {
        return enumUtils;
    }

    public void setEnumUtils(TkEnumUtils enumUtils) {
        this.enumUtils = enumUtils;
    }

    
}

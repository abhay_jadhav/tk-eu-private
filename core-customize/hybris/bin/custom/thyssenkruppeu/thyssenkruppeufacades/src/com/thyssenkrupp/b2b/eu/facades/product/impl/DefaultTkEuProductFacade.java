package com.thyssenkrupp.b2b.eu.facades.product.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.C2L_CONFIGURATION_CATEGORY;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductFacade;
import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import com.thyssenkrupp.b2b.eu.facades.product.data.VariantValueDisplayFlag;
import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.TkEuProductSearchFacade;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductVariantFacade;
import de.hybris.platform.commerceservices.product.data.ReferenceData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

public class DefaultTkEuProductFacade extends DefaultProductVariantFacade implements TkEuProductFacade {

    private CommerceStockService      commerceStockService;
    private UrlResolver<ProductModel> productModelUrlResolver;
    private TkEuProductSearchFacade   productSearchFacade;

    private TkB2bCategoryService                                                       tkB2bCategoryService;
    private Converter<AbstractOrderEntryProductInfoModel, List<ConfigurationInfoData>> tkEuProductConfigurationConverter;

    @Override
    public boolean isBaseProduct(String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        return !(productModel instanceof VariantProductModel);
    }

    @Override
    public boolean isPurchasableProduct(final String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        boolean isPurchasable = true;
        if (productModel == null || (productModel.getVariantType() != null && CollectionUtils.isEmpty(productModel.getVariants()))) {
            isPurchasable = false;
        }
        return isPurchasable;
    }

    @Override
    public void populateActiveVariantCategoryValues(final List<VariantProductModel> variants, ProductData productData) {
        if (CollectionUtils.isNotEmpty(variants)) {
            for (final VariantProductModel variant : variants) {
                setActiveStatusOnVariantCategoryValues(variant, productData);
            }
        }
    }

    @Override
    public List<VariantProductModel> getAllVariantsFilteredBySelectedVvc(final ProductModel productModel,final Map<String, String> selectedVvcMap){
        return emptyIfNull(productModel.getVariants()).stream().filter(model -> isValidVariant(model, selectedVvcMap)).collect(Collectors.toList());
    }

    @Override
    public Optional<CategoryModel> getFirstVariantCategoryForVariantValueCategory(final VariantValueCategoryModel variantValueCategory) {
        validateParameterNotNullStandardMessage("VariantValueCategoryModel", variantValueCategory);
        return emptyIfNull(variantValueCategory.getSupercategories()).stream().filter(VariantCategoryModel.class::isInstance).findFirst();
    }

    @Override
    public List<VariantValueCategoryModel> getAllVariantValueCategoryFromProduct(final ProductModel productModel) {
        validateParameterNotNullStandardMessage("ProductModel", productModel);
        return emptyIfNull(productModel.getSupercategories()).stream().filter(VariantValueCategoryModel.class::isInstance)
          .map(VariantValueCategoryModel.class::cast).collect(Collectors.toList());
    }

    @Override
    public Optional<CategoryModel> getFirstCutToLengthConfigurationCategory(final ProductModel productModel) {
        validateParameterNotNullStandardMessage("ProductModel", productModel);
        return emptyIfNull(productModel.getSupercategories()).stream().filter(ConfigurationCategoryModel.class::isInstance)
            .filter(categoryModel -> C2L_CONFIGURATION_CATEGORY.equals(categoryModel.getCode())).findFirst();
    }

    @Override
    public Optional<ProductModel> getBaseProductForCode(final String productCode) {
        if (StringUtils.isNotEmpty(productCode)) {
            final ProductModel model = getProductService().getProductForCode(productCode);
            return getBaseProduct(model);
        }
        return Optional.empty();
    }

    @Override
    public Optional<ProductModel> getBaseProduct(final ProductModel product) {
        if (VariantProductModel.class.isInstance(product)) {
            return Optional.ofNullable(VariantProductModel.class.cast(product).getBaseProduct());
        }
        return Optional.ofNullable(product);
    }

    private boolean isValidVariant(VariantProductModel variant, Map<String, String> selectionMap) {
        return CollectionUtils.isNotEmpty(variant.getSupercategories()) && doesVariantContainAllValueCategories(variant.getSupercategories(), filterNonEmptyValues(selectionMap.values()));
    }

    private Collection<String> filterNonEmptyValues(final Collection<String> values) {
        return values.stream().filter(item -> StringUtils.isNotEmpty(item)).collect(Collectors.toList());
    }

    boolean doesVariantContainAllValueCategories(Collection<CategoryModel> supercategories, Collection<String> variantCategoryValues) {
        boolean isPresent = true;
        for (String value : variantCategoryValues) {
            if (supercategories.stream().noneMatch(cat -> StringUtils.equals(cat.getCode(), value) && cat instanceof VariantValueCategoryModel)) {
                isPresent = false;
                break;
            }
        }
        return isPresent;
    }

    private void setActiveStatusOnVariantCategoryValues(VariantProductModel variant, ProductData productData) {
        for (final CategoryModel superCategory : variant.getSupercategories()) {
            if (superCategory instanceof VariantValueCategoryModel) {
                setActiveFlag(variant, productData, superCategory.getCode());
            }
        }
    }

    private void setActiveFlag(VariantProductModel variant, ProductData productData, String name) {
        for (VariantMatrixElementData data : productData.getUnselectedVariantMatrixList()) {
            if (StringUtils.equals(data.getVariantValueCategory().getCode(), name)) {
                data.getVariantValueCategory().setDisplayStatus(VariantValueDisplayFlag.ACTIVE.toString());
                data.getVariantOption().setUrl(getProductModelUrlResolver().resolve(variant));
            }
        }
    }

    @Override
    public boolean isElementStatusActive(ProductData productData, String name) {
        for (VariantMatrixElementData data : productData.getUnselectedVariantMatrixList()) {
            if (StringUtils.equals(data.getVariantValueCategory().getCode(), name)) {
                return StringUtils.equals(data.getVariantValueCategory().getDisplayStatus(), VariantValueDisplayFlag.ACTIVE.toString());
            }
        }
        return false;
    }

    @Override
    public String getBaseProductCodeForVariant(String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        if (productModel instanceof ERPVariantProductModel) {
            return ((ERPVariantProductModel) productModel).getBaseProduct().getCode();
        } else {
            return productModel.getCode();
        }
    }

    @Override
    public boolean isVariantSelected(Map<String, String> variantSelectionMap, int variantValueCategoryCount) {
        long selectedValueCount = filterNonEmptyValues(variantSelectionMap.values()).size();
        return selectedValueCount != 0 && selectedValueCount == variantValueCategoryCount;
    }

    @Override
    public int getVariantValueCategoriesCount(final String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        final String catalogId = productModel.getCatalogVersion().getCatalog().getId();
        int tradeLengthValueCategoryCount = 0;
        int variantValueCategoryCount = 0;

        for (final CategoryModel categoryModel : productModel.getSupercategories()) {
            if (isTradeLengthVariantValueCategory(categoryModel)) {
                tradeLengthValueCategoryCount = 1;
            }else if (isVariantValueCategory(categoryModel, catalogId)) {
                variantValueCategoryCount++;
            }
        }
        return variantValueCategoryCount + tradeLengthValueCategoryCount;
    }

    protected boolean isTradeLengthVariantValueCategory(final CategoryModel categoryModel) {
        return StringUtils.containsIgnoreCase(categoryModel.getCode(), ThyssenkruppeufacadesConstants.TRADE_LENGTH_IDENTIFIER) && (categoryModel instanceof VariantValueCategoryModel);
    }

    protected boolean isVariantValueCategory(final CategoryModel categoryModel, final String catalogId) {
        return categoryModel.getCatalogVersion().getCatalog().getId().equals(catalogId) && (categoryModel instanceof VariantValueCategoryModel);
    }

    @Override
    public List<ProductReferenceData> getProductReferencesForCode(final String code,
      final List<ProductReferenceTypeEnum> referenceTypes, final List<ProductOption> options, final Integer limit) {
        final List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> references = getCommerceProductReferenceService().getProductReferencesForCode(code, referenceTypes, limit);

        final List<ProductReferenceData> result = new ArrayList<ProductReferenceData>();
        ArrayList<String> productCodeList = new ArrayList<String>();

        for (final ReferenceData<ProductReferenceTypeEnum, ProductModel> reference : references) {
            final ProductReferenceData productReferenceData = getReferenceDataProductReferenceConverter().convert(reference);
            getReferenceProductConfiguredPopulator().populate(reference.getTarget(), productReferenceData.getTarget(), options);
            result.add(productReferenceData);
            productCodeList.add(reference.getTarget().getCode());
        }
        ProductSearchPageData solrResponse = getReferenceProductAttributeFromSolr(productCodeList);
        setReferenceProductAttributes(solrResponse, result);
        return result;
    }

    private void setReferenceProductAttributes(ProductSearchPageData solrResponse, List<ProductReferenceData> productReferences) {
        if (solrResponse != null && CollectionUtils.isNotEmpty(solrResponse.getResults())) {
            final List<ProductData> searchResults = solrResponse.getResults();
            for (ProductReferenceData productReference : productReferences) {
                for (ProductData searchResult : searchResults) {
                    if (productReference.getTarget().getCode().equalsIgnoreCase(searchResult.getCode())) {
                        productReference.setTkEuPLPDisplayList(searchResult.getTkEuPLPDisplayList());
                    }
                }
            }
        }
    }

    @Override
    public Optional<VariantProductModel> singleVariantProduct(String baseProductCode) {
        final ProductModel productModel = getProductService().getProductForCode(baseProductCode);
        if (CollectionUtils.size(productModel.getVariants()) == 1) {
            return productModel.getVariants().stream().findFirst();
        }
        return Optional.empty();
    }

    private ProductSearchPageData getReferenceProductAttributeFromSolr(final List productCodeList) {
        return productSearchFacade.searchByProductCodes(productCodeList);
    }

    private void setTkEuPLPDisplayList(List<ProductReferenceData> productReferences, List<ProductData> searchResults) {

    }

    @Nullable
    @Override
    public SalesUnitData findSelectedSalasUnitData(@NotNull List<SalesUnitData> salesUnits, @Nullable String salesUom) {
        if (StringUtils.isNotEmpty(salesUom)) {
            return salesUnits.stream().filter(salesUnitData -> salesUom.equals(salesUnitData.getCode())).findFirst().orElse(null);
        } else {
            return salesUnits.stream().filter(salesUnitData -> salesUnitData.isIsDefaultUnit()).findFirst().orElse(null);
        }
    }

    protected CommerceStockService getCommerceStockService() {
        return commerceStockService;
    }

    @Required
    public void setCommerceStockService(final CommerceStockService commerceStockService) {
        this.commerceStockService = commerceStockService;
    }

    public UrlResolver<ProductModel> getProductModelUrlResolver() {
        return productModelUrlResolver;
    }

    @Required
    public void setProductModelUrlResolver(final UrlResolver<ProductModel> productModelUrlResolver) {
        this.productModelUrlResolver = productModelUrlResolver;
    }

    public TkB2bCategoryService getTkB2bCategoryService() {
        return tkB2bCategoryService;
    }

    public void setTkB2bCategoryService(TkB2bCategoryService tkB2bCategoryService) {
        this.tkB2bCategoryService = tkB2bCategoryService;
    }

    @Override
    protected Converter<AbstractOrderEntryProductInfoModel, List<ConfigurationInfoData>> getProductConfigurationConverter() {
        return tkEuProductConfigurationConverter;
    }

    @Required
    public void setTkEuProductConfigurationConverter(Converter<AbstractOrderEntryProductInfoModel, List<ConfigurationInfoData>> tkEuProductConfigurationConverter) {
        this.tkEuProductConfigurationConverter = tkEuProductConfigurationConverter;
    }

    public TkEuProductSearchFacade getProductSearchFacade() {
        return productSearchFacade;
    }

    public void setProductSearchFacade(TkEuProductSearchFacade productSearchFacade) {
        this.productSearchFacade = productSearchFacade;
    }
}

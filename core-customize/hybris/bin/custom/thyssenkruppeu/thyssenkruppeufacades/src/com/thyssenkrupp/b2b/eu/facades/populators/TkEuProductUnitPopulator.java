package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.facades.product.data.UnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatMtkName;

public class TkEuProductUnitPopulator implements Populator<UnitModel, UnitData> {

    @Override
    public void populate(UnitModel unitModel, UnitData unitData) throws ConversionException {
        unitData.setCode(unitModel.getCode());
        unitData.setName(formatMtkName(unitModel.getName()));
        unitData.setSapCode(unitModel.getSapCode());
        unitData.setUnitType(unitModel.getUnitType());
        unitData.setZilliantUnit(unitModel.getZilliantUnit());
        unitData.setConversion(unitModel.getConversion());
    }
}

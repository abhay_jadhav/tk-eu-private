package com.thyssenkrupp.b2b.eu.facades.populators;


import java.util.Objects;

import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuCartPopulator implements Populator<CartModel, CartData> {

    private Converter<AddressModel, AddressData> addressConverter;
    private TkEnumUtils enumUtils;


    @Override
    public void populate(final CartModel source, final CartData target) {
        if (!Objects.isNull(source) && null != source) {
        addShippingNotes(source, target);
        addBillingAddress(source, target);
        addB2bUnitTerms(source, target);
        addOrderName(source, target);
        addPoNumber(source, target);
        target.setModifiedTime(source.getModifiedtime());
        }
    }
    

        
    private void addB2bUnitTerms(CartModel orderModel, CartData cartData) {
        if (orderModel != null) {
            cartData.setShippingCondition(getEnumUtils().getNameFromEnumCode(orderModel.getShippingcondition()));
            cartData.setPaymentTerms(getEnumUtils().getNameFromEnumCode(orderModel.getPaymentterms()));
            cartData.setIncoTerms(getEnumUtils().getNameFromEnumCode(orderModel.getIncoterms()));
        }
    }

    protected void addShippingNotes(final CartModel orderModel, final CartData cartData) {
        String notes = "";
        if (orderModel != null && orderModel.getCustomerShippingNote() != null) {
            notes = orderModel.getCustomerShippingNote();
        }
        cartData.setCustomerShippingNotes(notes);
    }

    protected void addBillingAddress(final CartModel orderModel, final CartData cartData) {
        if (orderModel.getPaymentInfo() != null) {
            final AddressModel billingAddress = orderModel.getPaymentInfo().getBillingAddress();
            if (billingAddress != null) {
                cartData.setBillingAddress(getAddressConverter().convert(billingAddress));
            }
        }
    }

    protected void addOrderName(final CartModel cartModel, final CartData cartData) {
        if (cartModel != null && cartModel.getName() != null) {
            cartData.setName(cartModel.getName());
        }
    }

    protected void addPoNumber(final CartModel cartModel, final CartData cartData) {
        if (cartModel != null && cartModel.getPunchOutOrder() != null) {
            cartData.setPurchaseOrderNumber(cartModel.getPurchaseOrderNumber());
        }
    }


    public Converter<AddressModel, AddressData> getAddressConverter() {
        return addressConverter;
    }

    public void setAddressConverter(Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    public TkEnumUtils getEnumUtils() {
        return enumUtils;
    }

    public void setEnumUtils(TkEnumUtils enumUtils) {
        this.enumUtils = enumUtils;
    }
    
}

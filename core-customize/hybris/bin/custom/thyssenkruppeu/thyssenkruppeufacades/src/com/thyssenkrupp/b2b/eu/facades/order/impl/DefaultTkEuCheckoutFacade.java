package com.thyssenkrupp.b2b.eu.facades.order.impl;

import static com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants.TRADE_LENGTH_IDENTIFIER;
import static com.thyssenkrupp.b2b.eu.facades.predicate.TkEuAddressModelPredicates.isBillingAddress;
import static com.thyssenkrupp.b2b.eu.facades.predicate.TkEuAddressModelPredicates.isContactAddress;
import static com.thyssenkrupp.b2b.eu.facades.predicate.TkEuAddressModelPredicates.isShippingAddress;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKTRADELENGTH;
import static de.hybris.platform.util.localization.Localization.getLocalizedString;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang.BooleanUtils.toBoolean;
import static org.apache.commons.lang.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang.StringUtils.equalsIgnoreCase;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bOrderService;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkB2bProductService;

import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCartService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BCommentData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BReplenishmentRecurrenceEnum;
import de.hybris.platform.b2bacceleratorfacades.order.data.TriggerData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.product.ProductService;

public class DefaultTkEuCheckoutFacade extends DefaultB2BCheckoutFacade implements TkEuCheckoutFacade {

    private static final String CART_CHECKOUT_COSTCENTER_PAYMENTTYPE_INVALID   = "cart.costcenter.paymenttypeInvalid";
    private static final String CART_CHECKOUT_PAYMENTTYPE_INVALID              = "cart.paymenttype.invalid";
    private static final String CART_CHECKOUT_DELIVERYADDRESS_INVALID          = "cart.deliveryAddress.invalid";
    private static final String CART_CHECKOUT_DELIVERYMODE_INVALID             = "cart.deliveryMode.invalid";
    private static final String CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED       = "cart.transation.notAuthorized";
    private static final String CART_CHECKOUT_PAYMENTINFO_EMPTY                = "cart.paymentInfo.empty";
    private static final String CART_CHECKOUT_NOT_CALCULATED                   = "cart.not.calculated";
    private static final String CART_CHECKOUT_TERM_UNCHECKED                   = "cart.term.unchecked";
    private static final String CART_CHECKOUT_NO_QUOTE_DESCRIPTION             = "cart.no.quote.description";
    private static final String CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED = "cart.quote.requirements.not.satisfied";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE       = "cart.replenishment.no.startdate";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY       = "cart.replenishment.no.frequency";
    private static final String VARIANT_MAP_KEY_CODE                           = "code";
    private static final String VARIANT_MAP_KEY_VALUE                          = "value";
    private static final String VARIANT_MAP_KEY_NAME                           = "name";
    private static final Logger LOG                                            = LoggerFactory.getLogger(DefaultTkEuCheckoutFacade.class);

    private CustomerFacade customerFacade;

    private B2BUnitFacade b2bUnitFacade;

    private TkEnumUtils enumUtils;

    private DefaultTkB2bProductService b2bProductService;
    private ProductService             productService;

    private B2BCommerceUnitService b2bCommerceUnitService;
    
    private TkEuB2bOrderService tkEuB2bOrderService;

    @Override
    public CartData updateCheckoutCart(CartData cartData) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return null;
        }
        updateNameAndDescription(cartData, cartModel);
        if (cartData.getPaymentType() != null) {
            updatePaymentType(cartData, cartModel);
        }
        if (cartData.getCostCenter() != null) {
            setCostCenterForCart(cartData.getCostCenter().getCode(), cartModel);
        }
        if (cartData.getPurchaseOrderNumber() != null) {
            cartModel.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
        }
        if (cartData.getBillingAddress() != null) {
            setBillingAddress(cartData.getBillingAddress(), cartModel);
        }
        getModelService().save(cartModel);
        return getCheckoutCart();
    }

    @Override
    public void saveNewShippingAddress(AddressModel addressModel) {
        final CartModel cartModel = getCart();
        cartModel.setDeliveryAddress(addressModel);
        getModelService().save(cartModel);
        getModelService().refresh(cartModel);
    }

    @Override
    public void setCartPaymentType() {
        final CartModel cartModel = getCart();
        //cartModel.setPaymentType(null);
        getModelService().save(cartModel);
        getModelService().refresh(cartModel);
    }

    @Override
    public CartData getNewShippingAddress() {

        final CartData cartData = getCartFacade().getSessionCart();
        if (cartData != null) {
            cartData.setNewShippingAddress(getNewDeliveryAddress());
            cartData.setDeliveryAddress(getNewDeliveryAddress());
        }
        return cartData;
    }

    @Override
    public String checkShippingTerms(CartData cartData) {
        if (cartData.getShippingCondition().isEmpty()) {
            return getEnumUtils().getNameFromEnumCode(getB2bUnitFacade().getParentUnit().getShippingCondition());
        } else {
            return cartData.getShippingCondition();
        }
    }

    @Override
    public String checkPaymentTerms(CartData cartData) {
        if (cartData.getPaymentTerms().isEmpty()) {
            return getEnumUtils().getNameFromEnumCode(getB2bUnitFacade().getParentUnit().getPaymentTerms());
        } else {
            return cartData.getPaymentTerms();
        }
    }

    @Override
    public String checkIncoterms(CartData cartData) {
        if (cartData.getIncoTerms().isEmpty()) {
            return getEnumUtils().getNameFromEnumCode(getB2bUnitFacade().getParentUnit().getIncoTerms());
        } else {
            return cartData.getIncoTerms();
        }
    }

    protected AddressData getNewDeliveryAddress() {
        final CartModel cart = getCart();
        if (cart != null) {
            final AddressModel deliveryAddress = cart.getDeliveryAddress();
            if (deliveryAddress != null) {
                return getAddressConverter().convert(deliveryAddress);
            }
        }
        return null;
    }

    protected AddressData getBillingAddress() {
        final CartModel cart = getCart();
        if (cart != null) {
            final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
            if (paymentInfo != null && paymentInfo.getBillingAddress() != null) {
                getAddressConverter().convert(paymentInfo.getBillingAddress());
            }
        }
        return null;
    }

    protected void updatePaymentType(CartData cartData, CartModel cartModel) {
        final String newPaymentTypeCode = cartData.getPaymentType().getCode();
        if (cartModel.getPaymentType() == null || !newPaymentTypeCode.equalsIgnoreCase(cartModel.getPaymentType().getCode())) {
            cartModel.setDeliveryMode(null);
            cartModel.setPaymentInfo(null);
            setPaymentTypeForCart(newPaymentTypeCode, cartModel);
        }
    }

    @Override
    protected void setPaymentTypeForCart(final String paymentType, final CartModel cartModel) {
        cartModel.setPaymentType(CheckoutPaymentType.valueOf(paymentType));
        if (CheckoutPaymentType.ACCOUNT.getCode().equals(paymentType) && cartModel.getPaymentInfo() == null) {
            cartModel.setPaymentInfo(getCommerceCartService().createInvoicePaymentInfo(cartModel));
        }

        getCommerceCartService().calculateCartForPaymentTypeChange(cartModel);
    }

    private void updateNameAndDescription(CartData cartData, CartModel cartModel) {
        if (cartData.getName() != null) {
            cartModel.setName(cartData.getName());
        }
        if (cartData.getDescription() != null) {
            cartModel.setDescription(cartData.getDescription());
        }
    }

    protected void setBillingAddress(final AddressData billingAddressData, final CartModel cartModel) {
        PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        if (paymentInfo != null) {
            paymentInfo.setBillingAddress(getB2bCommerceUnitService().getAddressForCode(getB2BUnitModel(), billingAddressData.getId()));
        }
    }

    @Override
    public void setBillingDetails(String orderName, String poNumber) {

        final CartModel cartModel = getCart();
        cartModel.setName(orderName);
        cartModel.setPurchaseOrderNumber(poNumber);
        getModelService().save(cartModel);
    }

    @Override
    public boolean hasNoDeliveryAddress() {
        final CartData cartData = getNewShippingAddress();
        return hasShippingItems() && (cartData == null || cartData.getDeliveryAddress() == null);
    }

    @Override
    public <T extends AbstractOrderData> T placeOrder(final PlaceOrderData placeOrderData) throws InvalidCartException {
        // term must be checked
        if (!placeOrderData.getTermsCheck().equals(Boolean.TRUE)) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED));
        }

        // for CARD type, transaction must be authorized before placing order
        final boolean isCardtPaymentType = CheckoutPaymentType.CARD.getCode().equals(getCart().getPaymentType().getCode());
        isCardtPaymentType(isCardtPaymentType);

        if (isValidCheckoutCart(placeOrderData)) {
            // validate quote negotiation
            validateQuoteNegotiation(placeOrderData);

            // validate replenishment
            final T triggerData = validateReplenishment(placeOrderData);
            if (triggerData != null)
                return triggerData;

            final CartModel cartModel = getCart();
            if (cartModel != null) {
                cartModel.setUser(getCurrentUserForCheckout());
                getCartService().setSessionCart(cartModel);
            }
            return (T) super.placeOrder();
        }

        return null;
    }

    @Override
    public AddressData getPrimaryShippingAddress() {

        List<AddressData> shippingAddress = getCustomerB2bUnitShippingAddress();
        if (!CollectionUtils.isEmpty(shippingAddress)) {
            return shippingAddress.iterator().next();
        }
        return null;
    }

    @Override
    public List<AddressData> getB2bUnitContactAddress() {
        final B2BUnitModel b2BUnitModel = getCustomerB2BUnitModel();
        return getAddressForB2BUnit(b2BUnitModel, isContactAddress());
    }

    private <T extends AbstractOrderData> T validateReplenishment(PlaceOrderData placeOrderData) {
        if (placeOrderData.getReplenishmentOrder() != null && placeOrderData.getReplenishmentOrder().equals(Boolean.TRUE)) {
            if (placeOrderData.getReplenishmentStartDate() == null) {
                throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE));
            }

            if (placeOrderData.getReplenishmentRecurrence().equals(B2BReplenishmentRecurrenceEnum.WEEKLY)
                && CollectionUtils.isEmpty(placeOrderData.getNDaysOfWeek())) {
                throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY));
            }

            final TriggerData triggerData = new TriggerData();
            populateTriggerDataFromPlaceOrderData(placeOrderData, triggerData);

            return (T) scheduleOrder(triggerData);
        }
        return null;
    }

    private void validateQuoteNegotiation(PlaceOrderData placeOrderData) {
        if (placeOrderData.getNegotiateQuote() != null && placeOrderData.getNegotiateQuote().equals(Boolean.TRUE)) {
            if (StringUtils.isBlank(placeOrderData.getQuoteRequestDescription())) {
                throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NO_QUOTE_DESCRIPTION));
            } else {
                final B2BCommentData b2BComment = new B2BCommentData();
                b2BComment.setComment(placeOrderData.getQuoteRequestDescription());

                final CartData cartData = new CartData();
                cartData.setB2BComment(b2BComment);

                updateCheckoutCart(cartData);
            }
        }
    }

    private void isCardtPaymentType(boolean isCardtPaymentType) {
        if (isCardtPaymentType) {
            final List<PaymentTransactionModel> transactions = getCart().getPaymentTransactions();
            boolean authorized = false;
            for (final PaymentTransactionModel transaction : transactions) {
                for (final PaymentTransactionEntryModel entry : transaction.getEntries()) {
                    if (entry.getType().equals(PaymentTransactionType.AUTHORIZATION)
                        && TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus())) {
                        authorized = true;
                        break;
                    }
                }
            }
            if (!authorized) {
                throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED));
            }
        }
    }

    @Override
    protected boolean isValidCheckoutCart(final PlaceOrderData placeOrderData) {
        final CartData cartData = getCheckoutCart();
        final boolean valid = true;

        if (!cartData.isCalculated()) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NOT_CALCULATED));
        }

        validateDeliveryAddress(cartData);

        final boolean accountPaymentType = CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode());
        if (!accountPaymentType && cartData.getPaymentInfo() == null) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTINFO_EMPTY));
        }

        if (Boolean.TRUE.equals(placeOrderData.getNegotiateQuote()) && !cartData.getQuoteAllowed()) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED));
        }

        return valid;
    }

    private void validateDeliveryAddress(CartData cartData) {
        if (cartData.getDeliveryAddress() == null) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYADDRESS_INVALID));
        }

        if (cartData.getDeliveryMode() == null) {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYMODE_INVALID));
        }
    }

    @Override
    public CartData getCheckoutCart() {
        updateSessionCart(getCart());
        final CartData cartData = getCartFacade().getSessionCart();
        if (cartData != null) {
            cartData.setDeliveryAddress(getNewDeliveryAddress());
            cartData.setDeliveryMode(getDeliveryMode());
            cartData.setPaymentInfo(getPaymentDetails());
        }
        if (cartData != null) {
            cartData.setBillingAddress(getBillingAddress());
        }
        return cartData;
    }

    private void updateSessionCart(CartModel cart) {
        getCartService().setSessionCart(cart);
    }

    @Override
    public List<AddressData> getCustomerB2bUnitShippingAddress() {
        final B2BUnitModel b2BUnitModel = getCustomerB2BUnitModel();
        return getAddressForB2BUnit(b2BUnitModel, isShippingAddress());
    }

    @Override
    public List<AddressData> getCustomerB2bUnitBillingAddress() {
        final B2BUnitModel b2BUnitModel = getCustomerB2BUnitModel();
        return getAddressForB2BUnit(b2BUnitModel, isBillingAddress());
    }

    protected B2BUnitModel getCustomerB2BUnitModel() {
        final CustomerData b2bCustomerData = getCustomerFacade().getCurrentCustomer();
        final B2BUnitData b2bUnitData = b2bCustomerData.getUnit();
        return getB2bCommerceUnitService().getUnitForUid(b2bUnitData.getUid());
    }

    @Override
    public List<AddressData> getB2bUnitCompanyAddress() {
        final B2BUnitModel b2BUnitModel = getB2bCommerceUnitService().getRootUnit();
        return getAddressForparentB2BUnit(b2BUnitModel);
    }

    protected List<AddressData> getAddressForparentB2BUnit(final B2BUnitModel b2bUnit) {
        final Collection<AddressModel> b2bUnitAddresses = b2bUnit.getAddresses();
        if (CollectionUtils.isNotEmpty(b2bUnitAddresses)) {
            final List<AddressModel> addresses = b2bUnitAddresses.stream().filter(address->address.getConsignmentAddress()==null || !address.getConsignmentAddress()).collect(Collectors.toList());
            return getAddressConverter().convertAll(addresses);
        }
        return Collections.emptyList();
    }

    @Override
    public void setTradeLengthAttributes(OrderEntryData orderEntryData) {
        List<ConfigurationInfoData> configurations = orderEntryData.getConfigurationInfos();
        ProductData product = orderEntryData.getProduct();
        List<Map<String, String>> variantValues = product.getVariantValues();
        if (CollectionUtils.isNotEmpty(configurations)) {
            for (ConfigurationInfoData data : configurations) {
                reduceVariantValueListWithSelectedTradeLength(variantValues, data);
            }
        }
        product.setVariantValues(variantValues);
        orderEntryData.setProduct(product);
    }

    private void reduceVariantValueListWithSelectedTradeLength(List<Map<String, String>> variantValues, ConfigurationInfoData data) {
        if (data.getConfiguratorType() == TKTRADELENGTH) {
            if (toBoolean(data.getConfigurationValue())) {
                setTradeLengthValues(variantValues, data);
            } else {
                emptyIfNull(variantValues).removeIf(matchTradeLengthInfoMap(data));
            }
        }
    }

    private Predicate<Map<String, String>> matchTradeLengthInfoMap(ConfigurationInfoData data) {
        return variantValue -> containsIgnoreCase(variantValue.get(VARIANT_MAP_KEY_CODE), TRADE_LENGTH_IDENTIFIER) && equalsIgnoreCase(variantValue.get(VARIANT_MAP_KEY_VALUE), data.getConfigurationLabel());
    }

    @Override
    public void setSawingAttributes(OrderEntryData target) {
        List<ConfigurationInfoData> configurations = target.getConfigurationInfos();
        if (CollectionUtils.isNotEmpty(configurations)) {
            for (ConfigurationInfoData data : configurations) {
                if (data.getConfiguratorType() == CUTTOLENGTH_PRODINFO && toBoolean(data.getConfigurationValue())) {
                    populateCutToLengthValue(target, data);
                }
            }
        }
    }

    private void populateCutToLengthValue(OrderEntryData target, ConfigurationInfoData data) {
        ProductModel productModel = getProductService().getProductForCode(target.getProduct().getCode());
        List<Map<String, String>> productInfo = emptyIfNull(getB2bProductService().fetchProductInfo(productModel));

        for (Map<String, String> variantValue : productInfo) {
            if (containsIgnoreCase(variantValue.get(VARIANT_MAP_KEY_CODE), TRADE_LENGTH_IDENTIFIER)) {
                ProductData product = target.getProduct();
                addConfiguationMap(data, variantValue, product);
                break;
            }
        }
    }

    private void addConfiguationMap(ConfigurationInfoData data, Map<String, String> variantValue, ProductData product) {
        List<Map<String, String>> variantValues = product.getVariantValues();
        Map<String, String> map = new HashMap<>();
        map.put(VARIANT_MAP_KEY_VALUE, data.getConfigurationLabel());
        map.put(VARIANT_MAP_KEY_CODE, data.getConfiguratorType().getCode());
        map.put(VARIANT_MAP_KEY_NAME, variantValue.get(VARIANT_MAP_KEY_NAME));
        variantValues.add(map);
    }

    protected void setTradeLengthValues(List<Map<String, String>> variantValues, ConfigurationInfoData data) {
        if (CollectionUtils.isNotEmpty(variantValues)) {
            for (Map<String, String> variantValue : variantValues) {
                if (containsIgnoreCase(variantValue.get(VARIANT_MAP_KEY_CODE), TRADE_LENGTH_IDENTIFIER)) {
                    variantValue.put(VARIANT_MAP_KEY_VALUE, data.getConfigurationLabel());
                    break;
                }
            }
        }
    }

    @Override
    public void createCartFromOrder(String orderCode) {
        final OrderModel order = getTkEuB2bOrderService().getInitialOrderForCode(orderCode);
        validateUserUnit(order);

        AddressModel originalDeliveryAddress = getModelService().clone(order.getDeliveryAddress());
//        if (originalDeliveryAddress != null) {
//            originalDeliveryAddress = originalDeliveryAddress.getOriginal();
//        }

        AddressModel originalPaymentAddress = getModelService().clone(order.getPaymentAddress());
//        if (originalPaymentAddress != null) {
//            originalPaymentAddress = originalPaymentAddress.getOriginal();
//        }

        final PaymentInfoModel paymentInfoModel = getPaymentInfoModelForClonedCart(order);

        // detach the order and null the attribute that is not available on the cart to avoid cloning errors.
        detachAndResetCartAttributes(order);

        // reset quote related fields
        resetQuoteRelatedFields(order);

        // create cart from the order object.
        createCartFromOrder(order, originalDeliveryAddress, originalPaymentAddress, paymentInfoModel);
    }

    
    private void createCartFromOrder(OrderModel order, AddressModel originalDeliveryAddress, AddressModel originalPaymentAddress, PaymentInfoModel paymentInfoModel) {
        final CartModel cart = this.<B2BCartService>getCartService().createCartFromAbstractOrder(order);
        if (cart != null) {
            cart.setDeliveryAddress(originalDeliveryAddress);
            cart.setPaymentAddress(originalPaymentAddress);
            cart.setPaymentInfo(paymentInfoModel);
            cart.setUser(getUserService().getCurrentUser());
            getModelService().save(cart);
            getCartService().removeSessionCart();
            getCommerceCartService().calculateCart(cart);
            getModelService().refresh(cart);
            getCartService().setSessionCart(cart);
        }
    }

    private void detachAndResetCartAttributes(OrderModel order) {
        getModelService().detach(order);
        order.setSchedulingCronJob(null);
        order.setOriginalVersion(null);
        order.setStatus(OrderStatus.CREATED);
        order.setPaymentAddress(null);
        order.setPaymentType(null);
        order.setDeliveryAddress(null);
        order.setHistoryEntries(null);
        order.setPaymentInfo(null);
        order.setB2bcomments(Collections.emptyList());
        order.setWorkflow(null);
        order.setPermissionResults(Collections.emptyList());
        order.setExhaustedApprovers(Collections.emptySet());
        order.setExportStatus(null);
    }

    private void validateUserUnit(OrderModel order) {
        if (order != null) {
            B2BUnitModel orderUserUnit = getB2BUnitForUser(order.getUser());
            B2BUnitModel currentUserUnit = getB2BUnitForUser(getUserService().getCurrentUser());
            if (orderUserUnit != currentUserUnit)
                throw new IllegalArgumentException("Order doesn't belong to this user.");
        } else {
            throw new IllegalArgumentException("Order doesn't exist");
        }
    }

    private B2BUnitModel getB2BUnitForUser(UserModel userModel) {
        B2BUnitModel b2BUnitModel = null;
        if (userModel instanceof B2BCustomerModel) {
            B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) userModel;
            b2BUnitModel = b2BCustomerModel.getDefaultB2BUnit();
        }
        return b2BUnitModel;
    }

    protected B2BUnitModel getB2BUnitModel() {
        final CustomerData b2bCustomerData = getCheckoutCart().getB2bCustomerData();
        final B2BUnitData b2bUnitData = b2bCustomerData.getUnit();
        return getB2bCommerceUnitService().getUnitForUid(b2bUnitData.getUid());
    }

    protected List<AddressData> getAddressForB2BUnit(final B2BUnitModel b2bUnit, final Predicate<AddressModel> addressType) {
        final Collection<AddressModel> b2bUnitAddresses = b2bUnit.getAddresses();
        if (CollectionUtils.isNotEmpty(b2bUnitAddresses)) {
            final List<AddressModel> addresses = b2bUnitAddresses.stream().filter(addressType).collect(Collectors.toList());
            return getAddressConverter().convertAll(addresses);
        }
        return Collections.emptyList();
    }

    public CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    public B2BUnitFacade getB2bUnitFacade() {
        return b2bUnitFacade;
    }

    public void setB2bUnitFacade(B2BUnitFacade b2bUnitFacade) {
        this.b2bUnitFacade = b2bUnitFacade;
    }

    public TkEnumUtils getEnumUtils() {
        return enumUtils;
    }

    public void setEnumUtils(TkEnumUtils enumUtils) {
        this.enumUtils = enumUtils;
    }

    public DefaultTkB2bProductService getB2bProductService() {
        return b2bProductService;
    }

    public void setB2bProductService(DefaultTkB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void setBillingAddress(String selectedAddressCode) {
        final CartModel cartModel = getCart();
        final AddressModel selectedAddress = getB2bCommerceUnitService().getAddressForCode(getB2BUnitModel(), selectedAddressCode);
        if (selectedAddress != null) {
            setBillingInfo(cartModel, selectedAddress);
        }
    }

    @Override
    public void setCustomerShippingNotes(String shippingNotes) {
        // performing only in case a cart is existing
        if (hasCheckoutCart()) {
            final CartModel cartModel = getCartService().getSessionCart();
            cartModel.setCustomerShippingNote(shippingNotes);
            getModelService().save(cartModel);
        }
    }

    @Override
    public List<AddressData> getB2bUnitShippingAddress() {
        final B2BUnitModel b2BUnitModel = getB2BUnitModel();
        return getAddressForB2BUnit(b2BUnitModel, isShippingAddress());
    }

    @Override
    public List<AddressData> getB2bUnitBillingAddress() {
        final B2BUnitModel b2BUnitModel = getB2BUnitModel();
        return getAddressForB2BUnit(b2BUnitModel, isBillingAddress());
    }

    public B2BCommerceUnitService getB2bCommerceUnitService() {
        return b2bCommerceUnitService;
    }

    public void setB2bCommerceUnitService(B2BCommerceUnitService b2bCommerceUnitService) {
        this.b2bCommerceUnitService = b2bCommerceUnitService;
    }

    protected void setBillingInfo(final CartModel cartModel, final AddressModel selectedAddressModel) {
        final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        cloneBillingAddress(cartModel, selectedAddressModel, paymentInfo.getBillingAddress());
        paymentInfo.setBillingAddress(selectedAddressModel);
        final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
        parameter.setPaymentInfo(paymentInfo);
        getCommerceCheckoutService().setPaymentInfo(parameter);
    }

    private void cloneBillingAddress(final CartModel cart, final AddressModel selectedAddress, final AddressModel billingAddress) {
        if (billingAddress != null && !billingAddress.equals(selectedAddress)) {
            final AddressModel clonedAddress = getModelService().clone(billingAddress);
            if (billingAddress.equals(cart.getDeliveryAddress())) {
                cart.setDeliveryAddress(clonedAddress);
            }
            getModelService().save(clonedAddress);
        }
    }

    public TkEuB2bOrderService getTkEuB2bOrderService() {
        return tkEuB2bOrderService;
    }

    public void setTkEuB2bOrderService(TkEuB2bOrderService tkEuB2bOrderService) {
        this.tkEuB2bOrderService = tkEuB2bOrderService;
    }

}

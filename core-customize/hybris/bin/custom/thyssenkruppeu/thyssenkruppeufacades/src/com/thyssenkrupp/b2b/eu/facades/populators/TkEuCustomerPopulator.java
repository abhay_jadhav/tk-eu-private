package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

public class TkEuCustomerPopulator implements Populator<CustomerModel, CustomerData> {

    private Converter<AddressModel, AddressData> addressConverter;

    @Override
    public void populate(CustomerModel customerModel, CustomerData customerData) throws ConversionException {

        Assert.notNull(customerModel, "CustomerModel should not be null");
        Assert.notNull(customerData, "customerData should not be null");

        customerData.setEmail(StringUtils.trim(customerModel.getContactEmail()));
        final TitleModel title = customerModel.getTitle();
        if (title != null) {
            customerData.setTitleName(title.getName());
        }
        setPrimaryShippingAddress(customerModel, customerData);
        setPrimaryBillingAddress(customerModel, customerData);
    }

    protected void setPrimaryShippingAddress(CustomerModel customerModel, CustomerData customerData) {
        if (customerModel instanceof B2BCustomerModel) {

            final B2BCustomerModel customer = (B2BCustomerModel) customerModel;
            if(customer.getPrimaryShippingAddress() != null ) {
                customerData.setPrimaryShippingAddress(this.getAddressConverter().convert(customer.getPrimaryShippingAddress()));
            }
        }
    }

    protected void setPrimaryBillingAddress(CustomerModel customerModel, CustomerData customerData) {
        if (customerModel instanceof B2BCustomerModel) {

            final B2BCustomerModel customer = (B2BCustomerModel) customerModel;
            if(customer.getPrimaryBillingAddress() != null ) {
                customerData.setPrimaryBillingAddress(this.getAddressConverter().convert(customer.getPrimaryBillingAddress()));
            }
        }
    }

    protected Converter<AddressModel, AddressData> getAddressConverter() {
        return addressConverter;
    }

    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }
}

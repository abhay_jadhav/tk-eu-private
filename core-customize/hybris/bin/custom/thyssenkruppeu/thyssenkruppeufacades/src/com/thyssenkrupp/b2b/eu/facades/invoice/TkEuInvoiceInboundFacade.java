package com.thyssenkrupp.b2b.eu.facades.invoice;

import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceImportWsDTO;

public interface TkEuInvoiceInboundFacade {
    void createAndSaveInvoiceInfo(InvoiceImportWsDTO invoice);
    boolean isValidInvoice(InvoiceImportWsDTO invoice);
}

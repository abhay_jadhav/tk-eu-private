package com.thyssenkrupp.b2b.eu.facades.forgotpassword;

import de.hybris.platform.commercefacades.customer.CustomerFacade;

public interface ForgotPasswordFacade extends CustomerFacade {

    @Override
    void forgottenPassword(String uid);
}

package com.thyssenkrupp.b2b.eu.facades.atp.strategy;

import java.util.Date;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.facades.order.data.AvailabilityColor;

import de.hybris.platform.basecommerce.enums.InStockStatus;

public interface TkEuAtpAvailabilityStrategy {
    Optional<AvailabilityColor> getHybrisAtpAvailabilityColor(InStockStatus inStockStatus);

    Optional<String> getHybrisAtpAvailabilityMessage(InStockStatus inStockStatus);

    Optional<AvailabilityColor> getSapAtpAvailabilityColor(Date date);

    Optional<String> getSapAtpAvailabilityMessage();

    Optional<String> getPdpSapAtpAvailabilityStatus(Date date);

    Optional<String> getPdpSapAtpAvailabilityToolTip();

    Optional<AvailabilityColor> getPdpSapAtpAvailabilityColor(Date date);

    Optional<String> getPdpSapAtpAvailabilityMessage();

    Optional<String> getPdpHybrisAtpAvailabilityStatus(InStockStatus inStockStatus);

    Optional<String> getPdpHybrisAtpAvailabilityToolTip(InStockStatus inStockStatus);

    Optional<AvailabilityColor> getPdpHybrisAtpAvailabilityColor(InStockStatus inStockStatus);

    Optional<String> getPdpHybrisAtpAvailabilityMessage(InStockStatus inStockStatus);
}

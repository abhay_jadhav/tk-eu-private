package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

public class TkEuCommerceCartParameterBasicPopulator implements Populator<AddToCartParams, CommerceCartParameter> {

    private UnitService unitService;

    @Override
    public void populate(final AddToCartParams addToCartParams, final CommerceCartParameter parameter) throws ConversionException {
        if (StringUtils.isNotBlank(addToCartParams.getSalesUom())) {
            parameter.setUnit(getUnitService().getUnitForCode(addToCartParams.getSalesUom()));
        }
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }
}

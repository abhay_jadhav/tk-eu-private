package com.thyssenkrupp.b2b.eu.facades.order.impl;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkEuB2BUnitService;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderFacade;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;

public class DefaultTkEuOrderFacade extends DefaultOrderFacade implements TkEuOrderFacade {

    private static final Logger LOG                                     = LoggerFactory.getLogger(DefaultTkEuOrderFacade.class);
    private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE = "SAP order ID with guid %s not found current BaseStore";

    private TkEuB2bCustomerAccountService tkEuCustomerAccountService;
    private DefaultTkEuB2BUnitService     b2bUnitService;

    public TkEuB2bCustomerAccountService getTkEuCustomerAccountService() {
        return tkEuCustomerAccountService;
    }

    public void setTkEuCustomerAccountService(TkEuB2bCustomerAccountService tkEuCustomerAccountService) {
        this.tkEuCustomerAccountService = tkEuCustomerAccountService;
    }

    @Override
    public SearchPageData<OrderHistoryData> orderAcknowledgementSearchList(String searchKey, PageableData pageableData, OrderStatus... statuses) {
        final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
        final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
        final SearchPageData<OrderModel> orderResults = getTkEuCustomerAccountService().orderAcknowledgementSearchList(searchKey, currentCustomer, currentBaseStore,
            statuses, pageableData);

        return convertPageData(orderResults, getOrderHistoryConverter());
    }

    @Override
    public Integer findOrderAcknowledgementListCount(String searchKey, OrderStatus... statuses) {
        final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
        final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

        return getTkEuCustomerAccountService().findOrderAcknowledgementListCount(searchKey, currentCustomer, currentBaseStore,
            statuses);
    }

    @Override
    public SearchPageData<OrderHistoryData> orderConfirmationSearchList(String searchKey, PageableData pageableData, OrderStatus... statuses) {
        final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
        final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
        final SearchPageData<OrderModel> orderResults = getTkEuCustomerAccountService().orderConfirmationSearchList(searchKey, currentCustomer, currentBaseStore,
            statuses, pageableData);

        return convertPageData(orderResults, getOrderHistoryConverter());
    }

    @Override
    public OrderData getOrderConfirmationDetailsForCode(final String sapOrderId) {
        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();

        OrderModel orderModel = null;

        try {
            Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
            orderModel = getTkEuCustomerAccountService().getOrderConfirmationDetailsForCode(sapOrderId, baseStoreModel, b2bUnits);
        } catch (final ModelNotFoundException e) {
            throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, sapOrderId));
        }

        if (orderModel == null) {
            throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, sapOrderId));
        }
        return getOrderConverter().convert(orderModel);
    }

    @Override
    public Integer findOrderConfirmationListCount(String searchKey, OrderStatus... statuses) {
        final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
        final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

        return getTkEuCustomerAccountService().findOrderConfirmationListCount(searchKey, currentCustomer, currentBaseStore,
            statuses);
    }

    @Override
    public OrderData getOrderDetailsForCode(final String code) {
        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();

        OrderModel orderModel = null;
        if (getCheckoutCustomerStrategy().isAnonymousCheckout()) {
            orderModel = getCustomerAccountService().getOrderDetailsForGUID(code, baseStoreModel);
        } else {
            try {
                orderModel = getCustomerAccountService().getOrderForCode((CustomerModel) getUserService().getCurrentUser(), code, baseStoreModel);
            } catch (final ModelNotFoundException e) {
                throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
            }
        }

        if (orderModel == null) {
            throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
        }
        return getOrderConverter().convert(orderModel);
    }

    @Override
    public boolean oderConfirmationExists(String orderCode) {
        boolean orderConfirmationExists = true;

        try {
            OrderData data = getOrderConfirmationDetailsForCode(orderCode);
            if (data == null)
                orderConfirmationExists = false;
        } catch (UnknownIdentifierException e) {
            orderConfirmationExists = false;
        }
        return orderConfirmationExists;
    }

    @Override
    public String getSapOrderIdFromConfirmedOrder(final String orderCode) {
        Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        OrderModel orderModel = getTkEuCustomerAccountService().getConfirmedOrderByCode(orderCode, getBaseStoreService().getCurrentBaseStore(), b2bUnits);
        if (isNull(orderModel) || isEmpty(orderModel.getSapOrderId())) {
            LOG.warn("No SapOrderId found for order:" + orderCode);
            return null;
        }
        return orderModel.getSapOrderId();
    }

    @Override
    public String getSapOrderIdFromCancelledOrder(final String orderCode) {
        Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        OrderModel orderModel = getTkEuCustomerAccountService().getCancelledOrderByCode(orderCode, getBaseStoreService().getCurrentBaseStore(), b2bUnits);
        if (isNull(orderModel) || isEmpty(orderModel.getSapOrderId())) {
            LOG.warn("No SapOrderId found for order:" + orderCode);
            return null;
        }
        return orderModel.getSapOrderId();
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}

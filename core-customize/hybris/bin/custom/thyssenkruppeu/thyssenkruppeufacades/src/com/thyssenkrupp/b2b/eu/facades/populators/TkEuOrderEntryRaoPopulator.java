package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.rao.DiscountValueRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.ruleengineservices.util.CurrencyUtils;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.google.common.math.DoubleMath;

public class TkEuOrderEntryRaoPopulator implements Populator<AbstractOrderEntryModel, OrderEntryRAO> {
    private static final Logger LOG     = Logger.getLogger(TkEuOrderEntryRaoPopulator.class);
    private static final double EPSILON = 0.01d;
    private Converter<ProductModel, ProductRAO> productConverter;
    private Converter<DiscountValue, DiscountValueRAO> discountValueConverter;
    private CurrencyUtils currencyUtils;

    @Override
    public void populate(AbstractOrderEntryModel source, OrderEntryRAO target) {

        if (Objects.nonNull(source.getProduct())) {
            target.setProduct((ProductRAO) this.getProductConverter().convert(source.getProduct()));
        }

        if (Objects.nonNull(source.getQuantity())) {
            target.setQuantity(source.getQuantity().intValue());
        }

        Double basePrice = source.getBasePrice();
        if (Objects.nonNull(basePrice)) {
            target.setBasePrice(BigDecimal.valueOf(basePrice));
            target.setPrice(target.getBasePrice());
            AbstractOrderModel order = source.getOrder();
            if (Objects.nonNull(order) && Objects.nonNull(order.getCurrency())) {
                target.setCurrencyIsoCode(order.getCurrency().getIsocode());
            } else {
                LOG.warn("Order is null or the order currency is not set correctly");
            }
        }

        if (Objects.nonNull(source.getEntryNumber())) {
            target.setEntryNumber(source.getEntryNumber());
        }

        if (CollectionUtils.isNotEmpty(source.getDiscountValues())) {

            List<DiscountValue> promotionDiscountValues = getProductDiscountsAmount(source);
            promotionDiscountValues.forEach((discountValue) -> {
                this.applyDiscount(target, discountValue);
            });
            target.setDiscountValues(
                    Converters.convertAll(source.getDiscountValues(), this.getDiscountValueConverter()));
        }

    }

    protected void applyDiscount(OrderEntryRAO target, DiscountValue discountValue) {
        BigDecimal discountAmount = BigDecimal
                .valueOf(
                        discountValue
                                .apply(1.0D, target.getBasePrice().doubleValue(),
                                        this.getCurrencyUtils().getDigitsOfCurrencyOrDefault(
                                                target.getCurrencyIsoCode()),
                                        target.getCurrencyIsoCode())
                                .getAppliedValue());
        target.setPrice(target.getPrice().subtract(discountAmount));
    }

    protected List<DiscountValue> getProductDiscountsAmount(final AbstractOrderEntryModel source) {
        /*method to get only product discount (come with +ve sign) and remove all surcharges (come with -ve sign) */
        List<DiscountValue> discountValues = new ArrayList<>();
        final List<DiscountValue> discountList = source.getDiscountValues();
        if (discountList != null && !discountList.isEmpty()) {
            for (final DiscountValue discount : discountList) {
                final double value = discount.getAppliedValue();
                if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0) {
                    discountValues.add(discount);
                }
            }
        }
        return discountValues;
    }
    /**
     * @return the productConverter
     */
    public Converter<ProductModel, ProductRAO> getProductConverter() {
        return productConverter;
    }

    /**
     * @param productConverter
     *            the productConverter to set
     */
    public void setProductConverter(Converter<ProductModel, ProductRAO> productConverter) {
        this.productConverter = productConverter;
    }

    /**
     * @return the discountValueConverter
     */
    public Converter<DiscountValue, DiscountValueRAO> getDiscountValueConverter() {
        return discountValueConverter;
    }

    /**
     * @param discountValueConverter
     *            the discountValueConverter to set
     */
    public void setDiscountValueConverter(Converter<DiscountValue, DiscountValueRAO> discountValueConverter) {
        this.discountValueConverter = discountValueConverter;
    }

    /**
     * @return the currencyUtils
     */
    public CurrencyUtils getCurrencyUtils() {
        return currencyUtils;
    }

    /**
     * @param currencyUtils
     *            the currencyUtils to set
     */
    public void setCurrencyUtils(CurrencyUtils currencyUtils) {
        this.currencyUtils = currencyUtils;
    }

}

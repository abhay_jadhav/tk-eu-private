package com.thyssenkrupp.b2b.eu.facades.utils;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import org.apache.commons.lang.StringUtils;

public class TkEnumUtils {

    private EnumerationService enumerationService;

    public String getNameFromEnumCode(HybrisEnumValue hybrisEnumValue) {
        String enumerationValueName = "";
        if (hybrisEnumValue != null) {
            enumerationValueName = getEnumerationService().getEnumerationName(hybrisEnumValue);
            if (StringUtils.isBlank(enumerationValueName)) {
                enumerationValueName = hybrisEnumValue.getCode();
            }
        }
        return enumerationValueName;
    }

    public EnumerationService getEnumerationService() {
        return enumerationService;
    }

    public void setEnumerationService(EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }
}

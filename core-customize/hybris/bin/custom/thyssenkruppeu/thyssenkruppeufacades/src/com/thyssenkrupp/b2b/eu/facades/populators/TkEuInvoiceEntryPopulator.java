package com.thyssenkrupp.b2b.eu.facades.populators;

import static com.thyssenkrupp.b2b.eu.facades.utils.TkEuFacadesUtils.formatUom;
import static java.math.BigDecimal.valueOf;
import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.facades.product.data.UnitData;
import com.thyssenkrupp.b2b.eu.model.TempConsignmentEntryModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceEntryModel;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TkEuInvoiceEntryPopulator implements Populator<InvoiceEntryModel, TkEuInvoiceEntryData> {

    private static final Logger LOGGER = Logger.getLogger(TkEuInvoiceEntryPopulator.class);
    private static String DEFAULT_CURRENCY_CODE = "EUR";
    private Converter<UnitModel, UnitData> productUnitConverter;
    private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;
    private TkEuPriceDataFactory tkEuPriceDataFactory;

    @Override
    public void populate(InvoiceEntryModel invoiceEntryModel, TkEuInvoiceEntryData invoiceEntryData)
            throws ConversionException {
        invoiceEntryData.setInvoiceEntryNumber(invoiceEntryModel.getEntryNumber());
        invoiceEntryData.setProductCode(invoiceEntryModel.getProductCode());
        invoiceEntryData.setProductCodeCustomer(invoiceEntryModel.getProductCodeCustomer());
        invoiceEntryData.setProductName(invoiceEntryModel.getProductName());
        if (nonNull(invoiceEntryModel.getQuantity())) {
            invoiceEntryData.setQuantity(valueOf(invoiceEntryModel.getQuantity()).stripTrailingZeros().toPlainString());
        }
        invoiceEntryData.setTotalNet(getTotalNetAsPriceData(invoiceEntryModel));
        if (invoiceEntryModel.getUnit() != null) {
            invoiceEntryData.setUnit(getProductUnitConverter().convert(invoiceEntryModel.getUnit()));
        }
        List<ConsignmentEntryData> consignmentEntryDataList = new ArrayList<ConsignmentEntryData>();
        try {

            if (CollectionUtils.isNotEmpty(invoiceEntryModel.getConsignmentEntries())) {
                consignmentEntryDataList
                        .addAll(getConsignmentEntryConverter().convertAll(invoiceEntryModel.getConsignmentEntries()));
            }

        } catch (Exception e) {
            consignmentEntryDataList.addAll(populateConsignmentEntries(invoiceEntryModel.getConsignmentEntries(),
                    invoiceEntryModel.getInvoice().getCode()));
            LOGGER.error("Error in Populating Consignment Info for Invoice " + invoiceEntryModel.getInvoice().getCode()
                    + " :" + e.getMessage(), e);
        }

        if (CollectionUtils.isNotEmpty(invoiceEntryModel.getTempConsignmentEntries())) {
            consignmentEntryDataList
                    .addAll(populateTempConsignmentEntries(invoiceEntryModel.getTempConsignmentEntries(),invoiceEntryModel.getInvoice().getCode()));
        }

        invoiceEntryData.setConsignmentEntries(consignmentEntryDataList);

    }

    private Collection<? extends ConsignmentEntryData> populateConsignmentEntries(
            List<ConsignmentEntryModel> consignmentEntries, String invoiceCode) {
        List<ConsignmentEntryData> tempConsignmentEntryDataList = new ArrayList<ConsignmentEntryData>();
        consignmentEntries.forEach(tempConsignmentEntry -> {
            try {
                ConsignmentEntryData tempConsignmentEntryData = new ConsignmentEntryData();
                tempConsignmentEntryData.setConsignmentNumber(tempConsignmentEntry.getConsignment().getCode());
                tempConsignmentEntryData.setBatchGroupNumber(tempConsignmentEntry.getBatchGroupNumber());
                tempConsignmentEntryData.setIsLinkAvailable(false);
                tempConsignmentEntryDataList.add(tempConsignmentEntryData);
            } catch (Exception e) {
                LOGGER.error("Error in Populating Post Exception Consignment Info for Invoice " + invoiceCode + " :"
                        + e.getMessage(), e);
            }
        });
        return tempConsignmentEntryDataList;
    }

    private List<ConsignmentEntryData> populateTempConsignmentEntries(
            List<TempConsignmentEntryModel> tempConsignments, String invoiceCode) {

        List<ConsignmentEntryData> tempConsignmentEntryDataList = new ArrayList<ConsignmentEntryData>();
        tempConsignments.forEach(tempConsignmentEntry -> {
            try {
                ConsignmentEntryData tempConsignmentEntryData = new ConsignmentEntryData();
                tempConsignmentEntryData.setConsignmentNumber(tempConsignmentEntry.getConsignmentCode());
                tempConsignmentEntryData.setBatchGroupNumber(tempConsignmentEntry.getBatchGroupNumber());
                tempConsignmentEntryData.setIsLinkAvailable(false);
                tempConsignmentEntryDataList.add(tempConsignmentEntryData);
            } catch (Exception e) {
                LOGGER.error("Error in Populating Temp Consignment Info for Invoice " + invoiceCode + " :"
                        + e.getMessage(), e);
            }
        });
        return tempConsignmentEntryDataList;
    }

    private PriceData getTotalNetAsPriceData(InvoiceEntryModel invoiceEntryModel) {
        String currencyIsoCode = invoiceEntryModel.getInvoice().getCurrency() != null
                ? invoiceEntryModel.getInvoice().getCurrency().getIsocode()
                : DEFAULT_CURRENCY_CODE;
        String uomName = invoiceEntryModel.getUnit() != null ? invoiceEntryModel.getUnit().getName() : null;
        return getTkEuPriceDataFactory().create(PriceDataType.BUY, valueOf(invoiceEntryModel.getTotalNet()),
                currencyIsoCode, formatUom(uomName));

    }

    public Converter<UnitModel, UnitData> getProductUnitConverter() {
        return productUnitConverter;
    }

    public void setProductUnitConverter(Converter<UnitModel, UnitData> productUnitConverter) {
        this.productUnitConverter = productUnitConverter;
    }

    public Converter<ConsignmentEntryModel, ConsignmentEntryData> getConsignmentEntryConverter() {
        return consignmentEntryConverter;
    }

    public void setConsignmentEntryConverter(
            Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter) {
        this.consignmentEntryConverter = consignmentEntryConverter;
    }

    public TkEuPriceDataFactory getTkEuPriceDataFactory() {
        return tkEuPriceDataFactory;
    }

    public void setTkEuPriceDataFactory(TkEuPriceDataFactory tkEuPriceDataFactory) {
        this.tkEuPriceDataFactory = tkEuPriceDataFactory;
    }

}

package com.thyssenkrupp.b2b.eu.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil;

public class TkEuCartEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        addCustomerSpecifiedDeliveryDate(source, target);
    }

    protected void addCustomerSpecifiedDeliveryDate(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        String strDate = "";
        if (orderEntry.getCustomerSpecifiedDeliveryDate() == null) {
            entry.setCustomerSpecifiedDeliveryDate(strDate);
        } else {
            strDate = TkCommonUtil.dateToString(TkCommonUtil.YYYY_MM_DD, orderEntry.getCustomerSpecifiedDeliveryDate());
            if (strDate != null) {
                entry.setCustomerSpecifiedDeliveryDate(strDate);
            } else {
                entry.setCustomerSpecifiedDeliveryDate("");
            }
        }
    }
}

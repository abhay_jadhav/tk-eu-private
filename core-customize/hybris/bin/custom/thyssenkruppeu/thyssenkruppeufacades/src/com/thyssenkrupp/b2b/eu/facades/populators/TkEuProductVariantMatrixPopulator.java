package com.thyssenkrupp.b2b.eu.facades.populators;

import static java.util.Comparator.comparing;
import static org.apache.commons.lang.ArrayUtils.EMPTY_STRING_ARRAY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants;
import com.thyssenkrupp.b2b.eu.facades.utils.ProductModelUtils;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bProductService;
import com.thyssenkrupp.b2b.eu.core.service.impl.TkB2bDefaultCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductVariantMatrixPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantCategoryData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantValueCategoryData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

public class TkEuProductVariantMatrixPopulator extends ProductVariantMatrixPopulator {

    private              Converter<CategoryModel, CategoryData>             categoryConverter;
    private              ConfigurationService                               configurationService;
    private              TkB2bDefaultCatalogProductReplicationConfigService productReplicationConfigService;
    private              TkB2bProductService                                productService;
    private ProductModelUtils productModelUtils;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
        final Collection<VariantProductModel> variants = getProductModelUtils().getVariants(productModel);
        productData.setMultidimensional(Boolean.valueOf(CollectionUtils.isNotEmpty(variants)));

        if (productData.getMultidimensional().booleanValue()) {
            List<VariantMatrixElementData> variantMatrixList = new ArrayList<VariantMatrixElementData>();
            Collection<CategoryModel> categories = new ArrayList<>();
            for (final VariantProductModel variant : variants) {
                if (variant instanceof ERPVariantProductModel) {
                    populateVariantCategoryValues(variant, variantMatrixList, categories);
                }
            }
            variantMatrixList.sort(comparing(o -> o.getVariantValueCategory().getSequence()));
            productData.setUnselectedVariantMatrixList(variantMatrixList);
            Collection<CategoryData> categorDatas = getCategoryConverter().convertAll(categories);
            final String[] categoryAttributes = getCategoryAttributeRules(productModel);
            Collection<CategoryData> sortedCategories = sortCategoriesByRuleEngineConfiguration(categorDatas, categoryAttributes);
            productData.setCategories(sortedCategories);
        }
    }

    Collection<CategoryData> sortCategoriesByRuleEngineConfiguration(Collection<CategoryData> categorDatas, final String[] attributes) {
        Collection<CategoryData> sortedCategories = new ArrayList<>();
        for (String attribute : attributes) {
            for (CategoryData category : categorDatas) {
                String categoryCode = category.getCode();
                if (StringUtils.endsWith(categoryCode, attribute)) {
                    sortedCategories.add(category);
                    break;
                }
            }
        }
        // add any other Variant categories if existing
        for (CategoryData category : categorDatas) {
            if (!sortedCategories.contains(category)) {
                sortedCategories.add(category);
            }
        }

        return sortedCategories;
    }

    private String[] getCategoryAttributeRules(final ProductModel productModel) {
        final CategoryModel classificationCategory = getProductService().getClassificationCategoryForProduct(productModel);
        if (classificationCategory != null) {
            final Optional<String> maybeCategoryRules = getProductReplicationConfigService().getVariantGenerationRuleOrDefault(productModel.getCatalogVersion().getCatalog(), classificationCategory.getCode());
            return maybeCategoryRules.map(TkCommonUtil::splitByCommaCharacter).orElse(EMPTY_STRING_ARRAY);
        }
        return EMPTY_STRING_ARRAY;
    }

    void populateVariantCategoryValues(VariantProductModel variant, List<VariantMatrixElementData> variantMatrixList, Collection<CategoryModel> categories) {

        final List<VariantValueCategoryModel> valuesCategories = getVariantValuesCategories(variant);
        final List<VariantValueCategoryData> variantValueCategoryDatas = new LinkedList<>();
        if (CollectionUtils.isNotEmpty(valuesCategories)) {

            for (final VariantValueCategoryModel variantValueCategory : valuesCategories) {
                final VariantCategoryData parent = new VariantCategoryData();
                VariantMatrixElementData variantMatrixData = new VariantMatrixElementData();
                final VariantCategoryData parentVariantCategory = new VariantCategoryData();
                final VariantOptionData variantOptionData = new VariantOptionData();
                final VariantValueCategoryData data = new VariantValueCategoryData();
                final VariantCategoryModel variantCategoryModel = (VariantCategoryModel) variantValueCategory.getSupercategories().get(0);
                if (!categories.contains(variantCategoryModel)) {
                    categories.add(variantCategoryModel);
                }
                parent.setName(variantCategoryModel.getName());
                parent.setCode(variantCategoryModel.getCode());
                parent.setHasImage(variantCategoryModel.getHasImage());

                data.setName(variantValueCategory.getName());
                data.setCode(variantValueCategory.getCode());
                int indexOfVvc = 0;
                if (CollectionUtils.isNotEmpty(variantCategoryModel.getCategories())) {
                    indexOfVvc = variantCategoryModel.getCategories().indexOf(variantValueCategory);
                }
                data.setSequence(indexOfVvc);
                data.setSuperCategories(Collections.singletonList(parent));
                variantValueCategoryDatas.add(data);

                // setting unselected matrix
                if (!listContainsElement(variantMatrixList, data)) {
                    parentVariantCategory.setName(parent.getName());
                    parentVariantCategory.setCode(parent.getCode());
                    setTradeLengthCategoryFlag(variantCategoryModel, parentVariantCategory);
                    variantMatrixData.setIsLeaf(Boolean.TRUE);
                    variantOptionData.setCode(variant.getCode());
                    variantMatrixData.setVariantOption(variantOptionData);
                    variantMatrixData.setParentVariantCategory(parentVariantCategory);
                    variantMatrixData.setVariantValueCategory(data);
                    variantMatrixList.add(variantMatrixData);
                }
            }
        }
    }

    private void setTradeLengthCategoryFlag(VariantCategoryModel variantCategoryModel, VariantCategoryData parentVariantCategory) {
        if (StringUtils.containsIgnoreCase(variantCategoryModel.getCode(), ThyssenkruppeufacadesConstants.TRADE_LENGTH_IDENTIFIER)) {
            parentVariantCategory.setIsTradeLengthCategory(Boolean.TRUE);
        }
    }

    private boolean listContainsElement(List<VariantMatrixElementData> unselectedVariantMatrixList, VariantValueCategoryData data) {
        return unselectedVariantMatrixList.stream().anyMatch(variantMatrix -> variantMatrix.getVariantValueCategory().getCode() == data.getCode());
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    protected Converter<CategoryModel, CategoryData> getCategoryConverter() {
        return categoryConverter;
    }

    @Required
    public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    public TkB2bDefaultCatalogProductReplicationConfigService getProductReplicationConfigService() {
        return productReplicationConfigService;
    }

    @Required
    public void setProductReplicationConfigService(TkB2bDefaultCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }

    public TkB2bProductService getProductService() {
        return productService;
    }
    @Required
    public void setProductService(TkB2bProductService productService) {
        this.productService = productService;
    }

    public ProductModelUtils getProductModelUtils() {
        return productModelUtils;
    }

    public void setProductModelUtils(ProductModelUtils productModelUtils) {
        this.productModelUtils = productModelUtils;
    }
}

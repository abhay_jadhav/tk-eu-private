package com.thyssenkrupp.b2b.eu.facades.populators;

import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.TkEuFacetSearchService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SearchQueryTemplateNameResolver;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TkEuProductSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE> implements
  Populator<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE>> {

    private static final Logger LOG = Logger.getLogger(TkEuProductSearchSolrQueryPopulator.class);

    private CommonI18NService                      commonI18NService;
    private BaseSiteService                        baseSiteService;
    private BaseStoreService                       baseStoreService;
    private CatalogVersionService                  catalogVersionService;
    private TkEuFacetSearchService                 facetSearchService;
    private FacetSearchConfigService               facetSearchConfigService;
    private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
    private SearchQueryTemplateNameResolver        searchQueryTemplateNameResolver;

    @Override
    public void populate(SearchQueryPageableData<SolrSearchQueryData> source, SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target) throws ConversionException {

        target.setSearchQueryData(source.getSearchQueryData());
        target.setPageableData(source.getPageableData());

        setCatalogVersios(target);

        try {
            target.setFacetSearchConfig(getFacetSearchConfig());
        } catch (final NoValidSolrConfigException e) {
            LOG.error("No valid solrFacetSearchConfig found for the current context", e);
            throw new ConversionException("No valid solrFacetSearchConfig found for the current context", e);
        } catch (final FacetConfigServiceException e) {
            LOG.error(e.getMessage(), e);
            throw new ConversionException(e.getMessage(), e);
        }

        target.setIndexedType(getIndexedType(target.getFacetSearchConfig()));

        SearchQuery searchQuery;

        if (target.getFacetSearchConfig().getSearchConfig().isLegacyMode()) {
            searchQuery = createProductSearchQueryForLegacyMode(target.getFacetSearchConfig(), target.getIndexedType(),
              source.getSearchQueryData().getSearchQueryContext(), source.getSearchQueryData().getFreeTextSearch());
        } else {
            searchQuery = createProductSearchQuery(target.getFacetSearchConfig(), target.getIndexedType(),
              source.getSearchQueryData().getSearchQueryContext(), source.getSearchQueryData().getFreeTextSearch());
        }

        searchQuery.setCatalogVersions(target.getCatalogVersions());
        searchQuery.setCurrency(getCommonI18NService().getCurrentCurrency().getIsocode());
        searchQuery.setLanguage(getCommonI18NService().getCurrentLanguage().getIsocode());

        searchQuery.setEnableSpellcheck(true);

        target.setSearchQuery(searchQuery);
    }

    private void setCatalogVersios(SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target) {
        final Collection<CatalogVersionModel> catalogVersions = getSessionProductCatalogVersions();
        if (catalogVersions == null || catalogVersions.isEmpty()) {
            throw new ConversionException("Missing solr facet search indexed catalog versions");
        }
        target.setCatalogVersions(new ArrayList<CatalogVersionModel>(catalogVersions));
    }

    protected CatalogVersionModel getSessionProductCatalogVersion() {
        for (final CatalogVersionModel catalogVersion : getSessionProductCatalogVersions()) {
            final List<SolrFacetSearchConfigModel> facetSearchConfigs = catalogVersion.getFacetSearchConfigs();
            if (facetSearchConfigs != null && !facetSearchConfigs.isEmpty()) {
                return catalogVersion;
            }
        }
        return null;
    }

    protected Collection<CatalogVersionModel> getSessionProductCatalogVersions() {
        final BaseSiteModel currentSite = getBaseSiteService().getCurrentBaseSite();
        final List<CatalogModel> productCatalogs = getBaseSiteService().getProductCatalogs(currentSite);
        final Collection<CatalogVersionModel> sessionCatalogVersions = getCatalogVersionService().getSessionCatalogVersions();
        final Collection<CatalogVersionModel> result = new ArrayList<CatalogVersionModel>();
        for (final CatalogVersionModel sessionCatalogVersion : sessionCatalogVersions) {
            if (productCatalogs.contains(sessionCatalogVersion.getCatalog())) {
                result.add(sessionCatalogVersion);
            }
        }
        return result;
    }

    protected FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException {
        final SolrFacetSearchConfigModel solrFacetSearchConfigModel = solrFacetSearchConfigSelectionStrategy.getCurrentSolrFacetSearchConfig();
        return facetSearchConfigService.getConfiguration(solrFacetSearchConfigModel.getName());
    }

    protected IndexedType getIndexedType(final FacetSearchConfig config) {
        final IndexConfig indexConfig = config.getIndexConfig();

        final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
        if (indexedTypes != null && !indexedTypes.isEmpty()) {
            return indexedTypes.iterator().next();
        }
        return null;
    }

    protected SearchQuery createProductSearchQueryForLegacyMode(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
      final SearchQueryContext searchQueryContext, final String freeTextSearch) {
        final SearchQuery searchQuery = new SearchQuery(facetSearchConfig, indexedType);
        searchQuery.setDefaultOperator(SearchQuery.Operator.OR);
        searchQuery.setUserQuery(freeTextSearch);
        return searchQuery;
    }

    protected SearchQuery createProductSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType, final SearchQueryContext searchQueryContext, final String freeTextSearch) {
        final SearchQuery searchQuery = facetSearchService.createProductSearchQuery(facetSearchConfig, indexedType, freeTextSearch);
        return searchQuery;
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public TkEuFacetSearchService getFacetSearchService() {
        return facetSearchService;
    }

    public void setFacetSearchService(TkEuFacetSearchService facetSearchService) {
        this.facetSearchService = facetSearchService;
    }

    public FacetSearchConfigService getFacetSearchConfigService() {
        return facetSearchConfigService;
    }

    public void setFacetSearchConfigService(FacetSearchConfigService facetSearchConfigService) {
        this.facetSearchConfigService = facetSearchConfigService;
    }

    public SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy() {
        return solrFacetSearchConfigSelectionStrategy;
    }

    public void setSolrFacetSearchConfigSelectionStrategy(SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy) {
        this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
    }

    public SearchQueryTemplateNameResolver getSearchQueryTemplateNameResolver() {
        return searchQueryTemplateNameResolver;
    }

    public void setSearchQueryTemplateNameResolver(SearchQueryTemplateNameResolver searchQueryTemplateNameResolver) {
        this.searchQueryTemplateNameResolver = searchQueryTemplateNameResolver;
    }
}

package com.thyssenkrupp.b2b.eu.facades.customer.impl;

import static com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus.ACTIVATED;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;
import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.user.UserService;

public class DefaultTkEuB2bCustomerFacade extends DefaultB2BCustomerFacade implements TkEuB2bCustomerFacade {


    private static final String FORGOT_PASSWORD_TOKEN_VALIDITY_SECONDS = "forgot.password.token.validity.seconds";  //NOSONAR

    private CustomerAccountService customerAccountService;

    private SecureTokenService secureTokenService;

    private UserService userService;

    private ConfigurationService configurationService;

    private TkEuB2bCustomerService b2bCustomerService;

    private I18NService i18NService;

    @Override
    public CustomerData getCurrentB2bCustomer() {
        if (getCurrentUser() instanceof B2BCustomerModel) {
            return getCustomerConverter().convert(getCurrentUser());
        }
        return null;
    }

    @Override
    public AddressData getPrimaryShippingAddressOfCurrentUser() {
        CustomerData currentB2bCustomer = getCurrentB2bCustomer();
        return currentB2bCustomer != null ? currentB2bCustomer.getPrimaryShippingAddress() : null;
    }

    @Override
    public boolean completeCustomerRegistration(@NotNull final String uId) {
        final B2BCustomerModel b2BCustomerModel = getUserService().getUserForUID(uId, B2BCustomerModel.class);
        b2BCustomerModel.setRegistrationStatus(ACTIVATED);
        b2BCustomerModel.setToken(StringUtils.EMPTY);
        getModelService().save(b2BCustomerModel);
        getModelService().refresh(b2BCustomerModel);
        return true;
    }

    @Override
    public boolean isTokenExpired(final String token) {
        final SecureToken data = fetchDecryptedTokenData(token);
        final long delta = new Date().getTime() - data.getTimeStamp();
        long forgotPasswordTokenValidityInSeconds = getConfigurationService().getConfiguration().getInt(FORGOT_PASSWORD_TOKEN_VALIDITY_SECONDS);
        return (delta / 1000 > forgotPasswordTokenValidityInSeconds);
    }

    @Override
    public boolean isTokenInvalid(final String token) {
        final SecureToken data = fetchDecryptedTokenData(token);
        final CustomerModel customer = (CustomerModel) b2bCustomerService.getAllUserForUID(data.getData(), CustomerModel.class);
        return (!token.equals(customer.getToken()));
    }

    @Override
    public void forgottenPassword(final String uid) {
        Assert.hasText(uid, "The field [uid] cannot be empty");
        final CustomerModel customerModel = (CustomerModel) b2bCustomerService.getAllUserForUID(uid.toLowerCase(getI18NService().getCurrentLocale()), CustomerModel.class);
        getCustomerAccountService().forgottenPassword(customerModel);
    }

    @Override
    public String getEmailIdFromToken(final String token) {
        final SecureToken data = fetchDecryptedTokenData(token);
        return data.getData();
    }

    private SecureToken fetchDecryptedTokenData(final String token) {
        Assert.hasText(token, "The field [token] cannot be empty");
        return getSecureTokenService().decryptData(token);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public CustomerAccountService getCustomerAccountService() {
        return customerAccountService;
    }

    public void setCustomerAccountService(CustomerAccountService customerAccountService) {
        this.customerAccountService = customerAccountService;
    }

    public SecureTokenService getSecureTokenService() {
        return secureTokenService;
    }

    public void setSecureTokenService(SecureTokenService secureTokenService) {
        this.secureTokenService = secureTokenService;
    }

    public TkEuB2bCustomerService getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(TkEuB2bCustomerService b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }

    public I18NService getI18NService() {
        return i18NService;
    }

    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }
}

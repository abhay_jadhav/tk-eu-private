package com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TkEuFacetPopulator<SOURCE extends FacetData, TARGET extends FacetData> implements Populator<SOURCE, TARGET> {

    private Converter searchStateConverter;

    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        @SuppressWarnings("unchecked") final Optional<List<FacetValueData>> sourceValues = Optional.of(source.getValues());
        sourceValues.ifPresent(presentSourceValues -> {
            target.setSelected(hasSelectedFacetValue(presentSourceValues));
            if (target.isSelected()) {
                final List<FacetValueData> selectedFacetValues = presentSourceValues.stream().filter(FacetValueData::isSelected).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(selectedFacetValues)) {
                    buildResetFilterForFacet(target, selectedFacetValues);
                }
            }
        });
        target.setFacetSearchAndScroll(source.isFacetSearchAndScroll());
    }

    private void buildResetFilterForFacet(final FacetData target, final List<FacetValueData> selectedFacetValues) {
        final Optional<FacetValueData> firstFacetValue = selectedFacetValues.stream().findFirst();
        firstFacetValue.ifPresent(presentFirstFacetValue -> {
            final SolrSearchQueryData solrSearchQueryData = (SolrSearchQueryData) presentFirstFacetValue.getQuery();
            final List<SolrSearchQueryTermData> termsWithoutCurrentFacet = getTermsWithoutCurrentFacet(target.getCode(), solrSearchQueryData);
            solrSearchQueryData.setFilterTerms(termsWithoutCurrentFacet);
            target.setResetUrl(getResetUri(solrSearchQueryData));
        });
    }

    private List<SolrSearchQueryTermData> getTermsWithoutCurrentFacet(final String facetCode, final SolrSearchQueryData solrSearchQueryData) {
        final List<SolrSearchQueryTermData> filterTerms = solrSearchQueryData.getFilterTerms();
        return filterTerms.stream().filter(filterTerm -> !facetCode.equalsIgnoreCase(filterTerm.getKey())).collect(Collectors.toList());
    }

    private boolean hasSelectedFacetValue(final List<FacetValueData> facetValues) {
        return facetValues.stream().anyMatch(FacetValueData::isSelected);
    }

    private String getResetUri(final SolrSearchQueryData searchQueryData) {
        @SuppressWarnings("unchecked") final Optional<SearchStateData> queryData = Optional.ofNullable((SearchStateData) getSearchStateConverter().convert(searchQueryData));
        return queryData.map(SearchStateData::getUrl).orElse("");
    }

    protected Converter getSearchStateConverter() {
        return searchStateConverter;
    }

    public void setSearchStateConverter(final Converter searchStateConverter) {
        this.searchStateConverter = searchStateConverter;
    }
}

package com.thyssenkrupp.b2b.eu.facades.contact.impl;

import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.customerticketingfacades.customerticket.DefaultCustomerTicketingFacade;
import de.hybris.platform.customerticketingfacades.data.TicketData;
import de.hybris.platform.ticket.enums.CsEventReason;
import de.hybris.platform.ticket.enums.CsInterventionType;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticketsystem.data.CsTicketParameter;
import de.hybris.platform.ticket.enums.CsTicketState;
import org.apache.commons.lang.StringUtils;

public class DefaultTkEuContactFacade extends DefaultCustomerTicketingFacade {

    private String ticketPriority;
    private String ticketReason;

    @Override
    public TicketData createTicket(final TicketData ticketData) {
        final CsTicketModel ticket;
        final CsTicketParameter ticketParameter = createCsTicketParameter(ticketData);
        ticket = getTicketBusinessService().createTicket(ticketParameter);
        ticketData.setId(ticket.getTicketID());
        return ticketData;
    }

    protected CsTicketParameter createCsTicketParameter(final TicketData ticketData) {
        final CsTicketParameter ticketParameter = new CsTicketParameter();

        ticketParameter.setReason(getEnumerationService().getEnumerationValue(CsEventReason._TYPECODE, ticketReason));
        ticketParameter.setAssociatedTo(getTicketService().getAssociatedObject(ticketData.getAssociatedTo(), null, null));
        ticketParameter.setAssignedGroup(getDefaultCsAgentManagerGroup());
        addTitleIfPresent(ticketParameter, ticketData);
        ticketParameter.setHeadline(ticketData.getSubject());
        ticketParameter.setInterventionType(CsInterventionType.TICKETMESSAGE);
        ticketParameter.setCreationNotes(ticketData.getMessage());
        ticketParameter.setCustomer(getUserService().getCurrentUser());
        ticketParameter.setAttachments(ticketData.getAttachments());
        ticketParameter.setCompany(ticketData.getCompany());
        ticketParameter.setFirstname(ticketData.getFirstname());
        ticketParameter.setLastname(ticketData.getLastname());
        ticketParameter.setEmail(ticketData.getEmail());
        if (ticketData.isCustomerRegistrationTicket()) {
            ticketParameter.setCustomerRegistrationTicket(ticketData.isCustomerRegistrationTicket());
            ticketParameter.setCategory(CsTicketCategory.ENQUIRY);
            ticketParameter.setPriority(ticketData.getPriority());
            ticketParameter.setState(CsTicketState.NEW);
            ticketParameter.setBusiness(ticketData.getBusiness());
            ticketParameter.setCustomerNumber(ticketData.getCustomerId());
            ticketParameter.setPhone(ticketData.getPhone());
        } else {
            ticketParameter.setCategory(CsTicketCategory.valueOf(ticketData.getCsTicketCategory().getCode()));
            ticketParameter.setPriority(getEnumerationService().getEnumerationValue(CsTicketPriority._TYPECODE, ticketPriority));
        }
        return ticketParameter;
    }

    private void addTitleIfPresent(CsTicketParameter ticketParameter, TicketData ticketData) {
        if (StringUtils.isNotEmpty(ticketData.getTitle())) {
            final TitleData titleData = new TitleData();
            titleData.setCode(ticketData.getTitle());
            ticketParameter.setTitle(titleData);
        }
    }

    @Override
    public void setTicketReason(String ticketReason) {
        this.ticketReason = ticketReason;
    }

    @Override
    public void setTicketPriority(String ticketPriority) {
        this.ticketPriority = ticketPriority;
    }
}

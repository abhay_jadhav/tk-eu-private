package com.thyssenkrupp.b2b.eu.facades.order;

import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusResponse;

public interface TkEuDocumentDownloadFacade {

    boolean checkIfAuthorizedUser(DocumentDownloadRequest documents) throws Exception;

    GetDocumentStatusResponse getDocumentStatus(DocumentDownloadRequest documents) throws Exception;

    GetDocumentContentResponse getDocument(DocumentDownloadRequest documents) throws Exception;
}

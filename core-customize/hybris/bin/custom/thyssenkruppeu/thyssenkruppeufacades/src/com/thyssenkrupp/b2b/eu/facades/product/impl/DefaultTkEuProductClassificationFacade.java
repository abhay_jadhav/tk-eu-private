package com.thyssenkrupp.b2b.eu.facades.product.impl;

import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductClassificationFacade;
import de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultTkEuProductClassificationFacade implements TkEuProductClassificationFacade {

    @Override
    public void extractVisibleClassificationFeatures(ProductData productData) {
        Collection<ClassificationData> classifications = productData.getClassifications();
        if (CollectionUtils.isNotEmpty(classifications)) {
            Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> featureMap = classifications.stream()
                .map(classificationData -> classificationData.getFeatures()).flatMap(featureData -> featureData.stream()).filter(feature -> feature.getVisibility() != null)
                .collect(Collectors.groupingBy(feature -> feature.getVisibility()));
            sortVisibleFeatureMap(featureMap);
            productData.setVisibleFeatures(featureMap);
        }
    }

    @Override
    public Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> mergeClassificationFeatures(@NotNull ProductData masterProduct, @NotNull ProductData variantProduct) {
        final Map<String, Collection<FeatureData>> variantFeatureMap = collectFeatureMap(variantProduct);
        final Map<String, Collection<FeatureData>> masterFeatureMap = collectFeatureMap(masterProduct);
        final Collection<FeatureData> mergedVariantFeatures = new ArrayList<>();
        iterateOverVariantFeatures(variantFeatureMap, masterFeatureMap, mergedVariantFeatures);
        return extractVisibleClassificationFeatures(mergedVariantFeatures);
    }

    private void iterateOverVariantFeatures(Map<String, Collection<FeatureData>> variantFeatureMap, Map<String, Collection<FeatureData>> masterFeatureMap, Collection<FeatureData> mergedVariantFeatures) {
        if (MapUtils.isNotEmpty(variantFeatureMap)) {
            variantFeatureMap.forEach((k, v) -> {
                Collection<FeatureData> features = masterFeatureMap.get(k);
                Collection<FeatureData> mergedFeatures = new ArrayList<>(v);
                mergeFeatures(features, mergedFeatures);
                mergedVariantFeatures.addAll(mergedFeatures);
            });
        } else {
            List<FeatureData> mergedFeatures = masterFeatureMap.values().stream().flatMap(featureData -> featureData.stream()).collect(Collectors.toList());
            mergedVariantFeatures.addAll(mergedFeatures);
        }
    }

    private Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> extractVisibleClassificationFeatures(Collection<FeatureData> mergedVariantFeatures) {
        Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> visibleFeatureMap = mergedVariantFeatures.stream().collect(Collectors.groupingBy(feature -> feature.getVisibility()));
        sortVisibleFeatureMap(visibleFeatureMap);
        return visibleFeatureMap;
    }

    private void mergeFeatures(Collection<FeatureData> features, @NotNull Collection<FeatureData> mergedFeatures) {
        List<String> featureCodes = mergedFeatures.stream().map(featureData1 -> featureData1.getCode()).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(features)) {
            for (FeatureData featureData : features) {
                if (!featureCodes.contains(featureData.getCode())) {
                    mergedFeatures.add(featureData);
                }
            }
        }
    }

    private void sortVisibleFeatureMap(@NotNull Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> featureMap) {
        featureMap.forEach((k, v) -> v.sort(Comparator.comparing(FeatureData::getPosition, Comparator.nullsLast(Integer::compareTo))));
    }

    private Map<String, Collection<FeatureData>> collectFeatureMap(ProductData productData) {
        if (CollectionUtils.isNotEmpty(productData.getClassifications())) {
            return productData.getClassifications().stream().collect(Collectors.toMap(classifications -> classifications.getCode(), classifications -> classifications.getFeatures()));
        } else {
            return Collections.EMPTY_MAP;
        }
    }
}

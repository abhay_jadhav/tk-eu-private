package com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice.widgets.editors.boostitemreference;


import com.hybris.cockpitng.editor.defaultreferenceeditor.DefaultReferenceEditor;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import de.hybris.platform.adaptivesearch.data.AsIndexTypeData;
import de.hybris.platform.adaptivesearch.model.*;
import de.hybris.platform.adaptivesearch.strategies.AsSearchProvider;
import de.hybris.platform.adaptivesearch.strategies.AsSearchProviderFactory;
import de.hybris.platform.adaptivesearchbackoffice.editors.EditorRuntimeException;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TkEuBoostItemReferenceEditor extends DefaultReferenceEditor<Item> {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuBoostItemReferenceEditor.class);
    private static final String SEARCH_CONFIGURATION = "searchConfiguration";
    private static final String SEARCH_PROFILE = "searchProfile";

    @Resource
    private TkEuCatalogVersionService catalogVersionService;

    @Resource
    private ModelService modelService;

    @Resource
    private AsSearchProviderFactory asSearchProviderFactory;

    public TkEuBoostItemReferenceEditor() {
        // empty constructor
    }

    @Override
    public void render(Component parent, EditorContext<Item> context, EditorListener<Item> listener) {
        AbstractAsBoostItemConfigurationModel parentObject = (AbstractAsBoostItemConfigurationModel) context.getParameters().get("parentObject");
        AbstractAsConfigurableSearchConfigurationModel searchConfiguration = (AbstractAsConfigurableSearchConfigurationModel) this.modelService.getAttributeValue(parentObject, SEARCH_CONFIGURATION);
        AbstractAsSearchProfileModel searchProfile = (AbstractAsSearchProfileModel) this.modelService.getAttributeValue(searchConfiguration, SEARCH_PROFILE);
        AsSearchProvider searchProvider = this.asSearchProviderFactory.getSearchProvider();
        Optional<AsIndexTypeData> indexTypeData = searchProvider.getIndexTypeForCode(searchProfile.getIndexType());
        if (indexTypeData.isPresent()) {
            context.setParameter("restrictToType", ((AsIndexTypeData) indexTypeData.get()).getItemType());
            if (((AsIndexTypeData) indexTypeData.get()).isCatalogVersionAware()) {
                context.setParameter("referenceSearchCondition_catalogVersion", getProductCatalogVersions(searchProfile));
            }
            super.render(parent, context, listener);
        } else {
            throw new EditorRuntimeException("Index property not found: " + searchProfile.getIndexType());
        }
    }

    private CatalogVersionModel getProductCatalogVersions(AbstractAsSearchProfileModel searchProfile) {
        if (searchProfile != null) {
            return catalogVersionService.getDependantCatalogVersion(searchProfile.getCatalogVersion());
        }
        LOG.warn("Search profile should not be null.");
        return null;
    }

    protected void populateContextWithPKCondition(EditorContext<Item> context, AbstractAsConfigurableSearchConfigurationModel searchConfiguration) {
        List<PK> pks = this.resolveConditionValue(searchConfiguration);
        context.setParameter("referenceSearchCondition_pk_doesNotContain", pks.stream().map(PK::getLongValueAsString).collect(Collectors.joining(",")));
    }

    protected List<PK> resolveConditionValue(AbstractAsConfigurableSearchConfigurationModel searchConfiguration) {
        List<AsPromotedItemModel> promotedItems = searchConfiguration.getPromotedItems();
        List<AsExcludedItemModel> excludedItems = searchConfiguration.getExcludedItems();
        List<AbstractAsBoostItemConfigurationModel> results = (List) Stream.concat(promotedItems.stream(), excludedItems.stream()).collect(Collectors.toList());
        return (List) results.stream().map((resultConfiguration) -> {
            ProductModel product = (ProductModel) resultConfiguration.getItem();
            return product.getPk();
        }).collect(Collectors.toList());
    }

    public TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

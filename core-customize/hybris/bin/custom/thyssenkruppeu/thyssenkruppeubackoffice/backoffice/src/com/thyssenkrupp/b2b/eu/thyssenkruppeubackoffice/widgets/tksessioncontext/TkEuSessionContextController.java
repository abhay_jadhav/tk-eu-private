package com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice.widgets.tksessioncontext;

import com.hybris.cockpitng.annotations.ViewEvent;
import com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice.widgets.tksessioncontext.util.TkEuAsmUtils;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.customersupportbackoffice.widgets.sessioncontext.SessionContextController;
import de.hybris.platform.customersupportbackoffice.widgets.sessioncontext.model.SessionContextModel;
import de.hybris.platform.customersupportbackoffice.widgets.sessioncontext.util.SessionContextUtil;
import org.zkoss.zk.ui.Executions;

public class TkEuSessionContextController extends SessionContextController {

    @ViewEvent(
            componentID = "asmBtn",
            eventName = "onClick"
    )
    @Override
    public void launchASM() {
        SessionContextModel currentSessionContext = SessionContextUtil.getCurrentSessionContext(this.getWidgetInstanceManager().getModel());
        Executions.getCurrent().sendRedirect(TkEuAsmUtils.getAsmDeepLink((BaseSiteModel) this.availableSites.getModel().getElementAt(this.availableSites.getSelectedIndex()), currentSessionContext), "_blank");

    }
}

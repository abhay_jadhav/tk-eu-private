package com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice.widgets.tksessioncontext.util;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.customersupportbackoffice.widgets.sessioncontext.model.SessionContextModel;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;

import java.util.Iterator;

public final class TkEuAsmUtils {

    private TkEuAsmUtils() {
    }


    public static String getAsmDeepLink(BaseSiteModel currentSite, SessionContextModel sessionContext) {
        String deepLink = Config.getParameter("website.asm." + currentSite.getUid() + ".url");
        if (sessionContext != null && sessionContext.getCurrentCustomer() != null) {
            deepLink = deepLink + Config.getParameter("assistedservicestorefront.deeplink.link") + "?customerId=" + sessionContext.getCurrentCustomer().getUid();
            if (sessionContext.getCurrentOrder() != null) {
                deepLink = deepLink + "&orderId=" + sessionContext.getCurrentOrder().getGuid();
                return deepLink;
            }

            if (CollectionUtils.isNotEmpty(sessionContext.getCurrentCustomer().getCarts())) {
                Iterator var4 = sessionContext.getCurrentCustomer().getCarts().iterator();

                while (var4.hasNext()) {
                    CartModel cart = (CartModel) var4.next();
                    if (cart.getSite().getUid().equals(currentSite.getUid())) {
                        deepLink = deepLink + "&cartId=" + cart.getCode();
                        break;
                    }
                }
            }
        } else {
            deepLink = deepLink + "?asm=true";
        }

        return deepLink;
    }
}

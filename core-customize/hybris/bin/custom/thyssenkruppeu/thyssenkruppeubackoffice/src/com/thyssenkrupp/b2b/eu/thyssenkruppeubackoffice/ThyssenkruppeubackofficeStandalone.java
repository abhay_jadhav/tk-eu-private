package com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ThyssenkruppeubackofficeStandalone {

    private static final Logger LOG = LoggerFactory.getLogger(ThyssenkruppeubackofficeStandalone.class);

    public static void main(final String[] args) {
        new ThyssenkruppeubackofficeStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        LOG.info("Session ID: {}", jaloSession.getSessionID());
        LOG.info("User: {}", jaloSession.getUser());
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}

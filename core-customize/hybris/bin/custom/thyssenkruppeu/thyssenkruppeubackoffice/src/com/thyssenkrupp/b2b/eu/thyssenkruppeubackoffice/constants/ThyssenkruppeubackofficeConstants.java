package com.thyssenkrupp.b2b.eu.thyssenkruppeubackoffice.constants;


public final class ThyssenkruppeubackofficeConstants extends GeneratedThyssenkruppeubackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeubackoffice";

    private ThyssenkruppeubackofficeConstants() {

    }
}

package com.thyssenkrupp.b2b.eu.promotionengine.constants;

public final class ThyssenkruppeupromotionengineConstants
    extends GeneratedThyssenkruppeupromotionengineConstants {
  public static final String EXTENSIONNAME = "thyssenkruppeupromotionengine";
  public static final String PLATFORM_LOGO_CODE = "thyssenkruppeupromotionenginePlatformLogo";

  private ThyssenkruppeupromotionengineConstants() {
    //empty to avoid instantiating this constant class
  }


}

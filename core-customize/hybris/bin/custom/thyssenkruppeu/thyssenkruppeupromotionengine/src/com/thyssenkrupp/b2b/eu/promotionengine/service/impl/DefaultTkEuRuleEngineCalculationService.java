package com.thyssenkrupp.b2b.eu.promotionengine.service.impl;

import de.hybris.order.calculation.domain.Order;
import de.hybris.platform.ruleengineservices.calculation.NumberedLineItem;
import de.hybris.platform.ruleengineservices.calculation.impl.DefaultRuleEngineCalculationService;
import de.hybris.platform.ruleengineservices.rao.AbstractOrderRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;

import com.google.common.base.Preconditions;
import com.thyssenkrupp.b2b.eu.promotionengine.service.TkEuRuleEngineCalculationService;


public class DefaultTkEuRuleEngineCalculationService extends DefaultRuleEngineCalculationService
    implements TkEuRuleEngineCalculationService {

  @Override
  public DiscountRAO addOrderEntryLevelFixedDiscount(final OrderEntryRAO orderEntryRao,
      final boolean absolute, final BigDecimal amount) {
    ServicesUtil.validateParameterNotNull(orderEntryRao, "order entry rao must not be null");
    ServicesUtil.validateParameterNotNull(orderEntryRao.getOrder(),
        "corresponding entry cart rao must not be null");
    ServicesUtil.validateParameterNotNull(amount, "amount must not be null");
    return this.addOrderEntryLevelDiscountForSubTotal(orderEntryRao, absolute, amount,
        this.getConsumedQuantityForOrderEntry(orderEntryRao));
  }

  @Override
  public DiscountRAO addOrderEntryLevelDiscountForSubTotal(final OrderEntryRAO orderEntryRao,
      final boolean absolute, final BigDecimal amount, final int consumedQuantityForOrderEntry) {
    Preconditions.checkArgument(consumedQuantityForOrderEntry >= 0,
        "consumed quantity can't be negative");
    final Order cart = this.getAbstractOrderRaoToOrderConverter().convert(orderEntryRao.getOrder());
    final NumberedLineItem lineItem = this.findLineItem(cart, orderEntryRao);
    final int qty = orderEntryRao.getQuantity();
    final BigDecimal adjustedAmount = absolute ? amount
        : this.convertPercentageDiscountToAbsoluteDiscount(amount, qty, lineItem);
    final DiscountRAO discountRAO = this.createAbsoluteDiscountRAO(lineItem, adjustedAmount, qty,
        true);
    this.getRaoUtils().addAction(orderEntryRao, discountRAO);
    final AbstractOrderRAO cartRao = orderEntryRao.getOrder();
    this.recalculateTotals(cartRao, cart);
    return discountRAO;
  }
}

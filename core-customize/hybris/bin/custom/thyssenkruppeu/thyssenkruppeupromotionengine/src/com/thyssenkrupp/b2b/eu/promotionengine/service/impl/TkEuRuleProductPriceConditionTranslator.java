package com.thyssenkrupp.b2b.eu.promotionengine.service.impl;


import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruledefinitions.conditions.AbstractRuleConditionTranslator;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeRelConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Lists;


public class TkEuRuleProductPriceConditionTranslator extends AbstractRuleConditionTranslator {

  @Override
  public RuleIrCondition translate(final RuleCompilerContext context,
      final RuleConditionData condition, final RuleConditionDefinitionData conditionDefinition) {
    final Map<String, RuleParameterData> conditionParameters = condition.getParameters();
    final RuleParameterData operatorParameter = conditionParameters.get("operator");
    final RuleParameterData valueParameter = conditionParameters.get("value");
    if (this.verifyAllPresent(new Object[]
    { operatorParameter, valueParameter })) {
      final AmountOperator operator = (AmountOperator) operatorParameter.getValue();
      final Map<String, BigDecimal> value = (Map) valueParameter.getValue();
      if (this.verifyAllPresent(new Object[]
      { operator, value })) {
        return this.getProductPriceConditions(context, operator, value);
      }
    }

    return IrConditions.empty();
  }

  protected RuleIrGroupCondition getProductPriceConditions(final RuleCompilerContext context,
      final AmountOperator operator, final Map<String, BigDecimal> value) {
    final RuleIrGroupCondition irGroupCondition = RuleIrGroupConditionBuilder
        .newGroupConditionOf(RuleIrGroupOperator.OR).build();
    this.addProductPriceConditions(context, operator, value, irGroupCondition);
    return irGroupCondition;
  }

  protected void addProductPriceConditions(final RuleCompilerContext context,
      final AmountOperator operator, final Map<String, BigDecimal> value,
      final RuleIrGroupCondition irGroupCondition) {
    final String orderEntryRaoVariable = context.generateVariable(OrderEntryRAO.class);
    final String cartRaoVariable = context.generateVariable(CartRAO.class);
    final Iterator var8 = value.entrySet().iterator();

    while (var8.hasNext()) {
      final Entry<String, BigDecimal> entry = (Entry) var8.next();
      if (this.verifyAllPresent(new Object[]
      { entry.getKey(), entry.getValue() })) {
        final List<RuleIrCondition> conditions = Lists.newArrayList();
        final RuleIrGroupCondition irCurrencyGroupCondition = RuleIrGroupConditionBuilder
            .newGroupConditionOf(RuleIrGroupOperator.AND).withChildren(conditions).build();
        conditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(cartRaoVariable)
            .withAttribute("currencyIsoCode").withOperator(RuleIrAttributeOperator.EQUAL)
            .withValue(entry.getKey()).build());
        conditions.add(RuleIrAttributeConditionBuilder
            .newAttributeConditionFor(orderEntryRaoVariable).withAttribute("totalPrice")
            .withOperator(RuleIrAttributeOperator.valueOf(operator.name()))
            .withValue(entry.getValue()).build());
        conditions.add(
            RuleIrAttributeRelConditionBuilder.newAttributeRelationConditionFor(cartRaoVariable)
                .withAttribute("entries").withOperator(RuleIrAttributeOperator.CONTAINS)
                .withTargetVariable(orderEntryRaoVariable).build());
        conditions.addAll(this.createProductConsumedCondition(context, orderEntryRaoVariable));
        irGroupCondition.getChildren().add(irCurrencyGroupCondition);
      }
    }

  }
}

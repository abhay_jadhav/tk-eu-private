package com.thyssenkrupp.b2b.eu.promotionengine.setup;

import static com.thyssenkrupp.b2b.eu.promotionengine.constants.ThyssenkruppeupromotionengineConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.thyssenkrupp.b2b.eu.promotionengine.constants.ThyssenkruppeupromotionengineConstants;
import com.thyssenkrupp.b2b.eu.promotionengine.service.ThyssenkruppeupromotionengineService;


@SystemSetup(extension = ThyssenkruppeupromotionengineConstants.EXTENSIONNAME)
public class ThyssenkruppeupromotionengineSystemSetup {
  private final ThyssenkruppeupromotionengineService thyssenkruppeupromotionengineService;

  public ThyssenkruppeupromotionengineSystemSetup(
      final ThyssenkruppeupromotionengineService thyssenkruppeupromotionengineService) {
    this.thyssenkruppeupromotionengineService = thyssenkruppeupromotionengineService;
  }

  @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
  public void createEssentialData() {
    thyssenkruppeupromotionengineService.createLogo(PLATFORM_LOGO_CODE);
  }

  private InputStream getImageStream() {
    return ThyssenkruppeupromotionengineSystemSetup.class
        .getResourceAsStream("/thyssenkruppeupromotionengine/sap-hybris-platform.png");
  }
}

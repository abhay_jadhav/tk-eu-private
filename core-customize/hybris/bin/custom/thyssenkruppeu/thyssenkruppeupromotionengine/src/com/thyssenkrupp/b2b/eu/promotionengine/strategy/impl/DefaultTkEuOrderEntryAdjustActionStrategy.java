
package com.thyssenkrupp.b2b.eu.promotionengine.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotionengineservices.action.impl.DefaultOrderEntryAdjustActionStrategy;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultTkEuOrderEntryAdjustActionStrategy
    extends DefaultOrderEntryAdjustActionStrategy {
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultOrderEntryAdjustActionStrategy.class);

  @Override
  public List<PromotionResultModel> apply(final AbstractRuleActionRAO action) {
    if (!(action instanceof DiscountRAO)) {
      LOG.error("cannot apply {}, action is not of type DiscountRAO",
          this.getClass().getSimpleName());
      return Collections.emptyList();
    } else {
      final AbstractOrderEntryModel entry = this.getPromotionActionService().getOrderEntry(action);
      if (entry == null) {
        LOG.error("cannot apply {}, orderEntry could not be found.",
            this.getClass().getSimpleName());
        return Collections.emptyList();
      } else {
        final PromotionResultModel promoResult = this.getPromotionActionService()
            .createPromotionResult(action);
        if (promoResult == null) {
          LOG.error("cannot apply {}, promotionResult could not be created.",
              this.getClass().getSimpleName());
          return Collections.emptyList();
        } else {
          final AbstractOrderModel order = entry.getOrder();
          if (order == null) {
            LOG.error("cannot apply {}, order does not exist for order entry",
                this.getClass().getSimpleName());
            if (this.getModelService().isNew(promoResult)) {
              this.getModelService().detach(promoResult);
            }

            return Collections.emptyList();
          } else {
            final DiscountRAO discountRao = (DiscountRAO) action;
            final BigDecimal discountAmount = discountRao.getValue()
                .multiply(BigDecimal.valueOf(discountRao.getAppliedToQuantity()));
            this.adjustDiscountRaoValue(entry, discountRao, discountAmount);
            final RuleBasedOrderEntryAdjustActionModel actionModel = this
                .createOrderEntryAdjustAction(promoResult, action, entry, discountAmount);
            this.handleActionMetadata(action, actionModel);
            this.getPromotionActionService().createDiscountValue(discountRao, actionModel.getGuid(),
                entry);
            this.getModelService().saveAll(new Object[]
            { promoResult, actionModel, order, entry });
            this.recalculateIfNeeded(order);
            return Collections.singletonList(promoResult);
          }
        }
      }
    }
  }
}

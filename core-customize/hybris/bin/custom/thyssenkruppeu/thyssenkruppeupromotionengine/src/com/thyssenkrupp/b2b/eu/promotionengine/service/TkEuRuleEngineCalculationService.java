package com.thyssenkrupp.b2b.eu.promotionengine.service;

import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;

import java.math.BigDecimal;


public interface TkEuRuleEngineCalculationService {
  DiscountRAO addOrderEntryLevelFixedDiscount(OrderEntryRAO orderEntryRao, boolean absolute,
      BigDecimal amount);

  DiscountRAO addOrderEntryLevelDiscountForSubTotal(OrderEntryRAO orderEntryRao, boolean absolute,
      BigDecimal amount, int consumedQuantityForOrderEntry);

}

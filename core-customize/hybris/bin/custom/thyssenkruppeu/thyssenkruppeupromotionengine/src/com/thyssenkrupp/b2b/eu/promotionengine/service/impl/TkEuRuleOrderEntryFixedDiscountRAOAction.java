package com.thyssenkrupp.b2b.eu.promotionengine.service.impl;

import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.AbstractRuleExecutableSupport;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.promotionengine.service.TkEuRuleEngineCalculationService;



public class TkEuRuleOrderEntryFixedDiscountRAOAction extends AbstractRuleExecutableSupport {
  private TkEuRuleEngineCalculationService tkEuRuleEngineCalculationService;

  @Override
  public boolean performActionInternal(final RuleActionContext context) {
    final Set<OrderEntryRAO> orderEntries = context.getValues(OrderEntryRAO.class);
    final Map<String, BigDecimal> values = (Map) context.getParameter("value");
    boolean isPerformed = false;
    final Iterator var6 = orderEntries.iterator();

    while (var6.hasNext()) {
      final OrderEntryRAO orderEntry = (OrderEntryRAO) var6.next();
      final BigDecimal valueForCurrency = values.get(orderEntry.getCurrencyIsoCode());
      if (Objects.nonNull(valueForCurrency)) {
        isPerformed |= this.performAction(context, orderEntry, valueForCurrency);
      }
    }

    return isPerformed;
  }

  protected boolean performAction(final RuleActionContext context,
      final OrderEntryRAO orderEntryRao, final BigDecimal valueForCurrency) {
    boolean isPerformed = false;
    final int consumableQuantity = this.getConsumableQuantity(orderEntryRao);
    if (consumableQuantity > 0) {
      isPerformed = true;
      final DiscountRAO discount = getTkEuRuleEngineCalculationService()
          .addOrderEntryLevelFixedDiscount(orderEntryRao, true, valueForCurrency);
      this.setRAOMetaData(context, new AbstractRuleActionRAO[]
      { discount });
      this.consumeOrderEntry(orderEntryRao, consumableQuantity,
          this.adjustUnitPrice(orderEntryRao, consumableQuantity), discount);
      final RuleEngineResultRAO result = context.getRuleEngineResultRao();
      result.getActions().add(discount);
      context.scheduleForUpdate(new Object[]
      { orderEntryRao, orderEntryRao.getOrder(), result });
      context.insertFacts(new Object[]
      { discount });
      context.insertFacts(discount.getConsumedEntries());
    }

    return isPerformed;
  }

  public TkEuRuleEngineCalculationService getTkEuRuleEngineCalculationService() {
    return tkEuRuleEngineCalculationService;
  }

  public void setTkEuRuleEngineCalculationService(
      final TkEuRuleEngineCalculationService tkEuRuleEngineCalculationService) {
    this.tkEuRuleEngineCalculationService = tkEuRuleEngineCalculationService;
  }
}

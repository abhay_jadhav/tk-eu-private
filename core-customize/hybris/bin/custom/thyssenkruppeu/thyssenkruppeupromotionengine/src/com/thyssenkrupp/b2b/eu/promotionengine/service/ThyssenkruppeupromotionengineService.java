package com.thyssenkrupp.b2b.eu.promotionengine.service;

public interface ThyssenkruppeupromotionengineService {
  String getHybrisLogoUrl(String logoCode);

  void createLogo(String logoCode);
}

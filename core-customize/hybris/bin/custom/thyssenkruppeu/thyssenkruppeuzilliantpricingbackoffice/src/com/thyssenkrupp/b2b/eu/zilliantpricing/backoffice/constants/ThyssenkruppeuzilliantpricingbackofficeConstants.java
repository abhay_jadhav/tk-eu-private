package com.thyssenkrupp.b2b.eu.zilliantpricing.backoffice.constants;

public final class ThyssenkruppeuzilliantpricingbackofficeConstants extends GeneratedThyssenkruppeuzilliantpricingbackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeuzilliantpricingbackoffice";

    private ThyssenkruppeuzilliantpricingbackofficeConstants() {
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.backoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.thyssenkrupp.b2b.eu.zilliantpricing.backoffice.services.ThyssenkruppeuzilliantpricingbackofficeService;

public class ThyssenkruppeuzilliantpricingbackofficeController extends DefaultWidgetController {
    private static final long  serialVersionUID = 1L;
    private              Label label;

    @WireVariable
    private transient ThyssenkruppeuzilliantpricingbackofficeService thyssenkruppeuzilliantpricingbackofficeService;

    @Override
    public void initialize(final Component comp) {
        super.initialize(comp);
        label.setValue(thyssenkruppeuzilliantpricingbackofficeService.getHello() + " ThyssenkruppeuzilliantpricingbackofficeController");
    }
}

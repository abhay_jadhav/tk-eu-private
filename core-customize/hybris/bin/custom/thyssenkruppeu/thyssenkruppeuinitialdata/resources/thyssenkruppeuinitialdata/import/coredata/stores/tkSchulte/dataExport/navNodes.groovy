import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.commercefacades.product.data.*
import de.hybris.platform.category.model.*
import de.hybris.platform.core.model.product.*
import de.hybris.platform.jalo.*
import de.hybris.platform.jalo.c2l.*
import java.util.*
import de.hybris.platform.cms2.model.navigation.*
import de.hybris.platform.servicelayer.search.SearchResult

flexibleSearchService = spring.getBean "flexibleSearchService"

def getMainNavNodeForNavCategories(catalogName, version, navIdForMainNavCategory) {

    String queryStr = "SELECT {PK} \
        FROM {CMSNavigationNode as p JOIN CatalogVersion as cv on {p:catalogVersion}={cv:PK} \
        JOIN Catalog as c on {cv.catalog}={c.PK}} \
        WHERE {cv.version}=\'${version}\' \
        AND {c.id}=\'${catalogName}\' \
        AND {p.uid}=\'${navIdForMainNavCategory}\'"

    query = new FlexibleSearchQuery(queryStr.toString())
    flexibleSearchService.search(query)?.result?.get(0)
}

def traverseNavNodes(navNode, navNodes) {
    for(CMSNavigationNodeModel node : navNode.children) {
        navNodes.add(node)
        traverseNavNodes(node, navNodes)
    }
}

getMainNavNodeForNavCategories("tkSchulteContentCatalog", "Staged", "TkSchulteCategoryNavNode").each {
    List <CMSNavigationNodeModel> navNodes = new ArrayList<>()

    traverseNavNodes(it, navNodes)

    String str = null

    for(int i=0; i<navNodes.size(); i++) {

        if(i==0)
            str = '\'' + navNodes.get(i).uid + '\''
        else
          str += ', \'' + navNodes.get(i).uid + '\''
    }

    out.println str

}

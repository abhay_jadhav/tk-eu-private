$productCatalog = tkSchulteProductCatalog
$categoryCatalog = tkSchulteCategoryCatalog
$classificationCatalog = tkSchulteClassification
$productCatalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$sourceCategoryCV = sourceVersion(catalog(id[default = $categoryCatalog]), version[default = 'Staged'])[unique = true, default = $categoryCatalog:Staged]
$targetCategoryCV = targetVersion(catalog(id[default = $categoryCatalog]), version[default = 'Online'])[unique = true, default = $categoryCatalog:Online]
$languages = en, de, de_DE, en_US

# Product catalog
INSERT_UPDATE Catalog; id[unique = true]
; $productCatalog
; $categoryCatalog

# Product versions for product catalogs
INSERT_UPDATE CatalogVersion; catalog(id)[unique = true]; version[unique = true]; active; languages(isoCode)
; $productCatalog  ; Staged ; false ; $languages
; $productCatalog  ; Online ; true  ; $languages
; $categoryCatalog ; Staged ; false ; $languages
; $categoryCatalog ; Online ; true  ; $languages

# adding dependant catalog version
INSERT_UPDATE CatalogVersion; catalog(id)[unique = true]; version[unique = true]; dependantCatalogVersion(catalog(id), version); dependsOnCatalogVersion(catalog(id), version); isProductCatalog [default = false];
; $productCatalog  ; Staged ;                        ; $categoryCatalog:Staged ; true ;
; $productCatalog  ; Online ;                        ; $categoryCatalog:Online ; true ;
; $categoryCatalog ; Staged ; $productCatalog:Staged ;                         ;      ;
; $categoryCatalog ; Online ; $productCatalog:Online ;                         ;      ;

# tkSample Classification catalog
INSERT_UPDATE ClassificationSystem; id[unique = true]
; $classificationCatalog

# Insert Classifications System Version
INSERT_UPDATE ClassificationSystemVersion; catalog(id)[unique = true]; version[unique = true]; active; inclPacking[virtual = true, default = true]; inclDuty[virtual = true, default = true]; inclFreight[virtual = true, default = true]; inclAssurance[virtual = true, default = true]
; $classificationCatalog ; 1.0 ; true

# Create default tax row for all products in product catalog
INSERT_UPDATE TaxRow; $productCatalogVersion; tax(code)[unique = true]; pg(code)[unique = true]; ug(code)[unique = true]
; ; de-vat-full ; de-vat-full ; de-taxes

# Sync job for the new category catalog
INSERT_UPDATE CatalogVersionSyncJob; code[unique = true]; $sourceCategoryCV; $targetCategoryCV;
; sync tkSchulteCategoryCatalog:Staged->Online ; ; ; ;

# Sync job for the adaptive seach
INSERT_UPDATE CatalogVersionSyncJob; code[unique = true]; roottypes(code)[mode = append];
; sync tkSchulteCategoryCatalog:Staged->Online ; AbstractAsSearchProfile,AbstractAsSearchConfiguration,AbstractAsFacetConfiguration,AbstractAsBoostItemConfiguration,AbstractAsBoostRuleConfiguration,AsSearchProfileActivationSet ;

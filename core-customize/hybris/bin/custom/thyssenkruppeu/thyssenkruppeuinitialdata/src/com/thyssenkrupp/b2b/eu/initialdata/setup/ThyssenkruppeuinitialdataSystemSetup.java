package com.thyssenkrupp.b2b.eu.initialdata.setup;

import com.thyssenkrupp.b2b.eu.initialdata.constants.ThyssenkruppeuinitialdataConstants;
import com.thyssenkrupp.b2b.eu.initialdata.dataimport.TkMmpCoreDataImportService;
import com.thyssenkrupp.b2b.eu.initialdata.dataimport.TkMmpSampleDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = ThyssenkruppeuinitialdataConstants.EXTENSIONNAME)
public class ThyssenkruppeuinitialdataSystemSetup extends AbstractSystemSetup {
    @SuppressWarnings("unused")
    private static final String IMPORT_SAMPLE_DATA_ENABLED_CONFIG_KEY = "system.tkeuinitialdata.sample.enabled";
    private static final Logger LOG                                   = Logger.getLogger(ThyssenkruppeuinitialdataSystemSetup.class);
    private static final String IMPORT_CORE_DATA                      = "importCoreData";
    private static final String ACTIVATE_SOLR_CRON_JOBS               = "activateSolrCronJobs";
    private static final String WEBSITE_LIST                          = "websiteList";
    private static final String TK_PLASTICS                           = "tkPlastics";
    private static final String TK_SCHULTE                            = "tkSchulte";
    private static final String IMPORT_SAP_CONFIGURATION_DATA         = "importSapConfigurationData";
    private static final String IMPORT_SAMPLE_DATA                    = "importSampleData";
    private static final String DEV                                   = "DEV";

    private CoreDataImportService        coreDataImportService;
    private SampleDataImportService      sampleDataImportService;
    private ConfigurationService         configurationService;
    private TkMmpCoreDataImportService   tkMmpCoreDataImportService;
    private TkMmpSampleDataImportService tkMmpSampleDataImportService;

    /**
     * This method implement the logic to handle the logic to get the value from the local.properties
     *
     * @return
     */
    public boolean isSampleDataEnabled() {
        if (null != getConfigurationService()) {
            return getConfigurationService().getConfiguration().getBoolean(IMPORT_SAMPLE_DATA_ENABLED_CONFIG_KEY, true);
        }
        return false;
    }

    /**
     * This implement the logic to handle the values shown in the admin concole i.e.yes/no based on the attribute/property in the local.properties
     *
     * @param key
     * @param label
     * @param defaultValue
     * @return
     */
    public SystemSetupParameter createBooleanSystemSetUpParameterForSampleData(final String key, final String label, final boolean defaultValue) {
        final SystemSetupParameter syncProductsParam = new SystemSetupParameter(key);
        syncProductsParam.setLabel(label);
        if (isSampleDataEnabled()) {
            syncProductsParam.addValue(BOOLEAN_TRUE, defaultValue);
            syncProductsParam.addValue(BOOLEAN_FALSE, !defaultValue);
        } else {
            syncProductsParam.addValue(BOOLEAN_FALSE, !defaultValue);
        }
        return syncProductsParam;
    }

    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        final String[] websites = new String[] { TK_PLASTICS, TK_SCHULTE };
        params.add(createWebsiteListSystemSetupParameter(websites));
        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", false));
        params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", false));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAP_CONFIGURATION_DATA, "Import SAP Configuration Data", false));
        params.add(createBooleanSystemSetUpParameterForSampleData(IMPORT_SAMPLE_DATA, "Import Sample Data", false));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAP_CONFIGURATION_DATA, "Import SAP Configuration Data", false));
        params.add(createBooleanSystemSetupParameter(ThyssenkruppeuinitialdataConstants.IMPORT_MMP_CORE_DATA, "Import MMP Core Data", true));
        params.add(createBooleanSystemSetupParameter(ThyssenkruppeuinitialdataConstants.IMPORT_MMP_SAMPLE_DATA, "Import MMP Sample Data", true));

        return params;
    }

    /**
     * @param websites
     * @return
     */
    SystemSetupParameter createWebsiteListSystemSetupParameter(final String[] websites) {
        final SystemSetupParameter param = new SystemSetupParameter(WEBSITE_LIST);
        param.setLabel("Choose the websites");
        param.addValues(websites);
        param.setMultiSelect(true);
        return param;
    }

    /**
     * Implement this method to create initial objects. This method will be called by system creator during initialization and system update. Be sure that this method can be called repeatedly.
     *
     * @param context the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        // Add Essential Data here as you require
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system initialization. <br>
     * Add import data for each site you have configured
     *
     * @param context the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        final List<String> websites = new ArrayList<>();
        final List<ImportData> importDataList = new ArrayList<>();
        websites.add(TK_SCHULTE);
        // websites.add(TK_PLASTICS);
        for (final String website : websites) {
            final ImportData importData = createImportData(website);
            importDataList.add(importData);
            LOG.info("Adding website ::[" + website + "]");
        }
        getCoreDataImportService().execute(this, context, importDataList);
        LOG.info("Sample Data Flag is set to [" + isSampleDataEnabled() + "]");
        if (isSampleDataEnabled()) {
            LOG.info("Inside Sample Data Loop and the value is set to [" + isSampleDataEnabled() + "]");
            getSampleDataImportService().execute(this, context, importDataList);
        }
        // importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/productCatalogs/tkPlasticsProductCatalog/plastics-dependent-catalog-relation.impex", true);
        importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/productCatalogs/tkSchulteProductCatalog/schulte-dependent-catalog-relation.impex", true);
        if (Process.INIT.equals(context.getProcess())) {
            importImpexFile(context, "/thyssenkruppeuinitialdata/import/sampledata/productCatalogs/tkSchulteProductCatalog/tradelengthconfigurations.impex", true);
        }
        importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/common/site-essential-data.impex", true);

        //importMMPData(context);
        getTkMmpCoreDataImportService().execute(this, context, importDataList);
        getTkMmpSampleDataImportService().execute(this, context, importDataList);

        importCoreData(context);
    }

    private void importCoreData(SystemSetupContext context) {
        String environmentName = getEnvironmentId();

        final boolean importSapConfigurationData = getBooleanSystemSetupParameter(context, IMPORT_SAP_CONFIGURATION_DATA);

        if (importSapConfigurationData) {
            importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/common/sap-mvp-stock.impex", true);
            importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/common/sap-terms.impex", true);

            if (StringUtils.isNotBlank(environmentName)) {
                this.logInfo(context, String.format("Importing [%s] environment specific files for [%s]", environmentName, context.getExtensionName()));
                importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/environments/" + environmentName + "/sap-configuration.impex", true);
                importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/environments/" + environmentName + "/sap-mapping.impex", true);
                importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/environments/" + environmentName + "/solr.impex", false);
            }
            importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/environments/sap-configuration_de_DE.impex", true);
            importImpexFile(context, "/thyssenkruppeuinitialdata/import/coredata/environments/sap-configuration_en_US.impex", true);
        }
    }

    /**
     * @return environmentId
     */
    public String getEnvironmentId() {
        return getConfigurationService() != null ? getConfigurationService().getConfiguration().getString("environmentId") : "";
    }

    /**
     *
     */
    private ImportData createImportData(final String siteName) {
        final ImportData importData = new ImportData();
        importData.setProductCatalogName(siteName);
        importData.setContentCatalogNames(Arrays.asList(siteName));
        importData.setStoreNames(Arrays.asList(siteName));
        return importData;
    }

    public CoreDataImportService getCoreDataImportService() {
        return coreDataImportService;
    }

    @Required
    public void setCoreDataImportService(final CoreDataImportService coreDataImportService) {
        this.coreDataImportService = coreDataImportService;
    }

    public SampleDataImportService getSampleDataImportService() {
        return sampleDataImportService;
    }

    @Required
    public void setSampleDataImportService(final SampleDataImportService sampleDataImportService) {
        this.sampleDataImportService = sampleDataImportService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public TkMmpCoreDataImportService getTkMmpCoreDataImportService() {
        return tkMmpCoreDataImportService;
    }

    public void setTkMmpCoreDataImportService(TkMmpCoreDataImportService tkMmpCoreDataImportService) {
        this.tkMmpCoreDataImportService = tkMmpCoreDataImportService;
    }

    public TkMmpSampleDataImportService getTkMmpSampleDataImportService() {
        return tkMmpSampleDataImportService;
    }

    public void setTkMmpSampleDataImportService(TkMmpSampleDataImportService tkMmpSampleDataImportService) {
        this.tkMmpSampleDataImportService = tkMmpSampleDataImportService;
    }
}

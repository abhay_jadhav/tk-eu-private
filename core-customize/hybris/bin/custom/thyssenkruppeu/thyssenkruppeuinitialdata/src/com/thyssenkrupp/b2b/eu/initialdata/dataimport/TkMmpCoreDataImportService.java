package com.thyssenkrupp.b2b.eu.initialdata.dataimport;

import com.thyssenkrupp.b2b.eu.initialdata.constants.ThyssenkruppeuinitialdataConstants;
import de.hybris.platform.commerceservices.dataimport.AbstractDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.validation.services.ValidationService;

import java.util.List;

public class TkMmpCoreDataImportService extends AbstractDataImportService {
    @Override
    public void execute(AbstractSystemSetup systemSetup, SystemSetupContext context, List<ImportData> importData) {
        final boolean importCoreData = systemSetup.getBooleanSystemSetupParameter(context, ThyssenkruppeuinitialdataConstants.IMPORT_MMP_CORE_DATA);

        if (importCoreData) {
            for (final ImportData data : importData) {
                importAllData(systemSetup, context, data, false);
            }

            final ValidationService validation = getBeanForName("validationService");
            validation.reloadValidationEngine();
        }
    }

    @Override
    protected void importCommonData(String extensionName) {
        getSetupImpexService().importImpexFile(String.format("/%s/import/mmp/coredata/common/tkLocalizedMessages.impex", extensionName),
          true);
    }

    @Override
    protected void importProductCatalog(String extensionName, String productCatalogName) {
        // IMPORT PRODUCT CATALOG

    }

    @Override
    protected void importContentCatalog(String extensionName, String contentCatalogName) {
        getSetupImpexService()
          .importImpexFile(
            String.format("/%s/import/mmp/coredata/contentCatalogs/%sContentCatalog/cms-content.impex", extensionName,
              contentCatalogName), false);

        getSetupImpexService()
                .importImpexFile(
                        String.format("/%s/import/mmp/coredata/contentCatalogs/%sContentCatalog/emailImpex/email-content.impex", extensionName,
                                contentCatalogName), false);


    }

    @Override
    protected void importStore(String extensionName, String storeName, String productCatalogName) {
        getSetupImpexService().importImpexFile(String.format("/%s/import/mmp/coredata/stores/%s/store.impex", extensionName, storeName),
          false);
        getSetupImpexService().importImpexFile(
                String.format("/%s/import/mmp/coredata/stores/%s/cronjobs/tkEuCleanUpCronJobs.impex", extensionName, storeName), false);
    }

    @Override
    protected void importSolrIndex(String extensionName, String storeName) {

        getSetupImpexService().importImpexFile(String.format("/%s/import/mmp/coredata/stores/%s/solr.impex", extensionName, storeName),
          false);
    }

    @Override
    protected void importJobs(String extensionName, String storeName) {
        // IMPORT JOBS

    }
}

package com.thyssenkrupp.b2b.eu.initialdata.dataimport;

import com.thyssenkrupp.b2b.eu.initialdata.constants.ThyssenkruppeuinitialdataConstants;
import de.hybris.platform.commerceservices.dataimport.AbstractDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;

import de.hybris.platform.commerceservices.setup.data.ImportData;

import java.util.List;

public class TkMmpSampleDataImportService extends AbstractDataImportService {
    @Override
    public void execute(AbstractSystemSetup systemSetup, SystemSetupContext context, List<ImportData> importData) {

        final boolean importSampleData = systemSetup.getBooleanSystemSetupParameter(context, ThyssenkruppeuinitialdataConstants.IMPORT_MMP_SAMPLE_DATA);

        if (importSampleData) {
            for (final ImportData data : importData) {
                importAllData(systemSetup, context, data, true);
            }
        }
    }

    @Override
    protected void importCommonData(String extensionName) {
        // IMPORT COMMON DATA

    }

    @Override
    protected void importProductCatalog(String extensionName, String productCatalogName) {
        // Import Product Catalog Data

    }

    @Override
    protected void importContentCatalog(String extensionName, String contentCatalogName) {

        getSetupImpexService()
                .importImpexFile(
                        String.format("/%s/import/mmp/sampledata/contentCatalogs/%sContentCatalog/cms-content.impex", extensionName,
                                contentCatalogName), false);

    }

    @Override
    protected void importStore(String extensionName, String storeName, String productCatalogName) {
        // IMPORT STORE

    }

    @Override
    protected void importSolrIndex(String extensionName, String storeName) {
        //IMPORT SOLR INDEX

    }

    @Override
    protected void importJobs(String extensionName, String storeName) {
        //IMPORT JOBS

    }
}

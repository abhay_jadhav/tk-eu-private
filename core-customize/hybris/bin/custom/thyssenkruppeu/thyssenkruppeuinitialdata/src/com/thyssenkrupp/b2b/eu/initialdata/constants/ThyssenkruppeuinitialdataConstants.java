package com.thyssenkrupp.b2b.eu.initialdata.constants;

/**
 * Global class for all Thyssenkruppeuinitialdata constants.
 */
public final class ThyssenkruppeuinitialdataConstants extends GeneratedThyssenkruppeuinitialdataConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeuinitialdata";

    public static final String IMPORT_MMP_CORE_DATA = "importMmpCoreData";
    public static final String IMPORT_MMP_SAMPLE_DATA = "importMmpSampleData";

    private ThyssenkruppeuinitialdataConstants() {
        //empty
    }
}


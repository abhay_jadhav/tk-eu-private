/* global module */

module.exports = function() {

    var paths = {};

    paths.getE2eFiles = function getE2eFiles() {
        return [
            ['jsTests/**/e2e/**/*Test.js']
        ];
    };

    return paths;

}();

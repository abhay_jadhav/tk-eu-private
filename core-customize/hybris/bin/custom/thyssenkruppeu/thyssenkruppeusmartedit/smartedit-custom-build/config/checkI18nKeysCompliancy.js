/* global module */

module.exports = function() {

    return {
        prefix: {
            ignored: ['se.', 'type.'], // keys provided by smartedit-locales_en.properties
            expected: ['thyssenkruppeusmartedit.']
        },
        paths: {
            files: [
                "web/features/**/*Template.html",
                "web/features/**/*.js"
            ],
            properties: [
                "resources/localization/thyssenkruppeusmartedit-locales_en.properties",
                "gruntTasks/smartedit-locales_en.properties"
            ]
        }
    };
};

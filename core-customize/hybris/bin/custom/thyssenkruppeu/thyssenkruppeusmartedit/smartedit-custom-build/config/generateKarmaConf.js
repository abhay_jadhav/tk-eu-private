/* global module, require, global */

module.exports = function() {

    return {
        targets: ['generateSmarteditKarmaConf', 'generateSmarteditContainerKarmaConf'],
        config: function(data, conf) {

            var lodash = require('lodash');
            var path = require('path');
            // eslint-disable-next-line  no-unused-vars
            var paths = require('../paths');

            var pathsInBundle = global.smartedit.bundlePaths;
            var karmaSmartedit = require(path.resolve(pathsInBundle.external.generated.webpack.karmaSmartedit));
            var karmaSmarteditContainer = require(path.resolve(pathsInBundle.external.generated.webpack.karmaSmarteditContainer));

            var cmssmartedit = {

                singleRun: true,

                coverageReporter: {
                    // specify a common output directory
                    dir: 'jsTarget/test/thyssenkruppeusmartedit/coverage/',
                    reporters: [{
                        type: 'html',
                        subdir: 'report-html'
                    }, {
                        type: 'cobertura',
                        subdir: '.',
                        file: 'cobertura.xml'
                    }]
                },

                junitReporter: {
                    outputDir: 'jsTarget/test/thyssenkruppeusmartedit/junit/', // results will be saved as $outputDir/$browserName.xml
                    outputFile: 'testReport.xml' // if included, results will be saved as $outputDir/$browserName/$outputFile
                },

                files: lodash.concat(
                    pathsInBundle.test.unit.smarteditThirdPartyJsFiles,
                    pathsInBundle.test.unit.commonUtilModules, [
                        'jsTests/cmssmartedit/**/*Test.js',
                        {
                            // Images
                            pattern: 'web/webroot/images/**/*',
                            watched: false,
                            included: false,
                            served: true
                        }
                    ]
                ),

                proxies: {
                    '/thyssenkruppeusmartedit/images/': '/base/images/'
                },

                // list of files to exclude
                exclude: [
                    '**/index.ts',
                    '**/*.d.ts',
                    '*.d.ts'
                ],

                webpack: karmaSmartedit
            };


            var cmssmarteditContainer = {
                singleRun: true,

                coverageReporter: {
                    // specify a common output directory
                    dir: 'jsTests/coverage/',
                    reporters: [{
                        type: 'html',
                        subdir: 'report-html'
                    }, {
                        type: 'cobertura',
                        subdir: '.',
                        file: 'cobertura.xml'
                    }, ]
                },

                junitReporter: {
                    outputDir: 'jsTarget/test/thyssenkruppeusmarteditContainer/junit/', // results will be saved as $outputDir/$browserName.xml
                    outputFile: 'testReport.xml' // if included, results will be saved as $outputDir/$browserName/$outputFile
                },

                // list of files / patterns to load in the browser
                files: lodash.concat(
                    pathsInBundle.test.unit.smarteditContainerUnitTestFiles,
                    pathsInBundle.test.unit.commonUtilModules, [
                        'jsTests/cmssmarteditContainer/**/*Test.js',
                        {
                            // Images
                            pattern: 'web/webroot/images/**/*',
                            watched: false,
                            included: false,
                            served: true
                        }
                    ]
                ),

                proxies: {
                    '/thyssenkruppeusmartedit/images/': '/base/images/'
                },

                // list of files to exclude
                exclude: [
                    '**/index.ts',
                    '**/*.d.ts',
                    '*.d.ts'
                ],

                webpack: karmaSmarteditContainer
            };


            conf.generateSmarteditKarmaConf.data = lodash.merge(cmssmartedit, conf.generateSmarteditKarmaConf.data);

            conf.generateSmarteditContainerKarmaConf.data = lodash.merge(cmssmarteditContainer, conf.generateSmarteditContainerKarmaConf.data);


            return conf;
        }
    };
};

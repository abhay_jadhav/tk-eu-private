/* global module */

module.exports = function() {

    return {
        pattern: [
            'web/features/**/*.+(js|ts)',
            'jsTests/**/*.+(js|ts)'
        ]
    };
};

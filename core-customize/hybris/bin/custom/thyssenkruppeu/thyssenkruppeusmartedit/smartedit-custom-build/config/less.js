/* global module */

module.exports = function() {

    return {
        dev: {
            files: [{
                expand: true,
                cwd: 'web/features/styling',
                src: '*.less',
                dest: 'web/webroot/css/',
                ext: '.css'
            }]
        },
    };
};

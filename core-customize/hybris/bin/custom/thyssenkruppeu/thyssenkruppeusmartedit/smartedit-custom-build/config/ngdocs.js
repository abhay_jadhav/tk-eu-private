/* global module */

module.exports = function() {

    return {
        targets: ['thyssenkruppeusmartedit'],
        /*eslint-disable */
        config: function(data, conf) {
        /*eslint-enable */
            return {
                options: {
                    dest: 'jsTarget/docs',
                    title: "thyssenkruppeusmartedit API",
                    startPage: '/#/thyssenkruppeusmartedit',
                },
                thyssenkruppeusmartedit: {
                    api: true,
                    src: ['web/features/common/**/*.js', 'web/features/thyssenkruppeusmartedit/**/*.js', 'web/features/common/**/*.ts', 'web/features/thyssenkruppeusmartedit/**/*.ts'],
                    title: 'thyssenkruppeusmartedit'
                }
            };
        }
    };
};

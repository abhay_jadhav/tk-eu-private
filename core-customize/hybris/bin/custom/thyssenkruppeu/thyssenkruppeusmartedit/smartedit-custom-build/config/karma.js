/* global module */

module.exports = function() {

    return {
        targets: ['cmssmartedit', 'cmssmarteditContainer'],
        config: function(data, conf) {
            return {

                // just rename the targets to match the source folder names

                cmssmartedit: conf.unitSmartedit,

                cmssmarteditContainer: conf.unitSmarteditContainer,

            };
        }
    };
};

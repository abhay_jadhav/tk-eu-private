/* global module */

module.exports = function() {

    return {
        targets: ['files'],
        config: function(data, conf) {
            conf.files = [
                'web/features/**/*.js',
                'web/features/**/*.html',
                'jsTests/**/*.js',
                'smartedit-custom-build/**/*.+(js|html)'
            ];

            return conf;
        }
    };
};

/* global module */

module.exports = function() {

    return {
        pattern: [
            'jsTests/**/*.js',
            'jsTests/**/*.ts'
        ]
    };
};


package com.thyssenkrupp.b2b.eu.smartedit;

import org.apache.log4j.Logger;

/**
 * Simple test class to demonstrate how to include utility classes to your webmodule.
 */
public final class ThyssenkruppeusmarteditWebHelper {

    /**
     * Edit the local|project.properties to change logging behavior (properties log4j.*).
     */

    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(ThyssenkruppeusmarteditWebHelper.class.getName());

    private ThyssenkruppeusmarteditWebHelper() {
    }

    public static String getTestOutput() {
        return "testoutput";
    }
}

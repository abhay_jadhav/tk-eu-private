import * as angular from 'angular';

const thyssenkruppeusmarteditModule = angular
        .module('thyssenkruppeusmarteditModule', ['thyssenkruppeusmartedit/cmssmarteditTemplates'])
        .service('ycmsSampleService', () => {
            console.log('ycmsSampleService');
        });

export default thyssenkruppeusmarteditModule;

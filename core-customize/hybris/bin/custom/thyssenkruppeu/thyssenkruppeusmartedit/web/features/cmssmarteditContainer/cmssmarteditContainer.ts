import * as angular from 'angular';

const thyssenkruppeusmarteditModule = angular.module('thyssenkruppeusmarteditModule', ['genericEditorModule', 'thyssenkruppeusmartedit/cmssmarteditContainerTemplates']);
export default thyssenkruppeusmarteditModule;

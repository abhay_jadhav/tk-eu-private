/*global module, require*/

module.exports = function(grunt) {

    require('time-grunt')(grunt);
    require('./smartedit-build')(grunt).load();

    grunt.registerTask('generate', [
        'generateWebpackConfig',
        'generateTsConfig',
        'generateKarmaConf'
    ]);

    grunt.registerTask('docs', ['clean', 'ngdocs']);
    grunt.registerTask('linting', ['jshint', 'tslint']);
    grunt.registerTask('sanitize', ['jsbeautifier', 'tsformatter']);
    grunt.registerTask('test_only', ['generate', 'multiKarma']);
    grunt.registerTask('test', ['test_only']);
    grunt.registerTask('e2e', ['multiProtractor']);
    grunt.registerTask('e2e_max', ['multiProtractorMax']);
};

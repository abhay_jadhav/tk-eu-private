
package com.thyssenkrupp.b2b.eu.smartedit;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppeusmarteditStandalone {
    /**
     * Main class to be able to run it directly as a java program.
     *
     * @param args the arguments from commandline
     */
    public static void main(final String[] args) {
        new ThyssenkruppeusmarteditStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}

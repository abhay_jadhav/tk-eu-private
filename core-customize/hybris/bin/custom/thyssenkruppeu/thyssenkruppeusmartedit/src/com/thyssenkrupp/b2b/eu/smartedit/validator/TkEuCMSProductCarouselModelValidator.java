package com.thyssenkrupp.b2b.eu.smartedit.validator;

import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.cmsfacades.common.function.Validator;
import de.hybris.platform.cmsfacades.common.validator.ValidationErrorsProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.cmsfacades.common.validator.ValidationErrorBuilder.newValidationErrorBuilder;

public class TkEuCMSProductCarouselModelValidator implements Validator<ProductCarouselComponentModel> {

    private ValidationErrorsProvider validationErrorsProvider;

    @Override
    public void validate(final ProductCarouselComponentModel productCarouselModel) {
        List<ProductModel> products = new ArrayList<>();
        if (productCarouselModel.getProducts() != null && productCarouselModel.getProducts().size() > 0) {
            products.addAll(productCarouselModel.getProducts());
        }
        List<String> erpProductCodes = products.stream()
          .filter(p -> p instanceof ERPVariantProductModel)
          .map(p -> p.getCode())
          .collect(Collectors.toList());

        if (erpProductCodes != null && erpProductCodes.size() > 0) {
            getValidationErrorsProvider().getCurrentValidationErrors().add(newValidationErrorBuilder()
              .field(productCarouselModel.PRODUCTS)
              .errorCode("product.carousel.product.error.msg").errorArgs(new Object[] { erpProductCodes })
              .build());
        }
    }

    public ValidationErrorsProvider getValidationErrorsProvider() {
        return validationErrorsProvider;
    }

    public void setValidationErrorsProvider(ValidationErrorsProvider validationErrorsProvider) {
        this.validationErrorsProvider = validationErrorsProvider;
    }
}

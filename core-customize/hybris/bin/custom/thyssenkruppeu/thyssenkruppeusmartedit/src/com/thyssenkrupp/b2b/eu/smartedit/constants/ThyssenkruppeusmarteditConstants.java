
package com.thyssenkrupp.b2b.eu.smartedit.constants;

@SuppressWarnings("PMD")
public final class ThyssenkruppeusmarteditConstants extends GeneratedThyssenkruppeusmarteditConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeusmartedit";

    private ThyssenkruppeusmarteditConstants() {
        // Intentionally left empty.
    }
}

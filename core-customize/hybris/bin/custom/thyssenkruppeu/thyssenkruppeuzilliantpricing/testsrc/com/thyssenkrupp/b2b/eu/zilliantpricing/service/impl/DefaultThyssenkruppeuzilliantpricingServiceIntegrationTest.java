package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.ThyssenkruppeuzilliantpricingService;

@IntegrationTest
public class DefaultThyssenkruppeuzilliantpricingServiceIntegrationTest extends ServicelayerBaseTest {
    @Resource
    private ThyssenkruppeuzilliantpricingService thyssenkruppeuzilliantpricingService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldReturnProperUrlForLogo() throws Exception {
    }

    private MediaModel findLogoMedia(final String logoCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {PK} FROM {Media} WHERE {code}=?code");
        fQuery.addQueryParameter("code", logoCode);

        return flexibleSearchService.searchUnique(fQuery);
    }
}

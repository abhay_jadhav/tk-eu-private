package com.thyssenkrupp.b2b.eu.zilliantpricing.cronjob;

import com.thyssenkrupp.b2b.eu.zilliantpricing.integration.UnzipUtility;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuExtractZilliantFilesCronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

public class TkEuExtractZilliantFilesJob extends AbstractJobPerformable<TkEuExtractZilliantFilesCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkEuExtractZilliantFilesJob.class);
    private UnzipUtility unzipUtility;

    @Override
    public PerformResult perform(final TkEuExtractZilliantFilesCronJobModel jobModel) {
        try {
            LOG.info("Started the execution");
            final Map<String, String> fileNameToPath = jobModel.getFileNameToPath();
            getUnzipUtility().extractFileAndProcess(fileNameToPath);
            LOG.info("Finished the execution");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception exception) {
            LOG.error("Exception while TkEuExtractZilliantFilesJob : " + exception);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    protected UnzipUtility getUnzipUtility() {
        return unzipUtility;
    }

    @Required
    public void setUnzipUtility(UnzipUtility unzipUtility) {
        this.unzipUtility = unzipUtility;
    }
}

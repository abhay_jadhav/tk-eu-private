package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilCustomerDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilCustomerService;

import java.util.List;

public class DefaultTkEuZilCustomerService implements TkEuZilCustomerService {

    private TkEuZilCustomerDao zilCustomerDao;

    @Override
    public List<ZilCustomerModel> findZilCustomersByIdAndSparte(String b2bUnitUid, String sparte) {
        return getZilCustomerDao().findZilCustomersByIdAndSparte(b2bUnitUid, sparte);
    }

    public TkEuZilCustomerDao getZilCustomerDao() {
        return zilCustomerDao;
    }

    public void setZilCustomerDao(TkEuZilCustomerDao zilCustomerDao) {
        this.zilCustomerDao = zilCustomerDao;
    }
}

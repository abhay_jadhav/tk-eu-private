package com.thyssenkrupp.b2b.eu.zilliantpricing.integration.dataimport.batch.task;

import com.google.common.base.Stopwatch;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.AbstractZilItemModel;

import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupHelper;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.impex.enums.ImpExProcessModeEnum;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.CSVConstants;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.hybris.platform.core.model.ItemModel.PK;

public class TkEuCleanupHelper extends CleanupHelper {

    private static final Logger LOG = Logger.getLogger(TkEuCleanupHelper.class);
    private static final String TK_SCHULTE_PRICE_CATALOG = "tkEuZilliantPriceCatalog";
    private static final String[] PRICE_TYPES = { "ZilProduct", "ZilCustomer", "ZilPrice", "TkEuAccGrpMapping", "ZilCuspri", "TkEuConditionRowA800" };
    private static final String ACTIVE_FALSE = "0";
    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;
    private BaseSiteService baseSiteService;
    private ImportService importService;
    private CatalogService catalogService;

    public void cleanupZilliantRecords() {
        Stopwatch stopwatch = Stopwatch.createStarted();
        CatalogModel priceCatalog = getCatalogService().getCatalog(TK_SCHULTE_PRICE_CATALOG);
        Optional<List<CatalogVersionModel>>inActivePriceCatalogVersions = getInactivePriceCatalog(priceCatalog);
        if (inActivePriceCatalogVersions.isPresent() && inActivePriceCatalogVersions.get().get(0)!=null ) {
        Arrays.stream(PRICE_TYPES).forEach(presentTypeCode -> {
            LOG.info("Start cleanup for the :" + presentTypeCode);
                final Optional<SearchResult<Object>> searchResult = getSearchResults(presentTypeCode, inActivePriceCatalogVersions.get().get(0));
                removeAllItems(searchResult, presentTypeCode);
                LOG.info("End cleanup for  :" + presentTypeCode);

        });
        } else {
            LOG.error("The price catalog " + priceCatalog.getName() + " does not have an inactive version");
        }
        stopwatch.stop();
        LOG.info("Zilliant data clean up finished in " + stopwatch.elapsed(TimeUnit.SECONDS) + "s");
    }

    private Optional<List<CatalogVersionModel>> getInactivePriceCatalog(final CatalogModel priceCatalog) {
        final String findInactivePriceCatalogVersion = "SELECT {" + CatalogVersionModel.PK + "} FROM {" + CatalogVersionModel._TYPECODE + "} WHERE {" + CatalogVersionModel.ACTIVE + "} =?activeFlag AND {" + CatalogVersionModel.CATALOG + "}=?catalog";
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(findInactivePriceCatalogVersion);
        flexibleSearchQuery.addQueryParameter("activeFlag", ACTIVE_FALSE);
        flexibleSearchQuery.addQueryParameter("catalog", priceCatalog.getPk().toString());
        SearchResult<CatalogVersionModel> results = getFlexibleSearchService().search(flexibleSearchQuery);
        return Optional.ofNullable(results.getResult());
    }

    private void removeAllItems(final Optional<SearchResult<Object>> searchResult, String typeCode) {

        if (searchResult.isPresent()) {
            if (CollectionUtils.isNotEmpty(searchResult.get().getResult())) {

                StringBuilder buffer = new StringBuilder();
                List<String> primaryKeysOfDataToBeDeleted = new ArrayList<String>();
                primaryKeysOfDataToBeDeleted = convertPkToString(searchResult.get().getResult());
                createImpexScript(primaryKeysOfDataToBeDeleted, typeCode, buffer);
                final ImportConfig config = getImpexConfig(buffer);
                LOG.info("Removing " + primaryKeysOfDataToBeDeleted.size() + " record for type " + typeCode);
                getImportService().importData(config);

            } else {
                LOG.info("There are no items to be removed");
            }
        }

    }

    private Optional<SearchResult<Object>> getSearchResults(final String presentTypeCode, final CatalogVersionModel inActivePriceCatalogVersion) {
        final String findPKsNotMatchCurrentSequenceId = "SELECT {" + PK + "} FROM {" + presentTypeCode + "} WHERE {" + AbstractZilItemModel.CATALOGVERSION + "} = ?catalogVersion";
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(findPKsNotMatchCurrentSequenceId);
        flexibleSearchQuery.addQueryParameter("catalogVersion", inActivePriceCatalogVersion.getPk().toString());
        return Optional.ofNullable(getFlexibleSearchService().search(flexibleSearchQuery));
    }

    private List<String> convertPkToString(List<Object> result) {
        return result.stream().map(zilModel -> ((ItemModel) zilModel).getPk().toString()).collect(Collectors.toList());
    }

    private void createImpexScript(List<String> primaryKeysOfDataToBeDeleted, String typeCode, StringBuilder buffer) {

        boolean anyInstanceExists = false;
        for (String pk : primaryKeysOfDataToBeDeleted) {
            if (!anyInstanceExists) {
                buffer.append(ImpExProcessModeEnum.REMOVE.getCode() + " " + typeCode + CSVConstants.HYBRIS_FIELD_SEPARATOR + "pk[unique=true]");
                buffer.append(CSVConstants.HYBRIS_LINE_SEPARATOR);
                buffer.append(CSVConstants.HYBRIS_FIELD_SEPARATOR + pk);
                buffer.append(CSVConstants.HYBRIS_LINE_SEPARATOR);
                anyInstanceExists = true;
            }
            if (anyInstanceExists) {
                buffer.append(CSVConstants.HYBRIS_FIELD_SEPARATOR + pk);
                buffer.append(CSVConstants.HYBRIS_LINE_SEPARATOR);

            }
        }

    }

    private ImportConfig getImpexConfig(final StringBuilder stream) {
        final ImpExResource resource = createImpexResource(stream);
        final ImportConfig config = createImpexConfig();
        config.setRemoveOnSuccess(false);
        config.setScript(resource);
        config.setSynchronous(true);
        return config;
    }

    private ImportConfig createImpexConfig() {
        return new ImportConfig();
    }

    private ImpExResource createImpexResource(final StringBuilder stream) {
        return new StreamBasedImpExResource(new ByteArrayInputStream(stream.toString().getBytes(StandardCharsets.UTF_8)), CSVConstants.HYBRIS_ENCODING);
    }

    protected FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    protected ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    public ImportService getImportService() {
        return importService;
    }

    public void setImportService(ImportService importService) {
        this.importService = importService;
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }

    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
}

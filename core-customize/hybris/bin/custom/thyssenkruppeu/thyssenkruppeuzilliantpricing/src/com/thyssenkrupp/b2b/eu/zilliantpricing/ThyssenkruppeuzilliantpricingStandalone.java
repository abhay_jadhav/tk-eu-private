package com.thyssenkrupp.b2b.eu.zilliantpricing;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppeuzilliantpricingStandalone {

    public static void main(final String[] args) {
        new ThyssenkruppeuzilliantpricingStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();
        Utilities.printAppInfo();
        RedeployUtilities.shutdown();
    }
}

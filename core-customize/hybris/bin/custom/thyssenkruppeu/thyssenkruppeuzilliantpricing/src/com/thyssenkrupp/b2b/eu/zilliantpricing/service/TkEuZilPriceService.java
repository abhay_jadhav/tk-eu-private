package com.thyssenkrupp.b2b.eu.zilliantpricing.service;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;

import java.util.List;

public interface TkEuZilPriceService {

    List<ZilPriceModel> findZilPriceByCustomerAndProduct(ZilCustomerModel zilCustomerModel, ZilProductModel zilProductModel);

    List<ZilPriceModel> findZilPriceByDefaultCustomerKey(ZilProductModel zilProductModel);
}

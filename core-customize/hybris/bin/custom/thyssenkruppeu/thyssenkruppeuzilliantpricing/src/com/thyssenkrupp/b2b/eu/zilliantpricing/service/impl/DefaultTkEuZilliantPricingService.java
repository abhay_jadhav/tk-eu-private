package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.core.service.impl.TradeLengthUomConversionHelper;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuUomConversionException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuExternalPricingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPacPriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.exception.ZilPriceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilliantPriceProvider;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuAccumulationDiscountCalculationStrategy;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuCalculateAccumulatedQuantityStrategy;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuCartCalculationForExternalPriceStrategy;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuZilliantPriceModelFindingStrategy;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;

public class DefaultTkEuZilliantPricingService implements TkEuExternalPricingService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuZilliantPricingService.class);

    private CommonI18NService                           commonI18nService;
    private TkEuZilliantPriceModelFindingStrategy       tkEuZilliantPriceModelFindingStrategy;
    private TkEuZilliantPriceProvider                   tkEuZilliantPriceProvider;
    private TkEuUomConversionService                    tkEuUomConversionService;
    private TkEuAccumulationDiscountCalculationStrategy tkEuAccumulationDiscountCalculationStrategy;
    private UnitService                                 unitService;
    private DefaultB2BCommerceUnitService               defaultB2BCommerceUnitService;
    private TkEuConfiguratorSettingsService             configuratorSettingsService;
    private TkEuRoundingService                         tkEuRoundingService;
    private TradeLengthUomConversionHelper              tradeLengthUomConversionHelper;
    private TkEuPacPriceService                         tkEuPacPriceService;
    private PriceValue                                  priceValue;
    private UserService                                 userService;
    private TkEuPriceService                            tkEuPriceService;
    private TkEuCartCalculationForExternalPriceStrategy tkEuCartCalculationForExternalPriceStrategy;
    private TkEuCalculateAccumulatedQuantityStrategy            tkEuCalculateAccumulatedQuantity;

    @Override
    public PriceValue calculateBasePriceForCustomer(TkEuPriceParameter tkEuPriceParameter) throws TkEuPriceServiceException {
        LOG.debug("Start of calculation of product=\'{}\', b2bUnit= \'{}\', materialQuantity=\'{}\'.", tkEuPriceParameter.getProduct().getCode(), tkEuPriceParameter.getB2bUnit().getUid(), tkEuPriceParameter.getQuantityWrapper().getQuantity());
        ZilliantPriceParameter zilliantPriceParameter = new ZilliantPriceParameter();
        try {
            populateZilPriceModel(tkEuPriceParameter, zilliantPriceParameter);
              return calculateMaterialBasePriceFromZilPrice(tkEuPriceParameter, zilliantPriceParameter);
        } catch (ZilPriceUnKnownStateException | TkEuUomConversionException ex) {
            throw new TkEuPriceServiceException(ex);
        }
    }

    @Override
    public PriceValue calculateBasePriceForOrderEntry(final @NotNull AbstractOrderEntryModel orderEntryModel,
            final @NotNull B2BUnitModel b2BUnitModel) throws TkEuPriceServiceException {
        if (orderEntryModel.getQuantity() == null) {
            throw new TkEuPriceServiceException(
                    String.format("Qty can not be \"null\" for the orderEntry with productCode %s ",
                            orderEntryModel.getProduct().getCode()));
        }
        checkAndResetSelectedUnitIfInMemoryCart(orderEntryModel);
        recalculateAndSetOrderWeight(orderEntryModel);
        MaterialQuantityWrapper accumulatedMaterialQuantityInKg = getTkEuCalculateAccumulatedQuantity()
                .findAccumulatedQty(orderEntryModel);
        try {
            final UserModel currentUser = userService.getCurrentUser();
            final B2BUnitModel defaultB2bUnit = getParentB2bUnit((B2BCustomerModel) currentUser);
            MaterialQuantityWrapper quantityWrapper = buildMaterialQuantity(orderEntryModel.getPositionWeight(),
                    orderEntryModel.getUnit());
            TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, orderEntryModel.getProduct(),
                    defaultB2bUnit, quantityWrapper);
            LOG.debug("Starting calculation of pac from method-calculateBasePriceForOrderEntry" + "for product"
                    + orderEntryModel.getProduct().getCode() + "with accumumulated material quantity in kg "
                    + accumulatedMaterialQuantityInKg.getQuantity());
            priceValue = calculatePacPrice(tkEuPriceParameter);
            LOG.debug("Calculated pac price is " + priceValue.getValue());
            boolean isProsEnabled = TkEuConfigKeyValueUtils
                    .getBoolean(ThyssenkruppeupricedataConstants.PROS_PRICING_ENABLED, true);
            if (priceValue.getValue()==0.0d && isProsEnabled) {
                LOG.debug("Calling PROS since no valid PAC Price was found for Product with code \'{}\' and currentUser \'{}\' ",orderEntryModel.getProduct().getCode(), currentUser.getUid());
                TkEuPriceParameter tkEuProsPriceParameter = buildPriceParameter(orderEntryModel,
                        orderEntryModel.getProduct(), defaultB2bUnit, accumulatedMaterialQuantityInKg);
                priceValue = getTkEuPriceService().calculateProsBasePriceForOrderEntry(tkEuProsPriceParameter,
                        quantityWrapper);
                LOG.info(
                        "Finished PROS price calculation of product with code \'{}\' and currentUser \'{}\' with price \'{}\'.",
                        orderEntryModel.getProduct().getCode(), currentUser.getUid(), priceValue.getValue());
            }
            if (priceValue.getValue() > 0.0d) {
                priceValue = calculateAccumulatedBasePriceForOrderEntryForExternalPrice(orderEntryModel,
                        b2BUnitModel, accumulatedMaterialQuantityInKg);

                return priceValue;
            }

            boolean isZilEnabled = TkEuConfigKeyValueUtils
                    .getBoolean(ThyssenkruppeupricedataConstants.ZILLIANT_FALLBACK_ENABLED, true);

            if (isZilEnabled) {
                LOG.debug("Calling Zilliant since no valid PAC/PROS Price was found for Product with code \'{}\' and currentUser \'{}\' ",orderEntryModel.getProduct().getCode(), currentUser.getUid());
                priceValue = calculateAccumulatedBasePriceForOrderEntry(orderEntryModel, b2BUnitModel,
                        accumulatedMaterialQuantityInKg);
                return priceValue;
            }

            throw new TkEuPriceServiceException(
                    "No Valid Price from PAC/PROS Returned for Product" + orderEntryModel.getProduct().getCode());

        } catch (ZilPriceUnKnownStateException ex) {
            throw new TkEuPriceServiceException(ex);
        }
    }



    private void checkAndResetSelectedUnitIfInMemoryCart(AbstractOrderEntryModel orderEntryModel) throws TkEuPriceServiceException {
        AbstractOrderModel orderModel = orderEntryModel.getOrder();
        if (isInMemoryCart(orderModel)) {
            UnitModel selectedUnit = orderEntryModel.getUnit();
            List<TkUomConversionModel> storeSupportedUomConversions = getTkEuUomConversionService().getStoreSupportedUomConversions(orderEntryModel.getProduct());
            reduceIntoStoreSupportedUnit(orderEntryModel, selectedUnit, storeSupportedUomConversions);
            reduceIntoConfiguredUnitIfAvailable(orderEntryModel, storeSupportedUomConversions);
        }
    }

    private void reduceIntoConfiguredUnitIfAvailable(AbstractOrderEntryModel orderEntryModel, List<TkUomConversionModel> storeSupportedUomConversions) {
        if (getConfiguratorSettingsService().hasValidC2lConfigurationSetting(orderEntryModel)) {
            final UnitModel orderEntryModelUnit = orderEntryModel.getUnit();
            List<String> c2lSupportedUnits = getTradeLengthUomConversionHelper().getC2lSupportedUnits();
            if (c2lSupportedUnits.stream().noneMatch(unit -> equalsIgnoreCase(unit, orderEntryModelUnit.getCode()))) {
                for (final TkUomConversionModel tkUomConversionModel : storeSupportedUomConversions) {
                    final UnitModel salesUnit = tkUomConversionModel.getSalesUnit();
                    if (c2lSupportedUnits.stream().anyMatch(c2lUnit -> equalsIgnoreCase(c2lUnit, salesUnit.getCode()))) {
                        orderEntryModel.setUnit(salesUnit);
                        return;
                    }
                }
            }
        }
    }

    private void reduceIntoStoreSupportedUnit(AbstractOrderEntryModel orderEntryModel, UnitModel selectedUnit, List<TkUomConversionModel> storeSupportedUomConversions) {
        if (storeSupportedUomConversions.stream().noneMatch(isSelectedUnitAvailable(selectedUnit))) {
            TkUomConversionModel firstUomConversionModel = storeSupportedUomConversions.get(0);
            UnitModel availableUnit = firstUomConversionModel != null ? firstUomConversionModel.getSalesUnit() : selectedUnit;
            orderEntryModel.setUnit(availableUnit);
        }
    }

    private Predicate<TkUomConversionModel> isSelectedUnitAvailable(UnitModel selectedUnit) {
        return tkUomConversionModel -> tkUomConversionModel.getSalesUnit() == selectedUnit;
    }

    private void recalculateAndSetOrderWeight(@NotNull AbstractOrderEntryModel orderEntryModel) throws TkEuPriceServiceException {
        final AbstractOrderModel abstractOrderModel = orderEntryModel.getOrder();
        final UnitModel unitKgm = getUnitService().getUnitForCode(SAP_KGM_UNIT_CODE);
        BigDecimal totalWeightForOrderEntry = getTkEuUomConversionService().findTotalWeightForOrderEntryInKg(orderEntryModel);
        BigDecimal orderTotalWeightInKg = getTkEuUomConversionService().calculateOrderTotalWeightInKg(abstractOrderModel);
        orderEntryModel.setPositionWeight(totalWeightForOrderEntry.doubleValue());
        abstractOrderModel.setTotalWeight(orderTotalWeightInKg.doubleValue());
        abstractOrderModel.setWeightUnit(unitKgm);
    }

    @Override
    public List<PriceInformation> getPriceInformationsForProduct(ProductModel productModel, final B2BUnitModel b2BUnitModel) throws TkEuPriceServiceException {
        MaterialQuantityWrapper materialQuantityWrapper = buildMaterialQuantity(1, productModel.getBaseUnit());
        LOG.debug("Start fetching volume prices for product=\'{}\', b2bUnit= \'{}\', materialQuantity=\'{}\'.", productModel.getCode(), b2BUnitModel.getUid(), materialQuantityWrapper);
        ZilliantPriceParameter zilliantPriceParameter = new ZilliantPriceParameter();
        final TkEuPriceParameter priceParameter = buildPriceParameter(null, productModel, b2BUnitModel, materialQuantityWrapper);
        try {
            populateZilPriceModel(priceParameter, zilliantPriceParameter);
            return getTkEuZilliantPriceProvider().materialScaledBasePrices(priceParameter, zilliantPriceParameter);
        } catch (ZilPriceUnKnownStateException ex) {
            throw new TkEuPriceServiceException(ex);
        }
    }

    // 4 medhods for accumulation logic
    /*protected MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException {
        final AbstractOrderModel abstractOrderModel = abstractOrderEntryModel.getOrder();
        if (BooleanUtils.isFalse(isInMemoryCart(abstractOrderModel)) && abstractOrderModel.getEntries().size() > 1) {
            return findAccumulatedQty(abstractOrderModel, abstractOrderEntryModel);
        }
        return buildMaterialQuantity(abstractOrderEntryModel.getPositionWeight(), abstractOrderModel.getWeightUnit());
    }

    protected MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderModel cartModel, @NotNull AbstractOrderEntryModel abstractOrderEntryModel) {
        Map<String, List<AbstractOrderEntryModel>> accGroupMap = classifyCartEntriesByAccumulation(cartModel);
        if (CollectionUtils.isNotEmpty(accGroupMap.entrySet())) {
            final List<AbstractOrderEntryModel> accumulatedGroupEntries = accGroupMap.entrySet().stream()
                .map(entry -> entry.getValue()).filter(entryValue -> entryValue.contains(abstractOrderEntryModel)).findFirst()
                .orElse(Collections.EMPTY_LIST);
            try {
                return findAccumulatedQtyInWeightByOrderEntry(accumulatedGroupEntries, abstractOrderEntryModel);
            } catch (TkEuUomConversionException ex) {
                final List<String> productGroup = accumulatedGroupEntries.stream().map(abstractOrderEntry -> abstractOrderEntry.getProduct().getCode()).collect(Collectors.toList());
                LOG.error("Accumulation price can not be applied for the group {} with following reason ", productGroup, ex);
            }
        }
        return buildMaterialQuantity(abstractOrderEntryModel.getQuantity().doubleValue(), abstractOrderEntryModel.getUnit());
    }

    protected Map<String, List<AbstractOrderEntryModel>> classifyCartEntriesByAccumulation(AbstractOrderModel cartModel) {
        Map<String, List<AbstractOrderEntryModel>> groupMap = new HashMap<>();
        for (AbstractOrderEntryModel cartEntry : cartModel.getEntries()) {
            getTkEuAccumulationDiscountCalculationStrategy().buildAccumulatedGroups(groupMap, cartEntry);
        }
        return groupMap;
    }

    protected MaterialQuantityWrapper findAccumulatedQtyInWeightByOrderEntry(@NotNull List<AbstractOrderEntryModel> entries, @NotNull AbstractOrderEntryModel abstractOrderEntryModel) {
        final BigDecimal totalWeightInKg = getTkEuUomConversionService().calculateOrderEntriesTotalWeightInKg(entries);
        MaterialQuantityWrapper qtyInKgm = buildMaterialQuantity(totalWeightInKg.doubleValue(), abstractOrderEntryModel.getOrder().getWeightUnit());
        LOG.debug("Found Accumulated weight {} for the order entry {} ", qtyInKgm, abstractOrderEntryModel.getProduct().getCode());
        return qtyInKgm;
    }
*/
    protected PriceValue calculateAccumulatedBasePriceForOrderEntry(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity) throws TkEuPriceServiceException {
        ProductModel productModel = orderEntryModel.getProduct();
        try {
            return calculateAccumulatedBasePriceForOrderEntry(orderEntryModel, b2BUnitModel, materialQuantity, productModel);
        } catch (ZilPriceUnKnownStateException ex) {
            throw new TkEuPriceServiceException(ex);
        }
    }

    protected PriceValue calculateAccumulatedBasePriceForOrderEntryForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity) throws TkEuPriceServiceException {
        ProductModel productModel = orderEntryModel.getProduct();
        try {
            return getTkEuCartCalculationForExternalPriceStrategy().calculateAccumulatedBasePriceForOrderEntryForExternalPrice(orderEntryModel, b2BUnitModel, materialQuantity, productModel, priceValue);
        } catch (ZilPriceUnKnownStateException ex) {
            throw new TkEuPriceServiceException(ex);
        }
    }

    //zilliant calculation for cart
    protected PriceValue calculateAccumulatedBasePriceForOrderEntry(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel) throws TkEuPriceServiceException {
        String currencyIsoCode = orderEntryModel.getOrder().getCurrency().getIsocode();
        double tradeLength = 1;
        UnitModel salesUnit = orderEntryModel.getUnit();
        MaterialQuantityWrapper tradeLengthUnitIfApplied = getTkEuUomConversionService().findTradeLengthUnitIfApplied(orderEntryModel);

        if (tradeLengthUnitIfApplied != null) {
            salesUnit = tradeLengthUnitIfApplied.getUnitModel();
            tradeLength = tradeLengthUnitIfApplied.getQuantity();
        }

        if (ObjectUtils.equals(productModel.getBaseUnit(), salesUnit)) {
            return getOrderEntryBasePriceInProductBaseUom(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tradeLength);
        } else {
            return getBasePriceBasedOnAvailableSalesUom(orderEntryModel, b2BUnitModel, materialQuantity, productModel, currencyIsoCode, tradeLength, salesUnit);
        }
    }

/*    protected PriceValue calculateAccumulatedBasePriceForOrderEntryForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel) throws TkEuPriceServiceException {
        String currencyIsoCode = orderEntryModel.getOrder().getCurrency().getIsocode();
        double tradeLength = 1;
        UnitModel salesUnit = orderEntryModel.getUnit();
        MaterialQuantityWrapper tradeLengthUnitIfApplied = getTkEuUomConversionService().findTradeLengthUnitIfApplied(orderEntryModel);

        if (tradeLengthUnitIfApplied != null) {
            salesUnit = tradeLengthUnitIfApplied.getUnitModel();
            tradeLength = tradeLengthUnitIfApplied.getQuantity();
        }

        if (ObjectUtils.equals(productModel.getBaseUnit(), salesUnit)) {
            return getOrderEntryBasePriceInProductBaseUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tradeLength);
        } else {
            return getBasePriceBasedOnAvailableSalesUomForExternalPrice(orderEntryModel, b2BUnitModel, materialQuantity, productModel, currencyIsoCode, tradeLength, salesUnit);
        }
    }*/

    //zilliant
    protected PriceValue getBasePriceBasedOnAvailableSalesUom(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, String currencyIsoCode, double tradeLength, UnitModel salesUnit) throws TkEuPriceServiceException {
        List<TkUomConversionModel> supportedUomConversions = getTkEuUomConversionService().getSupportedUomConversions(productModel);
        Optional<TkUomConversionModel> uomConversionModel = findUomByTargetUnit(supportedUomConversions, salesUnit);
        if (uomConversionModel.isPresent()) {
            TkUomConversionModel selectedUomConversionModel = uomConversionModel.get();
            return getOrderEntryBasePriceInSalesUom(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, selectedUomConversionModel, tradeLength);
        } else if (!(orderEntryModel.getOrder() instanceof InMemoryCartModel)) {
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        } else {
            TkUomConversionModel tkUomConversionModel = supportedUomConversions != null ? supportedUomConversions.get(0) : null;
            if (tkUomConversionModel != null) {
                orderEntryModel.setUnit(tkUomConversionModel.getSalesUnit());
                return getOrderEntryBasePriceInSalesUom(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tkUomConversionModel, tradeLength);
            }
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        }
    }

/*    protected PriceValue getBasePriceBasedOnAvailableSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, String currencyIsoCode, double tradeLength, UnitModel salesUnit) throws TkEuPriceServiceException {
        List<TkUomConversionModel> supportedUomConversions = getTkEuUomConversionService().getSupportedUomConversions(productModel);
        Optional<TkUomConversionModel> uomConversionModel = findUomByTargetUnit(supportedUomConversions, salesUnit);
        if (uomConversionModel.isPresent()) {
            TkUomConversionModel selectedUomConversionModel = uomConversionModel.get();
            return getOrderEntryBasePriceInSalesUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, selectedUomConversionModel, tradeLength);
        } else if (!(orderEntryModel.getOrder() instanceof InMemoryCartModel)) {
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        } else {
            TkUomConversionModel tkUomConversionModel = supportedUomConversions != null ? supportedUomConversions.get(0) : null;
            if (tkUomConversionModel != null) {
                orderEntryModel.setUnit(tkUomConversionModel.getSalesUnit());
                return getOrderEntryBasePriceInSalesUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tkUomConversionModel, tradeLength);
            }
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        }
    }*/

    protected Optional<TkUomConversionModel> findUomByTargetUnit(List<TkUomConversionModel> supportedUomConversions, UnitModel targetUnit) {
        return supportedUomConversions.stream().filter(tkUomConversionModel -> ObjectUtils.equals(targetUnit, tkUomConversionModel.getSalesUnit())).findFirst();
    }

    //zilliant
    protected PriceValue getOrderEntryBasePriceInSalesUom(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, TkUomConversionModel selectedUomConversionModel, double tradeLength) throws TkEuPriceServiceException {
        BigDecimal salesToBaseUomFactor = getTkEuUomConversionService().calculateFactor(selectedUomConversionModel.getBaseUnitFactor(), selectedUomConversionModel.getSalesUnitFactor()).orElse(BigDecimal.ONE);
        TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        PriceValue basePriceInBaseUom = calculateBasePriceForCustomer(tkEuPriceParameter);
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue()).multiply(salesToBaseUomFactor).multiply(BigDecimal.valueOf(tradeLength));
        if (!getTkEuUomConversionService().isWeightedUomConversion(selectedUomConversionModel)) {
            priceVal = getTkEuUomConversionService().applyCommercialWeight(productModel, productModel.getBaseUnit(), priceVal, getTkEuUomConversionService().getUomConversionByProductCode(productModel.getCode()));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Sales Uom  SalesUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", materialQuantity.getUnitModel().getCode(), salesToBaseUomFactor, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }

/*    protected PriceValue getOrderEntryBasePriceInSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, TkUomConversionModel selectedUomConversionModel, double tradeLength) throws TkEuPriceServiceException {
        BigDecimal salesToBaseUomFactor = getTkEuUomConversionService().calculateFactor(selectedUomConversionModel.getBaseUnitFactor(), selectedUomConversionModel.getSalesUnitFactor()).orElse(BigDecimal.ONE);
        TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        LOG.debug("In method -getOrderEntryBasePriceInSalesUomForPac "+ priceValue.getValue());
        PriceValue basePriceInBaseUom = priceValue;
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue()).multiply(salesToBaseUomFactor).multiply(BigDecimal.valueOf(tradeLength));
        LOG.debug("Converted into sales uom, baseprice is "+priceValue.getValue()+" salesToBaseUomFactor is "+salesToBaseUomFactor.doubleValue()+" trade length is "+tradeLength+", Calculated value is "+priceVal.doubleValue());
        if (!getTkEuUomConversionService().isWeightedUomConversion(selectedUomConversionModel)) {
            priceVal = getTkEuUomConversionService().applyCommercialWeight(productModel, productModel.getBaseUnit(), priceVal, getTkEuUomConversionService().getUomConversionByProductCode(productModel.getCode()));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Sales Uom  SalesUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", materialQuantity.getUnitModel().getCode(), salesToBaseUomFactor, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }*/

    //when baseuom is pce, for zilliant
    protected PriceValue getOrderEntryBasePriceInProductBaseUom(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, double tradeLength) throws TkEuPriceServiceException {
        TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        PriceValue basePriceInBaseUom = calculateBasePriceForCustomer(tkEuPriceParameter);
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue() * tradeLength);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Base Uom BaseUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", productModel.getBaseUnit().getCode(), 1, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }

    //When base uom is pce, for external
/*    protected PriceValue getOrderEntryBasePriceInProductBaseUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, double tradeLength) throws TkEuPriceServiceException {
        TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        PriceValue basePriceInBaseUom = priceValue;
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue() * tradeLength);
        LOG.debug("In mehod-getOrderEntryBasePriceInProductBaseUomForPac ,calculated base price is"+ basePriceInBaseUom.getValue()+"Trade length for the "
                + "product is "+tradeLength+" final pac price is tradelength*baseprice"+ priceVal.doubleValue());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Base Uom BaseUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", productModel.getBaseUnit().getCode(), 1, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }*/

    private ZilPriceModel populateZilPriceModel(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) {
        getTkEuZilliantPriceModelFindingStrategy().findZilliantPrice(priceParameter, zilliantPriceParameter);
        return zilliantPriceParameter.getZilPrice();
    }

    private PriceValue calculateMaterialBasePriceFromZilPrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException {
        BigDecimal basePrice = getTkEuZilliantPriceProvider().materialBasePrice(priceParameter, zilliantPriceParameter);
        LOG.debug("Base Price for product {}: {}.", priceParameter.getProduct().getCode(), basePrice);
        return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), basePrice.doubleValue(), true);
    }

    @SuppressWarnings("unused")
    public PriceValue calculatePacPrice(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        PriceValue basePrice = getTkEuPacPriceService().getPACPriceForQuantity(priceParameter).getPrice();
        LOG.debug("PAC Price for product {}: {}.", priceParameter.getProduct().getCode(), basePrice);
        return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), basePrice.getValue(), true);
    }

    private boolean isInMemoryCart(AbstractOrderModel abstractOrderModel) {
        return abstractOrderModel instanceof InMemoryCartModel;
    }

    private TkEuPriceParameter buildPriceParameter(AbstractOrderEntryModel orderEntryModel, ProductModel productModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity) {
        TkEuPriceParameter priceParameter = new TkEuPriceParameter();
        priceParameter.setProduct(productModel);
        priceParameter.setQuantityWrapper(materialQuantity);
        priceParameter.setB2bUnit(b2BUnitModel);
        priceParameter.setCurrencyCode(getCommonI18nService().getCurrentCurrency().getIsocode());
        priceParameter.setChildB2bUnit(getDefaultB2BCommerceUnitService().getParentUnit());
        priceParameter.setOrderEntry(orderEntryModel);
        return priceParameter;
    }

    private B2BUnitModel getParentB2bUnit(B2BCustomerModel currentUser) {
        B2BUnitModel defaultUnit = currentUser.getDefaultB2BUnit();
        if (defaultUnit == null) {
            getDefaultB2BCommerceUnitService().getRootUnit();
        }
        return defaultUnit;
    }

    private MaterialQuantityWrapper buildMaterialQuantity(double quantity, UnitModel unitModel) {
        return MaterialQuantityWrapperBuilder.builder().withQuantity(quantity).withUnit(unitModel).build();
    }

    public TkEuRoundingService getTkEuRoundingService() {
        return tkEuRoundingService;
    }

    @Required
    public void setTkEuRoundingService(TkEuRoundingService tkEuRoundingService) {
        this.tkEuRoundingService = tkEuRoundingService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    public TkEuAccumulationDiscountCalculationStrategy getTkEuAccumulationDiscountCalculationStrategy() {
        return tkEuAccumulationDiscountCalculationStrategy;
    }

    @Required
    public void setTkEuAccumulationDiscountCalculationStrategy(TkEuAccumulationDiscountCalculationStrategy tkEuAccumulationDiscountCalculationStrategy) {
        this.tkEuAccumulationDiscountCalculationStrategy = tkEuAccumulationDiscountCalculationStrategy;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public TkEuZilliantPriceProvider getTkEuZilliantPriceProvider() {
        return tkEuZilliantPriceProvider;
    }

    @Required
    public void setTkEuZilliantPriceProvider(TkEuZilliantPriceProvider tkEuZilliantPriceProvider) {
        this.tkEuZilliantPriceProvider = tkEuZilliantPriceProvider;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public TkEuZilliantPriceModelFindingStrategy getTkEuZilliantPriceModelFindingStrategy() {
        return tkEuZilliantPriceModelFindingStrategy;
    }

    @Required
    public void setTkEuZilliantPriceModelFindingStrategy(TkEuZilliantPriceModelFindingStrategy tkEuZilliantPriceModelFindingStrategy) {
        this.tkEuZilliantPriceModelFindingStrategy = tkEuZilliantPriceModelFindingStrategy;
    }

    public TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public TradeLengthUomConversionHelper getTradeLengthUomConversionHelper() {
        return tradeLengthUomConversionHelper;
    }

    @Required
    public void setTradeLengthUomConversionHelper(TradeLengthUomConversionHelper tradeLengthUomConversionHelper) {
        this.tradeLengthUomConversionHelper = tradeLengthUomConversionHelper;
    }

    public TkEuPacPriceService getTkEuPacPriceService() {
        return tkEuPacPriceService;
    }

    @Required
    public void setTkEuPacPriceService(TkEuPacPriceService tkEuPacPriceService) {
        this.tkEuPacPriceService = tkEuPacPriceService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public TkEuPriceService getTkEuPriceService() {
        return tkEuPriceService;
    }

    public void setTkEuPriceService(TkEuPriceService tkEuPriceService) {
        this.tkEuPriceService = tkEuPriceService;
    }

    public TkEuCartCalculationForExternalPriceStrategy getTkEuCartCalculationForExternalPriceStrategy() {
        return tkEuCartCalculationForExternalPriceStrategy;
    }

    @Required
    public void setTkEuCartCalculationForExternalPriceStrategy(
            TkEuCartCalculationForExternalPriceStrategy tkEuCartCalculationForExternalPriceStrategy) {
        this.tkEuCartCalculationForExternalPriceStrategy = tkEuCartCalculationForExternalPriceStrategy;
    }

    public TkEuCalculateAccumulatedQuantityStrategy getTkEuCalculateAccumulatedQuantity() {
        return tkEuCalculateAccumulatedQuantity;
    }

    @Required
    public void setTkEuCalculateAccumulatedQuantity(TkEuCalculateAccumulatedQuantityStrategy tkEuCalculateAccumulatedQuantity) {
        this.tkEuCalculateAccumulatedQuantity = tkEuCalculateAccumulatedQuantity;
    }
}

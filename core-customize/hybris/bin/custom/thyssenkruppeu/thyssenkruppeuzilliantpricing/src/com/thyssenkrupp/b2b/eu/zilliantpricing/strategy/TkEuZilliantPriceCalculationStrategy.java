package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.math.BigDecimal;
import java.util.List;

public interface TkEuZilliantPriceCalculationStrategy {

    BigDecimal materialBasePrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException;

    List<PriceInformation> materialScaledBasePrices(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException;
}

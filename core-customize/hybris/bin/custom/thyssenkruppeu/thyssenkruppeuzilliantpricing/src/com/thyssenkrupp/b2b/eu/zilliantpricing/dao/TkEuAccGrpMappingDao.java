package com.thyssenkrupp.b2b.eu.zilliantpricing.dao;

import java.util.List;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuAccGrpMappingModel;

public interface TkEuAccGrpMappingDao {

    List<TkEuAccGrpMappingModel> findAccGrpIdBySaleAreaAndMaterialGroup(String salesArea, String materialGroup);
}

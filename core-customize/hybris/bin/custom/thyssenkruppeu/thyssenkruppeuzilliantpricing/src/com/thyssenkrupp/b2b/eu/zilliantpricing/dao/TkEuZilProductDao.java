package com.thyssenkrupp.b2b.eu.zilliantpricing.dao;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

public interface TkEuZilProductDao extends Dao {

    List<ZilProductModel> findZilProductsById(String matnr);
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.integration.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexConverter;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.ImpexTransformerTask;
import de.hybris.platform.util.CSVConstants;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class TkEuImpexTransformerTask extends ImpexTransformerTask {

    private TkEuCleanupHelper tkEuCleanupHelper;

    @Override
    public BatchHeader execute(final BatchHeader header) throws UnsupportedEncodingException, FileNotFoundException {
        Assert.notNull(header);
        Assert.notNull(header.getFile());
        final File file = header.getFile();
        header.setEncoding(CSVConstants.HYBRIS_ENCODING);
        final List<ImpexConverter> converters = getConverters(file);
        processImpexConverter(header, file, converters);
        return header;
    }

    private void processImpexConverter(final BatchHeader header, File file, final List<ImpexConverter> converters) throws UnsupportedEncodingException, FileNotFoundException {
        int position = 1;
        for (final ImpexConverter converter : converters) {
            final File impexFile = getImpexFile(file, position++);
            if (convertFile(header, file, impexFile, converter)) {
                header.addTransformedFile(impexFile);
            } else {
                getTkEuCleanupHelper().cleanupFile(impexFile);
            }
        }
    }

    public TkEuCleanupHelper getTkEuCleanupHelper() {
        return tkEuCleanupHelper;
    }

    public void setTkEuCleanupHelper(TkEuCleanupHelper tkEuCleanupHelper) {
        this.tkEuCleanupHelper = tkEuCleanupHelper;
    }

}

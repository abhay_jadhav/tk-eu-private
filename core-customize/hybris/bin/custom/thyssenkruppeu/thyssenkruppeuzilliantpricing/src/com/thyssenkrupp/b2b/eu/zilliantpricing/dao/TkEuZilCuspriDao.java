package com.thyssenkrupp.b2b.eu.zilliantpricing.dao;

import java.util.List;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

public interface TkEuZilCuspriDao extends Dao {

    /**
     * To find ZilCuspri record using Customer ID, Matkl and Product Quantity.
     * 
     * @param customerId
     * @param matkl
     * @param quantity
     * @return a valid ZilCuspri record
     */
    List<ZilCuspriModel> findZilCuspriForProductQuantity(String customerId, String matkl, Double quantity);

    /**
     * To find ZilCuspri record using Customer ID, Matkl and Minimum Quantity
     * 
     * @param customerId
     * @param matkl
     * @param minqtd
     * @return a valid ZilCuspri record
     */
    Optional<ZilCuspriModel> findZilCuspriForMinQtd(String customerId, String matkl, String minqtd);

    /**
     * To find ZilCuspri records for Customer ID and Material Class.
     * 
     * @param customerId
     * @param matkl
     * @return list of ZilCuspriModel
     */
    List<ZilCuspriModel> findZilCuspriEntries(String customerId, String matkl);
}

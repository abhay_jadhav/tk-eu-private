package com.thyssenkrupp.b2b.eu.zilliantpricing.jalo;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;
import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuExternalPricingService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;

/**
 * This is the extension manager of the Thyssenkruppeuzilliantpricing extension.
 */
public class ThyssenkruppeuzilliantpricingManager extends Europe1PriceFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ThyssenkruppeuzilliantpricingManager.class);

    private UserService                   userService;
    private TkEuExternalPricingService    tkEuExternalPricingService;
    private ModelService                  modelService;
    private TkEuPriceService              tkEuPriceService;
    private CommonI18NService             commonI18NService;
    private DefaultB2BCommerceUnitService defaultB2BCommerceUnitService;

    public ThyssenkruppeuzilliantpricingManager() {
        LOG.debug("constructor of ThyssenkruppeuzilliantpricingManager called.");
    }

    public static ThyssenkruppeuzilliantpricingManager getInstance() {
        return (ThyssenkruppeuzilliantpricingManager) Registry.getCurrentTenant().getJaloConnection().getExtensionManager().getExtension(ThyssenkruppeuzilliantpricingConstants.EXTENSIONNAME);
    }

    @Override
    public void init() {
        LOG.debug("init() of ThyssenkruppeupricingManager called, current tenant: {}", getTenant().getTenantID());
    }

    @Override
    public void destroy() {
        LOG.debug("destroy() of ThyssenkruppeupricingManager called, current tenant: {}", getTenant().getTenantID());
    }

    @Override
    public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException {
        initServicesIfNeeded();
        final UserModel currentUser = userService.getCurrentUser();
        final Product product = entry.getProduct();
        String productCode = product == null ? "(null)" : product.getCode();
        LOG.debug("Start of price calculation: Get price for order entry with Zilliant Product with code \'{}\' and user \'{}\'.", productCode, currentUser.getUid());
        try {
            if (product != null && currentUser instanceof B2BCustomerModel) {
                PriceValue orderEntryPriceValue = getOrderEntryPriceValue(entry, currentUser);
                LOG.debug("Finished price calculation of order entry with Zilliant Product \'{}\' and user \'{}\' with result orderEntryPriceValue \'{}\'.", productCode, currentUser.getUid(), orderEntryPriceValue.getValue());
                return orderEntryPriceValue;
            }
            throw new TkEuPriceServiceUnKnownStateException(String.format("Current User in an Anonymous user (or) not a B2BCustomer \'%s\'", currentUser.getUid()));
        } catch (TkEuPriceServiceUnKnownStateException | TkEuPriceServiceException ex) {
            LOG.error("Couldn't find price for product " + productCode + " and current user " + currentUser.getUid() + " due to the following reason", ex);
            throw new JaloPriceFactoryException(ex, 1);
        }
    }

    @Override
    public String getName() {
        return ThyssenkruppeuzilliantpricingConstants.EXTENSIONNAME;
    }

    private PriceValue getOrderEntryPriceValue(AbstractOrderEntry entry, UserModel currentUser) throws TkEuPriceServiceException {
        final B2BUnitModel defaultUnit = getParentB2bUnit((B2BCustomerModel) currentUser);
        if (defaultUnit != null) {
            AbstractOrderEntryModel entryModel = modelService.get(entry);
            return tkEuExternalPricingService.calculateBasePriceForOrderEntry(entryModel, defaultUnit);
        }
        throw new TkEuPriceServiceUnKnownStateException(String.format("Missing default unit for the customer \'%s\'", currentUser.getUid()));
    }

    private B2BUnitModel getParentB2bUnit(B2BCustomerModel currentUser) {
        B2BUnitModel defaultUnit = currentUser.getDefaultB2BUnit();
        if (defaultUnit == null) {
            defaultB2BCommerceUnitService.getRootUnit();
        }
        return defaultUnit;
    }

    @Override
    public List getDiscountValues(AbstractOrderEntry entry) throws JaloPriceFactoryException {
        LOG.debug("Return Discount Values for Order Position");
        initServicesIfNeeded();
        List<DiscountValue> discounts = new ArrayList<>();
        AbstractOrderEntryModel entryModel = modelService.get(entry);
        UserModel currentUser = userService.getCurrentUser();
        discounts = tkEuPriceService.getDiscountValuesForOrderEntry(entryModel, currentUser, discounts);
        return discounts;
    }

    @Override
    public List getDiscountValues(AbstractOrder order) throws JaloPriceFactoryException {
        LOG.debug("Return Discount Values for Order");
        initServicesIfNeeded();
        List<DiscountValue> discounts = new ArrayList<>();
        AbstractOrderModel orderModel = modelService.get(order);
        if (CollectionUtils.isNotEmpty(orderModel.getEntries())) {
            UserModel currentUser = userService.getCurrentUser();
            discounts = tkEuPriceService.getDiscountValuesForOrder(orderModel, currentUser, discounts);
        }
        return discounts;
    }

    @SuppressWarnings("deprecation")
    public List getProductPriceInformations(final SessionContext ctx, final Product product, final Date date, final boolean net, final long quantity) throws JaloPriceFactoryException {
        initServicesIfNeeded();
        return super.getProductPriceInformations(ctx, product, date, net);
    }

    @Override
    public List<PriceRow> matchPriceRowsForInfo(SessionContext ctx, Product product, EnumerationValue productGroup, User user, EnumerationValue userGroup, Currency currency, Date date, boolean net) throws JaloPriceFactoryException {
        initServicesIfNeeded();
        return super.matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, currency, date, net);
    }

    protected void initServicesIfNeeded() {

        final Field field = org.springframework.util.ReflectionUtils.findField(this.getClass(), "retrieveChannelStrategy");
        org.springframework.util.ReflectionUtils.makeAccessible(field);
        final Object value = org.springframework.util.ReflectionUtils.getField(field, this);

        if (value == null) {
            final RetrieveChannelStrategy retrieveChannelStrategy = (RetrieveChannelStrategy) Registry.getApplicationContext().getBean("retrieveChannelStrategy");

            final Field field1 = org.springframework.util.ReflectionUtils.findField(this.getClass(), "retrieveChannelStrategy");
            org.springframework.util.ReflectionUtils.makeAccessible(field1);
            org.springframework.util.ReflectionUtils.setField(field1, this, retrieveChannelStrategy);
        }

        if (userService == null) {
            userService = Registry.getApplicationContext().getBean("userService", UserService.class);
        }
        if (tkEuExternalPricingService == null) {
            tkEuExternalPricingService = Registry.getApplicationContext().getBean("tkEuExternalPricingService", TkEuExternalPricingService.class);
        }
        if (modelService == null) {
            modelService = Registry.getApplicationContext().getBean("modelService", ModelService.class);
        }
        if (tkEuPriceService == null) {
            tkEuPriceService = Registry.getApplicationContext().getBean("priceService", TkEuPriceService.class);
        }
        if (commonI18NService == null) {
            commonI18NService = Registry.getApplicationContext().getBean("commonI18NService", CommonI18NService.class);
        }
        if (defaultB2BCommerceUnitService == null) {
            defaultB2BCommerceUnitService = Registry.getApplicationContext().getBean("b2bCommerceUnitService", DefaultB2BCommerceUnitService.class);
        }
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.decorator;

import java.util.Map;
import java.util.Objects;
import de.hybris.platform.util.CSVCellDecorator;
import com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TkEuZilliantCellEndDateDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuZilliantCellEndDateDecorator.class);

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        final String impexContent = impexLine.get(Integer.valueOf(position));
        if (!Objects.isNull(impexContent)) {
            String date = impexContent.concat(StringUtils.SPACE + ThyssenkruppeuzilliantpricingConstants.ZILLIANT_TS);
            LOG.debug(impexContent + " date appended with time " + date);
            return date;
        }
        return null;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.service;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;

import java.util.List;

public interface TkEuZilCustomerService {
    List<ZilCustomerModel> findZilCustomersByIdAndSparte(String b2bUnitUid, String sparte);
}

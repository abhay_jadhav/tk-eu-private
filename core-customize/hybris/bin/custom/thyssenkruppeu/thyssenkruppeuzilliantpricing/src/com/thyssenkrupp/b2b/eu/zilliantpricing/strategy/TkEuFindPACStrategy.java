package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import java.util.List;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;

import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

public interface TkEuFindPACStrategy {

    List<DiscountRowModel> findPacScalePriceRowsByConditionKey(String scaleConditionKey);

    DiscountRowModel findScalePriceRowsByConditionKeyAndQuantity(String scaleConditionKey,
            Long quantityInScaleUnit);

    List<PriceInformation> getPACPrices(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException;

    PriceInformation getPACPriceForQuantity(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException;

}

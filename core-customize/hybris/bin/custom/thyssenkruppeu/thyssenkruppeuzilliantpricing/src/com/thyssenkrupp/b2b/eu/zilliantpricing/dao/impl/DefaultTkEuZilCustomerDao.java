package com.thyssenkrupp.b2b.eu.zilliantpricing.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilCustomerDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.List;

import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel.*;

public class DefaultTkEuZilCustomerDao extends DefaultGenericDao<ZilCustomerModel> implements TkEuZilCustomerDao {
    private static final     Logger LOG                                 = LoggerFactory.getLogger(DefaultTkEuZilCustomerDao.class);
    private static final String FIND_ZILCUSTOMER_BY_ID_AND_DIVISION = "SELECT {" + PK + "} FROM {" + _TYPECODE + "} WHERE {" + CUSTOMERID + "}=?customerId" + " "
        + "AND {" + SPARTE + "} =?sparte " + "AND {" + VALIDFROM + "} <= ?validfrom " + "AND" + "{" + VALIDTO + "}>?validto AND {" + CATALOGVERSION + "}=?catalogVersion";

    private TkEuCatalogVersionService catalogVersionService;


    public DefaultTkEuZilCustomerDao(String typeCode) {
        super(typeCode);
    }

    @Override
    public List<ZilCustomerModel> findZilCustomersByIdAndSparte(String customerId, String sparte) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILCUSTOMER_BY_ID_AND_DIVISION);
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        final Date date = getTruncatedDate();
        fQuery.addQueryParameter("customerId", customerId);
        fQuery.addQueryParameter("validTo", date);
        fQuery.addQueryParameter("validFrom", date);
        fQuery.addQueryParameter("sparte", sparte);
        fQuery.addQueryParameter("catalogVersion", catalogVersion);

        final SearchResult<ZilCustomerModel> zilCustomers = getFlexibleSearchService().search(fQuery);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \'{}\' for params: customerId=\'{}\', sparte=\'{}\'. Found {} results.", _TYPECODE, customerId, sparte, zilCustomers.getResult().size());
            if (zilCustomers.getResult().size() > 0) {
                LOG.debug("Logging first zilCustomer: customerKey2=\'{}\'.", zilCustomers.getResult().get(0).getCustomerKey2());
            }
        }
        return zilCustomers.getResult();
    }

    public TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

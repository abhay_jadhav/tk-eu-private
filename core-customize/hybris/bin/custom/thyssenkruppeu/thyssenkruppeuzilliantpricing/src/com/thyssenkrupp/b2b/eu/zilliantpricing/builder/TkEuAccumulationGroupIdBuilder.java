package com.thyssenkrupp.b2b.eu.zilliantpricing.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceUnKnownStateException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuAccGrpMappingDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuAccGrpMappingModel;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

public class TkEuAccumulationGroupIdBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuAccumulationGroupIdBuilder.class);
    private BaseStoreService     baseStoreService;
    private TkEuAccGrpMappingDao tkEuAccGrpMappingDao;

    public Map<String, List<AbstractOrderEntryModel>> buildMap(String key, Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        if (groupMap.containsKey(key)) {
            // Add to existing list
            LOG.debug("Adding cartEntry to existing groupMap with key \'{}\': cartCode=\'{}\', entryNumber=\'{}\'.", key, cartEntry.getOrder().getCode(), cartEntry.getEntryNumber());
            groupMap.get(key).add(cartEntry);
        } else {
            // Add a new entry
            final List<AbstractOrderEntryModel> list = new ArrayList<>();
            list.add(cartEntry);
            LOG.debug("Adding cartEntry to new groupMap with key \'{}\': cartCode=\'{}\', entryNumber=\'{}\'.", key, cartEntry.getOrder().getCode(), cartEntry.getEntryNumber());
            groupMap.put(key, list);
        }
        return groupMap;
    }

    public Map<String, List<AbstractOrderEntryModel>> groupByKey(String key, Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        LOG.debug("Building Accumulation Group with key: \'{}\'.", key);
        buildMap(key, groupMap, cartEntry);
        return groupMap;
    }

    public Map<String, List<AbstractOrderEntryModel>> buildKeyWithAccumulationGrpId(String accGrpId, Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        groupByKey(accGrpId, groupMap, cartEntry);
        return groupMap;
    }

    public Map<String, List<AbstractOrderEntryModel>> buildKeyWithDivision(String division, Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        groupByKey(division.substring(0, 1), groupMap, cartEntry);
        return groupMap;
    }

    public Map<String, List<AbstractOrderEntryModel>> buildKeyWithMaterialGroupAndConstant(String materialGrp, Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        groupByKey(baseStore.getUid() + materialGrp.substring(0, 2), groupMap, cartEntry);
        return groupMap;
    }

    public String findAccGrpIdBySaleAreaAndMaterialGroup(String salesArea, String materialGroup) {
        if(StringUtils.isEmpty(salesArea) || StringUtils.isEmpty(materialGroup)) {
            throw new TkEuPriceServiceUnKnownStateException("salesArea and materialGroup can not be empty");
        }
        List<TkEuAccGrpMappingModel> accGroupRow = getTkEuAccGrpMappingDao().findAccGrpIdBySaleAreaAndMaterialGroup(salesArea, materialGroup);
        if (CollectionUtils.isEmpty(accGroupRow)) {
            LOG.debug("No matching group found in Accumulation mapping table for " + salesArea + " and " + materialGroup);
            return StringUtils.EMPTY;
        }
        LOG.debug("Matching group found: groupId=\'{}\', distributionChannel=\'{}\', salesOrg=\'{}\', materialGrp=\'{}\'.", accGroupRow.get(0).getAccumulationGroupId(), accGroupRow.get(0).getDistributionChannel(), accGroupRow.get(0).getSalesOrganization(), accGroupRow.get(0).getMaterialGroup());
        return accGroupRow.get(0).getAccumulationGroupId();
    }

    public TkEuAccGrpMappingDao getTkEuAccGrpMappingDao() {
        return tkEuAccGrpMappingDao;
    }

    public void setTkEuAccGrpMappingDao(TkEuAccGrpMappingDao tkEuAccGrpMappingDao) {
        this.tkEuAccGrpMappingDao = tkEuAccGrpMappingDao;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilPriceDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilPriceService;

import java.util.List;

public class DefaultTkEuZilPriceService implements TkEuZilPriceService {

    private TkEuZilPriceDao zilPriceDao;

    @Override
    public List<ZilPriceModel> findZilPriceByCustomerAndProduct(ZilCustomerModel zilCustomerModel, ZilProductModel zilProductModel) {
        return getZilPriceDao().findZilPriceByCustomerAndProduct(zilCustomerModel, zilProductModel);
    }

    @Override
    public List<ZilPriceModel> findZilPriceByDefaultCustomerKey(ZilProductModel zilProductModel) {
        return getZilPriceDao().findZilPriceByDefaultCustomerKey(zilProductModel);
    }

    public TkEuZilPriceDao getZilPriceDao() {
        return zilPriceDao;
    }

    public void setZilPriceDao(TkEuZilPriceDao zilPriceDao) {
        this.zilPriceDao = zilPriceDao;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
//import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuCartCalculationForExternalPriceStrategy;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import de.hybris.platform.util.PriceValue;

public class DefaultTkEuCartCalculationForExternalPriceStrategy implements TkEuCartCalculationForExternalPriceStrategy{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCartCalculationForExternalPriceStrategy.class);
    private TkEuUomConversionService                    tkEuUomConversionService;
    private CommonI18NService                           commonI18nService;
    private DefaultB2BCommerceUnitService               defaultB2BCommerceUnitService;

    public PriceValue calculateAccumulatedBasePriceForOrderEntryForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, PriceValue priceValue) throws TkEuPriceServiceException {
        String currencyIsoCode = orderEntryModel.getOrder().getCurrency().getIsocode();
        double tradeLength = 1;
        UnitModel salesUnit = orderEntryModel.getUnit();
        MaterialQuantityWrapper tradeLengthUnitIfApplied = getTkEuUomConversionService().findTradeLengthUnitIfApplied(orderEntryModel);

        if (tradeLengthUnitIfApplied != null) {
            salesUnit = tradeLengthUnitIfApplied.getUnitModel();
            tradeLength = tradeLengthUnitIfApplied.getQuantity();
        }

        if (ObjectUtils.equals(productModel.getBaseUnit(), salesUnit)) {
            return getOrderEntryBasePriceInProductBaseUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tradeLength, priceValue);
        } else {
            return getBasePriceBasedOnAvailableSalesUomForExternalPrice(orderEntryModel, b2BUnitModel, materialQuantity, productModel, currencyIsoCode, tradeLength, salesUnit, priceValue);
        }
    }

    //When baseuom is PCE
    public PriceValue getOrderEntryBasePriceInProductBaseUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, double tradeLength, PriceValue priceValue) throws TkEuPriceServiceException {
        //TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        PriceValue basePriceInBaseUom = priceValue;
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue() * tradeLength);
        LOG.debug("In mehod-getOrderEntryBasePriceInProductBaseUomForPac ,calculated base price is"+ basePriceInBaseUom.getValue()+"Trade length for the "
                + "product is "+tradeLength+" final pac price is tradelength*baseprice"+ priceVal.doubleValue());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Base Uom BaseUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", productModel.getBaseUnit().getCode(), 1, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }

    //when base uom is other than pce, go for conversion
    public PriceValue getBasePriceBasedOnAvailableSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, String currencyIsoCode, double tradeLength, UnitModel salesUnit, PriceValue priceValue) throws TkEuPriceServiceException {
        List<TkUomConversionModel> supportedUomConversions = getTkEuUomConversionService().getSupportedUomConversions(productModel);
        Optional<TkUomConversionModel> uomConversionModel = findUomByTargetUnit(supportedUomConversions, salesUnit);
        if (uomConversionModel.isPresent()) {
            TkUomConversionModel selectedUomConversionModel = uomConversionModel.get();
            return getOrderEntryBasePriceInSalesUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, selectedUomConversionModel, tradeLength, priceValue);
        } else if (!(orderEntryModel.getOrder() instanceof InMemoryCartModel)) {
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        } else {
            TkUomConversionModel tkUomConversionModel = supportedUomConversions != null ? supportedUomConversions.get(0) : null;
            if (tkUomConversionModel != null) {
                orderEntryModel.setUnit(tkUomConversionModel.getSalesUnit());
                return getOrderEntryBasePriceInSalesUomForExternalPrice(orderEntryModel, currencyIsoCode, b2BUnitModel, productModel, materialQuantity, tkUomConversionModel, tradeLength, priceValue);
            }
            throw new TkEuPriceServiceException(String.format("Missing UnitOfConversions for the selected variant \'%s\' for the unit and qty \"%s\"", productModel.getCode(), materialQuantity));
        }
    }

    public PriceValue getOrderEntryBasePriceInSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, TkUomConversionModel selectedUomConversionModel, double tradeLength, PriceValue priceValue) throws TkEuPriceServiceException {
        BigDecimal salesToBaseUomFactor = getTkEuUomConversionService().calculateFactor(selectedUomConversionModel.getBaseUnitFactor(), selectedUomConversionModel.getSalesUnitFactor()).orElse(BigDecimal.ONE);
        //TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(orderEntryModel, productModel, b2BUnitModel, materialQuantity);
        LOG.debug("In method -getOrderEntryBasePriceInSalesUomForPac "+ priceValue.getValue());
        PriceValue basePriceInBaseUom = priceValue;
        BigDecimal priceVal = BigDecimal.valueOf(basePriceInBaseUom.getValue()).multiply(salesToBaseUomFactor).multiply(BigDecimal.valueOf(tradeLength));
        LOG.debug("Converted into sales uom, baseprice is "+priceValue.getValue()+" salesToBaseUomFactor is "+salesToBaseUomFactor.doubleValue()+" trade length is "+tradeLength+", Calculated value is "+priceVal.doubleValue());
        if (!getTkEuUomConversionService().isWeightedUomConversion(selectedUomConversionModel)) {
            priceVal = getTkEuUomConversionService().applyCommercialWeight(productModel, productModel.getBaseUnit(), priceVal, getTkEuUomConversionService().getUomConversionByProductCode(productModel.getCode()));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calc BasePrice In Sales Uom  SalesUom=\'{}\', salesToBaseUomFactor= \'{}\', basePriceInBaseUom=\'{}\'.", materialQuantity.getUnitModel().getCode(), salesToBaseUomFactor, basePriceInBaseUom.getValue());
        }
        return new PriceValue(currencyIsoCode, priceVal.doubleValue(), true);
    }

/*    public TkEuPriceParameter buildPriceParameter(AbstractOrderEntryModel orderEntryModel, ProductModel productModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity) {
        TkEuPriceParameter priceParameter = new TkEuPriceParameter();
        priceParameter.setProduct(productModel);
        priceParameter.setQuantityWrapper(materialQuantity);
        priceParameter.setB2bUnit(b2BUnitModel);
        priceParameter.setCurrencyCode(getCommonI18nService().getCurrentCurrency().getIsocode());
        priceParameter.setChildB2bUnit(getDefaultB2BCommerceUnitService().getParentUnit());
        priceParameter.setOrderEntry(orderEntryModel);
        return priceParameter;
    }*/

    //for UOM conversions
    public Optional<TkUomConversionModel> findUomByTargetUnit(List<TkUomConversionModel> supportedUomConversions, UnitModel targetUnit) {
        return supportedUomConversions.stream().filter(tkUomConversionModel -> ObjectUtils.equals(targetUnit, tkUomConversionModel.getSalesUnit())).findFirst();
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

}

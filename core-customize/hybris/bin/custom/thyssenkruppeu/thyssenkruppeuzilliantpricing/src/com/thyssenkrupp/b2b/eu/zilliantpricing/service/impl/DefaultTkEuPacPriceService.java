package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPacPriceService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl.DefaultTkEuFindPACStrategy;

import de.hybris.platform.jalo.order.price.PriceInformation;

public class DefaultTkEuPacPriceService implements TkEuPacPriceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPacPriceService.class);
    private DefaultTkEuFindPACStrategy defaultTkEuFindPACStrategy;

    @Override
    public List<PriceInformation> getPACValuesForCustomer(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        List<PriceInformation> newPI = getDefaultTkEuFindPACStrategy().getPACPrices(priceParameter);
        return newPI;
    }

    @Override
    public PriceInformation getPACPriceForQuantity(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        PriceInformation newPI = getDefaultTkEuFindPACStrategy().getPACPriceForQuantity(priceParameter);
        LOG.info("Calculated Pac for "+ priceParameter.getProduct().getCode() +"with price"+ newPI.getPrice().getValue());
        return newPI;
    }

    public DefaultTkEuFindPACStrategy getDefaultTkEuFindPACStrategy() {
        return defaultTkEuFindPACStrategy;
    }

    @Required
    public void setDefaultTkEuFindPACStrategy(DefaultTkEuFindPACStrategy defaultTkEuFindPACStrategy) {
        this.defaultTkEuFindPACStrategy = defaultTkEuFindPACStrategy;
    }
}

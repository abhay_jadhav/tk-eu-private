package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuUomConversionException;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuConditionKeyBuilderSupplier;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuScalePriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.impl.DefaultTkEuConditionKeyBuilderSupplier;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilCuspriService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuZilliantPriceCalculationStrategy;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuZilliantPriceModelFindingStrategy;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.PriceValue;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_WEIGHTED_TYPE_CODE;

public class DefaultTkEuZilliantPriceCalculationStrategy implements TkEuZilliantPriceCalculationStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuZilliantPriceCalculationStrategy.class);

    private TkEuUomConversionService tkEuUomConversionService;
    private TkEuScalePriceService tkEuScalePriceService;
    private DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier;
    private TkEuRoundingService roundingService;
    private CostConfigurationProductInfoService costConfigurationProductInfoService;
    private CommonI18NService commonI18nService;
    private TkEuZilliantPriceModelFindingStrategy tkEuZilliantPriceModelFindingStrategy;
    private TkEuZilCuspriService tkEuZilCuspriService;

    @Override
    public BigDecimal materialBasePrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException {
        final BigDecimal zilliantPrice = zilliantPrice(priceParameter, zilliantPriceParameter);
        LOG.debug("Calculated zilPrice in zilUnit and Qty for product {} : {}.", priceParameter.getProduct().getCode(), zilliantPrice);
        priceParameter.setZilliantPriceParameter(zilliantPriceParameter);
        String scaleConditionKey = getConditionKey(priceParameter);
        return convertAndCalculateScaledBasePrice(priceParameter, zilliantPrice, scaleConditionKey);
    }

    //method to display scales on pdp
    @Override
    public List<PriceInformation> materialScaledBasePrices(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException {
        final BigDecimal zilliantPrice = zilliantPrice(priceParameter, zilliantPriceParameter);
        LOG.debug("Calculated zilPrice in zilUnit and Qty for product {} : {}.", priceParameter.getProduct().getCode(), zilliantPrice);
        priceParameter.setZilliantPriceParameter(zilliantPriceParameter);
        String scaleConditionKey = getConditionKey(priceParameter);
        List<DiscountRowModel> scalePriceRowsByConditionKey = getTkEuScalePriceService().findScalePriceRowsByConditionKey(scaleConditionKey);
        scalePriceRowsByConditionKey.stream().sorted(Comparator.comparing(DiscountRowModel::getMinqtd));
        return createScaledPriceMap(zilliantPrice, scalePriceRowsByConditionKey, priceParameter);
    }

    private List<PriceInformation> createScaledPriceMap(BigDecimal zilPriceInBaseUom, @NotNull List<DiscountRowModel> list, TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        List<PriceInformation> scaledDiscounts = new ArrayList<>();
        String isoCode = priceParameter.getCurrencyCode();
        ZilProductModel zilProduct = priceParameter.getZilliantPriceParameter().getZilProduct();
        Map<Double, ZilCuspriModel> zilCuspriWithLowerBound = getTkEuZilCuspriService().findSortedZilCuspriForCustomerIdAndMatkl(priceParameter.getB2bUnit().getUid(), zilProduct.getMatkl());
        for (DiscountRowModel priceRowCondA800Model : list) {
            Long minQtd = priceRowCondA800Model.getMinqtd();
            Map qualifiers = new HashMap();
            qualifiers.put(PriceRow.MINQTD, minQtd);
            qualifiers.put(PriceRow.UNIT, priceRowCondA800Model.getUnit());

            BigDecimal calcPrice = zilPriceInBaseUom;
            BigDecimal scaleFactor = getTkEuScalePriceService().findScaleFactorFromDiscountRow(priceRowCondA800Model);
            if (scaleFactor != null && BigDecimal.ZERO.compareTo(scaleFactor) != 0) {
                calcPrice = zilPriceInBaseUom.multiply(scaleFactor);
            }
            BigDecimal roundedZilDiscountPrice = getRoundingService().roundPriceBasedOnPriceQuantity(priceParameter, calcPrice);
            if (MapUtils.isNotEmpty(zilCuspriWithLowerBound) && zilCuspriWithLowerBound.containsKey(Double.valueOf(minQtd))) {
                BigDecimal roundedZilCuspriValue = calculateZilCuspriValue(zilProduct, zilCuspriWithLowerBound.get(Double.valueOf(minQtd)), priceParameter.getProduct().getCode(), priceParameter.getProduct());
                if (BigDecimal.ZERO.compareTo(roundedZilCuspriValue) != 0) {
                    if (roundedZilDiscountPrice.add(roundedZilCuspriValue).signum() != -1) {
                        roundedZilDiscountPrice = roundedZilDiscountPrice.add(roundedZilCuspriValue).setScale(8, BigDecimal.ROUND_HALF_UP);
                    } else {
                        LOG.error("Negative Zil Price After Adding ZilCuspri Value : {}. Skipping Zil Cuspri Value addition." + roundedZilDiscountPrice.add(roundedZilCuspriValue));
                    }
                }
            }
            BigDecimal zilPricePerUnit = roundedZilDiscountPrice.divide(BigDecimal.valueOf(zilProduct.getPriceQuantity()), 8, RoundingMode.HALF_UP).stripTrailingZeros();
            BigDecimal priceInBaseUom = calculatePriceInBaseUOM(priceParameter, zilPricePerUnit, zilProduct);
            BigDecimal roundedPriceInBaseUom = getRoundingService().roundScaledBasePrice(priceInBaseUom);
            PriceInformation priceInfo = new PriceInformation(qualifiers, new PriceValue(isoCode, roundedPriceInBaseUom.doubleValue(), false));
            scaledDiscounts.add(priceInfo);
        }
        return scaledDiscounts;
    }

    private BigDecimal convertAndCalculateScaledBasePrice(TkEuPriceParameter priceParameter, BigDecimal zilliantPriceInZilUnit, String scaleConditionKey) throws TkEuPriceServiceException {
        ZilProductModel zilProduct = priceParameter.getZilliantPriceParameter().getZilProduct();
        BigDecimal priceInZilUom = zilliantPriceInZilUnit;
        final Pair<BigDecimal, DiscountRowModel> pair = findScaleFactor(priceParameter, scaleConditionKey);
        final BigDecimal scaleFactor = pair.getKey();
        final String scaleMinQtd = Objects.nonNull(pair.getValue()) ? pair.getValue().getMinqtd().toString() : StringUtils.EMPTY;
        if (scaleFactor != null && (BigDecimal.ZERO.compareTo(scaleFactor) != 0)) {
            priceInZilUom = zilliantPriceInZilUnit.multiply(scaleFactor);
            LOG.debug("Discounted Zil Price In Zilliant Unit : (ZilPriceInZilUom) * (ScaleFactor) = " + priceInZilUom);
        }

        BigDecimal roundedZilDiscountPrice = getRoundingService().roundPriceBasedOnPriceQuantity(priceParameter, priceInZilUom);
        BigDecimal roundedZilDiscountPriceWithCuspriValue = roundedZilDiscountPrice;

        UnitModel hybrisZilUnit = getTkEuUomConversionService().getHybrisUnitFromZilliantUnit(zilProduct.getUnit());
        AbstractOrderEntryModel orderEntry = priceParameter.getOrderEntry();
        if (orderEntry != null) {
            CostConfigurationProductInfoModel zilliantCostConfigurationProductInfo = getCostConfigurationProductInfoService().createZilliantCostConfigurationProductInfo(orderEntry, getCommonI18nService().getCurrentCurrency(), zilProduct.getPriceQuantity(), hybrisZilUnit, roundedZilDiscountPrice.doubleValue());
            getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(orderEntry, zilliantCostConfigurationProductInfo);
            BigDecimal roundedZilCuspriValue = findZilCuspriValueWithMinQtd(priceParameter, scaleMinQtd);
            if (BigDecimal.ZERO.compareTo(roundedZilCuspriValue) != 0) {
                if (roundedZilDiscountPrice.add(roundedZilCuspriValue).signum() != -1) {
                    roundedZilDiscountPriceWithCuspriValue = roundedZilDiscountPrice.add(roundedZilCuspriValue).setScale(8, BigDecimal.ROUND_HALF_UP);
                    CostConfigurationProductInfoModel zilCuspriCostConfigurationProductInfo = getCostConfigurationProductInfoService().createZilCuspriCostConfigurationProductInfo(orderEntry, getCommonI18nService().getCurrentCurrency(), zilProduct.getPriceQuantity(), hybrisZilUnit, roundedZilCuspriValue.doubleValue());
                    zilCuspriCostConfigurationProductInfo.setValue(roundedZilCuspriValue.doubleValue());
                    getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(orderEntry, zilCuspriCostConfigurationProductInfo);
                    LOG.debug("Zil Price With ZilCuspri Value In Zilliant Unit : (ZilPriceInZilUom) + (ZilCuspriValueInZilUom) = " + roundedZilDiscountPriceWithCuspriValue);
                } else {
                    LOG.error("Negative Zil Price After Adding ZilCuspri Value : {}. Skipping Zil Cuspri Value addition." + roundedZilDiscountPriceWithCuspriValue);
                }
            } else {
                getCostConfigurationProductInfoService().removeZilCuspriCostConfigurationProductInfo(orderEntry);
            }
        }
        BigDecimal zilPricePerUnit = roundedZilDiscountPriceWithCuspriValue.divide(BigDecimal.valueOf(zilProduct.getPriceQuantity()), 16, RoundingMode.HALF_UP);
        return calculatePriceInBaseUOM(priceParameter, zilPricePerUnit, zilProduct);
    }

    protected BigDecimal findZilCuspriValueWithMinQtd(TkEuPriceParameter priceParameter, String minqtd) {
        getTkEuZilliantPriceModelFindingStrategy().findZilliantCuspri(priceParameter, priceParameter.getZilliantPriceParameter(), minqtd);
        return calculateZilCuspriValue(priceParameter.getZilliantPriceParameter().getZilProduct(), priceParameter.getZilliantPriceParameter().getZilCuspri(), priceParameter.getProduct().getCode(), priceParameter.getProduct());
    }

    private BigDecimal calculateZilCuspriValue(@NotNull ZilProductModel zilProductModel, ZilCuspriModel zilCuspriModel, @NotNull String productCode, ProductModel productModel) {
        BigDecimal roundedZilCuspriValue = BigDecimal.ZERO;
        if (Objects.nonNull(zilCuspriModel)) {
            BigDecimal zilCuspriDeltaAmount = BigDecimal.valueOf(zilCuspriModel.getDeltaAmount());
            BigDecimal zilCuspriDeltaPerMeasure = BigDecimal.valueOf(zilCuspriModel.getDeltaPerMeasure());
            if (StringUtils.equalsIgnoreCase(zilCuspriModel.getDeltaPerMeasureUom(), zilProductModel.getUnit())) {
                if (BigDecimal.ZERO.compareTo(zilCuspriDeltaPerMeasure) != 0) {
                    roundedZilCuspriValue = (zilCuspriDeltaAmount.divide(zilCuspriDeltaPerMeasure)).multiply(BigDecimal.valueOf(zilProductModel.getPriceQuantity()));
                    LOG.debug("Calculated ZilCuspri Value for product in ZilUnit {} : {}", roundedZilCuspriValue, productCode);
                }
            } else {
                roundedZilCuspriValue = calculateZilCuspriForDifferentUnits(productModel, zilProductModel,zilCuspriModel, roundedZilCuspriValue);
                LOG.error("Calculating ZilCuspri Value for product in ZilUnit {} : {}", roundedZilCuspriValue, productCode);
            }
        }
        return roundedZilCuspriValue;
    }

    private BigDecimal calculateZilCuspriForDifferentUnits(ProductModel productModel, ZilProductModel zilProductModel, ZilCuspriModel zilCuspriModel, BigDecimal zilCuspriValue) {
        BigDecimal roundedZilCuspriValue = zilCuspriValue;
        try {
            LOG.debug("Calculating ZilCuspri value for different units");
            List<TkUomConversionModel> conversionFactors = getTkEuUomConversionService().getSupportedUomConversions(productModel);
            for (TkUomConversionModel conversionFactor : conversionFactors) {

                /*As per TKS-174, the Cuspri markup is always per 1 KG*/
                if(conversionFactor.getSalesUnit().getCode().equalsIgnoreCase(SAP_KGM_UNIT_CODE)){
                    BigDecimal zilCuspriDeltaAmount = BigDecimal.valueOf(zilCuspriModel.getDeltaAmount());
                    Double baseUnitFactor = conversionFactor.getBaseUnitFactor();
                    Double salesUnitFactor = conversionFactor.getSalesUnitFactor();

                    BigDecimal cuspriValue = (zilCuspriDeltaAmount.divide(BigDecimal.valueOf(baseUnitFactor),4,RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(salesUnitFactor));
                    roundedZilCuspriValue = cuspriValue.setScale(2, RoundingMode.HALF_UP);
                }
            }
        } catch (TkEuPriceServiceException e) {
            LOG.error("calculateZilCuspriForDifferentUnits Error :" + e);
        }
        return roundedZilCuspriValue;

    }

    protected Pair<BigDecimal, DiscountRowModel> findScaleFactor(TkEuPriceParameter priceParameter, String scaleConditionKey) throws TkEuPriceServiceException {
        final List<DiscountRowModel> scalePriceRowsByConditionKey = getTkEuScalePriceService().findScalePriceRowsByConditionKey(scaleConditionKey);
        if (CollectionUtils.isNotEmpty(scalePriceRowsByConditionKey)) {
            final MaterialQuantityWrapper materialQuantityInScaleUnit = getTkEuUomConversionService().calculateQuantityInTargetUnit(priceParameter.getQuantityWrapper(), scalePriceRowsByConditionKey.get(0).getUnit(), priceParameter.getProduct());
            applyCommercialWeight(priceParameter, materialQuantityInScaleUnit);
            final DiscountRowModel matchingDiscountRowByQty = getTkEuScalePriceService().findMatchingDiscountRowByQty(scalePriceRowsByConditionKey, (long) materialQuantityInScaleUnit.getQuantity());
            return Pair.of(getTkEuScalePriceService().findScaleFactorFromDiscountRow(matchingDiscountRowByQty), matchingDiscountRowByQty);
        }
        return Pair.of(BigDecimal.ONE, null);
    }

    private void applyCommercialWeight(TkEuPriceParameter priceParameter, MaterialQuantityWrapper materialQuantityInScaleUnit) throws TkEuPriceServiceException {
        if (StringUtils.equalsIgnoreCase(SAP_KGM_UNIT_CODE, materialQuantityInScaleUnit.getUnitModel().getCode())) {
            if (!StringUtils.equals(SAP_WEIGHTED_TYPE_CODE, priceParameter.getQuantityWrapper().getUnitModel().getUnitType())) {
                ProductModel productModel = priceParameter.getProduct();
                BigDecimal commercialWeight = getTkEuUomConversionService().applyCommercialWeight(productModel, productModel.getBaseUnit(), BigDecimal.valueOf(materialQuantityInScaleUnit.getQuantity()), getTkEuUomConversionService().getSupportedUomConversions(productModel));
                materialQuantityInScaleUnit.setQuantity(commercialWeight.doubleValue());
            }
        }
    }

    protected BigDecimal zilliantPrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) {
        final ZilPriceModel zilPrice = zilliantPriceParameter.getZilPrice();
        final ZilProductModel zilProduct = zilliantPriceParameter.getZilProduct();
        BigDecimal roundedZilPriceValue = getRoundingService().roundZilPriceValue(zilProduct);
        BigDecimal roundedTargetFactor = getRoundingService().roundZilTargetFactor(zilPrice);
        BigDecimal zilliantPrice = roundedTargetFactor.multiply(roundedZilPriceValue);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calculating ZilPrice for product=\'{}\' with TargetFactor=\'{}\', quantity=\'{}\' and zilPriceValue=\'{}\' : ZilPrice=\'{} * {} = {}\'.", priceParameter.getProduct().getCode(), roundedTargetFactor, zilProduct.getPriceQuantity(), roundedZilPriceValue, roundedTargetFactor, roundedZilPriceValue, zilliantPrice);
        }
        return zilliantPrice;
    }

    protected String getConditionKey(TkEuPriceParameter priceParameter) {
        TkEuConditionKeyBuilderSupplier scalePriceKeyBuilderSupplier = getDefaultTkEuConditionKeyBuilderSupplier().getScalePriceKeyBuilderSupplier();
        final List<Pair<GenericDao, List<String>>> conditionKeys = scalePriceKeyBuilderSupplier.get().stream().map(tkEuPriceConditionKeyBuilderFactory -> tkEuPriceConditionKeyBuilderFactory.build(priceParameter)).collect(Collectors.toList());
        LOG.debug("Scale Condition keys = " + conditionKeys);
        return conditionKeys.stream().findFirst().map(genericDaoListPair -> genericDaoListPair.getValue().stream().findFirst().orElse(StringUtils.EMPTY)).orElse(StringUtils.EMPTY);
    }

    private void logMissingZilliantUnitMapping(TkEuPriceParameter priceParameter, String zilliantUnit, UnitModel unitModel) {
        final String hybrisUnit = unitModel.getCode();
        if (StringUtils.isEmpty(hybrisUnit)) {
            throw new TkEuUomConversionException(String.format("Zilliant unit \"%s\" mapping missing for the product \"%s\"", zilliantUnit, priceParameter.getProduct().getCode()));
        } else {
            LOG.debug("Found Zilliant Unit: {}, SAP Hybris Unit: {} for product {}.", zilliantUnit, hybrisUnit, priceParameter.getProduct().getCode());
        }
    }

    protected BigDecimal calculatePriceInBaseUOM(TkEuPriceParameter priceParameter, BigDecimal zilliantPricePerUnit, ZilProductModel zilProduct) throws TkEuPriceServiceException {
        final String zilliantUnit = zilProduct.getUnit();
        UnitModel hybrisZilUnit = getTkEuUomConversionService().getHybrisUnitFromZilliantUnit(zilliantUnit);
        logMissingZilliantUnitMapping(priceParameter, zilliantUnit, hybrisZilUnit);
        ProductModel product = priceParameter.getProduct();
        if (ObjectUtils.notEqual(hybrisZilUnit, product.getBaseUnit())) {
            LOG.debug("Selected Unit: {} not Equal to Base Unit: {} Calculating Pricing in BaseUOM for product {}.", hybrisZilUnit, product.getBaseUnit().getCode(), priceParameter.getProduct().getCode());
            BigDecimal basePrice = tkEuUomConversionService.calculatePriceInBaseUOM(product, zilliantPricePerUnit, hybrisZilUnit);
            return basePrice;
        }
        return zilliantPricePerUnit;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public CostConfigurationProductInfoService getCostConfigurationProductInfoService() {
        return costConfigurationProductInfoService;
    }

    @Required
    public void setCostConfigurationProductInfoService(CostConfigurationProductInfoService costConfigurationProductInfoService) {
        this.costConfigurationProductInfoService = costConfigurationProductInfoService;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public TkEuScalePriceService getTkEuScalePriceService() {
        return tkEuScalePriceService;
    }

    public void setTkEuScalePriceService(TkEuScalePriceService tkEuScalePriceService) {
        this.tkEuScalePriceService = tkEuScalePriceService;
    }

    public DefaultTkEuConditionKeyBuilderSupplier getDefaultTkEuConditionKeyBuilderSupplier() {
        return defaultTkEuConditionKeyBuilderSupplier;
    }

    @Required
    public void setDefaultTkEuConditionKeyBuilderSupplier(DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier) {
        this.defaultTkEuConditionKeyBuilderSupplier = defaultTkEuConditionKeyBuilderSupplier;
    }

    public TkEuRoundingService getRoundingService() {
        return roundingService;
    }

    public void setRoundingService(TkEuRoundingService roundingService) {
        this.roundingService = roundingService;
    }

    protected TkEuZilliantPriceModelFindingStrategy getTkEuZilliantPriceModelFindingStrategy() {
        return tkEuZilliantPriceModelFindingStrategy;
    }

    @Required
    public void setTkEuZilliantPriceModelFindingStrategy(TkEuZilliantPriceModelFindingStrategy tkEuZilliantPriceModelFindingStrategy) {
        this.tkEuZilliantPriceModelFindingStrategy = tkEuZilliantPriceModelFindingStrategy;
    }

    protected TkEuZilCuspriService getTkEuZilCuspriService() {
        return tkEuZilCuspriService;
    }

    @Required
    public void setTkEuZilCuspriService(TkEuZilCuspriService tkEuZilCuspriService) {
        this.tkEuZilCuspriService = tkEuZilCuspriService;
    }

}

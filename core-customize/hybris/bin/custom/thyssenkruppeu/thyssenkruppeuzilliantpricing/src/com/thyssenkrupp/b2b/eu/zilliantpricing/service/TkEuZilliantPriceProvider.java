package com.thyssenkrupp.b2b.eu.zilliantpricing.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.math.BigDecimal;
import java.util.List;

public interface TkEuZilliantPriceProvider {

    <P1 extends TkEuPriceParameter, P2 extends ZilliantPriceParameter> List<PriceInformation> materialScaledBasePrices(P1 priceParameter, P2 zilliantPriceParameter) throws TkEuPriceServiceException;

    <P1 extends TkEuPriceParameter, P2 extends ZilliantPriceParameter> BigDecimal materialBasePrice(P1 priceParameter, P2 zilliantPriceParameter) throws TkEuPriceServiceException;
}

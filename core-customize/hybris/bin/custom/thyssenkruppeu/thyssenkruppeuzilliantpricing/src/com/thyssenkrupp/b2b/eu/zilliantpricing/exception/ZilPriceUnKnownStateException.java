package com.thyssenkrupp.b2b.eu.zilliantpricing.exception;

public class ZilPriceUnKnownStateException extends RuntimeException {

    private static final long serialVersionUID = 8441320081124389801L;

    public ZilPriceUnKnownStateException() {
        super();
    }

    public ZilPriceUnKnownStateException(String message) {
        super(message);
    }

    public ZilPriceUnKnownStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZilPriceUnKnownStateException(Throwable cause) {
        super(cause);
    }

    protected ZilPriceUnKnownStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

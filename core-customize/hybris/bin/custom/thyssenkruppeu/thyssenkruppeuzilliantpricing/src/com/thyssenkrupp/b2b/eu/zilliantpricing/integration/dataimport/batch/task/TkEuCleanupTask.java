package com.thyssenkrupp.b2b.eu.zilliantpricing.integration.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupTask;

public class TkEuCleanupTask extends CleanupTask {

    private TkEuCleanupHelper tkEuCleanupHelper;

    @Override
    public BatchHeader execute(final BatchHeader header) {
        getTkEuCleanupHelper().cleanup(header, false);
        return super.execute(header);
    }

    public TkEuCleanupHelper getTkEuCleanupHelper() {
        return tkEuCleanupHelper;
    }

    public void setTkEuCleanupHelper(TkEuCleanupHelper tkEuCleanupHelper) {
        this.tkEuCleanupHelper = tkEuCleanupHelper;
    }

}

package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilProductDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilProductService;

import java.util.List;

public class DefaultTkEuZilProductService implements TkEuZilProductService {

    private TkEuZilProductDao zilProductDao;

    @Override
    public List<ZilProductModel> findZilProductsById(String matnr) {
        return getZilProductDao().findZilProductsById(matnr);
    }

    public TkEuZilProductDao getZilProductDao() {
        return zilProductDao;
    }

    public void setZilProductDao(TkEuZilProductDao zilProductDao) {
        this.zilProductDao = zilProductDao;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.exception.ZilPriceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilCuspriService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilCustomerService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilPriceService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilProductService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuZilliantPriceModelFindingStrategy;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Optional;

public class DefaultTkEuZilliantPriceModelFindingStrategy implements TkEuZilliantPriceModelFindingStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuZilliantPriceModelFindingStrategy.class);

    private TkEuZilPriceService    tkEuZilPriceService;
    private TkEuZilCustomerService tkEuZilCustomerService;
    private TkEuZilProductService  tkEuZilProductService;
    private TkEuZilCuspriService  tkEuZilCuspriService;
    private TkEuUomConversionService tkEuUomConversionService;

    @Override
    public ZilliantPriceParameter findZilliantCuspri(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter, String minqtd) {
        if (StringUtils.isNotBlank(minqtd)) {
            try {
                final Optional<ZilCuspriModel> zilCuspri = getTkEuZilCuspriService().findZilCuspriForCustomerIdMatklAndMinQtd(priceParameter.getB2bUnit().getUid(), zilliantPriceParameter.getZilProduct().getMatkl(), minqtd);
                if (zilCuspri.isPresent()) {
                    zilliantPriceParameter.setZilCuspri(zilCuspri.get());
                    LOG.debug(String.format("Found ZilCuspri Entry for the B2BUnit variant \'%s\' ,Matkl \'%s\' ,quantity \'%s\' and deltaamount \'%s\' for deltameasure \'%s\' per deltaUnit \'%s\'", priceParameter.getB2bUnit().getUid(), zilliantPriceParameter.getZilProduct().getMatkl(), priceParameter.getQuantityWrapper().getQuantity(), zilCuspri.get().getDeltaAmount(), zilCuspri.get().getDeltaPerMeasure(), zilCuspri.get().getDeltaPerMeasureUom()));
                } else {
                    LOG.warn(String.format("Missing ZilCuspri Entry for the B2BUnit variant \'%s\' ,Matkl \'%s\' and quantity \'%s\' ", priceParameter.getB2bUnit().getUid(), zilliantPriceParameter.getZilProduct().getMatkl(), priceParameter.getQuantityWrapper().getQuantity()));
                }
            } catch (Exception e) {
                LOG.warn(String.format("Something went wrong to find ZilCuspri Entry for the B2BUnit variant \'%s\' ,Matkl \'%s\' and quantity \'%s\' ", priceParameter.getB2bUnit().getUid(), zilliantPriceParameter.getZilProduct().getMatkl(), priceParameter.getQuantityWrapper().getQuantity()) + e);
            }
        }
        return zilliantPriceParameter;
    }

    @Override
    public ZilliantPriceParameter findZilliantProduct(TkEuPriceParameter priceParamater, ZilliantPriceParameter zilliantPriceParameter) {
            final List<ZilProductModel> zilProducts = getTkEuZilProductService().findZilProductsById(priceParamater.getProduct().getCode());
            final Optional<ZilProductModel> firstZilProduct = zilProducts.stream().findFirst();
            if (firstZilProduct.isPresent()) {
                zilliantPriceParameter.setZilProduct(firstZilProduct.get());
            } else {
                throw new ZilPriceUnKnownStateException(String.format("Missing ZilProduct Entry for the selected variant \'%s\'", priceParamater.getProduct().getCode()));
            }
        return zilliantPriceParameter;
    }

    @Override
    public ZilliantPriceParameter findZilliantCustomer(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) {
        if (zilliantPriceParameter.getZilProduct() == null) {
            findZilliantProduct(priceParameter, zilliantPriceParameter);
            final List<ZilCustomerModel> zilCustomers = getTkEuZilCustomerService().findZilCustomersByIdAndSparte(priceParameter.getB2bUnit().getUid(),
                    zilliantPriceParameter.getZilProduct().getSparte());
            final Optional<ZilCustomerModel> firstZilCustomer = zilCustomers.stream().findFirst();
            if (firstZilCustomer.isPresent()) {
                zilliantPriceParameter.setZilCustomer(firstZilCustomer.get());
            } else {
                zilliantPriceParameter.setZilCustomer(new ZilCustomerModel());
                LOG.warn(String.format("Missing ZilCustomer Entry for the B2BUnit \'%s\' and division \'%s\'", priceParameter.getB2bUnit().getUid(),
                        zilliantPriceParameter.getZilProduct().getSparte()));
            }

        }
        return zilliantPriceParameter;
    }

    @Override
    public ZilliantPriceParameter findZilliantPrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) {
        if (zilliantPriceParameter.getZilCustomer() == null) {
            findZilliantCustomer(priceParameter, zilliantPriceParameter);
            final List<ZilPriceModel> zilPrices = getTkEuZilPriceService().findZilPriceByCustomerAndProduct(zilliantPriceParameter.getZilCustomer(), zilliantPriceParameter.getZilProduct());
            final Optional<ZilPriceModel> firstZilPrice = zilPrices.stream().findFirst();
            if (firstZilPrice.isPresent()) {
                zilliantPriceParameter.setZilPrice(firstZilPrice.get());
            } else {
                throw new ZilPriceUnKnownStateException(String.format("Missing ZilPrice Entry for the zilProductKey \'%s\'", zilliantPriceParameter.getZilProduct().getProductKey()));
            }
        }
        return zilliantPriceParameter;
    }

    public TkEuZilPriceService getTkEuZilPriceService() {
        return tkEuZilPriceService;
    }

    public void setTkEuZilPriceService(TkEuZilPriceService tkEuZilPriceService) {
        this.tkEuZilPriceService = tkEuZilPriceService;
    }

    public TkEuZilCustomerService getTkEuZilCustomerService() {
        return tkEuZilCustomerService;
    }

    public void setTkEuZilCustomerService(TkEuZilCustomerService tkEuZilCustomerService) {
        this.tkEuZilCustomerService = tkEuZilCustomerService;
    }

    public TkEuZilProductService getTkEuZilProductService() {
        return tkEuZilProductService;
    }

    public void setTkEuZilProductService(TkEuZilProductService tkEuZilProductService) {
        this.tkEuZilProductService = tkEuZilProductService;
    }

    public TkEuZilCuspriService getTkEuZilCuspriService() {
        return tkEuZilCuspriService;
    }

    public void setTkEuZilCuspriService(TkEuZilCuspriService tkEuZilCuspriService) {
        this.tkEuZilCuspriService = tkEuZilCuspriService;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }
}

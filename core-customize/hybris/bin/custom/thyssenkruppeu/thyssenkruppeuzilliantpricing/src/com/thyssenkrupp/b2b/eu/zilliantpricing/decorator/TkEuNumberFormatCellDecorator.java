package com.thyssenkrupp.b2b.eu.zilliantpricing.decorator;

import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.util.CSVCellDecorator;

public class TkEuNumberFormatCellDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuNumberFormatCellDecorator.class);

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        final String impexContent = impexLine.get(Integer.valueOf(position));
        if (!Objects.isNull(impexContent)) {
            LOG.debug(impexContent + " formatted to " + impexContent.split("[.]")[0]);
            return impexContent.split("[.]")[0];
        }
        return impexContent;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.dao;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

public interface TkEuZilPriceDao extends Dao {

    List<ZilPriceModel> findZilPriceByCustomerAndProduct(ZilCustomerModel zilCustomerModel, ZilProductModel zilProductModel);

    List<ZilPriceModel> findZilPriceByDefaultCustomerKey(ZilProductModel zilProductModel);
}

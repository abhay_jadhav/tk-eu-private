package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuUomConversionException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuAccumulationDiscountCalculationStrategy;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuCalculateAccumulatedQuantityStrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;

public class DefaultTkEuCalculateAccumulatedQuantityStrategy implements TkEuCalculateAccumulatedQuantityStrategy{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCartCalculationForExternalPriceStrategy.class);
    private TkEuAccumulationDiscountCalculationStrategy tkEuAccumulationDiscountCalculationStrategy;
    private TkEuUomConversionService                    tkEuUomConversionService;

    public MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException {
        final AbstractOrderModel abstractOrderModel = abstractOrderEntryModel.getOrder();
        if (BooleanUtils.isFalse(isInMemoryCart(abstractOrderModel)) && abstractOrderModel.getEntries().size() > 1) {
            return findAccumulatedQty(abstractOrderModel, abstractOrderEntryModel);
        }
        return buildMaterialQuantity(abstractOrderEntryModel.getPositionWeight(), abstractOrderModel.getWeightUnit());
    }

    public MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderModel cartModel, @NotNull AbstractOrderEntryModel abstractOrderEntryModel) {
        Map<String, List<AbstractOrderEntryModel>> accGroupMap = classifyCartEntriesByAccumulation(cartModel);
        if (CollectionUtils.isNotEmpty(accGroupMap.entrySet())) {
            final List<AbstractOrderEntryModel> accumulatedGroupEntries = accGroupMap.entrySet().stream()
                .map(entry -> entry.getValue()).filter(entryValue -> entryValue.contains(abstractOrderEntryModel)).findFirst()
                .orElse(Collections.EMPTY_LIST);
            try {
                return findAccumulatedQtyInWeightByOrderEntry(accumulatedGroupEntries, abstractOrderEntryModel);
            } catch (TkEuUomConversionException ex) {
                final List<String> productGroup = accumulatedGroupEntries.stream().map(abstractOrderEntry -> abstractOrderEntry.getProduct().getCode()).collect(Collectors.toList());
                LOG.error("Accumulation price can not be applied for the group {} with following reason ", productGroup, ex);
            }
        }
        return buildMaterialQuantity(abstractOrderEntryModel.getQuantity().doubleValue(), abstractOrderEntryModel.getUnit());
    }

    private boolean isInMemoryCart(AbstractOrderModel abstractOrderModel) {
        return abstractOrderModel instanceof InMemoryCartModel;
    }

    public Map<String, List<AbstractOrderEntryModel>> classifyCartEntriesByAccumulation(AbstractOrderModel cartModel) {
        Map<String, List<AbstractOrderEntryModel>> groupMap = new HashMap<>();
        for (AbstractOrderEntryModel cartEntry : cartModel.getEntries()) {
            getTkEuAccumulationDiscountCalculationStrategy().buildAccumulatedGroups(groupMap, cartEntry);
        }
        return groupMap;
    }

    public MaterialQuantityWrapper buildMaterialQuantity(double quantity, UnitModel unitModel) {
        return MaterialQuantityWrapperBuilder.builder().withQuantity(quantity).withUnit(unitModel).build();
    }

    public MaterialQuantityWrapper findAccumulatedQtyInWeightByOrderEntry(@NotNull List<AbstractOrderEntryModel> entries, @NotNull AbstractOrderEntryModel abstractOrderEntryModel) {
        final BigDecimal totalWeightInKg = getTkEuUomConversionService().calculateOrderEntriesTotalWeightInKg(entries);
        MaterialQuantityWrapper qtyInKgm = buildMaterialQuantity(totalWeightInKg.doubleValue(), abstractOrderEntryModel.getOrder().getWeightUnit());
        LOG.debug("Found Accumulated weight {} for the order entry {} ", qtyInKgm, abstractOrderEntryModel.getProduct().getCode());
        return qtyInKgm;
    }

    public TkEuAccumulationDiscountCalculationStrategy getTkEuAccumulationDiscountCalculationStrategy() {
        return tkEuAccumulationDiscountCalculationStrategy;
    }

    @Required
    public void setTkEuAccumulationDiscountCalculationStrategy(TkEuAccumulationDiscountCalculationStrategy tkEuAccumulationDiscountCalculationStrategy) {
        this.tkEuAccumulationDiscountCalculationStrategy = tkEuAccumulationDiscountCalculationStrategy;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }
}

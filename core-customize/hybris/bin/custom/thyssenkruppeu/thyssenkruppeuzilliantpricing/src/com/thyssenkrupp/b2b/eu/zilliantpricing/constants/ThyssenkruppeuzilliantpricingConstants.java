package com.thyssenkrupp.b2b.eu.zilliantpricing.constants;

public final class ThyssenkruppeuzilliantpricingConstants extends GeneratedThyssenkruppeuzilliantpricingConstants {
    public static final String EXTENSIONNAME      = "thyssenkruppeuzilliantpricing";
    public static final String PLATFORM_LOGO_CODE = "thyssenkruppeuzilliantpricingPlatformLogo";
    public static final String TK_EU_ZILLIANT_PRICE_CATALOG_ID   = "tkEuZilliantPriceCatalog";
    public static final String ZILLIANT_TS = "23:59:59";

    private ThyssenkruppeuzilliantpricingConstants() {
    }
}

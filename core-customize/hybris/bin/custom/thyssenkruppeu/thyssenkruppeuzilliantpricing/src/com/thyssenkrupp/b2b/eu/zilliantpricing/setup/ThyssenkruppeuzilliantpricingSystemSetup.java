package com.thyssenkrupp.b2b.eu.zilliantpricing.setup;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.ThyssenkruppeuzilliantpricingService;

@SystemSetup(extension = ThyssenkruppeuzilliantpricingConstants.EXTENSIONNAME)
public class ThyssenkruppeuzilliantpricingSystemSetup {
    private final ThyssenkruppeuzilliantpricingService thyssenkruppeuzilliantpricingService;

    public ThyssenkruppeuzilliantpricingSystemSetup(final ThyssenkruppeuzilliantpricingService thyssenkruppeuzilliantpricingService) {
        this.thyssenkruppeuzilliantpricingService = thyssenkruppeuzilliantpricingService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
    }

    private InputStream getImageStream() {
        return ThyssenkruppeuzilliantpricingSystemSetup.class.getResourceAsStream("/thyssenkruppeuzilliantpricing/sap-hybris-platform.png");
    }
}

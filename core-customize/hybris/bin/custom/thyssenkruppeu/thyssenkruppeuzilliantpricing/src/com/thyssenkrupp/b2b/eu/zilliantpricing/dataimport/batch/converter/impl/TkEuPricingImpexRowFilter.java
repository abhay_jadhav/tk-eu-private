package com.thyssenkrupp.b2b.eu.zilliantpricing.dataimport.batch.converter.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class TkEuPricingImpexRowFilter implements ImpexRowFilter {

    private Integer dateToColumn = Integer.valueOf(0);
    private Integer loekzColumn = Integer.valueOf(0);

    @Override
    public boolean filter(final Map<Integer, String> row) {

        final String dateTo = row.get(dateToColumn);
        final String loekz = row.get(loekzColumn);

        try {
            final DateFormat df = new SimpleDateFormat("yyyyMMdd");
            final Date parsedToDate = df.parse(dateTo);

            final Date compareToDate = new DateTime(new Date()).minusDays(1).toDate();

            if (parsedToDate.after(compareToDate) && loekz.isEmpty()) {
                return true;
            }
        } catch (final Exception e) {
            return false;
        }
        return false;
    }

    @Required
    public void setDateTo(final Integer dateTo) {
        this.dateToColumn = dateTo;
    }

    @Required
    public void setLoekz(final Integer loekz) {
        this.loekzColumn = loekz;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils;
import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuAccGrpMappingDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuAccGrpMappingModel;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

import static com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuAccGrpMappingModel.*;

public class DefaultTkEuAccGrpMappingDao extends DefaultGenericDao<TkEuAccGrpMappingModel> implements TkEuAccGrpMappingDao {
    private static final     Logger LOG                                             = LoggerFactory.getLogger(DefaultTkEuAccGrpMappingDao.class);
    private static final String FIND_ACCGRP_ID_BY_SALES_AREA_AND_MATERIAL_GROUP = "SELECT {" + PK + "} FROM {" + _TYPECODE + "} WHERE {"
        + SALESORGANIZATION + "}=?salesArea" + " " + "AND {" + MATERIALGROUP + "} =?materialGroup "
        + "AND {" + STARTDATE + "} <= ?currDate " + "AND" + "{" + ENDDATE + "}>?currDate AND {" + CATALOGVERSION + "}=?catalogVersion";

    private TkEuCatalogVersionService catalogVersionService;


    public DefaultTkEuAccGrpMappingDao(String typeCode) {
        super(typeCode);
    }

    @Override
    public List<TkEuAccGrpMappingModel> findAccGrpIdBySaleAreaAndMaterialGroup(String salesArea, String materialGroup) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ACCGRP_ID_BY_SALES_AREA_AND_MATERIAL_GROUP);
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        fQuery.addQueryParameter("salesArea", salesArea);
        fQuery.addQueryParameter("materialGroup", materialGroup);
        fQuery.addQueryParameter("currDate", TkEuPriceUtils.getTruncatedDate());
        fQuery.addQueryParameter("catalogVersion", catalogVersion);

        final SearchResult<TkEuAccGrpMappingModel> accGrpIds = getFlexibleSearchService().search(fQuery);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \'{}\' for params: salesArea=\'{}\', materialGroup=\'{}\'. Found {} results.", _TYPECODE, salesArea, materialGroup, accGrpIds.getResult().size());
            if (accGrpIds.getResult().size() > 0) {
                LOG.debug("Logging first accGrp: groupId=\'{}\', distributionChannel=\'{}\'.", accGrpIds.getResult().get(0).getAccumulationGroupId(), accGrpIds.getResult().get(0).getDistributionChannel());
            }
        }
        return accGrpIds.getResult();
    }

    public TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

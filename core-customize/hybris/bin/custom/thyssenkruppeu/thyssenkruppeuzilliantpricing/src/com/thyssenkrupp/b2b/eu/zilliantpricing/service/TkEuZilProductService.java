package com.thyssenkrupp.b2b.eu.zilliantpricing.service;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;

import java.util.List;

public interface TkEuZilProductService {

    List<ZilProductModel> findZilProductsById(String matnr);
}

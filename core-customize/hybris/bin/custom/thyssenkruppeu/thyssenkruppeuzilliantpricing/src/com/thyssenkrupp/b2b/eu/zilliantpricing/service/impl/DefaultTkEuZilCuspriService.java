package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilCuspriDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilCuspriService;

public class DefaultTkEuZilCuspriService implements TkEuZilCuspriService {

    private TkEuZilCuspriDao zilCuspriDao;

    @Override
    public List<ZilCuspriModel> findZilCuspriForCustomerIdMatklAndQty(String b2bUnitUid, String matkl, Double quantity) {
        return getZilCuspriDao().findZilCuspriForProductQuantity(b2bUnitUid, matkl, quantity);
    }

    @Override
    public Optional<ZilCuspriModel> findZilCuspriForCustomerIdMatklAndMinQtd(String b2bUnitUid, String matkl, String minqtd) {
        return getZilCuspriDao().findZilCuspriForMinQtd(b2bUnitUid, matkl, minqtd);
    }

    @Override
    public List<ZilCuspriModel> findZilCuspriForCustomerIdAndMatkl(String b2bUnitUid, String matkl) {
        return getZilCuspriDao().findZilCuspriEntries(b2bUnitUid, matkl);
    }

    @Override
    public Map<Double, ZilCuspriModel> findSortedZilCuspriForCustomerIdAndMatkl(String b2bUnitUid, String matkl) {
        List<ZilCuspriModel> zilCuspriList = getZilCuspriDao().findZilCuspriEntries(b2bUnitUid, matkl);
        Map<Double, ZilCuspriModel> zilCuspriMap = new HashMap<>();
        zilCuspriList.forEach(zilCuspri -> zilCuspriMap.put(zilCuspri.getLowerBound(), zilCuspri));
        return zilCuspriMap;
    }

    protected TkEuZilCuspriDao getZilCuspriDao() {
        return zilCuspriDao;
    }

    @Required
    public void setZilCuspriDao(TkEuZilCuspriDao zilCuspriDao) {
        this.zilCuspriDao = zilCuspriDao;
    }
}

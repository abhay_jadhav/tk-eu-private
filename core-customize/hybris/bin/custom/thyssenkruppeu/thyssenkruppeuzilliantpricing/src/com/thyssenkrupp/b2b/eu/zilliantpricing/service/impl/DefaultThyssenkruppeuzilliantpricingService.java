package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.zilliantpricing.service.ThyssenkruppeuzilliantpricingService;

public class DefaultThyssenkruppeuzilliantpricingService implements ThyssenkruppeuzilliantpricingService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultThyssenkruppeuzilliantpricingService.class);

    private MediaService          mediaService;
    private ModelService          modelService;
    private FlexibleSearchService flexibleSearchService;

    private InputStream getImageStream() {
        return DefaultThyssenkruppeuzilliantpricingService.class.getResourceAsStream("/thyssenkruppeuzilliantpricing/sap-hybris-platform.png");
    }

    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import java.util.List;
import java.util.Map;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceUnKnownStateException;
import org.apache.commons.lang.StringUtils;

import com.thyssenkrupp.b2b.eu.zilliantpricing.builder.TkEuAccumulationGroupIdBuilder;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuAccumulationDiscountCalculationStrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

public class DefaultTkEuAccumulationDiscountCalculationStrategy implements TkEuAccumulationDiscountCalculationStrategy {
    private static final String PRODUCT_DIVISION_GROUP_CODE = "A";

    private BaseStoreService               baseStoreService;
    private TkEuAccumulationGroupIdBuilder tkEuAccumulationGroupIdBuilder;

    @Override
    public void buildAccumulatedGroups(Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry) {
        ProductModel product = cartEntry.getProduct();
        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        final String salesArea = baseStore.getSAPConfiguration().getSapcommon_salesOrganization();
        final String materialGroup = product.getMaterialGroup();
        final String division = product.getDivision();

        String accGrpId = tkEuAccumulationGroupIdBuilder.findAccGrpIdBySaleAreaAndMaterialGroup(salesArea, materialGroup);
        if (StringUtils.isNotEmpty(accGrpId)) {
            getTkEuAccumulationGroupIdBuilder().buildKeyWithAccumulationGrpId(accGrpId, groupMap, cartEntry);
        } else if (shouldGroupByDivision(division)) {
            getTkEuAccumulationGroupIdBuilder().buildKeyWithDivision(division, groupMap, cartEntry);
        } else {
            getTkEuAccumulationGroupIdBuilder().buildKeyWithMaterialGroupAndConstant(materialGroup, groupMap, cartEntry);
        }
    }

    public boolean shouldGroupByDivision(String division) {
        if (StringUtils.isEmpty(division)) {
            throw new TkEuPriceServiceUnKnownStateException("division for the product can not be empty");
        }
        return division.startsWith(PRODUCT_DIVISION_GROUP_CODE);
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuAccumulationGroupIdBuilder getTkEuAccumulationGroupIdBuilder() {
        return tkEuAccumulationGroupIdBuilder;
    }

    public void setTkEuAccumulationGroupIdBuilder(TkEuAccumulationGroupIdBuilder tkEuAccumulationGroupIdBuilder) {
        this.tkEuAccumulationGroupIdBuilder = tkEuAccumulationGroupIdBuilder;
    }

}

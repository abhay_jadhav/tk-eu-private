package com.thyssenkrupp.b2b.eu.zilliantpricing.dao;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

public interface TkEuZilCustomerDao extends Dao {

    List<ZilCustomerModel> findZilCustomersByIdAndSparte(String customerId, String sparte);
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilCuspriDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel.*;

public class DefaultTkEuZilCuspriDao extends DefaultGenericDao<ZilCuspriModel> implements TkEuZilCuspriDao {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuZilCuspriDao.class);

    private static final String SELECT_CLAUSE = "SELECT {" + PK + "} FROM {" + _TYPECODE + "} ";
    private static final String COMMON_WHERE_CLAUSE = "WHERE {" + CUSTOMERID + "} = ?customerId AND {" + MATKL + "} = ?matkl AND {" + VALIDFROM + "} <= ?validfrom AND {" + VALIDTO + "} > ?validto AND {" + CATALOGVERSION + "} = ?catalogVersion ";

    private static final String FIND_ZILCUSPRI_FOR_CUSTOMER_MATKL_AND_QUANTITY = SELECT_CLAUSE + COMMON_WHERE_CLAUSE + "AND {" + LOWERBOUND + "} < ?quantity AND {" + UPPERBOUND + "} >= ?quantity";
    private static final String FIND_ZILCUSPRI_FOR_CUSTOMER_MATKL_AND_MINQTD = SELECT_CLAUSE + COMMON_WHERE_CLAUSE + "AND {" + LOWERBOUND + "} = ?minqtd ";
    private static final String FIND_ZILCUSPRI_FOR_CUSTOMER_AND_MATKL = SELECT_CLAUSE + COMMON_WHERE_CLAUSE;

    private TkEuCatalogVersionService catalogVersionService;

    public DefaultTkEuZilCuspriDao(String typeCode) {
        super(typeCode);
    }

    @Override
    public List<ZilCuspriModel> findZilCuspriForProductQuantity(String customerId, String matkl, Double quantity) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILCUSPRI_FOR_CUSTOMER_MATKL_AND_QUANTITY);
        populateQueryParams(fQuery, customerId, matkl);
        fQuery.addQueryParameter("quantity", quantity);
        SearchResult<ZilCuspriModel> zilCuspris = getFlexibleSearchService().search(fQuery);
        LOG.debug("Queried table \'{}\' for params: customerId=\'{}\', matkl=\'{}\', quantity=\'{}\'. Found {} results.", _TYPECODE, customerId, matkl, quantity, zilCuspris.getResult().size());
        return zilCuspris.getResult();
    }

    @Override
    public Optional<ZilCuspriModel> findZilCuspriForMinQtd(String customerId, String matkl, String minqtd) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILCUSPRI_FOR_CUSTOMER_MATKL_AND_MINQTD);
        populateQueryParams(fQuery, customerId, matkl);
        fQuery.addQueryParameter("minqtd", minqtd);
        SearchResult<ZilCuspriModel> zilCuspris = getFlexibleSearchService().search(fQuery);
        LOG.debug("Queried table \'{}\' for params: customerId=\'{}\', matkl=\'{}\', minqtd=\'{}\'. Found {} results.", _TYPECODE, customerId, matkl, minqtd, zilCuspris.getResult().size());
        return Collections.isEmpty(zilCuspris.getResult()) ? Optional.empty() : Optional.of(zilCuspris.getResult().get(0));
    }

    @Override
    public List<ZilCuspriModel> findZilCuspriEntries(String customerId, String matkl) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILCUSPRI_FOR_CUSTOMER_AND_MATKL);
        populateQueryParams(fQuery, customerId, matkl);
        SearchResult<ZilCuspriModel> zilCuspris = getFlexibleSearchService().search(fQuery);
        LOG.debug("Queried table \'{}\' for params: customerId=\'{}\', matkl=\'{}\'. Found {} results.", _TYPECODE, customerId, matkl, zilCuspris.getResult().size());
        return zilCuspris.getResult();
    }

    private void populateQueryParams(FlexibleSearchQuery fQuery, String customerId, String matkl) {
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        final Date date = getTruncatedDate();
        fQuery.addQueryParameter("validTo", date);
        fQuery.addQueryParameter("validFrom", date);
        fQuery.addQueryParameter("catalogVersion", catalogVersion);
        fQuery.addQueryParameter("customerId", customerId);
        fQuery.addQueryParameter("matkl", matkl);
    }

    protected TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

}

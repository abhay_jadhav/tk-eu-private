/*
 * Put your copyright text here
 */
package com.thyssenkrupp.b2b.eu.zilliantpricing.integration.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlobDirectory;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;
import com.thyssenkrupp.b2b.eu.zilliantpricing.integration.UnzipUtility;

import de.hybris.platform.util.Config;
import reactor.util.StringUtils;

public class DefaultUnzipUtilityImpl implements UnzipUtility {

    private static final Logger LOG = Logger.getLogger(DefaultUnzipUtilityImpl.class);
    private static final int BUFFER_SIZE = 4096;
    private static final String AZURE_HOTFOLDER_CLOB_ACCOUNT = "azure.hotfolder.storage.account.connection-string";
    private static final String AZURE_HOTFOLDER_CLOB_CONTAINER_NAME = "azure.hotfolder.storage.container.name";
    private static final String AZURE_HOTFOLDER_CLOB_FOLDER_PATH = "azure.hotfolder.storage.container.hotfolder";
    private static final String AZURE_HOTFOLDER_CLOB_ZILLIANT_SOURCE_PATH = "azure.hotfolder.storage.container.hotfolder.source";

    private String sourceDirPath;
    private String timeStampPattern;
    private String tempDirPath;
    private String archiveDirPath;
    private String controlFilePrefix;
    private String fileExtensionToExtract;
    private String baseDirectoryTkSchulte;
    private String fileExtensionToBeUploaded;

    @Override
    public void extractFileAndProcess(final Map<String, String> fileNameToPath) {
        final Optional<File> fileToProcess = getFileToProcessOrEmptyCCv2();
        if (fileToProcess.isPresent()) {
            final Map<String, String> fileNameWithTimeStamp = getFileNameToPathWithTimeStamp(fileToProcess.get(),
                    fileNameToPath);
            if (fileNameWithTimeStamp.isEmpty()) {
                LOG.warn("May be issue finding timestamp from control file, verify the zip entries");
            } else {
                fileNameWithTimeStamp
                        .forEach((key, value) -> filteredExpandZipFile(zipEntry -> zipEntry.getName().contains(key),
                                fileToProcess.get(), value));
                moveFilesAndRemoveTempDir(fileToProcess.get());
            }
        } else {
            LOG.info("No file found with the extension : " + getFileExtensionToExtract() + " to process");
        }
    }

    private void moveFilesAndRemoveTempDir(final File zipFile) {
        Assert.notNull(getTempDirPath(), "tempDirPath cannot be null");
        final File tempDirectoryPath = new File(getTempDirPath());
        moveZilliantFilesToHotFolderForCCV2(tempDirectoryPath);
        moveFileToArchive(tempDirectoryPath);
    }

    private void moveFileToArchive(File tempDirectoryPath) {
        final File[] files = tempDirectoryPath.listFiles();
        Assert.notNull(files);
        Assert.notNull(getArchiveDirPath(), "archiveDirPath cannot be null");
        checkIfPathExistsElseCreateDir(getArchiveDirPath());

        for (final File file : files) {
            if (!file.isDirectory()) {
                try {
                    if (file.renameTo(new File(getArchiveDirPath() + File.separator + file.getName()))) {
                        LOG.info("File is moved successful!" + file.getName());
                    } else {
                        LOG.error("File is failed to move!" + file.getName());
                    }
                } catch (Exception e) {
                    LOG.error("Exception in moveZilliantFilesToArchive for file:" + file.getName(), e);
                }
            }
        }

    }

    protected void moveZilliantFilesToHotFolderForCCV2(final File tempDirectoryPath) {
        FileInputStream inputStream = null;
        try {
            final File[] files = tempDirectoryPath.listFiles();
            Assert.notNull(files);
            Assert.notNull(getFileExtensionToBeUploaded(), "fileExtensionToBeUploaded cannot be null");

            CloudBlobDirectory blobDir = getClobDirForPath(AZURE_HOTFOLDER_CLOB_FOLDER_PATH);

            if (!Objects.isNull(blobDir)) {
                for (final File file : files) {
                    try {// NOSONAR
                        if (checkIfValidFile(file, getFileExtensionToBeUploaded())) {
                            CloudBlockBlob blob = blobDir.getBlockBlobReference(file.getName()); // NOSONAR
                            inputStream = new FileInputStream(file); // NOSONAR
                            blob.upload(inputStream, file.length());
                            LOG.info("Uploaded File " + file.getName() + "to Azure Bolb "
                                    + Config.getString(AZURE_HOTFOLDER_CLOB_FOLDER_PATH, ""));

                        }
                    } catch (Exception e) {
                        LOG.error("Exception in moveZilliantFilesToHotFolder for file:" + file.getName(), e);
                    } finally {
                        if (null != inputStream) {
                            inputStream.close();
                        }
                    }
                }
            }
        } catch (final IOException ioException) {
            LOG.error("Exception in moveZilliantFilesToHotFolder", ioException);
        } catch (Exception e) {
            LOG.error("Exception in moveZilliantFilesToHotFolder", e);
        }

    }

    private boolean checkIfValidFile(File file, String fileExtension) {
        if(!file.isDirectory() && StringUtils.endsWithIgnoreCase(file.getName(), fileExtension)) {
            return true;
        }
        return false;
    }

    private CloudBlobDirectory getClobDirForPath(String azureHotfolderClobFolderPath) {
        try {
            CloudStorageAccount account = CloudStorageAccount.parse(Config.getString(AZURE_HOTFOLDER_CLOB_ACCOUNT, ""));
            CloudBlobClient client = account.createCloudBlobClient();
            CloudBlobContainer container = client
                    .getContainerReference(Config.getString(AZURE_HOTFOLDER_CLOB_CONTAINER_NAME, "hybris"));
            return container.getDirectoryReference(Config.getString(azureHotfolderClobFolderPath, "master/hotfolder"));
        } catch (Exception e) {
            LOG.error(
                    "Exception in getting Azure Blob path for " + azureHotfolderClobFolderPath + " :" + e.getMessage());
        }
        return null;
    }

    
    protected Map<String, String> getFileNameToPathWithTimeStamp(final File fileToProcess,
            final Map<String, String> fileNameToPath) {
        final Optional<String> controlFileName = getControlFileName(fileToProcess);
        final Map<String, String> fileNameToPathWithTimeStamp = new HashMap<>();
        if (controlFileName.isPresent()) {
            final Optional<String> timeStampFromControlFile = getTimeStampFromControlFile(controlFileName.get());
            timeStampFromControlFile.ifPresent(timeStampToAppend -> fileNameToPath.forEach((key, value) -> {
                final String fileNameWithTimeStamp = String.format(value, timeStampToAppend);
                fileNameToPathWithTimeStamp.put(key, fileNameWithTimeStamp);
            }));
        } else {
            LOG.warn("Control File is not present in the zip file, verify the zip entries");
        }
        return fileNameToPathWithTimeStamp;
    }

    private Optional<String> getControlFileName(final File zipFile) {
        Assert.notNull(getControlFilePrefix(), "controlFilePrefix cannot be null");
        try (ZipInputStream stream = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {
                if (entry.getName().contains(getControlFilePrefix())) {
                    return Optional.of(entry.getName());
                }
            }
        } catch (final IOException ioException) {
            LOG.error("Exception to find control file", ioException);
        }
        return Optional.empty();
    }

    private Optional<String> getTimeStampFromControlFile(final String controlFileName) {
        Assert.notNull(getTimeStampPattern(), "timeStampPattern cannot be null");
        final Pattern regexPattern = Pattern.compile(getTimeStampPattern());
        final Matcher match = regexPattern.matcher(controlFileName);
        if (match.find()) {
            return Optional.of(match.group(1));
        }
        return Optional.empty();
    }

    
    private Optional<File> getFileToProcessOrEmptyCCv2() {
        Assert.notNull(getSourceDirPath(), "sourceDirPath cannot be null");
        Assert.notNull(getFileExtensionToExtract(), "fileExtensionToExtract cannot be null");

        try {
            CloudBlobDirectory blobDir = getClobDirForPath(AZURE_HOTFOLDER_CLOB_ZILLIANT_SOURCE_PATH);
            if (!Objects.isNull(blobDir) && !Objects.isNull(blobDir.listBlobs())) {// NOSONAR
                for (ListBlobItem blobFile : blobDir.listBlobs()) {
                    Optional<File> file = downloadFileFromAzure(blobFile);
                    if (file.isPresent()) {
                        return file;
                    }
                }
            } else {
                LOG.error("No new Files found in Zilliant Source path Azure Blob location ");

            }

        } catch (Exception e) {
            LOG.error("Exception in reading file from Azure Blob ", e);
        }
        return Optional.empty();
    }

    private Optional<File> downloadFileFromAzure(ListBlobItem blobFile) {
        try {
            LOG.info("File to be read from BLOB source is :" + blobFile.getUri());
            if (blobFile instanceof CloudBlob) {
                CloudBlob blobFileItem = (CloudBlob) blobFile;
                if (StringUtils.endsWithIgnoreCase(blobFileItem.getName(), getFileExtensionToExtract())) {
                    Path path = Paths.get(blobFileItem.getName());
                    checkIfPathExistsElseCreateDir(tempDirPath);
                    blobFileItem.download(new FileOutputStream(tempDirPath + File.separator + path.getFileName()));
                    LOG.info("Deleting from from source after download :" + blobFileItem.deleteIfExists());
                    return Optional.of(new File(tempDirPath + File.separator + path.getFileName()));
                }
            }
        } catch (FileNotFoundException | StorageException e) {
            LOG.error("Exception in reading file from Azure Blob ", e);
        }
        return Optional.empty();
    }

    private void checkIfPathExistsElseCreateDir(String dirPath) {
        try {
            File directory = new File(String.valueOf(dirPath));
            LOG.info("Path to be checked :" + dirPath);
            if (!directory.exists()) {
                LOG.info("Created Path since it doesnot Exists :" + dirPath + " Status: " + directory.mkdirs());
            }
        } catch (Exception e) {
            LOG.error("Exception in Creating dir for " + dirPath + ": " + e.getMessage(), e);
        }
    }

    protected void filteredExpandZipFile(final Predicate<ZipEntry> filter, final File zipFile,
            final String csvFileName) {
        try (ZipInputStream stream = new ZipInputStream(new FileInputStream(zipFile))) {
            LOG.info("Zip file : " + zipFile.getName() + " has been opened");
            ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {
                if (filter.test(entry)) {
                    LOG.info("Matched file " + entry.getName());
                    extractFileFromArchive(stream, csvFileName);
                }
            }
        } catch (final IOException ioException) {
            LOG.error("Exception in filteredExpandZipFile : ", ioException);
        }
    }

    private void extractFileFromArchive(final ZipInputStream stream, final String csvFileName) {
        final File tempFile = getTempFile(csvFileName);
        try (FileOutputStream fileOutputStream = new FileOutputStream(tempFile, true)) {
            final byte[] buffer = new byte[BUFFER_SIZE];
            int len;
            while ((len = stream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, len);
            }
        } catch (final IOException ioException) {
            LOG.error("Exception in extractFileFromArchive : ", ioException);
        }
    }

    private File getTempFile(final String csvFileName) {
        Assert.notNull(getTempDirPath(), "tempDirPath cannot be null");
        final File tempFile = new File(getTempDirPath() + File.separator + csvFileName);
        try {
            if (isValidDirectoryOrFile(tempFile)) {
                return tempFile;
            }
        } catch (final IOException ioException) {
            LOG.error("Exception in getTempFile : ", ioException);
        }
        return tempFile;
    }

    private boolean isValidDirectoryOrFile(final File dirOrFilePath) throws IOException {
        return dirOrFilePath.getParentFile().mkdirs() || isValidFile(dirOrFilePath);
    }

    private boolean isValidFile(final File filePath) throws IOException {
        return filePath.createNewFile() || filePath.exists();
    }

    protected String getSourceDirPath() {
        return sourceDirPath;
    }

    @Required
    public void setSourceDirPath(String sourceDirPath) {
        this.sourceDirPath = sourceDirPath;
    }

    public String getTimeStampPattern() {
        return timeStampPattern;
    }

    @Required
    public void setTimeStampPattern(String timeStampPattern) {
        this.timeStampPattern = timeStampPattern;
    }

    protected String getTempDirPath() {
        return tempDirPath;
    }

    @Required
    public void setTempDirPath(String tempDirPath) {
        this.tempDirPath = tempDirPath;
    }

    protected String getArchiveDirPath() {
        return archiveDirPath;
    }

    @Required
    public void setArchiveDirPath(String archiveDirPath) {
        this.archiveDirPath = archiveDirPath;
    }

    protected String getControlFilePrefix() {
        return controlFilePrefix;
    }

    @Required
    public void setControlFilePrefix(String controlFilePrefix) {
        this.controlFilePrefix = controlFilePrefix;
    }

    protected String getFileExtensionToExtract() {
        return fileExtensionToExtract;
    }

    @Required
    public void setFileExtensionToExtract(String fileExtensionToExtract) {
        this.fileExtensionToExtract = fileExtensionToExtract;
    }

    protected String getBaseDirectoryTkSchulte() {
        return baseDirectoryTkSchulte;
    }

    @Required
    public void setBaseDirectoryTkSchulte(String baseDirectoryTkSchulte) {
        this.baseDirectoryTkSchulte = baseDirectoryTkSchulte;
    }

    public String getFileExtensionToBeUploaded() {
        return fileExtensionToBeUploaded;
    }

    public void setFileExtensionToBeUploaded(String fileExtensionToBeUploaded) {
        this.fileExtensionToBeUploaded = fileExtensionToBeUploaded;
    }
}

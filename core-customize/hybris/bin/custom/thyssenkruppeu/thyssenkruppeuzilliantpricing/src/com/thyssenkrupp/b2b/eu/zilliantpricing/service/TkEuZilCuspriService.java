package com.thyssenkrupp.b2b.eu.zilliantpricing.service;

import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCuspriModel;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface TkEuZilCuspriService {

    /**
     * To get ZilCuspri records for Customer ID, Matkl and Product Quantity.
     * 
     * @param b2bUnitUid
     * @param matkl
     * @param quantity
     * @return list of ZilCuspriModel
     */
    List<ZilCuspriModel> findZilCuspriForCustomerIdMatklAndQty(String b2bUnitUid, String matkl, Double quantity);

    /**
     * To get ZilCuspri records for Customer ID, Matkl and Minimum Quantity.
     * 
     * @param b2bUnitUid
     * @param matkl
     * @param minqtd
     * @return valid ZilCuspriModel
     */
    Optional<ZilCuspriModel> findZilCuspriForCustomerIdMatklAndMinQtd(String b2bUnitUid, String matkl, String minqtd);

    /**
     * To get ZilCuspri records for Customer ID and Matkl.
     * 
     * @param b2bUnitUid
     * @param matkl
     * @return list of ZilCuspriModel
     */
    List<ZilCuspriModel> findZilCuspriForCustomerIdAndMatkl(String b2bUnitUid, String matkl);

    /**
     * To get ZilCuspri records for Customer ID and Matkl.
     * 
     * @param b2bUnitUid
     * @param matkl
     * @return Map of ZilCuspriModel with lower bound as a key
     */
    Map<Double, ZilCuspriModel> findSortedZilCuspriForCustomerIdAndMatkl(String b2bUnitUid, String matkl);

}

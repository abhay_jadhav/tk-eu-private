package com.thyssenkrupp.b2b.eu.zilliantpricing.dataimport.batch.converter.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;
import java.util.stream.Stream;

public class TkEuZilliantImpexConverter extends DefaultImpexConverter {

    private static final char   PLUS_CHAR             = '+';
    private static final String SEMICOLON_CHAR        = ";";
    private static final String DOUBLE_QUOTES_CHAR    = "\"";
    private static final String ESCAPED_DOUBLE_QUOTES = "<ESCAPED_DOUBLE_QUOTES>";
    private static final String ESCAPED_SEMICOLON     = "<ESCAPED_SEMICOLON>";
    private String impexRow;

    @Override
    public String convert(final Map<Integer, String> row, final Long sequenceId) {
        String value = super.convert(row, sequenceId);
        if (StringUtils.isNotEmpty(value) && Stream.of(ESCAPED_DOUBLE_QUOTES, ESCAPED_SEMICOLON).anyMatch(value::contains)) {
            value = unEscapeSemiColon(value);
        }
        return value;
    }

    @Override
    protected void processValues(final Map<Integer, String> row, final StringBuilder builder, final int idx, final int endIdx) {
        final boolean mandatory = getImpexRow().charAt(idx + 1) == PLUS_CHAR;
        Integer mapIdx;
        try {
            mapIdx = Integer.valueOf(getImpexRow().substring(mandatory ? idx + 2 : idx + 1, endIdx));
        } catch (final NumberFormatException e) {
            throw new SystemException("Invalid row syntax [invalid column number]: " + getImpexRow(), e);
        }
        final String colValue = row.get(mapIdx);
        if (mandatory && StringUtils.isBlank(colValue)) {
            throw new IllegalArgumentException("Missing value for " + mapIdx);
        }
        if (colValue != null) {
            builder.append(escapeSemiColon(colValue));
        }
    }

    private String escapeSemiColon(final String colValue) {
        StringBuilder value = new StringBuilder();
        if (StringUtils.isNotEmpty(colValue) && colValue.contains(SEMICOLON_CHAR)) {
            value.append(ESCAPED_DOUBLE_QUOTES).append(colValue.replaceAll(SEMICOLON_CHAR, ESCAPED_SEMICOLON)).append(ESCAPED_DOUBLE_QUOTES);
        } else {
            value.append(colValue);
        }
        return value.toString();
    }

    private String unEscapeSemiColon(final String value) {
        return StringUtils.replaceEach(value, new String[] { ESCAPED_DOUBLE_QUOTES, ESCAPED_SEMICOLON }, new String[] { DOUBLE_QUOTES_CHAR, SEMICOLON_CHAR });
    }

    public String getImpexRow() {
        return impexRow;
    }

    @Required
    public void setImpexRow(final String impexRow) {
        super.setImpexRow(impexRow);
        this.impexRow = impexRow;
    }
}

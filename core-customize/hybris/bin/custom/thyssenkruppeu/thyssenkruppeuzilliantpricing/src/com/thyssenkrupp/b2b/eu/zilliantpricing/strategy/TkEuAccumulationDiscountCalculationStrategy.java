package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import java.util.List;
import java.util.Map;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public interface TkEuAccumulationDiscountCalculationStrategy {

    void buildAccumulatedGroups(Map<String, List<AbstractOrderEntryModel>> groupMap, AbstractOrderEntryModel cartEntry);

}

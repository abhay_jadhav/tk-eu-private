package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;

public interface TkEuZilliantPriceModelFindingStrategy {

    ZilliantPriceParameter findZilliantProduct(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter);

    ZilliantPriceParameter findZilliantCustomer(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter);

    ZilliantPriceParameter findZilliantPrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter);

    ZilliantPriceParameter findZilliantCuspri(TkEuPriceParameter priceParamater, ZilliantPriceParameter zilliantPriceParameter, String minqtd);
}

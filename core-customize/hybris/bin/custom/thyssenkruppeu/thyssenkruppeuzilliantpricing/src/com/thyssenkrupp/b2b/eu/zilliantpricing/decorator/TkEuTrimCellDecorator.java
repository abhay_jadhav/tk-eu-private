package com.thyssenkrupp.b2b.eu.zilliantpricing.decorator;

import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.util.CSVCellDecorator;

public class TkEuTrimCellDecorator implements CSVCellDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuTrimCellDecorator.class);

    @Override
    public String decorate(int position, Map<Integer, String> impexLine) {
        final String impexContent = impexLine.get(Integer.valueOf(position));
        if (!Objects.isNull(impexContent)) {
            LOG.debug(impexContent + " white spaces trimmed to " + StringUtils.trim(impexContent));
            return StringUtils.trim(impexContent);
        }
        return null;
    }

}

package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

public interface TkEuCalculateAccumulatedQuantityStrategy {

    MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException;

    MaterialQuantityWrapper findAccumulatedQty(@NotNull AbstractOrderModel cartModel, @NotNull AbstractOrderEntryModel abstractOrderEntryModel);

    Map<String, List<AbstractOrderEntryModel>> classifyCartEntriesByAccumulation(AbstractOrderModel cartModel);

    MaterialQuantityWrapper findAccumulatedQtyInWeightByOrderEntry(@NotNull List<AbstractOrderEntryModel> entries, @NotNull AbstractOrderEntryModel abstractOrderEntryModel);

}

package com.thyssenkrupp.b2b.eu.zilliantpricing.integration;

import java.util.Map;

public interface UnzipUtility {

    void extractFileAndProcess(Map<String, String> fileNameToPath);
}

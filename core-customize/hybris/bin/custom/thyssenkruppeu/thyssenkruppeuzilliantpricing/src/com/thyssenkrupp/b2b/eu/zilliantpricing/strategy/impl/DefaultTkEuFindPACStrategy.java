package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_WEIGHTED_TYPE_CODE;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuConditionKeyBuilderSupplier;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuFindPACStrategy;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuPriceRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.pricedata.service.impl.AbstractTkEuConditionDiscountRowFindingStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.PriceValue;

public class DefaultTkEuFindPACStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy implements TkEuFindPACStrategy {

    public static final String PAC_COND_KEY = "Y2MA";
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindPACStrategy.class);
    private TkEuPriceRowDao     tkEuPriceRowDao;
    private TkEuUomConversionService tkEuUomConversionService;
    private TkEuRoundingService roundingService;
    private CostConfigurationProductInfoService costConfigurationProductInfoService;
    private CommonI18NService commonI18nService;

      //for displaying scales on pdp
      @Override
      public List<PriceInformation> getPACPrices(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
       String scaleConditionKey = getPACConditionKeys(priceParameter);
       List<DiscountRowModel> scalePriceRowsByCondKey = findPacScalePriceRowsByConditionKey(scaleConditionKey);
       scalePriceRowsByCondKey.stream().sorted(Comparator.comparing(DiscountRowModel::getMinqtd));
       List<PriceInformation> pI= createScaledPriceMap(scalePriceRowsByCondKey, priceParameter);
       return pI;
       }

      //pac for specified quantity for pdp and cart
      @Override
      public PriceInformation getPACPriceForQuantity(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
          String scaleConditionKey = getPACConditionKeys(priceParameter);
          if(!scaleConditionKey.isEmpty()) {
          return convertAndCalculateScaledBasePrice(priceParameter, scaleConditionKey);
          } else
          return new PriceInformation(new HashMap(), new PriceValue(priceParameter.getCurrencyCode(), 0.0d, false));
          }

      protected String getPACConditionKeys(TkEuPriceParameter priceParameter) {
        TkEuConditionKeyBuilderSupplier scalePriceKeyBuilderSupplier = getDefaultTkEuConditionKeyBuilderSupplier().getPACKeyBuilderSupplier();
        final List<Pair<GenericDao, List<String>>> conditionKeys = scalePriceKeyBuilderSupplier.get().stream().map(tkEuPriceConditionKeyBuilderFactory -> tkEuPriceConditionKeyBuilderFactory.build(priceParameter)).collect(Collectors.toList());
        LOG.debug("PAC Scale Condition keys = " + conditionKeys);
        return conditionKeys.stream().findFirst().map(genericDaoListPair -> genericDaoListPair.getValue().stream().findFirst().orElse(StringUtils.EMPTY)).orElse(StringUtils.EMPTY);
    }

    private List<PriceInformation> createScaledPriceMap(@NotNull List<DiscountRowModel> list, TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        List<PriceInformation> scaledDiscounts = new ArrayList<>();
        String isoCode = priceParameter.getCurrencyCode();
        if(!list.isEmpty()){
        for (DiscountRowModel priceRowCondA800Model : list) {
            if(!Objects.isNull(priceRowCondA800Model.getUnit()) && priceRowCondA800Model.getUnit().equals(priceParameter.getProduct().getBaseUnit())) {
            Long minQtd = priceRowCondA800Model.getMinqtd();
            Map qualifiers = new HashMap();
            qualifiers.put(PriceRow.MINQTD, minQtd);
            qualifiers.put(PriceRow.UNIT, priceRowCondA800Model.getScaleUnit());
            qualifiers.put(PriceRow.UNITFACTOR, getUnitFactorForPac(priceRowCondA800Model));

            BigDecimal calcPrice = BigDecimal.valueOf(Math.abs(priceRowCondA800Model.getValue()));
            BigDecimal roundedPriceInBaseUom = getRoundingService().roundScaledBasePrice(calcPrice);
            PriceInformation priceInfo = new PriceInformation(qualifiers, new PriceValue(isoCode, roundedPriceInBaseUom.doubleValue(), false));
            scaledDiscounts.add(priceInfo);
        }
        }
        return scaledDiscounts;
        } else {
             return scaledDiscounts;
        }
    }

        private long getUnitFactorForPac(DiscountRowModel priceRowCondA800Model) {
        if(Objects.nonNull(priceRowCondA800Model) && Objects.nonNull(priceRowCondA800Model.getUnitFactor())) {
            return (long)priceRowCondA800Model.getUnitFactor()== 0 ? 1 : priceRowCondA800Model.getUnitFactor();
        }
        return 1L;
    }

        private PriceInformation convertAndCalculateScaledBasePrice(TkEuPriceParameter priceParameter, String scaleConditionKey) throws TkEuPriceServiceException {
        ProductModel product = priceParameter.getProduct();
        String isoCode = priceParameter.getCurrencyCode();
        final DiscountRowModel pacRow = findPacDiscountRow(priceParameter, scaleConditionKey);
        if(!Objects.isNull(pacRow)) {
        final Long scaleMinQtd = Objects.nonNull(pacRow.getMinqtd()) ? pacRow.getMinqtd() : 0L;
        Map qualifiers = new HashMap();
        qualifiers.put(PriceRow.MINQTD, scaleMinQtd);
        BigDecimal pacPrice = BigDecimal.valueOf(0.0d);
        if (!Objects.isNull(pacRow.getValue())) {
            qualifiers.put(PriceRow.PRICE, pacRow.getValue());
            pacPrice = BigDecimal.valueOf(pacRow.getValue());
            LOG.debug("Discounted pac Price In product Unit :" + pacPrice);
        }
        BigDecimal scaleFactor = findPacScaleFactorFromDiscountRow(pacRow);
        Integer unitFactor= Objects.isNull(pacRow.getUnitFactor()) ? 1 : pacRow.getUnitFactor();
        AbstractOrderEntryModel orderEntry = priceParameter.getOrderEntry();
        if (orderEntry != null) {
            CostConfigurationProductInfoModel pacCostConfigurationProductInfo = getCostConfigurationProductInfoService().createPacCostConfigurationProductInfo(orderEntry, getCommonI18nService().getCurrentCurrency(), unitFactor.intValue(), product.getBaseUnit(), pacPrice.doubleValue());
            getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(orderEntry, pacCostConfigurationProductInfo);
             LOG.debug("Pac Price for config object= " + pacPrice);
        }
        return new PriceInformation(qualifiers, new PriceValue(isoCode, scaleFactor.doubleValue(), false));
        }
        return new PriceInformation(new HashMap(), new PriceValue(priceParameter.getCurrencyCode(), 0.0d, false));
    }

    protected DiscountRowModel findPacDiscountRow(TkEuPriceParameter priceParameter, String scaleConditionKey) throws TkEuPriceServiceException {
        final List<DiscountRowModel> scalePriceRowsByCondKey = findPacScalePriceRowsByConditionKey(scaleConditionKey);
        MaterialQuantityWrapper quantityWrapper = priceParameter.getQuantityWrapper();
        quantityWrapper.setUnitModel(priceParameter.getProduct().getBaseUnit());
        try {
            MaterialQuantityWrapper materialQuantityInScaleUnit = new MaterialQuantityWrapper();
            priceParameter.setQuantityWrapper(quantityWrapper);
            if (CollectionUtils.isNotEmpty(scalePriceRowsByCondKey)) {
                LOG.debug("Calculating material quantity in scale unit for product "+ priceParameter.getProduct().getCode()+" base unit of product is "+priceParameter.getProduct().getBaseUnit()+" and quantity before conversion is "+priceParameter.getQuantityWrapper().getQuantity());
                materialQuantityInScaleUnit = setUnitAndQuantityForPac(scalePriceRowsByCondKey, priceParameter.getQuantityWrapper(), priceParameter.getProduct());
                if(!Objects.isNull(materialQuantityInScaleUnit)) {
                applyCommercialWeight(priceParameter, materialQuantityInScaleUnit);
                LOG.debug("Querying A805 table for product "+priceParameter.getProduct().getCode()+" for quantity "+(long) materialQuantityInScaleUnit.getQuantity());
                final DiscountRowModel matchingDiscountRowByQty = findScalePriceRowsByConditionKeyAndQuantity(scaleConditionKey, (long) materialQuantityInScaleUnit.getQuantity());
                return matchingDiscountRowByQty;
                }
                LOG.info("Pac is invalid");
            }
        } catch (TkEuPriceServiceException e) {
            LOG.error("findPacScaleFactor Error :" + e.getMessage());
        }
        return null;
    }

    public boolean ifScalesArePresent(List<DiscountRowModel> scalePriceRowsByCondKey) {
        for(DiscountRowModel d: scalePriceRowsByCondKey) {
            if(d.getSapConditionId().contains("_")) {
                return true;
            }
        }
        return false;
    }

    public MaterialQuantityWrapper setUnitAndQuantityForPac(List<DiscountRowModel> scalePriceRowsByCondKey, MaterialQuantityWrapper quantityWrapper, ProductModel product) {
        UnitModel targetUnit = null;
        if(!Objects.isNull(product.getBaseUnit()) && !Objects.isNull(scalePriceRowsByCondKey.get(0).getUnit()) && product.getBaseUnit().getCode().equals(scalePriceRowsByCondKey.get(0).getUnit().getCode())) {
        if(ifScalesArePresent(scalePriceRowsByCondKey) && !Objects.isNull(scalePriceRowsByCondKey.get(0).getScaleUnit()) ) {
            targetUnit = scalePriceRowsByCondKey.get(0).getScaleUnit();
            LOG.debug("Pac scales are present, Target unit for pac scales is "+ targetUnit.getCode());
        } else if(!ifScalesArePresent(scalePriceRowsByCondKey)){
            targetUnit = scalePriceRowsByCondKey.get(0).getUnit();
        }
        } else {
            LOG.debug("Pac data is invalid, base unit of product is "+ product.getBaseUnit() + " and pac price unit is "+ scalePriceRowsByCondKey.get(0).getUnit() );
            return null;
        }
        return calculateQuantityInTargetUnitForPac(quantityWrapper, targetUnit, product);
    }

    public BigDecimal findPacScaleFactorFromDiscountRow(@Nullable DiscountRowModel discountRowModel) {
        if (discountRowModel == null) {
            LOG.warn("No PriceRowCondA805 found hence returning scale factor as \"Null\".");
            return null;
        }
        BigDecimal discountRowFactor = BigDecimal.valueOf(Math.abs(discountRowModel.getValue()));
        BigDecimal roundedScaleFactor = getRoundingService().roundScaledBasePrice(discountRowFactor.divide(safelyConvertUnitFactor(discountRowModel), 4, RoundingMode.HALF_UP));
        LOG.debug("calculated scale factor from 805 table is "+roundedScaleFactor.doubleValue()+" for matching quantity "+discountRowModel.getMinqtd()+"Unit of pacdiscount row is "+ discountRowModel.getUnit().getCode());
        return roundedScaleFactor;
    }

    private void applyCommercialWeight(TkEuPriceParameter priceParameter, MaterialQuantityWrapper materialQuantityInScaleUnit) throws TkEuPriceServiceException {
        if (StringUtils.equalsIgnoreCase(SAP_KGM_UNIT_CODE, materialQuantityInScaleUnit.getUnitModel().getCode())) {
            if (!StringUtils.equals(SAP_WEIGHTED_TYPE_CODE, priceParameter.getQuantityWrapper().getUnitModel().getUnitType())) {
                ProductModel productModel = priceParameter.getProduct();
                BigDecimal commercialWeight = getTkEuUomConversionService().applyCommercialWeight(productModel, productModel.getBaseUnit(), BigDecimal.valueOf(materialQuantityInScaleUnit.getQuantity()), getTkEuUomConversionService().getSupportedUomConversions(productModel));
                materialQuantityInScaleUnit.setQuantity(commercialWeight.doubleValue());
                LOG.debug("Applying commercial weight" + materialQuantityInScaleUnit.getQuantity());
            }
        }
        LOG.debug("Commercial wt not applied");

    }

    private BigDecimal safelyConvertUnitFactor(@NotNull DiscountRowModel priceRowCondA800Model) {
        Integer unitFactor = priceRowCondA800Model.getUnitFactor();
        return unitFactor == 0 ? BigDecimal.ONE : BigDecimal.valueOf(unitFactor);
    }

    public MaterialQuantityWrapper calculateQuantityInTargetUnitForPac(MaterialQuantityWrapper sourceMaterialQty, UnitModel targetUnit, ProductModel product) {
        BigDecimal quantity=BigDecimal.valueOf(sourceMaterialQty.getQuantity());
        MaterialQuantityWrapper qtdWrapper = null;
        BigDecimal roundedQuantity = calculateQuantityInBaseUnit(sourceMaterialQty, product);
        try {
            if(!product.getBaseUnit().getCode().equals(targetUnit.getCode())) {
            List<TkUomConversionModel> conversionFactors = getTkEuUomConversionService().getSupportedUomConversions(product);
            for (TkUomConversionModel conversionFactor : conversionFactors) {
                if(conversionFactor.getSalesUnit().equals(targetUnit) && conversionFactor.getBaseUnit().getCode().equalsIgnoreCase(product.getBaseUnit().getCode())){
                    Double baseUnitFactor = conversionFactor.getBaseUnitFactor();
                    Double salesUnitFactor = conversionFactor.getSalesUnitFactor();
                    quantity = (BigDecimal.valueOf(salesUnitFactor).divide(BigDecimal.valueOf(baseUnitFactor),4,RoundingMode.HALF_UP)).multiply(roundedQuantity);
                    roundedQuantity = quantity.setScale(2, RoundingMode.HALF_UP);
                    LOG.debug("Calculated quantity in target unit for pac, unit is "+targetUnit.getCode()+" quantity= "+roundedQuantity.doubleValue());
                    qtdWrapper= buildMaterialQuantity(roundedQuantity.doubleValue(), targetUnit);
                }
            }
            } else{
            roundedQuantity = roundedQuantity.setScale(2, RoundingMode.HALF_UP);
            LOG.debug("Calculated quantity in target unit for pac, unit is "+ targetUnit.getCode()+" quantity= "+roundedQuantity.doubleValue());
            qtdWrapper= buildMaterialQuantity(roundedQuantity.doubleValue(), targetUnit);
            }
        } catch (TkEuPriceServiceException e) {
            LOG.error("Error in calculateQuantityInScaleUnitForPac Error :" + e.getMessage());
        }
        return qtdWrapper;
    }

    public BigDecimal calculateQuantityInBaseUnit(MaterialQuantityWrapper sourceMaterialQty,ProductModel product) {
        BigDecimal quantity=BigDecimal.valueOf(sourceMaterialQty.getQuantity());
        UnitModel unit= new UnitModel(SAP_KGM_UNIT_CODE, SAP_WEIGHTED_TYPE_CODE);
        try {
            List<TkUomConversionModel> conversionFactors = getTkEuUomConversionService().getSupportedUomConversions(product);
            for (TkUomConversionModel conversionFactor : conversionFactors) {
                if(conversionFactor.getSalesUnit().getCode().equals(unit.getCode()) && conversionFactor.getBaseUnit().getCode().equals(product.getBaseUnit().getCode())){
                    Double baseUnitFactor = conversionFactor.getBaseUnitFactor();
                    Double salesUnitFactor = conversionFactor.getSalesUnitFactor();
                    quantity = (BigDecimal.valueOf(baseUnitFactor).divide(BigDecimal.valueOf(salesUnitFactor),4,RoundingMode.HALF_UP)).multiply(quantity);
                    BigDecimal roundedQuantity = quantity.setScale(2, RoundingMode.HALF_UP);
                    LOG.debug("Calculated quantity in base unit for pac "+ roundedQuantity.doubleValue());
                    return roundedQuantity;
                }
            }
        } catch (TkEuPriceServiceException e) {
            LOG.error("Error in method calculateQuantityInBaseUnit for pac "+e.getMessage());
        }
        return quantity;
    }

    private MaterialQuantityWrapper buildMaterialQuantity(double quantity, UnitModel unitModel) {
        return MaterialQuantityWrapperBuilder.builder().withQuantity(quantity).withUnit(unitModel).build();
    }

    @Override
    public List<DiscountRowModel> findPacScalePriceRowsByConditionKey(String scaleConditionKey) {

     return getTkEuPriceRowDao().findPriceRowByCondKey(scaleConditionKey, PAC_COND_KEY);
    }

    @Override
    public DiscountRowModel findScalePriceRowsByConditionKeyAndQuantity(String scaleConditionKey, Long quantityInScaleUnit) {
     return getTkEuPriceRowDao().findPriceRowByCondKey(scaleConditionKey, PAC_COND_KEY, quantityInScaleUnit);
    }

    public TkEuPriceRowDao getTkEuPriceRowDao() {
        return tkEuPriceRowDao;
    }

    @Required
    public void setTkEuPriceRowDao(TkEuPriceRowDao tkEuPriceRowDao) {
        this.tkEuPriceRowDao = tkEuPriceRowDao;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }
    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public TkEuRoundingService getRoundingService() {
        return roundingService;
    }
    @Required
    public void setRoundingService(TkEuRoundingService roundingService) {
        this.roundingService = roundingService;
    }

    public CostConfigurationProductInfoService getCostConfigurationProductInfoService() {
        return costConfigurationProductInfoService;
    }

    @Required
    public void setCostConfigurationProductInfoService(
            CostConfigurationProductInfoService costConfigurationProductInfoService) {
        this.costConfigurationProductInfoService = costConfigurationProductInfoService;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }
}

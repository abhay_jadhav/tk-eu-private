package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.service.impl.DefaultTkEuPriceConditionKeyBuilderStrategy;

public class TkEuZilliantScalePriceKeyBuilderStrategy extends DefaultTkEuPriceConditionKeyBuilderStrategy {

    private static final String SINGLE_SPACE = " ";

    @Override
    protected String getJoiningKey() {
        return SINGLE_SPACE;
    }
}

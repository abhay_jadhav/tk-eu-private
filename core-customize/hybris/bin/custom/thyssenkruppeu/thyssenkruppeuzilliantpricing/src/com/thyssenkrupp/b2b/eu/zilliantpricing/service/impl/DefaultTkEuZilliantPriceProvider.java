package com.thyssenkrupp.b2b.eu.zilliantpricing.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.exception.ZilPriceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.TkEuZilliantPriceProvider;
import com.thyssenkrupp.b2b.eu.zilliantpricing.service.data.ZilliantPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.strategy.TkEuZilliantPriceCalculationStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.math.BigDecimal;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class DefaultTkEuZilliantPriceProvider implements TkEuZilliantPriceProvider {

    private TkEuZilliantPriceCalculationStrategy tkEuZilliantPriceCalculationStrategy;

    @Override
    public BigDecimal materialBasePrice(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException {
        validateInitialState(zilliantPriceParameter);
        ProductModel product = priceParameter.getProduct();
        validateParameterNotNullStandardMessage("productId", product.getCode());
        validateMaterialBaseUnit(product);

        checkArgument(priceParameter.getQuantityWrapper().getQuantity() > 0, "Material Should Need A Valid Quantity");
        return getTkEuZilliantPriceCalculationStrategy().materialBasePrice(priceParameter, zilliantPriceParameter);
    }

    @Override
    public List<PriceInformation> materialScaledBasePrices(TkEuPriceParameter priceParameter, ZilliantPriceParameter zilliantPriceParameter) throws TkEuPriceServiceException {
        validateInitialState(zilliantPriceParameter);
        ProductModel product = priceParameter.getProduct();
        validateParameterNotNullStandardMessage("productId", product.getCode());
        validateParameterNotNullStandardMessage("Currency Code", priceParameter.getCurrencyCode());
        validateMaterialBaseUnit(product);

        checkArgument(priceParameter.getQuantityWrapper().getQuantity() > 0, "Material Should Need A Valid Quantity");
        return getTkEuZilliantPriceCalculationStrategy().materialScaledBasePrices(priceParameter, zilliantPriceParameter);
    }

    private void validateMaterialBaseUnit(ProductModel product) {
        if (product.getBaseUnit() == null) {
            throw new ZilPriceUnKnownStateException(String.format("Material \"%s\" has no base unit attached", product.getCode()));
        }
    }

    private void validateInitialState(ZilliantPriceParameter zilliantPriceParameter) {
        validateParameterNotNullStandardMessage("ZilPriceModel", zilliantPriceParameter.getZilPrice());
        validateParameterNotNullStandardMessage("ZilProductModel", zilliantPriceParameter.getZilProduct());
    }

    public TkEuZilliantPriceCalculationStrategy getTkEuZilliantPriceCalculationStrategy() {
        return tkEuZilliantPriceCalculationStrategy;
    }

    public void setTkEuZilliantPriceCalculationStrategy(TkEuZilliantPriceCalculationStrategy tkEuZilliantPriceCalculationStrategy) {
        this.tkEuZilliantPriceCalculationStrategy = tkEuZilliantPriceCalculationStrategy;
    }
}

package com.thyssenkrupp.b2b.eu.zilliantpricing.strategy;

import javax.validation.constraints.NotNull;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.util.PriceValue;

public interface TkEuCartCalculationForExternalPriceStrategy {

     PriceValue calculateAccumulatedBasePriceForOrderEntryForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, PriceValue priceValue) throws TkEuPriceServiceException;

     PriceValue getOrderEntryBasePriceInProductBaseUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, double tradeLength, PriceValue priceValue) throws TkEuPriceServiceException;

     PriceValue getBasePriceBasedOnAvailableSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity, ProductModel productModel, String currencyIsoCode, double tradeLength, UnitModel salesUnit, PriceValue priceValue) throws TkEuPriceServiceException;

     PriceValue getOrderEntryBasePriceInSalesUomForExternalPrice(AbstractOrderEntryModel orderEntryModel, String currencyIsoCode, @NotNull B2BUnitModel b2BUnitModel, ProductModel productModel, MaterialQuantityWrapper materialQuantity, TkUomConversionModel selectedUomConversionModel, double tradeLength, PriceValue priceValue) throws TkEuPriceServiceException;
}

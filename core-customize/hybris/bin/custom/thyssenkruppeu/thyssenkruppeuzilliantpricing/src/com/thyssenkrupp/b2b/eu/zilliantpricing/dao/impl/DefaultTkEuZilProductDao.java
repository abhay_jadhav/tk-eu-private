package com.thyssenkrupp.b2b.eu.zilliantpricing.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilProductDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.List;

import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel.*;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuZilProductDao extends DefaultGenericDao<ZilProductModel> implements TkEuZilProductDao {
    private static final     Logger LOG                   = LoggerFactory.getLogger(DefaultTkEuZilProductDao.class);
    private static final String FIND_ZILPRODUCT_BY_ID = "SELECT {" + PK + "} FROM {" + _TYPECODE + "} WHERE {" + PRODUCTID + "}=?productId" + " "
        + "AND {" + VALIDFROM + "} <= ?validfrom " + "AND" + "{" + VALIDTO + "}>?validto AND {" + CATALOGVERSION + "}=?catalogVersion";

    private TkEuCatalogVersionService catalogVersionService;

    public DefaultTkEuZilProductDao(String typeCode) {
        super(typeCode);
    }

    @Override
    public List<ZilProductModel> findZilProductsById(final String productId) {
        validateParameterNotNull(productId, "product ID must not be null");
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        final Date date = getTruncatedDate();
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILPRODUCT_BY_ID);
        fQuery.addQueryParameter("productId", productId);
        fQuery.addQueryParameter("validTo", date);
        fQuery.addQueryParameter("validFrom", date);
        fQuery.addQueryParameter("catalogVersion", catalogVersion);

        final SearchResult<ZilProductModel> zilProducts = getFlexibleSearchService().search(fQuery);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \'{}\' for params: productId=\'{}\'. Found {} results.", _TYPECODE, productId, zilProducts.getResult().size());
            if (zilProducts.getResult().size() > 0) {
                LOG.debug("Logging first zilProduct: productKey=\'{}\', matkl=\'{}\', wrkst=\'{}\', sparte=\'{}\'.", zilProducts.getResult().get(0).getProductKey(), zilProducts.getResult().get(0).getMatkl(), zilProducts.getResult().get(0).getWrkst(), zilProducts.getResult().get(0).getSparte());
            }
        }
        return zilProducts.getResult();
    }

    public TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

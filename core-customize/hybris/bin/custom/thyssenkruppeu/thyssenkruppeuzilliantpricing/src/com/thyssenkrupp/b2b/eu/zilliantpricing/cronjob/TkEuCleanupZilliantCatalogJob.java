package com.thyssenkrupp.b2b.eu.zilliantpricing.cronjob;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.zilliantpricing.integration.dataimport.batch.task.TkEuCleanupHelper;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.TkEuCleanupZilliantCatalogJobModel;

import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

public class TkEuCleanupZilliantCatalogJob extends AbstractJobPerformable<TkEuCleanupZilliantCatalogJobModel> {
    private static final Logger LOG = Logger.getLogger(TkEuCleanupZilliantCatalogJob.class);
    private TkEuCleanupHelper tkEuCleanupHelper;

    @Override
    public PerformResult perform(TkEuCleanupZilliantCatalogJobModel priceCatalogModel) {
        try {
            getTkEuCleanupHelper().cleanupZilliantRecords();
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception exception) {
            LOG.error("Exception occurred while cleaning up inactive price catalog:", exception);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    public TkEuCleanupHelper getTkEuCleanupHelper() {
        return tkEuCleanupHelper;
    }

    public void setTkEuCleanupHelper(TkEuCleanupHelper tkEuCleanupHelper) {
        this.tkEuCleanupHelper = tkEuCleanupHelper;
    }

}

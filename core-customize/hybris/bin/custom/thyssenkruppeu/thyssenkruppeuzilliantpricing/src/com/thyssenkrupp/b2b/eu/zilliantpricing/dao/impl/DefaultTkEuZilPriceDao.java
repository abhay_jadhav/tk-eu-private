package com.thyssenkrupp.b2b.eu.zilliantpricing.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.dao.TkEuZilPriceDao;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilCustomerModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.List;

import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.constants.ThyssenkruppeuzilliantpricingConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel.*;

public class DefaultTkEuZilPriceDao extends DefaultGenericDao<ZilPriceModel> implements TkEuZilPriceDao {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuZilPriceDao.class);

    private static final String SECOND_LEVEL_CUSTOMER = "2";

    private static final String FIND_ZILPRICE_BY_CUSTOMER_AND_PRODUCT_KEY = "SELECT {" + PK + "} FROM {" + _TYPECODE + "} WHERE {" + PRODUCTKEY + "}=?productKey" + " "
        + "AND {" + CUSTOMERKEY + "} =?customerKey " + "AND {" + VALIDFROM + "} <= ?validfrom " + "AND" + "{" + VALIDTO + "}>?validto AND {" + CATALOGVERSION + "}=?catalogVersion";

    private String defaultZilCustomerKey;

    private TkEuCatalogVersionService catalogVersionService;


    public DefaultTkEuZilPriceDao(String typeCode) {
        super(typeCode);
    }

    @Override
    public List<ZilPriceModel> findZilPriceByCustomerAndProduct(ZilCustomerModel zilCustomerModel, ZilProductModel zilProductModel) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILPRICE_BY_CUSTOMER_AND_PRODUCT_KEY);

        selectCustomerKey(zilProductModel.getCustomerLevel(), zilCustomerModel, fQuery);
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        final Date date = getTruncatedDate();
        fQuery.addQueryParameter("productKey", zilProductModel.getProductKey());
        fQuery.addQueryParameter("validTo", date);
        fQuery.addQueryParameter("validFrom", date);
        fQuery.addQueryParameter("catalogVersion", catalogVersion);

        SearchResult<ZilPriceModel> zilPrices = getFlexibleSearchService().search(fQuery);
        LOG.debug("Queried table \'{}\' for params: productKey=\'{}\', customerKey=\'{}\'. Found {} results.", _TYPECODE, zilProductModel.getProductKey(), fQuery.getQueryParameters().get("customerKey"), zilPrices.getResult().size());
        if (zilPrices.getResult().size() > 0) {
            LOG.debug("Logging first zilPrice: scaleKey=\'{}\'.", zilPrices.getResult().get(0).getScaleKey());
            return zilPrices.getResult();
        } else {
            LOG.info("Couldn't find target factor for customer {} trying to find it with default customer key.", zilCustomerModel.getCustomerKey2());
            return findZilPriceByDefaultCustomerKey(zilProductModel);
        }
    }

    @Override
    public List<ZilPriceModel> findZilPriceByDefaultCustomerKey(ZilProductModel zilProductModel) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_ZILPRICE_BY_CUSTOMER_AND_PRODUCT_KEY);
        final Date date = getTruncatedDate();
        final CatalogVersionModel catalogVersion = getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID);
        fQuery.addQueryParameter("productKey", zilProductModel.getProductKey());
        fQuery.addQueryParameter("validTo", date);
        fQuery.addQueryParameter("validFrom", date);
        fQuery.addQueryParameter("customerKey", getDefaultZilCustomerKey());
        fQuery.addQueryParameter("catalogVersion", catalogVersion);

        SearchResult<ZilPriceModel> zilPrices = getFlexibleSearchService().search(fQuery);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \"{}\" for params: productKey=\'{}\', customerKey=\'{}\'. Found {} results.", _TYPECODE, zilProductModel.getProductKey(), getDefaultZilCustomerKey(), zilPrices.getResult().size());
            if (zilPrices.getResult().size() > 0) {
                LOG.debug("Logging first zilPrice: scaleKey=\'{}\'.", zilPrices.getResult().get(0).getScaleKey());
            }
        }
        return zilPrices.getResult();
    }

    private void selectCustomerKey(String productCustomerLevel, ZilCustomerModel zilCustomerModel, FlexibleSearchQuery fQuery) {
        if (StringUtils.isBlank(zilCustomerModel.getCustomerKey2()) && StringUtils.isBlank(zilCustomerModel.getCustomerKey1())) {
            fQuery.addQueryParameter("customerKey", getDefaultZilCustomerKey());
        } else if (SECOND_LEVEL_CUSTOMER.equals(productCustomerLevel)) {
            fQuery.addQueryParameter("customerKey", zilCustomerModel.getCustomerKey2());
        } else {
            fQuery.addQueryParameter("customerKey", zilCustomerModel.getCustomerKey1());
        }
    }

    public String getDefaultZilCustomerKey() {
        return defaultZilCustomerKey;
    }

    public void setDefaultZilCustomerKey(String defaultZilCustomerKey) {
        this.defaultZilCustomerKey = defaultZilCustomerKey;
    }

    public TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

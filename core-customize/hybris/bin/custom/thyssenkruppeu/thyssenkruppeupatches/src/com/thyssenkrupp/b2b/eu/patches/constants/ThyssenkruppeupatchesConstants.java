package com.thyssenkrupp.b2b.eu.patches.constants;

/**
 * Global class for all Thyssenkruppeupatches constants. You can add global constants for your
 * extension into this class.
 */
public final class ThyssenkruppeupatchesConstants extends GeneratedThyssenkruppeupatchesConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeupatches";

    private ThyssenkruppeupatchesConstants() {
        // empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension

}

package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class Sprint19SampleDataPatch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_19_00_sampledata";

    public Sprint19SampleDataPatch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, singletonList(TkEnvironment.DEV));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_19_00_001_PriceData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_002_cutToLengthSampleData.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }
}

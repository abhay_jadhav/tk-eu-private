package com.thyssenkrupp.b2b.eu.patches.releases.release4;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class Sprint28Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_4_28_00";

    public Sprint28Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_4_28_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_28_00_001-email.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_28_00_001-cms-content.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_28_00_001-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
    }
}

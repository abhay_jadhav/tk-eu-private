package com.thyssenkrupp.b2b.eu.patches.releases.release4;

import static java.util.Collections.singletonList;

import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint23HemDevDataSetUpPatch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_4_23_00_hem_dev_data_setup";

    public Sprint23HemDevDataSetUpPatch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, singletonList(TkEnvironment.DEV));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_4_23_00_hem_dev_data_setup_001-shopData.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }
}

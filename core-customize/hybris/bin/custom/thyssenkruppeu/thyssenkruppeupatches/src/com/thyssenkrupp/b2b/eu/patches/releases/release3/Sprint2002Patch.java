package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class Sprint2002Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_20_02";

    public Sprint2002Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_20_02_001-product_category_assignment.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
    }
}

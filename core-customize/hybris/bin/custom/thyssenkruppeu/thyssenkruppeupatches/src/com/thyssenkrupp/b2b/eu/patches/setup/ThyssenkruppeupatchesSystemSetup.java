package com.thyssenkrupp.b2b.eu.patches.setup;

import com.thyssenkrupp.b2b.eu.patches.constants.ThyssenkruppeupatchesConstants;
import com.thyssenkrupp.b2b.eu.core.service.TkSynchronizationService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.patches.AbstractPatchesSystemSetup;
import de.hybris.platform.util.Config;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

@SystemSetup(extension = ThyssenkruppeupatchesConstants.EXTENSIONNAME)
public class ThyssenkruppeupatchesSystemSetup extends AbstractPatchesSystemSetup {

    private static final String CURRENT_ENV_ID = Config.getParameter("environmentId");

    private TkSynchronizationService synchronizationService;
    private SetupImpexService        setupImpexService;

    @Override
    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
    public void createEssentialData(final SystemSetupContext setupContext) {

        super.createEssentialData(setupContext);
        if ("PROD".equals(CURRENT_ENV_ID)) {

            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_prod_solr_config.impex", true);
            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_prod_datahub_config.impex", true);
        } else if ("UAT".equals(CURRENT_ENV_ID)) {
            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_stag_solr_config.impex", true);
            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_stag_datahub_config.impex", true);
        } else if ("QA".equals(CURRENT_ENV_ID)) {

            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_test_solr_config.impex", true);
            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_test_datahub_config.impex", true);
        } else if ("DEV".equals(CURRENT_ENV_ID)) {

            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_dev_solr_config.impex", true);
            getSetupImpexService().importImpexFile("/thyssenkruppeupatches/releases/release_global/patch_essential/hem_dev_datahub_config.impex", true);
        }
    }

    @Override
    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext setupContext) {
        super.createProjectData(setupContext);
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        return super.getInitializationOptions();
    }

    public TkSynchronizationService getSynchronizationService() {
        return synchronizationService;
    }

    @Required
    public void setSynchronizationService(TkSynchronizationService synchronizationService) {
        this.synchronizationService = synchronizationService;
    }

    public SetupImpexService getSetupImpexService() {
        return setupImpexService;
    }

    @Required
    public void setSetupImpexService(SetupImpexService setupImpexService) {
        this.setupImpexService = setupImpexService;
    }
}

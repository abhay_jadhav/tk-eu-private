package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class Sprint18HemProdDataSetUpPatch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_18_00_hem_prod_data_setup";

    public Sprint18HemProdDataSetUpPatch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, singletonList(TkEnvironment.PROD));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_18_00_hem_prod_data_setup_tkSchulte_header.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_18_00_hem_prod_data_setup_001-globalData.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_3_18_00_hem_prod_data_setup_globalHeader.impex", languages, updateLanguagesOnly);
    }
}

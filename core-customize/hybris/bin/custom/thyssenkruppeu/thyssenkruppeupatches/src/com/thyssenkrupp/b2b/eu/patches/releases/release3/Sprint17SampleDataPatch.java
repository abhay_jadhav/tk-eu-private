package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class Sprint17SampleDataPatch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_17_00_sampledata";

    public Sprint17SampleDataPatch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, singletonList(TkEnvironment.DEV));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_17_00_001-order.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }
}

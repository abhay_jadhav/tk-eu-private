package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;


public class Sprint20Patch extends AbstractTkEuPatch {
    private static final String PATH_ID = "sprint_3_20_00";
    private static final String UPDATE_CATELOGVERSION_ZILITEM  ="update zilproduct set p_catalogversion = (select pk from catalogversions where p_version = 'TkEuZilliantCatalogVersion1') where p_catalogversion is null";
    private static final String UPDATE_CATELOGVERSION_ZILCUSTOMER  ="update zilcustomer set p_catalogversion = (select pk from catalogversions where p_version = 'TkEuZilliantCatalogVersion1') where p_catalogversion is null";
    private static final String UPDATE_CATELOGVERSION_ZILPRICE  ="update zilprice set p_catalogversion = (select pk from catalogversions where p_version = 'TkEuZilliantCatalogVersion1') where p_catalogversion is null";
    private static final String UPDATE_CATELOGVERSION_ACCUMULATIONGROUP  ="update tkeuaccgrpmapping set p_catalogversion = (select pk from catalogversions where p_version = 'TkEuZilliantCatalogVersion1') where p_catalogversion is null";

    public Sprint20Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_20_00_001-priceCatalog.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_20_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_20_00_002-zilliant-cronjob.impex", languages, unit, updateLanguagesOnly);
        executeUpdateOnDB("Update catalog version for existing zilProduct items",UPDATE_CATELOGVERSION_ZILITEM);
        executeUpdateOnDB("Update catalog version for existing accumulation group entries",UPDATE_CATELOGVERSION_ACCUMULATIONGROUP);
        executeUpdateOnDB("Update catalog version for existing zil customer entries",UPDATE_CATELOGVERSION_ZILCUSTOMER);
        executeUpdateOnDB("Update catalog version for existing zil price entries",UPDATE_CATELOGVERSION_ZILPRICE);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_20_00_001-globalData.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_3_20_00_001-globalRemoveData.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_3_20_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
       
    }
}

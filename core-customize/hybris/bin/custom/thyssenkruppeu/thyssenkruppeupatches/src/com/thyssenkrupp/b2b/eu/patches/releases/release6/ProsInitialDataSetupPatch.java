/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.patches.releases.release6;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class ProsInitialDataSetupPatch extends AbstractTkEuPatch {
    private static final String PATH_ID = "sprint_pros_config_data";

    public ProsInitialDataSetupPatch() {
        super(PATH_ID, PATH_ID, TkRelease.R6, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_pros_config_data_01-shopConfiguration.impex", languages, unit, updateLanguagesOnly);

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }
}


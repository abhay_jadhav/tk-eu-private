package com.thyssenkrupp.b2b.eu.patches.releases.release4;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint30Patch extends AbstractTkEuPatch{

    private static final String PATH_ID = "sprint_4_30_00";

    public Sprint30Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_4_30_00_002-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_30_00_003-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_30_00_004-email.impex", languages, unit, updateLanguagesOnly);
     }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_4_30_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);

    }

}


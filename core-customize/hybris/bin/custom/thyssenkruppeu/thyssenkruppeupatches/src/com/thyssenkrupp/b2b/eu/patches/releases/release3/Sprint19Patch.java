package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class Sprint19Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_19_00";
    private static final String UPDATE_ONLINE_CONSIGNMENTS ="UPDATE consignments set p_salesorg='W111',p_distributionchannel='EC',p_division='00' where p_salesorg is null and p_distributionchannel is null and p_division is null";
    private static final String UPDATE_ONLINE_INVOICES ="UPDATE tkinvoice set p_salesorg='W111',p_distributionchannel='EC',p_division='00' where p_salesorg is null and p_distributionchannel is null and p_division is null";

    public Sprint19Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_19_00_001_priceData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_002-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_001-email.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_003-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_001-cms-content.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_004_cutToLength_category.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_005-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_19_00_001_contactBasesiteTicketCategory.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_19_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
        executeUpdateOnDB("Alter datatype of TkCutToLengthConfiguredProductInfo.toleranceValue from Double to String","ALTER TABLE abstrcfgproductinfo MODIFY COLUMN p_tolerancevalue varchar(255)");
        executeUpdateOnDB("Update online consignments for missing sales area values",UPDATE_ONLINE_CONSIGNMENTS);
        executeUpdateOnDB("Update online invoices for missing sales area values",UPDATE_ONLINE_INVOICES);
    }

}

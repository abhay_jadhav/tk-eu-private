package com.thyssenkrupp.b2b.eu.patches.releases.release4;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class Sprint23Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_4_23_00";

    public Sprint23Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_4_23_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_23_00_001_priceData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_23_00_001-email.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_23_00_001-cms-content.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_4_23_00_001_sap_configuration.impex", languages, unit, updateLanguagesOnly);

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_4_23_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_4_23_00_001-globalData.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_4_23_00_002-cart-LocalizedMessages.impex", languages, updateLanguagesOnly);
    }
}

package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;


public class Sprint21Patch extends AbstractTkEuPatch {
    private static final String PATH_ID = "sprint_3_21_00";
    private static final String UPDATE_PRODUCT_LIFECYCLESTATUS ="UPDATE products set p_lifecyclestatus = (SELECT pk FROM enumerationvalues WHERE (code = 'TK_050_Complete')) where p_baseproduct is null";


    public Sprint21Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_21_00_001-cronJobScript.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_001-email.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_001-solr.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_002-baseProductGenerationJob.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_003-compositeCronJobs.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_21_00_001-cms-content.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_21_00_001-globalData.impex", languages, updateLanguagesOnly);
        executeUpdateOnDB("Update the productlifecyclestatus of base products to TK_050_Complete", UPDATE_PRODUCT_LIFECYCLESTATUS);
    }
}

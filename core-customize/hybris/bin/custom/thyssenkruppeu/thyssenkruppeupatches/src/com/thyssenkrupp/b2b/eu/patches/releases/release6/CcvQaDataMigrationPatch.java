package com.thyssenkrupp.b2b.eu.patches.releases.release6;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class CcvQaDataMigrationPatch extends AbstractTkEuPatch{
        private static final String PATH_ID = "ccv_qa_data_migration";

        public CcvQaDataMigrationPatch() {
            super(PATH_ID, PATH_ID, TkRelease.R6, TkStructureState.V1, Arrays.asList(TkEnvironment.QA));
        }

        @Override
        public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
            importShopSpecificData("rccv_qa_data_migration_001-shopConfiguration.impex", languages, unit, updateLanguagesOnly);
            importShopSpecificData("rccv_qa_data_migration_002-solr.impex", languages, unit, updateLanguagesOnly);

        }

        @Override
        public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        }

}

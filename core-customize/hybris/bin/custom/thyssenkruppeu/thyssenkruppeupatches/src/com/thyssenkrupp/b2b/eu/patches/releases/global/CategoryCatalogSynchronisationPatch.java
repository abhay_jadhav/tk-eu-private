package com.thyssenkrupp.b2b.eu.patches.releases.global;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import com.thyssenkrupp.b2b.eu.core.service.TkSynchronizationService;
import de.hybris.platform.patches.Rerunnable;
import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.StructureState;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;

public class CategoryCatalogSynchronisationPatch extends AbstractTkEuPatch implements Rerunnable {

    private static final String PATCH_ID = "category_catalog_synchronisation_patch";

    private TkSynchronizationService synchronizationService;

    public CategoryCatalogSynchronisationPatch() {
        super(PATCH_ID, PATCH_ID, TkRelease.Global, TkStructureState.V1, singletonList(TkEnvironment.ALL));
    }

    @Override
    public void createProjectData(StructureState structureState) {
        final List<String> shopNameList = Stream.of(TkShopOrganisation.values()).map(TkShopOrganisation::getCode).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(shopNameList)) {
            for (String shopName : shopNameList) {
                getSynchronizationService().synchronizeCategoryCatalog(shopName);
            }
        }
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }

    public TkSynchronizationService getSynchronizationService() {
        return synchronizationService;
    }

    public void setSynchronizationService(TkSynchronizationService synchronizationService) {
        this.synchronizationService = synchronizationService;
    }
}


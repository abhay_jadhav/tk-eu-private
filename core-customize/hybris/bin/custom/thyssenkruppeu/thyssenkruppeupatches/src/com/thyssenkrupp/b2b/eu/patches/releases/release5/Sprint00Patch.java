/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.patches.releases.release5;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint00Patch extends AbstractTkEuPatch {
    private static final String PATH_ID = "sprint_5_00_00";

    public Sprint00Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R5, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_5_00_00_001-cronJobScript.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
    }
}


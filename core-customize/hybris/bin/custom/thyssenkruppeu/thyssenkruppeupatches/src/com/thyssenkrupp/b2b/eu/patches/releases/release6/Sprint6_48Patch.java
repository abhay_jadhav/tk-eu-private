package com.thyssenkrupp.b2b.eu.patches.releases.release6;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint6_48Patch extends AbstractTkEuPatch{
    private static final String PATH_ID = "sprint_6_48_00";

    public Sprint6_48Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R6, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages,
            boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_6_48_00_essentialdata-definitions.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_6_48_01_004-email.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
    }
}

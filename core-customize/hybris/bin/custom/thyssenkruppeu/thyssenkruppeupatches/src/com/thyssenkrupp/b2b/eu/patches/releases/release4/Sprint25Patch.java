package com.thyssenkrupp.b2b.eu.patches.releases.release4;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint25Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_4_25_00";

    public Sprint25Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R4, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_4_25_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_4_25_00_001-globalData.impex", languages, updateLanguagesOnly);
    }
}

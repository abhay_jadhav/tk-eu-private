/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.patches.releases.release5;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;

import de.hybris.platform.patches.organisation.ImportLanguage;

public class Sprint5_32Patch extends AbstractTkEuPatch {
    private static final String PATH_ID = "sprint_5_32_00";

    public Sprint5_32Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R5, TkStructureState.V1, Arrays.asList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_5_32_00_002-cronJobScript.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_5_32_01_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_5_32_00_003-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_5_32_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
    }
}


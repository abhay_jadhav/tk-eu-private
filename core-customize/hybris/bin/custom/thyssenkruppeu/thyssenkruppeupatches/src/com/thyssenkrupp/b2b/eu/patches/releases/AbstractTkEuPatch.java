package com.thyssenkrupp.b2b.eu.patches.releases;

import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import de.hybris.platform.patches.AbstractPatch;
import de.hybris.platform.patches.Release;
import de.hybris.platform.patches.data.ImpexImportUnitOption;
import de.hybris.platform.patches.internal.logger.PatchLogger;
import de.hybris.platform.patches.internal.logger.PatchLoggerFactory;
import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.StructureState;
import de.hybris.platform.patches.utils.StructureStateUtils;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static de.hybris.platform.patches.internal.logger.PatchLogger.LoggingMode.HAC_CONSOLE;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.EnumUtils.getEnum;
import static org.apache.commons.lang3.EnumUtils.isValidEnum;

public abstract class AbstractTkEuPatch extends AbstractPatch implements TkEuPatch {

    private static final PatchLogger LOG            = PatchLoggerFactory.getLogger(AbstractTkEuPatch.class);
    private static final String      CURRENT_ENV_ID = Config.getParameter("environmentId");
    private final Collection<TkEnvironment> validEnvironments;

    public AbstractTkEuPatch(String patchId, String patchName, Release release, StructureState structureState, Collection<TkEnvironment> validEnvironments) {
        super(patchId, patchName, release, structureState);
        this.validEnvironments = validEnvironments;
    }

    @Override
    public void createProjectData(StructureState structureState) {
        if (isValidEnvironment()) {
            final boolean update = structureState != this.structureState;
            createGlobalData(structureState, update);
            createShopsData(structureState, update);
        } else {
            LOG.warn(HAC_CONSOLE, "Skipping patch:{} , cannot execute patch:{} in environment:{}", this.getPatchName(), this.getPatchName(), CURRENT_ENV_ID);
        }
    }

    private boolean isValidEnvironment() {
        final Collection<TkEnvironment> environments = getValidEnvironments();
        return isNotEmpty(environments) && StringUtils.isNotEmpty(CURRENT_ENV_ID) &&
            (environments.contains(TkEnvironment.ALL) || (isValidEnum(TkEnvironment.class, CURRENT_ENV_ID) && environments.contains(getEnum(TkEnvironment.class, CURRENT_ENV_ID))));
    }

    protected void createShopsData(final StructureState structureState, final boolean update) {
        final Set<TkShopOrganisation> newShops = new HashSet<>();
        final Set<TkShopOrganisation> updateShops = new HashSet<>();
        for (final TkShopOrganisation shop : TkShopOrganisation.values()) {
            if (shop.getStructureState() == structureState) {
                newShops.add(shop);
            } else if (structureState.isAfter(shop.getStructureState())) {
                updateShops.add(shop);
            }
        }
        createShopsData(newShops, false);
        createShopsData(updateShops, update);
    }

    protected void createShopsData(final Collection<TkShopOrganisation> shops, final boolean update) {
        for (final TkShopOrganisation shop : shops) {
            if (update) {
                LOG.info(HAC_CONSOLE, "Update shop specific data for {}", shop.getName());
            } else {
                LOG.info(HAC_CONSOLE, "Creating shop specific data for {}", shop.getName());
            }
            createShopData(shop, shop.getLanguages(), update);
        }
    }

    protected void createGlobalData(final StructureState structureState, final boolean update) {
        Set<ImportLanguage> importLanguages;
        String message;
        if (update) {
            importLanguages = StructureStateUtils.getNewGlobalLanguages(TkShopOrganisation.values(), structureState);
            message = "Update global data for {}";
        } else {
            importLanguages = StructureStateUtils.getAllGlobalLanguages(TkShopOrganisation.values(), structureState);
            message = "Creating global data for {}";
        }
        final String languages = importLanguages.stream().map(ImportLanguage::getCode).collect(Collectors.joining(","));
        LOG.info(HAC_CONSOLE, message, languages);
        createGlobalData(importLanguages, update);
    }

    @Override
    protected void importGlobalData(String fileName, Collection<ImportLanguage> languages, boolean runAgain) {
        LOG.info(HAC_CONSOLE, "Started executing impex file:{}", fileName);
        super.importGlobalData(fileName, languages, runAgain);
        LOG.info(HAC_CONSOLE, "Finished executing impex file:{}", fileName);
    }

    protected void importShopSpecificData(final String fileName, final Collection<ImportLanguage> languages,
        final TkShopOrganisation shopOrganisation, final boolean runAgain) {
        importShopSpecificData(fileName, languages, shopOrganisation, runAgain, null);
    }

    protected void importShopSpecificData(final String fileName, final Collection<ImportLanguage> languages,
        final TkShopOrganisation shopOrganisation, final boolean runAgain, final ImpexImportUnitOption[] importOptions) {
        LOG.info(HAC_CONSOLE, "Started executing impex file:{}", fileName);
        importData(fileName, shopOrganisation, languages, runAgain, importOptions, null);
        LOG.info(HAC_CONSOLE, "Finished executing impex file:{}", fileName);
    }

    public Collection<TkEnvironment> getValidEnvironments() {
        return validEnvironments;
    }
}

package com.thyssenkrupp.b2b.eu.patches.releases.global;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class HemProdDataSetupPatch extends AbstractTkEuPatch {

    private static final String PATCH_ID = "hem_prod_data_setup";

    public HemProdDataSetupPatch() {
        super(PATCH_ID, PATCH_ID, TkRelease.Global, TkStructureState.V1, singletonList(TkEnvironment.PROD));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rhem_prod_data_setup_001-globalData.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
    }
}

package com.thyssenkrupp.b2b.eu.patches.structure;

import de.hybris.platform.patches.organisation.ImportLanguage;

public enum TkLanguage implements ImportLanguage {

    DE("de"), DE_DE("de_DE"), EN("en"), EN_US("en_US");

    private final String code;

    TkLanguage(final String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }

}

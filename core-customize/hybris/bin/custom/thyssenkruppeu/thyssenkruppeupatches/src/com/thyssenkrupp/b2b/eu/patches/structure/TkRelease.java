/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.patches.structure;

public enum TkRelease implements de.hybris.platform.patches.Release {

    R3("3"), R4("4"), R5("5"),R6("6"), Global("global");

    private final String releaseId;

    TkRelease(final String releaseId) {
        this.releaseId = releaseId;
    }

    @Override
    public String getReleaseId() {
        return this.releaseId;
    }

}

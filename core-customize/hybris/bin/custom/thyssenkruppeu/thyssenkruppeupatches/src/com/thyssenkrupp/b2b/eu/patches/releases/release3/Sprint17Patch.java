package com.thyssenkrupp.b2b.eu.patches.releases.release3;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class Sprint17Patch extends AbstractTkEuPatch {

    private static final String PATH_ID = "sprint_3_17_00";

    public Sprint17Patch() {
        super(PATH_ID, PATH_ID, TkRelease.R3, TkStructureState.V1, singletonList(TkEnvironment.ALL));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importShopSpecificData("rsprint_3_17_00_001-shopData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rsprint_3_17_00_001-cms-content.impex", languages, unit, updateLanguagesOnly);
    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {
        importGlobalData("rsprint_3_17_00_001-globalData.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_3_17_00_001-LocalizedMessages.impex", languages, updateLanguagesOnly);
        importGlobalData("rsprint_3_17_00_002-sapGlobalData.impex",languages,updateLanguagesOnly);
    }
}

package com.thyssenkrupp.b2b.eu.patches.structure;

import de.hybris.platform.patches.organisation.StructureState;

public enum TkStructureState implements StructureState {
    V1, V2, V3, LAST;

    @Override
    public boolean isAfter(final StructureState structureState) {
        if (this == structureState) {
            return false;
        }
        for (final StructureState sState : values()) {
            if (structureState.equals(sState)) {
                return true;
            }
            if (this.equals(sState)) {
                return false;
            }
        }
        return false;
    }
}

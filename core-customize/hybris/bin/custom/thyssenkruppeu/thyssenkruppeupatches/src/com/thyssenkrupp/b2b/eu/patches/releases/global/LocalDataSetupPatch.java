package com.thyssenkrupp.b2b.eu.patches.releases.global;

import com.thyssenkrupp.b2b.eu.patches.releases.AbstractTkEuPatch;
import com.thyssenkrupp.b2b.eu.patches.structure.TkEnvironment;
import com.thyssenkrupp.b2b.eu.patches.structure.TkRelease;
import com.thyssenkrupp.b2b.eu.patches.structure.TkShopOrganisation;
import com.thyssenkrupp.b2b.eu.patches.structure.TkStructureState;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

public class LocalDataSetupPatch extends AbstractTkEuPatch {

    private static final String PATCH_ID = "local_data_setup";

    public LocalDataSetupPatch() {
        super(PATCH_ID, PATCH_ID, TkRelease.Global, TkStructureState.V1, singletonList(TkEnvironment.DEV));
    }

    @Override
    public void createShopData(TkShopOrganisation unit, Collection<ImportLanguage> languages, boolean updateLanguagesOnly) {

        importShopSpecificData("rlocal_data_setup_001-shopSapConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rlocal_data_setup_001-shopConfiguration.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rlocal_data_setup_001-shopTestData.impex", languages, unit, updateLanguagesOnly);
        importShopSpecificData("rlocal_data_setup_001-shopEmailConfiguration.impex", languages, unit, updateLanguagesOnly);

    }

    @Override
    public void createGlobalData(Set<ImportLanguage> languages, boolean updateLanguagesOnly) {

        importGlobalData("rlocal_data_setup_001-globalRemovalData.impex", languages, updateLanguagesOnly);

    }
}

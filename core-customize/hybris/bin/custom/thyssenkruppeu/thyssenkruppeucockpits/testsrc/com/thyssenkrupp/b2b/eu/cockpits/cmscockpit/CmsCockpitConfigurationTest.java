package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.util.BaseCommerceBaseTest;
import de.hybris.platform.cockpit.model.meta.DefaultEditorFactory;
import de.hybris.platform.cockpit.model.meta.DefaultPropertyEditorDescriptor;
import de.hybris.platform.cockpit.model.meta.PropertyEditorDescriptor;
import de.hybris.platform.core.Registry;
import de.hybris.platform.spring.ctx.ScopeTenantIgnoreDocReader;

@IntegrationTest
public class CmsCockpitConfigurationTest extends BaseCommerceBaseTest {
    private static ApplicationContext APPLICATION_CONTEXT;

    @BeforeClass
    public static void testsSetup() {
        Registry.setCurrentTenantByID("junit");
        final GenericApplicationContext context = new GenericApplicationContext();
        context.setResourceLoader(new DefaultResourceLoader(Registry.class.getClassLoader()));
        context.setClassLoader(Registry.class.getClassLoader());
        context.getBeanFactory().setBeanClassLoader(Registry.class.getClassLoader());
        context.setParent(Registry.getGlobalApplicationContext());
        final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(context);
        xmlReader.setBeanClassLoader(Registry.class.getClassLoader());
        xmlReader.setDocumentReaderClass(ScopeTenantIgnoreDocReader.class);
        xmlReader.loadBeanDefinitions(getSpringConfigurationLocations());
        context.refresh();
        APPLICATION_CONTEXT = context;
    }

    @Test
    public void verifyClassesExist() {
        final DefaultEditorFactory factory = (DefaultEditorFactory) APPLICATION_CONTEXT.getBean("acceleratorEditorFactory");
        for (PropertyEditorDescriptor descriptor : factory.getAllEditorDescriptors()) {
            DefaultPropertyEditorDescriptor defaultDescriptor = (DefaultPropertyEditorDescriptor) descriptor;
            for (final String editorClazz : defaultDescriptor.getEditors().values()) {
                try {
                    Class.forName(editorClazz);
                } catch (ClassNotFoundException e) {
                    Assert.fail(String.format("Class %s used in thyssenkruppeucockpits/cmscockpit configuration does not exist", editorClazz));
                }
            }
        }
    }

    @After
    public void destroyApplicationContext() {
        if (APPLICATION_CONTEXT != null) {
            ((GenericApplicationContext) APPLICATION_CONTEXT).destroy();
            APPLICATION_CONTEXT = null;
        }
    }

    protected static String[] getSpringConfigurationLocations() {
        return new String[]
            { "cmscockpit/cmscockpit-spring-configs.xml", //
                "classpath:/thyssenkruppeucockpits/cmscockpit/spring/import.xml" };
    }
}

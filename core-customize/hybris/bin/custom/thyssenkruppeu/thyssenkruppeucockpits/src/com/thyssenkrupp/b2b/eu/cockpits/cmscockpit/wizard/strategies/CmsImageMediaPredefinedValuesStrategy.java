package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.wizard.strategies;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.cockpits.cockpit.wizard.strategies.DefaultImageMediaPredefinedValuesStrategy;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.core.model.media.MediaModel;

public class CmsImageMediaPredefinedValuesStrategy extends DefaultImageMediaPredefinedValuesStrategy {
    private CMSAdminSiteService cmsAdminSiteService;

    @Override
    public Map<String, Object> getPredefinedValues(final CreateContext paramCreateContext) {
        final Map<String, Object> ret = super.getPredefinedValues(paramCreateContext);
        final CatalogVersionModel activeCatalogVersion = getCmsAdminSiteService().getActiveCatalogVersion();
        if (activeCatalogVersion != null) {
            ret.put(MediaModel._TYPECODE + "." + MediaModel.CATALOGVERSION, activeCatalogVersion);
        }

        return ret;
    }

    protected CMSAdminSiteService getCmsAdminSiteService() {
        return cmsAdminSiteService;
    }

    @Required
    public void setCmsAdminSiteService(final CMSAdminSiteService cmsAdminSiteService) {
        this.cmsAdminSiteService = cmsAdminSiteService;
    }
}


package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.session.impl;

import de.hybris.platform.cmscockpit.events.impl.CmsPerspectiveInitEvent;
import de.hybris.platform.cockpit.session.UISessionUtils;

import java.util.Map;

/**
 * Default implementation of WCMSPerspective.
 */
public class DefaultWCMSPerspective extends DefaultCmsCockpitPerspective {
    @Override
    public void initialize(final Map<String, Object> params) {
        UISessionUtils.getCurrentSession().sendGlobalEvent(new CmsPerspectiveInitEvent(this));
        super.initialize(params);
    }
}


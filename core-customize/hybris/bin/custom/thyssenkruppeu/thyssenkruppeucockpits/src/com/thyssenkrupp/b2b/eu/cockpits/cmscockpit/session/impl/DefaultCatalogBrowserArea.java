package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.session.impl;

import de.hybris.platform.cmscockpit.session.impl.CatalogBrowserArea;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.BrowserModel;
import org.apache.log4j.Logger;

/**
 *
 *
 *
 */
public class DefaultCatalogBrowserArea extends CatalogBrowserArea {
    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(DefaultCatalogBrowserArea.class);

    @Override
    public void update() {
        super.update();
        if (getPerspective().getActiveItem() != null) {
            final BrowserModel browserModel = getFocusedBrowser();
            if (browserModel instanceof DefaultCmsPageBrowserModel) {
                final TypedObject associatedPageTypeObject = ((DefaultCmsPageBrowserModel) browserModel).getCurrentPageObject();
                ((DefaultCmsCockpitPerspective) getPerspective()).activateItemInEditorFallback(associatedPageTypeObject);
            }
        }
    }
}


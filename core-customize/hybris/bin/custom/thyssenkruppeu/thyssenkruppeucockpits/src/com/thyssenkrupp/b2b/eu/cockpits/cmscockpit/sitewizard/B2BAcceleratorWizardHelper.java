package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.sitewizard;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.util.Config;

/**
 * Creates new B2B site from given information. Intended for usage within accelerator cms site wizard.
 */
public class B2BAcceleratorWizardHelper extends AcceleratorWizardHelper {

    @Override
    protected SiteChannel getSiteChannel() {
        return SiteChannel.B2B;
    }

    @Override
    protected String getStorefrontContextRoot() {
        return Config.getString("b2bStorefrontContextRoot", "/acceleratorstorefront");
    }
}


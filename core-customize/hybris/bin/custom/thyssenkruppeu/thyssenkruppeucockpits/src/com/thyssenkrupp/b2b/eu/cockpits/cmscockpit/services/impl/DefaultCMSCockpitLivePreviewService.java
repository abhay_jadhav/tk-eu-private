package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.services.impl;

import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSPreviewService;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * This service class overrides the clonePreviewData method as the default implementation has issues in cloning by
 * not copying the complete set of attributes for PreviewDataModel
 */
public class DefaultCMSCockpitLivePreviewService extends DefaultCMSPreviewService {
    @Override
    public PreviewDataModel clonePreviewData(final PreviewDataModel original) {
        if (original == null) {
            return null;
        }

        //refreshing
        final ModelService modelService = getModelService();
        if (!modelService.isNew(original) && !modelService.isRemoved(original)) {
            modelService.refresh(original);
        }

        return modelService.clone(original);
    }
}


package com.thyssenkrupp.b2b.eu.cockpits.constants;

/**
 * Global class for all Thyssenkruppeucockpits constants. You can add global constants for your extension into this class.
 */
public final class ThyssenkruppeucockpitsConstants extends GeneratedThyssenkruppeucockpitsConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeucockpits";

    public static final String JASPER_REPORTS_MEDIA_FOLDER = "jasperreports";

    private ThyssenkruppeucockpitsConstants() {
        // empty
    }
}


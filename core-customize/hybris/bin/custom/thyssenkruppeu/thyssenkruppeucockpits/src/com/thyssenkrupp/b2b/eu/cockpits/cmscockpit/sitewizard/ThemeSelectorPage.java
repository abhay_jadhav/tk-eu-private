package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.sitewizard;

import org.apache.commons.lang.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

import de.hybris.platform.cockpit.wizards.generic.AbstractGenericItemPage;

public class ThemeSelectorPage extends AbstractGenericItemPage {
    @Override
    public Component createRepresentationItself() {
        final Component parent = getWizard().getFrameComponent().getFellow("contentFrame");
        String pageCmpURI = getComponentURI();
        if (StringUtils.isNotBlank(getWizard().getPageRoot()) && pageCmpURI.charAt(0) != '/') {
            pageCmpURI = getWizard().getPageRoot() + "/" + pageCmpURI; // NOPMD
        }
        return Executions.createComponents(pageCmpURI, parent, pageContent.getAttributes());
    }
}


package com.thyssenkrupp.b2b.eu.cockpits.cmscockpit.browser.filters;

import de.hybris.platform.cockpit.model.search.Query;
import de.hybris.platform.util.localization.Localization;

public class DesktopUiExperienceBrowserFilter extends AbstractUiExperienceFilter {
    private static final String DESKTOP_UI_EXPERIENCE_LABEL_KEY = "desktop.ui.experience.label.key";

    @Override
    public boolean exclude(final Object item) {

        return false;
    }

    @Override
    public void filterQuery(final Query query) {
        //empty because DESKTOP pages are displayed as default
    }

    @Override
    public String getLabel() {
        return Localization.getLocalizedString(DESKTOP_UI_EXPERIENCE_LABEL_KEY);
    }
}


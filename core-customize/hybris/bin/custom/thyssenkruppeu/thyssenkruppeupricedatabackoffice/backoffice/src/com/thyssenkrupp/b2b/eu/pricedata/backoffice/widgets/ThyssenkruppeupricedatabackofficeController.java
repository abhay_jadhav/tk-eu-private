package com.thyssenkrupp.b2b.eu.pricedata.backoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.thyssenkrupp.b2b.eu.pricedata.backoffice.services.ThyssenkruppeupricedatabackofficeService;

public class ThyssenkruppeupricedatabackofficeController extends DefaultWidgetController {
    private static final long  serialVersionUID = 1L;
    private              Label label;

    @WireVariable
    private transient ThyssenkruppeupricedatabackofficeService thyssenkruppeupricedatabackofficeService;

    @Override
    public void initialize(final Component comp) {
        super.initialize(comp);
        label.setValue(thyssenkruppeupricedatabackofficeService.getHello() + " ThyssenkruppeupricedatabackofficeController");
    }
}

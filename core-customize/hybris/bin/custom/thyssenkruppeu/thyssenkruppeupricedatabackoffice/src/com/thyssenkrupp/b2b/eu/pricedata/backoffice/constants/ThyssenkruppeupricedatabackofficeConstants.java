package com.thyssenkrupp.b2b.eu.pricedata.backoffice.constants;

public final class ThyssenkruppeupricedatabackofficeConstants extends GeneratedThyssenkruppeupricedatabackofficeConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeupricedatabackoffice";

    private ThyssenkruppeupricedatabackofficeConstants() {
    }
}

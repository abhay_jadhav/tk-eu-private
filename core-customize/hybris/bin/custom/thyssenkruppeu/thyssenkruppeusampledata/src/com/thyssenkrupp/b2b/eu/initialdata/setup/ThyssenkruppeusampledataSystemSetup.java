package com.thyssenkrupp.b2b.eu.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import com.thyssenkrupp.b2b.eu.initialdata.constants.ThyssenkruppeusampledataConstants;
import de.hybris.platform.commerceservices.setup.data.ImportData;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = ThyssenkruppeusampledataConstants.EXTENSIONNAME)
public class ThyssenkruppeusampledataSystemSetup extends AbstractSystemSetup {
    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(ThyssenkruppeusampledataSystemSetup.class);

    private static final String IMPORT_CORE_DATA              = "importCoreData";
    private static final String IMPORT_SAMPLE_DATA            = "importSampleData";
    private static final String ACTIVATE_SOLR_CRON_JOBS       = "activateSolrCronJobs";
    private static final String WEBSITE_LIST                  = "websiteList";
    private static final String TK_PLASTICS                   = "tkPlastics";
    private static final String TK_SCHULTE                    = "tkSchulte";
    private static final String IMPORT_SAP_CONFIGURATION_DATA = "importSapConfigurationData";
    private static final String IMPORT_COMMERCE_ORG_DATA      = "importCommerceOrgData";

    private CoreDataImportService   coreDataImportService;
    private SampleDataImportService sampleDataImportService;

    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();

        final String[] websites = new String[] { TK_PLASTICS, TK_SCHULTE };
        params.add(createWebsiteListSystemSetupParameter(websites));
        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
        params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));
        // Add more Parameters here as you require
        params.add(createBooleanSystemSetupParameter(IMPORT_SAP_CONFIGURATION_DATA, "Import SAP Configuration Data", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_COMMERCE_ORG_DATA, "Import Commerce Org Data", true));

        return params;
    }

    SystemSetupParameter createWebsiteListSystemSetupParameter(final String[] websites) {
        final SystemSetupParameter param = new SystemSetupParameter(WEBSITE_LIST);
        param.setLabel("Choose the websites");
        param.addValues(websites);
        param.setMultiSelect(true);
        return param;
    }

    /**
     * Implement this method to create initial objects. This method will be called by system creator during
     * initialization and system update. Be sure that this method can be called repeatedly.
     *
     * @param context the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        // Add Essential Data here as you require

    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization. <br>
     * Add import data for each site you have configured
     *
     * @param context the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        final List<String> websites = new ArrayList<>();
        websites.add(TK_SCHULTE);
        //websites.add(TK_PLASTICS);

        /*
         * Add import data for each site you have configured
         */

        //params.add(createWebsiteListSystemSetupParameter(websites));

        final List<ImportData> importDataList = new ArrayList<>();

        for (final String website : websites) {
            final ImportData importData = createImportData(website);
            importDataList.add(importData);
        }

        getCoreDataImportService().execute(this, context, importDataList);
        getSampleDataImportService().execute(this, context, importDataList);
        //getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

        final boolean importCommerceOrgData = getBooleanSystemSetupParameter(context, IMPORT_COMMERCE_ORG_DATA);

        if (importCommerceOrgData) {
            importImpexFile(context, "/thyssenkruppeusampledata/import/sampledata/commerceorg/commerce-org.impex", true);
            importImpexFile(context, "/thyssenkruppeusampledata/import/coredata/productCatalogs/tkPlasticsProductCatalog/plastics-dependent-catalog-relation.impex", false);
            importImpexFile(context, "/thyssenkruppeusampledata/import/coredata/productCatalogs/tkSchulteProductCatalog/schulte-dependent-catalog-relation.impex", false);
        }
        importImpexFile(context, "/thyssenkruppeusampledata/import/coredata/common/warehouse-sampledata.impex", true);
    }

    /**
     *
     */
    private ImportData createImportData(final String siteName) {
        final ImportData importData = new ImportData();
        importData.setProductCatalogName(siteName);
        importData.setContentCatalogNames(Arrays.asList(siteName));
        importData.setStoreNames(Arrays.asList(siteName));
        return importData;
    }

    public CoreDataImportService getCoreDataImportService() {
        return coreDataImportService;
    }

    @Required
    public void setCoreDataImportService(final CoreDataImportService coreDataImportService) {
        this.coreDataImportService = coreDataImportService;
    }

    public SampleDataImportService getSampleDataImportService() {
        return sampleDataImportService;
    }

    @Required
    public void setSampleDataImportService(final SampleDataImportService sampleDataImportService) {
        this.sampleDataImportService = sampleDataImportService;
    }
}


package com.thyssenkrupp.b2b.eu.initialdata.constants;

/**
 * Global class for all Thyssenkruppeusampledata constants.
 */
public final class ThyssenkruppeusampledataConstants extends GeneratedThyssenkruppeusampledataConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeusampledata";

    private ThyssenkruppeusampledataConstants() {
        //empty
    }
}


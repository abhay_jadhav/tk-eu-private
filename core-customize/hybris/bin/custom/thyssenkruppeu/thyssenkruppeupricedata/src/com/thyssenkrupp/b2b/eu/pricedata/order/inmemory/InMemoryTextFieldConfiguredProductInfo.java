package com.thyssenkrupp.b2b.eu.pricedata.order.inmemory;

import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloOnlyItem;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.servicelayer.internal.jalo.order.JaloOnlyItemHelper;
import org.apache.log4j.Logger;
import de.hybris.platform.core.PK;

import java.util.Date;

public class InMemoryTextFieldConfiguredProductInfo extends GeneratedInMemoryTextFieldConfiguredProductInfo implements JaloOnlyItem {

    private static final Logger             LOG                = Logger.getLogger(InMemoryTextFieldConfiguredProductInfo.class.getName());
    private static final String             ITEM_TYPE_PROPERTY = "itemtype";
    private              JaloOnlyItemHelper textFieldData;

    public InMemoryTextFieldConfiguredProductInfo() {
        //empty constructor needed for generic creation
    }

    @Override
    @ForceJALO(reason = "something else")
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException {
        final Class jaloClass = type.getJaloClass();
        LOG.debug("Creating InMemory InMemoryTextFieldConfiguredProductInfo");
        try {
            final InMemoryTextFieldConfiguredProductInfo newInstance = (InMemoryTextFieldConfiguredProductInfo) jaloClass.newInstance();
            newInstance.setTenant(type.getTenant());
            newInstance.textFieldData = new JaloOnlyItemHelper((PK) allAttributes.get(PK), newInstance, type, new Date(), null);
            return newInstance;
        } catch (ClassCastException | InstantiationException | IllegalAccessException e) {
            throw new JaloGenericCreationException("could not instantiate wizard class " + jaloClass + " of type " + type.getCode() + " : " + e, 0);
        }
    }

    @ForceJALO(reason = "something else")
    protected ItemAttributeMap getNonInitialAttributes(SessionContext ctx, ItemAttributeMap allAttributes) {
        ItemAttributeMap ret = new ItemAttributeMap(allAttributes);
        ret.remove(Item.TYPE);
        ret.remove(Item.PK);
        ret.remove(ITEM_TYPE_PROPERTY);
        return ret;
    }

    @ForceJALO(reason = "something else")
    public String getConfigurationLabel(SessionContext ctx) {
        return (String) this.textFieldData.getProperty(ctx, "configurationLabel");
    }

    @ForceJALO(reason = "something else")
    public void setConfigurationLabel(SessionContext ctx, String value) {
        this.textFieldData.setProperty(ctx, "configurationLabel", value);
    }

    @ForceJALO(reason = "something else")
    public String getConfigurationValue(SessionContext ctx) {
        return (String) this.textFieldData.getProperty(ctx, "configurationValue");
    }

    @ForceJALO(reason = "something else")
    public void setConfigurationValue(SessionContext ctx, String value) {
        this.textFieldData.setProperty(ctx, "configurationValue", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getProductInfoStatus(SessionContext ctx) {
        return (EnumerationValue) this.textFieldData.getProperty(ctx, "productInfoStatus");
    }

    @ForceJALO(reason = "something else")
    public void setProductInfoStatus(SessionContext ctx, EnumerationValue value) {
        this.textFieldData.setProperty(ctx, "productInfoStatus", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getConfiguratorType(SessionContext ctx) {
        return (EnumerationValue) this.textFieldData.getProperty(ctx, "configuratorType");
    }

    @ForceJALO(reason = "something else")
    public void setConfiguratorType(SessionContext ctx, EnumerationValue value) {
        this.textFieldData.setProperty(ctx, "configuratorType", value);
    }

    public final ComposedType provideComposedType() {
        return this.textFieldData.provideComposedType();
    }

    public final Date provideCreationTime() {
        return this.textFieldData.provideCreationTime();
    }

    public final Date provideModificationTime() {
        return this.textFieldData.provideModificationTime();
    }

    public final de.hybris.platform.core.PK providePK() {
        return this.textFieldData.providePK();
    }

    public void removeJaloOnly() throws ConsistencyCheckException {
        this.textFieldData.removeJaloOnly();
    }

    public Object doGetAttribute(SessionContext ctx, String attrQualifier) throws JaloSecurityException {
        return this.textFieldData.doGetAttribute(ctx, attrQualifier);
    }

    public void doSetAttribute(SessionContext ctx, String attrQualifier, Object value) throws JaloBusinessException {
        this.textFieldData.doSetAttribute(ctx, attrQualifier, value);
    }
}

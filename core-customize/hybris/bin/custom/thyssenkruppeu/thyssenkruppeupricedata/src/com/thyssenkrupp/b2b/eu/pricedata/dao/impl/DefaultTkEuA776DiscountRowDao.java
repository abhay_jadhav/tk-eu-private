package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA776Model;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA776Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA776DiscountRowDao extends DefaultGenericDao<TkEuConditionRowA776Model> implements TkEuDiscountRowDao<TkEuConditionRowA776Model> {

    private static String QUERY_BY_COND_KEY = "SELECT {A776:" + PK + "} from {" + _TYPECODE + " AS A776 JOIN DISCOUNT AS discount"
      + " ON {A776:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A776:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A776:" + STARTTIME + "} <=?" + STARTTIME + " AND {A776:" + ENDTIME + "} >=?" + ENDTIME + "";

    public DefaultTkEuA776DiscountRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA776Model> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowA776Model> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowA776Model> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }
}

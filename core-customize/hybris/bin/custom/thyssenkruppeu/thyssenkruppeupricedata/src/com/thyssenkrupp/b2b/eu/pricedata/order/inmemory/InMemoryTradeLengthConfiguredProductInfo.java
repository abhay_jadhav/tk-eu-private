package com.thyssenkrupp.b2b.eu.pricedata.order.inmemory;

import de.hybris.platform.core.PK;
import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.JaloOnlyItem;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.servicelayer.internal.jalo.order.JaloOnlyItemHelper;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class InMemoryTradeLengthConfiguredProductInfo extends GeneratedInMemoryTradeLengthConfiguredProductInfo implements JaloOnlyItem {

    private static final Logger             LOG = Logger.getLogger(InMemoryTradeLengthConfiguredProductInfo.class.getName());
    private              JaloOnlyItemHelper tradeLengthData;

    public InMemoryTradeLengthConfiguredProductInfo() {
        //empty constructor needed for generic creation
    }

    @Override
    @ForceJALO(reason = "something else")
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException {
        Set missing = new HashSet();
        if (!checkMandatoryAttribute("unit", allAttributes, missing)) {
            throw new JaloInvalidParameterException("missing parameters to create a InMemoryTradeLengthConfiguredProductInfo ( missing " + missing + ")", 0);
        } else {
            final Class jaloClass = type.getJaloClass();
            LOG.debug("Creating InMemory TradeLengthConfiguredProductInfo");
            try {
                final InMemoryTradeLengthConfiguredProductInfo newInstance = (InMemoryTradeLengthConfiguredProductInfo) jaloClass.newInstance();
                newInstance.setTenant(type.getTenant());
                newInstance.tradeLengthData = new JaloOnlyItemHelper((PK) allAttributes.get(PK), newInstance, type, new Date(), null);
                return newInstance;
            } catch (ClassCastException | InstantiationException | IllegalAccessException e) {
                throw new JaloGenericCreationException("could not instantiate wizard class " + jaloClass + " of type " + type.getCode() + " : " + e, 0);
            }
        }
    }

    @ForceJALO(reason = "something else")
    protected ItemAttributeMap getNonInitialAttributes(SessionContext ctx, ItemAttributeMap allAttributes) {
        ItemAttributeMap ret = new ItemAttributeMap(allAttributes);
        ret.remove(Item.TYPE);
        ret.remove(Item.PK);
        ret.remove("itemtype");
        return ret;
    }

    @ForceJALO(reason = "something else")
    public String getLabel(SessionContext ctx) {
        return (String) this.tradeLengthData.getProperty(ctx, "label");
    }

    @ForceJALO(reason = "something else")
    public void setLabel(SessionContext ctx, String label) {
        this.tradeLengthData.setProperty(ctx, "label", label);
    }

    @ForceJALO(reason = "something else")
    public String getDisplayValue(SessionContext ctx) {
        return (String) this.tradeLengthData.getProperty(ctx, "displayValue");
    }

    @ForceJALO(reason = "something else")
    public void setDisplayValue(SessionContext ctx, String displayValue) {
        this.tradeLengthData.setProperty(ctx, "displayValue", displayValue);
    }

    @ForceJALO(reason = "something else")
    public Double getLengthValue(SessionContext ctx) {
        return this.tradeLengthData.getPropertyDouble(ctx, "lengthValue", 0.0D);
    }

    @ForceJALO(reason = "something else")
    public void setLengthValue(SessionContext ctx, double value) {
        this.tradeLengthData.setProperty(ctx, "lengthValue", value);
    }

    @ForceJALO(reason = "something else")
    public Boolean isChecked(SessionContext ctx) {
        return this.tradeLengthData.getPropertyBoolean(ctx, "checked", false);
    }

    @ForceJALO(reason = "something else")
    public void setChecked(SessionContext ctx, boolean value) {
        this.tradeLengthData.setProperty(ctx, "checked", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getProductInfoStatus(SessionContext ctx) {
        return (EnumerationValue) this.tradeLengthData.getProperty(ctx, "productInfoStatus");
    }

    @ForceJALO(reason = "something else")
    public void setProductInfoStatus(SessionContext ctx, EnumerationValue ds) {
        this.tradeLengthData.setProperty(ctx, "productInfoStatus", ds);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getConfiguratorType(SessionContext ctx) {
        return (EnumerationValue) this.tradeLengthData.getProperty(ctx, "configuratorType");
    }

    @ForceJALO(reason = "something else")
    public void setConfiguratorType(SessionContext ctx, EnumerationValue ds) {
        this.tradeLengthData.setProperty(ctx, "configuratorType", ds);
    }

    public final ComposedType provideComposedType() {
        return this.tradeLengthData.provideComposedType();
    }

    public final Date provideCreationTime() {
        return this.tradeLengthData.provideCreationTime();
    }

    public final Date provideModificationTime() {
        return this.tradeLengthData.provideModificationTime();
    }

    public final de.hybris.platform.core.PK providePK() {
        return this.tradeLengthData.providePK();
    }

    public void removeJaloOnly() throws ConsistencyCheckException {
        this.tradeLengthData.removeJaloOnly();
    }

    public Object doGetAttribute(SessionContext ctx, String attrQualifier) throws JaloSecurityException {
        return this.tradeLengthData.doGetAttribute(ctx, attrQualifier);
    }

    public void doSetAttribute(SessionContext ctx, String attrQualifier, Object value) throws JaloBusinessException {
        this.tradeLengthData.doSetAttribute(ctx, attrQualifier, value);
    }
}

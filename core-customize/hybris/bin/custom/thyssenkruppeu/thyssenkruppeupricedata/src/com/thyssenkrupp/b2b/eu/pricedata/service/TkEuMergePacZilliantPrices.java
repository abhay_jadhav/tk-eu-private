package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;

public class TkEuMergePacZilliantPrices {

      public List<PriceInformation> mergePacZillPriceInfos(List<PriceInformation> pacPriceInfo, List<PriceInformation> zilPriceInfo) {
        List<PriceInformation> mergePriceInfo = new ArrayList<PriceInformation>();
        zilPriceInfo.sort((m1, m2) ->  ((Long)m1.getQualifiers().get(PriceRow.MINQTD)).intValue() -((Long)m2.getQualifiers().get(PriceRow.MINQTD)).intValue());
        List<PriceInformation> tempPacPriceInfo = new ArrayList<PriceInformation>();
        PriceInformation pi;
        int i,j,l,k=0; Boolean flag=false;
        for(i=0;i<pacPriceInfo.size();i++) {
            if(pacPriceInfo.get(i).getPrice().getValue()==0.0d) {  //NOSONAR
                //logic to add zilliant price rows
                for(j=0;j<zilPriceInfo.size();j++) {
                    if((long)zilPriceInfo.get(j).getQualifiers().get(PriceRow.MINQTD)>= (long)pacPriceInfo.get(i).getQualifiers().get(PriceRow.MINQTD)) {

                        if(j>=1 && !flag) {
                        pi=addMissingScale(zilPriceInfo.get(j), pacPriceInfo.get(i), zilPriceInfo.get(j-1));
                        addscale(tempPacPriceInfo,pi);
                        flag=true;
                        }

                        tempPacPriceInfo.add(new PriceInformation(zilPriceInfo.get(j).getQualifiers(),zilPriceInfo.get(j).getPrice()));

                        //for more zil scales
                        j++;
                        while(j<zilPriceInfo.size() && (i+1 < pacPriceInfo.size()) &&((long)zilPriceInfo.get(j).getQualifiers().get(PriceRow.MINQTD) < (long)pacPriceInfo.get(i+1).getQualifiers().get(PriceRow.MINQTD))) {
                            tempPacPriceInfo.add(zilPriceInfo.get(j));
                            j++;
                        }
                        break;
                        //should break the loop
                    }
                }
            } else {
                tempPacPriceInfo.add(pacPriceInfo.get(i));
            }
            flag=false;
        }
            tempPacPriceInfo.addAll(mergePacZilPricForZeroQuantity(tempPacPriceInfo, zilPriceInfo));
            tempPacPriceInfo.sort((m1, m2) ->  ((Long)m1.getQualifiers().get(PriceRow.MINQTD)).intValue() -((Long)m2.getQualifiers().get(PriceRow.MINQTD)).intValue());
            return tempPacPriceInfo;
    }

      private void addscale(List<PriceInformation> tempPacPriceInfo, PriceInformation pi) {
          if(pi.getPriceValue().getValue()!=0.0d) {    //NOSONAR
              tempPacPriceInfo.add(pi);
          }
      }

    private PriceInformation addMissingScale(PriceInformation zil, PriceInformation pac, PriceInformation zil2) {
          if((long)zil.getQualifiers().get(PriceRow.MINQTD) > (long)pac.getQualifiers().get(PriceRow.MINQTD)) {
              Map qualifiers = new HashMap();
              qualifiers.put(PriceRow.MINQTD, pac.getQualifiers().get(PriceRow.MINQTD));
              qualifiers.put(PriceRow.UNIT, zil2.getQualifiers().get(PriceRow.UNIT));
              return new PriceInformation(qualifiers, new PriceValue("EUR", zil2.getPriceValue().getValue(), false));
          }
          return new PriceInformation(new HashMap(), new PriceValue("EUR", 0.0d, false));
    }

    //merge zilliant scales if pac scales doesnot start with 0 quantity
      public List<PriceInformation> mergePacZilPricForZeroQuantity(List<PriceInformation> pacPriceInfo, List<PriceInformation> zilPriceInfo){
          int l=0;
          List<PriceInformation> tempPriceInfo = new ArrayList<PriceInformation>();
          //logic for upper zilliant rows
          if(!zilPriceInfo.isEmpty() && ((long)pacPriceInfo.get(0).getQualifiers().get(PriceRow.MINQTD)> 0)) {
          while((Long)pacPriceInfo.get(0).getQualifiers().get(PriceRow.MINQTD)> (Long)zilPriceInfo.get(l).getQualifiers().get(PriceRow.MINQTD)) {
              tempPriceInfo.add(l, zilPriceInfo.get(l));
              l++;
          }
          }
          return tempPriceInfo;
      }

      /*public List<PriceInformation> removeZeroScalesFromPac(List<PriceInformation> pacPriceInfo){
          pacPriceInfo.removeIf(e -> e.getPrice().getValue()== 0.0d);        //NOSONAR
          return pacPriceInfo;
      }*/
}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuPriceRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA800Model;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA800Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static com.thyssenkrupp.b2b.eu.zilliantpricing.model.AbstractZilItemModel.CATALOGVERSION;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.TK_EU_ZILLIANT_PRICE_CATALOG_ID;

public class DefaultTkEuPriceRowDao extends DefaultGenericDao<TkEuConditionRowA800Model> implements TkEuPriceRowDao<TkEuConditionRowA800Model> {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPriceRowDao.class);
    private static final String CATALOGSWITCH = "thyssenkrupp.Y2ZP.catalog.aware";
    private static String QUERY_BY_COND_KEY = "SELECT {A800:" + PK + "} from {" + _TYPECODE + " AS A800 JOIN DISCOUNT AS discount"
      + " ON {A800:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A800:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A800:" + STARTTIME + "} <=?" + STARTTIME + " AND {A800:" + ENDTIME + "} >=?" + ENDTIME + "";
    private static String QUERY_BY_COND_KEY_AND_CATALOGVERSION = "SELECT {A800:" + PK + "} from {" + _TYPECODE
            + " AS A800 JOIN DISCOUNT AS discount" + " ON {A800:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A800:"
            + CONDKEY + "} =?" + CONDKEY + "" + " AND {discount:code} =?" + DISCOUNT + " AND " + "{A800:" + STARTTIME
            + "} <=?" + STARTTIME + " AND {A800:" + ENDTIME + "} >=?" + ENDTIME + " " + "AND {" + CATALOGVERSION + "} = ?catalogVersion";
    private TkEuCatalogVersionService catalogVersionService;

    public DefaultTkEuPriceRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA800Model> findPriceRowByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<TkEuConditionRowA800Model> result;
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        if(Boolean.valueOf(Config.getBoolean(CATALOGSWITCH, true))) {
        params.put(CATALOGVERSION, getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID));
        result = getFlexibleSearchService().search(QUERY_BY_COND_KEY_AND_CATALOGVERSION, params);
        }else {
            result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        }
        LOG.debug("Queried table \'{}\' for params: condKey=\'{}\', discount=\'{}\' with result count: {}.", _TYPECODE, condKey, sapConditionId, result.getCount());
        return result.getResult();
    }

    @Override
    public TkEuConditionRowA800Model findPriceRowByCondKey(String condKey, String sapConditionId, Long quantityInScaleUnit) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<TkEuConditionRowA800Model> result;
        params.put(CONDKEY, condKey);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        if(Boolean.valueOf(Config.getBoolean(CATALOGSWITCH, true))) {
            params.put(CATALOGVERSION, getCatalogVersionService().getActiveCatalogVersionForCatalogId(TK_EU_ZILLIANT_PRICE_CATALOG_ID));
            result = getFlexibleSearchService().search(QUERY_BY_COND_KEY_AND_CATALOGVERSION, params);
            }else {
                result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
            }
        LOG.debug("Queried table \'{}\' for params: condKey=\'{}\', discount=\'{}\', quantity=\'{}\' with result: {}.", _TYPECODE, condKey, sapConditionId, quantityInScaleUnit, result.getResult());
        return findMatchingPriceRowCondA800ForQty(result, quantityInScaleUnit);
    }

    private TkEuConditionRowA800Model findMatchingPriceRowCondA800ForQty(SearchResult<TkEuConditionRowA800Model> result, Long quantity) {
        final TreeMap<Long, TkEuConditionRowA800Model> priceRowCondA800Map = new TreeMap<>();
        for (TkEuConditionRowA800Model priceRowCondA800Model : result.getResult()) {
            priceRowCondA800Map.put(priceRowCondA800Model.getMinqtd(), priceRowCondA800Model);
        }
        final Entry<Long, TkEuConditionRowA800Model> matchingPriceRowCondA800 = priceRowCondA800Map.floorEntry(quantity);
        return matchingPriceRowCondA800 != null ? matchingPriceRowCondA800.getValue() : null;
    }

    protected TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindMinimumOrderSurchargeStrategy;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.DiscountValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.MINIMUM_ORDER_SURCHARGE;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.convertToNegativeValue;
import static de.hybris.platform.util.DiscountValue.createAbsolute;
import static java.util.Optional.empty;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

public class DefaultTkEuFindMinimumOrderSurchargeStrategy implements TkEuFindMinimumOrderSurchargeStrategy {

    public static final String MINIMUM_ORDER_SURCHARGE_COND_KEY = "minimum-order-value-surcharge";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindMinimumOrderSurchargeStrategy.class);

    private AssistedServiceService     assistedServiceService;
    private TkEuDefaultConditionRowDao defaultConditionRowDao;
    private BaseStoreService           baseStoreService;

    @Override
    public Optional<DiscountValue> getMinimumOrderValueSurcharge(AbstractOrderModel orderModel) {

        List<AbstractOrderEntryModel> entries = emptyIfNull(orderModel.getEntries());
        double minimumSurcharge = findMinimumOrderSurchargeFee(entries, findOrderEntriesSubTotal(entries));
        if (minimumSurcharge > 0) {
            return Optional.of(createAbsolute(MINIMUM_ORDER_SURCHARGE, convertToNegativeValue(minimumSurcharge), orderModel.getCurrency().getIsocode()));
        }
        return empty();
    }

    protected double findMinimumOrderSurchargeFee(@NotNull List<AbstractOrderEntryModel> entries, Double orderEntriesSubTotal) {

        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        if (baseStore != null) {
            final Double minimumOrderValueThreshold = baseStore.getMinimumOrderValueThreshold();
            if (minimumOrderValueThreshold != null && orderEntriesSubTotal < minimumOrderValueThreshold && !orderEntriesSubTotal.equals(minimumOrderValueThreshold)) {
                return getMinimumSurcharge(entries);
            }
        }
        return 0;
    }

    protected double getMinimumSurcharge(@NotNull List<AbstractOrderEntryModel> entries) {

        AbstractOrderEntryModel abstractOrderEntryModel = entries.get(0);
        if (abstractOrderEntryModel != null) {
            List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao().findPriceRowByCondKey(MINIMUM_ORDER_SURCHARGE_COND_KEY, abstractOrderEntryModel.getProduct().getCatalogVersion());
            Optional<PriceRowModel> priceRowModel = priceRowByCondKey.stream().findFirst();
            if (priceRowModel.isPresent()) {
                LOG.debug("Minimum order surcharge cost : " + priceRowModel.get().getPrice());
                return priceRowModel.get().getPrice();
            }
        }
        return 0;
    }

    protected double findOrderEntriesSubTotal(@NotNull List<AbstractOrderEntryModel> entries) {
        Double entriesSubTotal = Double.valueOf(0);
        for (final AbstractOrderEntryModel entry : entries) {

            entriesSubTotal = Double.valueOf(entriesSubTotal + getEntrySubTotal(entry));
        }
        return entriesSubTotal;
    }

    protected double getEntrySubTotal(@NotNull AbstractOrderEntryModel entry) {

        return Double.valueOf(entry.getBasePrice() * entry.getQuantity().doubleValue());
    }

    public AssistedServiceService getAssistedServiceService() {
        return assistedServiceService;
    }

    public void setAssistedServiceService(AssistedServiceService assistedServiceService) {
        this.assistedServiceService = assistedServiceService;
    }

    public TkEuDefaultConditionRowDao getDefaultConditionRowDao() {
        return defaultConditionRowDao;
    }

    public void setDefaultConditionRowDao(TkEuDefaultConditionRowDao defaultConditionRowDao) {
        this.defaultConditionRowDao = defaultConditionRowDao;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}

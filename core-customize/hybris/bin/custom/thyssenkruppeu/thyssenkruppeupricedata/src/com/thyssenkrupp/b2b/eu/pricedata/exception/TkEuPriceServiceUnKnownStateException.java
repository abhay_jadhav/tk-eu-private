package com.thyssenkrupp.b2b.eu.pricedata.exception;

public class TkEuPriceServiceUnKnownStateException extends RuntimeException {

    private static final long serialVersionUID = -3570639567372771107L;

    public TkEuPriceServiceUnKnownStateException() {
        super();
    }

    public TkEuPriceServiceUnKnownStateException(String message) {
        super(message);
    }

    public TkEuPriceServiceUnKnownStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public TkEuPriceServiceUnKnownStateException(Throwable cause) {
        super(cause);
    }

    protected TkEuPriceServiceUnKnownStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

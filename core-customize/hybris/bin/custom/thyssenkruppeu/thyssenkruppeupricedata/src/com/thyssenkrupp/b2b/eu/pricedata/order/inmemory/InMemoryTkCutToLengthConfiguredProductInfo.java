package com.thyssenkrupp.b2b.eu.pricedata.order.inmemory;

import de.hybris.platform.core.PK;
import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloOnlyItem;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.servicelayer.internal.jalo.order.JaloOnlyItemHelper;
import org.apache.log4j.Logger;

import java.util.Date;

public class InMemoryTkCutToLengthConfiguredProductInfo extends GeneratedInMemoryTkCutToLengthConfiguredProductInfo implements JaloOnlyItem {

    private static final Logger             LOG                = Logger.getLogger(InMemoryTkCutToLengthConfiguredProductInfo.class.getName());
    private static final String             ITEM_TYPE_PROPERTY = "itemtype";
    private              JaloOnlyItemHelper cutToLengthData;

    public InMemoryTkCutToLengthConfiguredProductInfo() {
        //empty constructor needed for generic creation
    }

    @Override
    @ForceJALO(reason = "something else")
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException {
        final Class jaloClass = type.getJaloClass();
        LOG.debug("Creating InMemory InMemoryTkCutToLengthConfiguredProductInfo");
        try {
            final InMemoryTkCutToLengthConfiguredProductInfo newInstance = (InMemoryTkCutToLengthConfiguredProductInfo) jaloClass.newInstance();
            newInstance.setTenant(type.getTenant());
            newInstance.cutToLengthData = new JaloOnlyItemHelper((PK) allAttributes.get(PK), newInstance, type, new Date(), null);
            return newInstance;
        } catch (ClassCastException | InstantiationException | IllegalAccessException e) {
            throw new JaloGenericCreationException("could not instantiate wizard class " + jaloClass + " of type " + type.getCode() + " : " + e, 0);
        }
    }

    @ForceJALO(reason = "something else")
    protected ItemAttributeMap getNonInitialAttributes(SessionContext ctx, ItemAttributeMap allAttributes) {
        ItemAttributeMap ret = new ItemAttributeMap(allAttributes);
        ret.remove(TYPE);
        ret.remove(Item.PK);
        ret.remove(ITEM_TYPE_PROPERTY);
        return ret;
    }

    @ForceJALO(reason = "something else")
    public Boolean isChecked(SessionContext ctx) {
        return this.cutToLengthData.getPropertyBoolean(ctx, "checked", false);
    }

    @ForceJALO(reason = "something else")
    public void setChecked(SessionContext ctx, Boolean value) {
        this.cutToLengthData.setProperty(ctx, "checked", value);
    }

    @ForceJALO(reason = "something else")
    public String getLabel(SessionContext ctx) {
        return (String) this.cutToLengthData.getProperty(ctx, "label");
    }

    @ForceJALO(reason = "something else")
    public void setLabel(SessionContext ctx, String value) {
        this.cutToLengthData.setProperty(ctx, "label", value);
    }

    @ForceJALO(reason = "something else")
    public String getSapExportValue(SessionContext ctx) {
        return (String) this.cutToLengthData.getProperty(ctx, "sapExportValue");
    }

    @ForceJALO(reason = "something else")
    public void setSapExportValue(SessionContext ctx, String value) {
        this.cutToLengthData.setProperty(ctx, "sapExportValue", value);
    }

    @ForceJALO(reason = "something else")
    public String getTolerance(SessionContext ctx) {
        return (String) this.cutToLengthData.getProperty(ctx, "tolerance");
    }

    @ForceJALO(reason = "something else")
    public void setTolerance(SessionContext ctx, String value) {
        this.cutToLengthData.setProperty(ctx, "tolerance", value);
    }

    @ForceJALO(reason = "something else")
    public String getToleranceValue(SessionContext ctx) {
        return (String) this.cutToLengthData.getProperty(ctx, "toleranceValue");
    }

    @ForceJALO(reason = "something else")
    public void setToleranceValue(SessionContext ctx, String value) {
        this.cutToLengthData.setProperty(ctx, "toleranceValue", value);
    }

    @ForceJALO(reason = "something else")
    public Unit getUnit(SessionContext ctx) {
        return (Unit) this.cutToLengthData.getProperty(ctx, "unit");
    }

    @ForceJALO(reason = "something else")
    public void setUnit(SessionContext ctx, Unit value) {
        this.cutToLengthData.setProperty(ctx, "unit", value);
    }

    @ForceJALO(reason = "something else")
    public Double getValue(SessionContext ctx) {
        return this.cutToLengthData.getPropertyDouble(ctx, "value", 0.0D);
    }

    @ForceJALO(reason = "something else")
    public void setValue(SessionContext ctx, Double value) {
        this.cutToLengthData.setProperty(ctx, "value", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getProductInfoStatus(SessionContext ctx) {
        return (EnumerationValue) this.cutToLengthData.getProperty(ctx, "productInfoStatus");
    }

    @ForceJALO(reason = "something else")
    public void setProductInfoStatus(SessionContext ctx, EnumerationValue value) {
        this.cutToLengthData.setProperty(ctx, "productInfoStatus", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getConfiguratorType(SessionContext ctx) {
        return (EnumerationValue) this.cutToLengthData.getProperty(ctx, "configuratorType");
    }

    @ForceJALO(reason = "something else")
    public void setConfiguratorType(SessionContext ctx, EnumerationValue value) {
        this.cutToLengthData.setProperty(ctx, "configuratorType", value);
    }

    public final ComposedType provideComposedType() {
        return this.cutToLengthData.provideComposedType();
    }

    public final Date provideCreationTime() {
        return this.cutToLengthData.provideCreationTime();
    }

    public final Date provideModificationTime() {
        return this.cutToLengthData.provideModificationTime();
    }

    public final de.hybris.platform.core.PK providePK() {
        return this.cutToLengthData.providePK();
    }

    public void removeJaloOnly() throws ConsistencyCheckException {
        this.cutToLengthData.removeJaloOnly();
    }

    public Object doGetAttribute(SessionContext ctx, String attrQualifier) throws JaloSecurityException {
        return this.cutToLengthData.doGetAttribute(ctx, attrQualifier);
    }

    public void doSetAttribute(SessionContext ctx, String attrQualifier, Object value) throws JaloBusinessException {
        this.cutToLengthData.doSetAttribute(ctx, attrQualifier, value);
    }
}

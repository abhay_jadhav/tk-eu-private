/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.CERTIFICATE_COSTS;
import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.YWZF;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.convertToNegativeValue;
import static de.hybris.platform.util.DiscountValue.createAbsolute;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.impl.DefaultTkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindCertificatePriceStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.DiscountValue;

public class DefaultTkEuFindCertificatePriceStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy implements TkEuFindCertificatePriceStrategy {

    public static final String CERTIFICATE_COND_KEY_PREFIX = "certificate";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindCertificatePriceStrategy.class);

    private TkEuDefaultConditionRowDao defaultConditionRowDao;
    private DefaultTkEuB2bCategoryDao defaultTkEuB2bCategoryDao;
    private CatalogVersionService      catalogVersionService;
    private BaseStoreService           baseStoreService;

    @Override
    public DiscountValue getCertificatePrice(TkEuPriceParameter priceParameter) {
        List<Pair<GenericDao, List<String>>> conditionKeys = getCertificateConditionKeys(priceParameter);
        Optional<DiscountRowModel> discountRowModel = findDiscountRowForConditionKeys(conditionKeys, YWZF);
        if (discountRowModel.isPresent()) {
            LOG.debug("Certificate Price for the variant " + priceParameter.getVariantId() + " = " + discountRowModel.get().getValue());
            return createAbsolute(CERTIFICATE_COSTS, discountRowModel.get().getValue(), priceParameter.getCurrencyCode());
        }
        return findDefaultCertificatePrice(priceParameter);
    }

    protected DiscountValue findDefaultCertificatePrice(TkEuPriceParameter priceParameter) {
        String variantId = priceParameter.getVariantId();
        CatalogVersionModel productCatalogVersion = getDefaultTkEuB2bCategoryDao().fetchProductCatalogBySalesOrganization(getBaseStoreService().getCurrentBaseStore().getSAPConfiguration().getSapcommon_salesOrganization());
        CatalogVersionModel activeProductCatalogVersion = productCatalogVersion.getCatalog().getActiveCatalogVersion();
        List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao().findPriceRowByCondKey(String.join("-", CERTIFICATE_COND_KEY_PREFIX, variantId), activeProductCatalogVersion);
        Optional<PriceRowModel> priceRowModel = priceRowByCondKey.stream().findFirst();
        if (priceRowModel.isPresent()) {
            LOG.warn("Default Certificate Price for the variant \"" + variantId + "\" is retrieved");
            return createAbsolute(CERTIFICATE_COSTS, convertToNegativeValue(priceRowModel.get().getPrice()), priceParameter.getCurrencyCode());
        } else {
            LOG.error("Certificate Cost is not mapped for the variant \"" + variantId + "\" hence returning zero");
            return createAbsolute(CERTIFICATE_COSTS, 0.0, priceParameter.getCurrencyCode());
        }
    }

    protected List<Pair<GenericDao, List<String>>> getCertificateConditionKeys(TkEuPriceParameter priceParameter) {
        List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories = getDefaultTkEuConditionKeyBuilderSupplier().getCertificateCostKeyBuilderSupplier().get();
        return tkEuPriceConditionKeyBuilderFactories.stream().map(tkEuPriceConditionKeyBuilderFactory -> {
            Pair<GenericDao, List<String>> conditionKeys = tkEuPriceConditionKeyBuilderFactory.build(priceParameter);
            LOG.debug("condition Keys for certificates : " + conditionKeys);
            return conditionKeys;
        }).collect(Collectors.toList());
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuDefaultConditionRowDao getDefaultConditionRowDao() {
        return defaultConditionRowDao;
    }

    @Required
    public void setDefaultConditionRowDao(TkEuDefaultConditionRowDao defaultConditionRowDao) {
        this.defaultConditionRowDao = defaultConditionRowDao;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public DefaultTkEuB2bCategoryDao getDefaultTkEuB2bCategoryDao() {
        return defaultTkEuB2bCategoryDao;
    }

    public void setDefaultTkEuB2bCategoryDao(DefaultTkEuB2bCategoryDao defaultTkEuB2bCategoryDao) {
        this.defaultTkEuB2bCategoryDao = defaultTkEuB2bCategoryDao;
    }
}

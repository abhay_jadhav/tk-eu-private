package com.thyssenkrupp.b2b.eu.pricedata.order.inmemory;

import de.hybris.platform.core.PK;
import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloOnlyItem;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.servicelayer.internal.jalo.order.JaloOnlyItemHelper;
import org.apache.log4j.Logger;

import java.util.Date;

public class InMemoryCostConfigurationProductInfo extends GeneratedInMemoryCostConfigurationProductInfo implements JaloOnlyItem {

    private static final Logger             LOG                = Logger.getLogger(InMemoryCostConfigurationProductInfo.class.getName());
    private static final String             ITEM_TYPE_PROPERTY = "itemtype";
    private              JaloOnlyItemHelper costConfigurationData;

    public InMemoryCostConfigurationProductInfo() {
        //empty constructor needed for generic creation
    }

    @Override
    @ForceJALO(reason = "something else")
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException {
        final Class jaloClass = type.getJaloClass();
        LOG.debug("Creating InMemory InMemoryCostConfigurationProductInfo");
        try {
            final InMemoryCostConfigurationProductInfo newInstance = (InMemoryCostConfigurationProductInfo) jaloClass.newInstance();
            newInstance.setTenant(type.getTenant());
            newInstance.costConfigurationData = new JaloOnlyItemHelper((PK) allAttributes.get(PK), newInstance, type, new Date(), null);
            return newInstance;
        } catch (ClassCastException | InstantiationException | IllegalAccessException e) {
            throw new JaloGenericCreationException("could not instantiate wizard class " + jaloClass + " of type " + type.getCode() + " : " + e, 0);
        }
    }

    @ForceJALO(reason = "something else")
    protected ItemAttributeMap getNonInitialAttributes(SessionContext ctx, ItemAttributeMap allAttributes) {
        ItemAttributeMap ret = new ItemAttributeMap(allAttributes);
        ret.remove(Item.PK);
        ret.remove(Item.TYPE);
        ret.remove(ITEM_TYPE_PROPERTY);
        return ret;
    }

    @ForceJALO(reason = "something else")
    public String getLabel(SessionContext ctx) {
        return (String) this.costConfigurationData.getProperty(ctx, "label");
    }

    @ForceJALO(reason = "something else")
    public void setLabel(SessionContext ctx, String value) {
        this.costConfigurationData.setProperty(ctx, "label", value);
    }

    @ForceJALO(reason = "something else")
    public Currency getCurrency(SessionContext ctx) {
        return (Currency) this.costConfigurationData.getProperty(ctx, "currency");
    }

    @ForceJALO(reason = "something else")
    public void setCurrency(SessionContext ctx, Currency value) {
        this.costConfigurationData.setProperty(ctx, "currency", value);
    }

    @ForceJALO(reason = "something else")
    public Double getValue(SessionContext ctx) {
        return this.costConfigurationData.getPropertyDouble(ctx, "value", 0.0D);
    }

    @ForceJALO(reason = "something else")
    public void setValue(SessionContext ctx, Double value) {
        this.costConfigurationData.setProperty(ctx, "value", value);
    }

    @ForceJALO(reason = "something else")
    public Long getQuantity(SessionContext ctx) {
        return this.costConfigurationData.getPropertyLong(ctx, "quantity", 0L);
    }

    @ForceJALO(reason = "something else")
    public void setQuantity(SessionContext ctx, Long value) {
        this.costConfigurationData.setProperty(ctx, "quantity", value);
    }

    @ForceJALO(reason = "something else")
    public Unit getUnit(SessionContext ctx) {
        return (Unit) this.costConfigurationData.getProperty(ctx, "unit");
    }

    @ForceJALO(reason = "something else")
    public void setUnit(SessionContext ctx, Unit value) {
        this.costConfigurationData.setProperty(ctx, "unit", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getProductInfoStatus(SessionContext ctx) {
        return (EnumerationValue) this.costConfigurationData.getProperty(ctx, "productInfoStatus");
    }

    @ForceJALO(reason = "something else")
    public void setProductInfoStatus(SessionContext ctx, EnumerationValue value) {
        this.costConfigurationData.setProperty(ctx, "productInfoStatus", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getConfiguratorType(SessionContext ctx) {
        return (EnumerationValue) this.costConfigurationData.getProperty(ctx, "configuratorType");
    }

    @ForceJALO(reason = "something else")
    public void setConfiguratorType(SessionContext ctx, EnumerationValue value) {
        this.costConfigurationData.setProperty(ctx, "configuratorType", value);
    }

    public final ComposedType provideComposedType() {
        return this.costConfigurationData.provideComposedType();
    }

    public final Date provideCreationTime() {
        return this.costConfigurationData.provideCreationTime();
    }

    public final Date provideModificationTime() {
        return this.costConfigurationData.provideModificationTime();
    }

    public final de.hybris.platform.core.PK providePK() {
        return this.costConfigurationData.providePK();
    }

    public void removeJaloOnly() throws ConsistencyCheckException {
        this.costConfigurationData.removeJaloOnly();
    }

    public Object doGetAttribute(SessionContext ctx, String attrQualifier) throws JaloSecurityException {
        return this.costConfigurationData.doGetAttribute(ctx, attrQualifier);
    }

    public void doSetAttribute(SessionContext ctx, String attrQualifier, Object value) throws JaloBusinessException {
        this.costConfigurationData.doSetAttribute(ctx, attrQualifier, value);
    }
}

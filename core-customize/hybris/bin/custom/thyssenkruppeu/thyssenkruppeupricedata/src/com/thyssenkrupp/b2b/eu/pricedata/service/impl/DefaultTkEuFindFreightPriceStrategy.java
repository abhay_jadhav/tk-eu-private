package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindFreightPriceStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.TRANSPORT_COSTS;
import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.YZZF;

public class DefaultTkEuFindFreightPriceStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy implements TkEuFindFreightPriceStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindFreightPriceStrategy.class);

    @Override
    public DiscountValue getFreightPrice(@NotNull TkEuPriceParameter priceParameter) {
        List<Pair<GenericDao, List<String>>> conditionKeys = getFreightCostConditionKeys(priceParameter);
        Optional<DiscountRowModel> discountRowModel = findDiscountRowForConditionKeys(conditionKeys, YZZF);
        if (discountRowModel.isPresent()) {
            LOG.debug("Freight (Transport) Cost for the order = " + discountRowModel.get().getValue());
            return DiscountValue.createAbsolute(TRANSPORT_COSTS, discountRowModel.get().getValue(), priceParameter.getCurrencyCode());
        }
        return DiscountValue.createAbsolute(TRANSPORT_COSTS, 0.0, priceParameter.getCurrencyCode());
    }

    protected List<Pair<GenericDao, List<String>>> getFreightCostConditionKeys(TkEuPriceParameter priceParameter) {
        List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories = getDefaultTkEuConditionKeyBuilderSupplier().getFreightCostKeyBuilderSupplier().get();
        return tkEuPriceConditionKeyBuilderFactories.stream().map(tkEuPriceConditionKeyBuilderFactory -> {
            Pair<GenericDao, List<String>> conditionKeys = tkEuPriceConditionKeyBuilderFactory.build(priceParameter);
            LOG.debug("condition Keys for Freight cost : " + conditionKeys);
            return conditionKeys;
        }).collect(Collectors.toList());
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderStrategy;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;
import java.util.List;
import java.util.Map;

public class AbstractTkEuPriceConditionKeyBuilderFactory implements TkEuPriceConditionKeyBuilderFactory {

    private Map<Integer, List<TkEuPriceConditionKeyBuilderComponents>> conditionKeyMap;
    private TkEuPriceConditionKeyBuilderStrategy                       tkEuPriceConditionKeyBuilderStrategy;
    private ConditionKeyTypes                                          conditionKeyType;
    private GenericDao                                                 conditionDao;

    @Override
    public Pair<GenericDao, List<String>> build(TkEuPriceParameter priceParameter) {
        List<String> conditionKeys = getTkEuPriceConditionKeyBuilderStrategy().apply(getConditionKeyMap(), priceParameter);
        return Pair.of(getConditionDao(), conditionKeys);
    }
    

    public Map<Integer, List<TkEuPriceConditionKeyBuilderComponents>> getConditionKeyMap() {
        return conditionKeyMap;
    }

    public TkEuPriceConditionKeyBuilderStrategy getTkEuPriceConditionKeyBuilderStrategy() {
        return tkEuPriceConditionKeyBuilderStrategy;
    }

    @Required
    public void setConditionKeyMap(Map<Integer, List<TkEuPriceConditionKeyBuilderComponents>> conditionKeyMap) {
        this.conditionKeyMap = conditionKeyMap;
    }

    @Required
    public void setTkEuPriceConditionKeyBuilderStrategy(TkEuPriceConditionKeyBuilderStrategy tkEuPriceConditionKeyBuilderStrategy) {
        this.tkEuPriceConditionKeyBuilderStrategy = tkEuPriceConditionKeyBuilderStrategy;
    }

    @Override
    public ConditionKeyTypes getConditionKeyType() {
        return conditionKeyType;
    }

    @Required
    public void setConditionKeyType(ConditionKeyTypes conditionKeyType) {
        this.conditionKeyType = conditionKeyType;
    }

    public GenericDao getConditionDao() {
        return conditionDao;
    }

    @Required
    public void setConditionDao(GenericDao conditionDao) {
        this.conditionDao = conditionDao;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;

import java.util.List;
import java.util.Optional;

public interface TkEuPriceService {

    PriceInformation calculateProcessingOptionPriceForCustomer(TkEuPriceParameter priceParameter);

    Optional<DiscountValue> getCertificatePriceForProduct(CertificateConfiguredProductInfoModel abstractOrderEntryProductInfoModel);

    Optional<DiscountValue> getSawingCostForOrderEntry(TkCutToLengthConfiguredProductInfoModel abstractOrderEntryProductInfoModel, ProductModel product, MaterialQuantityWrapper materialQuantityWrapper);

    Optional<PriceInformation> getProcessingOptionPriceForProduct(ProductModel product);

    List<DiscountValue> getDiscountValuesForOrderEntry(AbstractOrderEntryModel entryModel, UserModel currentUser, List<DiscountValue> discounts);

    List<DiscountValue> getDiscountValuesForOrder(AbstractOrderModel orderModel, UserModel currentUser, List<DiscountValue> discounts);

    Optional<DiscountValue> getFreightPriceForOrder(AbstractOrderModel orderModel);

    Optional<DiscountValue> getAsmServiceFee(AbstractOrderModel orderModel);
    
    PriceValue calculateProsBasePriceForOrderEntry(TkEuPriceParameter priceParameter, MaterialQuantityWrapper quantityWrapper);
}

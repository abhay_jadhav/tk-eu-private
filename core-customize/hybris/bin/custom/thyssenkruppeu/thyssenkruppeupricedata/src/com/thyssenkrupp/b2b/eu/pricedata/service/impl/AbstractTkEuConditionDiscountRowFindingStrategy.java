package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractTkEuConditionDiscountRowFindingStrategy {

    private DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier;

    public Optional<DiscountRowModel> findDiscountRowForConditionKeys(@NotNull List<Pair<GenericDao, List<String>>> conditionKeys, @NotNull ConditionKeyTypes conditionKeyTypes) {
        for (Pair<GenericDao, List<String>> genericDaoListPair : conditionKeys) {
            if (genericDaoListPair.getKey() instanceof TkEuDiscountRowDao) {
                TkEuDiscountRowDao tkEuDiscountRowDao = (TkEuDiscountRowDao) genericDaoListPair.getKey();
                for (String conditionKey : genericDaoListPair.getValue()) {
                    Optional<DiscountRowModel> discountRow = tkEuDiscountRowDao.findDiscountRowByCondKey(conditionKey, conditionKeyTypes.toString());
                    if (discountRow.isPresent()) {
                        return discountRow;
                    }
                }
            }
        }
        return Optional.empty();
    }

    public List<DiscountRowModel> findDiscountRowsForConditionKeys(@NotNull List<Pair<GenericDao, List<String>>> conditionKeys, @NotNull ConditionKeyTypes conditionKeyTypes) {
        for (Pair<GenericDao, List<String>> genericDaoListPair : conditionKeys) {
            if (genericDaoListPair.getKey() instanceof TkEuDiscountRowDao) {
                TkEuDiscountRowDao tkEuDiscountRowDao = (TkEuDiscountRowDao) genericDaoListPair.getKey();
                for (String conditionKey : genericDaoListPair.getValue()) {
                    List<DiscountRowModel> discountRows = tkEuDiscountRowDao.findDiscountRowsByCondKey(conditionKey, conditionKeyTypes.toString());
                    if (CollectionUtils.isNotEmpty(discountRows)) {
                        return discountRows;
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    public DefaultTkEuConditionKeyBuilderSupplier getDefaultTkEuConditionKeyBuilderSupplier() {
        return defaultTkEuConditionKeyBuilderSupplier;
    }

    @Required
    public void setDefaultTkEuConditionKeyBuilderSupplier(DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier) {
        this.defaultTkEuConditionKeyBuilderSupplier = defaultTkEuConditionKeyBuilderSupplier;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.pricedata.dao.impl.DefaultTkEuPackagingGroupDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.PackagingGroupConfigModel;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFetchPackagingGroupService;

import de.hybris.platform.product.ProductService;

public class DefaultTkEuFetchPackagingGroupServiceImpl implements TkEuFetchPackagingGroupService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFetchPackagingGroupServiceImpl.class);
    private DefaultTkEuPackagingGroupDao defaultTkEuPackagingGroupDao;
    private ProductService productService;

    @Override
    public Optional<String> getPackagingGroupFromMaterialGroup(String materialGroup) {

        LOG.info("INTO SERVICE with material Group {}", materialGroup);
        List<PackagingGroupConfigModel> packagingGroupList = getDefaultTkEuPackagingGroupDao()
                .findPackagingGroupByMaterialGroup(materialGroup);
        String packagingGroup = "";
        if (CollectionUtils.isNotEmpty(packagingGroupList)) {
            Optional<PackagingGroupConfigModel> packagingGroupConfig = packagingGroupList.stream().findFirst();
            if (packagingGroupConfig.isPresent()) {
                packagingGroup = packagingGroupConfig.get().getPackagingGroup();
            }
        }
        return Optional.ofNullable(packagingGroup);
        }

    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * @return the defaultTkEuPackagingGroupDao
     */
    public DefaultTkEuPackagingGroupDao getDefaultTkEuPackagingGroupDao() {
        return defaultTkEuPackagingGroupDao;
    }

    /**
     * @param defaultTkEuPackagingGroupDao
     *            the defaultTkEuPackagingGroupDao to set
     */
    public void setDefaultTkEuPackagingGroupDao(DefaultTkEuPackagingGroupDao defaultTkEuPackagingGroupDao) {
        this.defaultTkEuPackagingGroupDao = defaultTkEuPackagingGroupDao;
    }

}

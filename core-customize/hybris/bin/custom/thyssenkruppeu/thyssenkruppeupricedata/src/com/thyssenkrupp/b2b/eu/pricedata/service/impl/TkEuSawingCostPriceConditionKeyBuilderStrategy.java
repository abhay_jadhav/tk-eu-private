package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderStrategy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TkEuSawingCostPriceConditionKeyBuilderStrategy extends DefaultTkEuPriceConditionKeyBuilderStrategy implements TkEuPriceConditionKeyBuilderStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuSawingCostPriceConditionKeyBuilderStrategy.class);

    @Override
    protected String buildKeyForConditionComponent(final TkEuPriceConditionKeyBuilderComponents component, final TkEuPriceConditionKeyBuilderDto dto) {
        String key = "";
        if (TkEuPriceConditionKeyBuilderComponents.EMPTY.equals(component)) {
            LOG.debug("Found empty key component");
        } else if (component.isOptional()) {
            key = StringUtils.repeat(' ', component.getFixedLength());
        } else {
            key = evaluateKeyBuilderFunction(component, dto);
        }
        if (StringUtils.isNotEmpty(key) && component.getFixedLength() > key.length()) {
            LOG.debug("Condition key component \'{}\' for {} does not meet the required fixed length \'{}\'. Remaining Chars filling with spaces", key, component, component.getFixedLength());
            String spacesToFill = StringUtils.repeat(' ', component.getFixedLength() - key.length());
            key = key + spacesToFill;
        }
        return key == null ? "" : key;
    }
}

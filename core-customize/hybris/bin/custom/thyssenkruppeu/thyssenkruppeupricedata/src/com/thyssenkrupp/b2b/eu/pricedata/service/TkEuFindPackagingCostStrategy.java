package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.thyssenkrupp.b2b.eu.core.service.PackagingCostTypes;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

public interface TkEuFindPackagingCostStrategy {

    
    Double getPackagingCost(AbstractOrderEntryModel orderEntryModel, String currencyCode,
            TkEuPriceParameter priceParameter);

    List<Pair<GenericDao, List<String>>> getConditionKeysForPackagingCost(AbstractOrderEntryModel orderEntryModel,
            String currencyCode, TkEuPriceParameter priceParameter, PackagingCostTypes packagingCostTypes);
}

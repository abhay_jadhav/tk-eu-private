package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.List;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;

import de.hybris.platform.jalo.order.price.PriceInformation;

public interface TkEuPacPriceService {

    List<PriceInformation> getPACValuesForCustomer(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException;

    PriceInformation getPACPriceForQuantity(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException;

}

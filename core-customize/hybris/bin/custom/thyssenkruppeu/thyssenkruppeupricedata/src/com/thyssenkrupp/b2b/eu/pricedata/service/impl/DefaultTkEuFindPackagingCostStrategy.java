package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.convertToNegativeValue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.PackagingCostTypes;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.data.PackagingCostKeyFactors;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFetchPackagingGroupService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindPackagingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

public class DefaultTkEuFindPackagingCostStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy
        implements TkEuFindPackagingCostStrategy {

    public static final String PACKAGING_SHAPE_LONG_COND_KEY = "packaging-shape-long";
    public static final String PACKAGING_SHAPE_FLAT_COND_KEY = "packaging-shape-flat";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindPackagingCostStrategy.class);

    private TkEuDefaultConditionRowDao defaultConditionRowDao;

    private TkEuB2bProductService tkEuB2bProductService;

    private TkEuFetchPackagingGroupService tkEuFetchPackagingGroupService;
    private DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier;

    @Override
    public Double getPackagingCost(@NotNull AbstractOrderEntryModel orderEntryModel, String currencyCode,
            TkEuPriceParameter priceParameter) {
        return getDiscountValueForPackagingCost(orderEntryModel, currencyCode, priceParameter).get();
    }

    private Optional<Double> getDiscountValueForPackagingCost(AbstractOrderEntryModel orderEntryModel, String currencyCode,
            TkEuPriceParameter priceParameter) {
      Double discountValue = 0d;

        for (PackagingCostTypes type : PackagingCostTypes.values()) {
            LOG.debug("FOR TYPE = {}", type);
            List<Pair<GenericDao, List<String>>> conditionKeyList = getConditionKeysForPackagingCost(orderEntryModel,
                    currencyCode, priceParameter, type);
            if (!conditionKeyList.isEmpty()) {
                Optional<DiscountRowModel> discountRow = findDiscountRowForConditionKeys(conditionKeyList,
                        ConditionKeyTypes.YPPF);
                if (discountRow.isPresent()) {
                    discountValue = discountRow.get().getValue();
                    LOG.info("For Condition key {} we have discount values {}.",conditionKeyList.get(0).getValue(),discountValue);
                    return Optional.of(discountValue);
                }
            }else {
                LOG.info("No packagingGroup found so discount value is 0");
                return Optional.of(discountValue);
            }
        }
        LOG.info("Condition key not found, so discount value is 0.");
        return Optional.of(discountValue);
    }

    @Override
    public List<Pair<GenericDao, List<String>>> getConditionKeysForPackagingCost(
            @NotNull AbstractOrderEntryModel orderEntryModel, String currencyCode, TkEuPriceParameter priceParameter,
            PackagingCostTypes packagingCostType) {

        Optional<String> packagingGroup = getPackagingGroupFromMaterialGroup(orderEntryModel);
        if (!packagingGroup.isPresent()) {
            LOG.info("Packaging group not found in the PackagingGroupTable.");
            return Collections.emptyList();
        }
        List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories = getDefaultTkEuConditionKeyBuilderSupplier()
                .getPackagingCostKeyBuilderSupplier().get();
        PackagingCostKeyFactors packagingCostFactors = new PackagingCostKeyFactors();
        packagingCostFactors.setPackagingGroup(packagingGroup);
        packagingCostFactors.setPackagingCostType(packagingCostType);
        priceParameter.setPackagingCostFactors(packagingCostFactors);
        return tkEuPriceConditionKeyBuilderFactories.stream().map(tkEuPriceConditionKeyBuilderFactory -> {
            Pair<GenericDao, List<String>> conditionKeys = tkEuPriceConditionKeyBuilderFactory.build(priceParameter);
            if (null != conditionKeys) {
                LOG.info("condition Keys for Packaging cost : {} for packagingCostType= {}", conditionKeys.getValue(),
                        packagingCostType);
            }
            return conditionKeys;
        }).collect(Collectors.toList());
    }

    private Optional<String> getPackagingGroupFromMaterialGroup(@NotNull AbstractOrderEntryModel orderEntryModel) {
        Optional<String> packagingGroup = Optional.empty();
        try {
            String materialGroup = orderEntryModel.getProduct().getMaterialGroup();
            if (StringUtils.isNotEmpty(materialGroup)) {
                packagingGroup = getTkEuFetchPackagingGroupService().getPackagingGroupFromMaterialGroup(materialGroup);
                LOG.info("For ProductId {} with Material group= {}, PackagingGroup is {} ",orderEntryModel.getProduct().getCode(), materialGroup, packagingGroup);
            }
        } catch (Exception e) {
            LOG.error("Exception {} occur while fetching Pakaging Group from Material Group", e);
        }
        return packagingGroup;
    }

    protected double getDefaultLongMaterialPackagingCost(CatalogVersionModel catalogVersion) {
        List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao()
                .findPriceRowByCondKey(PACKAGING_SHAPE_LONG_COND_KEY, catalogVersion);
        Optional<PriceRowModel> priceRowModel = priceRowByCondKey.stream().findFirst();
        if (priceRowModel.isPresent()) {
            LOG.debug("Default Price For The Long Material = " + priceRowModel.get().getPrice());
            return convertToNegativeValue(priceRowModel.get().getPrice());
        }
        return 0;
    }

    protected double getDefaultFlatMaterialPackagingCost(CatalogVersionModel catalogVersion) {
        List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao()
                .findPriceRowByCondKey(PACKAGING_SHAPE_FLAT_COND_KEY, catalogVersion);
        Optional<PriceRowModel> priceRowModel = priceRowByCondKey.stream().findFirst();
        if (priceRowModel.isPresent()) {
            LOG.debug("Default Price For The Flat Material = " + priceRowModel.get().getPrice());
            return convertToNegativeValue(priceRowModel.get().getPrice());
        }
        return 0;
    }

    public TkEuB2bProductService getTkEuB2bProductService() {
        return tkEuB2bProductService;
    }

    @Required
    public void setTkEuB2bProductService(TkEuB2bProductService tkEuB2bProductService) {
        this.tkEuB2bProductService = tkEuB2bProductService;
    }

    public TkEuDefaultConditionRowDao getDefaultConditionRowDao() {
        return defaultConditionRowDao;
    }

    @Required
    public void setDefaultConditionRowDao(TkEuDefaultConditionRowDao defaultConditionRowDao) {
        this.defaultConditionRowDao = defaultConditionRowDao;
    }

    /**
     * @return the tkEuFetchPackagingGroupService
     */
    public TkEuFetchPackagingGroupService getTkEuFetchPackagingGroupService() {
        return tkEuFetchPackagingGroupService;
    }

    /**
     * @param tkEuFetchPackagingGroupService
     *            the tkEuFetchPackagingGroupService to set
     */
    public void setTkEuFetchPackagingGroupService(TkEuFetchPackagingGroupService tkEuFetchPackagingGroupService) {
        this.tkEuFetchPackagingGroupService = tkEuFetchPackagingGroupService;
    }

    /**
     * @return the defaultTkEuConditionKeyBuilderSupplier
     */
    public DefaultTkEuConditionKeyBuilderSupplier getDefaultTkEuConditionKeyBuilderSupplier() {
        return defaultTkEuConditionKeyBuilderSupplier;
    }

    /**
     * @param defaultTkEuConditionKeyBuilderSupplier
     *            the defaultTkEuConditionKeyBuilderSupplier to set
     */
    public void setDefaultTkEuConditionKeyBuilderSupplier(
            DefaultTkEuConditionKeyBuilderSupplier defaultTkEuConditionKeyBuilderSupplier) {
        this.defaultTkEuConditionKeyBuilderSupplier = defaultTkEuConditionKeyBuilderSupplier;
    }
}

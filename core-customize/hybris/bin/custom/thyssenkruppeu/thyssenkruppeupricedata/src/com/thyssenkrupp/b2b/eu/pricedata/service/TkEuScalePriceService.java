package com.thyssenkrupp.b2b.eu.pricedata.service;

import de.hybris.platform.europe1.model.DiscountRowModel;

import java.math.BigDecimal;
import java.util.List;

public interface TkEuScalePriceService {

    DiscountRowModel findScalePriceRowByConditionKeyAndQuantity(String concatenatedKey, Long quantity);

    DiscountRowModel findMatchingDiscountRowByQty(List<DiscountRowModel> discountRows, Long quantity);

    List<DiscountRowModel> findScalePriceRowsByConditionKey(String concatenatedKey);

    BigDecimal findScaleFactorFromDiscountRow(DiscountRowModel discountRowModel);
}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.Optional;

public interface TkEuFetchPackagingGroupService {
  
   Optional<String> getPackagingGroupFromMaterialGroup(String materialGroup);

}

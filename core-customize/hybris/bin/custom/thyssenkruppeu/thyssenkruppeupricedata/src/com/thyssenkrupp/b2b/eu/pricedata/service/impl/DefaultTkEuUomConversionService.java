package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.core.enums.WeightingType;
import com.thyssenkrupp.b2b.eu.core.service.MaterialShape;
import com.thyssenkrupp.b2b.eu.core.service.ProductLengthConfigurationParam;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuUomConversionDao;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuUomConversionException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkUomConversionService;
import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_WEIGHTED_TYPE_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_YKH_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.service.impl.TradeLengthUomConversionHelper.isValidTradeLengthSupportedUnit;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKTRADELENGTH;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKCCLENGTH;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

public class DefaultTkEuUomConversionService extends DefaultTkUomConversionService implements TkEuUomConversionService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuUomConversionService.class);

    private BaseStoreService                baseStoreService;
    private CommonI18NService               commonI18nService;
    private TkEuUomConversionDao            tkEuUomConversionDao;
    private TkEuConfiguratorSettingsService configuratorSettingsService;
    private TkEuRoundingService             tkEuRoundingService;
    private DefaultB2BCommerceUnitService   defaultB2BCommerceUnitService;
    private UserService                     userService;
    private TkEuB2bProductService           tkEuB2bProductService;
    private ProductService                  productService;

    @Override
    public UnitModel getHybrisUnitFromZilliantUnit(String zilliantUnit) {
        List<UnitModel> unitModels = tkEuUomConversionDao.fetchHybrisUnitByZilliantUnit(zilliantUnit);
        return CollectionUtils.isNotEmpty(unitModels) ? unitModels.get(0) : new UnitModel();
    }

    @Override
    public MaterialQuantityWrapper calculateQuantityInTargetUnit(MaterialQuantityWrapper sourceMaterialQty, UnitModel targetUnit, ProductModel product) {
        final Pair<Double, UnitModel> pair = calculateQuantityInTargetUnit(Pair.of(sourceMaterialQty.getQuantity(), sourceMaterialQty.getUnitModel()), targetUnit, product);
        return buildMaterialQuantity(pair.getKey(), pair.getValue());
    }

    @Override
    public BigDecimal applyCommercialWeight(String productCode, UnitModel baseUnit, BigDecimal calculatedWeight, List<TkUomConversionModel> productUomConversionModels) {
        validateParameterNotNull(productCode, "Product Code cannot be null");
        try {
            final ProductModel productModel = getProductService().getProductForCode(productCode);
            return applyCommercialWeight(productModel, baseUnit, calculatedWeight, productUomConversionModels);
        } catch (Exception e) {
            LOG.error("Exception in populating Product UOM: " + e.getMessage());
        }
        return ZERO;
    }

    @Override
    public BigDecimal calculatePriceInBaseUOM(ProductModel productModel, BigDecimal sourceFactor, UnitModel sourceUnit) throws TkEuPriceServiceException {
        List<TkUomConversionModel> conversionModels = getSupportedUomConversions(productModel);
        BigDecimal pricePerBaseUom = sourceFactor;
        boolean isUomConversionNotFound = true;
        if (CollectionUtils.isNotEmpty(conversionModels)) {
            for (TkUomConversionModel conversionModel : conversionModels) {
                if (ObjectUtils.equals(conversionModel.getSalesUnit(), sourceUnit)) {
                    isUomConversionNotFound = false;
                    pricePerBaseUom = calculatePriceInBaseUOM(productModel.getCode(), sourceFactor, conversionModel);
                }
            }
        }
        if (isUomConversionNotFound) {
            throw new TkEuUomConversionException(String.format("Can not find a matching conversion for the unit \"%s\" in product \" %s \"", (sourceUnit == null ? "null" : sourceUnit.getCode()), productModel));
        }
        return pricePerBaseUom;
    }

    private BigDecimal calculatePriceInBaseUOM(String productCode, BigDecimal calculatedZilliantValue, TkUomConversionModel conversionModel) {
        LOG.debug("UOM Conversion: " + conversionModel.getBaseUnitFactor() + conversionModel.getBaseUnit().getCode() + "  = " + conversionModel.getSalesUnitFactor() + conversionModel.getSalesUnit().getCode());
        BigDecimal divisor = valueOf(conversionModel.getBaseUnitFactor());
        BigDecimal conversionUnitPerBaseUnit = calculateFactor(conversionModel.getSalesUnitFactor(), divisor.doubleValue()).orElse(ONE);
        BigDecimal pricePerBaseUom = calculatedZilliantValue.multiply(conversionUnitPerBaseUnit);
        if (LOG.isDebugEnabled()) {
            LOG.debug("UOM conversion - Price Per Base UOM for product \'{}\': \'{} * {} = {}\'.", productCode, calculatedZilliantValue, conversionUnitPerBaseUnit, pricePerBaseUom);
        }
        return pricePerBaseUom;
    }

    @Override
    public List<UnitModel> getSupportedUnitsForBaseStore() {
        return getBaseStoreService().getCurrentBaseStore().getSalesUnits();
    }

    @Override
    public List<TkUomConversionModel> getSupportedUomConversions(ProductModel productModel) throws TkEuPriceServiceException {
        final List<UnitModel> baseStoreUnits = emptyIfNull(getSupportedUnitsForBaseStore());
        final UnitModel productBaseUnit = productModel.getBaseUnit();
        final List<TkUomConversionModel> productUomConversions = getUomConversionByProductCode(productModel.getCode());
        if (CollectionUtils.isEmpty(baseStoreUnits) || CollectionUtils.isEmpty(productUomConversions)) {
            throw new TkEuPriceServiceException(String.format("Please check the sales units for store \"%1s\" (or) product \"2%s\"", getBaseStoreService().getCurrentBaseStore().getUid(), productModel.getCode()));
        } else if (ObjectUtils.notEqual(productBaseUnit, productUomConversions.get(0).getBaseUnit())) {
            throw new TkEuPriceServiceException(String.format("Product \"%1s\" has a different UomConversion basedUnit and Material baseUnit \"%2s\" and product baseUnit \"%3s\" ", productModel.getCode(), productUomConversions.get(0).getBaseUnit().getCode(), productBaseUnit));
        }
        return addSupportedUomIfOneToOneMappingIsMissing(productModel, productUomConversions);
    }

    @Override
    public List<TkUomConversionModel> getStoreSupportedUomConversions(ProductModel productModel) throws TkEuPriceServiceException {
        final List<UnitModel> baseStoreUnits = emptyIfNull(getSupportedUnitsForBaseStore());
        final List<TkUomConversionModel> tkUomConversionModels = getSupportedUomConversions(productModel);
        return tkUomConversionModels.stream().filter(c -> baseStoreUnits.contains(c.getSalesUnit())).collect(Collectors.toList());
    }

    private List<TkUomConversionModel> addSupportedUomIfOneToOneMappingIsMissing(ProductModel productModel, List<TkUomConversionModel> productUomConversions) {
        final Optional<TkUomConversionModel> oneToOneUomUnit = productUomConversions.stream().filter(tkUomConversionModel -> tkUomConversionModel.getSalesUnit().equals(productModel.getBaseUnit())).findAny();
        final List<TkUomConversionModel> tkUomConversionModels = new ArrayList<>();
        tkUomConversionModels.addAll(productUomConversions);
        if (!oneToOneUomUnit.isPresent()) {
            final TkUomConversionModel tkUomConversionModel = getOneToOneUomConversionModel(productModel);
            tkUomConversionModels.add(tkUomConversionModel);
        }
        return tkUomConversionModels;
    }

    @Override
    public Optional<BigDecimal> calculateWeightPerBaseUnit(@NotNull TkUomConversionModel weightedUomConversion) { //NOSONAR
        checkArgument(isWeightedUomConversion(weightedUomConversion), "weightedUomConversion, should be a weighted UomConversion");
        Optional<BigDecimal> result = Optional.of(ONE);
        if (BooleanUtils.isFalse(hasSameSalesUnitAndBaseUnit(weightedUomConversion))) {
            result = calculateBaseUnitToSalesUnitFactor(weightedUomConversion.getSalesUnitFactor(), weightedUomConversion.getBaseUnitFactor()); //NOSONAR
        }

        return result;
    }

    @Override
    public Optional<BigDecimal> calculateWeightPerSalesUnit(@NotNull TkUomConversionModel firstWeightedUomConversion, TkUomConversionModel salesConversionModel) {
        validateParameterNotNullStandardMessage("salesConversionModel", salesConversionModel);

        BigDecimal weightFactorPerBaseUnit = calculateWeightPerBaseUnit(firstWeightedUomConversion).orElse(ONE);
        BigDecimal baseFactorPerSalesUnit = calculateFactor(salesConversionModel.getBaseUnitFactor(), salesConversionModel.getSalesUnitFactor()).orElse(ONE);
        BigDecimal weightPerSalesUnit = baseFactorPerSalesUnit.multiply(weightFactorPerBaseUnit);

        Optional<BigDecimal> result = Optional.of(ONE);
        if (!isWeightedUomConversion(salesConversionModel)) {
            List<TkUomConversionModel> conversionModels = getUomConversionByProductCode(salesConversionModel.getProductId());
            applyCommercialWeight(salesConversionModel.getProductId(), salesConversionModel.getBaseUnit(), weightPerSalesUnit, conversionModels);
            return Optional.of(weightPerSalesUnit);
        }
        LOG.debug("Calculated WeightPerSalesUnit for productModel:" + salesConversionModel.getProductId() + " Result:" + weightPerSalesUnit);
        return result;
    }

    @Override
    public BigDecimal calculateOrderTotalWeightInKg(@NotNull AbstractOrderModel abstractOrderModel) {
        List<AbstractOrderEntryModel> entries = emptyIfNull(abstractOrderModel.getEntries());
        return calculateOrderEntriesTotalWeightInKg(entries);
    }

    @Override
    public BigDecimal calculateOrderEntriesTotalWeightInKg(@NotNull List<AbstractOrderEntryModel> orderEntries) {
        Set<String> productIds = orderEntries.stream().map(abstractOrderEntryModel -> abstractOrderEntryModel.getProduct().getCode()).collect(Collectors.toSet());
        List<TkUomConversionModel> uomConversionByProducts = getTkEuUomConversionDao().findUomConversionByProducts(productIds);
        BigDecimal orderTotalWeight = ZERO;
        try {
            orderTotalWeight = findOrderTotalWeightByEntry(orderEntries, uomConversionByProducts);
            LOG.debug("Calculated Weight for order in KGM : {}.", orderTotalWeight);
        } catch (TkEuPriceServiceException ex) {
            LOG.error("Weight for the order can not be calculated with following reason ", ex);
        }
        return orderTotalWeight;
    }

    private BigDecimal findOrderTotalWeightByEntry(List<AbstractOrderEntryModel> entries, List<TkUomConversionModel> uomConversionByProducts) throws TkEuPriceServiceException {
        BigDecimal orderTotalWeightInKg = ZERO;
        for (AbstractOrderEntryModel abstractOrderEntryModel : entries) {
            final UnitModel orderEntrySalesUnit = abstractOrderEntryModel.getUnit();
            if (StringUtils.equals(SAP_WEIGHTED_TYPE_CODE, orderEntrySalesUnit.getUnitType())) {
                orderTotalWeightInKg = orderTotalWeightInKg.add(valueOf(abstractOrderEntryModel.getQuantity() * orderEntrySalesUnit.getConversion()));
            } else {
                orderTotalWeightInKg = orderTotalWeightInKg.add(findTotalWeightForOrderEntryInKg(uomConversionByProducts, abstractOrderEntryModel));
            }
        }
        return orderTotalWeightInKg;
    }

    @Override
    public BigDecimal findTotalWeightForOrderEntryInKg(AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException {
        List<TkUomConversionModel> uomConversionByProductCode = getTkEuUomConversionDao().findUomConversionByProductCode(abstractOrderEntryModel.getProduct().getCode());
        return findTotalWeightForOrderEntryInKg(emptyIfNull(uomConversionByProductCode), abstractOrderEntryModel);
    }

    protected BigDecimal findTotalWeightForOrderEntryInKg(List<TkUomConversionModel> uomConversionByProducts, AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException {

        UnitModel salesUnit = abstractOrderEntryModel.getUnit();
        if (StringUtils.equalsIgnoreCase(SAP_KGM_UNIT_CODE, salesUnit.getCode())) {
            return BigDecimal.valueOf(abstractOrderEntryModel.getQuantity());
        }

        Optional<ProductLengthConfigurationParam> productLengthConfigurationParam = getConfiguratorSettingsService().getProductLengthInfoForOrderEntry(abstractOrderEntryModel);

        MaterialQuantityWrapper materialQtyInTradeLength = buildMaterialQuantity(abstractOrderEntryModel.getQuantity(), salesUnit);
        if (productLengthConfigurationParam.isPresent()) {
            materialQtyInTradeLength = buildMaterialQuantityFromLength(materialQtyInTradeLength, abstractOrderEntryModel, productLengthConfigurationParam.get());
        }

        List<TkUomConversionModel> productUomConversionModels = uomConversionByProducts.stream().filter(uomConversionByProduct -> uomConversionByProduct.getProductId().equals(abstractOrderEntryModel.getProduct().getCode())).collect(Collectors.toList());
        Optional<TkUomConversionModel> firstWeightedUomConversion = getFirstWeightedUomConversion(productUomConversionModels);

        Supplier<TkEuUomConversionException> tkEuUomConversionExceptionSupplier = () -> new TkEuUomConversionException(String.format("Can not find a matching conversion for the product \" %s \" in Weight", abstractOrderEntryModel.getProduct().getCode()));
        TkUomConversionModel weightedUomConversionModel = firstWeightedUomConversion.orElseThrow(tkEuUomConversionExceptionSupplier);
        return getTotalWeightOfEntryInKg(abstractOrderEntryModel, productUomConversionModels, weightedUomConversionModel, materialQtyInTradeLength); // NOSONAR
    }

    private MaterialQuantityWrapper buildMaterialQuantityFromLength(@Nullable MaterialQuantityWrapper materialQtyInTradeLength, AbstractOrderEntryModel abstractOrderEntryModel, @NotNull ProductLengthConfigurationParam productLengthConfigurationParam) {
        final double lengthValue = (materialQtyInTradeLength != null ? materialQtyInTradeLength.getQuantity() : 1) * productLengthConfigurationParam.getLengthValue();
        if ((productLengthConfigurationParam.getConfigurationType() == TKTRADELENGTH || productLengthConfigurationParam.getConfigurationType() == TKCCLENGTH ) && isValidTradeLengthSupportedUnit(abstractOrderEntryModel.getUnit().getCode())) {
            return buildMaterialQuantity(lengthValue, productLengthConfigurationParam.getUnit());
        } else if (productLengthConfigurationParam.getConfigurationType() == CUTTOLENGTH_PRODINFO) {
            return buildMaterialQuantity(lengthValue, productLengthConfigurationParam.getUnit());
        }
        return materialQtyInTradeLength;
    }

    private BigDecimal getTotalWeightOfEntryInKg(AbstractOrderEntryModel abstractOrderEntryModel, @NotNull List<TkUomConversionModel> productUomConversionModels, TkUomConversionModel firstWeightedUomConversion, MaterialQuantityWrapper materialQtyInTradeLength) throws TkEuPriceServiceException {
        BigDecimal weightFactorPerBaseUnit = calculateWeightPerBaseUnit(firstWeightedUomConversion).orElse(ONE);
        final UnitModel orderEntrySalesUnit = materialQtyInTradeLength.getUnitModel();
        final Optional<TkUomConversionModel> selectedUomModel = productUomConversionModels.stream().filter(tkUomConversionModel -> tkUomConversionModel.getSalesUnit().equals(orderEntrySalesUnit)).findFirst();
        ProductModel productModel = abstractOrderEntryModel.getProduct();
        Supplier<TkEuPriceServiceException> priceServiceExceptionSupplier = () -> new TkEuPriceServiceException(String.format("UomConversion for unit \"%s\" has been removed for the product \"%s\"", orderEntrySalesUnit.getCode(), productModel.getCode()));
        TkUomConversionModel salesUomConversionModel = selectedUomModel.orElseThrow(priceServiceExceptionSupplier);

        BigDecimal baseFactorPerSalesUnit = calculateFactor(salesUomConversionModel.getBaseUnitFactor(), salesUomConversionModel.getSalesUnitFactor()).orElse(ONE);
        BigDecimal weightPerSalesUnit = baseFactorPerSalesUnit.multiply(weightFactorPerBaseUnit);
        BigDecimal salesUnitConversion = valueOf(firstWeightedUomConversion.getSalesUnit().getConversion());
        BigDecimal calculatedWeight = weightPerSalesUnit.multiply(valueOf(materialQtyInTradeLength.getQuantity())).multiply(salesUnitConversion);
        return StringUtils.equals(SAP_WEIGHTED_TYPE_CODE, orderEntrySalesUnit.getUnitType()) ? calculatedWeight : applyCommercialWeight(productModel, productModel.getBaseUnit(), calculatedWeight, getUomConversionByProductCode(productModel.getCode()));
    }

    @Override
    public BigDecimal applyCommercialWeight(ProductModel productModel, UnitModel baseUnit, BigDecimal calculatedWeight, @NotNull List<TkUomConversionModel> productUomConversionModels) {
        return findCommercialWeight(productModel, calculatedWeight, productUomConversionModels, baseUnit);
    }

    private BigDecimal findCommercialWeight(ProductModel productModel, BigDecimal calculatedWeight, @NotNull List<TkUomConversionModel> productUomConversionModels, UnitModel baseUnit) {
        BigDecimal commercialWeight = calculatedWeight;
        if (checkForCommercialWeight(baseUnit) && Optional.of(MaterialShape.LONG).equals(getTkEuB2bProductService().getProductShape(productModel))) {
            LOG.debug("In method findCommercialWeight[DefaultTkEuUomConversionService], material shape is "+getTkEuB2bProductService().getProductShape(productModel));
            commercialWeight = findCommercialWeight(calculatedWeight, productUomConversionModels, commercialWeight);
            LOG.debug("Calculated commercial weight is"+ commercialWeight.doubleValue());
        }
        LOG.debug("Commercial weight calculation check failed, Calculated commercial weight is"+ commercialWeight.doubleValue());
        return commercialWeight;
    }

    private boolean checkForCommercialWeight(UnitModel baseUnit) {
        if (getUserService().getCurrentUser() instanceof B2BCustomerModel) {
            B2BUnitModel childB2bUnit = getDefaultB2BCommerceUnitService().getParentUnit();
            if(!Objects.isNull(childB2bUnit)) {
            LOG.debug("In method checkForCommercialWeight[DefaultTkEuUomConversionService], b2b unit is "+ childB2bUnit.getUid()+" and weighting type is "+childB2bUnit.getWeightingType());
            if (SAP_KGM_UNIT_CODE.equalsIgnoreCase(baseUnit.getCode())) {
                LOG.debug("Comparing weighting type "+childB2bUnit.getWeightingType()+ " and "+ WeightingType.EMPTY);
                return childB2bUnit.getWeightingType() == WeightingType.EMPTY;
            }
            }
        }
        return false;
    }

    private BigDecimal findCommercialWeight(BigDecimal calculatedWeight, @NotNull List<TkUomConversionModel> productUomConversionModels, BigDecimal commercialWeight) {
        BigDecimal appliedCommercialWeight = commercialWeight;
        Optional<TkUomConversionModel> tkUomConversionModel = productUomConversionModels.stream().filter(uomModels -> SAP_YKH_UNIT_CODE.equalsIgnoreCase(uomModels.getSalesUnit().getCode()))
          .findFirst();
        if (tkUomConversionModel.isPresent()) {
            TkUomConversionModel conversionModel = tkUomConversionModel.get();
            BigDecimal commercialWeightFactor = calculateFactor(conversionModel.getSalesUnitFactor(), conversionModel.getBaseUnitFactor()).orElse(ONE);
            appliedCommercialWeight = calculatedWeight.multiply(commercialWeightFactor);
        } else {
            Optional<TkUomConversionModel> optionalUomConversionModel = productUomConversionModels.stream().findFirst();
            LOG.error("{} UomConversion missing for product {} and Commercial Weight can not be applied", SAP_YKH_UNIT_CODE, optionalUomConversionModel.isPresent() ? optionalUomConversionModel.get().getProductId() : "");
        }
        return appliedCommercialWeight;
    }

    @Nullable
    @Override
    public MaterialQuantityWrapper findTradeLengthUnitIfApplied(AbstractOrderEntryModel abstractOrderEntryModel) {
        Optional<ProductLengthConfigurationParam> productLengthConfigurationParam = getConfiguratorSettingsService().getProductLengthInfoForOrderEntry(abstractOrderEntryModel);
        if (productLengthConfigurationParam.isPresent()) {
            return buildMaterialQuantityFromLength(null, abstractOrderEntryModel, productLengthConfigurationParam.get());
        }
        return null;
    }

    private boolean hasSameSalesUnitAndBaseUnit(@NotNull TkUomConversionModel conversionModel) {
        return ObjectUtils.equals(conversionModel.getSalesUnit(), conversionModel.getBaseUnit());
    }

    private TkUomConversionModel getOneToOneUomConversionModel(ProductModel productModel) {
        final TkUomConversionModel tkUomConversionModel = new TkUomConversionModel();
        tkUomConversionModel.setBaseUnit(productModel.getBaseUnit());
        tkUomConversionModel.setSalesUnit(productModel.getBaseUnit());
        tkUomConversionModel.setBaseUnitFactor(1.0);
        tkUomConversionModel.setSalesUnitFactor(1.0);
        tkUomConversionModel.setProductId(productModel.getCode());
        return tkUomConversionModel;
    }

    private MaterialQuantityWrapper buildMaterialQuantity(double quantity, UnitModel unitModel) {
        return MaterialQuantityWrapperBuilder.builder().withQuantity(quantity).withUnit(unitModel).build();
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public TkEuRoundingService getTkEuRoundingService() {
        return tkEuRoundingService;
    }

    @Required
    public void setTkEuRoundingService(TkEuRoundingService tkEuRoundingService) {
        this.tkEuRoundingService = tkEuRoundingService;
    }

    public TkEuUomConversionDao getTkEuUomConversionDao() {
        return tkEuUomConversionDao;
    }

    @Required
    public void setTkEuUomConversionDao(TkEuUomConversionDao tkEuUomConversionDao) {
        this.tkEuUomConversionDao = tkEuUomConversionDao;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public TkEuB2bProductService getTkEuB2bProductService() {
        return tkEuB2bProductService;
    }

    @Required
    public void setTkEuB2bProductService(TkEuB2bProductService tkEuB2bProductService) {
        this.tkEuB2bProductService = tkEuB2bProductService;
    }

    public ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}

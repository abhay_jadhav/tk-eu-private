package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class DefaultTkEuRoundingService implements TkEuRoundingService {
    private static final Logger LOG                                      = LoggerFactory.getLogger(DefaultTkEuRoundingService.class);
    private static final String UNIT_KGM                                 = "KG";
    private static final int    KGM_THRESHOLD                            = 1000;
    private static final int    SCALE_BY_TWO                             = 2;
    private static final int    SCALE_BY_THREE                           = 3;
    private static final int    SCALE_BY_FOUR                            = 4;
    private static final int    PRICE_ROUNDING_EXCEEDING_THRESHOLD_SCALE = 0;

    @Override
    public BigDecimal roundPriceBasedOnPriceQuantity(TkEuPriceParameter priceParameter, BigDecimal priceInZilUom) {
        if (isExceedingThreshold(priceParameter)) {
            BigDecimal roundedPriceInZilUom = priceInZilUom.setScale(PRICE_ROUNDING_EXCEEDING_THRESHOLD_SCALE, BigDecimal.ROUND_UP);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will round discounted ZilPrice from \'{}\' to \'{}\' since price quantity is exceeding threshold of 1000 KGMs.", priceInZilUom, roundedPriceInZilUom);
            }
            return roundedPriceInZilUom;
        }
        BigDecimal roundedPriceInZilUom = priceInZilUom.setScale(SCALE_BY_TWO, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round discounted ZilPrice from \'{}\' to \'{}\'.", priceInZilUom, roundedPriceInZilUom);
        }
        return roundedPriceInZilUom;
    }

    @Override
    public BigDecimal roundPriceBasedOnPriceQuantityForPac(TkEuPriceParameter priceParameter, BigDecimal priceInZilUom) {
        BigDecimal roundedPriceInZilUom = priceInZilUom.setScale(SCALE_BY_TWO, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round discounted ZilPrice from \'{}\' to \'{}\'.", priceInZilUom, roundedPriceInZilUom);
        }
        return roundedPriceInZilUom;
    }

    @Override
    public BigDecimal roundZilPriceValue(@NotNull ZilProductModel zilProductModel) {
        Double priceValue = zilProductModel.getPriceValue();
        BigDecimal roundedPriceValue = BigDecimal.valueOf(priceValue).setScale(SCALE_BY_TWO, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round zilPriceValue from \'{}\' to \'{}\'.", priceValue, roundedPriceValue.doubleValue());
        }
        return roundedPriceValue;
    }

    @Override
    public BigDecimal roundZilTargetFactor(@NotNull ZilPriceModel zilPriceModel) {
        Double targetFactor = zilPriceModel.getTargetFactor();
        BigDecimal roundedTargetFactor = BigDecimal.valueOf(targetFactor).setScale(SCALE_BY_THREE, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round targetFactor from \'{}\' to \'{}\'.", targetFactor, roundedTargetFactor.doubleValue());
        }
        return roundedTargetFactor;
    }

    @Override
    public BigDecimal roundDiscountScaleFactor(@NotNull BigDecimal scaleFactor) {
        BigDecimal discountScaleFactor = scaleFactor;
        BigDecimal roundedScaleFactor = discountScaleFactor.setScale(SCALE_BY_THREE, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round discount ScaleFactor (A800) from \'{}\' to \'{}\'.", discountScaleFactor, roundedScaleFactor);
        }
        return roundedScaleFactor;
    }

    @Override
    public BigDecimal roundBasePrice(@NotNull BigDecimal basePrice) {
        BigDecimal price = basePrice;
        BigDecimal roundedPrice = price.setScale(SCALE_BY_TWO, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round basePrice from \'{}\' to \'{}\'.", price, roundedPrice);
        }
        return roundedPrice;
    }

    @Override
    public BigDecimal roundScaledBasePrice(@NotNull BigDecimal scaledPrice) {
        BigDecimal price = scaledPrice;
        BigDecimal roundedPrice = price.setScale(SCALE_BY_FOUR, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round value from \'{}\' to \'{}\'.", price, roundedPrice);
        }
        return roundedPrice;
    }

    @Override
    public BigDecimal scaleByThree(@NotNull BigDecimal value) {
        BigDecimal price = value;
        BigDecimal roundedPrice = price.setScale(SCALE_BY_THREE, RoundingMode.FLOOR);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will scale the Price from \'{}\' to \'{}\'.", price, roundedPrice);
        }
        return roundedPrice;
    }

    private boolean isExceedingThreshold(TkEuPriceParameter priceParameter) {
        ZilProductModel zilProduct = priceParameter.getZilliantPriceParameter().getZilProduct();
        return UNIT_KGM.equals(zilProduct.getUnit()) && zilProduct.getPriceQuantity() >= KGM_THRESHOLD;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA772Model;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA772Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA772DiscountRowDao extends DefaultGenericDao<TkEuConditionRowA772Model> implements TkEuDiscountRowDao<TkEuConditionRowA772Model> {

    private static String QUERY_BY_COND_KEY = "SELECT {A772:" + PK + "} from {" + _TYPECODE + " AS A772 JOIN DISCOUNT AS discount"
      + " ON {A772:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A772:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A772:" + STARTTIME + "} <=?" + STARTTIME + " AND {A772:" + ENDTIME + "} >=?" + ENDTIME + "";

    public DefaultTkEuA772DiscountRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA772Model> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowA772Model> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowA772Model> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }
}

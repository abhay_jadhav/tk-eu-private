/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.pricedata.dao;

import java.util.List;
import java.util.Set;

import com.thyssenkrupp.b2b.eu.core.dao.TkUomConversionDao;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;

import de.hybris.platform.core.model.product.UnitModel;

public interface TkEuUomConversionDao extends TkUomConversionDao {

    List<UnitModel> fetchHybrisUnitByZilliantUnit(String zilliantUnit);

    List<TkUomConversionModel> findUomConversionByProducts(Set<String> productIds);
    
    List<TkUomConversionModel> findUomConversionByProductCode(String productCode);

}

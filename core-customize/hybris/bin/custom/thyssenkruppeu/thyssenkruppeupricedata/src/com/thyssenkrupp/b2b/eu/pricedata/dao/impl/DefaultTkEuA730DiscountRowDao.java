package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA730Model;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA730Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA730DiscountRowDao extends DefaultGenericDao<TkEuConditionRowA730Model>
        implements TkEuDiscountRowDao<TkEuConditionRowA730Model> {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuA730DiscountRowDao.class);
    private static String QUERY_BY_COND_KEY = "SELECT {A730:" + PK + "} from {" + _TYPECODE
            + " AS A730 JOIN DISCOUNT AS discount" + " ON {A730:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A730:"
            + CONDKEY + "} =?" + CONDKEY + "" + " AND {discount:code} =?" + DISCOUNT + " AND " + "{A730:" + STARTTIME
            + "} <=?" + STARTTIME + " AND {A730:" + ENDTIME + "} >=?" + ENDTIME + "";


    public DefaultTkEuA730DiscountRowDao() {
        super(_TYPECODE);
    }

    public List<TkEuConditionRowA730Model> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowA730Model> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY,
                params);
        LOG.info("Queried table \'{}\' for params: condKey=\'{}\', discount=\'{}\' with result count: {}.", _TYPECODE, condKey, sapConditionId, result.getCount());
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowA730Model> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }

}

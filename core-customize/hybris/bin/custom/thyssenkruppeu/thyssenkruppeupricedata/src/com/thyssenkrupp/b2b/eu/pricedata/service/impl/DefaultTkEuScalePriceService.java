package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuPriceRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuScalePriceService;

import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.Y2ZP;

public class DefaultTkEuScalePriceService implements TkEuScalePriceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuScalePriceService.class);

    private static final String     S_Y2ZP     = Y2ZP.toString();
    private static final BigDecimal PERCENTAGE = BigDecimal.valueOf(100);

    private CommonI18NService   commonI18nService;
    private TkEuPriceRowDao     tkEuPriceRowDao;
    private TkEuRoundingService roundingService;

    @Override
    public DiscountRowModel findScalePriceRowByConditionKeyAndQuantity(String concatenatedKey, Long quantity) {
        return getTkEuPriceRowDao().findPriceRowByCondKey(concatenatedKey, S_Y2ZP, quantity);
    }

    @Override
    public DiscountRowModel findMatchingDiscountRowByQty(List<DiscountRowModel> discountRows, Long quantity) {
        final TreeMap<Long, DiscountRowModel> priceRowCondA800Map = new TreeMap<>();
        for (DiscountRowModel priceRowCondA800Model : discountRows) {
            priceRowCondA800Map.put(priceRowCondA800Model.getMinqtd(), priceRowCondA800Model);
        }
        final Map.Entry<Long, DiscountRowModel> matchingPriceRowCondA800 = priceRowCondA800Map.floorEntry(quantity);
        return matchingPriceRowCondA800 != null ? matchingPriceRowCondA800.getValue() : null;
    }

    @Override
    public List<DiscountRowModel> findScalePriceRowsByConditionKey(String concatenatedKey) {
        return getTkEuPriceRowDao().findPriceRowByCondKey(concatenatedKey, S_Y2ZP);
    }

    @Nullable
    @Override
    public BigDecimal findScaleFactorFromDiscountRow(@Nullable DiscountRowModel discountRowModel) {
        if (discountRowModel == null) {
            LOG.warn("No PriceRowCondA800 found hence returning scale factor as \"Null\".");
            return null;
        }
        BigDecimal discountRowFactor = BigDecimal.valueOf(Math.abs(discountRowModel.getValue()));
        BigDecimal roundedScaleFactor = getRoundingService().roundDiscountScaleFactor(discountRowFactor.divide(safelyConvertUnitFactor(discountRowModel)));
        BigDecimal scaleFactor = roundedScaleFactor.divide(PERCENTAGE);
        return scaleFactor;
    }

    private BigDecimal safelyConvertUnitFactor(@NotNull DiscountRowModel priceRowCondA800Model) {
        Integer unitFactor = priceRowCondA800Model.getUnitFactor();
        return unitFactor == 0 ? BigDecimal.ONE : BigDecimal.valueOf(unitFactor);
    }

    public TkEuPriceRowDao getTkEuPriceRowDao() {
        return tkEuPriceRowDao;
    }

    @Required
    public void setTkEuPriceRowDao(TkEuPriceRowDao tkEuPriceRowDao) {
        this.tkEuPriceRowDao = tkEuPriceRowDao;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public TkEuRoundingService getRoundingService() {
        return roundingService;
    }

    @Required
    public void setRoundingService(TkEuRoundingService roundingService) {
        this.roundingService = roundingService;
    }
}

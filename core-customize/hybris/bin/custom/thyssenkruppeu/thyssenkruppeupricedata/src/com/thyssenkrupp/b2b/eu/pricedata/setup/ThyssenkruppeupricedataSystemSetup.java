package com.thyssenkrupp.b2b.eu.pricedata.setup;

import de.hybris.platform.core.initialization.SystemSetup;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import com.thyssenkrupp.b2b.eu.pricedata.service.ThyssenkruppeupricedataService;

@SystemSetup(extension = ThyssenkruppeupricedataConstants.EXTENSIONNAME)
public class ThyssenkruppeupricedataSystemSetup {
    private final ThyssenkruppeupricedataService thyssenkruppeupricedataService;

    public ThyssenkruppeupricedataSystemSetup(final ThyssenkruppeupricedataService thyssenkruppeupricedataService) {
        this.thyssenkruppeupricedataService = thyssenkruppeupricedataService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
        //
    }

}

package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.collections4.ListUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class TkEuHandlingCostsPopulator implements Populator<AbstractOrderModel, AbstractOrderData> {
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        Optional<DiscountValue> handlingCosts = findHandlingCosts(ListUtils.emptyIfNull(source.getGlobalDiscountValues()));

        if (handlingCosts.isPresent()) {
            double handlingValue = Math.abs(handlingCosts.get().getValue());
            target.setHandlingCosts(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(handlingValue), handlingCosts.get().getCurrencyIsoCode()));
        }
    }

    private Optional<DiscountValue> findHandlingCosts(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> ThyssenkruppeupricedataConstants.HANDLING_COSTS.equals(discountValue.getCode())).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowDefaultModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowDefaultModel.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class TkEuDefaultConditionRowDaoImpl extends DefaultGenericDao<TkEuConditionRowDefaultModel> implements TkEuDefaultConditionRowDao<TkEuConditionRowDefaultModel> {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuDefaultConditionRowDaoImpl.class);

    private static String QUERY_BY_COND_KEY = "SELECT {" + PK + "} FROM "
      + "{" + _TYPECODE + "} WHERE {" + CONDKEY + "} =?" + CONDKEY + "  AND "
      + "{" + STARTTIME + "} <=?" + STARTTIME + " AND {" + ENDTIME + "} >=?" + ENDTIME + " AND {" + CATALOGVERSION + "} = ?catalogVersion";

    public TkEuDefaultConditionRowDaoImpl() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowDefaultModel> findPriceRowByCondKey(String condKey, CatalogVersionModel catalogVersionModel) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(catalogVersionModel, "catalogVersion must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(CATALOGVERSION, catalogVersionModel);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowDefaultModel> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \'{}\' for params: condKey=\'{}\'. Found {} results.", _TYPECODE, condKey, result.getResult().size());
            if (result.getResult().size() > 0) {
                LOG.debug("Logging first condition row: sapConditionId=\'{}\', price=\'{}\'", result.getResult().get(0).getSapConditionId(), result.getResult().get(0).getPrice());
            }
        }
        return result.getResult();
    }
}

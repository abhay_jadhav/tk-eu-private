package com.thyssenkrupp.b2b.eu.pricedata.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.util.List;

public interface TkEuDefaultConditionRowDao<T extends PriceRowModel> {

    List<T> findPriceRowByCondKey(String condKey, CatalogVersionModel catalogVersionModel);
}

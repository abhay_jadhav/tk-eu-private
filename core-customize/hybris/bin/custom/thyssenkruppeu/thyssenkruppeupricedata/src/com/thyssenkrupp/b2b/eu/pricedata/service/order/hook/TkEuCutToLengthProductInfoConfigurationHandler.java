package com.thyssenkrupp.b2b.eu.pricedata.service.order.hook;

import com.thyssenkrupp.b2b.eu.pricedata.model.order.inmemory.InMemoryTkCutToLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.eu.pricedata.service.order.hook.helper.InMemoryCartHelper;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook.TkCutToLengthProductInfoConfigurationHandler;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.springframework.beans.factory.annotation.Required;

public class TkEuCutToLengthProductInfoConfigurationHandler extends TkCutToLengthProductInfoConfigurationHandler {

    private InMemoryCartHelper inMemoryCartHelper;

    @Override
    protected TkCutToLengthConfiguredProductInfoModel createCutToLengthConfiguredProductInfoModel(final AbstractOrderEntryModel entry) {
        if (getInMemoryCartHelper().isInMemoryCartEntry(entry)) {
            return getModelService().create(InMemoryTkCutToLengthConfiguredProductInfoModel.class);
        }
        return getModelService().create(TkCutToLengthConfiguredProductInfoModel.class);
    }

    public InMemoryCartHelper getInMemoryCartHelper() {
        return inMemoryCartHelper;
    }

    @Required
    public void setInMemoryCartHelper(InMemoryCartHelper inMemoryCartHelper) {
        this.inMemoryCartHelper = inMemoryCartHelper;
    }
}

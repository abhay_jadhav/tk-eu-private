package com.thyssenkrupp.b2b.eu.pricedata.exception;

public class TkEuUomConversionException extends RuntimeException {

    private static final long serialVersionUID = -3665031957150567390L;

    public TkEuUomConversionException() {
        super();
    }

    public TkEuUomConversionException(String message) {
        super(message);
    }

    public TkEuUomConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public TkEuUomConversionException(Throwable cause) {
        super(cause);
    }

    protected TkEuUomConversionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

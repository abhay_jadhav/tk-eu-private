package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA597Model;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA597Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA597DiscountRowDao extends DefaultGenericDao<TkEuConditionRowA597Model> implements TkEuDiscountRowDao<TkEuConditionRowA597Model> {

    private static String QUERY_BY_COND_KEY = "SELECT {A597:" + PK + "} from {" + _TYPECODE + " AS A597 JOIN DISCOUNT AS discount"
      + " ON {A597:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A597:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A597:" + STARTTIME + "} <=?" + STARTTIME + " AND {A597:" + ENDTIME + "} >=?" + ENDTIME + "";

    public DefaultTkEuA597DiscountRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA597Model> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowA597Model> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowA597Model> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }
}

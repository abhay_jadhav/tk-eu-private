package com.thyssenkrupp.b2b.eu.pricedata.dao;

import de.hybris.platform.europe1.model.DiscountRowModel;

import java.util.List;
import java.util.Optional;

public interface TkEuDiscountRowDao<T extends DiscountRowModel> {

    List<T> findDiscountRowsByCondKey(String condKey, String sapConditionId);

    Optional<T> findDiscountRowByCondKey(String condKey, String sapConditionId);
}

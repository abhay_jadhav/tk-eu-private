package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface TkEuUomConversionService extends TkUomConversionService {

    UnitModel getHybrisUnitFromZilliantUnit(String zilliantUnit);

    BigDecimal calculatePriceInBaseUOM(ProductModel productModel, BigDecimal sourceFactor, UnitModel sourceUnit) throws TkEuPriceServiceException;

    List<UnitModel> getSupportedUnitsForBaseStore();

    List<TkUomConversionModel> getSupportedUomConversions(ProductModel productModel) throws TkEuPriceServiceException;

    List<TkUomConversionModel> getStoreSupportedUomConversions(ProductModel productModel) throws TkEuPriceServiceException;

    Optional<BigDecimal> calculateWeightPerBaseUnit(TkUomConversionModel weightedUomConversion);

    @Deprecated
    Optional<BigDecimal> calculateWeightPerSalesUnit(TkUomConversionModel firstWeightedUomConversion, TkUomConversionModel salesConversionModel);

    BigDecimal calculateOrderTotalWeightInKg(AbstractOrderModel abstractOrderModel);

    BigDecimal calculateOrderEntriesTotalWeightInKg(List<AbstractOrderEntryModel> orderEntries);

    BigDecimal applyCommercialWeight(ProductModel productModel, UnitModel baseUnit, BigDecimal calculatedWeight, @NotNull List<TkUomConversionModel> productUomConversionModels);

    MaterialQuantityWrapper findTradeLengthUnitIfApplied(AbstractOrderEntryModel abstractOrderEntryModel);

    BigDecimal findTotalWeightForOrderEntryInKg(AbstractOrderEntryModel abstractOrderEntryModel) throws TkEuPriceServiceException;

    MaterialQuantityWrapper calculateQuantityInTargetUnit(MaterialQuantityWrapper sourceMaterialQty, UnitModel targetUnit, ProductModel product) throws TkEuPriceServiceException;

    BigDecimal applyCommercialWeight(String productCode, UnitModel baseUnit, BigDecimal calculatedWeight, @NotNull List<TkUomConversionModel> productUomConversionModels);
}

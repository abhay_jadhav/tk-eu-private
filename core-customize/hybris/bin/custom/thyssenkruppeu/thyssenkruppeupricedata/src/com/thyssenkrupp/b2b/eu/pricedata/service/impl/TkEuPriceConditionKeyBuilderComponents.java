package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.microsoft.sqlserver.jdbc.StringUtils;

import static java.util.Optional.empty;

public enum TkEuPriceConditionKeyBuilderComponents {

    SALES_ORGANIZATION("baseStore.SAPConfiguration.sapcommon_salesOrganization", 4, dto -> keyBuilder(() -> dto.getBaseStore().getSAPConfiguration().getSapcommon_salesOrganization())),

    OFFLINE_DISTRIBUTION_CHANNEL("offlineDistributionChannel", 2, dto -> keyBuilder(() -> dto.getBaseStore().getSAPConfiguration().getOfflineDistributionChannel())),

    ONLINE_DISTRIBUTION_CHANNEL("onlineDistributionChannel", 2, dto -> keyBuilder(() -> dto.getBaseStore().getSAPConfiguration().getSapcommon_distributionChannel())),

    SALES_OFFICE("childB2bUnit.salesOffice", 4, dto -> keyBuilder(() -> dto.getChildB2bUnit().getSalesOffice())),

    DIVISION("product.division", 2, dto -> keyBuilder(() -> dto.getProduct().getDivision())),

    SHIPPING_CONDITIONS("childB2bUnit.shippingcondition", 2, dto -> keyBuilder(() -> dto.getChildB2bUnit().getShippingcondition().toString())),

    ATZ_GROUP("childB2bUnit.atzGroup", 3, dto -> keyBuilder(() -> dto.getChildB2bUnit().getAtzGroup())),

    INCOTERMS("childB2bUnit.incoterms", 3, dto -> keyBuilder(() -> dto.getChildB2bUnit().getIncoterms().toString())),

    CURRENCY("currencyCode", 5, dto -> keyBuilder(dto::getCurrencyCode)),

    MATERIAL("product.code", 18, dto -> keyBuilder(() -> dto.getProduct().getCode())),

    VARIANT("variantId", 26, dto -> keyBuilder(dto::getVariantId)),

    VARIANT_FS("variantFs", 2, dto -> keyBuilder(dto::getVariantId)),

    CUSTOMER("b2BUnit.uid", 10, dto -> keyBuilder(() -> dto.getB2bUnit().getUid())),

    MATERIAL_CLASS("NOT_DEFINED", 18, dto -> empty()),

    MATKL("zilProduct.matkl", 4, dto -> keyBuilder(() -> dto.getZilProduct().getMatkl())),

    WRKST("zilProduct.wrkst", 0, dto -> keyBuilder(() -> dto.getZilProduct().getWrkst())),

    SCALE_KEY("zilPrice.scaleKey", 0, dto -> keyBuilder(() -> dto.getZilPrice().getScaleKey())),

    EMPTY("", 0, dto -> empty()),

    SPACE("", 0, dto -> Optional.of(StringUtils.SPACE)),
    
    PACKAGING_GROUP("packaging_group", 10, dto -> keyBuilder(() -> dto.getPackagingCostKeyFactors().getPackagingGroup().get())),
    
    OPTIONAL_VARIANT(VARIANT),

    OPTIONAL_SALES_OFFICE(SALES_OFFICE),

    OPTIONAL_DISTRIBUTION_CHANNEL(OFFLINE_DISTRIBUTION_CHANNEL),

    OPTIONAL_CUSTOMER(CUSTOMER);

    private String                                                      code;
    private int                                                         fixedLength;
    private boolean                                                     optional;
    private Function<TkEuPriceConditionKeyBuilderDto, Optional<String>> keyBuilderFunction;
    private String                                                      spaceCode;

    TkEuPriceConditionKeyBuilderComponents(String code, int fixedLength, Function<TkEuPriceConditionKeyBuilderDto, Optional<String>> function) {
        this.code = code;
        this.fixedLength = fixedLength;
        this.optional = false;
        this.keyBuilderFunction = function;
    }

    TkEuPriceConditionKeyBuilderComponents(TkEuPriceConditionKeyBuilderComponents tkEuPriceConditionKeyBuilderComponents) {
        this.code = tkEuPriceConditionKeyBuilderComponents.code;
        this.fixedLength = tkEuPriceConditionKeyBuilderComponents.fixedLength;
        this.optional = true;
        this.keyBuilderFunction = tkEuPriceConditionKeyBuilderComponents.keyBuilderFunction;
    }

    public String getCode() {
        return code;
    }

    public int getFixedLength() {
        return fixedLength;
    }

    public boolean isOptional() {
        return optional;
    }

    public Function<TkEuPriceConditionKeyBuilderDto, Optional<String>> getKeyBuilderFunction() {
        return keyBuilderFunction;
    }

    private static Optional<String> keyBuilder(Supplier<String> supplier) {
        return Optional.ofNullable(supplier.get());
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindSawingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.SAWINGCOSTS;
import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.YBSA;

public class DefaultTkEuFindSawingCostStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy implements TkEuFindSawingCostStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindSawingCostStrategy.class);

    @Override
    public DiscountValue getSawingCost(TkEuPriceParameter priceParameter) {
        List<Pair<GenericDao, List<String>>> conditionKeys = getSawingCostConditionKeys(priceParameter);
        List<DiscountRowModel> discountRowsModel = findDiscountRowsForConditionKeys(conditionKeys, YBSA);
        long selectedPieces = (long) priceParameter.getQuantityWrapper().getQuantity();
        DiscountRowModel matchingDiscountRowByQty = findMatchingDiscountRowByQty(discountRowsModel, selectedPieces);
        if (matchingDiscountRowByQty != null) {
            LOG.debug("Sawing Cost Per Piece for the order = " + matchingDiscountRowByQty.getValue());
            return DiscountValue.createAbsolute(SAWINGCOSTS, matchingDiscountRowByQty.getValue() * selectedPieces, priceParameter.getCurrencyCode());
        }
        return DiscountValue.createAbsolute(SAWINGCOSTS, 0.0, priceParameter.getCurrencyCode());
    }

    @Nullable
    protected DiscountRowModel findMatchingDiscountRowByQty(List<DiscountRowModel> discountRows, Long quantity) {
        final TreeMap<Long, DiscountRowModel> discountRowCondMap = new TreeMap<>();
        for (DiscountRowModel discountRowCondModel : discountRows) {
            discountRowCondMap.put(discountRowCondModel.getMinqtd(), discountRowCondModel);
        }
        final Map.Entry<Long, DiscountRowModel> matchingDiscountRow = discountRowCondMap.floorEntry(quantity);
        return matchingDiscountRow != null ? matchingDiscountRow.getValue() : null;
    }

    protected List<Pair<GenericDao, List<String>>> getSawingCostConditionKeys(TkEuPriceParameter priceParameter) {
        List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories = getDefaultTkEuConditionKeyBuilderSupplier().getSawingCostKeyBuilderSupplier().get();
        return tkEuPriceConditionKeyBuilderFactories.stream().map(tkEuPriceConditionKeyBuilderFactory -> {
            Pair<GenericDao, List<String>> conditionKeys = tkEuPriceConditionKeyBuilderFactory.build(priceParameter);
            LOG.debug("condition Keys for Sawing cost : " + conditionKeys);
            return conditionKeys;
        }).collect(Collectors.toList());
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.constants;

public final class ThyssenkruppeupricedataConstants extends GeneratedThyssenkruppeupricedataConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeupricedata";

    public static final String SAWINGCOSTS = "Sawing";

    public static final String SAWING_VARIANT_ID = "FS";

    public static final String PACKAGING_COSTS = "Packaging";

    public static final String ASM_SERVICE_FEE = "AsmServiceFee";

    public static final String CERTIFICATE_COSTS = "Certificate";

    public static final String TRANSPORT_COSTS = "Transport";

    public static final String HANDLING_COSTS_FIXED = "HandlingAbs";

    public static final String HANDLING_COSTS = "Handling";

    public static final String ZILLIANT = "ZILLIANT";

    public static final String MINIMUM_ORDER_SURCHARGE = "MinimumOrderSurcharge";

    public static final String CUSTOMER_SPECIFIC_PRICE = "CSP";

    public static final String TK_EU_ZILLIANT_PRICE_CATALOG_ID   = "tkEuZilliantPriceCatalog";

    public static final String PAC = "PAC";

    public static final String PROS = "PROS";

    public static final String ZILLIANT_FALLBACK_ENABLED = "pricing.zilliant.enabled";

    public static final String PROS_PRICING_ENABLED = "pricing.pros.enabled";


    private ThyssenkruppeupricedataConstants() {

    }
}

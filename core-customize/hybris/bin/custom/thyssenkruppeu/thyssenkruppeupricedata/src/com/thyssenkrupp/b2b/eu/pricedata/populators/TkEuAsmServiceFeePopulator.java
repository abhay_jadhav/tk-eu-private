package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.collections4.ListUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class TkEuAsmServiceFeePopulator implements Populator<AbstractOrderModel, AbstractOrderData> {
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        Optional<DiscountValue> asmServiceFee = findAsmServiceFee(ListUtils.emptyIfNull(source.getGlobalDiscountValues()));

        if (asmServiceFee.isPresent()) {
            double asmServiceValue = Math.abs(asmServiceFee.get().getValue());
            target.setAsmServiceFee(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(asmServiceValue), asmServiceFee.get().getCurrencyIsoCode()));
        }
    }

    private Optional<DiscountValue> findAsmServiceFee(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> ThyssenkruppeupricedataConstants.ASM_SERVICE_FEE.equals(discountValue.getCode())).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}

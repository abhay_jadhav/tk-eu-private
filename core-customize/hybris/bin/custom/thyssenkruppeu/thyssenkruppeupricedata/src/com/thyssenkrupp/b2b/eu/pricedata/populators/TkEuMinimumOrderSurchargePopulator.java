package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.collections4.ListUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class TkEuMinimumOrderSurchargePopulator implements Populator<AbstractOrderModel, AbstractOrderData> {

    private PriceDataFactory priceDataFactory;
    private BaseStoreService baseStoreService;

    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        Optional<DiscountValue> minimumSurcharge = findMinimumSurcharge(ListUtils.emptyIfNull(source.getGlobalDiscountValues()));
        if (minimumSurcharge.isPresent()) {
            double minimumOrderSurcharge = Math.abs(minimumSurcharge.get().getValue());
            target.setMinimumOrderValueSurcharge(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(minimumOrderSurcharge), minimumSurcharge.get().getCurrencyIsoCode()));
            target.setMinimumSurchargeDisplayToolTip(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(minimumOrderSurcharge), minimumSurcharge.get().getCurrencyIsoCode()));
            target.setMinimumSurchargeDisplayToolTipValue(target.getMinimumSurchargeDisplayToolTip().getFormattedValue());
        }
        getMinimumOrderValueThreshold(target);
    }

    private void getMinimumOrderValueThreshold(AbstractOrderData target) {
        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        if (baseStore.getMinimumOrderValueThreshold() != null) {
            target.setMinimumOrderValueThreshold(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(baseStore.getMinimumOrderValueThreshold()), target.getSubTotal().getCurrencyIso()));
            target.setMinimumOrderValueThresholdValue(target.getMinimumOrderValueThreshold().getFormattedValue());
        }
    }

    private Optional<DiscountValue> findMinimumSurcharge(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> ThyssenkruppeupricedataConstants.MINIMUM_ORDER_SURCHARGE.equals(discountValue.getCode())).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;

import java.util.List;

public interface TkEuExternalPricingService {

    PriceValue calculateBasePriceForCustomer(TkEuPriceParameter tkEuPriceParameter) throws TkEuPriceServiceException;

    PriceValue calculateBasePriceForOrderEntry(AbstractOrderEntryModel orderEntryModel, B2BUnitModel b2BUnitModel) throws TkEuPriceServiceException;

    List<PriceInformation> getPriceInformationsForProduct(ProductModel productModel, B2BUnitModel b2BUnitModel) throws TkEuPriceServiceException;
}

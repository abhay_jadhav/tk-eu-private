package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuUomConversionDao;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel.PRODUCTID;
import static com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel._TYPECODE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuUomConversionDao implements TkEuUomConversionDao {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuUomConversionDao.class);

    private static final String HYBRIS_UNIT_BY_ZILLIANT_UNIT = "SELECT {pk} FROM {Unit} WHERE {zilliantUnit}=?zilliantUnit";
    private static final String QUERY_BY_PRODUCTS = "select {pk} from {" + _TYPECODE + "} where {" + PRODUCTID + "} IN (?productId) ";
    private static final String UOM_CONVERSION_QUERY = "select {pk} from {" + _TYPECODE + "} where {" + PRODUCTID + "} = ?productCode ";

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<UnitModel> fetchHybrisUnitByZilliantUnit(String zilliantUnit) {
        validateParameterNotNull(zilliantUnit, "zilliantUnit cannot not be null!");
        final Map<String, Object> params = new HashMap<>();
        params.put(UnitModel.ZILLIANTUNIT, zilliantUnit);
        final SearchResult<UnitModel> result = getFlexibleSearchService().search(HYBRIS_UNIT_BY_ZILLIANT_UNIT, params);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queried table \'Unit\' for params: zilliantUnit \'{}\'. Found {} results.", zilliantUnit, result.getResult().size());
            if (result.getResult().size() > 0) {
                UnitModel unitModel = result.getResult().get(0);
                LOG.debug("Logging first unit: code=\'{}\', unitType=\'{}\', sapCode=\'{}\', zilliantUnit=\'{}\'.", unitModel.getCode(), unitModel.getUnitType(), unitModel.getSapCode(), unitModel.getZilliantUnit());
            }
        }
        return result.getResult();
    }

    @Override
    public List<TkUomConversionModel> findUomConversionByProducts(Set<String> productIds) {
        validateParameterNotNull(productIds, "productIds cannot not be null!");
        final Map<String, Object> params = new HashMap<>();
        params.put(PRODUCTID, productIds);
        SearchResult<TkUomConversionModel> result = getFlexibleSearchService().search(QUERY_BY_PRODUCTS, params);
        LOG.debug("Queried table \'{}\' for params: productIds=\'{}\' with result count: {}.", _TYPECODE, productIds, result.getCount());
        return ListUtils.emptyIfNull(result.getResult());
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }


    public List<TkUomConversionModel> findUomConversionByProductCode(final String productCode) {
        final SearchResult<TkUomConversionModel> searchResult = getFlexibleSearchService().search(getFlexibleQueryFilteredByProductCode(productCode));
        return searchResult.getResult();
    }

    private FlexibleSearchQuery getFlexibleQueryFilteredByProductCode(final String productCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(UOM_CONVERSION_QUERY);
        fQuery.addQueryParameter("productCode", productCode);
        return fQuery;
    }


}

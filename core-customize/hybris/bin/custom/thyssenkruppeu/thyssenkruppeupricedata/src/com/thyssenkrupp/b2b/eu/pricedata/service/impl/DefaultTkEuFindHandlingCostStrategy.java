package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindHandlingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TONNES_SAP_UNIT_CODE;
import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PDTRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.*;
import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.YFYA;
import static com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes.YFXF;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.convertToNegativeValue;

public class DefaultTkEuFindHandlingCostStrategy extends AbstractTkEuConditionDiscountRowFindingStrategy implements TkEuFindHandlingCostStrategy {

    public static final String HANDLING_COST_COND_KEY = "handling";
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindHandlingCostStrategy.class);
    private CostConfigurationProductInfoService costConfigurationProductInfoService;
    private TkEuDefaultConditionRowDao defaultConditionRowDao;
    private TkEuUomConversionService tkEuUomConversionService;
    private DefaultB2BCommerceUnitService defaultB2BCommerceUnitService;
    private UnitService unitService;

    @Override
    public DiscountValue getHandlingCost(AbstractOrderEntryModel entryModel, TkEuPriceParameter priceParameter) {
        try {
            if (null != entryModel) {
                BigDecimal totalWeight = getEntryTotalWeight(entryModel);
                LOG.info("Total weight for entry" + entryModel.getOrder().getCode() + entryModel.getProduct().getCode() + "is" + totalWeight);
                return getHandlingCostInWeight(priceParameter, totalWeight);
            }
        } catch (Exception e) {
            LOG.error("Error in calculating handling cost for entry" + entryModel.getOrder().getCode() + entryModel.getProduct().getCode() + e.getMessage());
        }
        return null;
    }

    @Override
    public DiscountValue getHandlingCostInWeight(TkEuPriceParameter priceParameter, BigDecimal totalWeight) {
        BigDecimal weightBasedPrice = BigDecimal.ZERO;
        Optional<DiscountRowModel> discountRowModelWeightBased = getHandlingCostAsDiscountRow(priceParameter, YFYA);
        Optional<DiscountRowModel> discountRowModelFixed = getHandlingCostAsDiscountRow(priceParameter, YFXF);

        if (discountRowModelWeightBased.isPresent()) {
            double discountValue = discountRowModelWeightBased.get().getValue();
            weightBasedPrice = calcHandlingCostFromDiscountValue(totalWeight, discountValue);
        }
        if (discountRowModelWeightBased.isPresent() && discountRowModelFixed.isPresent() && !Objects.isNull(weightBasedPrice)) {
            if (Math.abs(discountRowModelFixed.get().getValue()) >= Math.abs(weightBasedPrice.doubleValue())) {
                createAndUpdateHandlingCostConfigForEntry(priceParameter.getOrderEntry(), discountRowModelFixed.get(), HANDLING_COSTS_FIXED);
                return DiscountValue.createAbsolute(HANDLING_COSTS_FIXED, discountRowModelFixed.get().getValue(), priceParameter.getCurrencyCode());
            } else {
                createAndUpdateHandlingCostConfigForEntry(priceParameter.getOrderEntry(), discountRowModelWeightBased.get(), HANDLING_COSTS);
                return DiscountValue.createAbsolute(HANDLING_COSTS, weightBasedPrice.doubleValue(), priceParameter.getCurrencyCode());
            }
        } else if (discountRowModelWeightBased.isPresent() && !Objects.isNull(weightBasedPrice)) {
            createAndUpdateHandlingCostConfigForEntry(priceParameter.getOrderEntry(), discountRowModelWeightBased.get(), HANDLING_COSTS);
            return DiscountValue.createAbsolute(HANDLING_COSTS, weightBasedPrice.doubleValue(), priceParameter.getCurrencyCode());
        } else if (discountRowModelFixed.isPresent()) {
            createAndUpdateHandlingCostConfigForEntry(priceParameter.getOrderEntry(), discountRowModelFixed.get(), HANDLING_COSTS_FIXED);
            return DiscountValue.createAbsolute(HANDLING_COSTS_FIXED, discountRowModelFixed.get().getValue(), priceParameter.getCurrencyCode());
        }
        return calculateOrderEntryFallBackHandlingCost(priceParameter, totalWeight);
    }

    private DiscountValue calculateOrderEntryFallBackHandlingCost(TkEuPriceParameter priceParameter, BigDecimal totalWeight) {
        try {
            Optional<PriceRowModel> discountRowModelFallBack = getFallBackHandlingCostAsPriceRow(priceParameter);
            if (discountRowModelFallBack.isPresent()) {
                double discountValueFallBack = discountRowModelFallBack.get().getPrice();
                BigDecimal weightBasedPrice = calcHandlingCostFromDiscountValue(totalWeight, discountValueFallBack);
                if(!Objects.isNull(weightBasedPrice)) {
                createAndUpdateHandlingCostConfigForEntry(priceParameter.getOrderEntry(), discountRowModelFallBack.get(), HANDLING_COSTS);
                return DiscountValue.createAbsolute(HANDLING_COSTS, weightBasedPrice.doubleValue(), priceParameter.getCurrencyCode());
                }
            }
        } catch (Exception e) {
            LOG.error("Error in calculating fall back handling cost of the cart with product code"+ priceParameter.getOrderEntry().getProduct().getCode() + e.getMessage());
        }
        return DiscountValue.createAbsolute(HANDLING_COSTS, BigDecimal.ZERO.doubleValue(), priceParameter.getCurrencyCode());
    }

    private void createAndUpdateHandlingCostConfigForEntry(AbstractOrderEntryModel entry, PDTRowModel rowModel, String handlingType) {
        DiscountRowModel discountRowModel;
        PriceRowModel priceRowModel;
        CostConfigurationProductInfoModel handlingCostConfigurationProductInfo;
        if (rowModel instanceof DiscountRowModel) {
            discountRowModel = (DiscountRowModel) rowModel;
            handlingCostConfigurationProductInfo = getCostConfigurationProductInfoService().createHandlingCostConfigurationProductInfo(entry, discountRowModel.getCurrency(), discountRowModel.getUnitFactor(), discountRowModel.getUnit(), discountRowModel.getValue(), handlingType);
            getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(entry, handlingCostConfigurationProductInfo);
        } else {
            if (rowModel instanceof PriceRowModel) {
                priceRowModel = (PriceRowModel) rowModel;
                handlingCostConfigurationProductInfo = getCostConfigurationProductInfoService().createHandlingCostConfigurationProductInfo(entry, priceRowModel.getCurrency(), priceRowModel.getUnitFactor(), priceRowModel.getUnit(), priceRowModel.getPrice(), handlingType);
                getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(entry, handlingCostConfigurationProductInfo);
            }
        }
    }

    // method to find yfya and yfxf
    @Override
    public Optional<DiscountRowModel> getHandlingCostAsDiscountRow(TkEuPriceParameter priceParameter, ConditionKeyTypes handlingkey) {
        List<Pair<GenericDao, List<String>>> conditionKeys = getHandlingCostConditionKeys(priceParameter);
        return findDiscountRowForConditionKeys(conditionKeys, handlingkey);
    }

    // new method to calculate total weight of order entry
    public BigDecimal getEntryTotalWeight(AbstractOrderEntryModel entryModel) {
        try {
            if (entryModel != null) {
                BigDecimal totalWeight = getTkEuUomConversionService().findTotalWeightForOrderEntryInKg(entryModel);
                return totalWeight;
            }
        } catch (Exception e) {
            LOG.error("Error in calculating total weight of the cart entry with pk" + entryModel.getOrder().getCode() + entryModel.getProduct().getCode() + e.getMessage());
        }
        return null;
    }

    private BigDecimal calcHandlingCostFromDiscountValue(final BigDecimal totalWeight, Double discountValue) {
        try {
            UnitModel tonnes = getUnitService().getUnitForCode(TONNES_SAP_UNIT_CODE);
            BigDecimal totalPriceInWeight = BigDecimal.valueOf(discountValue).multiply(totalWeight);
            if (null != totalPriceInWeight) {
            BigDecimal handlingCost = getTkEuUomConversionService().calculateFactor(totalPriceInWeight.doubleValue(), tonnes.getConversion()).orElse(BigDecimal.ONE);
            return handlingCost;
            }
        } catch (Exception e) {
            LOG.error("Error in calculating handling cost" + e.getMessage());
        }
        return null;
    }

    @Override
    public Optional<PriceRowModel> getFallBackHandlingCostAsPriceRow(@NotNull TkEuPriceParameter priceParameter) {
        List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao().findPriceRowByCondKey(HANDLING_COST_COND_KEY, priceParameter.getProduct().getCatalogVersion());
        return priceRowByCondKey.stream().findFirst();
    }

    protected List<Pair<GenericDao, List<String>>> getHandlingCostConditionKeys(TkEuPriceParameter priceParameter) {
        List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories = getDefaultTkEuConditionKeyBuilderSupplier().getHandlingCostKeyBuilderSupplier().get();
        return tkEuPriceConditionKeyBuilderFactories.stream().map(tkEuPriceConditionKeyBuilderFactory -> {
            Pair<GenericDao, List<String>> conditionKeys = tkEuPriceConditionKeyBuilderFactory.build(priceParameter);
            LOG.debug("condition Keys for handling cost : " + conditionKeys);
            return conditionKeys;
        }).collect(Collectors.toList());
    }

    protected double getFallBackHandlingCost(@NotNull TkEuPriceParameter priceParameter) {
        Optional<PriceRowModel> priceRowModel = getFallBackHandlingCostAsPriceRow(priceParameter);
        if (priceRowModel.isPresent()) {
            LOG.debug("FallBack Handling cost = " + priceRowModel.get().getPrice());
            return convertToNegativeValue(priceRowModel.get().getPrice());
        }
        return 0.0;
    }

    public TkEuDefaultConditionRowDao getDefaultConditionRowDao() {
        return defaultConditionRowDao;
    }

    @Required
    public void setDefaultConditionRowDao(TkEuDefaultConditionRowDao defaultConditionRowDao) {
        this.defaultConditionRowDao = defaultConditionRowDao;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    public CostConfigurationProductInfoService getCostConfigurationProductInfoService() {
        return costConfigurationProductInfoService;
    }

    @Required
    public void setCostConfigurationProductInfoService(CostConfigurationProductInfoService costConfigurationProductInfoService) {
        this.costConfigurationProductInfoService = costConfigurationProductInfoService;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowATZModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowATZModel.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.*;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuATZDiscountRowDao extends DefaultGenericDao<TkEuConditionRowATZModel> implements TkEuDiscountRowDao<TkEuConditionRowATZModel> {

    private static String QUERY_BY_COND_KEY = "SELECT {ATZ:" + PK + "} from {" + _TYPECODE + " AS ATZ JOIN DISCOUNT AS discount"
      + " ON {ATZ:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {ATZ:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{ATZ:" + STARTTIME + "} <=?" + STARTTIME + " AND {ATZ:" + ENDTIME + "} >=?" + ENDTIME + "";

    public DefaultTkEuATZDiscountRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowATZModel> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowATZModel> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowATZModel> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }
}

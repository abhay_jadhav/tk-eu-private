package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import org.apache.commons.lang3.tuple.Pair;
import java.util.List;

public interface TkEuPriceConditionKeyBuilderFactory {

    ConditionKeyTypes getConditionKeyType();

    Pair<GenericDao, List<String>> build(TkEuPriceParameter priceParameter);
}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;

import java.math.BigDecimal;

public interface TkEuRoundingService {

    BigDecimal roundPriceBasedOnPriceQuantity(TkEuPriceParameter priceParameter, BigDecimal priceInBaseUom);

    BigDecimal roundZilPriceValue(ZilProductModel zilProductModel);

    BigDecimal roundZilTargetFactor(ZilPriceModel zilPriceModel);

    BigDecimal roundDiscountScaleFactor(BigDecimal scaleFactor);

    BigDecimal roundBasePrice(BigDecimal basePrice);

    BigDecimal roundScaledBasePrice(BigDecimal scaledPrice);

    BigDecimal scaleByThree(BigDecimal value);

    BigDecimal roundPriceBasedOnPriceQuantityForPac(TkEuPriceParameter priceParameter, BigDecimal priceInZilUom);
}

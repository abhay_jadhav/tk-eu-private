package com.thyssenkrupp.b2b.eu.pricedata.exception;

public class TkEuPriceServiceException extends Exception {

    private static final long serialVersionUID = -9113175713361778115L;

    public TkEuPriceServiceException() {
        super();
    }

    public TkEuPriceServiceException(String message) {
        super(message);
    }

    public TkEuPriceServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public TkEuPriceServiceException(Throwable cause) {
        super(cause);
    }

    protected TkEuPriceServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

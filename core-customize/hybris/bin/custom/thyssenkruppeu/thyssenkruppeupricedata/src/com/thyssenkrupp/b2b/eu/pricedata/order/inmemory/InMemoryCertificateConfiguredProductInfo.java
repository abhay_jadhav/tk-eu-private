package com.thyssenkrupp.b2b.eu.pricedata.order.inmemory;

import de.hybris.platform.core.PK;
import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.JaloOnlyItem;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.servicelayer.internal.jalo.order.JaloOnlyItemHelper;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class InMemoryCertificateConfiguredProductInfo extends GeneratedInMemoryCertificateConfiguredProductInfo implements JaloOnlyItem {

    private static final Logger             LOG                     = Logger.getLogger(InMemoryCertificateConfiguredProductInfo.class.getName());
    private static final String             CERTIFICATE_ID_PROPERTY = "certificateId";
    private              JaloOnlyItemHelper certificateData;

    public InMemoryCertificateConfiguredProductInfo() {
        //empty constructor needed for generic creation
    }

    @Override
    @ForceJALO(reason = "something else")
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException {
        Set missing = new HashSet();
        if (!checkMandatoryAttribute(CERTIFICATE_ID_PROPERTY, allAttributes, missing)) {
            throw new JaloInvalidParameterException("missing parameters to create a InMemoryCertificateConfiguredProductInfo ( missing " + missing + ")", 0);
        } else {
            final Class jaloClass = type.getJaloClass();
            LOG.debug("Creating InMemory CertificateConfiguredProductInfo");
            try {
                final InMemoryCertificateConfiguredProductInfo newInstance = (InMemoryCertificateConfiguredProductInfo) jaloClass.newInstance();
                newInstance.setTenant(type.getTenant());
                newInstance.certificateData = new JaloOnlyItemHelper((PK) allAttributes.get(PK), newInstance, type, new Date(), null);
                return newInstance;
            } catch (ClassCastException | InstantiationException | IllegalAccessException e) {
                throw new JaloGenericCreationException("could not instantiate wizard class " + jaloClass + " of type " + type.getCode() + " : " + e, 0);
            }
        }
    }

    @ForceJALO(reason = "something else")
    protected ItemAttributeMap getNonInitialAttributes(SessionContext ctx, ItemAttributeMap allAttributes) {
        ItemAttributeMap ret = new ItemAttributeMap(allAttributes);
        ret.remove(Item.PK);
        ret.remove(Item.TYPE);
        ret.remove("itemtype");
        return ret;
    }

    @ForceJALO(reason = "something else")
    public String getCertificateId(SessionContext ctx) {
        return (String) this.certificateData.getProperty(ctx, CERTIFICATE_ID_PROPERTY);
    }

    @ForceJALO(reason = "something else")
    public void setCertificateId(SessionContext ctx, String certificateId) {
        this.certificateData.setProperty(ctx, CERTIFICATE_ID_PROPERTY, certificateId);
    }

    @ForceJALO(reason = "something else")
    public String getName(SessionContext ctx) {
        return (String) this.certificateData.getProperty(ctx, "name");
    }

    @ForceJALO(reason = "something else")
    public void setName(SessionContext ctx, String name) {
        this.certificateData.setProperty(ctx, "name", name);
    }

    @ForceJALO(reason = "something else")
    public String getDescription(SessionContext ctx) {
        return (String) this.certificateData.getProperty(ctx, "description");
    }

    @ForceJALO(reason = "something else")
    public void setDescription(SessionContext ctx, String description) {
        this.certificateData.setProperty(ctx, "description", description);
    }

    @ForceJALO(reason = "something else")
    public Boolean isChecked(SessionContext ctx) {
        return this.certificateData.getPropertyBoolean(ctx, "checked", false);
    }

    @ForceJALO(reason = "something else")
    public void setChecked(SessionContext ctx, boolean value) {
        this.certificateData.setProperty(ctx, "checked", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getProductInfoStatus(SessionContext ctx) {
        return (EnumerationValue) this.certificateData.getProperty(ctx, "productInfoStatus");
    }

    @ForceJALO(reason = "something else")
    public void setProductInfoStatus(SessionContext ctx, EnumerationValue value) {
        this.certificateData.setProperty(ctx, "productInfoStatus", value);
    }

    @ForceJALO(reason = "something else")
    public EnumerationValue getConfiguratorType(SessionContext ctx) {
        return (EnumerationValue) this.certificateData.getProperty(ctx, "configuratorType");
    }

    @ForceJALO(reason = "something else")
    public void setConfiguratorType(SessionContext ctx, EnumerationValue value) {
        this.certificateData.setProperty(ctx, "configuratorType", value);
    }

    public final ComposedType provideComposedType() {
        return this.certificateData.provideComposedType();
    }

    public final Date provideCreationTime() {
        return this.certificateData.provideCreationTime();
    }

    public final Date provideModificationTime() {
        return this.certificateData.provideModificationTime();
    }

    public final PK providePK() {
        return this.certificateData.providePK();
    }

    public void removeJaloOnly() throws ConsistencyCheckException {
        this.certificateData.removeJaloOnly();
    }

    public Object doGetAttribute(SessionContext ctx, String attrQualifier) throws JaloSecurityException {
        return this.certificateData.doGetAttribute(ctx, attrQualifier);
    }

    public void doSetAttribute(SessionContext ctx, String attrQualifier, Object value) throws JaloBusinessException {
        this.certificateData.doSetAttribute(ctx, attrQualifier, value);
    }
}

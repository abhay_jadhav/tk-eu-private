package com.thyssenkrupp.b2b.eu.pricedata.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections4.ListUtils;
import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

public class TkEuHandlingCostOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {

    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(AbstractOrderEntryModel source, OrderEntryData target) throws ConversionException {

        Optional<DiscountValue> handlingCosts = findHandlingCosts(ListUtils.emptyIfNull(source.getDiscountValues()));
        if (handlingCosts.isPresent()) {
            String handlingType = handlingCosts.get().getCode();
            double handlingValue = Math.abs(handlingCosts.get().getValue());
            if (!Objects.isNull(handlingType)) {
            target.setHandlingCostType(handlingType);
            }
            target.setHandlingCost(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(handlingValue), handlingCosts.get().getCurrencyIsoCode()));
        }
    }

    private Optional<DiscountValue> findHandlingCosts(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> discountValue.getCode().contains(ThyssenkruppeupricedataConstants.HANDLING_COSTS)).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuPriceRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA805Model;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Required;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA805Model.*;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.europe1.model.AbstractDiscountRowModel.DISCOUNT;
import static de.hybris.platform.europe1.model.PDTRowModel.CONDKEY;
import static de.hybris.platform.europe1.model.PDTRowModel.ENDTIME;
import static de.hybris.platform.europe1.model.PDTRowModel.STARTTIME;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA805PriceRowDao extends DefaultGenericDao<TkEuConditionRowA805Model> implements TkEuPriceRowDao<TkEuConditionRowA805Model> {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuA805PriceRowDao.class);
    private static String QUERY_BY_COND_KEY = "SELECT {A805:" + PK + "} from {" + _TYPECODE + " AS A805 JOIN DISCOUNT AS discount"
      + " ON {A805:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A805:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A805:" + STARTTIME + "} <=?" + STARTTIME + " AND {A805:" + ENDTIME + "} >=?" + ENDTIME + "";

    private TkEuCatalogVersionService catalogVersionService;

    public DefaultTkEuA805PriceRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA805Model> findPriceRowByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<TkEuConditionRowA805Model> result;
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        LOG.debug("Queried table \'{}\' for params: condKey=\'{}\', discount=\'{}\' with result count: {}.", _TYPECODE, condKey, sapConditionId, result.getCount());
        return getPacConditionOrScales(result.getResult());
    }

    @Override
    public TkEuConditionRowA805Model findPriceRowByCondKey(String condKey, String sapConditionId,
            Long quantityInScaleUnit) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<TkEuConditionRowA805Model> result;
        params.put(CONDKEY, condKey);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        params.put(DISCOUNT, sapConditionId);
        result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        LOG.debug("Queried table \'{}\' for params: condKey=\'{}\', discount=\'{}\', quantity=\'{}\' with result: {}.", _TYPECODE, condKey, sapConditionId, quantityInScaleUnit, result.getResult());
        return findMatchingPriceRowCondA805ForQty(result, quantityInScaleUnit);
    }

    private TkEuConditionRowA805Model findMatchingPriceRowCondA805ForQty(SearchResult<TkEuConditionRowA805Model> result, Long quantity) {
        final TreeMap<Long, TkEuConditionRowA805Model> priceRowCondA805Map = new TreeMap<>();
        List<TkEuConditionRowA805Model> pacResult = getPacConditionOrScales(result.getResult());
        if(CollectionUtils.isNotEmpty(pacResult)) {
        for (TkEuConditionRowA805Model priceRowCondA805Model : pacResult) {
            priceRowCondA805Map.put(priceRowCondA805Model.getMinqtd(), priceRowCondA805Model);
        }
        }
        final Entry<Long, TkEuConditionRowA805Model> matchingPriceRowCondA805 = priceRowCondA805Map.floorEntry(quantity);
        return matchingPriceRowCondA805 != null ? matchingPriceRowCondA805.getValue() : null;
    }

    private List<TkEuConditionRowA805Model> getPacConditionOrScales(List<TkEuConditionRowA805Model> result) {
        List<TkEuConditionRowA805Model> pacCondition= new ArrayList<TkEuConditionRowA805Model>();
        List<TkEuConditionRowA805Model> pacScales= new ArrayList<TkEuConditionRowA805Model>();
        if(CollectionUtils.isNotEmpty(result))
            for(TkEuConditionRowA805Model row: result) {
                if(row.getSapConditionId().contains("_")) {
                    pacScales.add(row);
                }else {
                    pacCondition.add(row);
                }
            }
        if(!pacScales.isEmpty()) {
                return pacScales;
        } else{
                return pacCondition;
            }
    }

    protected TkEuCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }


}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.util.DiscountValue;

import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.Optional;

public interface TkEuFindHandlingCostStrategy {
    DiscountValue getHandlingCostInWeight(TkEuPriceParameter priceParameter,BigDecimal totalWeight);
    Optional<DiscountRowModel> getHandlingCostAsDiscountRow(TkEuPriceParameter priceParameter, ConditionKeyTypes handlingkey);
    Optional<PriceRowModel> getFallBackHandlingCostAsPriceRow(@NotNull TkEuPriceParameter priceParameter);
    DiscountValue getHandlingCost(AbstractOrderEntryModel entryModel, TkEuPriceParameter priceParameter);
}

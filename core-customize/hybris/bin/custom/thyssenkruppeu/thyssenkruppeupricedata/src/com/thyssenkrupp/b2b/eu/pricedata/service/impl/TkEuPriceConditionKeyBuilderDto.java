package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.PackagingCostKeyFactors;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilPriceModel;
import com.thyssenkrupp.b2b.eu.zilliantpricing.model.ZilProductModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;

public class TkEuPriceConditionKeyBuilderDto {

    private B2BUnitModel b2bUnit;

    private ProductModel product;

    private UserModel user;

    private BaseStoreModel baseStore;

    private String variantId;

    private String currencyCode;

    private ZilPriceModel zilPrice;

    private ZilProductModel zilProduct;

    private B2BUnitModel childB2bUnit;
    
    private PackagingCostKeyFactors packagingCostKeyFactors;
    

    public B2BUnitModel getB2bUnit() {
        return b2bUnit;
    }

    public void setB2bUnit(B2BUnitModel b2bUnit) {
        this.b2bUnit = b2bUnit;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public BaseStoreModel getBaseStore() {
        return baseStore;
    }

    public void setBaseStore(BaseStoreModel baseStore) {
        this.baseStore = baseStore;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public B2BUnitModel getChildB2bUnit() {
        return childB2bUnit;
    }

    public void setChildB2bUnit(B2BUnitModel childB2bUnit) {
        this.childB2bUnit = childB2bUnit;
    }

    public ZilPriceModel getZilPrice() {
        return zilPrice;
    }

    public void setZilPrice(ZilPriceModel zilPrice) {
        this.zilPrice = zilPrice;
    }

    public ZilProductModel getZilProduct() {
        return zilProduct;
    }

    public void setZilProduct(ZilProductModel zilProduct) {
        this.zilProduct = zilProduct;
    }

    /**
     * @return the packagingCostKeyFactors
     */
    public PackagingCostKeyFactors getPackagingCostKeyFactors() {
        return packagingCostKeyFactors;
    }

    /**
     * @param packagingCostKeyFactors the packagingCostKeyFactors to set
     */
    public void setPackagingCostKeyFactors(PackagingCostKeyFactors packagingCostKeyFactors) {
        this.packagingCostKeyFactors = packagingCostKeyFactors;
    }

}

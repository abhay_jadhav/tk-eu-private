package com.thyssenkrupp.b2b.eu.pricedata;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.RedeployUtilities;

public class ThyssenkruppeupricedataStandalone {

    public static void main(final String[] args) {
        new ThyssenkruppeupricedataStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();
        RedeployUtilities.shutdown();
    }
}

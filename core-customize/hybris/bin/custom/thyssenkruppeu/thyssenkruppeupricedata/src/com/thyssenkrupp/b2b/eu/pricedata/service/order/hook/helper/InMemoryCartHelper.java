package com.thyssenkrupp.b2b.eu.pricedata.service.order.hook.helper;

import com.thyssenkrupp.b2b.eu.pricedata.model.order.inmemory.InMemoryCertificateConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.eu.pricedata.model.order.inmemory.InMemoryTextFieldConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.eu.pricedata.model.order.inmemory.InMemoryTradeLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartEntryModel;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguratorSettingModel;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguredProductInfoModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public class InMemoryCartHelper {

    private static final Logger              LOG                            = LoggerFactory.getLogger(InMemoryCartHelper.class);
    private static final String              IN_MEMORY_CART_ENTRY_TYPE_CODE = "InMemoryCartEntry";
    private              Map<String, String> configuratorType2orderEntryProductInfoTypeMapping;
    private              I18NService         i18nService;
    private              ModelService        modelService;

    public Predicate<AbstractOrderModel> isInMemoryCartPredicate() {
        return InMemoryCartModel.class::isInstance;
    }

    public Predicate<AbstractOrderEntryModel> isInMemoryCartEntryPredicate() {
        return InMemoryCartEntryModel.class::isInstance;
    }

    public boolean isInMemoryCartEntry(final AbstractOrderEntryModel entry) {
        return nonNull(entry) && IN_MEMORY_CART_ENTRY_TYPE_CODE.equals(getModelService().getModelType(entry));
    }

    public boolean isInMemoryProductInfoEntry(final AbstractConfiguratorSettingModel configuration, final AbstractOrderEntryModel entry) {
        return isInMemoryCartEntry(entry) && nonNull(configuration) && isValidConfiguratorType(configuration.getConfiguratorType().getCode());
    }

    public void createInMemoryProductInfo(final AbstractConfiguratorSettingModel productSettings, final AbstractOrderEntryModel entry) {
        final List<AbstractOrderEntryProductInfoModel> productInfoList = createInMemoryProductInfos(productSettings);
        if (CollectionUtils.isNotEmpty(productInfoList)) {
            entry.setProductInfos(Stream.concat(entry.getProductInfos() == null ? Stream.empty() : entry.getProductInfos().stream(),
                productInfoList.stream().peek(item -> item.setOrderEntry(entry)).peek(getModelService()::save)).collect(toList()));
        }
    }

    private boolean isValidConfiguratorType(final String code) {
        return StringUtils.isNotEmpty(code) && getConfiguratorType2orderEntryProductInfoTypeMapping().containsKey(code);
    }

    private List<AbstractOrderEntryProductInfoModel> createInMemoryProductInfos(final AbstractConfiguratorSettingModel productSettings) {
        List<AbstractOrderEntryProductInfoModel> productInfoList = new ArrayList<>();
        if (productSettings instanceof TkTradeLengthConfiguratorSettingsModel) {
            productInfoList.add(createInMemoryTradeLengthConfiguredProductInfo(productSettings));
        } else if (productSettings instanceof CertificateConfiguratorSettingsModel) {
            productInfoList.add(createInMemoryCertificateConfiguredProductInfo(productSettings));
        } else if (productSettings instanceof TextFieldConfiguratorSettingModel) {
            productInfoList.add(createInMemoryTextFieldConfiguredProductInfo(productSettings));
        } else {
            LOG.warn("Cannot create InMemoryProductInfo, InValid productSettings");
        }
        return productInfoList;
    }

    private AbstractOrderEntryProductInfoModel createInMemoryTradeLengthConfiguredProductInfo(final AbstractConfiguratorSettingModel productSettings) {
        final TkTradeLengthConfiguratorSettingsModel settingsModel = (TkTradeLengthConfiguratorSettingsModel) productSettings;
        final TkTradeLengthConfiguredProductInfoModel productInfoModel = getModelService().create(InMemoryTradeLengthConfiguredProductInfoModel.class);
        productInfoModel.setConfiguratorType(ConfiguratorType.TKTRADELENGTH);
        productInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        productInfoModel.setLabel(settingsModel.getLabel());
        productInfoModel.setDisplayValue(settingsModel.getDisplayValue(getI18nService().getCurrentLocale()));
        productInfoModel.setLengthValue(settingsModel.getLengthValue());
        productInfoModel.setUnit(settingsModel.getUnit());
        productInfoModel.setChecked(settingsModel.isChecked());
        return productInfoModel;
    }

    private AbstractOrderEntryProductInfoModel createInMemoryCertificateConfiguredProductInfo(final AbstractConfiguratorSettingModel productSettings) {
        final CertificateConfiguratorSettingsModel settingsModel = (CertificateConfiguratorSettingsModel) productSettings;
        final CertificateConfiguredProductInfoModel productInfoModel = getModelService().create(InMemoryCertificateConfiguredProductInfoModel.class);
        productInfoModel.setConfiguratorType(ConfiguratorType.CERTIFICATE);
        productInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        productInfoModel.setCertificateId(settingsModel.getCertificateId());
        productInfoModel.setName(settingsModel.getName());
        productInfoModel.setChecked(settingsModel.isChecked());
        productInfoModel.setDescription(settingsModel.getDescription());
        return productInfoModel;
    }

    private AbstractOrderEntryProductInfoModel createInMemoryTextFieldConfiguredProductInfo(final AbstractConfiguratorSettingModel productSettings) {
        final TextFieldConfiguratorSettingModel settingsModel = (TextFieldConfiguratorSettingModel) productSettings;
        final TextFieldConfiguredProductInfoModel productInfoModel = getModelService().create(InMemoryTextFieldConfiguredProductInfoModel.class);
        productInfoModel.setConfiguratorType(ConfiguratorType.TEXTFIELD);
        productInfoModel.setConfigurationLabel(settingsModel.getTextFieldLabel());
        productInfoModel.setConfigurationValue(settingsModel.getTextFieldDefaultValue());
        if (StringUtils.isNotEmpty(productInfoModel.getConfigurationValue())) {
            productInfoModel.setProductInfoStatus(ProductInfoStatus.SUCCESS);
        } else {
            productInfoModel.setProductInfoStatus(ProductInfoStatus.ERROR);
        }
        return productInfoModel;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public Map<String, String> getConfiguratorType2orderEntryProductInfoTypeMapping() {
        return configuratorType2orderEntryProductInfoTypeMapping;
    }

    public void setConfiguratorType2orderEntryProductInfoTypeMapping(Map<String, String> configuratorType2orderEntryProductInfoTypeMapping) {
        this.configuratorType2orderEntryProductInfoTypeMapping = configuratorType2orderEntryProductInfoTypeMapping;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}

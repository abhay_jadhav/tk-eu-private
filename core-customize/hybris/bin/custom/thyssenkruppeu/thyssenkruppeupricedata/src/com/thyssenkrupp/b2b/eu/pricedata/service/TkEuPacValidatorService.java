package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.order.price.PriceInformation;

public class TkEuPacValidatorService {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuPacValidatorService.class);

    @SuppressWarnings("deprecation")
        //for Y2MA without Scales with non zero condition value
        public Boolean pacWithOutScalesWithNonZeroConditionValue(List<PriceInformation> pacPriceInfo) {
        if(!Objects.isNull(pacPriceInfo)) {
        if(pacPriceInfo.size()==1 && (Long)pacPriceInfo.get(0).getQualifiers().get(PriceRow.MINQTD) == 0L && pacPriceInfo.get(0).getPrice().getValue()!=0.0d) {//NOSONAR
            return true;
        }
        }
        return false;
        }

       //for Y2MA without Scales with zero condition value
        public Boolean pacWithOutScalesWithZeroConditionValue(List<PriceInformation> pacPriceInfo) {
        if(!Objects.isNull(pacPriceInfo)) {
        if(pacPriceInfo.size()==1 && (Long)pacPriceInfo.get(0).getQualifiers().get(PriceRow.MINQTD) == 0L && pacPriceInfo.get(0).getPrice().getValue()==0.0d) {//NOSONAR
            return true;
        }
        }
        return false;
        }

        //if scale price quantity starts with 0, Pacpriceinfo is already sorted by quantity, Pac has proper scales
        public boolean checkIfPacScaleStartsWithZero(List<PriceInformation> pacPriceInfo) {
        if(!pacPriceInfo.isEmpty() && !Objects.isNull(pacPriceInfo.get(0)) && pacPriceInfo.size()>1 ) {//NOSONAR
            if((Long)pacPriceInfo.get(0).getQualifiers().get(PriceRow.MINQTD) == 0L) {
                return true;
          }
        }
        return false;
        }

        //any pac for any scale has price value 0
        public boolean checkIfPacScaleValueIsZero(List<PriceInformation> pacPriceInfo) {
            if(!pacPriceInfo.isEmpty()) {
            for (PriceInformation priceInfo : pacPriceInfo) {
                if(priceInfo.getPrice().getValue() == 0.0d)   //NOSONAR
                    return true;
                }
            }
            return false;
        }

      //check if pac and zilliant have same scale unit
        public boolean checkPacAndZilScaleUnit(PriceInformation pacPriceInfo, PriceInformation zilPriceInfo) {
            UnitModel zilUnit = (UnitModel)zilPriceInfo.getQualifiers().get(PriceRow.UNIT);
            UnitModel pacUnit = (UnitModel)pacPriceInfo.getQualifiers().get(PriceRow.UNIT);
            if(zilUnit.getCode().equalsIgnoreCase(pacUnit.getCode())) {
                return true;
            }
            LOG.debug("Pac scales "+ pacUnit.getCode()+" and zil scales "+ zilUnit.getCode()+"do not match, scales will not be merged");
            return false;
        }
}

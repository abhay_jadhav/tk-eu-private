package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;

import de.hybris.platform.jalo.order.price.PriceInformation;

public interface TkEuFindTradeLengthPriceStrategy {

    PriceInformation getTradeLengthPrice(TkEuPriceParameter priceParameter);

}

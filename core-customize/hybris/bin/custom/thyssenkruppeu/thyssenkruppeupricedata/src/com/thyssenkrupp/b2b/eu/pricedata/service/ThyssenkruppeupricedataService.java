
package com.thyssenkrupp.b2b.eu.pricedata.service;

public interface ThyssenkruppeupricedataService {
    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);
}

package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDiscountRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model.CONDKEY;
import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model.DISCOUNT;
import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model.ENDTIME;
import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model.PK;
import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model.STARTTIME;
import static com.thyssenkrupp.b2b.eu.pricedata.model.TkEuConditionRowA854Model._TYPECODE;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.getTruncatedDate;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuA854DiscountRowDao extends DefaultGenericDao<TkEuConditionRowA854Model> implements TkEuDiscountRowDao<TkEuConditionRowA854Model> {

    private static String QUERY_BY_COND_KEY = "SELECT {A854:" + PK + "} from {" + _TYPECODE + " AS A854 JOIN DISCOUNT AS discount"
      + " ON {A854:" + DISCOUNT + "} = {discount:" + PK + "}} WHERE {A854:" + CONDKEY + "} =?" + CONDKEY + ""
      + " AND {discount:code} =?" + DISCOUNT + " AND "
      + "{A854:" + STARTTIME + "} <=?" + STARTTIME + " AND {A854:" + ENDTIME + "} >=?" + ENDTIME + "";

    public DefaultTkEuA854DiscountRowDao() {
        super(_TYPECODE);
    }

    @Override
    public List<TkEuConditionRowA854Model> findDiscountRowsByCondKey(String condKey, String sapConditionId) {
        validateParameterNotNull(condKey, "condKey must not be null!");
        validateParameterNotNull(sapConditionId, "sapConditionId must not be null!");
        final Date date = getTruncatedDate();
        final Map<String, Object> params = new HashMap<>();
        params.put(CONDKEY, condKey);
        params.put(DISCOUNT, sapConditionId);
        params.put(STARTTIME, date);
        params.put(ENDTIME, date);
        final SearchResult<TkEuConditionRowA854Model> result = getFlexibleSearchService().search(QUERY_BY_COND_KEY, params);
        return result.getResult();
    }

    @Override
    public Optional<TkEuConditionRowA854Model> findDiscountRowByCondKey(String condKey, String sapConditionId) {
        return findDiscountRowsByCondKey(condKey, sapConditionId).stream().findFirst();
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.order.hook;

import com.thyssenkrupp.b2b.eu.core.service.hooks.CommerceDoAddToCartMethodHook;
import com.thyssenkrupp.b2b.eu.pricedata.service.order.hook.helper.InMemoryCartHelper;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook.TkProductConfigurationHandler;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.ProductConfigurationHandler;
import de.hybris.platform.commerceservices.order.ProductConfigurationHandlerFactory;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

public class TkEuConfigurableProductDoAddToCartMethodHook implements CommerceDoAddToCartMethodHook {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuConfigurableProductDoAddToCartMethodHook.class);

    private ProductConfigurationHandlerFactory configurationFactory;
    private ModelService                       modelService;
    private ConfiguratorSettingsService        configuratorSettingsService;
    private InMemoryCartHelper                 inMemoryCartHelper;

    @Override
    public void beforeDoAddToCart(CommerceCartParameter parameters) {
        // Implementation not needed
    }

    @Override
    public void afterDoAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result) {
        if (result.getQuantityAdded() > 0) {
            ServicesUtil.validateParameterNotNullStandardMessage("parameters", parameters);
            ServicesUtil.validateParameterNotNullStandardMessage("result", result);

            final AbstractOrderEntryModel entry = result.getEntry();
            if (entry == null) {
                LOG.warn("No entry created");
                return;
            } else if (CollectionUtils.isEmpty(entry.getProductInfos()) || containsOnlyCostConfig(entry.getProductInfos())) {
                getConfiguratorSettingsService().getConfiguratorSettingsForProduct(parameters.getProduct())
                    .forEach(config -> createProductInfo(config, entry));
            }

            final Collection<ProductConfigurationItem> productConfiguration = parameters.getProductConfiguration();
            updateOrderEntryWithProductInfo(entry, productConfiguration, tkProductHandlerPredicate(), castIntoTkProductConfigurationHandlerFuntion());

            createCutToLengthProductInfoIfAvailable(entry, productConfiguration);
        }
    }

    protected void createCutToLengthProductInfoIfAvailable(AbstractOrderEntryModel entry, Collection<ProductConfigurationItem> productConfiguration) {
        List<ProductConfigurationItem> cutToLengthConfigurations = emptyIfNull(productConfiguration).stream().filter(productConfigurationItem -> productConfigurationItem.getConfiguratorType() == CUTTOLENGTH_PRODINFO).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(cutToLengthConfigurations)) {
            List<AbstractOrderEntryProductInfoModel> abstractOrderEntryProductInfoModels = getConfigurationFactory().handlerOf(CUTTOLENGTH_PRODINFO).convert(cutToLengthConfigurations, entry);
            abstractOrderEntryProductInfoModels.forEach(abstractOrderEntryProductInfoModel -> abstractOrderEntryProductInfoModel.setOrderEntry(entry));
            abstractOrderEntryProductInfoModels.addAll(emptyIfNull(entry.getProductInfos()));
            entry.setProductInfos(abstractOrderEntryProductInfoModels);
        }
    }

    public void updateOrderEntryWithProductInfo(final AbstractOrderEntryModel entry, final Collection<ProductConfigurationItem> productConfiguration, final Predicate<AbstractOrderEntryProductInfoModel> tkProductHandler, final Function<AbstractOrderEntryProductInfoModel, TkProductConfigurationHandler> tkProductConfigurationHandler) {
        final List<AbstractOrderEntryProductInfoModel> productInfos = entry.getProductInfos();

        if (CollectionUtils.isNotEmpty(productConfiguration) && CollectionUtils.isNotEmpty(productInfos)) {
            productInfos.stream().filter(tkProductHandler).forEach(abstractOrderEntryProductInfoModel -> tkProductConfigurationHandler.apply(abstractOrderEntryProductInfoModel).updateOrderEntryWithProductInfo(productConfiguration, abstractOrderEntryProductInfoModel, entry));
        }
    }

    protected Function<AbstractOrderEntryProductInfoModel, TkProductConfigurationHandler> castIntoTkProductConfigurationHandlerFuntion() {
        return abstractOrderEntryProductInfoModel -> (TkProductConfigurationHandler) getConfigurationFactory().handlerOf(abstractOrderEntryProductInfoModel.getConfiguratorType());
    }

    protected Predicate<AbstractOrderEntryProductInfoModel> tkProductHandlerPredicate() {
        return abstractOrderEntryProductInfoModel -> abstractOrderEntryProductInfoModel != null && getConfigurationFactory().handlerOf(abstractOrderEntryProductInfoModel.getConfiguratorType()) instanceof TkProductConfigurationHandler;
    }

    protected void createProductInfo(final AbstractConfiguratorSettingModel configuration, final AbstractOrderEntryModel entry) {
        if (getInMemoryCartHelper().isInMemoryProductInfoEntry(configuration, entry)) {
            getInMemoryCartHelper().createInMemoryProductInfo(configuration, entry);
        } else {
            final ProductConfigurationHandler productConfigurationHandler = getConfigurationFactory().handlerOf(configuration.getConfiguratorType());
            if (productConfigurationHandler == null) {
                throw new IllegalStateException("No ProductConfigurationHandler registered for configurator type " + configuration.getConfiguratorType());
            }
            List<AbstractOrderEntryProductInfoModel> infos = productConfigurationHandler.createProductInfo(configuration);
            entry.setProductInfos(Stream.concat(
                entry.getProductInfos() == null ? Stream.empty() : entry.getProductInfos().stream(),
                infos.stream().peek(item -> item.setOrderEntry(entry)).peek(getModelService()::save))
                .collect(Collectors.toList()));
        }
    }

    private boolean containsOnlyCostConfig(List<AbstractOrderEntryProductInfoModel> existingInfos) {
        if (existingInfos != null) {
            return !existingInfos.stream().anyMatch(abstractOrderEntryProductInfoModel -> ConfiguratorType.COSTCONFIGURATION != abstractOrderEntryProductInfoModel.getConfiguratorType());
        }
        return false;
    }

    public ProductConfigurationHandlerFactory getConfigurationFactory() {
        return configurationFactory;
    }

    public void setConfigurationFactory(ProductConfigurationHandlerFactory configurationFactory) {
        this.configurationFactory = configurationFactory;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public ConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    public void setConfiguratorSettingsService(ConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public InMemoryCartHelper getInMemoryCartHelper() {
        return inMemoryCartHelper;
    }

    @Required
    public void setInMemoryCartHelper(InMemoryCartHelper inMemoryCartHelper) {
        this.inMemoryCartHelper = inMemoryCartHelper;
    }
}

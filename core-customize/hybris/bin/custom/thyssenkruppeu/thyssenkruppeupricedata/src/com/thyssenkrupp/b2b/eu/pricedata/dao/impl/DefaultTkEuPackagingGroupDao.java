package com.thyssenkrupp.b2b.eu.pricedata.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.pricedata.model.PackagingGroupConfigModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.thyssenkrupp.b2b.eu.pricedata.model.PackagingGroupConfigModel.*;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuPackagingGroupDao {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPackagingGroupDao.class);

    private static final String QUERY_BY_MATERIALGROUP = "select {pk} from {" + _TYPECODE + "} where {" + MATERIALGROUP + "} = ?materialGroup ";

    private FlexibleSearchService flexibleSearchService;

    public List<PackagingGroupConfigModel> findPackagingGroupByMaterialGroup(String materialGroup) {
        validateParameterNotNull(materialGroup, "materialGroup must not be null!");
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<PackagingGroupConfigModel> result;
        params.put(MATERIALGROUP, materialGroup);
        result = getFlexibleSearchService().search(QUERY_BY_MATERIALGROUP, params);
        LOG.debug("Queried table \'PackagingGroupConfig\' for params: materialGroup \'{}\'. Found {} results.", materialGroup, result.getResult().size());
        if(result.getResult().size()>0) {
            PackagingGroupConfigModel packagingGroupConfigModel = result.getResult().get(0);
        LOG.debug("In table \'PackagingGroupConfig\' for params: materialGroup \'{}\'. we have packaging group {}.", materialGroup,packagingGroupConfigModel.getPackagingGroup());  
        }
        
        if(result.getResult().size()==0) {
        LOG.debug("In table \'PackagingGroupConfig\' for params: materialGroup \'{}\'. no packaging group found so no Packaging cost applied.", materialGroup, result.getResult().size());  
        }
        return result.getResult();
    }
    
    
    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}

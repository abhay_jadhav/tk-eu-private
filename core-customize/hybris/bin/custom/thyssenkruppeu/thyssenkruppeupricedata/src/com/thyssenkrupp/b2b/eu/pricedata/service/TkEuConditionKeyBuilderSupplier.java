package com.thyssenkrupp.b2b.eu.pricedata.service;

import java.util.List;
import java.util.function.Supplier;

@FunctionalInterface
public interface TkEuConditionKeyBuilderSupplier extends Supplier<List<TkEuPriceConditionKeyBuilderFactory>> {
}

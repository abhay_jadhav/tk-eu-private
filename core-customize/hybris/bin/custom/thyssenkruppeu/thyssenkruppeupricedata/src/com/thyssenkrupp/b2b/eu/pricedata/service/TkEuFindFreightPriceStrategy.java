package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import de.hybris.platform.util.DiscountValue;

public interface TkEuFindFreightPriceStrategy {

    DiscountValue getFreightPrice(TkEuPriceParameter priceParameter);
}

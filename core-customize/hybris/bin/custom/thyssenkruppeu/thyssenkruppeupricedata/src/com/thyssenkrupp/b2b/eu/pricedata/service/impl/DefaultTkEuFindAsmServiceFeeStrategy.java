package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.dao.TkEuDefaultConditionRowDao;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindAsmServiceFeeStrategy;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.util.DiscountValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.ASM_SERVICE_FEE;
import static com.thyssenkrupp.b2b.eu.pricedata.utils.TkEuPriceUtils.convertToNegativeValue;
import static de.hybris.platform.util.DiscountValue.createAbsolute;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

public class DefaultTkEuFindAsmServiceFeeStrategy implements TkEuFindAsmServiceFeeStrategy {

    public static final String ASM_SERVICE_FEE_COND_KEY = "asm-surcharge";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFindAsmServiceFeeStrategy.class);

    private AssistedServiceService     assistedServiceService;
    private TkEuDefaultConditionRowDao defaultConditionRowDao;

    @Override
    public Optional<DiscountValue> getAsmServiceFee(@NotNull AbstractOrderModel orderModel) {
        if (getAssistedServiceService().getAsmSession() != null) {
            List<AbstractOrderEntryModel> entries = emptyIfNull(orderModel.getEntries());
            double discountValue = findAsmServiceFee(entries);
            return Optional.of(createAbsolute(ASM_SERVICE_FEE, discountValue, orderModel.getCurrency().getIsocode()));
        }
        return Optional.empty();
    }

    protected double findAsmServiceFee(@NotNull List<AbstractOrderEntryModel> entries) {
        AbstractOrderEntryModel abstractOrderEntryModel = entries.get(0);
        if (abstractOrderEntryModel != null) {
            List<PriceRowModel> priceRowByCondKey = getDefaultConditionRowDao().findPriceRowByCondKey(ASM_SERVICE_FEE_COND_KEY, abstractOrderEntryModel.getProduct().getCatalogVersion());
            Optional<PriceRowModel> priceRowModel = priceRowByCondKey.stream().findFirst();
            if (priceRowModel.isPresent()) {
                LOG.debug("ASM service Fee applied with a Cost : " + priceRowModel.get().getPrice());
                return convertToNegativeValue(priceRowModel.get().getPrice());
            }
        }
        return 0;
    }

    public AssistedServiceService getAssistedServiceService() {
        return assistedServiceService;
    }

    @Required
    public void setAssistedServiceService(AssistedServiceService assistedServiceService) {
        this.assistedServiceService = assistedServiceService;
    }

    public TkEuDefaultConditionRowDao getDefaultConditionRowDao() {
        return defaultConditionRowDao;
    }

    @Required
    public void setDefaultConditionRowDao(TkEuDefaultConditionRowDao defaultConditionRowDao) {
        this.defaultConditionRowDao = defaultConditionRowDao;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.populators.CertificateConfigurationsPopulator;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.util.List;

/*
 * Certificate Price Populator is used only for PDP hence making sure OrderEntry is Null
 * And In Order we use discounts for showing the Certificate Price
 */
public class TkEuProductCertificatePricePopulator<T extends AbstractOrderEntryProductInfoModel> extends CertificateConfigurationsPopulator<T> {

    private TkEuPriceService tkEuPriceService;
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(T source, List<ConfigurationInfoData> target) throws ConversionException {
        super.populate(source, target);
        if (source.getConfiguratorType() == ConfiguratorType.CERTIFICATE) {
            final CertificateConfiguredProductInfoModel model = (CertificateConfiguredProductInfoModel) source;
            getTkEuPriceService().getCertificatePriceForProduct(model).ifPresent(priceInformation -> {
                  final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(Math.abs(priceInformation.getValue())), priceInformation.getCurrencyIsoCode());
                  target.stream().forEach(configurationInfoData -> {
                      if (configurationInfoData.getUniqueId().equals(model.getCertificateId())) {
                          configurationInfoData.setPriceData(priceData);
                      }
                  });
              }
            );
        }
    }

    public TkEuPriceService getTkEuPriceService() {
        return tkEuPriceService;
    }

    @Required
    public void setTkEuPriceService(TkEuPriceService tkEuPriceService) {
        this.tkEuPriceService = tkEuPriceService;
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}

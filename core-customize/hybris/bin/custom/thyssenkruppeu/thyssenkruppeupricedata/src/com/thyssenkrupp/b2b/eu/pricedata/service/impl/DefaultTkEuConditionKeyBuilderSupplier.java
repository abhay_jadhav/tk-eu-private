package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.service.ConditionKeyTypes;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuConditionKeyBuilderSupplier;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultTkEuConditionKeyBuilderSupplier {

    private List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories;

    public TkEuConditionKeyBuilderSupplier getSawingCostKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.YBSA.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getScalePriceKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.Y2ZP.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getHandlingCostKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.YFYA.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getFreightCostKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.YZZF.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getCertificateCostKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.YWZF.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getPACKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.Y2MA.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }

    public TkEuConditionKeyBuilderSupplier getPackagingCostKeyBuilderSupplier() {
        return () -> getTkEuPriceConditionKeyBuilderFactories().stream().filter(tkEuPriceConditionKeyBuilderFactory -> ConditionKeyTypes.YPPF.equals(tkEuPriceConditionKeyBuilderFactory.getConditionKeyType())).collect(Collectors.toList());
    }
    
    public List<TkEuPriceConditionKeyBuilderFactory> getTkEuPriceConditionKeyBuilderFactories() {
        return tkEuPriceConditionKeyBuilderFactories;
    }

    @Required
    public void setTkEuPriceConditionKeyBuilderFactories(List<TkEuPriceConditionKeyBuilderFactory> tkEuPriceConditionKeyBuilderFactories) {
        this.tkEuPriceConditionKeyBuilderFactories = tkEuPriceConditionKeyBuilderFactories;
    }
}

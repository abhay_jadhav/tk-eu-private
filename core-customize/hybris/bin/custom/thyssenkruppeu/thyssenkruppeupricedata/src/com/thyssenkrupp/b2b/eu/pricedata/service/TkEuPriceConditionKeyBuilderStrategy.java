package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.eu.pricedata.service.impl.TkEuPriceConditionKeyBuilderComponents;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface TkEuPriceConditionKeyBuilderStrategy {

    List<String> apply(@NotNull Map<Integer, List<TkEuPriceConditionKeyBuilderComponents>> factors,
            TkEuPriceParameter priceParameter);
}

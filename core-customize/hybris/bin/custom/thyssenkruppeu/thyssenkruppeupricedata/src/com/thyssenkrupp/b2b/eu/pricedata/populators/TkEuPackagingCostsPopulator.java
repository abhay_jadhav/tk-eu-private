package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.collections4.ListUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class TkEuPackagingCostsPopulator implements Populator<AbstractOrderModel, AbstractOrderData> {
    private static final Logger LOG = Logger.getLogger(TkEuPackagingCostsPopulator.class);
    private PriceDataFactory priceDataFactory;
    
    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        Optional<DiscountValue> packagingCosts = findPackagingCosts(ListUtils.emptyIfNull(source.getGlobalDiscountValues()));
        if (packagingCosts.isPresent()) {
            double packagingValue = Math.abs(packagingCosts.get().getValue());
            target.setPackagingCosts(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(packagingValue), packagingCosts.get().getCurrencyIsoCode()));
            LOG.debug("In TkEuPackagingCostsPopulator packaigng cost set in orderEntrtData is = "+packagingValue);
        }
    }
    
    private Optional<DiscountValue> findPackagingCosts(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> ThyssenkruppeupricedataConstants.PACKAGING_COSTS.equals(discountValue.getCode())).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuSapGlobalConfigurationService;
import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuUomConversionException;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindAsmServiceFeeStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindMinimumOrderSurchargeStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindSawingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuScalePriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuExternalPricingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindCertificatePriceStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindFreightPriceStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindHandlingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuFindPackagingCostStrategy;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuUomConversionService;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;
import com.thyssenkrupp.b2b.eu.prospricing.service.TkEuProsPricingService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPacPriceService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPacValidatorService;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuMergePacZilliantPrices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.*;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.util.DiscountValue.createAbsolute;
import static java.util.Optional.empty;

public class DefaultTkEuPriceService extends AbstractBusinessService implements PriceService, TkEuPriceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPriceService.class);

    private UserService                         userService;
    private UnitService                         unitService;
    private CommonI18NService                   commonI18nService;
    private TkEuScalePriceService               tkEuScalePriceService;
    private TkEuUomConversionService            tkEuUomConversionService;
    private TkEuExternalPricingService          tkEuExternalPricingService;
    private TkEuFindFreightPriceStrategy        findFreightPriceStrategy;
    private TkEuFindHandlingCostStrategy        findHandlingCostStrategy;
    private TkEuFindCertificatePriceStrategy      findCertificatePriceStrategy;
    private TkEuFindSawingCostStrategy            findSawingCostStrategy;
    private TkEuFindPackagingCostStrategy         findPackagingCostStrategy;
    private TkEuFindAsmServiceFeeStrategy         findAsmServiceFeeStrategy;
    private TkEuSapGlobalConfigurationService     tkEuSapGlobalConfigurationService;
    private DefaultB2BCommerceUnitService         defaultB2BCommerceUnitService;
    private CostConfigurationProductInfoService   costConfigurationProductInfoService;
    private TkEuFindMinimumOrderSurchargeStrategy findMinimumOrderSurchargeStrategy;
    private TkEuPacPriceService                tkEuPacPriceService;
    private TkEuPacValidatorService            tkEuPacValidatorService;
    private TkEuMergePacZilliantPrices         tkEuMergePacZilliantPrices;
    private TkEuProsPricingService             tkEuProsPricingService;

    @Override
    public List<PriceInformation> getPriceInformationsForProduct(final ProductModel product) {
        final UserModel currentUser = userService.getCurrentUser();
        if (!userService.isAnonymousUser(currentUser) && currentUser instanceof B2BCustomerModel) {
            try {
                return fetchScalePriceForCustomer(product);
            } catch (TkEuPriceServiceUnKnownStateException | TkEuUomConversionException | TkEuPriceServiceException ex) {
                LOG.error("Scale Price can not be fetched with following reason", ex);
            }
        }
        return new ArrayList();
    }

    private List<PriceInformation> fetchScalePriceForCustomer(ProductModel product) throws TkEuPriceServiceException {
        B2BUnitModel defaultUnit = getCurrentB2bUnit().orElse(null);
        if (defaultUnit != null && product.getBaseUnit() != null) {
            return fetchScales(product,defaultUnit);
        }
        throw new TkEuPriceServiceUnKnownStateException(String.format("Missing Either Customer default B2BUnit \'%s\' (or) Product baseUnit(UOM) \'%s\'", defaultUnit, product.getBaseUnit()));
    }

    //new pricing logic to display PAC and zilliant scales on pdp
    private List<PriceInformation> fetchScales(ProductModel product, B2BUnitModel defaultUnit) throws TkEuPriceServiceException{
        TkEuPriceParameter priceParameter = buildPriceParameter(product, getCurrentB2bUnit().orElse(null), commonI18nService.getCurrentCurrency().getIsocode(), buildMaterialQuantity(1, product.getBaseUnit()), product.getCode());
        List<PriceInformation> pacPriceInfo= getTkEuPacPriceService().getPACValuesForCustomer(priceParameter);
        Boolean zilliantFlag= checkZilliantFallback();
        if(!pacPriceInfo.isEmpty()) {
            if(getTkEuPacValidatorService().pacWithOutScalesWithNonZeroConditionValue(pacPriceInfo)) {
                pacPriceInfo.remove(0);
                return pacPriceInfo;
            } else if(getTkEuPacValidatorService().pacWithOutScalesWithZeroConditionValue(pacPriceInfo) && zilliantFlag) {
                 return getTkEuExternalPricingService().getPriceInformationsForProduct(product, defaultUnit);
            } else if(zilliantFlag && (getTkEuPacValidatorService().checkIfPacScaleValueIsZero(pacPriceInfo) || !getTkEuPacValidatorService().checkIfPacScaleStartsWithZero(pacPriceInfo))){  //add check
                return mergeScales(pacPriceInfo, product, defaultUnit);
                } else return pacPriceInfo;
        } else if(zilliantFlag){
            return getTkEuExternalPricingService().getPriceInformationsForProduct(product, defaultUnit); //pac is empty and zilliant fallback is on
        }
        return Collections.emptyList();//Pac is empty and zilliant fallback is off
    }

    public boolean checkZilliantFallback() {
        if(TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeupricedataConstants.ZILLIANT_FALLBACK_ENABLED, true) &&
                !TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeupricedataConstants.PROS_PRICING_ENABLED, false)){
                    return true;
                }
        return false;
    }

    private List<PriceInformation> mergeScales(List<PriceInformation> pacPriceInfo, ProductModel product, B2BUnitModel defaultUnit) throws TkEuPriceServiceException {
        List<PriceInformation> zilPriceInfo= getTkEuExternalPricingService().getPriceInformationsForProduct(product, defaultUnit);
            if(!zilPriceInfo.isEmpty() && getTkEuPacValidatorService().checkPacAndZilScaleUnit(pacPriceInfo.get(0), zilPriceInfo.get(0))) {
                return getTkEuMergePacZilliantPrices().mergePacZillPriceInfos(pacPriceInfo,zilPriceInfo);
            }else
                return pacPriceInfo;
    }

    @Override
    public List<DiscountValue> getDiscountValuesForOrderEntry(AbstractOrderEntryModel entryModel, UserModel currentUser, List<DiscountValue> discounts) {
        List<AbstractOrderEntryProductInfoModel> productInfos = createProductInfoList(entryModel.getProductInfos());
        for (AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel : productInfos) {
            if (abstractOrderEntryProductInfoModel instanceof CertificateConfiguredProductInfoModel && ((CertificateConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).isChecked()) {
                addCertificateCost(discounts, (CertificateConfiguredProductInfoModel) abstractOrderEntryProductInfoModel);
            } else if (abstractOrderEntryProductInfoModel instanceof TkCutToLengthConfiguredProductInfoModel && BooleanUtils.isTrue(((TkCutToLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).getChecked())) {
                addSawingCost(discounts, entryModel, (TkCutToLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel);
            }
        }
        // calculate handling cost at position level
        if (!(entryModel.getOrder() instanceof InMemoryCartModel)) {
            addHandlingCost(discounts, entryModel);
        }
        return discounts;
    }

    private void addSawingCost(List<DiscountValue> discounts, AbstractOrderEntryModel entryModel, TkCutToLengthConfiguredProductInfoModel abstractOrderEntryProductInfoModel) {
        TkCutToLengthConfiguredProductInfoModel productInfo = abstractOrderEntryProductInfoModel;
        Optional<DiscountValue> sawingCost = getSawingCostForOrderEntry(productInfo, entryModel.getProduct(), buildMaterialQuantity(entryModel.getQuantity().doubleValue(), entryModel.getUnit()));
        sawingCost.map(priceInformation -> discounts.add(priceInformation));
    }

    private void addCertificateCost(List<DiscountValue> discounts, CertificateConfiguredProductInfoModel abstractOrderEntryProductInfoModel) {
        CertificateConfiguredProductInfoModel productInfo = abstractOrderEntryProductInfoModel;
        Optional<DiscountValue> certificatePrice = getCertificatePriceForProduct(productInfo);
        certificatePrice.map(priceInformation -> discounts.add(priceInformation));
    }

    private Double getPackagingCostOnEntryLevel(AbstractOrderEntryModel entryModel,String currencyCode) {


        TkEuPriceParameter priceParameter = buildPriceParameter(entryModel.getProduct(), getCurrentB2bUnit().orElse(null), currencyCode, buildMaterialQuantity(), "", entryModel);
     return getPackagingCost(entryModel,priceParameter);
    }

    // method added to find handling cost at position level
    private void addHandlingCost(List<DiscountValue> discounts, AbstractOrderEntryModel entryModel) {
        try {
            String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
            TkEuPriceParameter priceParameter = buildPriceParameter(entryModel.getProduct(), getCurrentB2bUnit().orElse(null), currencyCode, buildMaterialQuantity(), "", entryModel);
            DiscountValue handlingCost = getFindHandlingCostStrategy().getHandlingCost(entryModel, priceParameter);
            LOG.info("Total handling cost for entry " + entryModel.getOrder().getCode() + entryModel.getProduct().getCode() + "is" + handlingCost.getValue());
            discounts.add(handlingCost);
        } catch (Exception e) {
            LOG.error("Error in calculating handling cost for entry" + entryModel.getOrder().getCode() + entryModel.getProduct().getCode() + e.getMessage());
        }
    }

    @Override
    public List<DiscountValue> getDiscountValuesForOrder(AbstractOrderModel orderModel, UserModel currentUser, List<DiscountValue> discounts) {
        if (!(orderModel instanceof InMemoryCartModel)) {
            getAsmServiceFee(orderModel).map(discountValue -> discounts.add(discountValue));
            getMinimumOrderValueSurcharge(orderModel).map(discountValue -> discounts.add(discountValue));

            /* PackagingCost code for condition keys and discount values and add maximum discount value among all*/
            getPackagingCostOnOrderLevel(orderModel).map(discountValue -> discounts.add(discountValue));
        }
        return discounts;
    }

    private Optional <DiscountValue> getPackagingCostOnOrderLevel(AbstractOrderModel orderModel) {
        List<Double> discountValueList = new ArrayList<>();
        String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
        for (AbstractOrderEntryModel entryModel : orderModel.getEntries()) {

           Double discountEntryLevel=getPackagingCostOnEntryLevel(entryModel, currencyCode);
            discountValueList.add(discountEntryLevel);
        }

        LOG.info("List of all discount values with their condition keys= {}", discountValueList);

        /*
         * maxDiscountValue is max value among all discountValues, since all values are in
         * negative '-' we are using "Collections.min()" function
         */
        Double maxDiscountValue = Collections.min(discountValueList);

        LOG.info("Maximum Discount Value among all order entries is = {}", maxDiscountValue);

        return Optional.of(createAbsolute(PACKAGING_COSTS, maxDiscountValue, currencyCode));
    }

    private List<AbstractOrderEntryProductInfoModel> createProductInfoList(List<AbstractOrderEntryProductInfoModel> existingInfos) {
        return new ArrayList<>(existingInfos);
    }

    private Double getPackagingCost(AbstractOrderEntryModel orderEntryModel, TkEuPriceParameter priceParameter) {
        if (orderEntryModel != null) {
            String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
           return getFindPackagingCostStrategy().getPackagingCost(orderEntryModel,currencyCode,priceParameter);
        }
        return 0d;
    }

    @Override
    public Optional<DiscountValue> getAsmServiceFee(AbstractOrderModel orderModel) {
        if (orderModel != null) {
            return getFindAsmServiceFeeStrategy().getAsmServiceFee(orderModel);
        }
        return empty();
    }

    public Optional<DiscountValue> getMinimumOrderValueSurcharge(AbstractOrderModel orderModel) {
        if (orderModel != null) {
            return getFindMinimumOrderSurchargeStrategy().getMinimumOrderValueSurcharge(orderModel);
        }
        return empty();
    }

    @Override
    public Optional<DiscountValue> getCertificatePriceForProduct(CertificateConfiguredProductInfoModel abstractOrderEntryProductInfoModel) {
        validateParameterNotNull(abstractOrderEntryProductInfoModel, "CertificateConfiguredProductInfoModel cannot be null");
        String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
        return getCurrentB2bUnit().map(b2bUnit -> getFindCertificatePriceStrategy().getCertificatePrice(buildPriceParameter(new ProductModel(), b2bUnit, currencyCode, buildMaterialQuantity(), abstractOrderEntryProductInfoModel.getCertificateId())));
    }

    @Override
    public Optional<DiscountValue> getSawingCostForOrderEntry(TkCutToLengthConfiguredProductInfoModel abstractOrderEntryProductInfoModel, ProductModel product, MaterialQuantityWrapper materialQuantityWrapper) {
        validateParameterNotNull(abstractOrderEntryProductInfoModel, "TkCutToLengthConfiguredProductInfoModel cannot be null");
        String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
        return getCurrentB2bUnit().map(b2bUnit -> getFindSawingCostStrategy().getSawingCost(buildPriceParameter(product, b2bUnit, currencyCode, materialQuantityWrapper, SAWING_VARIANT_ID)));
    }

    @Override
    public Optional<DiscountValue> getFreightPriceForOrder(AbstractOrderModel orderModel) {
        return getCurrentB2bUnit().map(b2bUnit -> getFindFreightPriceStrategy().getFreightPrice(buildPriceParameter(new ProductModel(), b2bUnit, orderModel.getCurrency().getIsocode(), buildMaterialQuantity(), "")));
    }

    @Override
    public Optional<PriceInformation> getProcessingOptionPriceForProduct(ProductModel product) {
        validateParameterNotNull(product, "Product model cannot be null");
        String currencyCode = commonI18nService.getCurrentCurrency().getIsocode();
        return getCurrentB2bUnit().map(b2bUnit -> calculateProcessingOptionPriceForCustomer(buildPriceParameter(product, b2bUnit, currencyCode, buildMaterialQuantity(), "")));
    }

    private Optional<B2BUnitModel> getCurrentB2bUnit() {
        final UserModel currentUser = userService.getCurrentUser();
        if (currentUser != null && !userService.isAnonymousUser(currentUser) && currentUser instanceof B2BCustomerModel) {
            B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) currentUser;
            B2BUnitModel defaultB2BUnit = b2BCustomerModel.getDefaultB2BUnit();
            if (defaultB2BUnit == null) {
                defaultB2BUnit = getDefaultB2BCommerceUnitService().getRootUnit();
            }
            return Optional.ofNullable(defaultB2BUnit);
        }
        return empty();
    }

    @Override
    public PriceInformation calculateProcessingOptionPriceForCustomer(TkEuPriceParameter priceParameter) {
        // TODO : Implement Price using strategy if required
        return new PriceInformation(new PriceValue(priceParameter.getCurrencyCode(), Math.abs(0), false));
    }

    private TkEuPriceParameter buildPriceParameter(final ProductModel productModel, final B2BUnitModel b2BUnitModel, final String currencyCode, final MaterialQuantityWrapper materialQuantity, String variantId) {
        TkEuPriceParameter priceParameter = new TkEuPriceParameter();
        priceParameter.setCurrencyCode(currencyCode);
        priceParameter.setB2bUnit(b2BUnitModel);
        priceParameter.setProduct(productModel);
        priceParameter.setQuantityWrapper(materialQuantity);
        priceParameter.setVariantId(variantId);
        priceParameter.setChildB2bUnit(getDefaultB2BCommerceUnitService().getParentUnit());
        return priceParameter;
    }

    private TkEuPriceParameter buildPriceParameter(final ProductModel productModel, final B2BUnitModel b2BUnitModel, final String currencyCode, final MaterialQuantityWrapper materialQuantity, String variantId, AbstractOrderEntryModel orderEntry) {
        TkEuPriceParameter priceParameter = new TkEuPriceParameter();
        priceParameter.setCurrencyCode(currencyCode);
        priceParameter.setB2bUnit(b2BUnitModel);
        priceParameter.setProduct(productModel);
        priceParameter.setQuantityWrapper(materialQuantity);
        priceParameter.setVariantId(variantId);
        priceParameter.setChildB2bUnit(getDefaultB2BCommerceUnitService().getParentUnit());
        priceParameter.setOrderEntry(orderEntry);
        return priceParameter;
    }

    private MaterialQuantityWrapper buildMaterialQuantity() {
        return buildMaterialQuantity(1.0, new UnitModel());
    }

    private MaterialQuantityWrapper buildMaterialQuantity(double qty, UnitModel unitModel) {
        return MaterialQuantityWrapperBuilder.builder().withQuantity(qty).withUnit(unitModel).build();
    }

    public CostConfigurationProductInfoService getCostConfigurationProductInfoService() {
        return costConfigurationProductInfoService;
    }

    @Required
    public void setCostConfigurationProductInfoService(CostConfigurationProductInfoService costConfigurationProductInfoService) {
        this.costConfigurationProductInfoService = costConfigurationProductInfoService;
    }

    public TkEuSapGlobalConfigurationService getTkEuSapGlobalConfigurationService() {
        return tkEuSapGlobalConfigurationService;
    }

    @Required
    public void setTkEuSapGlobalConfigurationService(TkEuSapGlobalConfigurationService tkEuSapGlobalConfigurationService) {
        this.tkEuSapGlobalConfigurationService = tkEuSapGlobalConfigurationService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public TkEuExternalPricingService getTkEuExternalPricingService() {
        return tkEuExternalPricingService;
    }

    @Required
    public void setTkEuExternalPricingService(TkEuExternalPricingService tkEuExternalPricingService) {
        this.tkEuExternalPricingService = tkEuExternalPricingService;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    @Required
    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public TkEuScalePriceService getTkEuScalePriceService() {
        return tkEuScalePriceService;
    }

    @Required
    public void setTkEuScalePriceService(TkEuScalePriceService tkEuScalePriceService) {
        this.tkEuScalePriceService = tkEuScalePriceService;
    }

    public TkEuUomConversionService getTkEuUomConversionService() {
        return tkEuUomConversionService;
    }

    @Required
    public void setTkEuUomConversionService(TkEuUomConversionService tkEuUomConversionService) {
        this.tkEuUomConversionService = tkEuUomConversionService;
    }

    public TkEuFindCertificatePriceStrategy getFindCertificatePriceStrategy() {
        return findCertificatePriceStrategy;
    }

    public TkEuFindSawingCostStrategy getFindSawingCostStrategy() {
        return findSawingCostStrategy;
    }

    @Required
    public void setFindSawingCostStrategy(TkEuFindSawingCostStrategy findSawingCostStrategy) {
        this.findSawingCostStrategy = findSawingCostStrategy;
    }

    @Required
    public void setFindCertificatePriceStrategy(TkEuFindCertificatePriceStrategy findCertificatePriceStrategy) {
        this.findCertificatePriceStrategy = findCertificatePriceStrategy;
    }

    public TkEuFindFreightPriceStrategy getFindFreightPriceStrategy() {
        return findFreightPriceStrategy;
    }

    @Required
    public void setFindFreightPriceStrategy(TkEuFindFreightPriceStrategy findFreightPriceStrategy) {
        this.findFreightPriceStrategy = findFreightPriceStrategy;
    }

    @Required
    public void setFindHandlingCostStrategy(TkEuFindHandlingCostStrategy findHandlingCostStrategy) {
        this.findHandlingCostStrategy = findHandlingCostStrategy;
    }

    public TkEuFindPackagingCostStrategy getFindPackagingCostStrategy() {
        return findPackagingCostStrategy;
    }

    public TkEuFindHandlingCostStrategy getFindHandlingCostStrategy() {
        return findHandlingCostStrategy;
    }

    @Required
    public void setFindPackagingCostStrategy(TkEuFindPackagingCostStrategy findPackagingCostStrategy) {
        this.findPackagingCostStrategy = findPackagingCostStrategy;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public TkEuFindAsmServiceFeeStrategy getFindAsmServiceFeeStrategy() {
        return findAsmServiceFeeStrategy;
    }

    @Required
    public void setFindAsmServiceFeeStrategy(TkEuFindAsmServiceFeeStrategy findAsmServiceFeeStrategy) {
        this.findAsmServiceFeeStrategy = findAsmServiceFeeStrategy;
    }

    public TkEuFindMinimumOrderSurchargeStrategy getFindMinimumOrderSurchargeStrategy() {
        return findMinimumOrderSurchargeStrategy;
    }

    public void setFindMinimumOrderSurchargeStrategy(TkEuFindMinimumOrderSurchargeStrategy findMinimumOrderSurchargeStrategy) {
        this.findMinimumOrderSurchargeStrategy = findMinimumOrderSurchargeStrategy;
    }

    public TkEuPacPriceService getTkEuPacPriceService() {
        return tkEuPacPriceService;
    }

    @Required
    public void setTkEuPacPriceService(TkEuPacPriceService tkEuPacPriceService) {
        this.tkEuPacPriceService = tkEuPacPriceService;
    }

    public TkEuPacValidatorService getTkEuPacValidatorService() {
        return tkEuPacValidatorService;
    }

    @Required
    public void setTkEuPacValidatorService(TkEuPacValidatorService tkEuPacValidatorService) {
        this.tkEuPacValidatorService = tkEuPacValidatorService;
    }

    public TkEuMergePacZilliantPrices getTkEuMergePacZilliantPrices() {
        return tkEuMergePacZilliantPrices;
    }

    @Required
    public void setTkEuMergePacZilliantPrices(TkEuMergePacZilliantPrices tkEuMergePacZilliantPrices) {
        this.tkEuMergePacZilliantPrices = tkEuMergePacZilliantPrices;
    }

    @Override
    public PriceValue calculateProsBasePriceForOrderEntry(TkEuPriceParameter tkEuPriceParameter,
            MaterialQuantityWrapper quantityWrapper) {
        try {
            Optional<TkEuProsPriceReponseParameter> prosPriceResponse = tkEuProsPricingService
                    .getProsPriceForOrderEntry(tkEuPriceParameter, quantityWrapper);
            if (prosPriceResponse.isPresent() && StringUtils.isEmpty(prosPriceResponse.get().getErrorMessage())) {
                double prosPrice = prosPriceResponse.get().getTargetPrice().doubleValue();
                Long priceFactor = prosPriceResponse.get().getPriceFactor();
                if (prosPrice > 0.0d && null != tkEuPriceParameter.getOrderEntry()) {
                    AbstractOrderEntryModel orderEntry = tkEuPriceParameter.getOrderEntry();
                    CostConfigurationProductInfoModel pacCostConfigurationProductInfo = getCostConfigurationProductInfoService()
                            .createProsCostConfigurationProductInfo(orderEntry,
                                    getCommonI18nService().getCurrentCurrency(), priceFactor.intValue(),
                                    tkEuPriceParameter.getProduct().getBaseUnit(), prosPrice);
                    getCostConfigurationProductInfoService().updateCostConfigurationInOrderEntry(orderEntry,
                            pacCostConfigurationProductInfo);
                    LOG.debug("PROS Price for config object= " + prosPrice);
                }

                double prodPricePerBaseUnit = prosPrice / priceFactor.doubleValue();
                return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), prodPricePerBaseUnit,
                        true);
            }
        } catch (Exception e) {
            LOG.error("Excpetion in calculateProsBasePriceForOrderEntry ", e);
        }
        return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), 0.0d, true);
    }

    public TkEuProsPricingService getTkEuProsPricingService() {
        return tkEuProsPricingService;
    }

    public void setTkEuProsPricingService(TkEuProsPricingService tkEuProsPricingService) {
        this.tkEuProsPricingService = tkEuProsPricingService;
    }
}

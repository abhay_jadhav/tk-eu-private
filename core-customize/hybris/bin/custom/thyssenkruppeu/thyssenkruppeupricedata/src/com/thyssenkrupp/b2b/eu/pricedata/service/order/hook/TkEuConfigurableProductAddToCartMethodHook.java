package com.thyssenkrupp.b2b.eu.pricedata.service.order.hook;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.service.order.hook.TkConfigurableProductAddToCartMethodHook;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public class TkEuConfigurableProductAddToCartMethodHook extends TkConfigurableProductAddToCartMethodHook {

    /**
     * Product Configurations Are Copied Over CommerceDoAddToCartMethodHook Hence This Implementation is hidden for future use cases
     *
     * @see TkEuConfigurableProductDoAddToCartMethodHook#afterDoAddToCart(CommerceCartParameter, CommerceCartModification)
     */
    @Override
    public void afterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result) throws CommerceCartModificationException {
        // Implementation not needed
    }
}

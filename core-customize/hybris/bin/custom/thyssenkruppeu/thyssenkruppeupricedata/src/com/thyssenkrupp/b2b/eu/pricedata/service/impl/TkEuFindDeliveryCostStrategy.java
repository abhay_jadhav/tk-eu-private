package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;

public class TkEuFindDeliveryCostStrategy extends AbstractBusinessService implements FindDeliveryCostStrategy {

    private TkEuPriceService tkEuPriceService;

    @Override
    public PriceValue getDeliveryCost(AbstractOrderModel order) {
        if (CollectionUtils.isNotEmpty(order.getEntries())) {
            Optional<DiscountValue> freightPriceForOrder = getTkEuPriceService().getFreightPriceForOrder(order);
            if (freightPriceForOrder.isPresent()) {
                double deliveryCost = freightPriceForOrder.get().getValue();

                return new PriceValue(order.getCurrency().getIsocode(), Math.abs(deliveryCost), order.getNet());
            }
        }
        return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet());
    }

    public TkEuPriceService getTkEuPriceService() {
        return tkEuPriceService;
    }

    @Required
    public void setTkEuPriceService(TkEuPriceService tkEuPriceService) {
        this.tkEuPriceService = tkEuPriceService;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.model.order.inmemory.InMemoryCostConfigurationProductInfoModel;
import com.thyssenkrupp.b2b.eu.pricedata.service.CostConfigurationProductInfoService;
import com.thyssenkrupp.b2b.eu.pricedata.service.order.hook.helper.InMemoryCartHelper;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.ZILLIANT;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.HANDLING_COSTS;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.PAC;
import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.PROS;

import static com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants.CUSTOMER_SPECIFIC_PRICE;
import static de.hybris.platform.catalog.enums.ConfiguratorType.COSTCONFIGURATION;

public class DefaultCostConfigurationProductInfoService implements CostConfigurationProductInfoService {

    private ModelService modelService;
    private InMemoryCartHelper inMemoryCartHelper;

    @Override
    public CostConfigurationProductInfoModel createHandlingCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value, String handlingType) {
        return createCostConfigurationProductInfoModel(entry, currency, quantity, unit, value, handlingType);
    }

    @Override
    public CostConfigurationProductInfoModel createZilliantCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value) {
        return createCostConfigurationProductInfoModel(entry, currency, quantity, unit, value, ZILLIANT);
    }

    @Override
    public CostConfigurationProductInfoModel createPacCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, double quantity, UnitModel unit, double value) {
        return createCostConfigurationProductInfoModel(entry, currency, (long) quantity, unit, value, PAC);
    }

    @Override
    public CostConfigurationProductInfoModel createProsCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, double quantity, UnitModel unit, double value) {
        return createCostConfigurationProductInfoModel(entry, currency, (long) quantity, unit, value, PROS);
    }

    @Override
    public CostConfigurationProductInfoModel createZilCuspriCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value) {
        return createCostConfigurationProductInfoModel(entry, currency, quantity, unit, value, CUSTOMER_SPECIFIC_PRICE);
    }

    @Override
    public void updateCostConfigurationInOrderEntry(@NotNull AbstractOrderEntryModel entry, @NotNull CostConfigurationProductInfoModel costConfigurationProductInfoModel) {
        List<AbstractOrderEntryProductInfoModel> productInfos = entry.getProductInfos() != null ? new ArrayList<>(entry.getProductInfos()) : new ArrayList<>();
        Predicate<AbstractOrderEntryProductInfoModel> costConfigPredicate = abstractOrderEntryProductInfoModel -> {
            if (COSTCONFIGURATION == abstractOrderEntryProductInfoModel.getConfiguratorType()) {
                CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel = (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel;
                if(StringUtils.equals(costConfigurationProductInfoModel.getLabel(), targetCostConfigurationProductInfoModel.getLabel())) {
                    return true;
                }else if(costConfigurationProductInfoModel.getLabel().startsWith(HANDLING_COSTS) && (targetCostConfigurationProductInfoModel.getLabel().startsWith(HANDLING_COSTS)))
                    return true;
                else return checkCostConfigurationLabelForPrice(costConfigurationProductInfoModel, targetCostConfigurationProductInfoModel);
            }
            return false;
        };
        getModelService().save(costConfigurationProductInfoModel);
        final List<AbstractOrderEntryProductInfoModel> oldCostConfigurations = productInfos.stream().filter(costConfigPredicate).collect(Collectors.toList());
        productInfos.removeIf(costConfigPredicate);
        getModelService().removeAll(oldCostConfigurations);
        productInfos.add(costConfigurationProductInfoModel);
        entry.setProductInfos(productInfos);
    }

    public boolean checkCostConfigurationLabelForPrice(CostConfigurationProductInfoModel costConfigurationProductInfoModel, CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel) {
        if(costConfigurationProductInfoModel.getLabel().startsWith(PAC) && (targetCostConfigurationProductInfoModel.getLabel().startsWith(ZILLIANT)))
            return true;
        else if(costConfigurationProductInfoModel.getLabel().startsWith(ZILLIANT) && (targetCostConfigurationProductInfoModel.getLabel().startsWith(PAC)))
            return true;
        else if(costConfigurationProductInfoModel.getLabel().startsWith(PAC) && (targetCostConfigurationProductInfoModel.getLabel().startsWith(PROS)))
            return true;
        else if(costConfigurationProductInfoModel.getLabel().startsWith(PROS) && (targetCostConfigurationProductInfoModel.getLabel().startsWith(PAC)))
            return true;
        return false;
    }

    @Override
    public void removeZilCuspriCostConfigurationProductInfo(@NotNull AbstractOrderEntryModel entry) {
        List<AbstractOrderEntryProductInfoModel> productInfos = entry.getProductInfos();
        if (Objects.nonNull(productInfos)) {
            for (Iterator<AbstractOrderEntryProductInfoModel> productInfoIterator = productInfos.iterator(); productInfoIterator.hasNext();) {
                AbstractOrderEntryProductInfoModel productInfo = productInfoIterator.next();
                if (Objects.nonNull(productInfo) && (productInfo instanceof CostConfigurationProductInfoModel)) {
                    CostConfigurationProductInfoModel zilCuspriCostConfig = (CostConfigurationProductInfoModel) productInfo;
                    if (CUSTOMER_SPECIFIC_PRICE.equals(zilCuspriCostConfig.getLabel())) {
                        productInfoIterator.remove();
                        break;
                    }
                }
            }
        }
    }

    @Override
    public Predicate<AbstractOrderEntryProductInfoModel> isZilliantCostConfig() {
        return abstractOrderEntryProductInfoModel -> {
            if (COSTCONFIGURATION == abstractOrderEntryProductInfoModel.getConfiguratorType()) {
                CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel = (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel;
                return ZILLIANT.equals(targetCostConfigurationProductInfoModel.getLabel());
            }
            return false;
        };
    }

    @Override
    public Predicate<AbstractOrderEntryProductInfoModel> isPacCostConfig() {
        return abstractOrderEntryProductInfoModel -> {
            if (COSTCONFIGURATION == abstractOrderEntryProductInfoModel.getConfiguratorType()) {
                CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel = (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel;
                return PAC.equals(targetCostConfigurationProductInfoModel.getLabel());
            }
            return false;
        };
    }

    @Override
    public Predicate<AbstractOrderEntryProductInfoModel> isProsCostConfig() {
        return abstractOrderEntryProductInfoModel -> {
            if (COSTCONFIGURATION == abstractOrderEntryProductInfoModel.getConfiguratorType()) {
                CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel = (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel;
                return PROS.equals(targetCostConfigurationProductInfoModel.getLabel());
            }
            return false;
        };
    }

    @Override
    public Predicate<AbstractOrderEntryProductInfoModel> isZilCuspriCostConfig() {
        return abstractOrderEntryProductInfoModel -> {
            if (COSTCONFIGURATION == abstractOrderEntryProductInfoModel.getConfiguratorType()) {
                CostConfigurationProductInfoModel targetCostConfigurationProductInfoModel = (CostConfigurationProductInfoModel) abstractOrderEntryProductInfoModel;
                return CUSTOMER_SPECIFIC_PRICE.equals(targetCostConfigurationProductInfoModel.getLabel());
            }
            return false;
        };
    }

    private CostConfigurationProductInfoModel createCostConfigurationProductInfoModel(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value, @NotNull String label) {
        final CostConfigurationProductInfoModel costConfigurationProductInfo;
        if (getInMemoryCartHelper().isInMemoryCartEntry(entry)) {
            costConfigurationProductInfo = getModelService().create(InMemoryCostConfigurationProductInfoModel.class);
        } else {
            costConfigurationProductInfo = getModelService().create(CostConfigurationProductInfoModel.class);
        }
        costConfigurationProductInfo.setLabel(label);
        costConfigurationProductInfo.setCurrency(currency);
        costConfigurationProductInfo.setQuantity(quantity);
        costConfigurationProductInfo.setUnit(unit);
        costConfigurationProductInfo.setValue(Math.abs(value));
        costConfigurationProductInfo.setOrderEntry(entry);
        costConfigurationProductInfo.setConfiguratorType(COSTCONFIGURATION);
        return costConfigurationProductInfo;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public InMemoryCartHelper getInMemoryCartHelper() {
        return inMemoryCartHelper;
    }

    @Required
    public void setInMemoryCartHelper(InMemoryCartHelper inMemoryCartHelper) {
        this.inMemoryCartHelper = inMemoryCartHelper;
    }

}

package com.thyssenkrupp.b2b.eu.pricedata.service;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CostConfigurationProductInfoModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;

import javax.validation.constraints.NotNull;
import java.util.function.Predicate;

public interface CostConfigurationProductInfoService {

    CostConfigurationProductInfoModel createHandlingCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value, String handlingType);

    CostConfigurationProductInfoModel createZilliantCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value);

    CostConfigurationProductInfoModel createZilCuspriCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, long quantity, UnitModel unit, double value);

    void updateCostConfigurationInOrderEntry(@NotNull AbstractOrderEntryModel entry, @NotNull CostConfigurationProductInfoModel costConfigurationProductInfoModel);

    Predicate<AbstractOrderEntryProductInfoModel> isZilliantCostConfig();

    Predicate<AbstractOrderEntryProductInfoModel> isZilCuspriCostConfig();

    void removeZilCuspriCostConfigurationProductInfo(@NotNull AbstractOrderEntryModel entry);

    CostConfigurationProductInfoModel createPacCostConfigurationProductInfo(AbstractOrderEntryModel entry,
            CurrencyModel currency, double quantity, UnitModel unit, double value);
    
    CostConfigurationProductInfoModel createProsCostConfigurationProductInfo(AbstractOrderEntryModel entry, CurrencyModel currency, double quantity, UnitModel unit, double value);


    Predicate<AbstractOrderEntryProductInfoModel> isPacCostConfig();
    
    Predicate<AbstractOrderEntryProductInfoModel> isProsCostConfig();

}

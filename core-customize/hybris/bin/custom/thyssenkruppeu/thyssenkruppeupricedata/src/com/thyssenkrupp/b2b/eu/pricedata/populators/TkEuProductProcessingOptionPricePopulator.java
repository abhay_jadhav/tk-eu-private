package com.thyssenkrupp.b2b.eu.pricedata.populators;

import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;

public class TkEuProductProcessingOptionPricePopulator<S extends ProductModel, T extends ProductData> extends AbstractProductPopulator<S, T> {

    private TkEuPriceService tkEuPriceService;
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(S productModel, T productData) {
        getTkEuPriceService().getProcessingOptionPriceForProduct(productModel).ifPresent(info -> {
            final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(info.getPriceValue().getValue()), info.getPriceValue().getCurrencyIso());
            productData.setPrice(priceData);
        });
    }

    public TkEuPriceService getTkEuPriceService() {
        return tkEuPriceService;
    }

    @Required
    public void setTkEuPriceService(TkEuPriceService tkEuPriceService) {
        this.tkEuPriceService = tkEuPriceService;
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}


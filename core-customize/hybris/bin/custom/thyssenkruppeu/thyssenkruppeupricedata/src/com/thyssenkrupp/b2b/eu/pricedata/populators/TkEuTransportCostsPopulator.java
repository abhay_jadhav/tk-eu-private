package com.thyssenkrupp.b2b.eu.pricedata.populators;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

public class TkEuTransportCostsPopulator implements Populator<AbstractOrderModel, AbstractOrderData> {
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        if (source.getDeliveryCost() != null && source.getCurrency() != null) {
            target.setDeliveryCost(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getDeliveryCost()), source.getCurrency().getIsocode()));
        }
    }

    private Optional<DiscountValue> findTransportsCost(List<DiscountValue> globalDiscountValues) {
        return globalDiscountValues.stream().filter(discountValue -> ThyssenkruppeupricedataConstants.TRANSPORT_COSTS.equals(discountValue.getCode())).findFirst();
    }

    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}

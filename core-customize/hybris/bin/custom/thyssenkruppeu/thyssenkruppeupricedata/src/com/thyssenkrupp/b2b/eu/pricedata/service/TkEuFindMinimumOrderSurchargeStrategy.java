package com.thyssenkrupp.b2b.eu.pricedata.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;

import java.util.Optional;

public interface TkEuFindMinimumOrderSurchargeStrategy {

    Optional<DiscountValue> getMinimumOrderValueSurcharge(AbstractOrderModel orderModel);
}

package com.thyssenkrupp.b2b.eu.pricedata.dao;

import de.hybris.platform.europe1.model.DiscountRowModel;
import java.util.List;

public interface TkEuPriceRowDao<T extends DiscountRowModel> {

    List<T> findPriceRowByCondKey(String condKey, String sapConditionId);

    T findPriceRowByCondKey(String condKey, String sapConditionId, Long quantityInScaleUnit);
}

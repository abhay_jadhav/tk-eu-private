package com.thyssenkrupp.b2b.eu.pricedata.utils;

import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public final class TkEuPriceUtils {

    private TkEuPriceUtils() {
        throw new IllegalAccessError("Utility class may not be instantiated");
    }

    public static RoundingMode getRoundingMode() {
        return RoundingMode.HALF_EVEN;
    }

    public static Date getTruncatedDate() {
        return Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static double convertToNegativeValue(double value) {
        return Math.abs(value) * -1;
    }
}

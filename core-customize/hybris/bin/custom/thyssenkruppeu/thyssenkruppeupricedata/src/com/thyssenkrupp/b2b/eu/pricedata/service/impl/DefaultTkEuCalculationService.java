package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuCalculationException;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuRoundingService;
import com.thyssenkrupp.b2b.eu.pricedata.service.order.hook.helper.InMemoryCartHelper;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultTkEuCalculationService extends DefaultCalculationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCalculationService.class);

    private OrderRequiresCalculationStrategy tkEuOrderRequiresCalculationStrategy;
    private CommonI18NService                tkCommonI18NService;
    private TkEuRoundingService              roundingService;
    private InMemoryCartHelper               inMemoryCartHelper;

    @Override
    protected void saveOrder(AbstractOrderModel order) {
        if (getInMemoryCartHelper().isInMemoryCartPredicate().negate().test(order)) {
            super.saveOrder(order);
        }
    }

    @Override
    public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate) {
        if (recalculate || getTkEuOrderRequiresCalculationStrategy().requiresCalculation(entry)) {
            final AbstractOrderModel order = entry.getOrder();
            final CurrencyModel curr = order.getCurrency();
            final int digits = curr.getDigits().intValue();

            final double totalPriceWithoutDiscount = entry.getBasePrice().doubleValue()
              * entry.getQuantity().longValue();

            final double roundedTotalPriceWithoutDiscount = getRoundingService().roundBasePrice(BigDecimal.valueOf(totalPriceWithoutDiscount)).doubleValue();

            /*
             * thyssenkrupp does not have a concept of 'qty' for applying order entry discounts
             * its applied per line item
             */
            final double lineItem = 1;

            final List appliedDiscounts = DiscountValue.apply(lineItem, roundedTotalPriceWithoutDiscount, digits,
              convertDiscountValues(order, entry.getDiscountValues()), curr.getIsocode());
            entry.setDiscountValues(appliedDiscounts);
            double totalPrice = roundedTotalPriceWithoutDiscount;
            for (final Iterator it = appliedDiscounts.iterator(); it.hasNext(); ) {
                totalPrice -= ((DiscountValue) it.next()).getAppliedValue();
            }
            entry.setTotalPrice(totalPrice);
            calculateTotalTaxValues(entry);
            setCalculatedStatus(entry);
            if (getInMemoryCartHelper().isInMemoryCartEntryPredicate().negate().test(entry)) {
                getModelService().save(entry);
            }
        }
    }

    @Override
    public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException {
        double subtotal = 0.0;
        Set<AbstractOrderEntryModel> inValidEntries = new HashSet<>();
        for (final AbstractOrderEntryModel e : order.getEntries()) {
            try {
                recalculateOrderEntryIfNeeded(e, forceRecalculate);
                subtotal += e.getTotalPrice().doubleValue();
            } catch (CalculationException ex) {
                inValidEntries.add(e);
            }
        }
        order.setTotalPrice(Double.valueOf(subtotal));
        if (CollectionUtils.isNotEmpty(inValidEntries)) {
            removeInvalidCartEntries(inValidEntries, order);
        }
    }

    @Override
    protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException {
        // taxes
        final Collection<TaxValue> entryTaxes = findTaxValues(entry);
        entry.setTaxValues(entryTaxes);
        final PriceValue pv = findBasePrice(entry);
        final AbstractOrderModel order = entry.getOrder();
        final PriceValue basePrice = convertPriceIfNecessaryWithoutRounding(pv, order.getNet().booleanValue(), order.getCurrency(), entryTaxes);
        entry.setBasePrice(Double.valueOf(basePrice.getValue()));
        final List<DiscountValue> entryDiscounts = findDiscountValues(entry);
        entry.setDiscountValues(entryDiscounts);
    }

    public PriceValue convertPriceIfNecessaryWithoutRounding(final PriceValue pv, final boolean toNet, final CurrencyModel toCurrency,
      final Collection taxValues) {
        // net - gross - conversion
        double convertedPrice = pv.getValue();
        if (pv.isNet() != toNet) {
            convertedPrice = pv.getOtherPrice(taxValues).getValue();
        }
        // currency conversion
        final String iso = pv.getCurrencyIso();
        if (iso != null && !iso.equals(toCurrency.getIsocode())) {
            try {
                final CurrencyModel basePriceCurrency = getTkCommonI18NService().getCurrency(iso);
                convertedPrice = getTkCommonI18NService().convertAndRoundCurrency(basePriceCurrency.getConversion().doubleValue(),
                  toCurrency.getConversion().doubleValue(), 16, convertedPrice);
            } catch (final UnknownIdentifierException e) {
                LOG.warn("Cannot convert from currency '" + iso + "' to currency '" + toCurrency.getIsocode() + "' since '" + iso
                  + "' doesn't exist any more - ignored");
            }
        }
        return new PriceValue(toCurrency.getIsocode(), convertedPrice, toNet);
    }

    private void removeInvalidCartEntries(@NotNull Set<AbstractOrderEntryModel> inValidEntries, AbstractOrderModel order) throws TkEuCalculationException {
        List<String> invalidItems = inValidEntries.stream().map(abstractOrderEntryModel -> abstractOrderEntryModel.getProduct().getCode()).collect(Collectors.toList());
        getModelService().removeAll(inValidEntries);
        getModelService().refresh(order);
        order.setCalculated(Boolean.FALSE);
        if (CollectionUtils.isEmpty(order.getEntries())) {
            order.setTotalPrice(0.0);
            getModelService().save(order);
        }
        throw new TkEuCalculationException("Cart Calculation Failed with invalid cart entries", invalidItems);
    }

    public TkEuRoundingService getRoundingService() {
        return roundingService;
    }

    @Required
    public void setRoundingService(TkEuRoundingService roundingService) {
        this.roundingService = roundingService;
    }

    public OrderRequiresCalculationStrategy getTkEuOrderRequiresCalculationStrategy() {
        return tkEuOrderRequiresCalculationStrategy;
    }

    @Required
    public void setTkEuOrderRequiresCalculationStrategy(OrderRequiresCalculationStrategy tkEuOrderRequiresCalculationStrategy) {
        this.tkEuOrderRequiresCalculationStrategy = tkEuOrderRequiresCalculationStrategy;
    }

    public CommonI18NService getTkCommonI18NService() {
        return tkCommonI18NService;
    }

    @Required
    public void setTkCommonI18NService(CommonI18NService tkCommonI18NService) {
        this.tkCommonI18NService = tkCommonI18NService;
    }

    public InMemoryCartHelper getInMemoryCartHelper() {
        return inMemoryCartHelper;
    }

    @Required
    public void setInMemoryCartHelper(InMemoryCartHelper inMemoryCartHelper) {
        this.inMemoryCartHelper = inMemoryCartHelper;
    }
}

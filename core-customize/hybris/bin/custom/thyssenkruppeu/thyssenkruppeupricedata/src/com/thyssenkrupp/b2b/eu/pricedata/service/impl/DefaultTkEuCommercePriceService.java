package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.impl.MaterialQuantityWrapperBuilder;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceException;
import com.thyssenkrupp.b2b.eu.pricedata.exception.TkEuPriceServiceUnKnownStateException;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuExternalPricingService;
import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPacPriceService;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;
import com.thyssenkrupp.b2b.eu.prospricing.service.TkEuProsPricingService;

public class DefaultTkEuCommercePriceService extends DefaultCommercePriceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCommercePriceService.class);

    private UserService                       userService;
    private CommonI18NService                 commonI18nService;
    private DefaultB2BCommerceUnitService     defaultB2BCommerceUnitService;
    private TkEuExternalPricingService        tkEuExternalPricingService;
    private TkEuPacPriceService               tkEuPacPriceService;
    private TkEuProsPricingService              tkEuProsPricingService;

    public TkEuProsPricingService getTkEuProsPricingService() {
        return tkEuProsPricingService;
    }
    
    @Required
    public void setTkEuProsPricingService(TkEuProsPricingService tkEuProsPricingService) {
        this.tkEuProsPricingService = tkEuProsPricingService;
    }

    @Override
    public PriceInformation getWebPriceForProduct(final ProductModel product) {
        LOG.debug("Start of price calculation: Get WebPrice Information for Zilliant Product with code \'{}\'.", product.getCode());
        final UserModel currentUser = userService.getCurrentUser();
        try {
            if (!userService.isAnonymousUser(currentUser) && currentUser instanceof B2BCustomerModel) {
                return getPriceInformation(product, (B2BCustomerModel) currentUser);
            }
            throw new TkEuPriceServiceUnKnownStateException(String.format("Current User in an Anonymous user (or) not a B2BCustomer \'%s\'", currentUser.getUid()));
        } catch (TkEuPriceServiceUnKnownStateException | TkEuPriceServiceException ex) {
            LOG.error("Can not find a web price for the material due to following reason ", ex);
        }
        LOG.error("Couldn't find price for product {} and current user {} return as price \"null\"", product.getCode(), currentUser.getUid());
        return null;
    }

    //for base price (displayed above scales)
    private PriceInformation getPriceInformation(ProductModel product, B2BCustomerModel currentUser) throws TkEuPriceServiceException {
        final B2BUnitModel defaultB2bUnit = getParentB2bUnit(currentUser);
        if (defaultB2bUnit != null) {
            TkEuPriceParameter tkEuPriceParameter = buildPriceParameter(product, defaultB2bUnit, buildMaterialQuantity(product.getUnit()));
            //pac call
            PriceValue priceValue= calculatePacPrice(tkEuPriceParameter);
            LOG.info("Finished PAC price calculation of product with code \'{}\' and currentUser \'{}\' with price \'{}\'.", product.getCode(), currentUser.getUid(), priceValue.getValue());
            if(priceValue.getValue()== 0.0d && TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeupricedataConstants.PROS_PRICING_ENABLED, true)) {       //NOSONAR
           
                priceValue =calculateProsPrice(tkEuPriceParameter);
                LOG.info("Finished PROS price calculation of product with code \'{}\' and currentUser \'{}\' with price \'{}\'.", product.getCode(), currentUser.getUid(), priceValue.getValue());

            }
            if (priceValue.getValue() <= 0.0d
                    && TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeupricedataConstants.ZILLIANT_FALLBACK_ENABLED, true)) {

                priceValue = getTkEuExternalPricingService().calculateBasePriceForCustomer(tkEuPriceParameter);
                LOG.info(
                        "Finished zilliant price calculation of product with code \'{}\' and currentUser \'{}\' with price \'{}\'.",
                        product.getCode(), currentUser.getUid(), priceValue.getValue());
            }
            if(priceValue.getValue() <= 0.0d)  {
                
                throw new TkEuPriceServiceException("No Valid Price from PROS WebService call Returned for Product"+ product.getCode());
            }

            return new PriceInformation(priceValue);
        }

        throw new TkEuPriceServiceUnKnownStateException(String.format("Missing default unit for the customer \'%s\'", currentUser.getUid()));
    }

    private PriceValue calculateProsPrice(TkEuPriceParameter tkEuPriceParameter) {
        Optional<TkEuProsPriceReponseParameter> prosPriceResponse=tkEuProsPricingService.getProsPriceForProduct(tkEuPriceParameter);
        
        if(prosPriceResponse.isPresent() && StringUtils.isEmpty(prosPriceResponse.get().getErrorMessage())) {
         return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), prosPriceResponse.get().getTargetPrice().doubleValue(), true);    
        }
        return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), 0.0d, true);
    }

    public PriceValue calculatePacPrice(TkEuPriceParameter priceParameter) throws TkEuPriceServiceException {
        PriceValue basePrice = getTkEuPacPriceService().getPACPriceForQuantity(priceParameter).getPrice();
        LOG.debug("PAC Price for product {}: {}.", priceParameter.getProduct().getCode(), basePrice);
        return new PriceValue(getCommonI18nService().getCurrentCurrency().getIsocode(), basePrice.getValue(), true);
    }

    private TkEuPriceParameter buildPriceParameter(ProductModel productModel, B2BUnitModel b2BUnitModel, MaterialQuantityWrapper materialQuantity) {
        TkEuPriceParameter priceParameter = new TkEuPriceParameter();
        priceParameter.setProduct(productModel);
        priceParameter.setQuantityWrapper(materialQuantity);
        priceParameter.setB2bUnit(b2BUnitModel);
        priceParameter.setCurrencyCode(getCommonI18nService().getCurrentCurrency().getIsocode());
        priceParameter.setChildB2bUnit(getDefaultB2BCommerceUnitService().getParentUnit());
        return priceParameter;
    }

    private B2BUnitModel getParentB2bUnit(B2BCustomerModel currentUser) {
        B2BUnitModel defaultUnit = currentUser.getDefaultB2BUnit();
        if (defaultUnit == null) {
            getDefaultB2BCommerceUnitService().getRootUnit();
        }
        return defaultUnit;
    }

    private MaterialQuantityWrapper buildMaterialQuantity(UnitModel unitModel) {
        /*Web Price For Product is always found in baseUnit and quantity = 1 */
        return MaterialQuantityWrapperBuilder.builder().withQuantity(1.0).withUnit(unitModel).build();
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    @Required
    public void setDefaultB2BCommerceUnitService(DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public TkEuExternalPricingService getTkEuExternalPricingService() {
        return tkEuExternalPricingService;
    }

    public void setTkEuExternalPricingService(TkEuExternalPricingService tkEuExternalPricingService) {
        this.tkEuExternalPricingService = tkEuExternalPricingService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public CommonI18NService getCommonI18nService() {
        return commonI18nService;
    }

    public void setCommonI18nService(CommonI18NService commonI18nService) {
        this.commonI18nService = commonI18nService;
    }

    public TkEuPacPriceService getTkEuPacPriceService() {
        return tkEuPacPriceService;
    }

    @Required
    public void setTkEuPacPriceService(TkEuPacPriceService tkEuPacPriceService) {
        this.tkEuPacPriceService = tkEuPacPriceService;
    }
}

package com.thyssenkrupp.b2b.eu.pricedata.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.TkEuPriceConditionKeyBuilderStrategy;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import com.thyssenkrupp.b2b.eu.core.service.PackagingCostTypes;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultTkEuPriceConditionKeyBuilderStrategy implements TkEuPriceConditionKeyBuilderStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPriceConditionKeyBuilderStrategy.class);
    private UserService      userService;
    private BaseStoreService baseStoreService;

    @Override
    public List<String> apply(@NotNull Map<Integer, List<TkEuPriceConditionKeyBuilderComponents>> unSortedFactors, TkEuPriceParameter priceParameter) {
        List<List<TkEuPriceConditionKeyBuilderComponents>> factors = unSortedFactors.entrySet().stream()
            .sorted(Map.Entry.comparingByKey())
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());

        if (null != priceParameter.getPackagingCostFactors()
                && priceParameter.getPackagingCostFactors().getPackagingGroup().isPresent()) {
            final TkEuPriceConditionKeyBuilderDto dto = createKeyBuilderDtoForPackagingCost(priceParameter);
            return factors.stream()
                    .map(tkEuPricingConditionKeyBuildFactors -> tkEuPricingConditionKeyBuildFactors.stream()
                            .map(c -> buildKeyForConditionComponent(c, dto))
                            .collect(Collectors.joining(getJoiningKey())))
                    .collect(Collectors.toList());

        } else {
            final TkEuPriceConditionKeyBuilderDto dto = createKeyBuilderDto(priceParameter);
            return factors.stream()
                    .map(tkEuPricingConditionKeyBuildFactors -> tkEuPricingConditionKeyBuildFactors.stream()
                            .map(c -> buildKeyForConditionComponent(c, dto))
                            .collect(Collectors.joining(getJoiningKey())))
                    .collect(Collectors.toList());
        }     
        
    }
    
    private TkEuPriceConditionKeyBuilderDto createKeyBuilderDtoForPackagingCost(TkEuPriceParameter parameter) {
        TkEuPriceConditionKeyBuilderDto dto = new TkEuPriceConditionKeyBuilderDto();

        dto.setVariantId(parameter.getVariantId());
        dto.setCurrencyCode(parameter.getCurrencyCode());
        dto.setBaseStore(getBaseStoreService().getCurrentBaseStore());
        dto.setProduct(parameter.getProduct());
        dto.setUser(getUserService().getCurrentUser());

        if (parameter.getPackagingCostFactors().getPackagingCostType().equals(PackagingCostTypes.CUSTOMER_SPECIFIC)) {

            dto.setChildB2bUnit(parameter.getChildB2bUnit());
            dto.setB2bUnit(parameter.getB2bUnit());

        } else if (parameter.getPackagingCostFactors().getPackagingCostType()
                .equals(PackagingCostTypes.SALES_OFFICE_DEFAULT)) {
            B2BUnitModel tempB2bUnit = new B2BUnitModel();
            tempB2bUnit.setUid(StringUtils.repeat(StringUtils.SPACE, 10));
            dto.setB2bUnit(tempB2bUnit);
            dto.setChildB2bUnit(parameter.getChildB2bUnit());
        } else if (parameter.getPackagingCostFactors().getPackagingCostType()
                .equals(PackagingCostTypes.GLOBAL_DEFAULT)) {

            B2BUnitModel tempB2bUnit = new B2BUnitModel();
            tempB2bUnit.setUid(StringUtils.repeat(StringUtils.SPACE, 10));
            tempB2bUnit.setSalesOffice(StringUtils.repeat(StringUtils.SPACE, 4));
            dto.setB2bUnit(tempB2bUnit);
            dto.setChildB2bUnit(tempB2bUnit);
        }

        dto.setPackagingCostKeyFactors(parameter.getPackagingCostFactors());
        return dto;

    }

    private TkEuPriceConditionKeyBuilderDto createKeyBuilderDto(TkEuPriceParameter parameter) {
        TkEuPriceConditionKeyBuilderDto dto = new TkEuPriceConditionKeyBuilderDto();
        dto.setVariantId(parameter.getVariantId());
        dto.setCurrencyCode(parameter.getCurrencyCode());
        dto.setB2bUnit(parameter.getB2bUnit());
        dto.setBaseStore(getBaseStoreService().getCurrentBaseStore());
        dto.setProduct(parameter.getProduct());
        dto.setUser(getUserService().getCurrentUser());
        dto.setChildB2bUnit(parameter.getChildB2bUnit());
        if (parameter.getZilliantPriceParameter() != null) {
            dto.setZilPrice(parameter.getZilliantPriceParameter().getZilPrice());
            dto.setZilProduct(parameter.getZilliantPriceParameter().getZilProduct());
        }
        return dto;
    }

    protected String buildKeyForConditionComponent(final TkEuPriceConditionKeyBuilderComponents component, final TkEuPriceConditionKeyBuilderDto dto) {
        String key = "";
        if (TkEuPriceConditionKeyBuilderComponents.EMPTY.equals(component)) {
            LOG.debug("Found empty key component");
        } else if (component.isOptional()) {
            key = StringUtils.repeat(' ', component.getFixedLength());
        } else {
            key = evaluateKeyBuilderFunction(component, dto);
        }
        if (StringUtils.isNotEmpty(key) && component.getFixedLength() != key.length()) {
            LOG.debug("Condition key component \'{}\' for {} does not meet the required fixed length \'{}\'.", key, component, component.getFixedLength());
        }
        return key == null ? "" : key;
    }

    protected String evaluateKeyBuilderFunction(final TkEuPriceConditionKeyBuilderComponents conditionKeyComponents, final TkEuPriceConditionKeyBuilderDto dto) {
        String key = "";
        try {
            key = conditionKeyComponents.getKeyBuilderFunction().apply(dto).orElse("");
        } catch (Exception e) {
            LOG.warn("Price Condition Key Builder Returned \"Exception\" for conditionCode {}.", conditionKeyComponents.getCode());
        }
        return key;
    }

    protected String getJoiningKey() {
        return StringUtils.EMPTY;
    }

    public UserService getUserService() {
        return userService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

}

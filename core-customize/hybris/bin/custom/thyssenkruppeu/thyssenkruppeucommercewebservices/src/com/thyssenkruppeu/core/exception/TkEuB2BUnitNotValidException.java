package com.thyssenkruppeu.core.exception;

public class TkEuB2BUnitNotValidException extends RuntimeException {
    public TkEuB2BUnitNotValidException(final String message) {
        super(message, null, true, false);
    }

    @Override
    public String toString() {
        return getLocalizedMessage();
    }
}

package com.thyssenkrupp.b2b.eu.queues.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;

import com.thyssenkrupp.b2b.eu.model.expressupdate.cron.ProductExpressUpdateCleanerCronJobModel;
import com.thyssenkrupp.b2b.eu.queues.impl.ProductExpressUpdateQueue;

/**
 * A Cron Job for cleaning up {@link ProductExpressUpdateQueue}.
 */
public class ProductExpressUpdateCleanerJob extends AbstractJobPerformable<ProductExpressUpdateCleanerCronJobModel> {
    private ProductExpressUpdateQueue productExpressUpdateQueue;

    @Override
    public PerformResult perform(final ProductExpressUpdateCleanerCronJobModel cronJob) {
        final Date timestamp = new Date(
                System.currentTimeMillis() - (cronJob.getQueueTimeLimit().intValue() * 60 * 1000));
        productExpressUpdateQueue.removeItems(timestamp);
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public void setProductExpressUpdateQueue(final ProductExpressUpdateQueue productExpressUpdateQueue) {
        this.productExpressUpdateQueue = productExpressUpdateQueue;
    }
}

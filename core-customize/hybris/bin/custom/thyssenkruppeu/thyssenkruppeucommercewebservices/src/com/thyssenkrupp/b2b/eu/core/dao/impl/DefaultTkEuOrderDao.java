package com.thyssenkrupp.b2b.eu.core.dao.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.OrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuOrderDao;

public class DefaultTkEuOrderDao implements TkEuOrderDao {

    private static final String FIND_ORDER_BY_SAP_ORDER_ID_VERSION_NULL = "select {pk} from {" + OrderModel._TYPECODE
            + " } where {" + OrderModel.SAPORDERID + "}= ?sapOrderId and {" + OrderModel.VERSIONID + "} IS NULL";

    private static final String FIND_ORDER_BY_CODE = "select {pk} from {" + OrderModel._TYPECODE
            + " } where {" + OrderModel.CODE + "}= ?code";

    private static final String FIND_ORDER_BY_CODE_AND_VERSION = "select {pk} from {" + OrderModel._TYPECODE
            + " } where {" + OrderModel.CODE + "}= ?code and {" + OrderModel.VERSIONID + "} IS NULL";

    private static final String SAP_ORDER_ID = "sapOrderId";
    private static final String CODE = "code";

    private FlexibleSearchService flexibleSearchService;

    private OrderDao orderDao;

    @Override
    public OrderEntryModel findOrderEntryBySapOrderId(final String sapOrderId, final int orderEntryNumber) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_SAP_ORDER_ID_VERSION_NULL);
        query.addQueryParameter(SAP_ORDER_ID, sapOrderId);
        final SearchResult<OrderModel> searchResult = flexibleSearchService.search(query);

        if (CollectionUtils.isEmpty(searchResult.getResult())) {
            return null;
        }

        final List<AbstractOrderEntryModel> orderEntries = orderDao.findEntriesByNumber(searchResult.getResult().get(0),
                orderEntryNumber);

        if (CollectionUtils.isNotEmpty(orderEntries)) {
            return (OrderEntryModel) orderEntries.get(0);
        }
        return null;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public void setOrderDao(final OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public OrderModel findOrderModelBySapOrderId(final String sapOrderId) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_SAP_ORDER_ID_VERSION_NULL);
        query.addQueryParameter(SAP_ORDER_ID, sapOrderId);
        final SearchResult<OrderModel> searchResult = flexibleSearchService.search(query);

        if (CollectionUtils.isEmpty(searchResult.getResult())) {
            return null;
        }
        return searchResult.getResult().get(0);
    }

    @Override
    public OrderEntryModel findOrderEntryFromOrder(final int orderEntryNumber, final OrderModel orderModel) {
        final List<AbstractOrderEntryModel> orderEntries = orderDao.findEntriesByNumber(orderModel, orderEntryNumber);

        if (CollectionUtils.isNotEmpty(orderEntries)) {
            return (OrderEntryModel) orderEntries.get(0);
        }
        return null;
    }

    @Override
    public List<OrderModel> findOrderModelByCode(final String code) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_CODE);
        query.addQueryParameter(CODE, code);
        final SearchResult<OrderModel> searchResult = flexibleSearchService.search(query);

        return searchResult.getResult();

    }

    @Override
    public OrderModel findLatestOrderModelByCode(final String code) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_CODE_AND_VERSION);
        query.addQueryParameter(CODE, code);
        final SearchResult<OrderModel> searchResult = flexibleSearchService.search(query);

        if (CollectionUtils.isEmpty(searchResult.getResult())) {
            return null;
        }
        return searchResult.getResult().get(0);

    }
}

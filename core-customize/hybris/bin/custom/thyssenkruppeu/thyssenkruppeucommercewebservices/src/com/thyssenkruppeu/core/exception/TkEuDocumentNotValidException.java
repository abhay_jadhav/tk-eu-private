package com.thyssenkruppeu.core.exception;

public class TkEuDocumentNotValidException extends RuntimeException {


    public TkEuDocumentNotValidException(final String message) {
        super(message, null, true, false);
    }

    @Override
    public String toString() {
        return getLocalizedMessage();
    }
}

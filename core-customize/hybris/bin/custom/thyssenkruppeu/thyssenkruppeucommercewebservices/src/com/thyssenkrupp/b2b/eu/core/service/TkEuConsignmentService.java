package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.VendorModel;

import java.util.List;
import java.util.Optional;


public interface TkEuConsignmentService {

    Optional<ConsignmentModel> findConsignmentForCode(String consignmentCode);

    Optional<VendorModel> findVendorByCode(String vendorCode);

    List<ConsignmentEntryModel> findConsignmentEntry(String consignmentCode, String batchGroupNumber);

}

package com.thyssenkrupp.b2b.eu.core.dao;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public interface TkEuInvoiceDao {
    InvoiceModel findByCode(String invoiceCode);
}

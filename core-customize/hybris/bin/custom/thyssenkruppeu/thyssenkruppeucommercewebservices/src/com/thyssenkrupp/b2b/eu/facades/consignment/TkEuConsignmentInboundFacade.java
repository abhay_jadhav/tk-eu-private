package com.thyssenkrupp.b2b.eu.facades.consignment;

import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentImportWsDTO;

public interface TkEuConsignmentInboundFacade {

    String createAndSaveConsignmentInfo(ConsignmentImportWsDTO consignment);
}

package com.thyssenkrupp.b2b.eu.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import org.springframework.util.CollectionUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuInvoiceDao;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class DefaultTkEuInvoiceDao implements TkEuInvoiceDao {

    private static final String INVOICE_CODE = "code";
    private FlexibleSearchService flexibleSearchService;

    @Override
    public InvoiceModel findByCode(final String invoiceCode) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                "SELECT {" + InvoiceModel.PK + "} from {" + InvoiceModel._TYPECODE + "} where {" + InvoiceModel.CODE
                        + "}= ?code");
        query.addQueryParameter(INVOICE_CODE, invoiceCode);
        final SearchResult<InvoiceModel> searchResult = flexibleSearchService.search(query);

        if (CollectionUtils.isEmpty(searchResult.getResult())) {
            return null;
        }
        return searchResult.getResult().get(0);
    }



    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}

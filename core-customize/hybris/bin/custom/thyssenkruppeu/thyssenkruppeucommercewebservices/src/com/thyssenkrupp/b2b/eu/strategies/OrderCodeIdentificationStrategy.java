package com.thyssenkrupp.b2b.eu.strategies;

/**
 * Strategy for identifying is given string is GUID.
 */
public interface OrderCodeIdentificationStrategy {
    boolean isID(String potentialId);
}

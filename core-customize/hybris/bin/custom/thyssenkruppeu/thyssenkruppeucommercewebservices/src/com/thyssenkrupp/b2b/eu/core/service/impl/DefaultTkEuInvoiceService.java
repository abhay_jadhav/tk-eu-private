package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuInvoiceDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuInvoiceService;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class DefaultTkEuInvoiceService implements TkEuInvoiceService {

    private TkEuInvoiceDao tkEuInvoiceDao;

    @Override
    public InvoiceModel findByCode(final String invoiceCode) {
        return tkEuInvoiceDao.findByCode(invoiceCode);
    }

    public TkEuInvoiceDao getTkEuInvoiceDao() {
        return tkEuInvoiceDao;
    }

    public void setTkEuInvoiceDao(final TkEuInvoiceDao tkEuInvoiceDao) {
        this.tkEuInvoiceDao = tkEuInvoiceDao;
    }



}

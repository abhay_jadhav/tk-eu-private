package com.thyssenkrupp.b2b.eu.queues.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;

import com.thyssenkrupp.b2b.eu.model.expressupdate.cron.OrderStatusUpdateCleanerCronJobModel;
import com.thyssenkrupp.b2b.eu.queues.impl.OrderStatusUpdateQueue;

/**
 * A Cron Job for cleaning up
 * {@link com.thyssenkrupp.b2b.eu.queues.impl.OrderStatusUpdateQueue}.
 */
public class OrderStatusUpdateCleanerJob extends AbstractJobPerformable<OrderStatusUpdateCleanerCronJobModel> {
    private OrderStatusUpdateQueue orderStatusUpdateQueue;

    @Override
    public PerformResult perform(final OrderStatusUpdateCleanerCronJobModel cronJob) {
        final Date timestamp = new Date(
                System.currentTimeMillis() - (cronJob.getQueueTimeLimit().intValue() * 60 * 1000));
        getOrderStatusUpdateQueue().removeItems(timestamp);
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    protected OrderStatusUpdateQueue getOrderStatusUpdateQueue() {
        return orderStatusUpdateQueue;
    }

    public void setOrderStatusUpdateQueue(final OrderStatusUpdateQueue orderStatusUpdateQueue) {
        this.orderStatusUpdateQueue = orderStatusUpdateQueue;
    }
}

/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.CollectionUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuConsignmentDao;

/**
 * @author vagrant
 *
 */
public class DefaultTkEuConsignmentDao extends AbstractItemDao implements TkEuConsignmentDao {

    protected static final String VENDOR_CODE = "code";

    protected static final String FIND_VENDOR_FOR_ID = "SELECT {" + VendorModel.PK + "} FROM {" + VendorModel._TYPECODE
            + "} WHERE {" + VendorModel.CODE + "} = ?code";

    protected static final String FIND_COSIGNMENT_ENTRY_FOR_NUMBER = "select {entry." + ConsignmentEntryModel.PK
            + "} from" + "{ConsignmentEntry AS entry JOIN Consignment as c ON " + "{entry."
            + ConsignmentEntryModel.CONSIGNMENT + "}={c." + ConsignmentModel.PK + "}} WHERE {c." + ConsignmentModel.CODE
            + "}=?code AND " + "{entry." + ConsignmentEntryModel.BATCHGROUPNUMBER + "}=?batchgroupnumber";

    private static final String CONSIGNMENT_CODE = "code";

    private static final String BATCH_GROUP_NUMBER = "batchgroupnumber";

    @Override
    public Optional<ConsignmentModel> findConsignmentForCode(final String consignmentId) {
        final StringBuilder query = new StringBuilder("SELECT {cons." + ConsignmentModel.PK + "} ");
        ConsignmentModel consignment = null;
        query.append("FROM {" + ConsignmentModel._TYPECODE + " AS cons} ");
        query.append("WHERE {cons." + ConsignmentModel.CODE + "} = ?" + ConsignmentModel.CODE);
        final Map<String, Object> params = new HashMap<String, Object>(1);
        params.put(ConsignmentModel.CODE, consignmentId);
        final SearchResult<ConsignmentModel> searchRes = search(query.toString(), params);

        if (!searchRes.getResult().isEmpty()) {
            consignment = searchRes.getResult().get(0);
        } else {
            return Optional.empty();
        }
        return Optional.of(consignment);
    }

    @Override
    public Optional<VendorModel> findVendorByCode(final String vendorCode) {
        validateParameterNotNull(vendorCode, "Vendor code must not be null");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_VENDOR_FOR_ID);
        query.addQueryParameter(VENDOR_CODE, vendorCode);
        try {
            return Optional.ofNullable(getFlexibleSearchService().searchUnique(query));
        } catch (final Exception e) {
            return Optional.empty();
        }
    }

    public List<ConsignmentEntryModel> findConsignmentEntry(final String consignmentCode,
            final String batchGroupNumber) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_COSIGNMENT_ENTRY_FOR_NUMBER);

        fQuery.addQueryParameter(CONSIGNMENT_CODE, consignmentCode);
        fQuery.addQueryParameter(BATCH_GROUP_NUMBER, batchGroupNumber);
        final SearchResult<ConsignmentEntryModel> result = getFlexibleSearchService().search(fQuery);
        if (CollectionUtils.isEmpty(result.getResult())) {
            return Collections.emptyList();
        } else {
            return result.getResult();
        }
    }

}

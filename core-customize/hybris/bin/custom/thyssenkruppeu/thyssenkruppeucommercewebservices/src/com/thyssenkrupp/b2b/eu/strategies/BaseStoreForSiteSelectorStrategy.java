package com.thyssenkrupp.b2b.eu.strategies;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

/**
 * Strategy for retrieving base store from the base site.
 */
public interface BaseStoreForSiteSelectorStrategy {
    BaseStoreModel getBaseStore(BaseSiteModel baseSiteModel);
}

/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;

import java.util.List;

import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;

/**
 * @author vagrant
 *
 */
public interface TkEuOrderCancelRecordsHandler extends OrderCancelRecordsHandler{

    /**
     * @param orderModelToUpdate
     * @param orderHistoryEntryModel
     * @param snapshot
     * @param cancelledOrderEntryDtoList
     * @param request
     * @return
     */
    boolean cancelOrderRequest(OrderModel orderModelToUpdate, OrderHistoryEntryModel orderHistoryEntryModel,
            OrderModel snapshot, List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList, OrderCancelRequest request);

}

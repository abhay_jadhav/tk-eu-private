package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

public interface TkEuOrderDao {

    OrderEntryModel findOrderEntryBySapOrderId(String sapOrderId, int orderEntryNumber);

    OrderModel findOrderModelBySapOrderId(String sapOrderId);

    OrderEntryModel findOrderEntryFromOrder(int orderEntryNumber, OrderModel orderModel);

    List<OrderModel> findOrderModelByCode(String code);

    OrderModel findLatestOrderModelByCode(String code);
}

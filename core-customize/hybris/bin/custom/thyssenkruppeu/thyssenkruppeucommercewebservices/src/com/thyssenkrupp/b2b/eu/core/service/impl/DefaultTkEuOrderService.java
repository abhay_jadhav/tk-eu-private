/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.OrderCancelEntryStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuOrderDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderCancelRecordsHandler;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderService;
import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.OrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;

/**
 * @author vagrant
 *
 */
public class DefaultTkEuOrderService implements TkEuOrderService {
    private static final Logger LOG = Logger.getLogger(DefaultTkEuOrderService.class);

    private static final String DUMMY = "D-";

    private static final String TK_EU_PRODUCT_CATALOG_ID = "tkSchulteProductCatalog";

    private static final String DUMMY_PRODUCT_NAME = "sap_dummy_product";

    private static final String DEFAULT_PRODUCT_UNIT = "PCE";

    private static final String SAP_DUMMY_USER = "sap_dummy_user";

    private static final String DEFAULT_CURRENCY = "EUR";

    private TkEuOrderDao tkEuOrderDao;

    private ModelService modelService;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Resource(name = "catalogVersionService")
    private CatalogVersionService catalogVersionService;

    @Resource(name = "unitService")
    private UnitService unitService;

    @Resource(name = "orderCancelService")
    private OrderCancelService orderCancelService;

    @Resource(name = "tkEuOrderCancelRecordsHandler")
    private TkEuOrderCancelRecordsHandler tkEuOrderCancelRecordsHandler;

    @Override
    public OrderEntryModel findOrderEntryBySapOrderId(final String sapOrderId, final int orderEntryNumber) {
        return tkEuOrderDao.findOrderEntryBySapOrderId(sapOrderId, orderEntryNumber);
    }

    @Override
    public OrderModel findOrderModelBySapOrderId(final String sapOrderId) {
        return tkEuOrderDao.findOrderModelBySapOrderId(sapOrderId);
    }

    @Override
    public OrderEntryModel findOrderEntryFromOrder(final int orderEntryNumber, final OrderModel orderModel) {
        return tkEuOrderDao.findOrderEntryFromOrder(orderEntryNumber, orderModel);
    }

    @Override
    public List<OrderModel> findOrderModelByCode(final String code) {
        return tkEuOrderDao.findOrderModelByCode(code);
    }

    /**
     * @param orderEntry
     * @param consignmentEntry
     * @param consignmentEntryModel
     * @return
     */
    @Override
    public OrderEntryModel createDummyOrderEntry(final OrderEntryImportWsDTO orderEntry,
            final ConsignmentEntryImportWsDTO consignmentEntry, final ConsignmentEntryModel consignmentEntryModel) {
        final OrderModel dummyOrderModel = modelService.create(OrderModel.class);
        if (Objects.nonNull(orderEntry)) {
            dummyOrderModel.setCode(getDummyOrderCode(orderEntry.getSapOrderId()));
        } else {
            dummyOrderModel.setCode(getDummyOrderCode(""));
        }
        dummyOrderModel.setUser(userService.getUserForUID(SAP_DUMMY_USER));
        dummyOrderModel.setDate(new Date());
        dummyOrderModel.setStatus(OrderStatus.CHECKED_INVALID);
        dummyOrderModel.setCurrency(commonI18NService.getCurrency(DEFAULT_CURRENCY));
        modelService.save(dummyOrderModel);
        return createDummyOrderEntryForOrder(dummyOrderModel, consignmentEntry, consignmentEntryModel);
    }

    /**
     * @param sapOrderId
     * @return
     */
    private String getDummyOrderCode(final String sapOrderId) {
        return DUMMY + (new Random()).nextInt(1000) + "-"
                + (StringUtils.isNotEmpty(sapOrderId) ? sapOrderId : (new Random()).nextInt(1000000));
    }

    /**
     * @param order
     * @param consignmentEntry
     * @param orderEntry
     * @param consignmentEntryModel
     * @return
     */
    @Override
    public OrderEntryModel createDummyOrderEntryForOrder(final AbstractOrderModel order,
            final ConsignmentEntryImportWsDTO consignmentEntry, final ConsignmentEntryModel consignmentEntryModel) {
        final OrderEntryModel dummyOrderEntryModel = modelService.create(OrderEntryModel.class);
        dummyOrderEntryModel.setOrder(order);
        userService.setCurrentUser(userService.getAdminUser());
        dummyOrderEntryModel.setProduct(productService.getProductForCode(
                catalogVersionService.getCatalogVersion(TK_EU_PRODUCT_CATALOG_ID, CatalogManager.ONLINE_VERSION),
                DUMMY_PRODUCT_NAME));
        userService.setCurrentUser(userService.getAnonymousUser());
        dummyOrderEntryModel.setUnit(unitService.getUnitForCode(DEFAULT_PRODUCT_UNIT));
        dummyOrderEntryModel.setQuantity(Long.valueOf(consignmentEntry.getQuantity()));
        modelService.save(dummyOrderEntryModel);
        return dummyOrderEntryModel;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public TkEuOrderDao getTkEuOrderDao() {
        return tkEuOrderDao;
    }

    public void setTkEuOrderDao(final TkEuOrderDao tkEuOrderDao) {
        this.tkEuOrderDao = tkEuOrderDao;
    }

    @Override
    public OrderModel findLatestOrderModelByCode(final String code) {
        return tkEuOrderDao.findLatestOrderModelByCode(code);
    }

    @Override
    public ProductModel getSapDummyProductModel() {
        userService.setCurrentUser(userService.getAdminUser());
        final ProductModel dummyProduct = productService.getProductForCode(
                catalogVersionService.getCatalogVersion(TK_EU_PRODUCT_CATALOG_ID, CatalogManager.ONLINE_VERSION),
                DUMMY_PRODUCT_NAME);
        userService.setCurrentUser(userService.getAnonymousUser());
        return dummyProduct;
    }

    @Override
    public boolean cancelOrderEntries(final OrderModel orderModel,
            final List<AbstractOrderEntryModel> cancelledEntryList) {

        return false;
    }

    @Override
    public String cancelOrder(final OrderModel orderModelToUpdate, final OrderHistoryEntryModel orderHistoryEntryModel,
            final OrderModel snapshot, final List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList,
            final boolean fullCancelation) {
        final OrderCancelRequest request = createOrderCancelRequest(orderModelToUpdate, snapshot,
                cancelledOrderEntryDtoList, fullCancelation);
        if (request != null) {

            return tkEuOrderCancelRecordsHandler.cancelOrderRequest(orderModelToUpdate, orderHistoryEntryModel,
                    snapshot, cancelledOrderEntryDtoList, request) ? "Cancellation Successfull!!"
                            : "Cancellation Falied!!";
        } else {

            return "No Cancellation required as No Entries to be cancelled in Order!!";
        }
    }

    /**
     * @param orderModelToUpdate
     * @param snapshot
     * @param cancelledOrderEntryDtoList
     * @param fullCancelation
     * @return
     */
    private OrderCancelRequest createOrderCancelRequest(final OrderModel orderModelToUpdate, final OrderModel snapshot,
            final List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList, final boolean fullCancelation) {
        final List<OrderCancelEntry> entriesToBeCancelled = mapOrderCancelEntriesRequest(cancelledOrderEntryDtoList,
                orderModelToUpdate);
        if (CollectionUtils.isNotEmpty(entriesToBeCancelled)) {
            return new OrderCancelRequest(orderModelToUpdate, entriesToBeCancelled,
                    fullCancelation ? OrderCancelEntryStatus.FULL.getCode() : OrderCancelEntryStatus.PARTIAL.getCode());
        } else {
            return null;
        }
    }

    /**
     * @param cancelledOrderEntryDtoList
     * @param orderModelToUpdate
     * @return
     */
    private List<OrderCancelEntry> mapOrderCancelEntriesRequest(
            final List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList, final OrderModel orderModelToUpdate) {
        final List<OrderCancelEntry> entriesToBeCancelled = new ArrayList<>();
        cancelledOrderEntryDtoList.stream().forEach(orderEntryDto -> {
            try {
                final int hybrisEntryNumber = TkEuWebServiceUtils
                        .getHybrisEntryNoFromSAPEntryNo(String.valueOf(orderEntryDto.getEntryNumber()));

                final CancelReason cancelReason = (StringUtils.isNotEmpty(orderEntryDto.getRejectionCode())
                        && "01".equalsIgnoreCase(orderEntryDto.getRejectionCode())) ? CancelReason.LATEDELIVERY
                                : CancelReason.OTHER;

                final String rejectionReason = (StringUtils.isNotEmpty(orderEntryDto.getRejectionCode())
                        ? orderEntryDto.getRejectionCode() + ":"
                        : "")
                        + (StringUtils.isNotEmpty(orderEntryDto.getRejectionReason())
                                ? orderEntryDto.getRejectionReason()
                                : "");
                final OrderEntryModel orderEntry = this.findOrderEntryFromOrder(hybrisEntryNumber, orderModelToUpdate);
                if (orderEntry != null) {
                    final OrderCancelEntry orderEntryCancelRequest = new OrderCancelEntry(orderEntry, rejectionReason,
                            cancelReason);
                    entriesToBeCancelled.add(orderEntryCancelRequest);
                }
            } catch (final Exception e) {
                LOG.error("Exception in mapOrderCancelEntriesRequest ->" + orderModelToUpdate.getCode()
                        + " Entry Number:" + (orderEntryDto.getEntryNumber() - 1));
            }
        });
        return entriesToBeCancelled;
    }

}

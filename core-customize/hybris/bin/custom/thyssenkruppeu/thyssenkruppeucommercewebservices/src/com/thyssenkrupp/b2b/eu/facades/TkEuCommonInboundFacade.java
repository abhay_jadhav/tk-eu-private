package com.thyssenkrupp.b2b.eu.facades;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

import com.thyssenkrupp.b2b.eu.dto.inbound.AddressImportWsDTO;

public interface TkEuCommonInboundFacade {

    B2BUnitModel getB2BUnit(String unit, String owner, String distributionChannel);

    AddressModel createOrUpdateAddress(AddressImportWsDTO addressDto, boolean update, AddressModel address,
            Object owner, String addressType, String consignmentCode);

    boolean isValidSerialNumberForRequest(String serialNumberFromDB, String serialNumberFromRequest,
            String documentCode);

}

package com.thyssenkrupp.b2b.eu.facades.order;

import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderImportWsDTO;

public interface TkEuOrderInboundFacade {

    String createAndUpdateOrder(ABOrderImportWsDTO abOrder);
}

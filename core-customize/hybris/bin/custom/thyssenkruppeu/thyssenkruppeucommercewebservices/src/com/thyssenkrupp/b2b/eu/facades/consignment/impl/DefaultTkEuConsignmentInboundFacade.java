package com.thyssenkrupp.b2b.eu.facades.consignment.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.sap.sapmodel.enums.ConsignmentEntryStatus;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.constants.YcommercewebservicesConstants;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConsignmentService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderService;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.TkEuCommonInboundFacade;
import com.thyssenkrupp.b2b.eu.facades.consignment.TkEuConsignmentInboundFacade;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;
import com.thyssenkruppeu.core.exception.TkEuB2BUnitNotValidException;
import com.thyssenkruppeu.core.exception.TkEuDocumentNotValidException;

public class DefaultTkEuConsignmentInboundFacade implements TkEuConsignmentInboundFacade {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuConsignmentInboundFacade.class);

    private static final String DEFAULT_DELIVERY_MODE_CODE = "standard-net";

    private static final String DEFAULT_VENDOR_CODE = "ERP";

    private static final String SUCCESS = "Success";

    private ModelService modelService;

    private B2BUnitService b2bUnitService;

    private TkEuOrderService tkEuOrderService;

    private UnitService unitService;

    private TkEuCommonInboundFacade tkEuCommonInboundFacade;

    @Resource(name = "tkEuConsignmentService")
    private TkEuConsignmentService tkEuConsignmentService;

    private WarehouseService warehouseService;

    @Resource(name = "deliveryModeService")
    private DeliveryModeService deliveryModeService;

    public WarehouseService getWarehouseService() {
        return warehouseService;
    }

    public void setWarehouseService(final WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @Override
    public String createAndSaveConsignmentInfo(final ConsignmentImportWsDTO consignment) {
        boolean update = false;

        final B2BUnitModel b2bUnit = tkEuCommonInboundFacade.getB2BUnit(consignment.getUnit(), consignment.getOwner(),
                consignment.getDistributionChannel());

        if (Objects.isNull(b2bUnit)) {
            throw new TkEuB2BUnitNotValidException("B2BUnit is not valid or Not Present in Hybris ");
        }

        if (Objects.isNull(consignment.getShippingAddress())) {
            throw new TkEuDocumentNotValidException("Consignment " + consignment.getCode()
                    + " is not valid. Rejecting due mandatory Shipping Address not in  Request!!");
        }
        try {
            final ConsignmentModel consignmentModel = tkEuConsignmentService
                    .findConsignmentForCode(consignment.getCode()).orElse(modelService.create(ConsignmentModel.class));
            update = isConsignmentUpdate(consignmentModel);

            if (update && !tkEuCommonInboundFacade.isValidSerialNumberForRequest(
                    consignmentModel.getDeliverySerialisationKey(), consignment.getDeliverySerialisationKey(),
                    consignment.getCode())) {
                throw new TkEuDocumentNotValidException("Consignment " + consignment.getCode()
                        + " is not valid. Rejecting due to Older SerialKey in Request!!");
            }
            AddressModel shippingAddress = null;
            if (!update) {
                consignmentModel.setCode(consignment.getCode());
                consignmentModel.setOwner(b2bUnit);
                consignmentModel.setUnit(b2bUnit);

                shippingAddress = tkEuCommonInboundFacade.createOrUpdateAddress(consignment.getShippingAddress(),
                        update, modelService.create(AddressModel.class), b2bUnit,
                        YcommercewebservicesConstants.ADDRESS_TYPE_CONSIGNMENT, consignment.getCode());
                consignmentModel.setShippingAddress(shippingAddress);
                consignmentModel
                        .setDeliveryMode(deliveryModeService.getDeliveryModeForCode(DEFAULT_DELIVERY_MODE_CODE));
                consignmentModel.setStatus(ConsignmentStatus.SHIPPED);
                consignmentModel.setNamedDeliveryDate(TkEuWebServiceUtils.getDateinFormat(
                        consignment.getNamedDeliveryDate(), YcommercewebservicesConstants.DATE_FORMAT));
                consignmentModel.setWarehouse(createOrGetWarehouseForCode(consignment.getWarehouse()));

            } else if (Objects.nonNull(consignmentModel.getShippingAddress())) {
                shippingAddress = tkEuCommonInboundFacade.createOrUpdateAddress(consignment.getShippingAddress(),
                        update, consignmentModel.getShippingAddress(), b2bUnit,
                        YcommercewebservicesConstants.ADDRESS_TYPE_CONSIGNMENT, consignment.getCode());
            }
            modelService.save(shippingAddress);

            populateUpdateableFieldsForConsignment(consignmentModel, consignment);
            modelService.save(consignmentModel);

            createAndUpdateConsignmentEntries(consignment.getConsignmentEntries(), consignmentModel, update);
            LOG.info(consignment.getCode() + " :"
                    + (update ? "Consignment Updated Successfully !!!" : "Consignment Created Successfully !!!"));

            return SUCCESS;
        } catch (final Exception e) {
            LOG.error("Exception in " + consignment.getCode() + " :"
                    + (update ? "Consignment Updation " : "Consignment Creation ") + " :" + e.getMessage(), e);

            throw new TkEuDocumentNotValidException("Consignment " + consignment.getCode()
                    + " is not processed. Rejecting due Exception in processing :" + e.getMessage());
        }
    }

    /**
     * @param consignmentModel
     * @return
     */
    private boolean isConsignmentUpdate(final ConsignmentModel consignmentModel) {
        if (StringUtils.isNotEmpty(consignmentModel.getCode())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param consignmentModel
     * @param consignment
     */
    private void populateUpdateableFieldsForConsignment(final ConsignmentModel consignmentModel,
            final ConsignmentImportWsDTO consignment) {
        consignmentModel.setDivision(consignment.getDivision());
        consignmentModel.setSalesOrg(consignment.getSalesOrg());
        consignmentModel.setDistributionChannel(consignment.getDistributionChannel());
        consignmentModel.setDeliverySerialisationKey(consignment.getDeliverySerialisationKey());
        consignmentModel.setTkNamedDeliveryDate(TkEuWebServiceUtils
                .getDateinFormat(consignment.getTkNamedDeliveryDate(), YcommercewebservicesConstants.DATE_FORMAT));
        consignmentModel.setShippingDate(TkEuWebServiceUtils.getDateinFormat(consignment.getShippingDate(),
                YcommercewebservicesConstants.DATE_FORMAT));
        consignmentModel.setGoodsRecipient(consignment.getGoodsRecipient());
    }

    /**
     * @return
     */
    private WarehouseModel createOrGetWarehouseForCode(final String wareHouseCode) {

        WarehouseModel warehouse;
        try {
            warehouse = warehouseService.getWarehouseForCode(wareHouseCode);
        } catch (final Exception e) {
            LOG.error(" Warehouse with Code " + wareHouseCode + " not found, hence creating the same in System. ");
            warehouse = modelService.create(WarehouseModel.class);
            warehouse.setCode(wareHouseCode);
            warehouse.setVendor(tkEuConsignmentService.findVendorByCode(DEFAULT_VENDOR_CODE).get());
            modelService.save(warehouse);
            LOG.info(" Created Warehouse with Code " + wareHouseCode + " in System. ");

        }
        return warehouse;
    }

    /**
     * @param consignmentEntries
     * @param consignmentModel
     * @param update
     * @return
     */
    private void createAndUpdateConsignmentEntries(final List<ConsignmentEntryImportWsDTO> consignmentEntries,
            final ConsignmentModel consignmentModel, final boolean update) {
        final List<ConsignmentEntryModel> consignmentEntriesToBeRemoved = new ArrayList<>();
        consignmentEntriesToBeRemoved.addAll(consignmentModel.getConsignmentEntries());
        consignmentEntries.forEach(consignmentEntry -> {
            try {
                final ConsignmentEntryModel consignmentEntryModel = update
                        ? getConsignmentEntryToUpdateFromList(consignmentModel.getConsignmentEntries(),
                                consignmentEntry.getEntryNumber())
                        : modelService.create(ConsignmentEntryModel.class);
                final boolean entryUpdate = (update && !(Objects.isNull(consignmentEntryModel.getConsignment())))
                        ? Boolean.TRUE
                        : Boolean.FALSE;
                boolean isDummyOrderEntrySet = false;

                if (!entryUpdate) {
                    consignmentEntryModel.setConsignment(consignmentModel);
                    consignmentEntryModel.setEntryNumber(consignmentEntry.getEntryNumber());
                    isDummyOrderEntrySet = setOrderEntryForConsignment(consignmentEntry, consignmentEntryModel);

                } else {
                    consignmentEntriesToBeRemoved.remove(consignmentEntryModel);
                }

                populateUpdatableFieldForConsignmentEntry(consignmentEntryModel, consignmentEntry, consignmentModel);

                modelService.save(consignmentEntryModel);
                if (isDummyOrderEntrySet) {
                    modelService.remove(consignmentEntryModel.getOrderEntry().getOrder());
                }
            } catch (final Exception e) {
                LOG.error("Exception in saving Consignment Entry for code " + consignmentModel.getCode()
                        + " Entry Number:" + consignmentEntry.getEntryNumber() + " Error:" + e.getMessage(), e);
            }
        });
        removeDeletedConsignmentEntries(consignmentEntriesToBeRemoved, consignmentModel.getCode());
    }

    /**
     * @param consignmentEntryModel
     * @param consignmentEntry
     * @param consignmentModel
     */
    private void populateUpdatableFieldForConsignmentEntry(final ConsignmentEntryModel consignmentEntryModel,
            final ConsignmentEntryImportWsDTO consignmentEntry, final ConsignmentModel consignmentModel) {
        consignmentEntryModel.setBatchGroupNumber(consignmentEntry.getBatchGroupNumber());
        consignmentEntryModel.setStatus(ConsignmentEntryStatus.SHIPPED);
        consignmentEntryModel.setQuantity(Long.valueOf(consignmentEntry.getQuantity()));
        consignmentEntryModel.setShippedQuantity(Long.valueOf(consignmentEntry.getShippedQuantity()));
        consignmentEntryModel.setProductCode(consignmentEntry.getProductCode());
        consignmentEntryModel.setProductName(consignmentEntry.getProductName());
        consignmentEntryModel.setBatchNumber(consignmentEntry.getBatchNumber());
        consignmentEntryModel.setSupplierLotNumber(consignmentEntry.getSupplierLotNumber());
        if (Objects.nonNull(consignmentEntry.getOrderEntry())) {
            consignmentEntryModel.setSapOrderId(consignmentEntry.getOrderEntry().getSapOrderId());
            consignmentEntryModel.setHybrisOrderId(consignmentEntry.getOrderEntry().getHybrisOrderId());
            consignmentEntryModel.setSapOrderEntryRowNumber(TkEuWebServiceUtils
                    .getHybrisEntryNoFromSAPEntryNo(consignmentEntry.getOrderEntry().getEntryNumber()));
        }
        populateShippedQuantityUnit(consignmentEntryModel, consignmentEntry);
        consignmentEntryModel.setSalesOffice(consignmentEntry.getSalesOffice());
        consignmentEntryModel.setDivision(consignmentModel.getDivision());
        consignmentEntryModel.setDistributionChannel(consignmentModel.getDistributionChannel());
        consignmentEntryModel.setCertificateOrdered(consignmentEntry.getCertificateOrdered());

    }

    /**
     * @param consignmentEntriesToBeRemoved
     * @param consignmentCode
     */
    private void removeDeletedConsignmentEntries(final List<ConsignmentEntryModel> consignmentEntriesToBeRemoved,
            final String consignmentCode) {
        try {
            if (CollectionUtils.isNotEmpty(consignmentEntriesToBeRemoved)) {
                modelService.removeAll(consignmentEntriesToBeRemoved);
            }
        } catch (final Exception e) {
            LOG.error("Exception in deleting unwanted Consignment Entry for code " + consignmentCode + " Error:"
                    + e.getMessage(), e);
        }
    }

    /**
     * @param consignmentEntryModel
     * @param consignmentEntry
     */
    private void populateShippedQuantityUnit(final ConsignmentEntryModel consignmentEntryModel,
            final ConsignmentEntryImportWsDTO consignmentEntry) {
        try {
            consignmentEntryModel
                    .setShippedQuantityUnit(unitService.getUnitForCode(consignmentEntry.getShippedQuantityUnit()));
        } catch (final Exception e) {
            LOG.error("For Consignment " + consignmentEntry.getConsignmentCode() + " Unit with Code "
                    + consignmentEntry.getShippedQuantityUnit() + " not found " + " Error:" + e.getMessage());
        }

    }

    /**
     * @param consignmentEntry
     * @param consignmentEntryModel
     * @return
     */
    private boolean setOrderEntryForConsignment(final ConsignmentEntryImportWsDTO consignmentEntry,
            final ConsignmentEntryModel consignmentEntryModel) {

        try {
            if (Objects.nonNull(consignmentEntry.getOrderEntry())
                    && StringUtils.isNotEmpty(consignmentEntry.getOrderEntry().getEntryNumber())
                    && StringUtils.isNotEmpty(consignmentEntry.getOrderEntry().getSapOrderId())) {
                final int hybrisEntryNumber = TkEuWebServiceUtils
                        .getHybrisEntryNoFromSAPEntryNo(consignmentEntry.getOrderEntry().getEntryNumber());

                final OrderEntryModel orderEntryModel = tkEuOrderService.findOrderEntryBySapOrderId(
                        consignmentEntry.getOrderEntry().getSapOrderId(), hybrisEntryNumber);

                if (Objects.nonNull(orderEntryModel)) {
                    consignmentEntryModel.setOrderEntry(orderEntryModel);
                    return false;
                } else {

                    consignmentEntryModel.setOrderEntry(tkEuOrderService.createDummyOrderEntry(
                            consignmentEntry.getOrderEntry(), consignmentEntry, consignmentEntryModel));
                    return true;
                }
            } else {
                consignmentEntryModel.setOrderEntry(tkEuOrderService.createDummyOrderEntry(
                        consignmentEntry.getOrderEntry(), consignmentEntry, consignmentEntryModel));
                return true;
            }
        } catch (final Exception e) {
            LOG.error(
                    "Exception in getting orderEntryModel for code " + consignmentEntry.getOrderEntry().getSapOrderId()
                            + " :" + (Integer.valueOf(consignmentEntry.getOrderEntry().getEntryNumber()) - 1)
                            + " Consignment Code:" + consignmentEntryModel.getConsignment().getCode() + " Entry Number:"
                            + consignmentEntryModel.getEntryNumber() + " Error:" + e.getMessage(),
                    e);
            throw new TkEuDocumentNotValidException(
                    "Consignment Entry is not created for " + consignmentEntryModel.getConsignment().getCode());

        }

    }

    /**
     * @param consignmentEntries
     * @param entryNumber
     * @return
     */
    private ConsignmentEntryModel getConsignmentEntryToUpdateFromList(
            final Set<ConsignmentEntryModel> consignmentEntries, final String entryNumber) {
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            return consignmentEntries.stream().filter(
                    consignmentEntryModelObj -> consignmentEntryModelObj.getEntryNumber().equalsIgnoreCase(entryNumber))
                    .findFirst().orElse(modelService.create(ConsignmentEntryModel.class));
        }
        return modelService.create(ConsignmentEntryModel.class);
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public B2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(final B2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    public TkEuCommonInboundFacade getTkEuCommonInboundFacade() {
        return tkEuCommonInboundFacade;
    }

    public void setTkEuCommonInboundFacade(final TkEuCommonInboundFacade tkEuCommonInboundFacade) {
        this.tkEuCommonInboundFacade = tkEuCommonInboundFacade;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    public TkEuOrderService getTkEuOrderService() {
        return tkEuOrderService;
    }

    public void setTkEuOrderService(final TkEuOrderService tkEuOrderService) {
        this.tkEuOrderService = tkEuOrderService;
    }

}

package com.thyssenkrupp.b2b.eu.core.service;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public interface TkEuInvoiceService {

    InvoiceModel findByCode(String invoiceCode);

}

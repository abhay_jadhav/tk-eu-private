package com.thyssenkrupp.b2b.eu.facades.order;

import com.thyssenkrupp.b2b.eu.dto.inbound.VBOrderImportWsDTO;

public interface TkEuOrderAcknowledgementInboundFacade {

    String updateAndSaveOrderAcknowledgementInfo(VBOrderImportWsDTO vbOrder);
}

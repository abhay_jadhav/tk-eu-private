/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.List;

import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.OrderEntryImportWsDTO;

/**
 * @author vagrant
 *
 */
public interface TkEuOrderService {

    OrderEntryModel findOrderEntryBySapOrderId(String sapOrderId, int orderEntryNumber);

    OrderModel findOrderModelBySapOrderId(String sapOrderId);

    OrderEntryModel findOrderEntryFromOrder(int sapOrderId, OrderModel orderModel);

    OrderEntryModel createDummyOrderEntryForOrder(AbstractOrderModel order,
            ConsignmentEntryImportWsDTO consignmentEntry, ConsignmentEntryModel consignmentEntryModel);

    OrderEntryModel createDummyOrderEntry(OrderEntryImportWsDTO orderEntry,
            ConsignmentEntryImportWsDTO consignmentEntry, ConsignmentEntryModel consignmentEntryModel);

    List<OrderModel> findOrderModelByCode(String code);

    ProductModel getSapDummyProductModel();

    boolean cancelOrderEntries(OrderModel orderModel, List<AbstractOrderEntryModel> cancelledEntryList);

    String cancelOrder(OrderModel orderModelToUpdate, OrderHistoryEntryModel orderHistoryEntryModel,
            OrderModel snapshot,
            List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList, boolean fullCancelation);


    /**
     * @param code
     * @return
     */
    OrderModel findLatestOrderModelByCode(String code);
}

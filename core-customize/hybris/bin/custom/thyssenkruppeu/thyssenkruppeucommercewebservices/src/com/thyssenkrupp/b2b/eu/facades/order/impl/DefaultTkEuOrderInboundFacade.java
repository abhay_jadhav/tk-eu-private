package com.thyssenkrupp.b2b.eu.facades.order.impl;

import static java.util.Optional.ofNullable;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.constants.YcommercewebservicesConstants;
import com.thyssenkrupp.b2b.eu.core.enums.Incoterms;
import com.thyssenkrupp.b2b.eu.core.enums.ShippingCondition;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bOrderService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderService;
import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.AddressImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.CustomerShippingNoteWsDTO;
import com.thyssenkrupp.b2b.eu.dto.wrapper.ABOrderUpdateDTO;
import com.thyssenkrupp.b2b.eu.facades.TkEuCommonInboundFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderInboundFacade;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;
import com.thyssenkrupp.b2b.global.baseshop.core.model.TkSapProductInfoModel;
import com.thyssenkruppeu.core.exception.TkEuB2BUnitNotValidException;
import com.thyssenkruppeu.core.exception.TkEuDocumentNotValidException;

public class DefaultTkEuOrderInboundFacade implements TkEuOrderInboundFacade {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuOrderInboundFacade.class);

    private static final String ONLINE_CHANNEL = "EC";

    private static final String OFFLINE_CHANNEL = "01";

    private static final String SUCCESS = "Success";

    private static final String SAP_DUMMY_USER = "sap_dummy_user";

    private TkEuOrderService tkEuOrderService;

    private CommonI18NService commonI18NService;

    private OrderHistoryService orderHistoryService;

    private ModelService modelService;

    private TkEuCommonInboundFacade tkEuCommonInboundFacade;

    private UserService userService;

    private UnitService unitService;

    private BaseStoreService baseStoreService;

    private TkEuB2bOrderService tkEuB2bOrderService;

    @Override
    public String createAndUpdateOrder(final ABOrderImportWsDTO abOrder) {

        // Get the Latest OrderModel from DB for code and VersionId as null
        final OrderModel orderModel = tkEuOrderService.findLatestOrderModelByCode(abOrder.getCode());

        // isUpdate = true if Order is present in DB else its false
        final boolean isUpdate = (orderModel == null) ? Boolean.FALSE : Boolean.TRUE;

        // Scenario 1: Validate Rejection scenarios for Order IDOC

        validateOrderRequest(orderModel, abOrder, isUpdate);

        // isOfflineOrder = true if its Offline else false if its Online Order
        final boolean isOfflineOrder = ONLINE_CHANNEL.equalsIgnoreCase(abOrder.getDistributionChannel()) ? Boolean.FALSE
                : Boolean.TRUE;

        // Get the OrderModel to Update / create
        final ABOrderUpdateDTO orderModelWrapperDto = getOrderModelToUpdate(isOfflineOrder, orderModel, isUpdate,
                abOrder);

        final OrderModel orderModelToUpdate = orderModelWrapperDto.getOrderModelToUpdate();

        if (!isUpdate) {
            // Populate the Initial Fields for creation of Offline Order
            populateOrderModelFields(orderModelToUpdate, abOrder);
        }

        // Create/Update the addresses for Order
        populateAddressModelInOrder(orderModelToUpdate, abOrder);

        // Update the updatable Fields for updation of Offline/Online Order
        updateOrderModelFields(orderModelToUpdate, abOrder);

        // Create/Update the order entries for Order
        createAndUpdateOrderEntry(orderModelToUpdate, abOrder, isOfflineOrder, isUpdate);

        // Update Order Status & Cancellation
        orderModelWrapperDto.setOrderModelToUpdate(orderModelToUpdate);
        updateOrderStatusAndCancellation(orderModelToUpdate, orderModelWrapperDto, abOrder, isOfflineOrder, isUpdate);

        modelService.save(orderModelToUpdate);

        LOG.info(abOrder.getCode() + " :"
                + (isUpdate ? "ABOrder Updated Successfully !!!" : "ABOrder Created Successfully !!!"));

        return SUCCESS;
    }

    /**
     * @param orderModelToUpdate
     * @param orderModelWrapperDto
     * @param abOrder
     * @param isOfflineOrder
     * @param isUpdate
     */
    private void updateOrderStatusAndCancellation(final OrderModel orderModelToUpdate,
            final ABOrderUpdateDTO orderModelWrapperDto, final ABOrderImportWsDTO abOrder, final boolean isOfflineOrder,
            final boolean isUpdate) {
        try {
            final Map<Boolean, List<ABOrderEntryImportWsDTO>> orderEntryRequestMap = abOrder.getOrderEntries().stream()
                    .collect(Collectors.partitioningBy(orderEntry -> !checkIfCancelledEntry(orderEntry)));
            final List<ABOrderEntryImportWsDTO> cancelledOrderEntryList = orderEntryRequestMap.get(Boolean.FALSE);
            if (CollectionUtils.isNotEmpty(cancelledOrderEntryList)) {
                String cancellationResult = "";
                if (cancelledOrderEntryList.size() == abOrder.getOrderEntries().size()) {
                    // Full cancelation
                    orderModelToUpdate.setStatus(OrderStatus.CANCELLED);
                    setInitialOrderStatusToCancelled(orderModelWrapperDto.getInitialOrder());
                    cancellationResult = tkEuOrderService.cancelOrder(orderModelToUpdate,
                            orderModelWrapperDto.getHistorySnapshot(),
                            orderModelWrapperDto.getSnapshot(),
                            cancelledOrderEntryList, true);
                } else {
                    // Partial cancelation
                    orderModelToUpdate.setStatus(OrderStatus.CONFIRMED);
                    cancellationResult = tkEuOrderService.cancelOrder(orderModelToUpdate,
                            orderModelWrapperDto.getHistorySnapshot(),
                            orderModelWrapperDto.getSnapshot(),
                            cancelledOrderEntryList, false);
                }

                LOG.info("Cancellation Status Upate for ABOrder " + orderModelToUpdate.getCode() + " Message:"
                        + cancellationResult);
            } else {
                orderModelToUpdate.setStatus(OrderStatus.CONFIRMED);
            }
        } catch (final Exception e) {
            LOG.error("Exception in cancellation & status Upate for ABOrder " + orderModelToUpdate.getCode() + " Error:"
                    + e.getMessage(), e);

            orderModelToUpdate.setStatus(OrderStatus.CONFIRMED);
        }

    }

    /**
     * @param initialOrder
     */
    private void setInitialOrderStatusToCancelled(final OrderModel initialOrder) {

        if (!Objects.isNull(initialOrder)) {
            initialOrder.setStatus(OrderStatus.CANCELLED);
            modelService.save(initialOrder);
            LOG.info(initialOrder.getCode() + " :" + ("InitialOrder Cancelled Successfully !!!"));
        }
    }

    /**
     * @param orderModelToUpdate
     * @param abOrder
     * @param isOfflineOrder
     * @param isUpdate
     */
    private void createAndUpdateOrderEntry(final OrderModel orderModelToUpdate, final ABOrderImportWsDTO abOrder,
            final boolean isOfflineOrder, final boolean isUpdate) {
        // List of Order Entries deleted in Request
        final List<AbstractOrderEntryModel> orderEntriesToBeRemoved = new ArrayList<>();
        orderEntriesToBeRemoved.addAll(orderModelToUpdate.getEntries());

        abOrder.getOrderEntries().forEach(orderEntry -> {
            try {

                // hybrisEntryNumber = Sap entry Number-1
                final int hybrisEntryNumber = TkEuWebServiceUtils
                        .getHybrisEntryNoFromSAPEntryNo(String.valueOf(orderEntry.getEntryNumber()));

                // isCancelledEntry = true if value present in RejectionReason or RejectionCode
                final boolean isCancelledEntry = checkIfCancelledEntry(orderEntry);

                // Get the OrderEntryModel that needs to be saved
                final OrderEntryModel orderEntryModel = getOrderEntryModel(isUpdate, hybrisEntryNumber,
                        orderModelToUpdate, orderEntry, isOfflineOrder, isCancelledEntry);

                if (orderEntryModel != null) {
                    final boolean entryUpdate = checkIfOrderEntryUpdate(isUpdate, orderEntryModel);

                    if (!entryUpdate) {
                        // Populate the Initial Fields for creation of Order entry with Product as SAP
                        // Dummy Product

                        orderEntryModel.setOrder(orderModelToUpdate);
                        orderEntryModel.setEntryNumber(hybrisEntryNumber);
                        orderEntryModel.setProduct(tkEuOrderService.getSapDummyProductModel());
                        orderEntryModel.setUnit(unitService.getUnitForCode(orderEntry.getUnit()));
                        orderEntryModel.setQuantity(Long.valueOf(orderEntry.getQuantity()));

                    } else {

                        // If Order Entry is present in request then remove it from deletion list
                        orderEntriesToBeRemoved.remove(orderEntryModel);
                    }

                    // Populate the Updatable Fields for updation of Order entry Model
                    populateUpdatableFieldForOrderEntry(orderEntryModel, orderEntry, isCancelledEntry);

                    modelService.save(orderEntryModel);

                    // Populate and save ProductInfo with Name and Code of product from request
                    populateAndSaveProductInfo(orderEntry, orderEntryModel);

                }
            } catch (final Exception e) {
                LOG.error("Exception in saving Order Entry for code " + orderModelToUpdate.getCode() + " Entry Number:"
                        + orderEntry.getEntryNumber() + " Error:" + e.getMessage(), e);
            }
        });

        // Remove all deleted Entries from request
        removeDeletedOrderEntries(orderEntriesToBeRemoved, orderModelToUpdate.getCode());

    }

    /**
     * @param orderEntryModel
     * @param isUpdate
     * @return
     */
    private boolean checkIfOrderEntryUpdate(final boolean isUpdate, final OrderEntryModel orderEntryModel) {
        return (isUpdate && !(Objects.isNull(orderEntryModel.getOrder()))) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * @param orderEntry
     * @return
     */
    private boolean checkIfCancelledEntry(final ABOrderEntryImportWsDTO orderEntry) {
        return (StringUtils.isEmpty(orderEntry.getRejectionCode())
                && StringUtils.isEmpty(orderEntry.getRejectionReason())) ? Boolean.FALSE : Boolean.TRUE;
    }

    /**
     * @param orderEntriesToBeRemoved
     * @param code
     */
    private void removeDeletedOrderEntries(final List<AbstractOrderEntryModel> orderEntriesToBeRemoved,
            final String code) {
        try {
            if (CollectionUtils.isNotEmpty(orderEntriesToBeRemoved)) {
                modelService.removeAll(orderEntriesToBeRemoved);
            }
        } catch (final Exception e) {
            LOG.error("Exception in deleting removd Order Entry for ABOrder " + code + " Error:" + e.getMessage(), e);
        }

    }

    /**
     * @param orderEntryModel
     * @param orderEntry
     * @param entryUpdate
     * @param isCancelledEntry
     */
    private void populateUpdatableFieldForOrderEntry(final OrderEntryModel orderEntryModel,
            final ABOrderEntryImportWsDTO orderEntry, final boolean isCancelledEntry) {

        orderEntryModel.setQuantity(Long.valueOf(orderEntry.getQuantity()));
        orderEntryModel.setUnit(unitService.getUnitForCode(orderEntry.getUnit()));
        orderEntryModel.setPositionWeight(orderEntry.getNetWeight());

        orderEntryModel.setTotalPrice(orderEntry.getItemTotal());
        if (isCancelledEntry) {

            // Set the Order quantity status as DEAD for Cancelled Entry in request
            orderEntryModel.setQuantityStatus(OrderEntryStatus.DEAD);
        }

    }

    /**
     * @param orderEntryModel
     * @param orderEntry
     *
     */
    private void populateAndSaveProductInfo(final ABOrderEntryImportWsDTO orderEntry,
            final OrderEntryModel orderEntryModel) {
        final TkSapProductInfoModel productInfoModel = (TkSapProductInfoModel) orderEntryModel.getProductInfos()
                .stream()
                .filter(productInfo -> ConfiguratorType.SAPPRODUCTINFO.equals(productInfo.getConfiguratorType())
                        && productInfo instanceof TkSapProductInfoModel)
                .findFirst().orElse(modelService.create(TkSapProductInfoModel.class));
        productInfoModel.setProductCode(orderEntry.getProduct());
        productInfoModel.setProductName(orderEntry.getProductName());
        productInfoModel.setOrderEntry(orderEntryModel);
        productInfoModel.setProductName(orderEntry.getProductName(), Locale.GERMAN);
        productInfoModel.setCustomerMaterialNumber(orderEntry.getCustomerMaterialNumber());
        productInfoModel.setConfiguratorType(ConfiguratorType.SAPPRODUCTINFO);
        modelService.save(productInfoModel);
    }

    /**
     * @param isUpdate
     * @param hybrisEntryNumber
     * @param orderModelToUpdate
     * @param isOfflineOrder
     * @param orderEntry
     * @param isCancelledEntry
     * @return
     */
    private OrderEntryModel getOrderEntryModel(final boolean isUpdate, final int hybrisEntryNumber,
            final OrderModel orderModelToUpdate, final ABOrderEntryImportWsDTO orderEntry, final boolean isOfflineOrder,
            final boolean isCancelledEntry) {

        // Case 1:If Order is to be Updated fetch the order entry if already present in
        // DB else create new
        if (isUpdate) {
            final OrderEntryModel orderEntryModel = tkEuOrderService.findOrderEntryFromOrder(hybrisEntryNumber,
                    orderModelToUpdate);
            if (orderEntryModel != null) {
                return orderEntryModel;
            } else if (!isCancelledEntry) {
                // Case 2:If Order is to be updated and new entry is added in Update then return
                // new Order Entry model if the
                // Entry is not cancelled in request
                return modelService.create(OrderEntryModel.class);
            }
        } else {

            // Case 3:If Order is to be created then return new Order Entry model if the
            // Entry is not cancelled in request
            if (!isCancelledEntry) {
                return modelService.create(OrderEntryModel.class);
            }

        }

        // Case 4:If Order Entry is cancelled then return then return null as it
        // can be ignored else return new Order Entry model
        return null;

    }

    /**
     * @param orderModelToUpdate
     * @param abOrder
     */
    private void updateOrderModelFields(final OrderModel orderModelToUpdate, final ABOrderImportWsDTO abOrder) {

        orderModelToUpdate.setABSerialisationKey(abOrder.getIdocSerialNumber());
        orderModelToUpdate.setPurchaseOrderNumber(abOrder.getPurchaseOrderNumber());
        orderModelToUpdate
                .setShippingcondition(getShippingCondition(abOrder.getShippingCondition(), abOrder.getCode()));
        orderModelToUpdate.setIncoterms(getIncoterms(abOrder.getIncoTerms(), abOrder.getCode()));
        orderModelToUpdate.setLogicalSystem(abOrder.getLogicalSystem());
        orderModelToUpdate.setSubtotal(abOrder.getSubTotal());
        orderModelToUpdate.setTotalPrice(abOrder.getSubTotal());
        orderModelToUpdate.setSapOrderId(abOrder.getSapOrderId());
        orderModelToUpdate.setSalesOrganization(abOrder.getSalesOrganization());
        orderModelToUpdate.setDistributionChannel(abOrder.getDistributionChannel());
        orderModelToUpdate.setDivision(abOrder.getDivision());
        orderModelToUpdate.setName(abOrder.getCustomerOrderName());
        orderModelToUpdate.setCustomerShippingNote(getShippingNote(abOrder.getCustomerShippingNote()));
        orderModelToUpdate.setTotalTax(abOrder.getTotalTax());
        orderModelToUpdate.setSalesOffice(abOrder.getSalesOffice());
        updateOrderDeliveryDate(orderModelToUpdate, abOrder);
        modelService.save(orderModelToUpdate);
    }

    /**
     * @param shippingCondition
     * @return
     */
    private ShippingCondition getShippingCondition(final String shippingCondition, final String code) {
        try {
            return ShippingCondition.valueOf(shippingCondition);
        } catch (final Exception e) {
            LOG.error("ABOrder " + code + " :Could not find shippingCondition for " + shippingCondition + ":"
                    + e.getMessage());
        }
        return null;
    }

    /**
     * @param customerShippingNote
     * @return
     */
    private String getShippingNote(final List<CustomerShippingNoteWsDTO> customerShippingNote) {
        // Write Business logic for Shipping note where ISO is DE and concatenate
        // without any delimiter, if missing do it for ISO EN
        if (CollectionUtils.isNotEmpty(customerShippingNote)) {

            final CustomerShippingNoteWsDTO shippingNote = customerShippingNote.stream()
                    .filter(note -> (Locale.GERMAN.getLanguage().equalsIgnoreCase(note.getLangISO())
                            && CollectionUtils.isNotEmpty(note.getValueList())))
                    .findFirst().orElse(
                            customerShippingNote.stream()
                                    .filter(note -> (Locale.ENGLISH.getLanguage().equalsIgnoreCase(note.getLangISO())
                                            && CollectionUtils.isNotEmpty(note.getValueList())))
                                    .findFirst().orElse(null));

            if (!Objects.isNull(shippingNote)) {
                return String.join("", shippingNote.getValueList().stream().distinct().collect(Collectors.toList()));
            }

        }
        return "";

    }

    /**
     * @param orderModelToUpdate
     * @param abOrder
     */
    private void populateAddressModelInOrder(final OrderModel orderModelToUpdate, final ABOrderImportWsDTO abOrder) {

        // Get the Delivery Address from request and set it in Delivery Address Field.
        final Optional<AddressImportWsDTO> deliveryAddress = abOrder.getAddresses().stream()
                .filter(address -> YcommercewebservicesConstants.ADDRESS_TYPE_SHIPPING
                        .equalsIgnoreCase(address.getAddressType()))
                .findFirst();
        if (deliveryAddress.isPresent()) {
            orderModelToUpdate.setDeliveryAddress(getAddressModelForAddress(orderModelToUpdate.getDeliveryAddress(),
                    deliveryAddress.get(), YcommercewebservicesConstants.ADDRESS_TYPE_SHIPPING, orderModelToUpdate));
        }

        // Get the Billing Address from request and set it in Payment Address Field.
        final Optional<AddressImportWsDTO> paymentAddress = abOrder.getAddresses().stream()
                .filter(address -> YcommercewebservicesConstants.ADDRESS_TYPE_BILLING
                        .equalsIgnoreCase(address.getAddressType()))
                .findFirst();
        if (paymentAddress.isPresent()) {
            orderModelToUpdate.setPaymentAddress(getAddressModelForAddress(orderModelToUpdate.getPaymentAddress(),
                    paymentAddress.get(), YcommercewebservicesConstants.ADDRESS_TYPE_BILLING, orderModelToUpdate));
        }

        modelService.save(orderModelToUpdate);

    }

    /**
     * @param address
     * @param addressDto
     * @param addressType
     * @param orderModelToUpdate
     * @return
     */
    private AddressModel getAddressModelForAddress(final AddressModel address, final AddressImportWsDTO addressDto,
            final String addressType, final OrderModel orderModelToUpdate) {
        final AddressModel newAddress = tkEuCommonInboundFacade.createOrUpdateAddress(addressDto,
                (address != null ? Boolean.TRUE : Boolean.FALSE),
                (address != null ? address : modelService.create(AddressModel.class)), orderModelToUpdate, addressType,
                orderModelToUpdate.getCode());
        modelService.save(newAddress);
        return newAddress;
    }

    /**
     * @param orderModelToUpdate
     * @param abOrder
     */
    private void populateOrderModelFields(final OrderModel orderModelToUpdate, final ABOrderImportWsDTO abOrder) {

        final B2BUnitModel b2bUnit = tkEuCommonInboundFacade.getB2BUnit(abOrder.getUnit(), abOrder.getUnit(),
                abOrder.getDistributionChannel());
        // Rejection Case 5: If B2bUnit for Offline Order is not in DB
        if (Objects.isNull(b2bUnit)) {
            throw new TkEuB2BUnitNotValidException(
                    "ABOrder " + abOrder.getCode() + " is not valid. B2BUnit is not valid or Not Present in Hybris "
                            + abOrder.getUnit() + "owner " + abOrder.getUnit());
        }

        orderModelToUpdate.setCode(abOrder.getCode());

        if (StringUtils.isNotEmpty(abOrder.getCreationDate())) {
        orderModelToUpdate.setDate(TkEuWebServiceUtils.getDateinFormat(abOrder.getCreationDate(),
                YcommercewebservicesConstants.DATE_FORMAT));
        } else {
            orderModelToUpdate.setDate(TkEuWebServiceUtils.getDateinFormat(abOrder.getCreationDateFallback(),
                    YcommercewebservicesConstants.DATE_FORMAT));
        }
        orderModelToUpdate.setUnit(b2bUnit);
        orderModelToUpdate.setUser(userService.getUserForUID(SAP_DUMMY_USER));
        orderModelToUpdate.setSalesApplication(SalesApplication.OFFLINE);
        orderModelToUpdate.setCurrency(commonI18NService.getCurrency(abOrder.getCurrency()));
        orderModelToUpdate.setStatus(OrderStatus.CONFIRMED);
        populateSiteAndStoreForOrder(abOrder, orderModelToUpdate);
        modelService.save(orderModelToUpdate);

    }

    private void updateOrderDeliveryDate(final OrderModel orderModelToUpdate, final ABOrderImportWsDTO abOrder) {
        final Date currentDeliveryDate = orderModelToUpdate.getDeliveryDate();

        try {
            final List<Date> orderEntryDeliveryDates = abOrder.getOrderEntries().stream()
                    .filter(entry -> StringUtils.isNotEmpty(entry.getDeliveryDate())).map(entry -> TkEuWebServiceUtils
                            .getDateinFormat(entry.getDeliveryDate(), YcommercewebservicesConstants.DATE_FORMAT))
                    .collect(Collectors.toList());

            final Date maxOrderEntryDeliveryDate = orderEntryDeliveryDates.stream()
                    .max(Comparator.nullsLast(Comparator.naturalOrder())).orElse(null);

            LOG.info("ABOrder " + abOrder.getCode() + " :Order entry delivery date is " + maxOrderEntryDeliveryDate);
            if ((currentDeliveryDate == null && maxOrderEntryDeliveryDate != null)
                    || (currentDeliveryDate != null && maxOrderEntryDeliveryDate != null
                            && currentDeliveryDate.compareTo(maxOrderEntryDeliveryDate) != 0)) {
                orderModelToUpdate.setDeliveryDate(maxOrderEntryDeliveryDate);
                LOG.info("ABOrder " + abOrder.getCode() + " :Successfully set order  delivery date as "
                        + maxOrderEntryDeliveryDate);
            }

        } catch (final Exception e) {
            LOG.error("ABOrder " + abOrder.getCode() + " :Could not find max deliverydate" + e.getMessage());
        }

    }

    /**
     * @param abOrder
     * @param orderModelToUpdate
     */
    private void populateSiteAndStoreForOrder(final ABOrderImportWsDTO abOrder, final OrderModel orderModelToUpdate) {

        final String salesOrganization = abOrder.getSalesOrganization();
        final Optional<BaseStoreModel> maybeBaseStore = getBaseStoreForSalesOrganization(salesOrganization);
        if (maybeBaseStore.isPresent()) {
            final BaseStoreModel baseStoreModel = maybeBaseStore.get();
            orderModelToUpdate.setStore(baseStoreModel);
            ofNullable(baseStoreModel.getDefaultLanguage()).ifPresent(orderModelToUpdate::setLanguage);
            ofNullable(baseStoreModel.getDefaultCurrency()).ifPresent(orderModelToUpdate::setCurrency);
            orderModelToUpdate.setNet(baseStoreModel.isNet());
            emptyIfNull(baseStoreModel.getCmsSites()).stream().findFirst().ifPresent(orderModelToUpdate::setSite);

        } else {
            final String errorMessage = String.format("No BaseStore model found for salesOrganization:%s",
                    salesOrganization);
            // Rejection Case 6: No BaseStore model found for salesOrganization
            LOG.error(errorMessage);
            throw new TkEuDocumentNotValidException(
                    "ABOrder " + abOrder.getCode() + " is not valid. Rejecting due to " + errorMessage);

        }

    }

    private Optional<BaseStoreModel> getBaseStoreForSalesOrganization(final String salesOrgValue) {
        if (isNotEmpty(salesOrgValue)) {
            return emptyIfNull(getBaseStoreService().getAllBaseStores()).stream()
                    .filter(baseStoreModel -> baseStoreModel.getSAPConfiguration() != null)
                    .filter(baseStoreModel -> StringUtils.equals(salesOrgValue,
                            baseStoreModel.getSAPConfiguration().getSapcommon_salesOrganization()))
                    .findFirst();
        }
        return Optional.empty();
    }

    /**
     * @param incoTerms
     * @return
     */
    private Incoterms getIncoterms(final String incoTerms, final String code) {
        try {
            if (StringUtils.isNotEmpty(incoTerms)) {
            return Incoterms.valueOf(incoTerms);
            }
        } catch (final Exception e) {
            LOG.error("ABOrder " + code + " :Could not find max incoTerms for " + incoTerms + ":" + e.getMessage());

        }
        return null;
    }

    /**
     * @param isOfflineOrder
     * @param orderModel
     * @param isUpdate
     * @param abOrder
     * @return
     */
    private ABOrderUpdateDTO getOrderModelToUpdate(final boolean isOfflineOrder, final OrderModel orderModel,
            final boolean isUpdate, final ABOrderImportWsDTO abOrder) {
        try {
            final ABOrderUpdateDTO orderModelWrapperDto = new ABOrderUpdateDTO();

            // Case 1: Return new OrderModel if its Offline Order and is Not present in DB
            if (isOfflineOrder && !isUpdate) {
                orderModelWrapperDto.setOrderModelToUpdate(modelService.create(OrderModel.class));
                return orderModelWrapperDto;
            }

            // Case 2: Return the OrderModel once History Snapshot is created and saved for
            // the Order
            final OrderModel snapshot = createOrderSnapShot(orderModel, abOrder);
            final OrderHistoryEntryModel orderHistoryEntrySnapshot = createOrderHistory(orderModel, snapshot);
            orderModelWrapperDto.setOrderModelToUpdate(orderModel);
            orderModelWrapperDto.setSnapshot(snapshot);
            orderModelWrapperDto.setHistorySnapshot(orderHistoryEntrySnapshot);
            if (!Boolean.TRUE.equals(snapshot.getInitialOrderVersion()) && !isOfflineOrder) {
                orderModelWrapperDto.setInitialOrder(tkEuB2bOrderService.getInitialOrderForCode(orderModel.getCode()));
            } else {
                orderModelWrapperDto.setInitialOrder(snapshot);
            }

            return orderModelWrapperDto;
        } catch (final Exception e) {
            throw new TkEuDocumentNotValidException("ABOrder " + abOrder.getCode()
                    + " is not valid. Exception in creating SnapShot of Order !!" + e.getMessage());
        }
    }

    /**
     * @param snapshot
     */
    private void setStatusOfSnapShotAsConfirmed(final OrderModel snapshot) {
        // Write business logic to write Status as CONFIRMED in case when the previous
        // status is CREATED/RECEIVED IN ERP
        if (OrderStatus.CREATED.getCode().equalsIgnoreCase(snapshot.getStatus().getCode())
                || OrderStatus.RECEIVED_IN_ERP.getCode().equalsIgnoreCase(snapshot.getStatus().getCode())) {
            snapshot.setStatus(OrderStatus.CONFIRMED);
        }

    }

    public OrderHistoryEntryModel createOrderHistory(final OrderModel order, final OrderModel snapshot) {
        final OrderHistoryEntryModel historyEntry = modelService.create(OrderHistoryEntryModel.class);
        historyEntry.setOrder(order);
        historyEntry.setPreviousOrderVersion(snapshot);
        historyEntry.setDescription("Updated by SAP");
        historyEntry.setTimestamp(new Date());
        modelService.save(historyEntry);
        return historyEntry;
    }

    public OrderModel createOrderSnapShot(final OrderModel order, final ABOrderImportWsDTO abOrder) {
        final OrderModel snapshot = orderHistoryService.createHistorySnapshot(order);
        if (StringUtils.isEmpty(snapshot.getSapOrderId())) {
            snapshot.setSapOrderId(abOrder.getSapOrderId());
        }
        setStatusOfSnapShotAsConfirmed(snapshot);
        orderHistoryService.saveHistorySnapshot(snapshot);
        return snapshot;
    }

    /**
     * @param orderModel
     * @param abOrder
     * @param isUpdate
     */
    private void validateOrderRequest(final OrderModel orderModel, final ABOrderImportWsDTO abOrder,
            final boolean isUpdate) {
        // case 1: Rejection based on serial Key

        if (isUpdate && StringUtils.isNotEmpty(orderModel.getABSerialisationKey())
                && !tkEuCommonInboundFacade.isValidSerialNumberForRequest(orderModel.getABSerialisationKey(),
                        abOrder.getIdocSerialNumber(), abOrder.getCode())) {
            throw new TkEuDocumentNotValidException(
                    "ABOrder " + abOrder.getCode() + " is not valid. Rejecting due to Older SerialKey in Request!!");
        }

        switch (abOrder.getDistributionChannel()) {

        // case 2: Rejection based on Order not Present in DB for Online Order
        case ONLINE_CHANNEL:
            if (!isUpdate) {
                throw new TkEuDocumentNotValidException(
                        "ABOrder " + abOrder.getCode() + " is not valid. No Online Order with Code found in DB");

            }
            break;
        // case 3: Rejection based on Mandatory Fields not Present in Request for
        // Offline Order

        case OFFLINE_CHANNEL:
            validateOfflineOrder(abOrder, isUpdate);
            break;
        // case 4: Rejection based on Invalid Distribution Channel in Request

        default:
            throw new TkEuDocumentNotValidException("ABOrder " + abOrder.getCode()
                    + " is not valid. Invalid Distribution Channel in Request :" + abOrder.getDistributionChannel());

        }

    }

    private void validateOfflineOrder(final ABOrderImportWsDTO abOrder, final boolean isUpdate) {

        if (!isUpdate) {
            if (StringUtils.isEmpty(abOrder.getCreationDate()) && StringUtils.isEmpty(abOrder.getCreationDateFallback())) {
                throw new TkEuDocumentNotValidException(
                        "ABOrder " + abOrder.getCode() + " is not valid. Missing creation Date in the Request");
            }
            if (StringUtils.isEmpty(abOrder.getUnit())) {
                throw new TkEuDocumentNotValidException(
                        "ABOrder " + abOrder.getCode() + " is not valid. unit is missing in the Request");
            }
        }
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public OrderHistoryService getOrderHistoryService() {
        return orderHistoryService;
    }

    public void setOrderHistoryService(final OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public TkEuCommonInboundFacade getTkEuCommonInboundFacade() {
        return tkEuCommonInboundFacade;
    }

    public void setTkEuCommonInboundFacade(final TkEuCommonInboundFacade tkEuCommonInboundFacade) {
        this.tkEuCommonInboundFacade = tkEuCommonInboundFacade;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public TkEuOrderService getTkEuOrderService() {
        return tkEuOrderService;
    }

    public void setTkEuOrderService(final TkEuOrderService tkEuOrderService) {
        this.tkEuOrderService = tkEuOrderService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuB2bOrderService getTkEuB2bOrderService() {
        return tkEuB2bOrderService;
    }

    public void setTkEuB2bOrderService(final TkEuB2bOrderService tkEuB2bOrderService) {
        this.tkEuB2bOrderService = tkEuB2bOrderService;
    }

}

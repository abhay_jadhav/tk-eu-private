package com.thyssenkrupp.b2b.eu.queues.impl;

import com.thyssenkrupp.b2b.eu.queues.data.ProductExpressUpdateElementData;

/**
 * Queue for
 * {@link com.thyssenkrupp.b2b.eu.queues.data.ProductExpressUpdateElementData}
 */
public class ProductExpressUpdateQueue extends AbstractUpdateQueue<ProductExpressUpdateElementData> {
    // EMPTY - just to instantiate bean
}

package com.thyssenkrupp.b2b.eu.queues.impl;

import com.thyssenkrupp.b2b.eu.queues.data.OrderStatusUpdateElementData;

/**
 * Queue for
 * {@link com.thyssenkrupp.b2b.eu.queues.data.OrderStatusUpdateElementData}
 */
public class OrderStatusUpdateQueue extends AbstractUpdateQueue<OrderStatusUpdateElementData> {
    // EMPTY - just to instantiate bean
}

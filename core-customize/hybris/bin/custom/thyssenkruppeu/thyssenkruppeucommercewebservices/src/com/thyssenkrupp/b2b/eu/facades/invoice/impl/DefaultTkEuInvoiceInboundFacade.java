package com.thyssenkrupp.b2b.eu.facades.invoice.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.constants.YcommercewebservicesConstants;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConsignmentService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuInvoiceService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderService;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.OrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.TkEuCommonInboundFacade;
import com.thyssenkrupp.b2b.eu.facades.invoice.TkEuInvoiceInboundFacade;
import com.thyssenkrupp.b2b.eu.model.TempConsignmentEntryModel;
import com.thyssenkrupp.b2b.eu.model.TempOrderEntryModel;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceEntryModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;
import com.thyssenkruppeu.core.exception.TkEuB2BUnitNotValidException;
import com.thyssenkruppeu.core.exception.TkEuDocumentNotValidException;

public class DefaultTkEuInvoiceInboundFacade implements TkEuInvoiceInboundFacade {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuInvoiceInboundFacade.class);



    private TkEuInvoiceService tkEuInvoiceService;

    private ModelService modelService;

    private B2BUnitService b2bUnitService;

    private TkEuConsignmentService tkEuConsignmentService;

    private TkEuOrderService tkEuOrderService;

    private TkEuCommonInboundFacade tkEuCommonInboundFacade;

    private UnitService unitService;

    @Override
    public void createAndUpdateInvoice(final InvoiceImportWsDTO invoice) {
        boolean update = true;
        InvoiceModel invoiceModel = tkEuInvoiceService.findByCode(invoice.getCode());
        final B2BUnitModel b2bUnit = tkEuCommonInboundFacade.getB2BUnit(invoice.getUnit(), invoice.getOwner(),
                invoice.getDistributionChannel());

        if (Objects.isNull(b2bUnit)) {
            throw new TkEuB2BUnitNotValidException(
                    "Invoice " + invoice.getCode() + " is not valid. B2BUnit is not valid or Not Present in Hybris "
                            + invoice.getUnit() + "owner " + invoice.getOwner());
        }

        if (Objects.isNull(invoiceModel)) {
            update = false;
            invoiceModel = modelService.create(InvoiceModel.class);
        }

        if (update && !tkEuCommonInboundFacade.isValidSerialNumberForRequest(invoiceModel.getSequenceId(),
                invoice.getIdocSerialNumber(), invoice.getCode())) {
            throw new TkEuDocumentNotValidException(
                    "Invoice " + invoice.getCode() + " is not valid. Rejecting due to Older SerialKey in Request!!");
        }



        setInvoiceModelParameters(invoice, invoiceModel, b2bUnit, update);

        final List<InvoiceEntryModel> invoiceEntries = invoiceModel.getEntries();

        final List<InvoiceEntryModel> newInvoiceEntries = createInvoiceEntries(invoice.getInvoiceEntries(),
                invoiceModel);

        if (update && CollectionUtils.isNotEmpty(invoiceEntries) && CollectionUtils.isNotEmpty(newInvoiceEntries)) {
            modelService.removeAll(invoiceEntries);
        }
        modelService.saveAll(newInvoiceEntries);

        LOG.info(invoice.getCode() + " :"
                + (update ? "Invoice Updated Successfully !!!" : "Invoice Created Successfully !!!"));

    }

    /**
     * @param update
     */
    private void setInvoiceModelParameters(final InvoiceImportWsDTO invoice, final InvoiceModel invoiceModel,
            final B2BUnitModel b2bUnit, final boolean update) {
         AddressModel billToAddress = null;
         AddressModel soldToAddress = null;
        if (!update) {
            invoiceModel.setCode(invoice.getCode());
            invoiceModel.setOwner(b2bUnit);
            invoiceModel.setUnit(b2bUnit);
            invoiceModel.setDivision(invoice.getDivison());
            invoiceModel.setSalesOrg(invoice.getSalesOrg());
            invoiceModel.setDistributionChannel(invoice.getDistributionChannel());
            soldToAddress = modelService.create(AddressModel.class);
            billToAddress = modelService.create(AddressModel.class);
        } else {
            soldToAddress = invoiceModel.getSoldToAddress();
            billToAddress = invoiceModel.getBillToAddress();
        }
        invoiceModel.setSalesOffice(invoice.getSalesOffice());
        invoiceModel.setSequenceId(invoice.getIdocSerialNumber());
        invoiceModel.setDate(
                TkEuWebServiceUtils.getDateinFormat(invoice.getDate(), YcommercewebservicesConstants.DATE_FORMAT));
        invoiceModel.setDueDate(
                TkEuWebServiceUtils.getDateinFormat(invoice.getDueDate(), YcommercewebservicesConstants.DATE_FORMAT));
        invoiceModel.setTotalNet(Double.valueOf(invoice.getTotalNet()));
        invoiceModel.setTotalGross(Double.valueOf(invoice.getTotalGross()));
        invoiceModel.setTotalTax(Double.valueOf(invoice.getTotalTax()));
        invoiceModel.setPaymentTerms(invoice.getPaymentTerms());

        if (null != invoice.getSoldToAddress()) {
            final AddressModel newSoldToAddress = tkEuCommonInboundFacade.createOrUpdateAddress(
                    invoice.getSoldToAddress(), update, soldToAddress, invoiceModel,
                    YcommercewebservicesConstants.ADDRESS_TYPE_SOLDTO, invoice.getCode());
            modelService.save(newSoldToAddress);
            invoiceModel.setSoldToAddress(newSoldToAddress);
        }
        if (null != invoice.getBillingAddress()) {
            final AddressModel newBillToAddress = tkEuCommonInboundFacade.createOrUpdateAddress(
                    invoice.getBillingAddress(), update, billToAddress, invoiceModel,
                    YcommercewebservicesConstants.ADDRESS_TYPE_BILLING, invoice.getCode());
            modelService.save(newBillToAddress);
            invoiceModel.setBillToAddress(newBillToAddress);
        }
        modelService.save(invoiceModel);
    }

    private List<InvoiceEntryModel> createInvoiceEntries(final List<InvoiceEntryImportWsDTO> invoiceEntries,
            final InvoiceModel invoiceModel) {

        final List<InvoiceEntryModel> invoiceEntryList = new ArrayList<>();
        for (final InvoiceEntryImportWsDTO invoiceEntry : invoiceEntries) {
            try {
                final InvoiceEntryModel invoiceEntryModel = modelService.create(InvoiceEntryModel.class);
                invoiceEntryModel.setEntryNumber(invoiceEntry.getInvoiceEntryNumber());
                invoiceEntryModel.setQuantity(Double.valueOf(invoiceEntry.getQuantity()));
                invoiceEntryModel.setProductName(invoiceEntry.getProductName());
                invoiceEntryModel.setProductCode(invoiceEntry.getProductCode());
                invoiceEntryModel.setProductNameCustomer(invoiceEntry.getProductCodeCustomer());
                invoiceEntryModel.setTotalTax(Double.valueOf(invoiceEntry.getTotalTax()));
                invoiceEntryModel.setTaxValue(Double.valueOf(invoiceEntry.getTaxValue()));
                invoiceEntryModel.setTotalNet(Double.valueOf(invoiceEntry.getTotalNet()));
                setUOMtoInvoiceEntryModel(invoiceEntryModel,invoiceEntry);

                invoiceEntryModel.setInvoice(invoiceModel);
                createConsignmentEntries(invoiceEntryModel, invoiceEntry, invoiceModel.getCode());
                createOrderEntries(invoiceEntryModel, invoiceEntry, invoiceModel.getCode());
                invoiceEntryList.add(invoiceEntryModel);
            } catch (final Exception e) {
                LOG.error("Exception in creating Invoice Entry for Invoice : " + invoiceModel.getCode()
                        + " Entry Number in DB:" + invoiceEntry.getInvoiceEntryNumber() + " ErrorMessage:"
                        + e.getMessage());
            }
        }
        return invoiceEntryList;
    }

    /**
     *
     */
    private void setUOMtoInvoiceEntryModel(final InvoiceEntryModel invoiceEntryModel,final InvoiceEntryImportWsDTO invoiceEntry) {
        try {
            invoiceEntryModel.setUnit(unitService.getUnitForCode(invoiceEntry.getUnitOfMeasure()));
            }catch(final Exception e){
                LOG.error("Exception while setting unitOfMeasure to invoice entry!"+e);
            }
    }

    private void createConsignmentEntries(final InvoiceEntryModel invoice, final InvoiceEntryImportWsDTO invoiceEntry,
            final String invoiceCode) {

        final List<TempConsignmentEntryModel> consignmentEntries = new ArrayList<>();
        final List<ConsignmentEntryModel> consignmentEntryModels = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(invoiceEntry.getConsignments())) {

            for (final ConsignmentEntryImportWsDTO consignmentEntry : invoiceEntry.getConsignments()) {
                try {
                    final List<ConsignmentEntryModel> consignmentEntryModelList = tkEuConsignmentService
                            .findConsignmentEntry(consignmentEntry.getConsignmentCode(),
                                    consignmentEntry.getBatchGroupNumber());

                    if (CollectionUtils.isEmpty(consignmentEntryModelList)) {
                        final TempConsignmentEntryModel tempConsignmentEntryModel = modelService
                                .create(TempConsignmentEntryModel.class);
                        tempConsignmentEntryModel.setConsignmentCode(consignmentEntry.getConsignmentCode());
                        tempConsignmentEntryModel.setBatchGroupNumber(consignmentEntry.getBatchGroupNumber());
                        consignmentEntries.add(tempConsignmentEntryModel);
                    } else {
                        consignmentEntryModels.addAll(consignmentEntryModelList);
                    }
                } catch (final Exception e) {
                    LOG.error("Exception in Mapping Consignment Entry to Invoice Entry for Invoice : " + invoiceCode
                            + " Entry Number in DB:" + invoiceEntry.getInvoiceEntryNumber() + " For Consignment:  "
                            + consignmentEntry.getConsignmentCode() + ":" + consignmentEntry.getBatchGroupNumber()
                            + "ErrorMessage:" + e.getMessage());
                }

            }
        }
        invoice.setConsignmentEntries(consignmentEntryModels);
        invoice.setTempConsignmentEntries(consignmentEntries);
    }

    private void createOrderEntries(final InvoiceEntryModel invoice, final InvoiceEntryImportWsDTO invoiceEntry,
            final String invoiceCode) {
        final List<OrderEntryModel> orderEntries = new ArrayList<>();
        final List<TempOrderEntryModel> tempOrderEntries = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(invoiceEntry.getOrderEntries())) {

            for (final OrderEntryImportWsDTO orderEntry : invoiceEntry.getOrderEntries()) {
                try {

                    final int hybrisEntryNumber = TkEuWebServiceUtils
                            .getHybrisEntryNoFromSAPEntryNo(orderEntry.getEntryNumber());
                    final OrderEntryModel orderEntryModel = tkEuOrderService
                            .findOrderEntryBySapOrderId(orderEntry.getSapOrderId(), hybrisEntryNumber);

                    if (Objects.isNull(orderEntryModel)) {
                        final TempOrderEntryModel tempOrderEntryModel = modelService.create(TempOrderEntryModel.class);
                        tempOrderEntryModel.setEntryNumber(hybrisEntryNumber);
                        tempOrderEntryModel.setSapOrderId(orderEntry.getSapOrderId());
                        tempOrderEntries.add(tempOrderEntryModel);
                    } else {
                        orderEntries.add(orderEntryModel);
                    }
                } catch (final Exception e) {
                    LOG.error("Exception in Mapping Consignment Entry to Invoice Entry for Invoice : " + invoiceCode
                            + " Entry Number in DB:" + invoiceEntry.getInvoiceEntryNumber() + " For Consignment:  "
                            + orderEntry.getSapOrderId() + ":" + orderEntry.getEntryNumber() + "ErrorMessage:"
                            + e.getMessage());
                }
            }
        }
        invoice.setTempOrderEntries(tempOrderEntries);
        invoice.setOrderEntries(orderEntries);
    }

    public TkEuInvoiceService getTkEuInvoiceService() {
        return tkEuInvoiceService;
    }

    public void setTkEuInvoiceService(final TkEuInvoiceService tkEuInvoiceService) {
        this.tkEuInvoiceService = tkEuInvoiceService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public B2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(final B2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    public TkEuCommonInboundFacade getTkEuCommonInboundFacade() {
        return tkEuCommonInboundFacade;
    }

    public void setTkEuCommonInboundFacade(final TkEuCommonInboundFacade tkEuCommonInboundFacade) {
        this.tkEuCommonInboundFacade = tkEuCommonInboundFacade;
    }


    public TkEuConsignmentService getTkEuConsignmentService() {
        return tkEuConsignmentService;
    }

    public void setTkEuConsignmentService(final TkEuConsignmentService tkEuConsignmentService) {
        this.tkEuConsignmentService = tkEuConsignmentService;
    }

    public TkEuOrderService getTkEuOrderService() {
        return tkEuOrderService;
    }

    public void setTkEuOrderService(final TkEuOrderService tkEuOrderService) {
        this.tkEuOrderService = tkEuOrderService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

}

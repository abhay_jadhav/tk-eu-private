package com.thyssenkrupp.b2b.eu.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class TkEuWebServiceUtils {
    private static final Logger LOG = Logger.getLogger(TkEuWebServiceUtils.class);

    private TkEuWebServiceUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param dateInString
     * @param format
     * @return
     */
    public static Date getDateinFormat(final String dateInString, final String format) {
        try {
            final SimpleDateFormat formatter = new SimpleDateFormat(format);

            return formatter.parse(dateInString);
        } catch (final ParseException e) {
            LOG.error("Exception in parsing date for Request : " + dateInString + " ErrorMessage:" + e.getMessage());
        }
        return null;
    }

    public static double validateAndGetDoubleValue(final String doubleInString) {
        try {
            return Double.parseDouble(doubleInString);
        } catch (final Exception e) {
            LOG.error("Exception in parsing double value for Request : " + doubleInString + " ErrorMessage:"
                    + e.getMessage());
        }
        return 0.0d;
    }

    public static <T> void logDebug(final T body, final String preMessage) {
        // if (LOG.isDebugEnabled()) {
        try {
            LOG.info(preMessage + ":" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(body)
                    .replaceAll("\\n", " ").replaceAll("\\r", " "));
        } catch (final JsonProcessingException e) {
            LOG.warn("Unable to log " + preMessage + " json");
        }
        // }
    }

    /**
     * @param entryNumber
     * @return
     */
    public static int getHybrisEntryNoFromSAPEntryNo(final String entryNumber) {
        try {
            if(StringUtils.isNotEmpty(entryNumber)) {
                return Integer.valueOf(entryNumber) - 1;
            }
        } catch (final Exception e) {
            LOG.error("Exception in parsing Int value for Request getHybrisEntryNoFromSAPEntryNo : " + entryNumber
                    + " ErrorMessage:" + e.getMessage());
            return -1;
        }
        return -1;

    }
}

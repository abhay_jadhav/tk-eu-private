/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;
import java.util.Optional;

/**
 * @author vagrant
 *
 */
public interface TkEuConsignmentDao extends Dao {
    Optional<ConsignmentModel> findConsignmentForCode(String consignmentId);

    List<ConsignmentEntryModel> findConsignmentEntry(String consignmentCode, String batchGroupNumber);

    Optional<VendorModel> findVendorByCode(String vendorCode);

}

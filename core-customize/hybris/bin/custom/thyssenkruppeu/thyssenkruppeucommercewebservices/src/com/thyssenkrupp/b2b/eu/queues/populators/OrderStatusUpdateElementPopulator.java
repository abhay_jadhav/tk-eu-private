package com.thyssenkrupp.b2b.eu.queues.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.thyssenkrupp.b2b.eu.queues.data.OrderStatusUpdateElementData;

/**
 * Class populate information from OrderModel to OrderStatusUpdateElementData
 */
public class OrderStatusUpdateElementPopulator implements Populator<OrderModel, OrderStatusUpdateElementData> {
    @Override
    public void populate(final OrderModel source, final OrderStatusUpdateElementData target)
            throws ConversionException { // NOSONAR

        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setCode(source.getCode());
        if (source.getStatus() != null) {
            target.setStatus(source.getStatus().getCode());
        }
        if (source.getSite() != null) {
            target.setBaseSiteId(source.getSite().getUid());
        }
    }
}

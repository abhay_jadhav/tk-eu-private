/**
 *
 */
package com.thyssenkrupp.b2b.eu.interceptors;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.sap.hybris.sapcustomerb2b.inbound.DefaultSAPCustomerAddressConsistencyInterceptor;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;

/**
 * @author vagrant
 *
 */
public class TkEuSAPCustomerAddressConsistencyInterceptor extends DefaultSAPCustomerAddressConsistencyInterceptor {

    private static final String SAP_ADDRESS_CONSISTENCY_ENABLED_KEY = "sap.address.consistency.interceptor.enabled";

    private static final Logger LOG = Logger.getLogger(TkEuSAPCustomerAddressConsistencyInterceptor.class);

    private FlexibleSearchService flexibleSearchService;

    @Override
    public void onPrepare(final Object model, final InterceptorContext context) throws InterceptorException {

        // Check whether the model is an address model and whether the model is modified
        if (isCheckEnabled() && model instanceof AddressModel && context.isModified(model)) {
            final AddressModel source = (AddressModel) model;

            // Check whether the SAP customer ID is not null and check if the address is
            // already queue to be saved
            if (source.getSapCustomerID() != null && !context.contains(source, PersistenceOperation.SAVE)) {
                final String sapCustomerId = source.getSapCustomerID();

                // Get all related addresses
                final List<AddressModel> relatedAddresses = getRelatedAddresses(sapCustomerId);

                // Update each address if they are different
                relatedAddresses.stream().filter(a -> compareAddress(source, a))
                        .forEach(a -> updateAddress(context, source, a));
            }
        } else {
            LOG.debug("Address Interceptor is not called");
        }
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Override
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param model
     * @return
     */
    private boolean isCheckEnabled() {

        return TkEuConfigKeyValueUtils.getBoolean(SAP_ADDRESS_CONSISTENCY_ENABLED_KEY, false);
    }

    /**
     * Retrieves addresses which are related to sapCustomerId
     * 
     * @param sapCustomerId
     * @return
     */
    @Override
    protected List<AddressModel> getRelatedAddresses(final String sapCustomerId) {
        final String query = "SELECT {PK} FROM {" + AddressModel._TYPECODE + "} WHERE {" + AddressModel.SAPCUSTOMERID
                + "} =?kunnr " + " and {" + AddressModel.DUPLICATE + "} =?duplicate";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.addQueryParameter("kunnr", sapCustomerId);
        fsQuery.addQueryParameter("duplicate", Boolean.FALSE);

        final SearchResult<AddressModel> searchResult = flexibleSearchService.search(fsQuery);
        if (searchResult != null) {
            return searchResult.getResult();
        } else {
            return Collections.emptyList();
        }
    }

}

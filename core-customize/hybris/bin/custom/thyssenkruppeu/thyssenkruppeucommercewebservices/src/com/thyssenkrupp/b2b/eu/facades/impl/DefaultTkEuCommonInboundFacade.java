package com.thyssenkrupp.b2b.eu.facades.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.constants.YcommercewebservicesConstants;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bUnitService;
import com.thyssenkrupp.b2b.eu.dto.inbound.AddressImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.TkEuCommonInboundFacade;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class DefaultTkEuCommonInboundFacade implements TkEuCommonInboundFacade {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuCommonInboundFacade.class);

    private TkEuB2bUnitService tkEuB2bUnitService;



    private CommonI18NService commonI18NService;

    @Override
    public B2BUnitModel getB2BUnit(final String unit, final String owner, final String distributionChannel) {
        String b2bUnitId = owner;
        try {
            if (YcommercewebservicesConstants.OFFLINE_DISTRIBUTION_CHANNEL.equalsIgnoreCase(distributionChannel)) {
                b2bUnitId = unit;
            }
            return tkEuB2bUnitService.getUnitForUid(b2bUnitId);
        } catch (final Exception e) {
            LOG.error("Exception in fetching b2b2Unit : " + b2bUnitId + " :" + e.getMessage());
        }
        return null;
    }

    public AddressModel createOrUpdateAddress(final AddressImportWsDTO addressDto, final boolean update,
            final AddressModel address, final Object owner, final String addressType, final String code) {
        try {
            address.setCompany(addressDto.getCompany());
            if (!update) {
                address.setPublicKey(
                        addPublicKey(addressDto.getPublicKey(), code, addressDto.getSapCustomerId(), addressType));
                address.setSapCustomerID(addressDto.getSapCustomerId());
                if (owner instanceof InvoiceModel) {
                    address.setOwner((InvoiceModel) owner);
                } else if (owner instanceof ConsignmentModel) {
                    address.setOwner((ConsignmentModel) owner);
                } else if (owner instanceof OrderModel) {
                    address.setOwner((OrderModel) owner);
                } else if (owner instanceof B2BUnitModel) {
                    address.setOwner((B2BUnitModel) owner);
                }
            }
            setAddressTypeForAddress(address, addressType);
            address.setFirstname(addressDto.getFirstName());
            address.setLastname(addressDto.getLastName());
            address.setStreetnumber(addressDto.getStreetNumber());
            address.setStreetname(addressDto.getStreetName());
            address.setPobox(addressDto.getPoBox());
            address.setPostalcode(addressDto.getPostalCode());
            address.setTown(addressDto.getTown());
            /*
             * setting department and title as null or blank so that it erase previous data
             */
            address.setDepartment("");
            address.setTitle(null);

            if (StringUtils.isNotEmpty(addressDto.getCountryISOCode())) {
                setCountryAndRegionForAddress(address, addressDto);
            }
            address.setPhone1(addressDto.getPhone());
            address.setFax(addressDto.getFax());
            return address;
        } catch (final Exception e) {
            LOG.error("Exception in setting Address : " + " :" + e.getMessage());
        }
        return null;
    }

    /**
     * @param address
     * @param addressDto
     */
    private void setCountryAndRegionForAddress(final AddressModel address, final AddressImportWsDTO addressDto) {
        try {
            final CountryModel countryModel = getCommonI18NService().getCountry(addressDto.getCountryISOCode());
            address.setCountry(countryModel);
            if (StringUtils.isNotEmpty(addressDto.getRegionISOCode()) && null != countryModel) {
                address.setRegion(getCommonI18NService().getRegion(countryModel, addressDto.getRegionISOCode()));
            }
        } catch (final Exception e) {
            LOG.error("Exception in setting Country /Region in Address : " + " :" + e.getMessage());
        }
    }

    /**
     * @param address
     * @param addressType
     */
    private void setAddressTypeForAddress(final AddressModel address, final String addressType) {
        switch (addressType) {
        case "WE":
            address.setShippingAddress(true);
            break;
        case "RE":
            address.setBillingAddress(true);
            break;
        case "AG":
            address.setContactAddress(true);
            break;
        case "Consignment":
            address.setConsignmentAddress(true);
            break;
        default:

        }
    }

    @Override
    public boolean isValidSerialNumberForRequest(final String serialNumberFromDB, final String serialNumberFromRequest,
            final String documentCode) {

        try {
            boolean valid = false;
            if (Long.valueOf(serialNumberFromDB) < Long.valueOf(serialNumberFromRequest)) {
                valid = true;
            }
            return valid;
        } catch (final Exception e) {
            LOG.error("Exception in checking the SerialKey  Value for : " + documentCode + " Serial Key in DB:"
                    + serialNumberFromDB + " Serial Key in Request:" + serialNumberFromRequest + " ErrorMessage:"
                    + e.getMessage());
            return false;
        }
    }


    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public TkEuB2bUnitService getTkEuB2bUnitService() {
        return tkEuB2bUnitService;
    }

    public void setTkEuB2bUnitService(final TkEuB2bUnitService tkEuB2bUnitService) {
        this.tkEuB2bUnitService = tkEuB2bUnitService;
    }

    private String addPublicKey(final String publicKey, final String code, final String sapCustomerId,
            final String addressType) {
        return publicKey != null ? publicKey : code + "|" + sapCustomerId + "|" + addressType;
    }
}

package com.thyssenkrupp.b2b.eu.facades.order.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.microsoft.sqlserver.jdbc.StringUtils;
import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderService;
import com.thyssenkrupp.b2b.eu.dto.inbound.VBOrderImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.TkEuCommonInboundFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderAcknowledgementInboundFacade;
import com.thyssenkruppeu.core.exception.TkEuDocumentNotValidException;

public class DefaultTkEuOrderAcknowledgementInboundFacade implements TkEuOrderAcknowledgementInboundFacade {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuOrderAcknowledgementInboundFacade.class);
    private static final String SUCCESS = "Success";
    @Resource(name = "tkEuOrderService")
    private TkEuOrderService tkEuOrderService;
    private ModelService modelService;

    private TkEuCommonInboundFacade tkEuCommonInboundFacade;

    @Override
    public String updateAndSaveOrderAcknowledgementInfo(final VBOrderImportWsDTO vbOrder) {

        final List<OrderModel> orders = tkEuOrderService.findOrderModelByCode(vbOrder.getCode());

        if (CollectionUtils.isEmpty(orders)) {
            throw new TkEuDocumentNotValidException(
                    "VBOrder " + vbOrder.getCode() + " is not valid. Rejecting because order is not present in DB!!");
        }
        /*
         * scenario 3: Rejecting VB because of older Serialization key
         *
         */
        final OrderModel latestOrder = orders.stream().filter(order -> StringUtils.isEmpty(order.getVersionID()))
                .findAny().orElse(null);

        validateVBOrderSerialKey(vbOrder, latestOrder);

        final boolean isStatusUpdate = validateStatusUpdate(latestOrder);

        if (isStatusUpdate) {

            /* Scenario 1: VB is coming first time */

            latestOrder.setNonABSerialisationKey(vbOrder.getIdocSerialNumber());
            latestOrder.setSapOrderId(vbOrder.getSapOrderId());
            latestOrder.setStatus(OrderStatus.RECEIVED_IN_ERP);
            modelService.save(latestOrder);

            LOG.info(vbOrder.getCode() + " VB Order Updated Successfully !!!");

        } else {
            /*
             * scenario 2: Status is already updated in system , so only
             * nonABserialisationeky need to update
             */

            for (final OrderModel order : orders) {
                order.setNonABSerialisationKey(vbOrder.getIdocSerialNumber());
            }
            LOG.error("Can not update order status for order: " + vbOrder.getCode() + "Latest orderStatus is :"
                    + (latestOrder != null ? latestOrder.getStatus() : "NA"));
            modelService.saveAll(orders);
        }

        return SUCCESS;
    }

    /**
     * @param latestOrder
     * @param vbOrder
     * @param orders
     * @return
     */
    private boolean validateStatusUpdate(final OrderModel latestOrder) {

        return (latestOrder != null && latestOrder.getInitialOrderVersion()
                && OrderStatus.CREATED.equals(latestOrder.getStatus()));

    }

    private void validateVBOrderSerialKey(final VBOrderImportWsDTO vbOrder, final OrderModel latestOrder) {

        if (latestOrder != null && !StringUtils.isEmpty(latestOrder.getNonABSerialisationKey())
                && !tkEuCommonInboundFacade.isValidSerialNumberForRequest(latestOrder.getNonABSerialisationKey(),
                        vbOrder.getIdocSerialNumber(), vbOrder.getCode())) {
            throw new TkEuDocumentNotValidException(
                    "VBOrder " + vbOrder.getCode() + " is not valid. Rejecting due to Older SerialKey in Request!!");
        }
    }

    public TkEuOrderService getTkEuOrderService() {
        return tkEuOrderService;
    }

    public void setTkEuOrderService(final TkEuOrderService tkEuOrderService) {
        this.tkEuOrderService = tkEuOrderService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public TkEuCommonInboundFacade getTkEuCommonInboundFacade() {
        return tkEuCommonInboundFacade;
    }

    public void setTkEuCommonInboundFacade(final TkEuCommonInboundFacade tkEuCommonInboundFacade) {
        this.tkEuCommonInboundFacade = tkEuCommonInboundFacade;
    }
}

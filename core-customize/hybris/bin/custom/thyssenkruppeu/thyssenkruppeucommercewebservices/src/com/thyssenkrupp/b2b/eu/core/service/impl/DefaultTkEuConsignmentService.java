package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.VendorModel;

import java.util.List;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuConsignmentDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConsignmentService;

public class DefaultTkEuConsignmentService implements TkEuConsignmentService {

    private TkEuConsignmentDao tkEuConsignmentDao;

    public TkEuConsignmentDao getTkEuConsignmentDao() {
        return tkEuConsignmentDao;
    }

    public void setTkEuConsignmentDao(final TkEuConsignmentDao tkEuConsignmentDao) {
        this.tkEuConsignmentDao = tkEuConsignmentDao;
    }

    @Override
    public Optional<ConsignmentModel> findConsignmentForCode(final String consignmentCode) {

        return tkEuConsignmentDao.findConsignmentForCode(consignmentCode);
    }

    @Override
    public Optional<VendorModel> findVendorByCode(final String vendorCode) {
        return tkEuConsignmentDao.findVendorByCode(vendorCode);
    }

    @Override
    public List<ConsignmentEntryModel> findConsignmentEntry(final String consignmentCode,
            final String batchGroupNumber) {
        return tkEuConsignmentDao.findConsignmentEntry(consignmentCode, batchGroupNumber);
    }
}

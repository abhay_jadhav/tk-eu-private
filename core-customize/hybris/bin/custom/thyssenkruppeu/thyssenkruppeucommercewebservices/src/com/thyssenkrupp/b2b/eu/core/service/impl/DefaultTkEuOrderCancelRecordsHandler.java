/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.dao.OrderCancelDao;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.service.TkEuOrderCancelRecordsHandler;
import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;

/**
 * @author vagrant
 *
 */
public class DefaultTkEuOrderCancelRecordsHandler extends DefaultOrderCancelRecordsHandler
        implements TkEuOrderCancelRecordsHandler {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuOrderCancelRecordsHandler.class);

    private OrderCancelDao orderCancelDao;
    private OrderHistoryService orderHistoryService;
    private UserService userService;
    private ModelService modelService;

    /**
     * @param orderModelToUpdate
     * @param orderHistoryEntryModel
     * @param snapshot
     * @param cancelledOrderEntryDtoList
     * @param request
     * @return
     */
    public boolean cancelOrderRequest(final OrderModel orderModelToUpdate,
            final OrderHistoryEntryModel orderHistoryEntryModel, final OrderModel snapshot,
            final List<ABOrderEntryImportWsDTO> cancelledOrderEntryDtoList, final OrderCancelRequest request) {
        final OrderCancelRecordModel cancelRecord = this.getOrCreateCancelRecord(orderModelToUpdate);

        final Map<Integer, AbstractOrderEntryModel> originalOrderEntriesMapping = this
                .storeOriginalOrderEntriesMapping(snapshot);
        try {
            final OrderCancelRecordEntryModel cancelRecordEntry = this.createCancelRecordEntry(request,
                    orderModelToUpdate, cancelRecord, orderHistoryEntryModel,
                    originalOrderEntriesMapping);
            cancelRecordEntry.setStatus(OrderModificationEntryStatus.SUCCESSFULL);
            modelService.save(cancelRecordEntry);
            return true;
        } catch (final OrderCancelRecordsHandlerException e) {
            LOG.error("Exception in cancelOrderRequest removd Order Entry for ABOrder " + orderModelToUpdate.getCode()
                    + " Error:" + e.getMessage(), e);
        }
        return false;
    }

    @Override
    public OrderCancelDao getOrderCancelDao() {
        return orderCancelDao;
    }

    @Override
    public void setOrderCancelDao(final OrderCancelDao orderCancelDao) {
        this.orderCancelDao = orderCancelDao;
    }

    @Override
    public OrderHistoryService getOrderHistoryService() {
        return orderHistoryService;
    }

    @Override
    public void setOrderHistoryService(final OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Override
    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

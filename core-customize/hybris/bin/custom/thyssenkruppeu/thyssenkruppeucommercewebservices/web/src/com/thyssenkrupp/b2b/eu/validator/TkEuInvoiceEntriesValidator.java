package com.thyssenkrupp.b2b.eu.validator;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceImportWsDTO;

/**
 * @author vagrant
 *
 */
public class TkEuInvoiceEntriesValidator implements Validator {

    private String fieldPath;

    @Override
    public boolean supports(final Class clazz) {
        return true;
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        Assert.notNull(errors, "Errors object must not be null");

        final InvoiceImportWsDTO invoice = (InvoiceImportWsDTO) object;
        if (null != invoice && CollectionUtils.isEmpty(invoice.getInvoiceEntries())) {
            errors.rejectValue(this.fieldPath, "", "");
        }
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(final String fieldPath) {
        this.fieldPath = fieldPath;
    }

}

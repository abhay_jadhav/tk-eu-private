package com.thyssenkrupp.b2b.eu.conv;

import de.hybris.platform.commercefacades.product.data.ReviewData;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thyssenkrupp.b2b.eu.product.data.ReviewDataList;

/**
 * Specific converter for a {@link ReviewDataList} object.
 */
public class ReviewDataListConverter extends AbstractRedirectableConverter {
    @Override
    public boolean canConvert(final Class type) {
        return type == getConvertedClass();
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
        final ReviewDataList reviews = (ReviewDataList) source;

        for (final ReviewData rd : reviews.getReviews()) {
            writer.startNode("review");
            context.convertAnother(rd);
            writer.endNode();
        }
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
        return getTargetConverter().unmarshal(reader, context);
    }

    @Override
    public Class getConvertedClass() {
        return ReviewDataList.class;
    }

}

/**
 *
 */
package com.thyssenkrupp.b2b.eu.v2.controller;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ABOrderImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.AddressImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.VBOrderImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderAcknowledgementInboundFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderInboundFacade;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/{baseSiteId}/orders")
@Api(tags = "OrderInbound")
public class OrderController extends BaseController {

    @Resource(name = "tkEuOrderAcknowledgementInboundFacade")
    private TkEuOrderAcknowledgementInboundFacade tkEuOrderAcknowledgementInboundFacade;

    @Resource(name = "tkEuOrderInboundFacade")
    private TkEuOrderInboundFacade tkEuOrderInboundFacade;

    @Resource(name = "vbOrderImportValidator")
    private Validator vbOrderImportValidator;

    @Resource(name = "abOrderImportValidator")
    private Validator abOrderImportValidator;

    @Resource(name = "abOrderEntryImportValidator")
    private Validator abOrderEntryImportValidator;

    @Resource(name = "tkCommonAddressValidator")
    private Validator tkCommonAddressValidator;

    @PutMapping(value = "/acknowledgement", consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = " Imports VB Order from ERP", notes = "Imports Inbound VB Order information for ERP ")
    @ApiBaseSiteIdParam
    public void importVBOrders(
            @ApiParam(value = "User's object.", required = true) @RequestBody final VBOrderImportWsDTO vbOrder,
            final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
            throws DuplicateUidException, UnknownIdentifierException, // NOSONAR
            IllegalArgumentException, WebserviceValidationException, UnsupportedEncodingException { // NOSONAR

        TkEuWebServiceUtils.logDebug(vbOrder, "PI/PO VB order Input Request:");
        validate(vbOrder, "vborder", vbOrderImportValidator);
        tkEuOrderAcknowledgementInboundFacade.updateAndSaveOrderAcknowledgementInfo(vbOrder);
    }

    @PostMapping(value = "/import", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = " Imports AB Order from ERP", notes = "Imports Inbound AB Order information for ERP ")
    @ApiBaseSiteIdParam
    public void importABOrders(
            @ApiParam(value = "User's object.", required = true) @RequestBody final ABOrderImportWsDTO abOrder,
            final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
            throws DuplicateUidException, UnknownIdentifierException, // NOSONAR
            IllegalArgumentException, WebserviceValidationException, UnsupportedEncodingException { // NOSONAR

        TkEuWebServiceUtils.logDebug(abOrder, "PI/PO AB order Input Request:");
        validate(abOrder, "aborder", abOrderImportValidator);
        for (final ABOrderEntryImportWsDTO entries : abOrder.getOrderEntries()) {
            validate(entries, "abOrderEntries", abOrderEntryImportValidator);
        }
        for (final AddressImportWsDTO entries : abOrder.getAddresses()) {
            validate(entries, "addresses", tkCommonAddressValidator);
        }
        tkEuOrderInboundFacade.createAndUpdateOrder(abOrder);
    }

}

package com.thyssenkrupp.b2b.eu.v2.filter;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Filter that puts user from the requested url into the session.
 */
public class UserMatchingFilter extends AbstractUrlMatchingFilter {
    private static final Logger LOG = Logger.getLogger(UserMatchingFilter.class);
    private static final String USERNAME = Config.getString("thyssenkrupp.webservice.auth.username", "Hybris_Dev");
    private static final String PASSWORD = Config.getString("thyssenkrupp.webservice.auth.password", "Bris321");

    private String regexp;
    private UserService userService;

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain filterChain) throws ServletException, IOException {
        try {
            final String authorization = request.getHeader("Authorization");
            if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
                // Authorization: Basic base64credentials
                final String base64Credentials = authorization.substring("Basic".length()).trim();
                final byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
                final String credentials = new String(credDecoded, StandardCharsets.UTF_8);
                // credentials = username:password
                final String[] values = credentials.split(":", 2);

                final String userName = values[0];
                final String password = values[1];
                if (!userName.equals(USERNAME) || !password.equals(PASSWORD)) {
                    throw new AccessDeniedException("UnAuthorized User");
                }

            } else {
                throw new AccessDeniedException("UnAuthorized User");
            }
        } catch (final Exception ex) {
            throw new AccessDeniedException("UnAuthorized User");
        }

        filterChain.doFilter(request, response);
    }

    protected String getRegexp() {
        return regexp;
    }

    @Required
    public void setRegexp(final String regexp) {
        this.regexp = regexp;
    }

    protected UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    protected boolean hasRole(final String role, final Authentication auth) {
        for (final GrantedAuthority ga : auth.getAuthorities()) {
            if (ga.getAuthority().equals(role)) {
                return true;
            }
        }
        return false;
    }

    protected void setCurrentUser(final String uid) {
        try {
            final UserModel userModel = userService.getUserForUID(uid);
            userService.setCurrentUser(userModel);
        } catch (final UnknownIdentifierException ex) {
            LOG.debug(ex.getMessage());
            throw ex;
        }
    }

    protected void setCurrentUser(final UserModel user) {
        userService.setCurrentUser(user);
    }
}

package com.thyssenkrupp.b2b.eu.v2.controller;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentEntryImportWsDTO;
import com.thyssenkrupp.b2b.eu.dto.inbound.ConsignmentImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.consignment.TkEuConsignmentInboundFacade;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/{baseSiteId}/consignments")
@Api(tags = "ConsignmentInbound")
public class ConsignmentsController extends BaseController {
    @Resource(name = "tkEuConsignmentInboundFacade")
    private TkEuConsignmentInboundFacade tkEuConsignmentInboundFacade;

    @Resource(name = "consignmentImportValidator")
    private Validator consignmentImportValidator;

    @Resource(name = "consignmentEntryImportValidator")
    private Validator consignmentEntryImportValidator;

    @PostMapping(value = "/import", consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
     @ResponseStatus(value = HttpStatus.CREATED)
     @ResponseBody
    @ApiOperation(value = " Imports Delivery IDOCs from ERP", notes = "Imports Inbound Delivery information for ERP,both Online and Offline")

    @ApiBaseSiteIdParam
    public void importInvoice(
            @ApiParam(value = "User's object.", required = true) @RequestBody final ConsignmentImportWsDTO consignment,
            final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
            throws DuplicateUidException, UnknownIdentifierException, // NOSONAR
            IllegalArgumentException, WebserviceValidationException, UnsupportedEncodingException { // NOSONAR
        TkEuWebServiceUtils.logDebug(consignment, "PI/PO Consignment Input Request:");
        validate(consignment, "Consignment", consignmentImportValidator);
        for (final ConsignmentEntryImportWsDTO entries : consignment.getConsignmentEntries()) {
            validate(entries, "ConsignmentEntries", consignmentEntryImportValidator);
        }
        tkEuConsignmentInboundFacade.createAndSaveConsignmentInfo(consignment);
    }
}

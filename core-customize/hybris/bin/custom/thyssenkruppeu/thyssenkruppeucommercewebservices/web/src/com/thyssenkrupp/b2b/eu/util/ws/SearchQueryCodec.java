package com.thyssenkrupp.b2b.eu.util.ws;

/**
 */
public interface SearchQueryCodec<QUERY> {
    QUERY decodeQuery(String query);

    String encodeQuery(QUERY query);
}

package com.thyssenkrupp.b2b.eu.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class TkEuUnitValidator implements Validator {
    private String fieldPath;

    @Override
    public boolean supports(final Class clazz) {
        return true;
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        Assert.notNull(errors, "Errors object must not be null");
        final String fieldValue = (String) errors.getFieldValue(this.fieldPath);
        if (StringUtils.isBlank(fieldValue)) {
            errors.rejectValue(this.fieldPath, "", "");
        }
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(final String fieldPath) {
        this.fieldPath = fieldPath;
    }

}

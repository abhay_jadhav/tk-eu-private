package com.thyssenkrupp.b2b.eu.context;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import javax.servlet.http.HttpServletRequest;

import com.thyssenkrupp.b2b.eu.exceptions.InvalidResourceException;
import com.thyssenkrupp.b2b.eu.exceptions.RecalculationException;
import com.thyssenkrupp.b2b.eu.exceptions.UnsupportedCurrencyException;
import com.thyssenkrupp.b2b.eu.exceptions.UnsupportedLanguageException;

/**
 * Interface for context information loader
 */
public interface ContextInformationLoader {
    /**
     * Method resolves base site uid from URL and set it as current site i.e<br>
     * <i>/rest/v1/mysite/cart</i>, or <br>
     * <i>/rest/v1/mysite/customers/current</i><br>
     * would try to set base site with uid=mysite as a current site.<br>
     *
     * @param request
     *            - request from which we should get base site uid
     *
     * @return baseSite set as current site or null
     * @throws InvalidResourceException
     */
    BaseSiteModel initializeSiteFromRequest(HttpServletRequest request) throws InvalidResourceException;

    /**
     * Method set current language base on information from request
     *
     * @param request
     *            - request from which we should get language information
     * @return language set as current
     * @throws UnsupportedLanguageException
     */
    LanguageModel setLanguageFromRequest(HttpServletRequest request) throws UnsupportedLanguageException;

    /**
     * Method set current currency based on information from request and recalculate
     * cart for current session
     *
     * @param request
     *            - request from which we should get currency information
     * @return currency set as current
     * @throws UnsupportedCurrencyException
     * @throws RecalculationException
     */
    CurrencyModel setCurrencyFromRequest(HttpServletRequest request)
            throws UnsupportedCurrencyException, RecalculationException;

}

package com.thyssenkrupp.b2b.eu.exceptions;

import javax.servlet.ServletException;

/**
 * Thrown when request is not supported for current configuration.
 */
public class UnsupportedRequestException extends ServletException {
    public UnsupportedRequestException(final String message) {
        super(message);
    }
}

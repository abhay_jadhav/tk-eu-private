package com.thyssenkrupp.b2b.eu.v2.controller;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceImportWsDTO;
import com.thyssenkrupp.b2b.eu.facades.invoice.TkEuInvoiceInboundFacade;
import com.thyssenkrupp.b2b.eu.util.TkEuWebServiceUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/{baseSiteId}/invoices")
@Api(tags = "InvoiceInbound")
public class InvoiceController extends BaseController {
    @Resource(name = "tkEuInvoiceInboundFacade")
    private TkEuInvoiceInboundFacade tkEuInvoiceInboundFacade;

    @Resource(name = "invoiceImportValidator")
    private Validator invoiceImportValidator;

    @RequestMapping(value = "/import", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    @ApiOperation(value = " Imports Invoice from ERP", notes = "Imports Inbound Invoice information for ERP ,both Online and Offline")
    @ApiBaseSiteIdParam
    public void importInvoice(
            @ApiParam(value = "User's object.", required = true) @RequestBody final InvoiceImportWsDTO invoice,
            final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
            throws DuplicateUidException, UnknownIdentifierException, // NOSONAR
            IllegalArgumentException, WebserviceValidationException, UnsupportedEncodingException { // NOSONAR

        TkEuWebServiceUtils.logDebug(invoice, "PI/PO Invoice Input Request:");
        validate(invoice, "invoice", invoiceImportValidator);
        tkEuInvoiceInboundFacade.createAndUpdateInvoice(invoice);
    }
}

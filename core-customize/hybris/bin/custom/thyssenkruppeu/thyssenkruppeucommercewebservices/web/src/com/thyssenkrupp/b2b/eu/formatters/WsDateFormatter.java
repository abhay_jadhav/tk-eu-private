package com.thyssenkrupp.b2b.eu.formatters;

import java.util.Date;

/**
 *
 * 
 */
public interface WsDateFormatter {
    Date toDate(String timestamp);

    String toString(Date date);
}

package com.thyssenkrupp.b2b.eu.exceptions;

import javax.servlet.ServletException;

/**
 *
 * 
 */
public class InvalidResourceException extends ServletException {

    private final String baseSiteId;

    /**
     * @param baseSiteUid
     */
    public InvalidResourceException(final String baseSiteUid) {
        super("Base site " + baseSiteUid + " doesn't exist");
        this.baseSiteId = baseSiteUid;
    }

    /**
     * @return the baseSiteId
     */
    public String getBaseSiteId() {
        return baseSiteId;
    }
}

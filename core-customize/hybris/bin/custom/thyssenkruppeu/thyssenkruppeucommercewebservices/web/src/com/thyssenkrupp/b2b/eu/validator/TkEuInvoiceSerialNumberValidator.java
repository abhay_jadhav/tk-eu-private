package com.thyssenkrupp.b2b.eu.validator;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thyssenkrupp.b2b.eu.dto.inbound.InvoiceImportWsDTO;

public class TkEuInvoiceSerialNumberValidator implements Validator {
    private String fieldPath;

    @Override
    public boolean supports(final Class<?> arg0) {
        return false;
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        final InvoiceImportWsDTO invoiceImport = (InvoiceImportWsDTO) object;

        if (null != invoiceImport && StringUtils.isEmpty(invoiceImport.getIdocSerialNumber())) {
            errors.rejectValue(this.fieldPath, "", "");
        }

    }

    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(final String fieldPath) {
        this.fieldPath = fieldPath;
    }

}

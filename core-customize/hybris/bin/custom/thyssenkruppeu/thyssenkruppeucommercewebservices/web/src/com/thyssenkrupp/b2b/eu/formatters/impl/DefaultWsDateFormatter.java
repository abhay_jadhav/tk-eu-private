package com.thyssenkrupp.b2b.eu.formatters.impl;

import java.util.Date;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.thyssenkrupp.b2b.eu.formatters.WsDateFormatter;

/**
 *
 * 
 */
public class DefaultWsDateFormatter implements WsDateFormatter {
    private final DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();

    @Override
    public Date toDate(final String timestamp) {
        return parser.parseDateTime(timestamp).toDate();
    }

    @Override
    public String toString(final Date date) {
        return parser.print(date.getTime());
    }

}

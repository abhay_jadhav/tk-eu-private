package com.thyssenkrupp.b2b.eu.storefrontaddon.setup;

import static com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonConstants.EXTENSIONNAME;
import static de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService.IMPORT_CORE_DATA;
import static de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService.IMPORT_SAMPLE_DATA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.log4j.Logger;

@SystemSetup(extension = EXTENSIONNAME)
public class ThyssenkruppEuStoreFrontAddonSystemSetup extends AbstractSystemSetup {
    private static final Logger LOG = Logger.getLogger(ThyssenkruppEuStoreFrontAddonSystemSetup.class);
    private static final String TK_PLASTICS = "tkPlastics";
    private static final String TK_SCHULTE = "tkSchulte";
    private static final String IMPORT_SAMPLE_DATA_ENABLED_CONFIG_KEY = "system.tkeuinitialdata.sample.enabled";
    private CoreDataImportService coreDataImportService;
    private SampleDataImportService sampleDataImportService;
    private ConfigurationService configurationService;


    @Override
    @SystemSetupParameterMethod(extension = EXTENSIONNAME)
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
        params.add(createBooleanSystemSetUpParameterForSampleData(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
        return params;
    }

    @SystemSetup(type = Type.PROJECT, process = Process.ALL, extension = EXTENSIONNAME)
    public void createProjectData(final SystemSetupContext context) {
        final List<String> websites = new ArrayList<>();
        websites.add(TK_SCHULTE);
      //  websites.add(TK_PLASTICS);
        final List<ImportData> importDataList = new ArrayList<>();

        for (final String website : websites) {
            final ImportData importData = createImportData(website);
            importDataList.add(importData);
            LOG.info("Adding website ::["+website+"]");
        }
       // Stream.of(TK_PLASTICS, TK_SCHULTE).forEach(site -> eventData.add(createImportData(site)));
        getCoreDataImportService().execute(this, context, importDataList);

        LOG.info("Sample Data Flag is set to ["+isSampleDataEnabled()+ "]");
        if(isSampleDataEnabled()){
            LOG.info("Inside Sample Data Loop and the value is set to ["+isSampleDataEnabled()+ "]");
            getSampleDataImportService().execute(this, context, importDataList);
        }
        importImpexFile(context, "/thyssenkruppeustorefrontaddon/import/coredata/common/cms-action.impex", true);

    }

    private ImportData createImportData(final String siteName) {
        final ImportData importData = new ImportData();
        importData.setProductCatalogName(siteName);
        importData.setContentCatalogNames(Collections.singletonList(siteName));
        importData.setStoreNames(Collections.singletonList(siteName));
        return importData;

    }

    public boolean isSampleDataEnabled() {
        if (null != getConfigurationService()) {
            return getConfigurationService().getConfiguration().getBoolean(IMPORT_SAMPLE_DATA_ENABLED_CONFIG_KEY,
                    true);
        }
        return false;
    }


    public SystemSetupParameter createBooleanSystemSetUpParameterForSampleData(final String key, final String label,
                                                                               final boolean defaultValue) {

        final SystemSetupParameter syncProductsParam = new SystemSetupParameter(key);
        syncProductsParam.setLabel(label);
        if (isSampleDataEnabled()) {
            syncProductsParam.addValue(BOOLEAN_TRUE, defaultValue);
            syncProductsParam.addValue(BOOLEAN_FALSE, !defaultValue);
        } else {
            syncProductsParam.addValue(BOOLEAN_FALSE, !defaultValue);
        }
        return syncProductsParam;
    }

    public CoreDataImportService getCoreDataImportService() {
        return coreDataImportService;
    }

    public void setCoreDataImportService(CoreDataImportService coreDataImportService) {
        this.coreDataImportService = coreDataImportService;
    }

    public SampleDataImportService getSampleDataImportService() {
        return sampleDataImportService;
    }

    public void setSampleDataImportService(SampleDataImportService sampleDataImportService) {
        this.sampleDataImportService = sampleDataImportService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}


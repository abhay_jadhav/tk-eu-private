package com.thyssenkrupp.b2b.eu.storefrontaddon.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.CutToLengthSelectionDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.CutToLengthSelectionHelper;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkRangeConfiguratorSettingsModel;
import de.hybris.platform.commercefacades.product.data.ProductCutToLengthData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil.stripTrailingZeros;
import static java.lang.String.format;
import static java.util.Comparator.comparingDouble;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class DefaultCutToLengthSelectionHelper implements CutToLengthSelectionHelper {

    private static final Logger                                             LOG                                 = Logger.getLogger(DefaultCutToLengthSelectionHelper.class);
    private static final String                                             C2L_TOLERANCE                       = "tolerance";
    private static final String                                             C2L_RANGE                           = "range";
    private final        Comparator<TkCutToLengthConfiguratorSettingsModel> sortToleranceSettingsByDisplayValue = Comparator.comparing(TkCutToLengthConfiguratorSettingsModel::getToleranceDisplayValue);

    private TkEuConfiguratorSettingsService configuratorSettingsService;

    private TkEuProductFacade productFacade;

    @Override
    public CutToLengthSelectionDto createCutToLengthSelectionDto(Double selectedRange, String selectedTolerance, String selectedType, boolean isInvalidRange) {
        CutToLengthSelectionDto dto = new CutToLengthSelectionDto();
        dto.setRange(selectedRange);
        dto.setTolerance(selectedTolerance);
        dto.setSelectionType(selectedType);
        dto.setInvalidRange(isInvalidRange);
        return dto;
    }

    @Override
    public Optional<TkCutToLengthConfiguredProductInfoModel> getSelectedCutToLengthConfiguredProductInfoModelFromProductInfo(AbstractOrderEntryModel orderEntry) {
        return getFirstItemFromList(getCutToLengthProductInfoFromProductInfos(orderEntry.getProductInfos()));
    }

    private List<TkCutToLengthConfiguredProductInfoModel> getCutToLengthProductInfoFromProductInfos(List<AbstractOrderEntryProductInfoModel> productInfoModels) {
        return emptyIfNull(productInfoModels).stream().filter(isProductInfoOfTypeCutToLengthAndSelected()).map(TkCutToLengthConfiguredProductInfoModel.class::cast).collect(Collectors.toList());
    }

    private Predicate<AbstractOrderEntryProductInfoModel> isProductInfoOfTypeCutToLengthAndSelected() {
        return o -> (TkCutToLengthConfiguredProductInfoModel.class.isInstance(o) && TkCutToLengthConfiguredProductInfoModel.class.cast(o).getChecked());
    }

    private <T> Optional<T> getFirstItemFromList(List<T> list) {
        return emptyIfNull(list).stream().findFirst();
    }

    @Override
    public Optional<ProductCutToLengthData> generateRangeAndToleranceData(final Collection<VariantProductModel> variants, final CutToLengthSelectionDto dto) {
        if (isNotEmpty(variants) && variantBaseProductHasCutToLengthConfigurationCategory(variants)) {
            final ResultingConfiguratorSettings rangeAndTolerances = getRangeAndToleranceConfiguratorSettings(variants, dto);
            final Set<TkRangeConfiguratorSettingsModel> rangeSettings = rangeAndTolerances.getRangeSettings();
            final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = rangeAndTolerances.getToleranceSettings();
            if (isNotEmpty(rangeSettings) && isNotEmpty(toleranceSettings)) {
                final Optional<Pair<TkRangeConfiguratorSettingsModel, TkRangeConfiguratorSettingsModel>> minAndMaxRangeSettings = getMinAndMaxRangeConfiguratorSetting(rangeSettings);
                if (minAndMaxRangeSettings.isPresent()) {
                    return Optional.of(createProductCutToLengthData(minAndMaxRangeSettings.get().getLeft(), minAndMaxRangeSettings.get().getRight(), toleranceSettings));
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<ProductCutToLengthData> generateRangeAndToleranceData(final Collection<VariantProductModel> variants) {
        return this.generateRangeAndToleranceData(variants, new CutToLengthSelectionDto());
    }

    @Override
    public List<VariantProductModel> getVariantsFilteredByCutToLengthSelection(final Collection<VariantProductModel> variants, final CutToLengthSelectionDto dto) {
        final List<VariantProductModel> resultingList = new ArrayList<>();
        if (isNotEmpty(variants)) {
            for (VariantProductModel variant : variants) {
                final Optional<ResultingConfiguratorSettings> rangeAndToleranceSettings = getAllRangeAndToleranceSettings(variant);
                rangeAndToleranceSettings.ifPresent(pair -> {
                    if (isConfiguratorSettingsValidForSelection(pair, dto)) {
                        resultingList.add(variant);
                    }
                });
            }
        }
        return resultingList;
    }

    private boolean isConfiguratorSettingsValidForSelection(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto) {
        final Set<TkRangeConfiguratorSettingsModel> rangeSettings = rangeAndTolerances.getRangeSettings();
        final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = rangeAndTolerances.getToleranceSettings();
        final Set<TkRangeConfiguratorSettingsModel> filteredRanges = nonNull(dto.getRange()) ? filterRangeConfiguratorSettingByRange(rangeSettings, dto.getRange()) : rangeSettings;
        final Set<TkCutToLengthConfiguratorSettingsModel> filteredTolerances = nonNull(dto.getTolerance()) ? filterToleranceConfiguratorSettingByTolerance(toleranceSettings, dto.getTolerance()) : toleranceSettings;
        return isNotEmpty(filteredRanges) && isNotEmpty(filteredTolerances);
    }

    private Optional<Pair<TkRangeConfiguratorSettingsModel, TkRangeConfiguratorSettingsModel>> getMinAndMaxRangeConfiguratorSetting(final Collection<TkRangeConfiguratorSettingsModel> rangeSettings) {
        final Optional<TkRangeConfiguratorSettingsModel> rangeSettingWithMin = rangeSettings.stream().min(comparingDouble(TkRangeConfiguratorSettingsModel::getMinValue));
        final Optional<TkRangeConfiguratorSettingsModel> rangeSettingWithMax = rangeSettings.stream().max(comparingDouble(TkRangeConfiguratorSettingsModel::getMaxValue));
        if (rangeSettingWithMin.isPresent() && rangeSettingWithMax.isPresent()) {
            return Optional.of(Pair.of(rangeSettingWithMin.get(), rangeSettingWithMax.get()));
        }
        return Optional.empty();
    }

    private ResultingConfiguratorSettings getRangeAndToleranceConfiguratorSettings(final Collection<VariantProductModel> variants, final CutToLengthSelectionDto dto) {
        final Set<TkRangeConfiguratorSettingsModel> resultingRangeSettings = new HashSet<>();
        final Set<TkCutToLengthConfiguratorSettingsModel> resultingToleranceSettings = new HashSet<>();
        for (VariantProductModel variant : variants) {
            final Optional<ResultingConfiguratorSettings> maybeRangeAndTolerances = getAllRangeAndToleranceSettings(variant);
            if (maybeRangeAndTolerances.isPresent()) {
                LOG.debug(format("Range and Tolerance Configuration Settings available for product:%s", variant.getCode()));
                final ResultingConfiguratorSettings rangeAndTolerances = maybeRangeAndTolerances.get();
                populateSettingsWhenSelectionTypeIsNon(rangeAndTolerances, dto, resultingRangeSettings, resultingToleranceSettings);
                populateSettingsWhenSelectionTypeIsRange(rangeAndTolerances, dto, resultingRangeSettings, resultingToleranceSettings);
                populateSettingsWhenSelectionTypeIsTolerance(rangeAndTolerances, dto, resultingRangeSettings, resultingToleranceSettings);
            }
        }
        return new ResultingConfiguratorSettings(resultingRangeSettings, resultingToleranceSettings);
    }

    private void populateSettingsWhenSelectionTypeIsNon(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto, final Set<TkRangeConfiguratorSettingsModel> resultingRange, final Set<TkCutToLengthConfiguratorSettingsModel> resultingTolerances) {
        if (Objects.isNull(dto.getSelectionType())) {
            LOG.debug("Populating Range and Tolerance Configuration Settings for selectionType:non");
            resultingRange.addAll(rangeAndTolerances.getRangeSettings());
            resultingTolerances.addAll(rangeAndTolerances.getToleranceSettings());
        }
    }

    private void populateSettingsWhenSelectionTypeIsRange(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto, final Set<TkRangeConfiguratorSettingsModel> resultingRange, final Set<TkCutToLengthConfiguratorSettingsModel> resultingTolerances) {
        final String selectedType = dto.getSelectionType();
        final Double selectedRange = dto.getRange();
        if (Objects.equals(C2L_RANGE, selectedType)) {
            final String selectedTolerance = dto.getTolerance();
            LOG.debug(format("Populating Range and Tolerance Configuration Settings based on selectionType:%s and selectedRange:%s", selectedType, selectedRange));
            final Set<TkRangeConfiguratorSettingsModel> rangeSettings = rangeAndTolerances.getRangeSettings();
            final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = rangeAndTolerances.getToleranceSettings();
            final Set<TkRangeConfiguratorSettingsModel> filteredRanges = nonNull(selectedRange) ? filterRangeConfiguratorSettingByRange(rangeSettings, selectedRange) : rangeSettings;
            // tolerance already available and user selected a range
            if (isNotEmpty(filteredRanges) && isNotEmpty(selectedTolerance)) {
                if (isNotEmpty(filterToleranceConfiguratorSettingByTolerance(toleranceSettings, selectedTolerance))) {
                    resultingRange.addAll(filteredRanges);
                }
                resultingTolerances.addAll(toleranceSettings);
            } else if (isNotEmpty(filteredRanges)) {// tolerance not available and user selected a range
                resultingRange.addAll(filteredRanges);
                resultingTolerances.addAll(toleranceSettings);
            }
        }
    }

    private void populateSettingsWhenSelectionTypeIsTolerance(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto, final Set<TkRangeConfiguratorSettingsModel> resultingRange, final Set<TkCutToLengthConfiguratorSettingsModel> resultingTolerances) {
        final String selectedType = dto.getSelectionType();
        if (Objects.equals(C2L_TOLERANCE, selectedType)) {
            final Double selectedRange = dto.getRange();
            final String selectedTolerance = dto.getTolerance();
            LOG.debug(format("Populating Range and Tolerance Configuration Settings based on selectionType:%s, selectedTolerance:%s selectedRange:%s", selectedType, selectedTolerance, dto.getRange()));
            // range already available and user selected a tolerance
            if (nonNull(selectedRange) && isNotEmpty(selectedTolerance) && !dto.isInvalidRange()) {
                populateSettingsWhenSelectionTypeIsToleranceAndToleranceAndRangeAreAvailable(rangeAndTolerances, dto, resultingRange, resultingTolerances);
            } else if (isNotEmpty(selectedTolerance)) {// range not available and user selected a tolerance
                populateSettingsWhenSelectionTypeIsToleranceAndToleranceIsAvailable(rangeAndTolerances, dto, resultingRange, resultingTolerances);
            }
        }
    }

    private void populateSettingsWhenSelectionTypeIsToleranceAndToleranceAndRangeAreAvailable(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto, final Set<TkRangeConfiguratorSettingsModel> resultingRange, final Set<TkCutToLengthConfiguratorSettingsModel> resultingTolerances) {
        final Double selectedRange = dto.getRange();
        final String selectedTolerance = dto.getTolerance();
        final Set<TkRangeConfiguratorSettingsModel> rangeSettings = rangeAndTolerances.getRangeSettings();
        final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = rangeAndTolerances.getToleranceSettings();
        final Set<TkRangeConfiguratorSettingsModel> filteredRange = filterRangeConfiguratorSettingByRange(rangeSettings, selectedRange);
        if (isNotEmpty(filteredRange)) {
            final Set<TkCutToLengthConfiguratorSettingsModel> filteredTolerances = filterToleranceConfiguratorSettingByTolerance(toleranceSettings, selectedTolerance);
            if (isNotEmpty(filteredTolerances)) {
                resultingRange.addAll(rangeSettings);
            }
            resultingTolerances.addAll(toleranceSettings);
        }
    }

    private void populateSettingsWhenSelectionTypeIsToleranceAndToleranceIsAvailable(final ResultingConfiguratorSettings rangeAndTolerances, final CutToLengthSelectionDto dto, final Set<TkRangeConfiguratorSettingsModel> resultingRange, final Set<TkCutToLengthConfiguratorSettingsModel> resultingTolerances) {
        final String selectedTolerance = dto.getTolerance();
        final Set<TkRangeConfiguratorSettingsModel> rangeSettings = rangeAndTolerances.getRangeSettings();
        final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = rangeAndTolerances.getToleranceSettings();
        final Set<TkCutToLengthConfiguratorSettingsModel> filteredTolerances = filterToleranceConfiguratorSettingByTolerance(toleranceSettings, selectedTolerance);
        if (isNotEmpty(filteredTolerances)) {
            resultingRange.addAll(rangeSettings);
        }
        resultingTolerances.addAll(toleranceSettings);
    }

    public Optional<ResultingConfiguratorSettings> getAllRangeAndToleranceSettings(final ProductModel productModel) {
        final List<AbstractConfiguratorSettingModel> allSettings = getConfiguratorSettingsService().getConfiguratorSettingsForProduct(productModel);
        return getAllRangeAndToleranceSettings(allSettings);
    }

    private Optional<ResultingConfiguratorSettings> getAllRangeAndToleranceSettings(List<AbstractConfiguratorSettingModel> allSettings) {
        final Set<TkRangeConfiguratorSettingsModel> rangeSettings = new HashSet<>();
        final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings = new HashSet<>();
        if (isNotEmpty(allSettings)) {
            for (AbstractConfiguratorSettingModel settingModel : allSettings) {
                if (TkRangeConfiguratorSettingsModel.class.isInstance(settingModel) && rangeSettings.isEmpty()) {
                    rangeSettings.add(TkRangeConfiguratorSettingsModel.class.cast(settingModel));
                } else if (TkCutToLengthConfiguratorSettingsModel.class.isInstance(settingModel)) {
                    toleranceSettings.add(TkCutToLengthConfiguratorSettingsModel.class.cast(settingModel));
                }
            }
        }
        if (isNotEmpty(rangeSettings) && isNotEmpty(toleranceSettings)) {
            return Optional.of(new ResultingConfiguratorSettings(rangeSettings, toleranceSettings));
        }
        return Optional.empty();
    }

    private Set<TkRangeConfiguratorSettingsModel> filterRangeConfiguratorSettingByRange(final Set<TkRangeConfiguratorSettingsModel> rangeSettings, final Double selectedRange) {
        return emptyIfNull(rangeSettings).stream().filter(isRangeConfiguratorSettingInRange(selectedRange)).collect(Collectors.toSet());
    }

    private Set<TkCutToLengthConfiguratorSettingsModel> filterToleranceConfiguratorSettingByTolerance(final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings, final String selectedTolerance) {
        return emptyIfNull(toleranceSettings).stream().filter(isToleranceConfiguratorSettingWithId(selectedTolerance)).collect(Collectors.toSet());
    }

    private Predicate<TkRangeConfiguratorSettingsModel> isRangeConfiguratorSettingInRange(final Double range) {
        return o -> range >= o.getMinValue() && range <= o.getMaxValue();
    }

    private Predicate<TkCutToLengthConfiguratorSettingsModel> isToleranceConfiguratorSettingWithId(final String id) {
        return o -> id.equals(o.getId());
    }

    private ProductCutToLengthData createProductCutToLengthData(TkRangeConfiguratorSettingsModel min, TkRangeConfiguratorSettingsModel max, Set<TkCutToLengthConfiguratorSettingsModel> tolerances) {
        ProductCutToLengthData data = new ProductCutToLengthData();
        data.setRangeUnit(min.getUnit().getSapCode());
        data.setMinRange(stripTrailingZeros(min.getMinValue()));
        data.setMaxRange(stripTrailingZeros(max.getMaxValue()));
        data.setTolerances(sortAndConvertToleranceSettingsToMap(tolerances));
        return data;
    }

    private Map<String, String> sortAndConvertToleranceSettingsToMap(final Collection<TkCutToLengthConfiguratorSettingsModel> toleranceSettings) {
        final Map<String, String> sortedMap = new LinkedHashMap<>(toleranceSettings.size());
        if (isNotEmpty(toleranceSettings)) {
            for (TkCutToLengthConfiguratorSettingsModel setting : sortToleranceSettingsByDisplayValue(toleranceSettings)) {
                sortedMap.put(setting.getId(), setting.getToleranceDisplayValue());
            }
        }
        return sortedMap;
    }

    private Collection<TkCutToLengthConfiguratorSettingsModel> sortToleranceSettingsByDisplayValue(final Collection<TkCutToLengthConfiguratorSettingsModel> toleranceSettings) {
        return toleranceSettings.stream().sorted(sortToleranceSettingsByDisplayValue).collect(Collectors.toList());
    }

    private boolean variantBaseProductHasCutToLengthConfigurationCategory(final Collection<VariantProductModel> variants) {
        return emptyIfNull(variants).stream().anyMatch(model -> getProductFacade().getFirstCutToLengthConfigurationCategory(model.getBaseProduct()).isPresent());
    }

    private TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public TkEuProductFacade getProductFacade() {
        return productFacade;
    }

    @Required
    public void setProductFacade(TkEuProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public static final class ResultingConfiguratorSettings {

        private final Set<TkRangeConfiguratorSettingsModel>       rangeSettings;
        private final Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings;

        private ResultingConfiguratorSettings(Set<TkRangeConfiguratorSettingsModel> rangeSettings, Set<TkCutToLengthConfiguratorSettingsModel> toleranceSettings) {
            this.rangeSettings = rangeSettings;
            this.toleranceSettings = toleranceSettings;
        }

        private Set<TkRangeConfiguratorSettingsModel> getRangeSettings() {
            return rangeSettings;
        }

        private Set<TkCutToLengthConfiguratorSettingsModel> getToleranceSettings() {
            return toleranceSettings;
        }
    }
}

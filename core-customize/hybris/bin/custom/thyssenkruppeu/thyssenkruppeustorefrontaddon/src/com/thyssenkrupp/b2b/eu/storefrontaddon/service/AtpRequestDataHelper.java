package com.thyssenkrupp.b2b.eu.storefrontaddon.service;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;

import java.util.Optional;

public interface AtpRequestDataHelper {

    Optional<AtpRequestData> buildAtpRequestData();

    Optional<AtpRequestData> buildPdpAtpRequestData(String productCode, int qty, String salesUnit, Double quantityInKg);
   Optional<AtpRequestData> buildCartAtpRequestData(int entryNumber, String productCode);
    }

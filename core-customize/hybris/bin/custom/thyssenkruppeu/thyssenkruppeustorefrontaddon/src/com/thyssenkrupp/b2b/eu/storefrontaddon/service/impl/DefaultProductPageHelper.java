package com.thyssenkrupp.b2b.eu.storefrontaddon.service.impl;

import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.ConfigurationSelectionDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.ProductPageHelper;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CHECKBOX_CHECKED_VALUE;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CERTIFICATE;
import static de.hybris.platform.catalog.enums.ConfiguratorType.CUTTOLENGTH_PRODINFO;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKTRADELENGTH;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKCCLENGTH;
import static de.hybris.platform.catalog.enums.ProductInfoStatus.*;
import static java.lang.Double.valueOf;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

public class DefaultProductPageHelper implements ProductPageHelper {

    @Override
    public List<ConfigurationInfoData> generateConfigurationInfoData(final ConfigurationSelectionDto dto) {
        final List<ConfigurationInfoData> resultingList = new ArrayList<>();
        if (Objects.nonNull(dto)) {
            generateCertificateConfigurationInfoData(dto.getCertificateId()).ifPresent(resultingList::add);
            generateCutToLengthConfigurationInfoData(dto.getRange(), dto.getRangeUnit(), dto.getTolerance()).ifPresent(resultingList::add);
            generateTradeLengthConfigurationInfoData(dto.getTradeLength()).ifPresent(resultingList::add);
            generateLengthConfigurationInfoData(dto.getConfigurableLength()).ifPresent(resultingList::add);
        }
        return resultingList;
    }

    @Override
    public Optional<ConfigurationInfoData> generateCertificateConfigurationInfoData(final String certificateId) {
        if (isNotEmpty(certificateId)) {
            ConfigurationInfoData infoData = new ConfigurationInfoData();
            infoData.setStatus(SUCCESS);
            infoData.setConfiguratorType(CERTIFICATE);
            infoData.setConfigurationValue(CHECKBOX_CHECKED_VALUE);
            infoData.setUniqueId(certificateId);
            return of(infoData);
        }
        return empty();
    }

    @Override
    public Optional<ConfigurationInfoData> generateCutToLengthConfigurationInfoData(final String range, final String rangeUnit, final String tolerance) {
        if (isNotEmpty(range) && isNotEmpty(tolerance) && isNotEmpty(rangeUnit)) {
            ConfigurationInfoData infoData = new ConfigurationInfoData();
            infoData.setStatus(SUCCESS);
            infoData.setConfiguratorType(CUTTOLENGTH_PRODINFO);
            infoData.setCutToLengthRange(valueOf(range));
            infoData.setCutToLengthRangeUnit(rangeUnit);
            infoData.setCutToLengthTolerance(tolerance);
            return of(infoData);
        }
        return empty();
    }

    @Override
    public Optional<ConfigurationInfoData> generateTradeLengthConfigurationInfoData(String tradeLengthValue) {
        if(isNotEmpty(tradeLengthValue)) {
            ConfigurationInfoData infoData = new ConfigurationInfoData();
            infoData.setStatus(SUCCESS);
            infoData.setConfiguratorType(TKTRADELENGTH);
            infoData.setConfigurationValue(CHECKBOX_CHECKED_VALUE);
            infoData.setUniqueId(tradeLengthValue);
            infoData.setConfigurationLabel(tradeLengthValue);
            return of(infoData);
        }
        return empty();
    }

    @Override
    public Optional<ConfigurationInfoData> generateLengthConfigurationInfoData(String lengthValue) {
        if(isNotEmpty(lengthValue)) {
            ConfigurationInfoData infoData = new ConfigurationInfoData();
            infoData.setStatus(SUCCESS);
            infoData.setConfiguratorType(TKCCLENGTH);
            infoData.setConfigurationValue(CHECKBOX_CHECKED_VALUE);
            infoData.setUniqueId(lengthValue);
            infoData.setConfigurationLabel(lengthValue);
            return of(infoData);
        }
        return empty();
    }
}

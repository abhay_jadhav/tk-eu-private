package com.thyssenkrupp.b2b.eu.storefrontaddon.dto;

public final class ConfigurationSelectionDto {
    private final String certificateId;
    private final String range;
    private final String rangeUnit;
    private final String tolerance;
    private final String tradeLength;
    private final String configurableLength;

    private ConfigurationSelectionDto(final Builder builder) {
        this.certificateId = builder.certificateId;
        this.range = builder.range;
        this.rangeUnit = builder.rangeUnit;
        this.tolerance = builder.tolerance;
        this.tradeLength = builder.tradeLength;
        this.configurableLength = builder.configurableLength;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String certificateId;
        private String range;
        private String rangeUnit;
        private String tolerance;
        private String tradeLength;
        private String configurableLength;

        public Builder() {
            // empty constructor
        }

        public Builder withCertificateId(final String pCertificateId) {
            this.certificateId = pCertificateId;
            return this;
        }

        public Builder withRange(final String pRange) {
            this.range = pRange;
            return this;
        }

        public Builder withRangeUnit(final String pRangeUnit) {
            this.rangeUnit = pRangeUnit;
            return this;
        }

        public Builder withTolerance(final String pTolerance) {
            this.tolerance = pTolerance;
            return this;
        }

        public Builder withTradeLength(final String pTradeLength) {
            this.tradeLength = pTradeLength;
            return this;
        }

        public Builder withConfigurableLength(final String pConfigurableLength) {
            this.configurableLength = pConfigurableLength;
            return this;
        }

        public ConfigurationSelectionDto build() {
            return new ConfigurationSelectionDto(this);
        }
    }

    public String getCertificateId() {
        return certificateId;
    }

    public String getRange() {
        return range;
    }

    public String getRangeUnit() {
        return rangeUnit;
    }

    public String getTolerance() {
        return tolerance;
    }

    public String getTradeLength() {
        return tradeLength;
    }

    public String getConfigurableLength() {
        return configurableLength;
    }
}

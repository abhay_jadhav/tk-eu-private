package com.thyssenkrupp.b2b.eu.storefrontaddon.service;

import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.CutToLengthSelectionDto;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import de.hybris.platform.commercefacades.product.data.ProductCutToLengthData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CutToLengthSelectionHelper {
    Optional<ProductCutToLengthData> generateRangeAndToleranceData(Collection<VariantProductModel> variants);

    Optional<ProductCutToLengthData> generateRangeAndToleranceData(Collection<VariantProductModel> variants, CutToLengthSelectionDto dto);

    CutToLengthSelectionDto createCutToLengthSelectionDto(Double selectedRange, String selectedTolerance, String selectedType, boolean isInvalidRange);

    List<VariantProductModel> getVariantsFilteredByCutToLengthSelection(Collection<VariantProductModel> variants, CutToLengthSelectionDto dto);

    Optional<TkCutToLengthConfiguredProductInfoModel> getSelectedCutToLengthConfiguredProductInfoModelFromProductInfo(AbstractOrderEntryModel orderEntry);
}

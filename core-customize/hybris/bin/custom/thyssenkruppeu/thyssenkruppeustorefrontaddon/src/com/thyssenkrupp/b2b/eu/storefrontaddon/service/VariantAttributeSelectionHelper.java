package com.thyssenkrupp.b2b.eu.storefrontaddon.service;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public interface VariantAttributeSelectionHelper {
    Optional<AbstractOrderEntryModel> getOrderEntryForId(String orderEntryId);

    Optional<Map<String, String>> generateSelectedVariantAttribute(AbstractOrderEntryModel orderEntry);

    Optional<Map<String, String>> generateSelectedVariantAttribute(ProductModel productModel);

    Optional<String> getSelectedTradeLengthVariantValueCategory(ProductModel productModel);

    Map<String, List<String>> generateVcToVvcRelationsMapForVariants(List<VariantProductModel> filteredVariants);

    Optional<CertificateConfiguredProductInfoModel> getSelectedCertificateFromProductInfo(AbstractOrderEntryModel orderEntry);

    Optional<Map<String, String>> getSelectedProductVariant(ProductModel productModel);

    List<VariantValueCategoryModel> getAllNonTradeLengthVariantValueCategories(ProductModel productModel);

    String constructLocalizedVariantValueCategoryString(List<VariantValueCategoryModel> variantValueCategories, Locale locale);

    Optional<Pair<String, String>> constructLocalizedTradeLengthCategoryString(String categoryCode, Locale locale);

    Optional<String> constructLocalizedCutToLengthCategoryString(String c2LRangeRange, String c2LToleranceCode,String c2LRangeUom, Locale locale);

    Optional<String> getSelectedTradeLengthVariantValueCategory(AbstractOrderEntryModel orderEntry);

}

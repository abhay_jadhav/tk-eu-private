package com.thyssenkrupp.b2b.eu.storefrontaddon.constants;

public final class ThyssenkruppeustorefrontaddonConstants extends GeneratedThyssenkruppeustorefrontaddonConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeustorefrontaddon";

    private ThyssenkruppeustorefrontaddonConstants() {
        //empty to avoid instantiating this constant class
    }
    // implement here constants used by this extension
}


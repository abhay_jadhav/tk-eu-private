package com.thyssenkrupp.b2b.eu.storefrontaddon.service.impl;

import de.hybris.platform.acceleratorservices.urlresolver.impl.DefaultSiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuSiteBaseUrlResolutionService extends DefaultSiteBaseUrlResolutionService {

    private static final String CMS_SITE_MODEL_CANNOT_BE_NULL_MSG = "CMS site model cannot be null";
    private SessionService sessionService;

    @Override
    public String getWebsiteUrlForSite(final BaseSiteModel site, final String encodingAttributes, final boolean secure,
        final String path) {
        validateParameterNotNull(site, CMS_SITE_MODEL_CANNOT_BE_NULL_MSG);
        String currentDomain = sessionService.getCurrentSession().getAttribute("asm_domain");
        String url = null;

        if (StringUtils.isNotBlank(currentDomain)) {
            final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
                url = constructUrl(request);
            }
        } else {
            url = cleanupUrl(lookupConfig("website." + site.getUid() + (secure ? ".https" : ".http")));
        }

        if (url != null) {
            if (url.contains("?")) {
                final String queryString = url.substring(url.indexOf('?'));
                final String tmpUrl = url.substring(0, url.indexOf('?'));
                return cleanupUrl(tmpUrl) + (StringUtils.isNotBlank(encodingAttributes) ? encodingAttributes : "")
                    + (path == null ? "" : path) + "/" + queryString;
            }
            return url + (StringUtils.isNotBlank(encodingAttributes) ? encodingAttributes : "") + (path == null ? "" : path);
        }
        return getDefaultWebsiteUrlForSite(site, secure, path);
    }

    private String constructUrl(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        int index = StringUtils.ordinalIndexOf(request.getRequestURL().toString(), "/", 3);
        return requestURL.substring(0,index);
    }

    @Override
    public String getWebsiteUrlForSite(final BaseSiteModel site, final boolean secure, final String path) {
        return this.getWebsiteUrlForSite(site, getUrlEncoderService().getUrlEncodingPattern(), secure, path);
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }
}

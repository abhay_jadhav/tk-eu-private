package com.thyssenkrupp.b2b.eu.storefrontaddon.service.impl;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.AtpRequestDataHelper;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;

import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class DefaultAtpRequestDataHelper implements AtpRequestDataHelper {

     private Converter atpRequestEntryConverter;
     private CartFacade cartFacade;
     private SessionService sessionService;

     @Override
     public Optional<AtpRequestData> buildAtpRequestData() {
          final CartData cartData = getCartFacade().getSessionCart();
          final List<OrderEntryData> entries = cartData.getEntries();
          if (isNotEmpty(entries)) {
               List<AtpRequestEntryData> atpRequestEntryDataList = Converters.convertAll(cartData.getEntries(),
                         atpRequestEntryConverter);
               AtpRequestData requestData = createAtpRequestData(atpRequestEntryDataList, FALSE);
               return of(requestData);
          }
          return empty();
     }

     @Override
     public Optional<AtpRequestData> buildPdpAtpRequestData(String productCode, int qty, String salesUnit,
               Double quantityInKg) {
          if (isNotEmpty(productCode) && qty > 0 && isNotEmpty(salesUnit)) {
               AtpRequestEntryData atpRequestEntryData = createAtpRequestEntryData(productCode, qty, salesUnit,
                         quantityInKg);
               AtpRequestData requestData = createAtpRequestData(singletonList(atpRequestEntryData), TRUE);
               return of(requestData);
          }
          return empty();
     }

     private AtpRequestData createAtpRequestData(List<AtpRequestEntryData> atpRequestEntryDatas, Boolean isFromPdp) {
          AtpRequestData requestData = new AtpRequestData();
          requestData.setEntries(atpRequestEntryDatas);
          requestData.setIsPdpRequest(isFromPdp);
          return requestData;
     }

     private AtpRequestEntryData createAtpRequestEntryData(String productCode, int qty, String salesUnit,
               Double quantityInKg) {
          final Session session = getSessionService().getCurrentSession();
          AtpRequestEntryData atpRequestEntryData = new AtpRequestEntryData();
          atpRequestEntryData.setProductCode(productCode);
          atpRequestEntryData.setQuantity(qty);
          atpRequestEntryData.setSalesUnit(salesUnit);
          atpRequestEntryData.setPosition(1);
          atpRequestEntryData.setQuantityInKg(quantityInKg);
          if (session != null) {
               OrderEntryData entryData = session.getAttribute("InMemoryCartEntry");
               if (entryData != null) {
                    atpRequestEntryData.setConfigurationInfos(entryData.getConfigurationInfos());
               }
               session.removeAttribute("InMemoryCartEntry");
          }
          return atpRequestEntryData;
     }

     public CartFacade getCartFacade() {
          return cartFacade;
     }

     @Required
     public void setCartFacade(CartFacade cartFacade) {
          this.cartFacade = cartFacade;
     }

     public Converter getAtpRequestEntryConverter() {
          return atpRequestEntryConverter;
     }

     @Required
     public void setAtpRequestEntryConverter(Converter atpRequestEntryConverter) {
          this.atpRequestEntryConverter = atpRequestEntryConverter;
     }

     public SessionService getSessionService() {
          return sessionService;
     }

     public void setSessionService(SessionService sessionService) {
          this.sessionService = sessionService;
     }

     @Override
     public Optional<AtpRequestData> buildCartAtpRequestData(int entryNumber, String productCode) {
          final CartData cartData = getCartFacade().getSessionCart();
          final List<OrderEntryData> entries = cartData.getEntries();
          if (isNotEmpty(entries)) {
               OrderEntryData cartEntry = getEntryData(entries, entryNumber).orElse(null);
               if (!Objects.isNull(cartEntry) && cartEntry.getProduct().getCode().equals(productCode)) {
                    AtpRequestEntryData atpRequestEntryData = (AtpRequestEntryData) atpRequestEntryConverter
                              .convert(cartEntry);
                    AtpRequestData requestData = createAtpRequestData(singletonList(atpRequestEntryData), FALSE);
                    return of(requestData);

               }
          }
          return empty();
     }

     private Optional<OrderEntryData> getEntryData(List<OrderEntryData> entries, int entryNumber) {

          return entries.stream().filter(entry -> entry.getEntryNumber() == entryNumber).findFirst();
     }

}

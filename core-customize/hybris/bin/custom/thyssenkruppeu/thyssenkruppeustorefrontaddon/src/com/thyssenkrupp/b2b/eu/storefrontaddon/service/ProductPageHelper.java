package com.thyssenkrupp.b2b.eu.storefrontaddon.service;

import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.ConfigurationSelectionDto;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;

import java.util.List;
import java.util.Optional;

public interface ProductPageHelper {

    List<ConfigurationInfoData> generateConfigurationInfoData(ConfigurationSelectionDto dto);

    Optional<ConfigurationInfoData> generateCertificateConfigurationInfoData(String certificateId);

    Optional<ConfigurationInfoData> generateCutToLengthConfigurationInfoData(String range, String rangeUnit, String tolerance);

    Optional<ConfigurationInfoData> generateTradeLengthConfigurationInfoData(String tradeLengthValue);

    Optional<ConfigurationInfoData> generateLengthConfigurationInfoData(String tradeLengthValue);
}

package com.thyssenkrupp.b2b.eu.storefrontaddon.dto;

public class CutToLengthSelectionDto {

    private Double  range;
    private String  tolerance;
    private String  selectionType;
    private boolean invalidRange;

    public Double getRange() {
        return range;
    }

    public void setRange(Double range) {
        this.range = range;
    }

    public String getTolerance() {
        return tolerance;
    }

    public void setTolerance(String tolerance) {
        this.tolerance = tolerance;
    }

    public String getSelectionType() {
        return selectionType;
    }

    public void setSelectionType(String selectionType) {
        this.selectionType = selectionType;
    }

    public boolean isInvalidRange() {
        return invalidRange;
    }

    public void setInvalidRange(boolean invalidRange) {
        this.invalidRange = invalidRange;
    }
}

package com.thyssenkrupp.b2b.eu.storefrontaddon;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppeustorefrontaddonStandalone {

    public static void main(final String[] args) {
        new ThyssenkruppeustorefrontaddonStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}


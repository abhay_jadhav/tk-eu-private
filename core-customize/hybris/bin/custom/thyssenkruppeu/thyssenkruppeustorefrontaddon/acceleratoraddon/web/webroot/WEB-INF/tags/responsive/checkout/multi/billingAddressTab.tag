<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="disableStatus" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:theme code="checkout.order.billing.address.text" var="billingAddresText"/>
<spring:theme code="checkout.order.billing.paymentterms.text" var="paymentTermsText"/>
<spring:theme code="checkout.order.billing.orderName.text" var="orderNameText"/>
<spring:theme code="checkout.order.billing.poNumber.text" var="ponumberText"/>
<spring:theme code="checkout.order.billing.incoterms" text="Incoterms" var="incotermsText"/>

<jsp:include page="addressContainerPage.jsp" >
    <jsp:param name="checkoutId" value="checkout-billing-list" />
    <jsp:param name="formName" value="tkBillingAddress" />
    <jsp:param name="action" value="${request.contextPath}/checkout/multi/billing-address/choose" />
    <jsp:param name="selectedAddressCodeId" value="selectedBillingAddressId" />
    <jsp:param name="terms" value="${termsOfPayment}" />
    <jsp:param name="selectAddress" value="selectBillingAddress" />
    <jsp:param name="springCode" value="checkout.multi.bill.to" />
    <jsp:param name="springText" value="${billingAddresText}" />
    <jsp:param name="addressDropDown" value="checkout-billing-address-drop-down" />
    <jsp:param name="selectedAddressId" value="selectedBillingAddressId" />
    <jsp:param name="checkoutAddress" value="checkout-billing-address" />
    <jsp:param name="checkoutAddressLabel" value="control-label checkout-billing-address" />
    <jsp:param name="termsSpringCode" value="checkout.multi.paymentTerms" />
    <jsp:param name="termsSpringText" value="${paymentTermsText}" />
    <jsp:param name="checkoutTermsId" value="checkout-billing-terms" />
    <jsp:param name="checkoutTermsClass" value="form-control-static" />
    <jsp:param name="nextButton" value="tkBillingAddressSubmit" />
    <jsp:param name="nextButtonId" value="checkout-billing-next-btn" />
    <jsp:param name="status" value="paymentAndBilling" />
    <jsp:param name="orderName" value="${orderNameText}" />
    <jsp:param name="poNumber" value="${ponumberText}" />
    <jsp:param name="incoterms" value="${incoterms}" />
    <jsp:param name="incotermsLabel" value="${incotermsText}" />
    <jsp:param name="billingPoNumber" value="${poNumber}" />
    <jsp:param name="billingOrderName" value="${orderName}" />
    <jsp:param name="disableStatus" value="${disableStatus}" />
</jsp:include>

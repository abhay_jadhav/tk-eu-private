/*global ACC, document, $, setTimeout, window */

ACC.minicart = {

    _autoload: [
        "bindMiniCart"
    ],

    isMiniCartActive: true,

    bindMiniCart: function () {

        var $minicartDropdown = $('.js-mini-cart-dropdown');
        var $minicartDropdownContainer = $('.js-mini-cart-dropdown-menu');

        $(document).on("click", ".js-mini-cart-link", function (e) {

            if (ACC.minicart.isMiniCartActive) {
                e.preventDefault();
                var url = $(this).data("miniCartUrl");

                var $dropdownItem = $(this).closest('.dropdown');
                $dropdownItem.toggleClass('open');

                if ($dropdownItem.hasClass('open')) {

                    ACC.minicart.setBackdropVisibility(true);

                    $.ajax({
                        url: url,
                        cache: false,
                        type: 'GET',
                        success: function (cartResult) {

                            ACC.minicart.displayMiniCartPanel(cartResult.cartHtml, cartResult.cartClosingTime);
                            $('.m-mini-cart__count').html(cartResult.cartData.products.length);
                            if(cartResult.cartData.products.length < 1) {
                                var getZeroVal = $('.js-mini-cart').attr('data-total');
                                $('.m-mini-cart__price').html(getZeroVal);
                            }
                        }
                    });
                }
            }
        });

        $('body').click(function (event) {

            if ($minicartDropdownContainer.has(event.target).length === 0 && $minicartDropdown.hasClass('open')) {

                setTimeout(function () {
                    $minicartDropdown.removeClass('open');
                    ACC.minicart.setBackdropVisibility(false);
                }, 100);
            }
        });

        $('.js-mini-cart-close').click(function (event) {
            if ($minicartDropdownContainer.has(event.target).length === 0 && $minicartDropdown.hasClass('open')) {

                setTimeout(function () {
                    $minicartDropdown.removeClass('open');
                    ACC.minicart.setBackdropVisibility(false);
                }, 100);
            }
        });

        window.enquire.register("screen and (max-width: " + window.screenXsMax + ")", {
            match : function() { ACC.minicart.isMiniCartActive = false; },
            unmatch : function() { ACC.minicart.isMiniCartActive = true; },
        });
    },

    updateMiniCartDisplay: function () {
        var miniCartRefreshUrl = $(".js-mini-cart-link").data("miniCartRefreshUrl");
        $.ajax({
            url: miniCartRefreshUrl,
            cache: false,
            type: 'GET',
            success: function (jsonData) {
                $(".js-mini-cart-link .js-mini-cart-count").text($.trim(jsonData.miniCartCount));
                $(".js-mini-cart-link .js-mini-cart-price").text($.trim(jsonData.miniCartPrice));
            }
        });
    },

    setBackdropVisibility: function(mode, coverPrimaryNav) {
        coverPrimaryNav = coverPrimaryNav || false;

        if (mode) {
            var cssClass = 'zimmer';

            if (coverPrimaryNav) {
                cssClass +=' zimmer--primary-nav';
            }

            $('<a/>', {
                class: cssClass,
                href: 'javascript:void(0);'
            }).appendTo('body');
        } else {
            $('.zimmer').remove();
        }
    },

    displayMiniCartPanel: function(cartData, cartTimeOut) {
        var $dropdown = $('.navbar-primary .navbar-right .dropdown');
        $dropdown.find('.yamm-content').html(cartData);
        $dropdown.addClass('open');
        var subTotal = $('.m-mini-cart__content__totals__value .value').html();
        $('.dropdown .m-mini-cart__price').html(subTotal);
        $(document).on('click', '.js-close-mini-cart', function(e) {
            e.preventDefault();
            $dropdown.removeClass('open');
            ACC.minicart.setBackdropVisibility(false);
        });

        if(cartTimeOut > 0) {
            setTimeout(function() {
                $dropdown.removeClass('open');
                ACC.minicart.setBackdropVisibility(false);
            }, cartTimeOut*1000);
        }
        return $dropdown;
    },
};

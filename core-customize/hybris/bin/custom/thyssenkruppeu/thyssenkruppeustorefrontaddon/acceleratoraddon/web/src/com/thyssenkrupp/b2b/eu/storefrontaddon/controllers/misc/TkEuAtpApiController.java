package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.misc;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.pojo.CartAtpRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.PdpAtpRequest;
import com.thyssenkrupp.b2b.eu.core.service.strategies.impl.DefaultTkEuCommerceUpdateCartEntryStrategy;
import com.thyssenkrupp.b2b.eu.facades.atp.TkEuAtpFacade;
import com.thyssenkrupp.b2b.eu.facades.order.data.AtpViewData;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.AtpRequestDataHelper;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;

import static com.thyssenkrupp.b2b.eu.core.utils.TkEuAtpDateUtils.convertDateToStringDateMonthYear;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_KGM_UNIT_CODE;

@RestController
@RequestMapping("/api/atp")
public class TkEuAtpApiController extends AbstractPageController {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCommerceUpdateCartEntryStrategy.class);

    @Resource(name = "atpFacade")
    private TkEuAtpFacade atpFacade;

    @Resource(name = "atpRequestDataHelper")
    private AtpRequestDataHelper atpRequestDataHelper;

    @RequestMapping(value = "/cart/checkStatus", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> checkCartStatus(@RequestBody CartAtpRequest cartAtpRequest) {
        final Optional<AtpRequestData> atpRequestData = atpRequestDataHelper.buildCartAtpRequestData( Integer.parseInt(cartAtpRequest.getEntryNumber()),cartAtpRequest.getProductCode());
        if (atpRequestData.isPresent()) {
            final AtpViewData atpViewData = atpFacade.doAtpCheckAndBuildViewResponse(atpRequestData.get());
            updateAvailabilityMessage(atpViewData);
            return new ResponseEntity<>(atpViewData, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }



    @RequestMapping(value = "/pdp/checkStatus", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> checkPdpStatus(@RequestBody PdpAtpRequest pdpAtpRequest) {
        final Optional<AtpRequestData> atpRequestData = atpRequestDataHelper.buildPdpAtpRequestData(pdpAtpRequest.getProductCode(), Integer.parseInt(pdpAtpRequest.getQty()), SAP_KGM_UNIT_CODE, Double.valueOf(pdpAtpRequest.getTotalWeight()));
        if (atpRequestData.isPresent()) {
            final AtpViewData atpViewData = atpFacade.doAtpCheckAndBuildViewResponse(atpRequestData.get());
            updateAvailabilityMessage(atpViewData);
            return new ResponseEntity<>(atpViewData, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/cart/getConsolidateDate", method = RequestMethod.POST)
    public ResponseEntity<AtpViewData> getConsolidateDate() {
         try {
            final AtpViewData atpViewData = atpFacade.removeOutOfStockProductsAndGetConsolidateResult();
            String[] args = { atpViewData.getConsolidatedDate() };
            String consolidateDateLabel = getMessageSource().getMessage("cart.availability.total.expectedDate", args,"cart.availability.total.expectedDate", getI18nService().getCurrentLocale());
            atpViewData.setConsolidatedDateLabel(consolidateDateLabel);
                return new ResponseEntity<>(atpViewData, HttpStatus.OK);

         }catch(Exception e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
    }


     private void updateAvailabilityMessage(AtpViewData atpViewData) {
        atpViewData.getEntries().forEach(e -> {
            String message = e.getAvailabilityMessage();
            String status = e.getAvailabilityStatus();
            String toolTip = e.getAvailabilityTooltip();
            LOG.info("message= {} for entry= {} ",message,e);
            LOG.info("status= {} for entry= {}",status,e);
            LOG.info("toolTip= {} for entry= {}",toolTip,e);
            if (e.getDeliveryDate() != null) {
                LOG.info("DeliveryDate is not null for entry = {}",e);
                String[] args = { convertDateToStringDateMonthYear(e.getDeliveryDate()) };
                e.setAvailabilityMessage(getMessageSource().getMessage(message, args, message, getI18nService().getCurrentLocale()));
            } else {
                LOG.info("DeliveryDate is null for entry = {}",e);
                e.setAvailabilityMessage(getMessageSource().getMessage(message, null, message, getI18nService().getCurrentLocale()));
            }

            e.setAvailabilityStatus(getMessageSource().getMessage(status, null, status, getI18nService().getCurrentLocale()));
            e.setAvailabilityTooltip(getMessageSource().getMessage(toolTip, null, toolTip, getI18nService().getCurrentLocale()));
        });
    }


}

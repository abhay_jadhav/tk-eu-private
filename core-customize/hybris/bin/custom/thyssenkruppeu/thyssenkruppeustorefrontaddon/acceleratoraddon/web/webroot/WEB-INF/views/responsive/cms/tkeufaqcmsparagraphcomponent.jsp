<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="section" value="${hashedUrl}"/>
<c:if test="${fn:contains(hashedUrl, '#')}">
    <c:set var="section" value="${fn:replace(hashedUrl, '#', '')}"/>
</c:if>
<div class="l-faq__topics__content ${isExpanded ? 'active':''}" id='${section}'>
    <h2 class='h4 l-faq__topics__content__title'>${title}</h2>
    <p>${content}</p>
</div>

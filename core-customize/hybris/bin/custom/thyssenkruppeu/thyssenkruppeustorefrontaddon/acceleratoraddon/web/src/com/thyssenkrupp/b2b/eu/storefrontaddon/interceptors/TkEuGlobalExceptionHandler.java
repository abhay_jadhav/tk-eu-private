package com.thyssenkrupp.b2b.eu.storefrontaddon.interceptors;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuCalculationException;
import com.thyssenkrupp.b2b.eu.core.exception.TkEuInvalidCartException;
import com.thyssenkrupp.b2b.eu.core.service.strategies.impl.DefaultTkEuCommerceCartCalculationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class TkEuGlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTkEuCommerceCartCalculationStrategy.class);

    @ExceptionHandler(TkEuInvalidCartException.class)
    public String handleInvalidCartException(HttpServletRequest request, TkEuInvalidCartException ex) {

        Throwable exception = ex.getCause();
        if (exception instanceof TkEuCalculationException) {
            TkEuCalculationException tkEuCalculationException = (TkEuCalculationException) exception;
            LOGGER.error("Cart Calculation Failed with following invalid entries {}", tkEuCalculationException.getInvalidItem());
        } else {
            LOGGER.error("Cart Calculation Failed with following reason", exception);
        }

        return "redirect:/";
    }
}

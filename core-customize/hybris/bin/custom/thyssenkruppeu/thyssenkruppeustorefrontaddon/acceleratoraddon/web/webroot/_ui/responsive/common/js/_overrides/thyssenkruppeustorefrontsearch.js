/*global ACC, window, $ */


ACC.autocomplete.bindSearchAutocomplete = function() {

    // extend the default autocomplete widget, to solve issue on multiple instances of the searchbox component
    $.widget("custom.yautocomplete", $.ui.autocomplete, {
        _create: function() {

            // get instance specific options form the html data attr
            var option = this.element.data("options");
            // set the options to the widget
            this._setOptions({
                minLength: option.minCharactersBeforeRequest,
                displayProductImages: option.displayProductImages,
                delay: option.waitTimeBeforeRequest,
                autocompleteUrl: option.autocompleteUrl,
                source: this.source
            });
            // call the _super()
            $.ui.autocomplete.prototype._create.call(this);
        },
        options: {
            cache: {}, // init cache per instance
            focus: function() {
                return false;
            }, // prevent textfield value replacement on item focus
            select: function(event, ui) {
                window.location.href = ui.item.url;
            }
        },
        _renderItem: function(ul, item) {
            var $link = $("<a href='" + item.url + "'/>");
            var reg = new RegExp(this.element.val(), 'gi');
            var linkText = item.value.replace(reg, function(str) {
                return '<span class="highlight">' + str + '</span>';
            });

            $link.attr('data-suggestion-type', 'keyword').append("<div class='name' title='"+ item.value +"' data-qa-id='predictive-search-name'>" + linkText + "</div>");
            var sideNote = "";
            var qaAttribute = "";
            if (item.type == "suggestedKeyword") {
                sideNote = ACC.autosuggest.term;
                qaAttribute = "predictive-search-type";
            } else if (item.type == "suggestionCategory"){
                $link.addClass('has-image').attr('data-suggestion-type', 'category').prepend("<div class='thumb'></div>");

                if (item.image) {
                    $link.find('.thumb').html("<img src='" + item.image + "'data-qa-id='predictive-search-img'/>");
                }
                sideNote = item.numProducts + ' ' + ACC.autosuggest.numProductsPrefix;
                qaAttribute = "predictive-search-products-count";
            } else if (item.type == "suggestedProduct") {
                $link.addClass('has-image').attr('data-suggestion-type', 'product').prepend("<div class='thumb'></div>");
                if (item.image) {
                    $link.find('.thumb').html("<img src='" + item.image + "'data-qa-id='predictive-search-img'/>");
                }
                if (item.price) {
                    sideNote = item.price;
                    qaAttribute = "predictive-search-price";
                }
            }
            if(sideNote.length > 0) {
                $link.addClass('has-sidenote').append("<div class='side-note' data-qa-id='" + qaAttribute + "'>" + sideNote  + "</div>");
            }
            return $("<li>").data("item.autocomplete", item).append($link).appendTo(ul);
        },
        source: function(request, response) {
            var self = this;
            var term = request.term.toLowerCase();
            if (term in self.options.cache) {
                return response(self.options.cache[term]);
            }
            $.getJSON(self.options.autocompleteUrl, {term: request.term}, function(data) {
                var autoSearchData = [];
                if (data.suggestions != null) {
                    $.each(data.suggestions, function(i, obj) {
                        autoSearchData.push({
                            value: obj.term,
                            url: ACC.config.encodedContextPath + "/search?text=" + obj.term,
                            type: "suggestedKeyword"
                        });
                    });
                }
                if (data.categorySuggestions != null) {
                    $.each(data.categorySuggestions, function(i, obj) {
                        autoSearchData.push({
                            value: ACC.autocomplete.escapeHTML(obj.displayName),
                            code: obj.code,
                            url: ACC.config.encodedContextPath + obj.seoURI,
                            numProducts: obj.numOfProducts,
                            type: "suggestionCategory",
                            image: obj.mediaURI
                        });
                    });
                }
                if (data.products != null) {
                    $.each(data.products, function(i, obj) {
                        autoSearchData.push({
                            value: ACC.autocomplete.escapeHTML(obj.name ? obj.name : obj.code),
                            code: obj.code,
                            desc: obj.description,
                            manufacturer: obj.manufacturer,
                            url: ACC.config.encodedContextPath + obj.url,
                            price: obj.price === null ? null : obj.price.formattedValue,
                            type: "suggestedProduct",
                            image: (obj.images && self.options.displayProductImages) ? obj.images[0].url : null // prevent errors if obj.images = null
                        });
                    });
                }
                self.options.cache[term] = autoSearchData;
                return response(autoSearchData);
            });
        }

    });

    var $search = $(".js-site-search-input");
    if ($search.length > 0) {
        $search.yautocomplete();
        $search.keyup(function () {
            if ($(this).val().length > 0) {
                $(this).siblings('.form-control-clear').show();
                $(this).closest('.input-group').find('.js_search_button').addClass('active');
            } else {
                $(this).siblings('.form-control-clear').hide();
                $(this).closest('.input-group').find('.js_search_button').removeClass('active');
            }
        });
    }

    $('#search-form-mobile').on('hidden.bs.collapse', function () {
        $(this).find('.js-site-search-input').css('height', '0px');
        $('.js-search-toggle').addClass('m-header__primary-nav__search-toggle').removeClass('m-header__primary-nav__search-toggle--active');
        $('.js-search-toggle .u-icon-tk').addClass('u-icon-tk--search-alt').removeClass('u-icon-tk--close-single');
    })

    $('#search-form-mobile').on('show.bs.collapse', function () {
        $(this).find('.js-site-search-input').css('height', '42px');
        $('.js-search-toggle').addClass('m-header__primary-nav__search-toggle--active').removeClass('m-header__primary-nav__search-toggle');
        $('.js-search-toggle .u-icon-tk').addClass('u-icon-tk--close-single').removeClass('u-icon-tk--search-alt');
    })
};

ACC.autocomplete.bindDisableSearch = function () {
    $('.js-site-search-input').keyup(function () {
        $(this).val($(this).val().replace(/^\s+/gm, ''));
        $(this).closest('.input-group').find('.js_search_button').prop('disabled', this.value === "" ? true : false);
    })
};

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<c:if test="${not empty address.companyName}">
    ${address.companyName},&#x20;
</c:if>
<c:if test="${not empty address.line1}">
    ${address.line1} &#x20;
</c:if>
<c:if test="${not empty address.line2}">
    ${address.line2},&#x20;
</c:if>
<c:if test="${not empty address.town}">
    ${fn:escapeXml(address.postalCode)} &#x20;${fn:escapeXml(address.town)},&#x20;
</c:if>
${fn:escapeXml(address.country.name)}&#x20;

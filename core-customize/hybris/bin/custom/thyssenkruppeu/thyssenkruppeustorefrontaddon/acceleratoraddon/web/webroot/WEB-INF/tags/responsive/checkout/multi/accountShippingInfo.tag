<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common"%>
<c:if test="${cartData.deliveryItemsQuantity > 0 and showDeliveryAddress and not empty cartData.deliveryAddress}">
    <li class="checkout-order-summary-list-heading col-md-6">
        <div class="title"><spring:theme code="checkout.pickup.items.to.be.shipped" text="Ship To:"/></div><br/>
        <div class="address" data-qa-id="order-summary-shipping-address">
            <order:address address="${cartData.deliveryAddress}" />
        </div>
    </li>
</c:if>

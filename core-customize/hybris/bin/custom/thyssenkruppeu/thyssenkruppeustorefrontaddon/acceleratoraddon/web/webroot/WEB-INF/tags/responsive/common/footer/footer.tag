<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="isMiniFooter" required="false" type="java.lang.Boolean" %>


<footer class="m-footer ${isMiniFooter ? 'm-footer--minimal' : ''}">
    <div class="m-footer__utils">
        <div class="container">
            <div class="m-footer__utils__return-to-top" data-qa-id="to-the-top-btn">
                <a href="#" class="scrollup"> <span><spring:theme code="text.footer.tothetop"/> </span></a>
            </div>
        </div>
    </div>
    <div class="m-footer__content">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <cms:pageSlot position="TkEuDivisionName" var="feature">
                        <cms:component component="${feature}" element="h6" class="m-footer__content__department" data-qa-id="name-shop"/>
                    </cms:pageSlot>
                    <cms:pageSlot position="TkEuDepartmentName" var="feature">
                        <cms:component component="${feature}" element="h4" class="m-footer__content__country" data-qa-id="name-shop"/>
                    </cms:pageSlot>
                    <cms:pageSlot position="TkEuFooterNavMenu" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
                </div>
                <div class="col-md-3">
                    <div class="m-footer__content__external-links">
                        <cms:pageSlot position="TkEuFooterCorporateSite" var="feature">
                            <cms:component component="${feature}"/>
                        </cms:pageSlot>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-footer__legal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3" data-qa-id="thyssenkrupp-copyright">
                    <cms:pageSlot position="TkEuFooterCopyright" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
                </div>
                <div class="col-lg-9" data-qa-id="navigation-footer-lnk">
                    <cms:pageSlot position="TkEuCopyrightNavMenu" var="feature" element="nav">
                        <cms:component component="${feature}" element="ul" class="m-footer__legal__links"/>
                    </cms:pageSlot>
                </div>
            </div>
        </div>
    </div>
</footer>
<c:if test="${cookie['is-cookie-terms-accepted'].value ne '1'}">
    <cms:pageSlot position="TkEuCookieSlotName" var="feature">
        <div class="m-cookie-banner js-cookie-banner animate-slide-down">
            <div class="container">
                <div class="m-cookie-banner__inner" data-qa-id="accept-cookies-section">
                    <div class="row">
                        <div class="col-md-10">
                            <cms:component component="${feature}" element="p"/>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-default js-accept-cookie-terms-button" data-qa-id="accept-cookies-button"><spring:theme code="cookieNotification.legalNotes.accept" text="Accept"/></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </cms:pageSlot>
</c:if>

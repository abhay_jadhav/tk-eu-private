package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;

import de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

public class TkEuCMSParagraphComponentRenderer extends CMSParagraphComponentRenderer {
    @Override
    public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component) throws ServletException, IOException {
        final JspWriter out = pageContext.getOut();
        out.write(component.getContent() == null ? StringUtils.EMPTY : component.getContent());
    }
}


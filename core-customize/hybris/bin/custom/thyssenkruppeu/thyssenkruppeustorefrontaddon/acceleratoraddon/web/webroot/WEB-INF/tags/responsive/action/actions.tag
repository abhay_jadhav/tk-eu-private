<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="element" required="false" type="java.lang.String"%>
<%@ attribute name="styleClass" required="false" type="java.lang.String"%>
<%@ attribute name="parentComponent" required="false" type="de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach items="${actions}" var="action" varStatus="loop">
    <c:if test="${action.visible}">
        <c:choose>
            <c:when test="${not empty element}">
                <${element} class="${fn:escapeXml(parentComponent.uid)}-${fn:escapeXml(action.uid)} ${styleClass}" data-index="${loop.index + 1}">
                    <cms:component component="${action}" parentComponent="${parentComponent}" evaluateRestriction="true"/>
                </${element}>
            </c:when>
            <c:otherwise>
                <cms:component component="${action}" parentComponent="${parentComponent}" evaluateRestriction="true"/>
            </c:otherwise>
        </c:choose>
    </c:if>
</c:forEach>

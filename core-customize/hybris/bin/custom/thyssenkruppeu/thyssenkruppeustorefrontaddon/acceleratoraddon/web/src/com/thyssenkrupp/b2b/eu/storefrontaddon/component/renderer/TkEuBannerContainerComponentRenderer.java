/*
 * Put your copyright text here
 */
package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.thyssenkrupp.b2b.eu.core.model.contents.components.TksBannerContainerComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkGenericBannerComponentModel;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

public class TkEuBannerContainerComponentRenderer<C extends TksBannerContainerComponentModel>
        extends DefaultAddOnCMSComponentRenderer<C> {

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        List<TkGenericBannerComponentModel> tkGenericBannerComponents = new ArrayList<TkGenericBannerComponentModel>();
        if (!component.getGenericBannerComponents().isEmpty()) {
            for (TkGenericBannerComponentModel genricComponent : component.getGenericBannerComponents()) {
                if (genricComponent.getVisible() && genricComponent.getRenderOption().getCode().equals(component.getRenderOption().getCode())) {
                    tkGenericBannerComponents.add(genricComponent);
                }
            }
            model.put("tkGenericBannerComponents", tkGenericBannerComponents);
            model.put("carouselTimer", component.getCarouselTimer());
            model.put("sliderType", component.getRenderOption());
            }

        return model;
    }

}

/*global navigator, document  */
/* CTKS-11441: fix for smartedit ie11 issue  */

if( (document.currentScript === undefined) && (!!navigator.userAgent.match(/Trident\/7\./)) ) {
    var scripts = document.getElementsByTagName('script');
    document.currentScript = scripts[scripts.length - 1];
}

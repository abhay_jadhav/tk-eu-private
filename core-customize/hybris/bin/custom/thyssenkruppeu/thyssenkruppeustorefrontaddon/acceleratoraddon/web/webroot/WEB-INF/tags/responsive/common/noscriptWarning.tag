<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<noscript>
  <!-- Case One - NoScript extension or JavaScript disabled in Browser settings -->
  <div class="alert alert-warning" data-qa-id="noscript-warning-message">
    <div class="container">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button">
        <span class="u-icon-tk u-icon-tk--close-single"></span>
      </button>
      <spring:theme code="global.warning.noscript"/>
    </div>
  </div>
</noscript>

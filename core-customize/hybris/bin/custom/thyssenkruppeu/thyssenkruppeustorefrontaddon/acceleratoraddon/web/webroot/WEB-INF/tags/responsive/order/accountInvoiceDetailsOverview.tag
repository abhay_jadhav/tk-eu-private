<%@ attribute name="invoiceData" required="true" type="de.hybris.platform.commercefacades.order.data.TkEuInvoiceData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:set var="invoiceData" value="${invoiceData}"/>
<c:url value="/my-account/my-deliveries/" var="deliveryDetailsUrl"/>
<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="m-order-ack">
    <div class="row">
        <div class="col-xs-12">
            <h4 data-qa-id="invoicedocument-noheading-label"><spring:theme code="myaccount.invoices.details.headline.invoice" arguments="${fn:escapeXml(invoiceData.invoiceNumber)}" text="Invoice Nr. "/>&nbsp;</h4>
        </div>
    </div>
    <hr class="m-order-ack__separator m-order-ack__separator--thick"/>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="invoicedetails-date-label"><spring:theme code="myaccount.invoices.details.invoiceDate"/></p>
                <p class="m-item-group__value" data-qa-id="invoicedetails-date"><fmt:formatDate value="${invoiceData.invoiceDate}" dateStyle="medium" type="date"/></p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="invoicedetails-incoterms-label"><spring:theme code="myaccount.invoices.details.incoterms"/></p>
                <p class="m-item-group__value" data-qa-id="invoicedetails-incoterms">${invoiceData.paymentTerms}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="invoicedetails-duedate-label"><spring:theme code="myaccount.invoices.details.dueDate"/></p>
                <p class="m-item-group__value" data-qa-id="invoicedetails-duedate"><fmt:formatDate value="${invoiceData.dueDate}" dateStyle="medium" type="date"/></p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 m-order-ack--doc-download">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="invoicedetails-doccument-label"><spring:theme code="myaccount.invoices.details.invoiceDocument"/></p>
                <p class="m-item-group__value m-download" data-qa-id="orderconfirmationdetails-document-value">
                    <spring:theme code="myaccount.invoices.details.invoiceDocument.notAvailable.tooltip" var="invoiceTooltip"/>
                    <a id="doc${invoiceData.invoiceNumber}" class="m-download--link  js-document-primary js-document-download disabled" href="/document/download" disabled="disabled" data-document-obj='{"documentNo": "${invoiceData.invoiceNumber}","positionNo":"","batchNo":"","lineItemNo":"","documenType":"INVOICE"}' data-qa-id="invoicedetails-document" >
                        <i class="u-icon-tk u-icon-tk--md m-loading-icon" data-toggle="tooltip" title="${invoiceTooltip}"></i>
                    </a>
                    <input type="hidden" value="${CSRFTokenSession}" id="csrfId"/>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="m-accordion">
    <div class="m-accordion__header collapsed" data-toggle="collapse" data-target="#order-addresses" data-qa-id="invoicedetails-addresses-heading-label">
        <h3 class="h5 m-accordion__header__title m-text-color--blue"><spring:theme code="myaccount.invoices.details.headline.addresses"/></h3>
    </div>
    <div id="order-addresses" class="m-accordion__content collapse">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="m-order-address__title" data-qa-id="invoicedetails-billingaddress-label"><spring:theme code="myaccount.invoices.details.billToAddress"/></p>
                <div class="m-order-address__block" data-qa-id="invoicedetails-billtoaddress-value">
                    <common:address address="${invoiceData.billingAddress}"/>
                </div>
            </div>

             <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="m-order-address__title" data-qa-id="invoicedetails-customeraddress-label"><spring:theme code="myaccount.invoices.details.soldToAddress"/></p>
                <div class="m-order-address__block" data-qa-id="invoicedetails-soldtoaddress-value">
                    <common:address address="${invoiceData.soldToAddress}"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="m-page-section m-page-section--no-bottom-border">
    <h3 class="h5 m-page-section__title m-page-section__title--bottom-s6 m-page-section__title--no-bottom-border" data-qa-id="invoicedetails-itemsheading-label"><spring:theme code="myaccount.invoices.details.items" text="Items" arguments="${fn:length(invoiceData.invoiceEntries)}"/></h3>

    <c:if test="${invoiceData.isInvoiceDataValid}">
    <table class="m-order-history m-order-history--bottom-s6 table ">
        <thead>
        <tr class=" hidden-xs">
            <th id="header1" class="m-order-history__th" data-qa-id="invoicedetails-posheading-label"><spring:theme code="myaccount.invoices.details.position"/></th>
            <th id="header2" class="m-order-history__th" data-qa-id="invoicedetails-qtyheading-label"><spring:theme code="myaccount.invoices.details.quantity"/></th>
            <th id="header3" class="m-order-history__th" data-qa-id="invoicedetails-productname-value"><spring:theme code="myaccount.invoices.details.productName"/></th>
            <th id="header4" class="m-order-history__th" data-qa-id="invoicedetails-prodnumber-label"><spring:theme code="myaccount.invoices.details.articleNumber"/></th>
            <th id="header6" class="m-order-history__th" data-qa-id="invoicedetails-deliverydoc-label"><spring:theme code="myaccount.invoices.details.deliveryDocument"/></th>
            <th id="header7" class="m-order-history__th m-order-history__th__total" data-qa-id="invoicedetails-total-label"><spring:theme code="myaccount.invoices.details.total"/></th>
        </tr>
        </thead>
        <tbody>
            <c:forEach var="invoiceEntry" items="${invoiceData.invoiceEntries}">
                <tr class="m-text-color--black m-order-history__tr m-order-history__tr--confirmed-orders">
                    <td headers="header5" class="m-order-history__td" data-qa-id="invoicedetails-posh-value">
                        ${invoiceEntry.invoiceEntryNumber}
                    </td>
                    <td headers="header5" class="m-order-history__td m-order-history__td--qty" data-qa-id="invoicedetails-qtyheading-value">
                        ${invoiceEntry.quantity} ${invoiceEntry.unit.name}
                    </td>
                    <td headers="header5" class="m-order-history__td" data-qa-id="invoicedetails-productname-value">
                        ${invoiceEntry.productName}
                    </td>
                    <td headers="header5" class="m-order-history__td" data-qa-id="invoicedetails-prodnumber-label">
                        ${invoiceEntry.productCode}
                    </td>
                    <td headers="header5" class="m-order-history__td text-medium" data-qa-id="invoicedetails-deliverydoc-value">
                        <c:choose>
                            <c:when test="${not empty invoiceEntry.consignmentEntries}">
                               <c:forEach var="consignmentEntry" items="${invoiceEntry.consignmentEntries}">
                              
                                <c:choose>
                                  <c:when test="${consignmentEntry.isLinkAvailable}">
                                <a href="${deliveryDetailsUrl}${ycommerce:encodeUrl(consignmentEntry.consignmentNumber)}">${fn:escapeXml(consignmentEntry.consignmentNumber)}</a><br>
                                
                                </c:when>
                                <c:otherwise>
                                ${fn:escapeXml(consignmentEntry.consignmentNumber)}<br>
                                </c:otherwise>
                                </c:choose>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </td>
                    <td headers="header5" class="m-order-history__td m-order-history__td__total text-medium" data-qa-id="invoicedetails-total-value">
                        <format:price priceData="${invoiceEntry.totalNet}"/>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
        </c:if>
    </table>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-7">
            <div class="orderTotal m-order-detail__subtotal m-order-detail__subtotal--no-padding-y">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="small">
                          <spring:theme code="myaccount.invoices.details.netPrice" />
                        </p>
                    </div>
                     <div class="col-xs-6 text-right">
                        <p class="small">
                            <format:price priceData="${invoiceData.totalNet}" />
                        </p>
                     </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <p class="small">
                            <spring:theme code="myaccount.invoices.details.taxes" />
                        </p>
                    </div>
                     <div class="col-xs-6 text-right">
                        <p class="small">
                            <format:price priceData="${invoiceData.totalTax}" />
                        </p>
                     </div>
                </div>
                <hr class="m-order-detail__subtotal__separator m-order-detail__subtotal__separator--thick">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="totals h5 m-order-detail__subtotal__total-amount-label">
                            <spring:theme code="myaccount.invoices.details.orderTotal" />
                        </div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="totals h5 m-order-detail__subtotal__total-amount-value">
                           <format:price priceData="${invoiceData.total}"/>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>

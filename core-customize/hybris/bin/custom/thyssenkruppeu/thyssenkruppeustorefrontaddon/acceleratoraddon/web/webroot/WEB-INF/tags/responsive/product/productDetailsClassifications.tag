<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="baseProductData" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showDescription" required="true" type="java.lang.Boolean" %>
<%@ attribute name="visibilityEnum" required="true" type="de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<c:set var="visibleFeatures" value="${mergedVisibleFeatureMap}"/>
<c:set var="mainVisibleFeatures" value="${visibleFeatures[visibilityEnum]}"/>
<c:set var="productName" value="${not empty product.baseProductName? product.baseProductName : product.name}"/>
<c:set var="productDescription" value="${(isVariantSelected && not empty product.description)? product.description : baseProductData.description}"/>
<c:set var="productSummary" value="${(isVariantSelected && not empty product.summary)? product.summary : baseProductData.summary}"/>

<div class="m-product-classifications">
    <c:if test="${not empty mainVisibleFeatures}">
        <div class="row">
            <div class="col-sm-12">
                <div class="m-product-classifications__title">
                        ${fn:escapeXml(productName)}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <ul class="m-product-classifications__list js-product-classifications">
                    <c:forEach items="${mainVisibleFeatures}" var="feature">
                        <li class="m-product-classifications__list__item">
                            <div class="m-product-classifications__list__item__key">${fn:escapeXml(feature.name)}:</div>
                            <div class="m-product-classifications__list__item__value">
                                <c:forEach items="${feature.featureValues}" var="value" varStatus="status">
                                    ${fn:escapeXml(value.value)}
                                    <c:choose>
                                        <c:when test="${feature.range}">
                                            ${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
                                        </c:when>
                                        <c:otherwise>
                                            ${fn:escapeXml(feature.featureUnit.symbol)}
                                            ${not status.last ? '<br/>' : ''}
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>
    <c:if test="${showDescription && (not empty productDescription or not empty productSummary)}">
        <div class="row">
            <div class="col-sm-12">
                <p class="m-product-classifications__specification-desc js-product-classifications">${ycommerce:sanitizeHTML(productDescription)} ${fn:escapeXml(productSummary)}</p>
            </div>
        </div>
    </c:if>
</div>

package com.thyssenkrupp.b2b.eu.storefrontaddon.builders;


import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TkEuProductBreadcrumbBuilder extends ProductBreadcrumbBuilder {
    private TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService;

    public List<Breadcrumb> getBreadcrumbs(final String productCode) {

        final ProductModel productModel = getProductService().getProductForCode(productCode);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Collection<CategoryModel> categoryModels = new ArrayList<>();

        final ProductModel baseProductModel = getBaseProduct(productModel);
        categoryModels.addAll(baseProductModel.getSupercategories());

        while (!categoryModels.isEmpty()) {
            CategoryModel toDisplay = processCategoryModels(categoryModels, null);
            categoryModels.clear();
            if (toDisplay != null) {
                breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
                categoryModels.addAll(toDisplay.getSupercategories());
            }
        }
        Collections.reverse(breadcrumbs);
        return breadcrumbs;
    }

    protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels,
                                                  final CategoryModel toDisplay) {
        CategoryModel categoryToDisplay = toDisplay;

        for (final CategoryModel categoryModel : categoryModels) {
            if (tkStorefrontCategoryProviderService.isStorefrontCategory(categoryModel)) {
                if (categoryToDisplay == null) {
                    categoryToDisplay = categoryModel;
                }
                if (getBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null) {
                    break;
                }
            }
        }
        return categoryToDisplay;
    }

    public TkEuStorefrontCategoryProviderService getTkStorefrontCategoryProviderService() {
        return tkStorefrontCategoryProviderService;
    }

    public void setTkStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService) {
        this.tkStorefrontCategoryProviderService = tkStorefrontCategoryProviderService;
    }
}

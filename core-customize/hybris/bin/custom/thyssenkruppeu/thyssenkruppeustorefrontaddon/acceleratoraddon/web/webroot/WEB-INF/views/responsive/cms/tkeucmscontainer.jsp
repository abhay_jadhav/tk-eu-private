<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib  prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<div class="m-page-section m-page-section--no-bottom-border">
    <c:if test="${not empty title}">
        <${titleSize} class="${fn:toLowerCase(titleSize)} m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="home-primary-header">${title}</${titleSize}>
    </c:if>
    <div data-qa-id="home-content">
        <c:forEach items="${simpleCMSComponents}" var="linkcomponent" varStatus="loop">
            <c:if test="${loop.index < componentsPerRow}">
                <cms:component component="${linkcomponent}"/>
            </c:if>
        </c:forEach>
    </div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="price" required="false" type="java.lang.String"%>
<%@ attribute name="quantity" required="false" type="java.lang.Number"%>
<%@ attribute name="dimension1" required="false" type="java.lang.String"%>
<%@ attribute name="dimension2" required="false" type="java.lang.String"%>
<%@ attribute name="dimension3" required="false" type="java.lang.String"%>
<%@ attribute name="showPrice" required="false" type="java.lang.Boolean"%>
<%@ attribute name="unitName" required="false" type="java.lang.String"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<script>
    var products = products || [];
    products.push( {
        'name': '${product.name}',
        'id': '${product.code}',
        <c:if test="${not empty dimension1}">
        'dimension1': [
            <c:forTokens var="dimension1" items="${dimension1}" delims=",">
              '${dimension1}',
            </c:forTokens>
        ],
        </c:if>
        <c:if test="${not empty dimension2}">
            'dimension2': '${dimension2}',
        </c:if>
        <c:if test="${not empty dimension3}">
            'dimension3': '${dimension3}',
        </c:if>
        <c:if test="${showPrice}">
            <c:if test="${not empty price}">
                'price': '${price}',
            </c:if>
        </c:if>
        <c:if test="${not empty quantity}">
            'quantity': ${quantity},
        </c:if>
        'category': [
            <c:forEach var="category" items="${product.normalCategories}" varStatus="loop">
                '${category.name}',
            </c:forEach>
         ],
    });
</script>

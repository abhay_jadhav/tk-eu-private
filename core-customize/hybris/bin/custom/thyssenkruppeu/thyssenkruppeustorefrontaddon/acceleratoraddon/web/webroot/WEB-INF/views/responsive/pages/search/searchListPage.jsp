<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tk-nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>

<template:page pageTitle="${pageTitle}" class="l-page l-page__search">

    <div class="container l-page__content">
        <h3 class="l-page__title--no-description" data-qa-id="plp-search-results">
            <spring:theme code="search.page.searchText.count" arguments="${searchPageData.pagination.totalNumberOfResults}"/>&nbsp<spring:theme code="search.page.searchText" arguments="${searchPageData.freeTextSearch}" argumentSeparator="|"/>
        </h3>

        <c:if test="${not empty searchPageData.spellingSuggestion}">
            <div class="row m-search-info">
                <div class="col-sm-12">
                    <tk-nav:searchSpellingSuggestion spellingSuggestion="${searchPageData.spellingSuggestion}" />
                </div>
            </div>
        </c:if>

        <div class="row">
            <div class="col-md-9 col-md-offset-3">
                <div class="m-search-results__heading">
                    <tk-nav:pagination top="true"  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 hidden-sm hidden-xs">
                <cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" >
                    <cms:component component="${feature}" element="div" />
                </cms:pageSlot>
            </div>
            <div class="col-md-9">
                <cms:pageSlot position="SearchResultsListSlot" var="feature" element="div" >
                    <cms:component component="${feature}" element="div" />
                </cms:pageSlot>

                <storepickup:pickupStorePopup/>
            </div>
        </div>
    </div>

    <%-- TODO: Remove the below contentslot after the entry is removed from impex file. --%>
    <!--div class="row">
        <div class="col-xs-3 col-md-2">
            <div>
                <cms:pageSlot position="EncouragingListSlot" var="feature" element="div" >
                <cms:component component="${feature}" element="div"/>
                </cms:pageSlot>
            </div>
            <div>
                 <cms:pageSlot position="EncouragingListSecondSlot" var="feature" element="div" >
                 <cms:component component="${feature}" element="div"/>
                 </cms:pageSlot>
            </div>
        </div>
    </div-->

</template:page>

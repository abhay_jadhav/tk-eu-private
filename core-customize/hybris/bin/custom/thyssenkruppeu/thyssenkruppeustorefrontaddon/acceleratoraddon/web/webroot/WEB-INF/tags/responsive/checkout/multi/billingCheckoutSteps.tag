<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="checkoutSteps" required="true" type="java.util.List" %>
<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<ycommerce:testId code="checkoutSteps">
    <div class="m-checkout-tab ${cssClass}">
        <c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
            <c:url value="${checkoutStep.url}" var="stepUrl"/>
            <c:choose>
                <c:when test="${checkoutStep.stepNumber > activeCheckoutStepNumber and progressBarId ne checkoutStep.progressBarId}">
                 <c:set scope="page"  var="activeCheckoutStepNumber"  value="${activeCheckoutStepNumber}"/>
                    <div class="m-checkout-tab__heading js-checkout-step ">
                        <div class="m-checkout-tab__heading__title">
                            <spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
                        </div>
                        <a href="${stepUrl}" class="m-checkout-tab__heading__edit">
                            <spring:theme code="checkout.multi.tab.edit"/>
                        </a>
                    </div>
                    <div class="m-checkout-tab__body">
                        <jsp:doBody/>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:set scope="page"  var="activeCheckoutStepNumber"  value="${checkoutStep.stepNumber}"/>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
</ycommerce:testId>

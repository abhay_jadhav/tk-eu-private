<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<c:set var="searchResultCount" value="${searchCount}"/>
<c:url value="/my-account/my-invoices/search" var="searchActionUrl"/>
<c:set var="searchUrl" value="/my-account/my-invoices/search?init=true&searchKey=${searchKey}&sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
<c:url var="resetUrl" value="/my-account/my-invoices?init=true"/>
<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:theme code="myaccount.invoices.search.startover" var="resetUrlLabel"/>
<spring:theme code="myaccount.invoices.search.placeholder" var="searchPlaceholder"/>
<spring:theme code="myaccount.invoices.button.search" var="searchBtnText"/>
<spring:theme code="myaccount.invoices.button.reset" var="resetBtnText"/>
<b2b-order:accountOrderSearchSection formID="orderInvoicesSearchForm" searchActionUrl="${searchActionUrl}"
                               searchResultCount="${searchResultCount}" searchKey="${searchKey}"
                               searchPlaceholder="${searchPlaceholder}" searchBtnText="${searchBtnText}"
                               resetBtnText="${resetBtnText}" resetUrl="${resetUrl}" resetUrlLabel="${resetUrlLabel}"
                               qaAttributeTitle="${'order-invoices-title'}" qaAttributeSearchBtn="${'myaccount-searchbox'}" qaAttributeTextbox="${'order-invoices-search-textbox'}"
                               qaAttributeResetBtn="${'myaccount-invoice-reset-button'}" currentPage="${'invoices'}">
                               </b2b-order:accountOrderSearchSection>
<c:if test="${searchResultCount ne 0 or not empty searchKey}">
    <b2b-order:orderInvoicesListing searchUrl="${searchUrl}" messageKey="text.account.orderHistory.page"></b2b-order:orderInvoicesListing>
</c:if>

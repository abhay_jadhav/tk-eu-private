<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="m-product-detail-panel__weight js-addtocart-weight-panel display-none">
    <p class="text-right">
        <c:choose>
            <c:when test="${not empty cartEntryData}">
                <em>
                    <c:if test="${cartEntryData.salesUnitData.code != cartEntryData.product.baseUnit.code}">
                        <tk-eu-format:price priceData="${cartEntryData.salesUnitData.salesToBaseUnitFactor}" isWeight="${true}" withUnit="${true}"/><br/>
                    </c:if>
                </em>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty selectedSalesUnitData and not empty product}">
                    <em>
                        <c:if test="${selectedSalesUnitData.code != product.baseUnit.code}">
                            <tk-eu-format:price priceData="${selectedSalesUnitData.salesToBaseUnitFactor}" isWeight="${true}" withUnit="${true}"/><br/>
                        </c:if>
                    </em>
                </c:if>
            </c:otherwise>
        </c:choose>
    </p>
</div>

<div class="m-product-detail-panel__weight js-addtocart-total-weight-panel display-none">
    <p class="text-right">
        <c:if test="${not empty cartEntryData && cartEntryData.salesUnitData.isWeightedUnit eq false}">
            <em>
                <span data-qa-id="total-length-label"><spring:theme code="cartentry.item.totalweight" text="Total weight"/></span>:
                <span data-qa-id="total-length-value" data-total-weight="${not empty cartEntryData.totalWeightInKg ? cartEntryData.totalWeightInKg.value : 0.0}">
                    <c:if test="${cartEntryData.salesUnitData.isWeightedUnit eq false}">
                        <tk-eu-format:price priceData="${cartEntryData.totalWeightInKg}" withUnit="${true}" isWeight="${true}"/>
                    </c:if>
                </span>
            </em>
        </c:if>
    </p>
</div>

package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;


import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonConstants;
import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;
import org.springframework.beans.factory.annotation.Required;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import java.util.Map;

public class TkEuCMSLinkComponentRenderer<C extends CMSLinkComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {
    private static final Logger LOG = Logger.getLogger(TkEuCMSLinkComponentRenderer.class);
    private static final String BLANK = "_blank";
    private static final String SELF = "_self";

    private Converter<ProductModel, ProductData> productUrlConverter;
    private Converter<CategoryModel, CategoryData> categoryUrlConverter;
    private CommerceCommonI18NService commerceCommonI18NService;

    protected String getView(final C component) {
        return "/WEB-INF/views/addons/" + ThyssenkruppeustorefrontaddonConstants.EXTENSIONNAME + "/" + getUIExperienceFolder() + "/"
                + getCmsComponentFolder() + "/" + getViewResourceName(component) + ".jsp";
    }

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {

        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        model.put("renderOption", component.getRenderOption());
        model.put("dataIcon", component.getDataIcon(getCommerceCommonI18NService().getCurrentLocale()));
        model.put("url", getUrl(component, pageContext));
        model.put("target", getTarget(component.getTarget()));
        return model;
    }

    @Required
    public void setProductUrlConverter(final Converter<ProductModel, ProductData> productUrlConverter) {
        this.productUrlConverter = productUrlConverter;
    }

    private Converter<CategoryModel, CategoryData> getCategoryUrlConverter() {
        return categoryUrlConverter;
    }

    @Required
    public void setCategoryUrlConverter(final Converter<CategoryModel, CategoryData> categoryUrlConverter) {
        this.categoryUrlConverter = categoryUrlConverter;
    }

    protected String getUrl(final CMSLinkComponentModel component, final PageContext pageContext) {
        String url = Functions.getUrlForCMSLinkComponent(component, getProductUrlConverter(), getCategoryUrlConverter());
        try {
            return UrlSupport.resolveUrl(url, null, pageContext);
        } catch (JspException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(e);
            }
        }
        return "/";
    }


    private CommerceCommonI18NService getCommerceCommonI18NService() {
        return commerceCommonI18NService;
    }

    public void setCommerceCommonI18NService(CommerceCommonI18NService commerceCommonI18NService) {
        this.commerceCommonI18NService = commerceCommonI18NService;
    }

    private Converter<ProductModel, ProductData> getProductUrlConverter() {
        return productUrlConverter;
    }

    private String getTarget(LinkTargets targetFromModel) {
        if(!LinkTargets.SAMEWINDOW.equals(targetFromModel)) {
            return BLANK;
        }
        return SELF;
    }

}

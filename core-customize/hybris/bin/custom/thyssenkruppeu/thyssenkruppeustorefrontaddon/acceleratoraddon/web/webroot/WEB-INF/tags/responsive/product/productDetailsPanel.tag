<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>
<%@ taglib prefix="product-addon" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:set var="productName" value="${not empty product.baseProductName? product.baseProductName : product.name}"/>
<c:set var="selectedUom" value="${not empty cartEntryData? cartEntryData.unit : ''}"/>
<c:url value="/login" var="userLogin"/>
<c:url value="/register/customer" var="userRegister"/>
<spring:theme var="stockNotAvailableStatus" code="product.stock.notAvailable"/>
<spring:theme var="stockNotAvailableMessage" code="product.stock.availability.notAvailable"/>
<spring:theme var="stockNotAvailableToolTip" code="pdp.availability.unavailable.tooltip"/>
<!-- this hidden field is used by js-->
<input type="hidden" class="stockNotAvailableStatus js-hidden-stockNotAvailableStatus-selector" id="stockNotAvailableStatus" value="${stockNotAvailableStatus}"/>
<input type="hidden" class="stockNotAvailableMessage js-hidden-stockNotAvailableMessage-selector" id="stockNotAvailableMessage" value="${stockNotAvailableMessage}"/>
<input type="hidden" class="stockNotAvailableToolTip js-hidden-stockNotAvailableToolTip-selector" id="stockNotAvailableToolTip" value="${stockNotAvailableToolTip}"/>
<div class="row">
    <div class="col-sm-12">
        <h3 class="l-page__title l-page__title--no-description l-page__title--mobile" data-qa-id="pdp-item-name">
            ${fn:escapeXml(productName)}
        </h3>
    </div>
</div>
<div class=" m-page-section">
    <div class="row">
        <div class="m-pdp-left-panel">
            <div class="row">
                <div class="col-md-4">
                    <tk-eu-product:productImagePanel galleryImages="${galleryImages}"/>
                </div>
                <div class="col-md-8">
                    <product-addon:productVariantSelector product="${product}"/>
                </div>
                <div class="col-md-12 visible-md visible-lg">
                    <tk-eu-product:productPageTabs device="md"/>
                </div>
            </div>
            <%--TODO: move the component location above. --%>
            <%-- <div class="row">
                <div class="col-sm-12">
                    <cms:pageSlot position="VariantSelector" var="component">
                        <cms:component component="${component}"/>
                    </cms:pageSlot>
                </div>
            </div> --%>
        </div>

        <div class="m-pdp-right-panel">
            <div class="m-pdp-action visible-xs">
                <a class="visible-xs small" href="#productSpecifications"><spring:theme code="product.view.mobile.viewDetails" /></a>
            </div>
            <div class="m-product-detail-panel js-product-detail-price-panel">
            <spring:theme code="request.availability.address.tooltip" var="tooltipMsg"/>
                <c:if test="${not isAddressAvailableInCart && not empty isAddressAvailableInCart}">
                    <c:if test="${empty primaryShippingAddressData}">
                        <div class="m-product-detail-panel__price__info" data-qa-id="pdp-no-default-address-msg">
                            <spring:theme code="request.availability.address.nodefaultaddress"/>
                            <i data-toggle="tooltip" title="${tooltipMsg}" class="u-icon-tk u-icon-tk--info m-product-detail-panel__icon-info"></i>
                        </div>
                        <div class="m-product-detail-panel__shipping-address">
                            <spring:theme code="pdp.availability.shippingAddress.add" var="addAddress" />
                            <spring:theme code="request.availability.address.lightbox.title" var="modalTitle" />
                            <spring:theme code="request.availability.address.lightbox.subtitle" var="modalSubTitle" />
                            <c:url value="/delivery-address/choose-shipping-address" var="addAddressUrl"/>
                            <a class="btn btn-block btn-primary js-add-address-link" href="${addAddressUrl}" data-qa-id="pdp-add-shipping-address" data-modal-title="${modalTitle}" data-modal-subtitle="${modalSubTitle}">${addAddress}</a>
                        </div>
                    </c:if>

                    <c:if test="${not empty primaryShippingAddressData}">
                        <div class="m-product-detail-panel__price__info" data-qa-id="pdp-address-msg">
                            <spring:theme code="request.availability.address.message"/>
                            <i data-toggle="tooltip" title="${tooltipMsg}" class="u-icon-tk u-icon-tk--info m-product-detail-panel__icon-info"></i>
                        </div>
                        <div class="m-product-detail-panel__shipping-address">
                            <div data-qa-id="pdp-shipping-address">
                                <hr class="m-product-detail-panel__seperation">
                                <common:address address="${primaryShippingAddressData}"/>
                            </div>
                            <spring:theme code="request.availability.address.change" var="changeAddress" />
                            <spring:theme code="request.availability.address.confirm" var="confirmAddress" />
                            <spring:theme code="request.availability.address.lightbox.title" var="modalTitle" />
                            <spring:theme code="request.availability.address.lightbox.subtitle" var="modalSubTitle" />
                            <c:url value="/delivery-address/choose-shipping-address" var="addAddressUrl"/>
                            <a class="btn btn-block btn-primary js-confirm-address-link m-product-detail-panel__shipping-address__btn-confirm" data-qa-id="confirm-delivery-address">${confirmAddress}</a>
                            <a class="btn btn-block btn-default js-add-address-link" data-qa-id="change-delivery-address" href="${addAddressUrl}" data-qa-id="pdp-add-shipping-address" data-modal-title="${modalTitle}" data-modal-subtitle="${modalSubTitle}">${changeAddress}</a>
                        </div>
                    </c:if>
                </c:if>
                <c:if test="${isAddressAvailableInCart or empty isAddressAvailableInCart}">
                    <%--NOTE: OOTB promotion feature. Enable if required.
                    <product:productPromotionSection product="${product}"/> --%>
                        <c:choose>
                            <c:when test="${product.stock.stockLevelStatus.code eq 'notAvailable'}">
                                <c:set var="maxQty" value="9999"/>
                            </c:when>
                            <c:when test="${not empty product.stock.stockLevel}">
                                <c:set var="maxQty" value="${product.stock.stockLevel}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="maxQty" value="UNLIMITED"/>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${(empty showAddToCart ? true : showAddToCart)}">

                                <c:url value="${product.url}/updateSalesUom" var="uomSelectorActionUrl"/>
                                <c:if test = "${cartEntryData.isSupportedUomPresent || product.purchasable eq false}">
                                    <div class="m-product-detail-panel__quantity">
                                        <form:form method="post" name="uomSelectorForm" commandName="uomSelectorForm" id="uomSelectorForm" class="form-horizontal m-quantity-control__from" action="${uomSelectorActionUrl}">
                                            <c:set var="supportedSalesUnits" value="${(not empty cartEntryData and not empty cartEntryData.supportedSalesUnits)?cartEntryData.supportedSalesUnits : product.supportedSalesUnits}"/>
                                            <c:if test="${not empty supportedSalesUnits}">
                                                <div class="form-group  js-qty-selector m-quantity-control__group">
                                                    <label class="control-label col-xs-3 m-quantity-control__label">
                                                        <spring:theme code="text.account.order.qty" text="Qty"/>
                                                    </label>
                                                    <div class="controls col-xs-9">
                                                        <div class="input-group m-quantity-control__input-group">
                                                            <input type="text" maxlength="5" class="form-control qty-selector js-qty-selector-input" size="1" value="1" data-max="${maxQty}" data-min="1" name="pdpAddtoCartInput" id="pdpAddtoCartInput"/>
                                                            <c:if test="${not empty supportedSalesUnits}">
                                                                <c:set var="showButton" value="False"/>
                                                                <c:set var="defaultUnit" value="False"/>
                                                                <c:forEach items="${supportedSalesUnits}" var="salesUnit">
                                                                    <c:if test="${salesUnit.isDefaultUnit}">
                                                                        <c:set var="defaultUnit" value="True"/>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <c:choose>
                                                                    <c:when test="${defaultUnit and fn:length(supportedSalesUnits) gt 1}">
                                                                        <c:forEach items="${supportedSalesUnits}" var="salesUnit">
                                                                            <c:if test="${salesUnit.isDefaultUnit}">
                                                                                <c:set var="selectedUom" value="${salesUnit.code}"/>
                                                                                <c:set var="selectedUomName" value="${salesUnit.name}"/>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </c:when>
                                                                    <c:when test="${fn:length(supportedSalesUnits) eq 1}">
                                                                        <c:set var="showButton" value="True"/>
                                                                        <c:set var="selectedUom" value="${supportedSalesUnits[0].code}"/>
                                                                        <c:set var="selectedUomName" value="${supportedSalesUnits[0].name}"/>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <c:set var="showButton" value="False"/>
                                                                        <c:set var="selectedUom" value="${supportedSalesUnits[0].code}"/>
                                                                        <c:set var="selectedUomName" value="${supportedSalesUnits[0].name}"/>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <c:if test="${showButton}">
                                                                <span class="input-group-btn m-quantity-control__uom-selector" data-qa-id="pdp-uom-selector">
                                                                    <button type="button" class="btn dropdown-toggle m-quantity-control__uom-selector__btn js-dropdown-uom-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-value="${selectedUom}">
                                                                        <span class="js-selected-uom">${selectedUomName}</span>
                                                                    </button>
                                                                <span>
                                                                </c:if>
                                                                <c:if test="${not showButton}">
                                                                    <span class="input-group-btn m-quantity-control__uom-selector" data-qa-id="pdp-uom-selector">
                                                                        <button type="button" class="btn dropdown-toggle m-quantity-control__uom-selector__btn js-dropdown-uom-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-value="${selectedUom}">
                                                                            <span class="js-selected-uom">${selectedUomName}</span> <span class="caret"></span>
                                                                        </button>
                                                                        <ul class="dropdown-menu dropdown-menu-right js-dropdown-uom-values">
                                                                            <c:forEach items="${supportedSalesUnits}" var="salesUnit">
                                                                                <li><a href="#" data-value="${salesUnit.code}">${salesUnit.name}</a></li>
                                                                            </c:forEach>
                                                                        </ul>
                                                                    </span>
                                                                </c:if>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                            <input type="hidden" name="qty" class="qty js-qty-selector-input" id="uomSelectorForm-qty" value="1"/>
                                            <input type="hidden" name="salesUom" class="salesUom js-hidden-uom-selector-input" id="uomSelectorForm-salesUom" value="${selectedUom}"/>
                                            <input type="hidden" name="certOption" class="certOption js-hidden-certOption-selector-input" id="uomSelectorForm-certOption" data-qa-id="pdp-cert-code" value="none"/>
                                            <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
                                            <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
                                            <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
                                            <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${variantSelectionForm.selectedTradeLength}"/>
                                            <!-- this hidden field is used by js-->
                                            <input type="hidden" name="isProductPurchasable" class="js-hidden-isProductPurchasable-selector" id="isProductPurchasable-salesUom" value="${product.purchasable}"/>
                                            <input type="hidden" name="totalWeight" class="totalWeight js-total-weight-selector-input" id="totalWeight" value="${not empty cartEntryData.totalWeightInKg ? cartEntryData.totalWeightInKg.value : 0.0}"/>
                                        </form:form>
                                    </div>
                                </c:if>
                        </c:if>
                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">

                        <div class="m-product-detail-panel__specs">
                             <c:if test="${cartEntryData.isSupportedUomPresent}">
                                 <tk-eu-product:productAddToCartWeightPanel/>
                             </c:if>
                             <tk-eu-product:productAddToCartLengthPanel/>
                        </div>

                        <div class="m-product-detail-panel__price js-addtocart-price-panel display-none">
                            <tk-eu-product:productPricePanel product="${product}"/>
                            <c:if test="${product.configurable and certificateProduct}">
                                <tk-eu-product:productCertificateConfigurator configurations="${configurations}"/>
                            </c:if>
                            <tk-eu-product:productCartEntryPricePanel/>
                        </div>
                        <div class="m-product-detail-panel__price__info js-addtocart-price-panel-text display-none" data-qa-id="address-msg">
                            <spring:theme code="product.price.variant.select.text"/>
                        </div>

                        <div class="m-product-detail-panel__atp-detail js-addtocart-atp-detail" data-qa-id="pdp-info-msg">

                        </div>
                    </sec:authorize>
                    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                        <div class="m-product-detail-panel__message__text" data-qa-id="pdp-login-msg">
                            <small>
                                <spring:theme code="pdp.addtocart.notloggedin"/>
                            </small>
                        </div>
                        <div class="m-product-detail-panel__message__text text-center">
                            <small>
                                <spring:theme code="pdp.addtocart.login" var="loginLabel"/>
                                <a href="${userLogin}" dialogue-title="${loginLabel}" class="js-login-popup-link" data-qa-id="pdp-login-link">${loginLabel}</a>
                                <spring:theme code="pdp.addtocart.or"/>&nbsp;
                                <a class="js-register-link" href="${userRegister}" data-qa-id="pdp-register-link"><spring:theme code="pdp.addtocart.register"/></a>
                            </small>
                            <hr class="m-product-detail-panel__separator--bottom"/>
                        </div>
                    </sec:authorize>
                    <cms:pageSlot position="AddToCart" var="component" element="div" class="m-product-detail-panel__action-controls">
                        <cms:component component="${component}"/>
                    </cms:pageSlot>
                </c:if>
            </div>
            <c:if test="${isAddressAvailableInCart or empty isAddressAvailableInCart}">
                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                    <div class="m-product-detail-panel__message display-none js-default-addtocart-text">
                        <div class="m-product-detail-panel__message__text">
                            <spring:theme code="product.addtocartcomponent.default.message"/>
                        </div>
                    </div>
                </sec:authorize>
            </c:if>
        </div>
    </div>
    <div class="row visible-xs visible-sm custom-tab" id="productSpecifications">
        <div class="col-md-12">
            <tk-eu-product:productPageTabs device="sm" />
        </div>
    </div>
</div>
<spring:theme code="pdp.product.addToCart.successMessage" var="cartSuccessMsg" />
<div class="hidden js-cart-popup-mobile" data-title="${cartSuccessMsg}">
    <span class="u-icon-tk u-icon-tk--icon-check m-text-color--blue-brand"></span>
    <a class="btn btn-primary btn-block" href="/cart"><spring:theme code="pdp.product.addToCart.cart" /></a>
    <a class="btn btn-default btn-block js-continue-shopping" href="/"><spring:theme code="pdp.product.addToCart.continueShopping" /></a>
</div>
<div class="js-gtm-data">
    <gtm:productObjectGTM product="${product}"/>
    <gtm:productDetailGTM product="${product}"/>
</div>
<storepickup:pickupStorePopup/>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="row">
    <div class="col-md-7">
        <h3 class="l-page__title--no-description" data-qa-id="confirmation-success">
            <spring:theme code="checkout.orderConfirmation.thankYouForOrder" />
        </h3>
    </div>
</div>
<h4 class="m-order-confirmation__message" data-qa-id="confirmation-email-delivary-msg">
    <spring:theme code="checkout.orderConfirmation.copySentToShort" />&nbsp;${email}
</h4>

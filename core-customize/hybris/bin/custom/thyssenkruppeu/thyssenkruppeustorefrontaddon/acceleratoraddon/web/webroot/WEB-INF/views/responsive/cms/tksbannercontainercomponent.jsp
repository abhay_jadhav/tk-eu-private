<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="owl-carousel banner-slider owl-theme" data-carousel-timer="${carouselTimer}" data-slider-type="${sliderType}">
        <c:forEach items="${tkGenericBannerComponents}" var="tkEuGenericBannercomponent"
        varStatus="loop">
        <div class="item">
            <cms:component component="${tkEuGenericBannercomponent}" />
        </div>
    </c:forEach>
</div>

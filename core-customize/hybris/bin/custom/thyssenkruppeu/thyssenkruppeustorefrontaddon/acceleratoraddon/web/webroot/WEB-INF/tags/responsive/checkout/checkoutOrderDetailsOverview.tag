<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="m-accordion">
    <div class="m-accordion__header" data-qa-id="order-information-header" data-toggle="collapse" data-target="#order-information">
        <h3 class="h5 m-accordion__header__title m-text-color--blue">
            <spring:theme code="checkout.orderConfirmation.title" text="Order information"/>
        </h3>
    </div>
    <div id="order-information" class="m-accordion__content collapse in">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-order-number-label"><spring:theme code="checkout.orderConfirmation.orderNumber"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-order-number-value">${fn:escapeXml(orderData.code)}</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-date-submitted-label"><spring:theme code="checkout.orderConfirmation.datePlaced" text="Date submitted"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-date-submitted-value"><fmt:formatDate value="${order.created}" dateStyle="medium" timeStyle="short" type="both"/></p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-order-total-label"><spring:theme code="checkout.orderConfirmation.total"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-order-total-value"><format:price priceData="${order.totalPrice}"/></p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-order-status-label"><spring:theme code="text.account.orderHistory.orderStatus"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-order-status-value">${orderData.orderStatusName}&nbsp;</p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-order-name-label"><spring:theme code="checkout.orderConfirmation.orderName"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-order-name-value">${fn:escapeXml(orderData.name)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-po-number-label"><spring:theme code="checkout.orderConfirmation.purchaseOrderNumber"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-po-number-value">${orderData.paymentType.code=='ACCOUNT' and not empty orderData.purchaseOrderNumber ? fn:escapeXml(orderData.purchaseOrderNumber) : ""}&nbsp;</p>
                </div>
            </div>
        </div>

        <%--NOTE: Remove if not required -- start  --%>
        <c:if test="${orderData.paymentType.code eq 'ACCOUNT' and orderData.costCenter}">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label"><spring:theme code="text.account.order.orderDetails.CostCenter"/></p>
                        <p class="m-item-group__value">${fn:escapeXml(orderData.costCenter.name)}</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label"><spring:theme code="text.account.order.orderDetails.ParentBusinessUnit"/></p>
                        <p class="m-item-group__value">${fn:escapeXml(orderData.costCenter.unit.name)}</p>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <c:if test="${orderData.quoteCode ne null}">
                    <div class="m-item-group">
                        <spring:url htmlEscape="false" value="/my-account/my-quotes/${orderData.quoteCode}" var="quoteDetailUrl"/>
                        <p class="m-item-group__label"><spring:theme code="text.account.quote.code"/></p>
                        <p class="m-item-group__value"><a href="${quoteDetailUrl}" >${fn:escapeXml(orderData.quoteCode)}</a></p>
                    </div>
                </c:if>
            </div>
        </div>
        <%--NOTE: Remove if not required -- End  --%>

        <div class="row">
            <div class="col-md-6">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-comment-label"><spring:theme code="checkout.orderConfirmation.shippingComment" text="Comments" /></p>
                    <p class="m-item-group__value m-checkout-form__value--has-no-formatting" data-qa-id="order-details-shipping-comment-value">${fn:escapeXml(order.customerShippingNotes)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="m-item-group col-md-6">
                        <p class="m-item-group__label" data-qa-id="order-details-submitted-by-label"><spring:theme code="checkout.orderConfirmation.orderPlacedBy" text="Submitted by"/></p>
                        <p class="m-item-group__value" data-qa-id="order-details-submitted-by-value">${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</p>
                    </div>
                    <c:if test="${not empty orderData.placedBy}">
                        <div class="m-item-group col-md-6">
                            <p class="m-item-group__label"><spring:theme code="text.order.placedByText"/></p>
                            <p class="m-item-group__value"><spring:theme code="text.order.customerService"/></p>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hidePagination" required="false" type="String" %>
<%@ attribute name="hideSorting" required="false" type="String" %>
<%@ attribute name="searchUrl" required="true" type="String" %>
<%@ attribute name="messageKey" required="true" type="String" %>
<%@ attribute name="maxItems" required="false" type="String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/my-deliveries/" var="orderDeliveryDetailsUrl" htmlEscape="false"/>
<c:url value="/" var="homeUrl" />

<c:if test="${empty searchPageData.results}">
    <cms:pageSlot position="AccountOrderDeliveryNotFound" var="feature">
        <cms:component component="${feature}" element="p"/>
    </cms:pageSlot>
</c:if>

<c:if test="${not empty searchPageData.results}">
    <div class="hidden">
        <nav:pagination top="true" msgKey="${messageKey}" hideRefineButton="${true}" hideFavoriteFlag = "${true}"
                                   searchKey="${searchKey}"
                                   showTopTotals="${false}"
                                   supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                   searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                   numberPagesShown="${numberPagesShown}"/>
    </div>

    <c:forEach items="${searchPageData.sorts}" var="sort">
        <c:if test="${sort.selected}">
            <c:set var="currentSort" value="${sort.code}" />
        </c:if>
    </c:forEach>
    <c:set var="orderToSortBy" value="${'Asc'}"/>
    <c:if test="${not empty currentSort}">
        <c:set var="orderToSortBy" value="${fn:contains(currentSort, 'Asc') ? 'Desc' : 'Asc'}"/>
        <c:set var="sortClassPrefix" value="${fn:contains(currentSort, 'Asc') ? '-asc' : '-desc'}" />
    </c:if>
    
    <div class="table-responsive">
    <table class="m-order-history table">
        <thead>
            <tr>
                <c:choose>
                    <c:when test="${hideSorting}">
                        <th id="header1" class="m-order-history__th" data-qa-id="myaccount-delivery-number-title-text">
                            <spring:theme code="myaccount.deliveries.deliveryNumber"/>
                        </th>
                        <th id="header2" class="m-order-history__th" data-qa-id="myaccount-delivery-shipping-date-title-text">
                            <spring:theme code="myaccount.deliveries.shippingDate"/>
                        </th>
                        <th id="header3" class="m-order-history__th" data-qa-id="myaccount-delivery-date-title-text">
                            <spring:theme code="myaccount.deliveries.deliveryDate"/>
                        </th>
                        <th id="header4" class="m-order-history__th" data-qa-id="myaccount-shipping-address-title-text">
                            <spring:theme code="myaccount.deliveries.shippingAddress"/>
                        </th>
                        <th id="header5" class="m-order-history__th m-order-history__icon" data-qa-id="myaccount-delivery-pdf-icon">
                        </th>
                    </c:when>
                    <c:otherwise>
                        <th onclick="$('#sortOptions1').val('byDeliveryNo${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byDeliveryNo') ? sortClassPrefix : ''}" id="header1" data-qa-id="myaccount-delivery-number-title-text">
                            <spring:theme code="myaccount.deliveries.deliveryNumber"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byShippingDate${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byShippingDate') ? sortClassPrefix : ''}" id="header2" data-qa-id="myaccount-delivery-shipping-date-title-text">
                            <spring:theme code="myaccount.deliveries.shippingDate"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byDeliveryDate${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byDeliveryDate') ? sortClassPrefix : ''}" id="header3" data-qa-id="myaccount-delivery-date-title-text">
                            <spring:theme code="myaccount.deliveries.deliveryDate"/>
                        </th>
                        <th class="m-order-history__th" id="header4" data-qa-id="myaccount-shipping-address-title-text">
                            <spring:theme code="myaccount.deliveries.shippingAddress"/>
                        </th>
                        <th class="m-order-history__th m-order-history__icon" id="header5" data-qa-id="myaccount-delivery-pdf-icon">
                        </th>
                    </c:otherwise>
                </c:choose>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${searchPageData.results}" var="consignmentData" varStatus="loop">
                <c:if test="${empty maxItems or (not empty maxItems and loop.count le maxItems)}">
                    <tr class="">
                        <td headers="header1" class="m-order-history__td" data-qa-id="myaccount-delivery-number">
                            <a href="${orderDeliveryDetailsUrl}${ycommerce:encodeUrl(consignmentData.code)}">${fn:escapeXml(consignmentData.code)}</a>
                        </td>
                        <td headers="header2" class="m-order-history__td" data-qa-id="myaccount-delivery-shippingdate"><fmt:formatDate value="${consignmentData.shippingDate}" dateStyle="medium" type="date"/></td>
                        <td headers="header3" class="m-order-history__td" data-qa-id="myaccount-delivery-deliverydate"><fmt:formatDate value="${consignmentData.deliveryDate}" dateStyle="medium" type="date"/></td>
                        <td headers="header4" class="m-order-history__td" data-qa-id="myaccount-delivery-shippingaddress">
                            <common:address address="${consignmentData.shippingAddress}" isCommaRequired="true"></common:address>
                        </td>
                        <td headers="header5" class="m-order-history__icon" data-qa-id="myaccount-delivery-pdf-icon">
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
    </div>
    
    <c:if test="${not hidePagination}">
        <nav:pagination top="false" msgKey="${messageKey}" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}" numberPagesShown="${numberPagesShown}"/>
    </c:if>
</c:if>

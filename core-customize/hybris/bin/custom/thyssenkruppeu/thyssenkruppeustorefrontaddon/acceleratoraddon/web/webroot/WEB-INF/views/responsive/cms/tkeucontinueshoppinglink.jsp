<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<common:continueShoppingButton browseCatalogUrl="${url}" browseCatalogueButtonLabel="${linkName}" isButton="${isContinueShoppingButton}"/>

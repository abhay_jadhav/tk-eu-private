/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.registration;

import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_COMPLETE_REGISTRATION_PAGE;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.core.enums.TkEuBusinessType;
import com.thyssenkrupp.b2b.eu.core.facade.RegistrationFacade;
import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;
import com.thyssenkrupp.b2b.eu.storefrontaddon.builders.TkEuBusinessTypeDTO;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.CompleteRegistrationForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.RegistrationForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.CustomerRegistrationFormValidator;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.RegistrationFormValidator;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.customerticketingfacades.TicketFacade;
import de.hybris.platform.customerticketingfacades.data.TicketCategory;
import de.hybris.platform.customerticketingfacades.data.TicketData;
import de.hybris.platform.servicelayer.user.PasswordEncoderConstants;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import net.sourceforge.pmd.util.StringUtil;

@Controller
@RequestMapping(value = "/register")
public class RegistrationController extends AbstractPageController {

    private static final Logger                            LOG                                = Logger.getLogger(RegistrationController.class);
    private static final String                            REGISTER_CONFIRMATION_CMS_PAGE     = "RegistrationConfirmationContentPage";
    private static final String                            BREADCRUMBS_ATTR                   = "breadcrumbs";
    private static final String                            REGISTER_CMS_PAGE                  = "CompleteRegistrationContentPage";
    private static final String                            REGISTERATION_CMS_PAGE             = "RegistrationContentPage";
    private static final String                            TEXT_REGISTER_COMPLETEREGISTRATION = "text.register.completeRegistration";
    private static final String                            EXPIRED_TOKEN_CMS_PAGE             = "ExpiredTokenContentPage";
    private static final String                            TOKEN                              = "token";
    private static final String                            EMAIL_STATUS                       = "emailStatus";
    private static final String                            EMAIL                              = "email";
    private static final String                            EXISTING_CUSTOMER_TICKET_SUBJECT   = "ticket.registration.existing.customer.subject";
    private static final String                            NEW_CUSTOMER_TICKET_SUBJECT        = "ticket.registration.new.customer.subject";
    private static final String                            REGISTER_THANKYOU_CMS_PAGE         = "CustomerRegistrationThankYouPage";
    private static final String                            PAGE_UID                           = "termsAndConditionsRegistration";

    private static final String                            REDIRECT_TO_THANKYOU_PAGE          = REDIRECT_PREFIX + "/register/thankyou";

    @Resource(name = "baseStoreService")
    protected            BaseStoreService                  baseStoreService;
    private              String                            passwordEncoding                   = PasswordEncoderConstants.DEFAULT_ENCODING;
    @Resource(name = "accountBreadcrumbBuilder")
    private              ResourceBreadcrumbBuilder         accountBreadcrumbBuilder;
    @Resource(name = "registrationFacade")
    private              RegistrationFacade                registrationFacade;
    @Resource(name = "registrationFormValidator")
    private              RegistrationFormValidator         registrationFormValidator;
    @Resource(name = "b2bCustomerFacade")
    private              TkEuB2bCustomerFacade             tkEuB2bCustomerFacade;
    @Resource(name = "autoLoginStrategy")
    private              AutoLoginStrategy                 autoLoginStrategy;
    @Resource(name = "secureTokenService")
    private              SecureTokenService                secureTokenService;
    private              UserService                       userService;
    @Resource(name = "customerFacade")
    private              CustomerFacade                    customerFacade;
    private              TkEnumUtils                       tkEnumUtils;
    @Resource(name = "cmsSiteService")
    private              CMSSiteService                    cmsSiteService;
    @Resource(name = "customerRegistrationFormValidator")
    private              CustomerRegistrationFormValidator customerRegistrationFormValidator;
    @Resource(name = "defaultTicketFacade")
    private              TicketFacade                      ticketFacade;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public String register(@RequestParam("token") final String token, @ModelAttribute final CompleteRegistrationForm completeRegistrationForm, final Model model)
      throws CMSItemNotFoundException, TokenInvalidatedException {
        if (StringUtils.isNotBlank(token)) {
            try {
                final SecureToken secureToken = secureTokenService.decryptData(token);
                if (!validateToken(token, secureToken, model)) {
                    setModelDataForExpiredToken(token, model, false, secureToken.getData());
                    return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_EXPIREDTOKEN_PAGE;
                }
                setFormData(completeRegistrationForm, secureToken, token);
            } catch (final RuntimeException exception) {
                LOG.error("Invalid token ", exception);
                return REDIRECT_PREFIX + ROOT;
            }
        }
        setUpCompleteRegistrationPageMetaData(completeRegistrationForm, model);

        return VIEWS_PAGES_REGISTER_COMPLETE_REGISTRATION_PAGE;
    }

    private void setUpCompleteRegistrationPageMetaData(final CompleteRegistrationForm completeRegistrationForm, final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
        setModelData(model, completeRegistrationForm);
    }

    @RequestMapping(value = "/completeRegistration", method = RequestMethod.POST)
    public String completeRegistration(@RequestParam("token") final String token, @ModelAttribute final CompleteRegistrationForm completeRegistrationForm, final BindingResult bindingResult, final Model model,
      RedirectAttributes redirectModel, HttpServletRequest request, HttpServletResponse response)
      throws CMSItemNotFoundException, TokenInvalidatedException {

        getRegistrationFormValidator().validate(completeRegistrationForm, bindingResult);
        if (bindingResult.hasErrors()) {
            resetFormData(completeRegistrationForm);
            setUpCompleteRegistrationPageMetaData(completeRegistrationForm, model);
            return VIEWS_PAGES_REGISTER_COMPLETE_REGISTRATION_PAGE;
        }

        if (!StringUtils.isBlank(token)) {
            try {
                final SecureToken secureToken = secureTokenService.decryptData(token);
                validateToken(token, secureToken, model);
                boolean registrationCompleted = tkEuB2bCustomerFacade.completeCustomerRegistration(secureToken.getData());
                getUserService().setPassword(secureToken.getData(), completeRegistrationForm.getPassword(), getPasswordEncoding());
                getAutoLoginStrategy().login(secureToken.getData(), completeRegistrationForm.getPassword(), request, response);
                LOG.info("Current User Registration \"" + (registrationCompleted ? "Completed" : "Failed") + "\"");
                model.addAttribute("currentStoreName", baseStoreService.getCurrentBaseStore().getName());
                model.addAttribute("user", getCustomerFacade().getCurrentCustomer());
            } catch (final TokenInvalidatedException e) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalidated");
            } catch (final RuntimeException e) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(e);
                }
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
            }
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_CONFIRMATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_CONFIRMATION_CMS_PAGE));
        setModelData(model, completeRegistrationForm);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_REGISTRATION_CONFIRMATION_PAGE;
    }

    @RequireHardLogIn
    @RequestMapping(value = "/completeRegistration", method = RequestMethod.GET)
    public String showCompleteRegistrationPage(@ModelAttribute final CompleteRegistrationForm completeRegistrationForm, final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_CONFIRMATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_CONFIRMATION_CMS_PAGE));
        setModelData(model, completeRegistrationForm);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_REGISTRATION_CONFIRMATION_PAGE;
    }

    private void resetFormData(final CompleteRegistrationForm completeRegistrationForm) {
        completeRegistrationForm.setPassword(StringUtils.EMPTY);
        completeRegistrationForm.setTermsAndConditions(false);
        completeRegistrationForm.setConfirmPassword(StringUtils.EMPTY);
    }

    @RequestMapping(value = "/termsAndConditionsRegistration")
    public String getTermsAndConditionsForRegistration(final Model model) throws CMSItemNotFoundException {
        final ContentPageModel pageForRequest = getContentPageForLabelOrId(PAGE_UID);
        storeCmsPageInModel(model, pageForRequest);
        setUpMetaDataForContentPage(model, pageForRequest);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_TERMS_AND_CONDITIONS;
    }

    @RequestMapping(value = "/emailResend", method = RequestMethod.POST)
    public String emailResend(@RequestParam("token") final String token, @RequestParam("email") final String email, final Model model) throws CMSItemNotFoundException {
        try {
            final CustomerModel customer = getUserService().getUserForUID(email, CustomerModel.class);
            registrationFacade.resendEmail(customer);
            setModelDataForExpiredToken(token, model, true, email);
        } catch (Exception ex) {
            LOG.error("Error in Emailsending " + ex);
            setModelDataForExpiredToken(token, model, false, email);
        }
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTER_EXPIREDTOKEN_PAGE;
    }

    @RequestMapping(value = "/submitRegistration", method = RequestMethod.POST)
    public String submitRegistration(final RegistrationForm form, final BindingResult bindingResult, final Model model, final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        customerRegistrationFormValidator.validate(form, bindingResult);

        if (bindingResult.hasErrors()) {
            storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
            setUpRegistrationPageMetaData(form, model);
            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTRATION_PAGE;
        }
        final TicketData createdTicketData = ticketFacade.createTicket(setTicketData(form));
        getSessionService().setAttribute("createdTicket", createdTicketData);
        return REDIRECT_TO_THANKYOU_PAGE;
    }

    @RequestMapping(value = "/thankyou", method = RequestMethod.GET)
    public String getCustomerRegistrationThankYouPage(final Model model) throws CMSItemNotFoundException {
        setModelData(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_THANKYOU_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_THANKYOU_CMS_PAGE));
        createBreadcrumb(model, REGISTER_THANKYOU_CMS_PAGE);
        getSessionService().removeAttribute("createdTicket");
        return getViewForPage(model);
    }

    private void setModelData(Model model) {
        final TicketData createdTicketData = getSessionService().getAttribute("createdTicket");
        model.addAttribute("FirstName", createdTicketData.getFirstname());
        model.addAttribute("LastName", createdTicketData.getLastname());
        final CMSSiteModel site = cmsSiteService.getCurrentSite();
        model.addAttribute("ShopName", site != null ? site.getName() : "");
    }

    private TicketData setTicketData(RegistrationForm customerRegistrationForm) {
        final TicketData ticketData = new TicketData();
        ticketData.setFirstname(customerRegistrationForm.getFirstName());
        ticketData.setLastname(customerRegistrationForm.getLastName());
        ticketData.setEmail(customerRegistrationForm.getEmail());
        ticketData.setPhone(customerRegistrationForm.getPhoneNumber());
        ticketData.setTicketCategory(TicketCategory.ENQUIRY);
        if (!customerRegistrationForm.getCustomerType().equals("New") && StringUtil.isNotEmpty(customerRegistrationForm.getCustomerNumber())) {
            ticketData.setCustomerId(customerRegistrationForm.getCustomerNumber());
            ticketData.setSubject(getMessageSource().getMessage(EXISTING_CUSTOMER_TICKET_SUBJECT, null, getI18nService().getCurrentLocale()));
            ticketData.setPriority(CsTicketPriority.MEDIUM);
        } else {
            ticketData.setCustomerId(null);
            ticketData.setBusiness(getTkEnumUtils().getEnumerationService().getEnumerationValue(TkEuBusinessType.class, customerRegistrationForm.getBusiness()));
            ticketData.setCompany(customerRegistrationForm.getCompany());
            ticketData.setPriority(CsTicketPriority.LOW);
            ticketData.setSubject(getMessageSource().getMessage(NEW_CUSTOMER_TICKET_SUBJECT, null, getI18nService().getCurrentLocale()));
        }
        ticketData.setMessage(createMessage(customerRegistrationForm).toString());
        ticketData.setCustomerRegistrationTicket(Boolean.TRUE.booleanValue());
        return ticketData;
    }

    private StringJoiner createMessage(RegistrationForm form) {
        StringJoiner sb = new StringJoiner(System.getProperty("line.separator"));
        sb.add("FirstName: " + form.getFirstName());
        sb.add("LastName: " + form.getLastName());
        sb.add("E-mail: " + form.getEmail());
        sb.add("Company: " + StringUtils.defaultString(form.getCompany()));
        sb.add("Telephone: " + form.getPhoneNumber());
        sb.add("Business: " + StringUtils.defaultString(form.getBusiness()));
        if (form.getCustomerNumber() != null && !form.getCustomerNumber().isEmpty()) {
            sb.add("Customer Number: " + form.getCustomerNumber());
            sb.add("Priority: " + CsTicketPriority.MEDIUM);
            sb.add("Subject: " + getMessageSource().getMessage(EXISTING_CUSTOMER_TICKET_SUBJECT, null, getI18nService().getCurrentLocale()));
        } else {
            sb.add("Priority: " + CsTicketPriority.LOW);
            sb.add("Subject: " + getMessageSource().getMessage(NEW_CUSTOMER_TICKET_SUBJECT, null, getI18nService().getCurrentLocale()));
        }
        sb.add("Category: " + TicketCategory.ENQUIRY);
        sb.add("Status: " + CsTicketState.NEW);
        return sb;
    }

    private void createBreadcrumb(Model model, String cmsPage) throws CMSItemNotFoundException {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("#", getContentPageForLabelOrId(cmsPage).getTitle(), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    private void setFormData(CompleteRegistrationForm completeRegistrationForm, SecureToken secureToken, String token) {
        completeRegistrationForm.setUserId(secureToken.getData());
        completeRegistrationForm.setTimeStampInMillis(secureToken.getTimeStamp());
        completeRegistrationForm.setToken(token);
    }

    private void setModelData(Model model, CompleteRegistrationForm completeRegistrationForm) {
        model.addAttribute("registrationForm", completeRegistrationForm);
        model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REGISTER_COMPLETEREGISTRATION));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
    }

    private boolean validateToken(String token, SecureToken data, Model model) throws TokenInvalidatedException, CMSItemNotFoundException {
        if (!registrationFacade.validateRegistrationToken(data)) {
            LOG.error("Token expired ");
            return false;
        } else {
            final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);
            if (customer != null && !token.equals(customer.getToken())) {
                throw new TokenInvalidatedException("Invalid Token");
            }
        }
        return true;
    }

    @ExceptionHandler(TokenInvalidatedException.class)
    public String handleTokenInvalidatedException(final TokenInvalidatedException invalidTokenException) {
        LOG.error("The token does not match with user token , the token has been updated. ", invalidTokenException);
        return REDIRECT_PREFIX + ROOT;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalArgumentException(final IllegalArgumentException illegalArgumentException) {
        LOG.error("Token expired ", illegalArgumentException);
        return REDIRECT_PREFIX + ROOT;
    }

    private void setModelDataForExpiredToken(String token, Model model, boolean status, String data) throws CMSItemNotFoundException {
        model.addAttribute(EMAIL_STATUS, status);
        model.addAttribute(TOKEN, token);
        model.addAttribute(EMAIL, data);
        model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REGISTER_COMPLETEREGISTRATION));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        storeCmsPageInModel(model, getContentPageForLabelOrId(EXPIRED_TOKEN_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EXPIRED_TOKEN_CMS_PAGE));
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String getRegistrationPage(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
        setUpRegistrationPageMetaData(new RegistrationForm(), model);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_REGISTRATION_PAGE;
    }

    private void setUpRegistrationPageMetaData(final RegistrationForm registrationForm, final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTERATION_CMS_PAGE));
        setModelDataForRegistrationPage(model, registrationForm);
    }

    private void setModelDataForRegistrationPage(Model model, RegistrationForm registrationForm) throws CMSItemNotFoundException {
        model.addAttribute("registrationForm", registrationForm);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        createRegistrationBreadcrumbs(model);
        setBusinessType(model);
    }

    private void setBusinessType(Model model) {
        List<TkEuBusinessTypeDTO> tkEuBusinessTypes = new ArrayList<>();
        getTkEnumUtils().getEnumerationService().getEnumerationValues(TkEuBusinessType.class).forEach(tkEuBusinessType -> {
            TkEuBusinessTypeDTO tkEuBusinessTypeDTO = new TkEuBusinessTypeDTO();
            tkEuBusinessTypeDTO.setCode(tkEuBusinessType.getCode());
            tkEuBusinessTypeDTO.setName(getTkEnumUtils().getNameFromEnumCode(tkEuBusinessType));
            tkEuBusinessTypes.add(tkEuBusinessTypeDTO);
        });
        model.addAttribute("tkEuBusinessTypes", tkEuBusinessTypes);
    }

    private void createRegistrationBreadcrumbs(Model model) throws CMSItemNotFoundException {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("/register/customer", getContentPageForLabelOrId(REGISTERATION_CMS_PAGE).getTitle(), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    protected UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public RegistrationFormValidator getRegistrationFormValidator() {
        return registrationFormValidator;
    }

    public void setRegistrationFormValidator(RegistrationFormValidator registrationFormValidator) {
        this.registrationFormValidator = registrationFormValidator;
    }

    protected SecureTokenService getSecureTokenService() {
        return secureTokenService;
    }

    @Required
    public void setSecureTokenService(final SecureTokenService secureTokenService) {
        this.secureTokenService = secureTokenService;
    }

    protected String getPasswordEncoding() {
        return passwordEncoding;
    }

    protected AutoLoginStrategy getAutoLoginStrategy() {
        return autoLoginStrategy;
    }

    public RegistrationFacade getRegistrationFacade() {
        return registrationFacade;
    }

    public void setRegistrationFacade(RegistrationFacade registrationFacade) {
        this.registrationFacade = registrationFacade;
    }

    public TkEnumUtils getTkEnumUtils() {
        return tkEnumUtils;
    }

    public void setTkEnumUtils(TkEnumUtils tkEnumUtils) {
        this.tkEnumUtils = tkEnumUtils;
    }
}

<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/cart" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="m-cart__footer">
    <hr class="m-order-detail__subtotal__separator m-order-detail__subtotal__separator--last">
    <div class="row">
        <div class="col-xs-12 col-md-8 m-cart__footer-data">
            <div class="m-cart__voucher m-cart__voucher--has-margin">
                <p class="text-bold js-cart-consolidatedMessage m-loading-icon m-text-color--black">
                </p>
                <p class="m-text-color--black hidden-xs hidden-sm" data-qa-id="cart-atp-order-info">
                    <spring:theme code="text.shipping.date.info"/>
                </p>
            </div>
            <div class="small hidden-xs hidden-sm">
                <spring:theme code="checkout.shippingAddress.change" var="addAddress"/>
                <spring:theme code="request.availability.address.lightbox.title" var="modalTitle"/>
                <spring:theme code="request.availability.address.lightbox.subtitle" var="modalSubTitle"/>
                <c:url value="/delivery-address/choose-shipping-address" var="addAddressUrl"/>
                <a class="js-add-address-link" href="${addAddressUrl}" data-qa-id="change-address" data-modal-title="${modalTitle}" data-modal-subtitle="${modalSubTitle}">${addAddress}</a>
            </div>
        </div>

        <div class="col-xs-12 col-md-4">
            <div class="m-cart__totals  m-order-detail__subtotal">
                <div class="js-cart-totals">
                    <checkout:tkOrderTotals cartData="${cartData}" showTax="true"/>
                </div>
                <cart:ajaxCartTotals/>
            </div>
        </div>
    </div>
</div>

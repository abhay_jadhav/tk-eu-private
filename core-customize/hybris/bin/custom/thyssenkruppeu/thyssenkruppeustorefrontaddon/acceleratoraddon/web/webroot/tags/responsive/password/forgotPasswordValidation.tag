<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<div class="alert positive forgotten-password" id="validEmail" tabindex="0" data-qa-id="reset-password-msg">
    <spring:theme code="account.confirmation.forgotten.password.link.sent"/>
</div>
<div class="text-center">
    <button class="btn btn-primary" id="js-reset-password-confirm"><spring:theme code="login.popup.resetpassword.button"/></button>
</div>

<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>


<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>
<c:url value="/" var="continueShoppingUrl"/>
<c:choose>
    <c:when test="${not empty cartData.quoteData}">
        <c:set var="miniCartProceed" value="quote.view"/>
    </c:when>
    <c:otherwise>
        <c:set var="miniCartProceed" value="checkout.checkout"/>
    </c:otherwise>
</c:choose>

{
   "cartData": {
       "total": "${cartData.totalPrice.value}",
       "products": [
           <c:forEach items="${cartData.entries}" var="cartEntry" varStatus="status">
               {
                   "sku": "${fn:escapeXml(cartEntry.product.code)}",
                   "name": "<c:out value='${cartEntry.product.name}' />",
                   "qty": "${cartEntry.quantity}",
                   "price": "${cartEntry.basePrice.value}",
                   "categories": [
                   <c:forEach items="${cartEntry.product.categories}" var="category" varStatus="categoryStatus">
                       "<c:out value='${category.name}' />"<c:if test="${not categoryStatus.last}">,</c:if>
                   </c:forEach>
                   ]

               }<c:if test="${not status.last}">,</c:if>
           </c:forEach>
           ]
       },

       "quickOrderErrorData": [
       <c:forEach items="${quickOrderErrorData}" var="quickOrderEntry" varStatus="status">
           {
               "sku": "${fn:escapeXml(quickOrderEntry.productData.code)}",
               "errorMsg": "<spring:theme code='${quickOrderEntry.errorMsg}' htmlEscape="true"/>"
           }<c:if test="${not status.last}">,</c:if>
       </c:forEach>
       ],
       "variants":{
            <c:forEach items="${product.variantValues}" var="variantValue" varStatus="loop">
                <c:set var="variantCode" value="${variantValue.code}"/>
                <c:if test = "${fn:contains( variantCode , 'Trade_Length')}">
                    "dimension2":"${fn:escapeXml(variantValue.value)}"
                </c:if>
                <c:if test = "${fn:contains( variantCode , 'CUTTOLENGTH_PRODINFO')}">
                    "dimension3":"${fn:escapeXml(variantValue.value)}"
                </c:if>
            </c:forEach>
       },

       "cartAnalyticsData":{"cartCode" : "${cartCode}","productPostPrice":"${entry.basePrice.value}","productName":"<c:out value='${product.name}' />"},

        <c:if test="${not empty minicartClosingTime}">
            "cartClosingTime": ${minicartClosingTime},
        </c:if>

       "cartHtml": "<spring:escapeBody javaScriptEscape="true" htmlEscape="false">
<spring:htmlEscape defaultHtmlEscape="true">
<div class="js-mini-cart" data-qa-id="mini-cart-section" data-total="${cartData.totalPrice.formattedValue}">
    <div class="m-mini-cart__content">
        <c:choose>
            <c:when test="${numberShowing > 0 }">
                <div class="m-mini-cart__content__title">
                    <div class="row m-b-25">
                        <div class="col-sm-9">
                            <span class="pali-h2 secondary-dark-txt m-mini-cart__content__title__content" data-qa-id="mini-cart-headline-text"><span class="m-mini-cart-icon"></span><spring:theme code="popup.cart.showing.title"/></span>
                        </div>
                        <div class="col-sm-3 m-mini-cart__content__title__info" data-qa-id="mini-cart-showingitemnumbertxt">
                            <span class="m-mini-cart-close js-close-mini-cart"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"><p class="pali-copy secondary m-b-15"><spring:theme code="popup.cart.showing.count" arguments="${numberItemsInCart}"/></p></div>
                    </div>
                </div>
                <div class="pali-black-hr1"></div>
                <ul class="m-mini-cart__content__list">
                    <c:forEach items="${entries}" var="entry" varStatus="loop">
                        <c:url value="${entry.product.url}" var="entryProductUrl"/>
                        <li class="m-mini-cart__content__list__item">
                            <div class="m-mini-cart__content__list__item__thumb">
                                <a data-qa-id="mini-cart-product-img" href="${entryProductUrl}">
                                    <product:productPrimaryImage product="${entry.product}" format="cartIcon"/>
                                </a>
                            </div>
                            <div class="m-mini-cart__content__list__item__details">
                                <c:if test="${isProductAdded && loop.index eq 0}">
                                    <div class="m-mini-cart__content__list__item__details__notify">
                                        <spring:theme code="basket.product.added" />:
                                    </div>
                                </c:if>
                                <a class="m-mini-cart__content__list__item__details__name pali-copy primary m-b-10" data-qa-id="mini-cart-product-name" href="${entryProductUrl}">${fn:escapeXml(entry.product.name)}</a>
                                <c:if test="${not empty entry.product.variantValues}">
                                    <div data-qa-id="mini-cart-variant-info" class="m-mini-cart__content__list__item__details__info">
                                        <c:forEach items="${entry.product.variantValues}" var="variantValue" varStatus="loop">
                                            <div class="m-item-group--horizontal d-block clearfix">
                                                <p class="m-item-group__label pali-copy references" data-qa-id="mini-cart-variant-label">
                                                    ${fn:escapeXml(variantValue.name)}:
                                                </p>
                                                <p class="m-item-group__label pali-copy primary" data-qa-id="mini-cart-variant-value">
                                                    ${fn:escapeXml(variantValue.value)}
                                                </p>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </c:if>
                                <div class="m-mini-cart__content__list__item__details__attributes m-t-10">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="pali-copy primary" data-qa-id="mini-cart-product-quantity"> ${entry.quantity}
                                                <c:if test="${not empty entry.unit}">
                                                    <c:set value=" ${entry.unitName}" var="formattedUnitName"/>
                                                    ${formattedUnitName}
                                                </c:if>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="pali-copy primary text-right" data-qa-id="mini-cart-product-price">
                                                <c:choose>
                                                <c:when test="${not empty entry.entryTotalDiscount}">
                                                    <strike> <format:price priceData="${entry.entryTotalPriceWithoutDiscount}"/></strike>
                                                </c:when><c:otherwise>
                                                    <format:price priceData="${entry.totalPrice}"/>
                                                </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <c:if test="${not empty entry.entryTotalDiscount}">
                                <div class="row m-t-10">
                                    <div class="col-sm-7">
                                        <div class="pali-copy references" data-qa-id="mini-cart-product-quantity">
                                            <format:price priceData="${entry.entryTotalDiscount}"/>&nbsp;<spring:theme code="discount.on.item" text="Material discount"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="pali-copy primary semi-strong red-text text-right" data-qa-id="mini-cart-product-price">
                                            <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                                        </div>
                                    </div>
                                </div>
                                </c:if>
                                
                                
                                <c:set var="hasCertificate" value="${false}"/>
                                <c:set var="hasCutToLength" value="${false}"/>
                                <c:forEach items="${entry.configurationInfos}" var="configurationInfo">
                                    <spring:eval expression="configurationInfo.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificate" />
                                    <c:if test="${isCertificate and configurationInfo.configurationValue eq 'true'}">
                                        <c:set var="hasCertificate" value="${true}"/>
                                    </c:if>
                                    <spring:eval expression="configurationInfo.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CUTTOLENGTH_PRODINFO" var="isCutToLength" />
                                    <c:if test="${isCutToLength and configurationInfo.configurationValue eq 'true'}">
                                        <c:set var="hasCutToLength" value="${true}"/>
                                    </c:if>
                                </c:forEach>
                                <c:if test="${hasCertificate}">
                                    <div class="m-mini-cart__content__list__item__details__certificate-label" data-qa-id="mini-cart-certificate-label">
                                        <small class="m-certificate" ><spring:theme code="popup.cart.showing.certificate"/></small>
                                    </div>
                                </c:if>
                                <c:if test="${hasCutToLength}">
                                    <div class="m-mini-cart__content__list__item__details__certificate-label">
                                        <small class="m-certificate" data-qa-id="mini-cart-certificate-label"><spring:theme code="sawing.cuttingCost.plus.symbol"/><spring:theme code="product.variant.sawing.cuttingCost"/></small>
                                    </div>
                                </c:if>
                                <c:forEach items="${entry.product.baseOptions}" var="baseOptions">
                                    <c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
                                        <c:if test="${baseOptionQualifier.qualifier eq 'style' and not empty baseOptionQualifier.image.url}">
                                            <div class="itemColor">
                                                <span class="label"><spring:theme code="product.variants.colour"/></span>
                                                <img src="${baseOptionQualifier.image.url}" alt="${fn:escapeXml(baseOptionQualifier.value)}" title="${fn:escapeXml(baseOptionQualifier.value)}"/>
                                            </div>
                                        </c:if>
                                        <c:if test="${baseOptionQualifier.qualifier eq 'size'}">
                                            <div class="itemSize">
                                                <span class="label"><spring:theme code="product.variants.size"/></span>
                                                ${fn:escapeXml(baseOptionQualifier.value)}
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                                <c:if test="${not empty entry.deliveryPointOfService.name}">
                                    <div class="itemPickup"><span class="itemPickupLabel"><spring:theme code="popup.cart.pickup"/></span>&nbsp;${fn:escapeXml(entry.deliveryPointOfService.name)}</div>
                                </c:if>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
                <div class="pali-black-hr1"></div>
                <div class="m-mini-cart__content__footer">
                    <div class="m-mini-cart__content__totals">
                        <c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
                            <cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"  />
                        </c:if>
                        <div class="m-mini-cart__content__totals__value">
                            <div class="key" data-qa-id="mini-cart-subtotal-text"><spring:theme code="popup.cart.subtotal"/></div>
                            <div class="pali-h3 primary-text text-right" data-qa-id="mini-cart-subtotal"><format:price priceData="${cartData.subTotal}"/></div>
                        </div>
                        <div class="m-mini-cart__content__totals__info">
                            <span class="key pali-copy secondary gray-medium-text" data-qa-id="mini-cart-estimated-price-text"><spring:theme code="popup.cart.estimated.price"/></span>
                            <span class="key pali-copy secondary gray-medium-text" data-qa-id="mini-cart-tax-text"><spring:theme code="popup.cart.tax.surcharges"/></span>
                        </div>
                    </div>
                    <div class="m-mini-cart__content__actions">
                        <div class="row">
                            <div class="col-sm-6 m-mini-cart__content__actions--left">
                                <a href="${continueShoppingUrl}" class="pali-btn second-prio d-block w-100 m-mini-cart__actions__close" data-qa-id="mini-cart-close-link"> <spring:theme code="popup.cart.close" /> </a>
                            </div>
                            <div class="col-sm-6 m-mini-cart__content__actions--right">
                                <a href="${cartUrl}" class="pali-btn d-block w-100 pull-right m-mini-cart__actions__view-cart" data-qa-id="mini-cart-checkout-btn">
                                    <spring:theme code="popup.cart.showing.button" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="m-mini-cart__content">
                    <c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
                        <cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"  />
                    </c:if>
                    <div class="m-mini-cart__content__title">
                        <div class="row m-b-25">
                            <div class="col-sm-9">
                                <span class="pali-h2 secondary-dark-txt m-mini-cart__content__title__content" data-qa-id="mini-cart-headline-text"><span class="m-mini-cart-icon"></span><spring:theme code="popup.cart.showing.title"/></span>
                            </div>
                            <div class="col-sm-3 m-mini-cart__content__title__info" data-qa-id="mini-cart-showingitemnumbertxt">
                                <span class="m-mini-cart-close js-close-mini-cart"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="pali-copy secondary p-b-10">
                                <spring:theme code="popup.cart.showing.count" arguments="${0}"/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="m-mini-cart__content__body m-mini-cart__content__body--empty" data-qa-id="mini-cart-empty-cart-text">
                        <spring:theme code="popup.cart.showing.empty.cart" />
                    </div>
                    <div class="m-mini-cart__content__actions">
                        <div class="row">
                            <div class="col-sm-6 m-mini-cart__content__actions--left">
                                <a href="${continueShoppingUrl}" class="pali-btn second-prio d-block w-100 m-mini-cart__actions__close" data-qa-id="mini-cart-close-link"> <spring:theme code="popup.cart.close" /> </a>
                            </div>
                            <div class="col-sm-6 m-mini-cart__content__actions--right">
                                <button disabled="disabled" class="pali-btn d-block w-100 pull-right m-mini-cart__actions__view-cart" data-qa-id="mini-cart-checkout-btn">
                                    <spring:theme code="popup.cart.showing.button" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</spring:htmlEscape>
</spring:escapeBody>"

}

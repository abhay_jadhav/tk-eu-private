<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template/cms" %>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<c:choose>
    <c:when test="${wro4jEnabled}">
        <link rel="stylesheet" type="text/css" media="all" href="${contextPath}/wro/${themeName}_responsive.css?v=${buildTimestamp}" />
        <link rel="stylesheet" type="text/css" media="all" href="${contextPath}/wro/addons_responsive.css?v=${buildTimestamp}" />
    </c:when>
    <c:otherwise>
        <%-- Theme CSS files --%>
        <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css?v=${buildTimestamp}"/>
        <%--  AddOn Common CSS files --%>
        <c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
            <link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}?v=${buildTimestamp}"/>
        </c:forEach>
        <%-- Pattern library CSS files --%>
        <link rel="stylesheet" type="text/css" media="all" href="${fn:escapeXml(siteRootUrl)}/patternlibrary/css/generic.min.css?v=${buildTimestamp}"/>
    </c:otherwise>
</c:choose>

<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
    <link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}?v=${buildTimestamp}"/>
</c:forEach>
<link rel="stylesheet" href="${commonResourcePath}/blueprint/print.css?v=${buildTimestamp}" type="text/css" media="print" />
<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />

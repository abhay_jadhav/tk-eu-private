<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container l-page__content">
    <div class="row">
        <div class="col-md-7">
            <c:if test="${not empty user.firstName and not empty user.lastName }">
                <h3 class="l-page__title l-page__title--no-description"> <spring:theme code="registration.confirmation.title" arguments="${user.firstName},${user.lastName} "/> </h3>
            </c:if>
            <div class="l-page__sub-title l-page__sub-title--has-grey-text" contenteditable="false"><spring:theme code="registration.confirmation.display" /></div>
            <a id="productCatalogueButton" href=${productCatalogueButtonUrl} type="submit" class="btn btn-default ">
                ${productCatalogueButton}</a>
        </div>
    </div>
</div>

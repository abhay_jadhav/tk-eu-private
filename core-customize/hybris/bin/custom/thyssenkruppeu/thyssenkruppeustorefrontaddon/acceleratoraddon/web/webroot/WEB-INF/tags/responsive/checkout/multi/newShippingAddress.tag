<%@ attribute name="supportedCountries" required="false" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<input type="hidden" name="addressStatus" value="true"/>
<div class="m-checkout-shipping__form__add js-shipping-address-add-form">
    <div class="row m-checkout-shipping__form__add__company">
        <div class="col-md-6 m-checkout-shipping__form__add__company__name js-input-mandatory">
            <spring:theme  code="checkout.shipping.address.company.name" var="companyName"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-companyName" labelKey="${companyName}" path="companyName" inputCSS="form-control" mandatory="true" qaAttribute="checkout-add-address-companyname"/>
        </div>
        <div class="col-md-6 m-checkout-shipping__form__add__company__department-name">
            <spring:theme  code="checkout.shipping.address.department.name" var="departmentName"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-department" labelKey="${departmentName}" path="departmentName" inputCSS="form-control" qaAttribute="checkout-add-address-departmentname"/>
        </div>
    </div>
    <div class="row m-checkout-shipping__form__add__person">
        <div class="col-md-3 m-checkout-shipping__form__add__person__salutation">
            <spring:theme  code="checkout.new.address.title" var="titleLabel"></spring:theme>
            <tkEuformElement:formSelectBox idKey="address-title" labelKey="${titleLabel}" path="titleCode" mandatory="false" skipBlank="false" skipBlankMessageKey="newaddress.title.pleaseSelect" items="${titles}" selectCSSClass="form-control selectpicker" qaAttribute="checkout-add-address-title"/>
        </div>
        <div class="col-md-4 m-checkout-shipping__form__add__person__first-name js-input-mandatory">
            <spring:theme  code="checkout.shipping.address.name" var="nameLabel"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-firstName" labelKey="${nameLabel}" path="firstName" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.firstname" qaAttribute="checkout-add-address-firstname"/>
        </div>
        <div class="col-md-5 m-checkout-shipping__form__add__person__last-name js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-lastName" labelKey="" path="lastName" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.lastname" qaAttribute="checkout-add-address-lastname"/>
        </div>
    </div>
    <div class="row m-checkout-shipping__form__add__address">
        <div class="col-md-9 m-checkout-shipping__form__add__address__line1 js-input-mandatory">
            <spring:theme  code="checkout.shipping.address.address" var="addressLabel"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-line1" labelKey="${addressLabel}" path="line1" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.street" qaAttribute="checkout-add-address-street"/>
        </div>
        <div class="col-md-3 m-checkout-shipping__form__add__address__line2 js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-line2" labelKey="" path="line2" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.streetNumber" qaAttribute="checkout-add-address-houseno"/>
        </div>
    </div>
    <div class="row m-checkout-shipping__form__add__city">
        <div class="col-md-3 m-checkout-shipping__form__add__city__zipcode js-input-mandatory">
            <spring:theme  code="checkout.shipping.address.city" var="cityLabel"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-postalCode" labelKey="${cityLabel}" path="postcode" maxlength="10" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.postcode" qaAttribute="checkout-add-address-postalcode"/>
        </div>
        <div class="col-md-6 m-checkout-shipping__form__add__city__name js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-townCity" labelKey="" path="townCity" inputCSS="form-control" mandatory="true" placeholder="checkout.new.address.city" qaAttribute="checkout-add-address-city"/>
        </div>
        <div class="col-md-3 m-checkout-shipping__form__add__city__country">
            <label class="control-label" data-qa-id="checkout-add-address-country-label">
                <spring:theme  code="checkout.shipping.address.country"></spring:theme>
            </label>
            <div data-qa-id="checkoust-add-address-country">
                <c:forEach var="supportedCountries" items="${supportedCountries}">
                    <input type="hidden" name="countryIso" value="${supportedCountries.isocode}"/>
                    <p class="form-control-static">${supportedCountries.name}</p>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="m-checkout-shipping__form__checkbox-wrapper">
        <tkEuformElement:formThemeCheckbox idKey="makeshippingCheckBox" labelKey="checkout.shipping.new.address.save" path="saveCustomerAddressCheckBox" inputCSS="js-shipping-address-checkbox" qaAttribute="addnewaddress-saveshippingaddress-checkbox" labelCSS="" mandatory="false"/>
        <tkEuformElement:formThemeCheckbox idKey="makePrimaryCheckBox" labelKey="checkout.shipping.new.address.makePrimary" path="makePrimaryCheckBox" inputCSS="js-primary-address-checkbox" qaAttribute="addnewaddress-setprimaryshippingaddress-checkbox" labelCSS="" mandatory="false"  disabled="true"/>
    </div>
</div>

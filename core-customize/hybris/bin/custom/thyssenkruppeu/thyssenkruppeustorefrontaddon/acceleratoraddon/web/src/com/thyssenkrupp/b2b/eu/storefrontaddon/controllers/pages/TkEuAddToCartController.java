package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.constants.ThyssenkruppconfigurableproductsaddonConstants.CHECKBOX_CHECKED_VALUE;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkEuAddToCartForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.ConfigurationSelectionDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.ProductPageHelper;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bConfiguratorSettingsService;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.misc.AddToCartController;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.facades.order.ConfigurableProductCartFacade;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantProductModel;

public class TkEuAddToCartController extends AddToCartController {

    private static final String          QUANTITY_ATTR                        = "quantity";
    private static final String          TYPE_MISMATCH_ERROR_CODE             = "typeMismatch";
    private static final String          ERROR_MSG_TYPE                       = "errorMsg";
    private static final String          QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
    private static final String          SHOWN_PRODUCT_COUNT                  = "thyssenkruppstorefront.storefront.minicart.shownProductCount";
    private static final String          CART_ENTRY_VARIABLE_PATTERN          = "{cartEntry:.*}";
    @Resource(name = "baseSiteService")
    protected            BaseSiteService baseSiteService;
    @Resource(name = "cartFacade")
    private              CartFacade      cartFacade;
    @Resource(name = "productVariantFacade")
    private              ProductFacade   productFacade;
    @Resource
    private              SessionService  sessionService;

    @Resource(name = "productPageHelper")
    private ProductPageHelper productPageHelper;

    @Resource
    private ConfigurableProductCartFacade configurableProductCartFacade;

    @Resource(name = "configuratorSettingsService")
    private TkB2bConfiguratorSettingsService configuratorSettingsService;

    @Resource(name = "productService")
    private ProductService productService;

    @Override
    @RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
    public String addToCart(@RequestParam("productCodePost") final String code, final Model model, @ModelAttribute final AddToCartForm tkAddToCartForm, final BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            return getViewWithBindingErrorMessages(model, bindingErrors);
        }

        final long qty = tkAddToCartForm.getQty();
        OrderEntryData orderEntryData = null;

        if (qty <= 0) {
            model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
            model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
        } else {
            try {
                AddToCartParams addToCartParams = prepareAddToCartParams(code, "", qty, Collections.EMPTY_LIST);
                final CartModificationData cartModification = cartFacade.addToCart(addToCartParams);
                setModelValues(model, cartModification);

                if (cartModification.getQuantityAdded() == 0L) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                } else if (cartModification.getQuantityAdded() < qty) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
                }
            } catch (final CommerceCartModificationException ex) {
                logDebugException(ex);
                model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
                model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
            }
        }
        sessionService.getCurrentSession().setAttribute("newAddressStatus", false);
        sessionService.getCurrentSession().setAttribute("addAddressButton", false);
        model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));
        return REDIRECT_PREFIX + "/cart/add/redirectMinicart";
    }

    @RequestMapping(value = "/cart/add/configurations", method = RequestMethod.POST, produces = "application/json")
    @RequireHardLogIn
    public String addToCart(@RequestParam("productCodePost") final String code, final Model model, @ModelAttribute final TkEuAddToCartForm tkAddToCartForm, final BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            return getViewWithBindingErrorMessages(model, bindingErrors);
        }

        final long qty = tkAddToCartForm.getQty();
        OrderEntryData orderEntryData = null;

        if (qty <= 0) {
            model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
            model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
        } else {
            try {
                CartModificationData cartModification;
                ConfigurationSelectionDto dto;
                VariantProductModel variantProductModel = (VariantProductModel) productService.getProductForCode(code);
                Optional<TkLengthConfiguratorSettingsModel> configuratorSetting = configuratorSettingsService.getFirstTkLengthConfiguratorSetting(variantProductModel);

                dto = ConfigurationSelectionDto.builder().withCertificateId(tkAddToCartForm.getCertOption())
                  .withRange(tkAddToCartForm.getSelectedCutToLengthRange())
                  .withRangeUnit(tkAddToCartForm.getSelectedCutToLengthRangeUnit())
                  .withTolerance(tkAddToCartForm.getSelectedCutToLengthTolerance())
                  .withConfigurableLength(configuratorSetting.isPresent() ? configuratorSetting.get().getLengthValue().toString() : "")
                  .withTradeLength(tkAddToCartForm.getSelectedTradeLength()).build();

                final List<ConfigurationInfoData> configurationInfoData = productPageHelper.generateConfigurationInfoData(dto);
                if (CollectionUtils.isNotEmpty(configurationInfoData)) {
                    AddToCartParams addToCartParams = prepareAddToCartParams(code, tkAddToCartForm.getSalesUom(), qty, configurationInfoData);
                    cartModification = configurableProductCartFacade.addToCart(addToCartParams);
                } else {
                    AddToCartParams addToCartParams = prepareAddToCartParams(code, tkAddToCartForm.getSalesUom(), qty, Collections.EMPTY_LIST);
                    cartModification = cartFacade.addToCart(addToCartParams);
                }
                setModelValues(model, cartModification);

                if (cartModification.getQuantityAdded() == 0L) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                } else if (cartModification.getQuantityAdded() < qty) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
                }
            } catch (final CommerceCartModificationException ex) {
                logDebugException(ex);
                model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
                model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
            }
        }
        sessionService.getCurrentSession().setAttribute("newAddressStatus", false);
        sessionService.getCurrentSession().setAttribute("addAddressButton", false);
        model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));
        return REDIRECT_PREFIX + "/cart/add/redirectMinicart";
    }

    private AddToCartParams prepareAddToCartParams(String productCode, String salesUom, long productQuantity, List<ConfigurationInfoData> configurationInfoData) {
        final AddToCartParams addToCartParams = new AddToCartParams();
        addToCartParams.setProductCode(productCode);
        addToCartParams.setQuantity(productQuantity);
        addToCartParams.setProductConfiguration(configurationInfoData);
        addToCartParams.setSalesUom(salesUom);
        return addToCartParams;
    }

    private List<ConfigurationInfoData> getConfigurationInfoData(String certificateId) {
        ConfigurationInfoData configurations = new ConfigurationInfoData();
        configurations.setConfiguratorType(ConfiguratorType.CERTIFICATE);
        configurations.setUniqueId(certificateId);
        configurations.setConfigurationValue(CHECKBOX_CHECKED_VALUE);
        return Collections.singletonList(configurations);
    }

    private void setModelValues(Model model, CartModificationData cartModification) {
        OrderEntryData orderEntryData;
        model.addAttribute(QUANTITY_ATTR, Long.valueOf(cartModification.getQuantityAdded()));
        orderEntryData = cartModification.getEntry();
        sessionService.setAttribute("recentEntry", orderEntryData);
        model.addAttribute("entry", orderEntryData);
        model.addAttribute("cartCode", cartModification.getCartCode());
        model.addAttribute("isQuote", cartFacade.getSessionCart().getQuoteData() != null ? Boolean.TRUE : Boolean.FALSE);
    }

    @RequestMapping(value = "/cart/add/redirectMinicart", method = RequestMethod.GET)
    public String rolloverMiniCartPopup(final Model model, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("isProductAdded", true);
        redirectAttributes.addFlashAttribute("minicartClosingTime", baseSiteService.getCurrentBaseSite().getMiniCartClosingTime());
        redirectAttributes.addFlashAttribute("entry", sessionService.getCurrentSession().getAttribute("recentEntry"));

        return REDIRECT_PREFIX + "/cart/rollover/TkMiniCart";
    }
}

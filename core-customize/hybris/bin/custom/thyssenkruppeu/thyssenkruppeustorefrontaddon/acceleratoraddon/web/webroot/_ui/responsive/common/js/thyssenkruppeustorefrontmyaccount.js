/*global $, ACC, window, document, navigator  */

ACC.thyssenkruppeustorefrontmyaccount = {
        _autoload: [
            ["bindAddNewShippingAddressPopup",  $(".js-add-shipping-address-link, .js-add-shipping-address-link").length != 0],
            ["bindDocumentDownloadPreset", $(".js-document-download").length > 0],
            ],

            bindAddNewShippingAddressPopup : function () {
                $('.js-add-shipping-address-link, .js-edit-shipping-address-link').on('click', function(e) {
                    e.preventDefault();
                    var title = $(this).attr('data-title');
                    ACC.colorbox.open(title, {
                        href: $(this).attr('href'),
                        width: '974px',
                        close: '<span class="u-icon-tk u-icon-tk--close"></span>',
                        title: '<div class="h4 m-colorbox__title" data-qa-id="add-address-popup-header">{title}</div>',
                        onComplete: function() {
                            $(document).off('click', '.js-add-address-submit');
                            $('.selectpicker').selectpicker({dropupAuto: false});
                            ACC.thyssenkruppeustorefront.bindAddCheckoutAddressValidation('addSippingAddressForm');

                            $('.js-cancel-button').click (function () {
                                ACC.colorbox.close();
                            });

                            ACC.colorbox.resize();

                            $(document).on('click', '.js-add-address-submit', function(e) {
                                e.preventDefault();
                                var $form = $(this).closest('form');
                                $form.find('button[type="submit"], input[type="submit"]').attr('disabled', 'disabled');
                                var actionUrl = $form.attr('action');
                                $.post(actionUrl, $form.serialize(), function (data) {
                                    if (data.indexOf('has-error') === -1) {

                                        var msg = $(data).find('.js-alert-msg').text().trim();
                                        if (msg && msg.length > 0) {
                                            ACC.thyssenkruppeustorefront.setGlobalMessage(msg);
                                        }
                                        var newAddresContent = $(data).find('.js-address-content').html();
                                        $('.js-address-content').html(newAddresContent);
                                        ACC.thyssenkruppeustorefrontmyaccount.bindAddNewShippingAddressPopup();
                                        $('.js-equal-height').syncHeight({ 'updateOnResize': true});
                                        ACC.colorbox.close();
                                    } else {
                                        $('#cboxLoadedContent .js-shipping-address-modal').replaceWith(data);
                                        $('.selectpicker').selectpicker();
                                        $('.js-cancel-button').click (function () {
                                            ACC.colorbox.close();
                                        });
                                        ACC.colorbox.resize();
                                    }
                                });
                            });
                        }
                    });
                });
                $('.js-delete-shipping-address-form').on('submit', function (e) {
                    e.preventDefault();
                    var title = $(this).attr('data-title');
                    var popupBody = $(this).find('.js-popup-content').html();
                    var actionUrl = $(this).attr('action');
                    ACC.colorbox.open(title, {
                        html: popupBody,
                        className: "js-dialogueModel m-popup m-popup--dialogue",
                        close: '<span class="u-icon-tk u-icon-tk--close"></span>',
                        title: '<div class="h4 m-colorbox__title" data-qa-id="delete-address-popup-header">{title}</div>',
                        width: '488px',
                        height: '443px',
                        onComplete: function() {

                            $('.js-cancel-button').click (function () {
                                ACC.colorbox.close();
                            });

                            $('.js-submit-button').click (function () {
                                window.location = actionUrl;
                                ACC.colorbox.close();
                            });
                        }
                    });
                });
            },

            bindDocumentDownloadPreset: function () {

                var obj = $('.js-document-download').data('document-obj');
                var objValue=obj.documenType;
            if (objValue!=="CERTIFICATE") {

                    var csrfToken = $('#csrfId').val();
                    $.ajax('/document/exists', {
                        type: 'POST',
                        data:  '{ "documentRequests": ['+ JSON.stringify(obj).toString() +'] }',
                        dataType: 'json',
                        contentType: 'application/json',
                        headers: {'CSRFToken':csrfToken},
                        success: function(data) {
                            if (data.downloadServiceStatus=='SWITCHOFF'){
                                $('.m-order-ack--doc-download').addClass('hidden');
                            }
                            else {
                                $('.m-order-ack--doc-download').removeClass('hidden');
                                $('.js-document-primary .u-icon-tk').removeClass('m-loading-icon');
                                $('.js-document-primary .u-icon-tk').addClass('u-icon-tk--file-pdf');
                                if (data.documentStatus){
                                    $('.js-document-primary').attr('disabled', !data.documentStatus);
                                    $('.js-document-primary').toggleClass('disabled');
                                    $('.js-document-primary .u-icon-tk').attr('data-original-title','');
                                }

                            }
                        }
                    });
                }

                //for certificate
                var docObject = [];
                $('.js-document-download').each(function () {
                    var objCertificate = $(this).data('document-obj');
                    var checkCertificate=objCertificate.documenType;
                    if (checkCertificate==='CERTIFICATE') {
                        docObject.push(JSON.stringify(objCertificate));
                    }
                });

                if(docObject.length>0){
                    $.ajax('/document/exists', {
                        type: 'POST',
                        data:  '{ "documentRequests": ['+ docObject.toString() +'] }',
                        dataType: 'json',
                        contentType: 'application/json',
                        headers: {'CSRFToken':csrfToken},
                        success: function(data) {
                            if (data.downloadServiceStatus=='SWITCHOFF'){
                                $('.m-order-ack--doc-download').addClass('hidden');
                            }
                            else {
                                if(data.certificates.length>0){
                                    for (var i = 0; i < data.certificates.length; i++) {
                                        $('#docLine'+data.certificates[i].lineItemNo).attr('disabled', !data.certificates[i].status);
                                        $('#docLine'+data.certificates[i].lineItemNo+' .u-icon-tk').removeClass('m-loading-icon');
                                        $('#docLine'+data.certificates[i].lineItemNo+' .u-icon-tk').addClass('u-icon-tk--file-pdf');
                                        if(data.certificates[i].status) {
                                            $('#docLine'+data.certificates[i].lineItemNo).toggleClass('disabled');
                                            $('#docLine'+data.certificates[i].lineItemNo+ ' .u-icon-tk').attr('data-original-title','');
                                        }
                                    }
                                }else{
                                    $('.js-document-download .u-icon-tk').removeClass('m-loading-icon');

                                }
                            }
                        }
                    });
                }

                $('.js-document-download').click(function (e) {
                    e.preventDefault();
                    if(!$(this).hasClass('disabled')) {
                        var obj = $(this).data('document-obj');
                        $.ajax({
                            type: 'POST',
                            data:  '{ "documentRequests": ['+ JSON.stringify(obj) +'] }',
                            dataType: 'json',
                            contentType: 'application/json',
                            url: "/document/download",
                            headers: {'CSRFToken':csrfToken},
                            success: function(data){
                                var documentcontent = data.Get_Document_Content.Document_Content;
                                if(data.Get_Document_Content.Content_Type !== '') {
                                    var binary = window.atob(documentcontent.replace(/\s/g, ''));
                                    var contentlength = binary.length;
                                    var buffer = new window.ArrayBuffer(contentlength);
                                    var view = new window.Uint8Array(buffer);
                                    for (var i = 0; i < contentlength; i++) {
                                        view[i] = binary.charCodeAt(i);
                                    }
                                    var apptype = data.Get_Document_Content.Content_Type;
                                    var blob = new window.Blob([view], {type: apptype});
                                    var name = ($('.js-document-download').data('document-obj')).documentNo;
                                    if (navigator.msSaveOrOpenBlob) {
                                        if (data.Get_Document_Content.Content_Type === 'application/pdf') {
                                            navigator.msSaveOrOpenBlob(blob, "Document_" + name + ".pdf");
                                        } else if (data.Get_Document_Content.Content_Type === 'application/ttf') {
                                            navigator.msSaveOrOpenBlob(blob, "Document_" + name + ".ttf");
                                        } else if (data.Get_Document_Content.Content_Type === 'FAILED') {
                                            $('.js-document-primary').addClass('disabled');
                                        }
                                    } else {
                                        var link=document.createElement('a');
                                        link.href=window.URL.createObjectURL(blob);
                                        document.body.appendChild(link);
                                        link.download = "Document_" + name;
                                        if (data.Get_Document_Content.Content_Type === 'application/pdf') {
                                            link.download = link.download + ".pdf";
                                        } else if (data.Get_Document_Content.Content_Type === 'application/ttf') {
                                            link.download = link.download + ".ttf";
                                        } else if (data.Get_Document_Content.Content_Type === 'FAILED') {
                                            $('.js-document-primary').addClass('disabled');
                                        }
                                        link.click();
                                        link.remove();
                                    }
                                }
                            }
                        });
                    }
                });
            },
};

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="priceData" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="displayFreeForZero" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayNegationForDiscount" required="false" type="java.lang.Boolean" %>
<%@ attribute name="withUnit" required="false" type="java.lang.Boolean" %>
<%@ attribute name="isWeight" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="de.hybris.platform.util.Config" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
--%>
<c:set value="${fn:escapeXml(priceData.formattedValue)}" var="formattedPrice"/>
<c:set value="${priceData.formattedUnit}" var="formattedUnit"/>
<spring:theme code='<%=Config.getString("thyssenkrupp.schulte.price.free","false")%>' var="displayFree"/>

<c:if test="${withUnit}">
    <c:set value="${priceData.formattedValueWithUnit}" var="formattedPrice"/>
</c:if>
<c:choose>
    <c:when test="${priceData.value > 0}">
        <c:if test="${displayNegationForDiscount}">
            -
        </c:if>
        <c:if test="${isWeight}">
            <fmt:formatNumber type = "number"
                 maxFractionDigits = "2" minFractionDigits = "2" value = "${priceData.value}" /> ${formattedUnit}
        </c:if>
        <c:if test="${!isWeight}">
            ${formattedPrice}
        </c:if>
    </c:when>
     <c:otherwise>
        <c:if test="${displayFreeForZero  and displayFree eq true}" >
            <spring:theme code="text.free" text="FREE"/>
        </c:if>
        <c:if test="${not displayFreeForZero}">
            ${formattedPrice}
        </c:if>
        <c:if test="${displayFreeForZero and displayFree eq false}">
            ${formattedPrice}
        </c:if>
    </c:otherwise>
</c:choose>

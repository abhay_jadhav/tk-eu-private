<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/cart" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/action" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
<div class="m-cart__actions m-cart__actions--bottom">
    <div class="row">
        <div class="col-xs-12 col-md-offset-8 col-md-4">
            <div class="">
                <button class="pali-btn btn-block btn--continue-checkout js-continue-checkout-button" data-qa-id="cart-checkout-btn" data-checkout-url="${checkoutUrl}">
                    <spring:theme code="checkout.checkout"/>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="delivery-info visible-xs visible-sm">
    <p class="m-text-color--black" data-qa-id="cart-atp-order-info">
        <spring:theme code="text.shipping.date.info"/>
    </p>
    <div class="small">
        <spring:theme code="checkout.shippingAddress.change" var="addAddress"/>
        <spring:theme code="request.availability.address.lightbox.title" var="modalTitle"/>
        <spring:theme code="request.availability.address.lightbox.subtitle" var="modalSubTitle"/>
        <c:url value="/delivery-address/choose-shipping-address" var="addAddressUrl"/>
        <a class="js-add-address-link" href="${addAddressUrl}" data-qa-id="change-address" data-modal-title="${modalTitle}" data-modal-subtitle="${modalSubTitle}">${addAddress}</a>
    </div>
</div>

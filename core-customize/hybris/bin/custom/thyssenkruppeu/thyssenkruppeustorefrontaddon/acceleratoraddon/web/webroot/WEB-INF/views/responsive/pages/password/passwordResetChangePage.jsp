<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<spring:theme code="password.reset.title" var="pageTitle"/>
<template:page pageTitle="${pageTitle}" class="l-page l-page__update-password">
    <cms:pageSlot position="BodyContent" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</template:page>

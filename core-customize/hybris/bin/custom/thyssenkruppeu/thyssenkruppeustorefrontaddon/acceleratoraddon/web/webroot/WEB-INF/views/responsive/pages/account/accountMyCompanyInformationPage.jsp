<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="my-account" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/account" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<div class="m-page-section m-page-section--no-title">
    <div class="m-page-section__content">
        <div class="row">
            <c:if test="${not empty companyName}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label" data-qa-id="myaccount-company-details-companyname-label">
                            <spring:theme code="myaccount.company.companyname"/>
                        </p>
                        <p class="m-item-group__value" data-qa-id="myaccount-company-details-companyname-value">${companyName}&nbsp;</p>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="row">
            <c:if test="${not empty incoterms}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label" data-qa-id="myaccount-company-details-incoterms-label">
                            <spring:theme code="myaccount.company.incoterms"/>
                        </p>
                        <p class="m-item-group__value" data-qa-id="myaccount-company-details-incoterms-value">${incoterms}&nbsp;</p>
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty termsOfPayment}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label" data-qa-id="myaccount-company-details-paymentterms-label">
                            <spring:theme code="myaccount.company.payment.terms"/>
                        </p>
                        <p class="m-item-group__value" data-qa-id="myaccount-company-details-paymentterms-value">${termsOfPayment}&nbsp;</p>
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty shippingTerms}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label" data-qa-id="myaccount-company-details-shippingterms-label">
                            <spring:theme code="myaccount.company.shipping.terms"/>
                        </p>
                        <p class="m-item-group__value" data-qa-id="myaccount-company-details-shippingterms-value">${shippingTerms}</p>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<c:if test="${not empty companyAddress}">
    <div class="m-page-section m-page-section--no-bottom-border m-page-section--has-address-blocks">
        <h3 class="h4 m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="myaccount-company-details-companyaddress-label">
            <spring:theme code="myaccount.company.companyaddress"/>
        </h3>
        <div class="m-page-section__content">
            <div class="row aligned-row">
                <c:forEach var="companyAddress" items="${companyAddress}">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="m-block" data-qa-id="myaccount-company-details-companyaddress-value">
                            <common:address address="${companyAddress}"/>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</c:if>
<c:if test="${not empty billingAddresses}">
    <div class="m-page-section m-page-section--no-bottom-border m-page-section--has-address-blocks">
        <h3 class="h4 m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="myaccount-company-details-billingaddress-label">
            <spring:theme code="myaccount.company.billing.address"/>
        </h3>
        <div class="m-page-section__content">
            <div class="row aligned-row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="m-block"  data-qa-id="myaccount-company-details-billingaddress-value">
                        <common:address address="${primaryBillingAddress}"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>
<div class="m-page-section m-page-section--no-bottom-border m-page-section--has-address-blocks">
    <h3 class="h4 m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="myaccount-company-details-otheraddress-label">
        <spring:theme code="myaccount.company.other.address"/>
    </h3>
    <div class="m-page-section__content">
        <div class="row aligned-row">
            <c:forEach var="billingAddress" items="${billingAddresses}" varStatus="loop">
                <c:if test="${primaryBillingAddress.id ne billingAddress.id}">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <spring:theme code="myaccount.company.billing.makePrimary" var="makePrimaryLabel" />
                        <my-account:myAccountAddressBlock address="${billingAddress}" makePrimaryLabel="${makePrimaryLabel}" makePrimaryUrl="/my-account/company-details/set-primary-address/${fn:escapeXml(billingAddress.id)}" />
                    </div>
                </c:if>
            </c:forEach>
        </div>
    </div>
</div>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<checkout:checkoutOrderDetailsOverview order="${orderData}"/>


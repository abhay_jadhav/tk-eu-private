<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>"/>
<div class="m-cart__items">
    <ul class="item__list item__list__cart" data-qa-id="cart-section">
        <li class="hidden-xs hidden-sm">
            <ul class="item__list--header">
                <%-- <li class="item__toggle"></li> --%>
                <li class="item__image"><spring:theme code="basket.page.item"/></li>
                <li class="item__quantity"><spring:theme code="basket.page.qty"/></li>
                <li class="item__price"><spring:theme code="basket.page.price"/></li>
                <li class="item__weight text-right"><spring:theme code="text.account.orderHistory.item.weight"/></li>
                <li class="item__total text-right"><spring:theme code="basket.page.total"/></li>
            </ul>
        </li>
        <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
            <c:set var="certificate" value="" />
            <c:set var="dimension2"  value=""/>
            <c:set var="dimension3"  value=""/>
            <c:if test="${not empty entry.statusSummaryMap}">
                <c:set var="errorCount" value="${entry.statusSummaryMap.get(errorStatus)}"/>
                <c:if test="${not empty errorCount && errorCount > 0}">
                    <li>
                        <div class="notification has-error">
                            <spring:theme code="basket.error.invalid.configuration" arguments="${errorCount}"/>
                            <a href="<c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" />">
                                <spring:theme code="basket.error.invalid.configuration.edit"/>
                            </a>
                        </div>
                    </li>
                </c:if>
            </c:if>
            <c:set var="showEditableGridClass" value=""/>
            <c:url value="${entry.product.url}" var="productUrl"/>
            <li class="item__list--item section-print ">
                    <%-- chevron for multi-d products --%>
                    <%--NOTE: Baseshop inherited properties. unhide if required.
                    <div class="hidden-xs hidden-sm item__toggle">
                        <c:if test="${entry.product.multidimensional}">
                            <div class="js-show-editable-grid" data-index="${loop.index}" data-read-only-multid-grid="${not entry.updateable}">
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </div>
                        </c:if>
                    </div> --%>

                    <%-- product heading for mobile --%>
                <div class="item__heading visible-xs visible-sm">
                    <a href="${productUrl}" data-qa-id="cart-product-name"><span class="m-text-color--black">${fn:escapeXml(entry.product.name)}</span></a>
                </div>
                    <%-- product image --%>
                <div class="item__image">
                    <a href="${productUrl}"><tk-eu-product:productPrimaryImage product="${entry.product}" format="thumbnail" /></a>
                </div>
                    <%-- product name, code, promotions --%>
                <div class="item__info">
                    <a href="${productUrl}" class="hidden-xs hidden-sm" data-qa-id="cart-product-name"><span class="m-text-color--black">${fn:escapeXml(entry.product.name)}</span></a>
                        <%--NOTE: Baseshop inherited properties. unhide if required.
                        <div class="item__code">${fn:escapeXml(entry.product.code)}</div>
                        <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                            <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                                <c:set var="displayed" value="false"/>
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
                                        <c:set var="displayed" value="true"/>
                                        <div class="promo">
                                            ${fn:escapeXml(promotion.description)}
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </c:if>
                        --%>
                    <c:if test="${not empty entry.product.variantValues}">
                        <div class="item__attributes cart-attr">
                            <c:forEach items="${entry.product.variantValues}" var="variantValue">
                                <div class="row">
                                    <div class="col-xs-7 col-sm-5">
                                        <p class="m-item-group__label small m-text-color--grey-dark" data-qa-id="variant-attribute-label">
                                            ${fn:escapeXml(variantValue.name)}:
                                        </p>
                                    </div>
                                    <div class="col-xs-5 col-sm-7">
                                        <p class="m-item-group__value small" data-qa-id="variant-attribute-value">
                                            ${fn:escapeXml(variantValue.value)}
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>

                    <%-- quantity --%>
                <div class="item__quantity hidden-xs hidden-sm m-cart__items__qty js-checkStatus-item">
                    <c:choose>
                        <c:when test="${not entry.product.multidimensional}">
                            <c:url value="/cart/update" var="cartUpdateFormAction"/>
                            <form:form id="updateCartForm${loop.index}" action="${cartUpdateFormAction}" method="post" commandName="updateQuantityForm${entry.entryNumber}"
                                       class="js-qty-form${loop.index}"
                                       data-cart='{"cartCode" : "${cartData.code}","productPostPrice":"${entry.basePrice.value}","productName":"${fn:escapeXml(entry.product.name)}"}'>
                                <input type="hidden" name="entryNumber" class="js-entryNumber" value="${entry.entryNumber}"/>
                                <input type="hidden" name="productCode" class="js-productCode" value="${entry.product.code}"/>
                                <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                <div class="input-group">
                                    <form:label cssClass="visible-xs visible-sm" path="quantity" for="quantity${entry.entryNumber}"></form:label>
                                    <form:input cssClass="form-control js-update-entry-quantity-input" disabled="${not entry.updateable}" qaAttribute="cart-product-qty" type="text" size="1" id="quantity_${loop.index}" maxlength="5" path="quantity"/>
                                    <div class="input-group-addon" data-qa-id="cart-uom-selector">${entry.unitName}</div>
                                </div>
                            </form:form>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/cart/updateMultiD" var="cartUpdateMultiDFormAction"/>
                            <form:form id="updateCartForm${loop.index}" action="${cartUpdateMultiDFormAction}" method="post" class="js-qty-form${loop.index}" commandName="updateQuantityForm${loop.index}">
                                <input type="hidden" name="entryNumber" class="js-entryNumber" value="${entry.entryNumber}"/>
                                <input type="hidden" name="productCode" class="js-productCode"  value="${entry.product.code}"/>
                                <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                <label class="visible-xs visible-sm"><spring:theme code="basket.page.qty"/>:</label>
                                <span class="qtyValue"><c:out value="${entry.quantity}"/></span>
                                <%--<input type="text" value="${entry.quantity}" class="form-control qtyValue" name="quantity" readonly>--%>
                                <input type="hidden" name="quantity" value="0"/>
                                <div id="QuantityProduct${loop.index}" class="updateQuantityProduct"></div>
                            </form:form>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${entry.updateable}">
                        <c:choose>
                            <c:when test="${not entry.product.multidimensional}">
                                <a href="#" class="item__quantity__remove js-remove-entry-button" data-qa-id="cart-remove-dropdown-button" id="removeEntry_${loop.index}">
                                    <spring:theme code="basket.page.remove"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" class="item__quantity__remove js-submit-remove-product-multi-d" data-index="${loop.index}" id="removeEntry_${loop.index}">
                                    <spring:theme code="basket.page.remove"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <div class="m-cart__items__status text-medium m-loading-icon js-cart-checkstatus" id="checkStatus${entry.entryNumber}"></div>
                </div>

                    <%-- price --%>
                <div class="item__price" data-qa-id="cart-product-price">
                    <span class="visible-xs visible-sm item__price__sales-prop"><spring:theme code="basket.page.itemPrice"/></span>
                    <span class="item__price__sales-value"><tk-eu-format:price priceData="${entry.pricePerBaseUnit}" withUnit="${true}"/></span>
                </div>

                    <%-- weight --%>
                <div class="item__weight text-right">
                    <span class="visible-xs visible-sm pull-left item__weight-prop"><spring:theme code="text.account.orderHistory.item.weight"/></span>
                    <span class="item__weight__per-unit"><c:if test="${not empty entry.salesUnitData && !entry.salesUnitData.isWeightedUnit}"><tk-eu-format:price priceData="${entry.kgPerSalesUnit}" withUnit="${true}" isWeight= "${true}"/></c:if></span>
                    <p class="item__weight__total small">
                        <spring:theme code="basket.page.weightTotal"/>&nbsp;<tk-eu-format:price priceData="${entry.totalWeightInKg}" withUnit="${true}" isWeight="${true}"/>
                    </p>
                </div>
                    <%-- total --%>
                <div class="item__total text-right js-item-total" data-qa-id="cart-product-item-total">
                    <span class="item__total__prop visible-xs visible-sm pull-left"><spring:theme code="basket.page.total"/></span>
                    <span class="item__total__val"><format:price priceData="${entry.subTotal}" displayFreeForZero="true"/></span>
                </div>

                <div class="item__quantity item__quantity--small visible-xs visible-sm m-cart__items__qty">
                    <c:choose>
                        <c:when test="${not entry.product.multidimensional}">
                            <c:url value="/cart/update" var="cartUpdateFormAction"/>
                            <form:form id="updateCartForml${loop.index}" action="${cartUpdateFormAction}" method="post" commandName="updateQuantityForm${entry.entryNumber}"
                                       class="js-qty-form${loop.index}"
                                       data-cart='{"cartCode" : "${cartData.code}","productPostPrice":"${entry.basePrice.value}","productName":"${fn:escapeXml(entry.product.name)}"}'>
                                <input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
                                <input type="hidden" name="productCode" value="${entry.product.code}"/>
                                <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                <div class="input-group">
                                    <form:input cssClass="form-control js-update-entry-quantity-input" disabled="${not entry.updateable}" qaAttribute="cart-product-qty" type="text" size="1" id="quantity_${loop.index}" maxlength="5" path="quantity"/>
                                    <div class="input-group-addon" data-qa-id="cart-uom-selector">${entry.unitName}</div>
                                </div>
                            </form:form>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/cart/updateMultiD" var="cartUpdateMultiDFormAction"/>
                            <form:form id="updateCartForm${loop.index}" action="${cartUpdateMultiDFormAction}" method="post" class="js-qty-form${loop.index}" commandName="updateQuantityForm${loop.index}">
                                <input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
                                <input type="hidden" name="productCode" value="${entry.product.code}"/>
                                <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                <label class="visible-xs visible-sm"><spring:theme code="basket.page.qty"/>:</label>
                                <span class="qtyValue"><c:out value="${entry.quantity}"/></span>
                                <%--<input type="text" value="${entry.quantity}" class="form-control qtyValue" name="quantity" readonly>--%>
                                <input type="hidden" name="quantity" value="0"/>
                                <div id="QuantityProduct${loop.index}" class="updateQuantityProduct"></div>
                            </form:form>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${entry.updateable}">
                        <c:choose>
                            <c:when test="${not entry.product.multidimensional}">
                                <a href="#" class="item__quantity__remove js-remove-entry-button" data-qa-id="cart-remove-dropdown-button" id="removeEntry_${loop.index}">
                                    <spring:theme code="basket.page.remove"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" class="item__quantity__remove js-submit-remove-product-multi-d" data-index="${loop.index}" id="removeEntry_${loop.index}">
                                    <spring:theme code="basket.page.remove"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <div class="m-cart__items__status text-medium m-loading-icon js-cart-checkstatus" id="checkStatus${entry.entryNumber}"></div>
                </div>

            </li>
            <li class="item__list--options">
                <c:set var="certificate" value="" scope="request"/>
                <c:if test="${entry.product.configurable}">
                    <div class="item__options">
                        <common:configurationsInfo entry="${entry}" certificates="${certificate}"/>
                        <%--NOTE: Accelerator inherited modify feature. Unhide when required.
                        <c:if test="${not empty entry.configurationInfos and not isTradeLength}">
                            <div class="row item__options__row">
                                <div class="col-md-6">
                                    <div class="item__attributes">
                                        <div class="m-item-group--horizontal">
                                            <p class="m-item-group__label"><small>&nbsp;</small></p>
                                            <div class="m-item-group__value">
                                                <c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
                                                <a href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        --%>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4 col-md-offset-8">
                        <div class="m-order-detail__subtotal m-b-5">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pali-gray-hr1 m-b-10"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="pali-copy primary semi-strong" data-qa-id="cart-item-grand-total-label">
                                        <spring:theme code="basket.page.item.total" text="Item total"/>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pali-copy primary semi-strong text-right" data-qa-id="cart-item-grand-total-value">
                                    <c:choose>
                                    <c:when test="${not empty entry.entryTotalDiscount}">
                                         <strike> <format:price priceData="${entry.entryTotalPriceWithoutDiscount}" displayFreeForZero="true"/></strike>
                                    </c:when><c:otherwise>
                                    <div class="pali-copy primary semi-strong text-right" data-qa-id="cart-item-grand-total-value">
                                        <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                                    </div>
                                    </c:otherwise>
                                    </c:choose>
                                    </div>
                                </div>
                            </div>
           
                            <c:if test="${not empty entry.entryTotalDiscount}">
                            <div class="row m-b-5">
                                <div class="col-xs-6">
                                    <div class="pali-copy references" data-qa-id="cart-item-grand-total-label">
                                        <format:price priceData="${entry.entryTotalDiscount}"/>&nbsp;<spring:theme code="discount.on.item" text="Material discount"/>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pali-copy primary semi-strong red-text text-right" data-qa-id="cart-item-grand-total-value">
                                        <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                                    </div>
                                </div>
                            </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <c:forEach items="${entry.product.variantValues}" var="variantValue" varStatus="loop">
                    <c:set var="variantCode" value="${variantValue.code}"/>
                    <c:if test = "${fn:contains( variantCode , 'Trade_Length')}">
                        <c:set var="dimension2"  value="${variantValue.value}"/>
                    </c:if>
                    <c:if test = "${fn:contains( variantCode , 'CUTTOLENGTH_PRODINFO')}">
                        <c:set var="dimension3"  value="${variantValue.value}"/>
                    </c:if>
                </c:forEach>
                <c:forEach var="config" items="${entry.configurationInfos}" varStatus="configLoop">
                    <spring:eval expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificate" />
                    <c:if test="${isCertificate}">
                        <c:if test="${config.configurationValue eq 'true'}">
                            <c:set var="certificate" value="${config.configurationLabel}" />
                        </c:if>
                    </c:if>
                </c:forEach>
                <gtm:cartReorderProductGTM product="${entry.product}" quantity="${entry.quantity}" dimension1="${certificate}" dimension2="${dimension2}" dimension3="${dimension3}"/>
            </li>
            <%--NOTE: Baseshop inhertied feature. Use if required.
            <li class="item__list--comment">
                <div class="item__comment quote__comments">
                    <c:if test="${not empty cartData.quoteData}">
                        <c:choose>
                            <c:when test="${not entry.product.multidimensional}">
                                <c:set var="entryNumber" value="${entry.entryNumber}"/>
                                <c:set var="entryComments" value="${entry.comments}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="entryNumber" value="${entry.entries.get(0).entryNumber}"/>
                                <c:set var="entryComments" value="${entry.entries.get(0).comments}"/>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${not empty entryComments}">
                                <order:orderEntryComments entryComments="${entryComments}" entryNumber="${entryNumber}"/>
                            </c:when>
                            <c:otherwise>
                                <div id="entryCommentListDiv_${fn:escapeXml(entryNumber)}" data-show-all-entry-comments="false"></div>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${entry.updateable}">
                            <div class="row">
                                <div class="col-sm-7 col-sm-offset-5">
                                    <div id="entryCommentDiv_${fn:escapeXml(entryNumber)}" class="${not empty entryComments ?'collapse in':'collapse'}">
                                        <c:set var="commentsPlaceholder"><spring:theme code="quote.enter.comment"/></c:set>
                                        <textarea class="form-control js-quote-entry-comments" id="entryComment_${fn:escapeXml(entryNumber)}" placeholder="${commentsPlaceholder}" data-entry-number="${fn:escapeXml(entryNumber)}" rows="3" maxlength="255"></textarea>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                </div>
            </li> --%>
            <li class="hide">
                <spring:url value="/cart/getProductVariantMatrix" var="targetUrl"/>
                <grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="add-to-cart-order-form-wrap hide" targetUrl="${targetUrl}"/>
            </li>
        </c:forEach>
        <c:if test="${reorderCart}">
            <gtm:cartReorderGTM />
        </c:if>
    </ul>
    <%--NOTE: Uncomment if required.
    <product:productOrderFormJQueryTemplates/>
    <storepickup:pickupStorePopup/> --%>
</div>

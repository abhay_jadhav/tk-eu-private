<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>

<c:url value="/" var="siteRootUrl"/>

<template:javaScriptVariables/>

<c:choose>
    <c:when test="${wro4jEnabled}">
          <script type="text/javascript" src="${contextPath}/wro/all_responsive.js?v=${buildTimestamp}"></script>
          <script type="text/javascript" src="${contextPath}/wro/addons_responsive.js?v=${buildTimestamp}"></script>
    </c:when>
    <c:otherwise>
        <%-- jquery --%>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery-3.5.1.min.js"></script>
        
                <%-- bootstrap --%>
        <script type="text/javascript" src="${commonResourcePath}/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/bootstrap/dist/js/bootstrap-select.js"></script>
        
        
        <%-- plugins --%>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script type="text/javascript" src="${commonResourcePath}/js/enquire.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/Imager.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.blockUI-2.66.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.hoverIntent.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.pstrength.custom-1.2.0.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.syncheight.custom.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.tabs.custom.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery-ui-1.12.1.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.zoom.custom.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/owl.carousel.custom.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.tmpl-1.0.0pre.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.currencies.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.waitforimages.min.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.slideviewer.custom.1.2.js"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/jquery.scrollbar.custom.js"></script>

        <%-- Custom ACC JS --%>

        <script type="text/javascript" src="${commonResourcePath}/js/acc.address.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.autocomplete.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.carousel.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.cart.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.cartitem.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.checkout.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.checkoutaddress.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.checkoutsteps.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.cms.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.colorbox.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.common.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.forgottenpassword.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.global.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.hopdebug.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.imagegallery.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.langcurrencyselector.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.minicart.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.navigation.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.order.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.paginationsort.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.payment.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.paymentDetails.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.pickupinstore.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.product.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.productDetail.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.quickview.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.ratingstars.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.refinements.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.silentorderpost.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.tabs.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.termsandconditions.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.track.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.storefinder.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.futurelink.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.productorderform.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.savedcarts.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.multidgrid.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.quickorder.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.quote.js?v=${buildTimestamp}"></script>
        <script type="text/javascript" src="${commonResourcePath}/js/acc.gtm.js?v=${buildTimestamp}"></script>

        <script type="text/javascript" src="${commonResourcePath}/js/acc.csv-import.js?v=${buildTimestamp}"></script>

        <script type="text/javascript" src="${commonResourcePath}/js/_autoload.js?v=${buildTimestamp}"></script>
       

        <%-- Cms Action JavaScript files --%>
        <c:forEach items="${cmsActionsJsFiles}" var="actionJsFile">
            <script type="text/javascript" src="${commonResourcePath}/js/cms/${actionJsFile}?v=${buildTimestamp}"></script>
        </c:forEach>

        <%-- AddOn JavaScript files --%>
        <c:forEach items="${addOnJavaScriptPaths}" var="addOnJavaScript">
            <script type="text/javascript" src="${addOnJavaScript}?v=${buildTimestamp}"></script>
        </c:forEach>
        
        <%-- Pattern library JS files --%>


    </c:otherwise>
</c:choose>


<cms:previewJS cmsPageRequestContextData="${cmsPageRequestContextData}" />

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="b2b-common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>

<div class="container l-page__content">
    <div class="row">
        <div class="col-sm-7">
            <h3 class="l-page__title">${headerText}</h3>
            <p class="l-page__description">${headerDetailText}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form:form role="form" method="post" class="l-page__update-password__form js-update-password-validation-form" commandName="updatePwdForm" id="update-pwd-form">
                <tkEuformElement:formPasswordBox idKey="password" labelKey="${setPassword}" path="pwd" inputCSS="form-control js-password-rule-check" mandatory="true" placeholder="${setPassword}"/>
                <tkEuformElement:formPasswordBox idKey="confirm-password" labelKey="${confirmPassword}" path="checkPwd" inputCSS="form-control js-confirm-password-rule-check" mandatory="true" placeholder="${confirmPassword}"/>
                <div class="l-page__update-password__form__btn-container">
                    <button type="submit" class="btn btn-default l-page__update-password__form__btn-submit">${changePasswordButton}</button>
                </div>
            </form:form>
        </div>
        <div class="col-sm-4">
            <div class="hidden js-validation-message">
                <b2b-common:passwordSuggestions />
            </div>
        </div>
    </div>
</div>

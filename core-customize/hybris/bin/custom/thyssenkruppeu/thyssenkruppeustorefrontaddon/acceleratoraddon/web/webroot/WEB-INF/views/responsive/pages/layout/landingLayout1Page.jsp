<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}" class="l-page l-page__home">
    <cms:pageSlot position="TkEuSection1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>    
    <cms:pageSlot position="TkEuSection2" var="feature" element="div" class="container">
        <cms:component component="${feature}" element="h3" class="l-page__title--no-description"/>
    </cms:pageSlot>
    <cms:pageSlot position="TkEuSection3" var="feature" element="div" class="container">
        <cms:component component="${feature}" element="div" />
    </cms:pageSlot>
    <div class="m-page-section m-page-section--no-bottom-border container">
        <cms:pageSlot position="TkEuSection4" var="feature">
            <cms:component component="${feature}" element="h4" class="m-page-section__title m-page-section__title--no-bottom-border"/>
        </cms:pageSlot>
        <cms:pageSlot position="TkEuSection5" var="feature" element="div" class="row m-page-section--no-margin slot5-row">
            <cms:component component="${feature}" class="col-md-3" element="div"/>
        </cms:pageSlot>
    </div>
    <cms:pageSlot position="TkEuSection6" var="feature" element="div" class="container">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</template:page>

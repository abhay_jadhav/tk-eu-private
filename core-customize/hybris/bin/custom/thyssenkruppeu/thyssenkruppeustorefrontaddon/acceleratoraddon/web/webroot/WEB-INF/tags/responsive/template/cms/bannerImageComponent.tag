<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="medias" required="true" type="java.util.List"%>
<%@ attribute name="mediaRenderingOption" required="true"
    type="com.thyssenkrupp.b2b.eu.storefrontaddon.enums.TkEuMediaRenderOption"%>
<%@ attribute name="isModuleTeaser" required="true"
    type="java.lang.Boolean"%>
<%@ attribute name="altText" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:forEach items="${medias}" var="media">
    <c:choose>
        <c:when test="${empty imagerData}">
            <c:set var="imagerData">"${media.width}":"${media.url}"</c:set>
        </c:when>
        <c:otherwise>
            <c:set var="imagerData">${imagerData},"${media.width}":"${media.url}"</c:set>
        </c:otherwise>
    </c:choose>
    <c:if test="${empty altText}">
        <c:set var="altText" value="${fn:escapeXml(media.altText)}" />
    </c:if>
</c:forEach>

<c:if test="${not empty imagerData}">
    <c:choose>
        <c:when
            test="${not empty mediaRenderingOption && mediaRenderingOption.code == 'ROUND' && isModuleTeaser}">
          <c:choose>
            <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
            <a href="${urlLink}" target="${targetUrl}">
            <img class="m-teaser__img js-responsive-image rounded"
                data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
                </a>
            </c:when>
            <c:otherwise>
            <img class="m-teaser__img js-responsive-image rounded"
                data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
                </c:otherwise>
        </c:choose>
        </c:when>
        <c:when
            test="${not empty mediaRenderingOption && mediaRenderingOption.code == 'ORIGINAL' && isModuleTeaser}">
            <c:choose>
            <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
            <a href="${urlLink}" target="${targetUrl}">
            <img class="m-teaser__img js-responsive-image"
                data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
                </a>
            </c:when>
            <c:otherwise>
            <img class="m-teaser__img js-responsive-image"
                data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
                </c:otherwise>
            </c:choose>
            </c:when>
        <c:otherwise>
            <a href="${urlLink}" target="${targetUrl}">
            <img class="m-banner__img js-responsive-image"
                data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
            </a>
        </c:otherwise>
    </c:choose>
</c:if>

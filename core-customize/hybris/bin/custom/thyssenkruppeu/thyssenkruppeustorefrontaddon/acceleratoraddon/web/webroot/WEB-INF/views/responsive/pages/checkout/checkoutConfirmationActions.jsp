<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="m-order-confirmation">
    <ul class="m-order-confirmation__actions m-list--inline">
        <action:actions element="li" parentComponent="${component}" />
    </ul>
</div>

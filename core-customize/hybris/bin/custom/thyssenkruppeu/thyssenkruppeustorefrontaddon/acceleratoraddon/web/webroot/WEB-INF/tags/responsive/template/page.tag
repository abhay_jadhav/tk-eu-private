<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ tag dynamic-attributes="dynattrs" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ attribute name="isMiniHeaderFooter" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common/header"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common/footer"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<template:master pageTitle="${pageTitle}">
    <jsp:attribute name="pageCss">
        <jsp:invoke fragment="pageCss" />
    </jsp:attribute>
    <jsp:attribute name="pageScripts">
        <jsp:invoke fragment="pageScripts" />
    </jsp:attribute>
    <jsp:body>
        <div class="branding-mobile hidden-md hidden-lg">
            <div class="js-mobile-logo">
                <%--populated by JS acc.navigation--%>
            </div>
        </div>
        <main data-currency-iso-code="${fn:escapeXml(currentCurrency.isocode)}"
            <c:forEach items="${dynattrs}" var="attr">
                ${attr.key}="${attr.value}"
            </c:forEach>
            >
            <spring:theme code="text.skipToContent" var="skipToContent" />
            <a href="#skip-to-content" class="skiptocontent hide" data-role="none">${fn:escapeXml(skipToContent)}</a>
            <spring:theme code="text.skipToNavigation" var="skipToNavigation" />
            <a href="#skiptonavigation" class="skiptonavigation hide" data-role="none">${fn:escapeXml(skipToNavigation)}</a>
            <header:header hideHeaderLinks="${hideHeaderLinks}" isMiniHeader="${isMiniHeaderFooter}"/>
            <a id="skip-to-content" class="hide"></a>
            <cart:cartRestoration />
            <jsp:doBody />
            <footer:footer isMiniFooter="${isMiniHeaderFooter}"/>
        </main>
    </jsp:body>

</template:master>

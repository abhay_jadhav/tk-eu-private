<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:choose> 
    <c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
        <div class="row">
            <div class="col-md-9 owl-home">
                <div class="m-page-section m-page-section--no-bottom-border">
                    <h3 class="m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="accessory-header-section">${fn:escapeXml(component.title)}</h3>
                    <product:productMultiItemCarousel />
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <component:emptyComponent />
    </c:otherwise>
</c:choose>

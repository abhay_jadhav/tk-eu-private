<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<div class="password-rules js-password-messages display-none" data-js-password-instruction="password">
    <ul class="password-rules__list">
        <li class="password-rules__list__item has_min_length"> <spring:theme code="SetPassword.NewPassword.Criteria.MinCharsText"/> </li>
        <li class="password-rules__list__item password-rules__list__item--text"> <spring:theme code="SetPassword.NewPassword.Criteria.Text"/> </li>
        <li class="password-rules__list__item has_uppercase"> <spring:theme code="SetPassword.NewPassword.Criteria.UpperCaseText"/> </li>
        <li class="password-rules__list__item has_lowercase"> <spring:theme code="SetPassword.NewPassword.Criteria.SmallCaseText"/> </li>
        <li class="password-rules__list__item has_number"> <spring:theme code="SetPassword.NewPassword.Criteria.NumberText"/> </li>
        <li class="password-rules__list__item has_special_char"> <spring:theme code="SetPassword.NewPassword.Criteria.SpecialCharsText"/> </li>
    </ul>
</div>

<div class="password-rules js-confirm-password-messages display-none" data-js-password-instruction="confirm-password">
    <ul class="password-rules__list">
        <li class="password-rules__list__item has_same_password"> <spring:theme code="SetPassword.ConfirmPassword.Criteria"/> </li>
    </ul>
</div>

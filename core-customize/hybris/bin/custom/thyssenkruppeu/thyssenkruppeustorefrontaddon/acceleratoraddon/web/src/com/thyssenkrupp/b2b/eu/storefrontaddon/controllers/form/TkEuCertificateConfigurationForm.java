package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

import java.io.Serializable;

public class TkEuCertificateConfigurationForm implements Serializable {
    private Long   quantity;
    private String certOption;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getCertOption() {
        return certOption;
    }

    public void setCertOption(String certOption) {
        this.certOption = certOption;
    }
}

<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData"%>
<%@ attribute name="isCommaRequired" required="false" type="java.lang.Boolean"%>
<%@ attribute name="isNewLineRequired" required="false" type="java.lang.Boolean"%>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showPhoneNumber" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="commaCharacter" value="" />
<c:if test="${isCommaRequired}">
    <c:set var="commaCharacter" value=", " />
</c:if>
<c:set var="plainTextAddress" value="" />
<c:if test="${not storeAddress}">
    <c:if test="${not empty address.companyName}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.companyName)}${commaCharacter}" />
    </c:if>
    <c:if test="${not empty address.department}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.department)}${commaCharacter}" />
    </c:if>
    <c:if test="${not empty address.title or not empty address.firstName or not empty address.lastName}">
        <c:if test="${not empty address.title}">
            <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.title)} " />
        </c:if>
        <c:if test="${not empty address.firstName}">
            <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.firstName)} " />
        </c:if>
        <c:if test="${not empty address.lastName}">
            <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.lastName)}" />
        </c:if>
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${commaCharacter}" />
    </c:if>
</c:if>
<c:if test="${not empty address.streetName or not empty address.streetNumber}">
    <c:if test="${not empty address.streetName}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.streetName)} " />
    </c:if>
    <c:if test="${not empty address.streetNumber}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.streetNumber)}" />
    </c:if>
    <c:set var = "plainTextAddress" value = "${plainTextAddress}${commaCharacter}" />
</c:if>
<c:if test="${not empty address.postalCode or not empty address.town or not empty address.region.name}">
    <c:if test="${not empty address.postalCode}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.postalCode)} " />
    </c:if>
    <c:if test="${not empty address.town}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.town)}" />
    </c:if>
    <c:if test="${not empty address.region.name}">
        <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.region.name)}" />
    </c:if><c:set var = "plainTextAddress" value = "${plainTextAddress}${commaCharacter}" />
</c:if>
<c:if test="${not empty address.country}">
    <c:set var = "plainTextAddress" value = "${plainTextAddress}${fn:escapeXml(address.country.name)}" />
</c:if>
${plainTextAddress}

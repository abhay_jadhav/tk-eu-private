<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tk-eu-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="m-page-section m-order-detail--items-section m-page-section--no-bottom-margin">
    <h3 class="h5 m-page-section__title" data-qa-id="order-details-contract-condition-header"><spring:theme code="order.orderItems" text="Order items" />&nbsp;(${fn:length(order.unconsignedEntries)})</h3>
    <div class="m-page-section__content">
        <ul class="m-order-detail__item-list item__list">
            <li class="hidden-xs hidden-sm">
                <ul class="m-order-detail__item-list--header item__list--header">
                    <%--NOTE: Baseshop inherited fields. Enable if required.
                    <li class="item__toggle hide"></li>
                    <li class="item__position hide"><spring:theme code="text.account.orderHistory.itemNumber"/></li> --%>
                    <li class="item__image"><spring:theme code="text.account.orderHistory.item.item"/></li>
                    <li class="item__product"></li>
                    <li class="item__quantity" data-qa-id="confirmation-column-header-quantity"><spring:theme code="basket.page.qty"/></li>
                    <li class="item__price" data-qa-id="confirmation-column-header-item-price"><spring:theme code="basket.page.itemPrice"/></li>
                    <li class="item__weight text-right" data-qa-id="confirmation-column-header-weight"><spring:theme code="text.account.orderHistory.item.weight" text="Weight"/></li>
                    <li class="item__total text-right" data-qa-id="confirmation-column-header-price"><spring:theme code="basket.page.total"/></li>
                </ul>
            </li>
            <c:forEach items="${order.unconsignedEntries}" var="entry" varStatus="loop">
                <tk-eu-order:orderEntryDetails orderEntry="${entry}" order="${order}" itemIndex="${loop.index}"/>
            </c:forEach>
        </ul>
    </div>
</div>

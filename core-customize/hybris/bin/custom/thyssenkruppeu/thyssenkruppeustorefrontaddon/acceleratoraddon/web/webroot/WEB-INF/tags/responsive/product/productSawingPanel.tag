<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="${variantSelectionForm.variantSelectionMap}" var="variantSelectionMap"/>
<div id="cutToLengthForm" class="m-vcs__cuttolength m-vcs__cuttolength js-cuttolenght collapse ${isSubmitTriggeredBySawing eq true ? 'in' : ''}">
    <c:url value="${product.url}" var="productCutToLengthUrl"/>
    <form:form action="${fn:escapeXml(productCutToLengthUrl)}" id="cutToLengthSelectionForm" name="cutToLengthSelectionForm" commandName="cutToLengthSelectionForm" method="post" class="m-vcs__cuttolength__form form-inline js-cuttolength-form">
        <%-- value for sawingType & selectedCutToLengthRangeInvalid will be set by js --%>
        <input type="hidden" name="sawingType" id="sawingType" value=""/>
        <input type="hidden" name="selectedCutToLengthRangeInvalid" id="selectedCutToLengthRangeInvalid" value=""/>

        <input type="hidden" id="totalVariantValues" class="js-total-variant-value" value="${totalVariantValues}"/>
        <input type="hidden" name="selectedVariantCategory" id="selectedVariantCategory" value=""/>
        <input type="hidden" name="selectedVariantValueCategory" id="selectedVariantValueCategory" value=""/>
        <c:forEach var="selectedVVCs" items="${variantSelectionForm.variantSelectionMap}">
            <input type="hidden"  name="variantSelectionMap[${selectedVVCs.key}]" id="variantSelectionMap[${selectedVVCs.key}]" value="${selectedVVCs.value}" />
        </c:forEach>
        <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${selectedTradelength}"/>
        <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${cutToLengthData.rangeUnit}"/>
        <div class="form-group m-vcs__cuttolength__form-group">
            <div class="input-group">
                <input type="number" value="${variantSelectionForm.selectedCutToLengthRange}"  data-qa-id="pdp-sawing-text-box" id="selectedCutToLengthRange" name="selectedCutToLengthRange" class="m-vcs__cuttolength__input js-cuttolength-length js-input-number form-control">
                <span class="small m-vcs__cuttolength__unit input-group-btn" data-qa-id="pdp-sawing-unit">${fn:toLowerCase(cutToLengthData.rangeUnit)}</span>
            </div>
        </div>
        <div class="form-group m-vcs__cuttolength__form-group">
            <label class="control-label small m-vcs__cuttolength__label"><spring:theme code="product.variant.sawing.tolerance"/></label>
            <select id="selectedCutToLengthTolerance" data-qa-id="pdp-tolerance-drop-down" name="selectedCutToLengthTolerance" class="form-control selectpicker always-validate col-xs-2 selectpicker--sm js-cuttolength-tolerance" tabindex="-98">
                <option selected="selected" value=""><spring:theme code="product.variant.sawing.selectTolerance"/></option>
                <c:forEach items="${cutToLengthData.tolerances}" var="tolerance">
                    <option data-qa-id="pdp-tolerance-drop-down-value" value="${tolerance.key}" ${tolerance.key eq variantSelectionForm.selectedCutToLengthTolerance ? 'selected="selected"' : ''}>
                        <c:out value="${tolerance.value}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
        <p class="help-block js-cuttolength-validate-msg" data-qa-id="pdp-tolerance-range-description"><i class="u-icon-tk u-icon-tk--info"></i><spring:theme code="product.variant.sawing.range" arguments=",${cutToLengthData.minRange},${cutToLengthData.maxRange},"/>
        </p>
        <button id="cutToLengthFormSubmitButton" type="submit" data-qa-id="" class="btn m-vcs__cuttolength__button btn-sm js-product-variant-submit" disabled="disabled" data-qa-id="pdp-sawing-set-button"><spring:theme code="product.variant.sawing.set"/></button>

        <!-- these hidden fields are used by js only-->
        <input class="js-cuttolength-min-range" type="hidden" name="js-cuttolength-min-range" id="js-cuttolength-min-range" value="${cutToLengthData.minRange}"/>
        <input class="js-cuttolength-max-range" type="hidden" name="js-cuttolength-max-range" id="js-cuttolength-max-range" value="${cutToLengthData.maxRange}"/>
    </form:form>
</div>

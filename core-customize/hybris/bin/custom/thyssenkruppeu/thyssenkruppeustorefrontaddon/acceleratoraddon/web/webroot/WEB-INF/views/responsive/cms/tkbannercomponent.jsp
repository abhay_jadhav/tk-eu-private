<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:forEach items="${medias}" var="media">
    <c:choose>
        <c:when test="${empty imagerData}">
            <c:set var="imagerData">"${media.width}":"${media.url}"</c:set>
        </c:when>
        <c:otherwise>
            <c:set var="imagerData">${imagerData},"${media.width}":"${media.url}"</c:set>
        </c:otherwise>
    </c:choose>
    <c:if test="${empty altText}">
        <c:set var="altText" value="${fn:escapeXml(media.altText)}"/>
    </c:if>
</c:forEach>
<c:choose>
    <c:when test="${not empty urlLink}">
        <c:url value="${urlLink}" var="encodedUrl" />
    </c:when>
    <c:otherwise>
        <c:url value="${fn:escapeXml(links[0].url)}" var="encodedUrl" />
    </c:otherwise>
</c:choose>
<c:set var="hasLink" value="${not (empty encodedUrl or encodedUrl eq '#')}"/>
<c:choose>
    <c:when test="${renderOption == 'TITLE' or renderOption == 'TITLE_SUBTITLE'}">
        <${hasLink ? 'a' : 'div'}  href="${hasLink ? encodedUrl : '' }" class="m-banner m-banner${renderOption == 'TITLE' ? '--title' : '--title-subtitle'}">
            <c:if test="${not empty imagerData}">
                <img class="m-banner__img js-responsive-image"  data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
            </c:if>
            <div class="m-banner__content">
                <c:choose>
                    <c:when test="${renderOption == 'TITLE' && not empty title}">
                        <div class="h5 m-banner__content__title m-banner__content__title--small">${title}</div>
                    </c:when>
                    <c:when test="${renderOption == 'TITLE_SUBTITLE'}">
                        <c:if test="${not empty title}">
                            <div class="h1 m-banner__content__title">${title}</div>
                        </c:if>
                        <c:if test="${not empty subtitle}">
                            <span class="m-banner__content__sub-title">${subtitle}</span>
                        </c:if>
                    </c:when>
                </c:choose>
            </div>
        </${hasLink ? 'a' : 'div'}>
    </c:when>

    <c:when test="${renderOption == 'NAVIGATION'}">
        <ul class="list-unstyled">
            <li>
                <div class="m-mega-menu__banner-component">
                    <c:choose>
                        <c:when test="${(empty encodedUrl || encodedUrl eq '#') && not empty imagerData}">
                            <img class="m-banner__img js-responsive-image"  data-media='{${imagerData}}' alt='${altText}' title='${altText}'/>
                        </c:when>
                        <c:otherwise>
                            <a href="${encodedUrl}">
                                <c:if test="${not empty imagerData}">
                                    <img class="m-banner__img js-responsive-image"  data-media='{${imagerData}}' title='${altText}' alt='${altText}'/>
                                </c:if>
                            </a>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${not empty title}">
                        <h4 class="m-mega-menu__banner-component__title">${title}</h5>
                    </c:if>
                    <c:if test="${not empty content}">
                        <div class="m-mega-menu__banner-component__desc">${content}</div>
                    </c:if>
                    <c:if test="${not empty links[0].url}">
                        <cms:component component="${links[0]}" evaluateRestriction="true" element="div" class="m-mega-menu__banner-component__btn"/>
                    </c:if>
                </div>
            </li>
        </ul>
    </c:when>

    <c:otherwise>
        <div class="m-encourage-blocks">
            <div class="m-encourage-blocks__content">
                <c:if test="${not empty title}">
                    <div class="m-encourage-blocks__content__title">
                        <h5>${title}</h5>
                    </div>
                </c:if>
                <c:choose>
                    <c:when test="${(empty encodedUrl || encodedUrl eq '#') && not empty imagerData}">
                        <div class="m-encourage-blocks__content__img">
                            <img class="m-banner__img js-responsive-image"  data-media='{${imagerData}}' alt='${altText}' title='${altText}'/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="m-encourage-blocks__content__img">
                            <a href="${encodedUrl}">
                                <c:if test="${not empty imagerData}">
                                    <img class="m-banner__img js-responsive-image"  data-media='{${imagerData}}' title='${altText}' alt='${altText}'/>
                                </c:if>
                            </a>
                        </div>
                    </c:otherwise>
                </c:choose>
                <c:if test="${not empty headline or not empty subtitle or not empty content}">
                    <div class="m-encourage-blocks__content__text">
                        <c:if test="${not empty headline}">
                            <h5>${headline}</h5>
                        </c:if>
                        <c:if test="${not empty subtitle}">
                            <h6>${subtitle}</h6>
                        </c:if>
                        ${content}
                    </div>
                </c:if>
                <c:if test="${not empty links[0].url}">
                    <cms:component component="${links[0]}" evaluateRestriction="true" element="div"/>
                </c:if>
            </div>
        </div>
    </c:otherwise>
</c:choose>

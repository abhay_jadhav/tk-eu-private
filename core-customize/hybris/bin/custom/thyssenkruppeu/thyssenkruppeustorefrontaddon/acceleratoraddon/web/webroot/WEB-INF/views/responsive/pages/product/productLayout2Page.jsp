<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<template:page pageTitle="${pageTitle}">
    <div class="container l-page__content">
        <cms:pageSlot position="Section1" var="comp" element="div" class="productDetailsPageSection1">
            <cms:component component="${comp}" element="div" class="productDetailsPageSection1-component"/>
        </cms:pageSlot>
        <tk-eu-product:productDetailsPanel />
        <cms:pageSlot position="CrossSelling" var="comp" element="div" class="productDetailsPageSectionCrossSelling">
            <cms:component component="${comp}" element="div" class="productDetailsPageSectionCrossSelling-component"/>
        </cms:pageSlot>
        <cms:pageSlot position="Section3" var="comp" element="div" class="productDetailsPageSection3">
            <cms:component component="${comp}" element="div" class="productDetailsPageSection3-component"/>
        </cms:pageSlot>
        <cms:pageSlot position="UpSelling" var="comp" element="div" class="productDetailsPageSectionUpSelling">
            <cms:component component="${comp}" element="div" class="productDetailsPageSectionUpSelling-component"/>
        </cms:pageSlot>
        <cms:pageSlot position="Section4" var="comp" element="div" class="productDetailsPageSection4">
            <cms:component component="${comp}" element="div" class="productDetailsPageSection4-component"/>
        </cms:pageSlot>
    </div>
</template:page>

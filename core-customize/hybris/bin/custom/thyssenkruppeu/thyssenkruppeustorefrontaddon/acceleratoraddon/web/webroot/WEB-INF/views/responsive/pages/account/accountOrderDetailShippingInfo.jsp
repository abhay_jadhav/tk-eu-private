<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty orderData}">
    <h3 class="h4"><spring:theme code="text.account.order.orderDetails.billingInformtion" /></h3>
    <div class="">
        <c:if test="${orderData.paymentType.code eq 'CARD'}">
            <div class="row">
                <div class="col-sm-6 order-billing-address">
                    <order:billingAddressItem order="${orderData}"/>
                </div>
                <c:if test="${not empty orderData.paymentInfo}">
                    <div class="col-sm-6 order-payment-data">
                        <order:paymentDetailsItem order="${orderData}"/>
                    </div>
                </c:if>
            </div>
        </c:if>
        <c:if test="${orderData.paymentType.code eq 'ACCOUNT'}">
            <div class="row">
                <div class="col-sm-6 order-billing-address">
                    <b2b-order:paymentDetailsAccountItem order="${orderData}"/>
                </div>
            </div>
        </c:if>
    </div>
</c:if>

/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.RegistrationConfirmationComponentModel;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

@Controller("RegistrationConfirmationComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_REGISTRATIONCONFIRMATION)
public class RegistrationConfirmationComponentController extends AbstractCMSComponentController<RegistrationConfirmationComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, RegistrationConfirmationComponentModel component) {

        model.addAttribute("productCatalogueButton", component.getProductCatalogueButton());
        model.addAttribute("productCatalogueButtonUrl", component.getProductCatalogueButtonUrl());
    }

    @Override
    protected String getView(RegistrationConfirmationComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
                StringUtils.lowerCase(RegistrationConfirmationComponentModel._TYPECODE);
    }
}


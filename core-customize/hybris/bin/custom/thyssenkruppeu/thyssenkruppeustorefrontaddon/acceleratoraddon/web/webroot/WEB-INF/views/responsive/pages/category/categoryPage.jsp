<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<template:page pageTitle="${pageTitle}" class="l-page l-page__category-page">
    <div class="container l-page__content">

        <h3 class="l-page__title--no-description">
             <spring:theme code="search.nav.categoryNav" />
        </h3>

        <cms:pageSlot position="Section1" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>

        <cms:pageSlot position="Section2" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>

        <cms:pageSlot position="Section3" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>

        <cms:pageSlot position="Section4" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>

        <%-- NOTE: Accelerator inherited sidebar navigation.
         <nav:categoryNav pageData="${searchPageData}" favoriteFacetData="${favouriteFacetData}"/>
        <product:productRefineButton styleClass="btn btn-default pull-right js-show-facets"/> --%>
    </div>
</template:page>

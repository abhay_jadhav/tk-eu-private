package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;

@Component("tkPasswordValidator")
public class TkEuPasswordValidator implements Validator {

    protected static final String UPDATE_PWD_INVALID = "updatePwd.pwd.invalid";

    @Resource(name = "passwordPattern")
    private String passwordPattern;

    @Override
    public boolean supports(Class<?> aClass) {
        return UpdatePasswordForm.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {

        final UpdatePasswordForm passwordForm = (UpdatePasswordForm) object;
        final String currentPassword = passwordForm.getCurrentPassword();
        final String newPassword = passwordForm.getNewPassword();
        final String checkPassword = passwordForm.getCheckNewPassword();

        if (StringUtils.isEmpty(currentPassword)) {
            errors.rejectValue("currentPassword", "profile.currentPassword.invalid");
        }

        if (StringUtils.isEmpty(newPassword)) {
            errors.rejectValue("newPassword", UPDATE_PWD_INVALID);
        } else if (StringUtils.isNotBlank(getPasswordPattern()) && !newPassword.matches(getPasswordPattern())) {
            errors.rejectValue("newPassword", UPDATE_PWD_INVALID);
        }

        if (StringUtils.isEmpty(checkPassword)) {
            errors.rejectValue("checkNewPassword", UPDATE_PWD_INVALID);
        } else if (StringUtils.isNotBlank(getPasswordPattern()) && !checkPassword.matches(getPasswordPattern())) {
            errors.rejectValue("checkNewPassword", UPDATE_PWD_INVALID);
        }
    }

    public String getPasswordPattern() {
        return passwordPattern;
    }

    public void setPasswordPattern(String passwordPattern) {
        this.passwordPattern = passwordPattern;
    }
}

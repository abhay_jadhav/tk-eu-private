<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="urlLink" required="true" type="java.lang.String"%>
<%@ attribute name="urlLinkRenderingOption" required="false"
    type="com.thyssenkrupp.b2b.eu.storefrontaddon.enums.TkEuURLLinkRenderOption"%>
<%@ attribute name="urlLinkTarget" required="true"
    type="com.thyssenkrupp.b2b.eu.storefrontaddon.enums.TkEuURLLinkOption"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:choose>
    <c:when
        test="${not empty urlLinkTarget && urlLinkTarget.code.equals('INTERNAL')}">
        <c:set value="_self" var="targetUrl" />
    </c:when>
    <c:when
        test="${not empty urlLinkTarget && urlLinkTarget.code.equals('EXTERNAL')}">
        <c:set value="_blank" var="targetUrl" />
    </c:when>
    <c:otherwise>
        <c:set value="_blank" var="targetUrl" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty urlLink}">
        <c:url value="${urlLink}" var="encodedUrl" />
    </c:when>
    <c:otherwise>
        <c:url value="${fn:escapeXml(links[0].url)}" var="encodedUrl" />
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when
        test="${not empty urlLinkRenderingOption && urlLinkRenderingOption.code == 'BUTTON'}">
        <!-- <button id="readMode" type="submit" class="btn btn-primary btn-block" onclick="${encodedUrl}">
            <spring:theme code="tk.generic.banner.button.text" />
        </button> -->
        <a href="${encodedUrl}" class="btn btn-primary btn-block">
            <spring:theme code="tk.generic.banner.button.text" />
        </a>
    </c:when>
    <c:when
        test="${not empty urlLinkRenderingOption && urlLinkRenderingOption.code == 'TEXT_LINK'}">
        <a href="${urlLink}" target="${targetUrl}"
            class="m-encourage-blocks__bottom-link"
            data-qa-id="pdpCarouselViewProductLink"> <spring:theme
                code="tk.generic.banner.text.link.text" />
        </a>
        <span class="u-icon-tk u-icon-tk--arrow-right"></span>

    </c:when>
    <c:otherwise>
        <!-- No Link -->
    </c:otherwise>

</c:choose>


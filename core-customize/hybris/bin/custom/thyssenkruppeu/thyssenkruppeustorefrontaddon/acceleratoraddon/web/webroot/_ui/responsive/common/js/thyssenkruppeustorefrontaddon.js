/*global $, ACC, window, document, Cookie, setTimeout , localStorage */
ACC.thyssenkruppeustorefront = {
    _autoload: [
        "bindDefaults",
        "bindCartQuantityFieldValidation",
        "bindForms",
        "bindPageToolTips",
        "bindLoginEnable",
        "bindPasswordValidation",
        "bindRegistrationForm",
        "fnEnableOrDisableLoginSubmit",
        "fnRemoveWhitespaces",
        "fnEnableOrDisableContactSubmit",
        "bindProductVariantFormAjax",
        "bindCartCertificateSelectAjax",
        "bindProductCertificateSelectAjax",
        "bindProductDetails",
        "bindResetPasswordBtnEnable",
        ["bindCartCheckStatus", $(".js-cart-checkstatus").length > 0],
        ["fnEnableDisableCuttolengthForm", $(".js-cuttolength-form").length > 0],
        ["bindContactEnable", $("#TkEuContactForm").length > 0],
        ["bindRegisterEnable", $("#TkEuRegisterForm").length > 0],
        ["bindReOrderPopup", $(".js-reorder-confirmation-popup-link").length > 0],
        ["bindAddCheckoutAddressValidation", $(".js-shipping-address-add-form, .js-choose-shipping-address").length > 0],
        ["bindAnoymousLoginPopup", $(".js-login-popup-link").length > 0],
        ["bindProductUomSelect", $(".js-qty-selector-input").length > 0],
        ["bindVolumePriceToggle",  $(".js-price-volume-toggle").length != 0],
        ["bindCookieTermsAndCondition",  Cookie.read('is-cookie-terms-accepted') == null],
        ["bindSelectShippingAddressPopup", $(".js-add-address-link").length > 0]
    ],
    setGlobalMessage: function (msg) {
        $('.js-alert-msg').remove();
        if (msg.length > 0) {
            var html = '<div class="m-global-alerts js-alert-msg">' +
                        '<div class="alert alert-info" data-qa-id="info-message">' +
                        '<div class="container">' +
                        '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">' +
                        '<span class="u-icon-tk u-icon-tk--close-single"></span>' +
                        '</button>' +
                            msg +
                        '</div>' +
                        '</div>' +
                        '</div>';
            $(html).insertAfter('.m-header__primary-nav');
        }
    },
    bindDefaults: function () {
        $(".scrollup").click(function() {
            ACC.thyssenkruppeustorefront.triggerScrollUp();
        });
        if ($('.js-sort-options').length > 0) {
            $('.js-sort-options').selectpicker();
        }
        $('.js-equal-height').syncHeight({ 'updateOnResize': true});

        if (document.location.href.indexOf('register/thankyou') > -1 && window.history && window.history.pushState) {
            window.history.pushState({'href':  document.location.href}, null, '/register/thankyou');
            $(window).on('popstate', function() {
                document.location.href = '/register/customer';
            });
        }
        $('.js-facet-checkbox-link, .js-facet-radio-link').on('change', function () {
            document.location.href = $(this).next().find('.js-facet-link-trigger').attr('href');
        });
        $('.js-input-number').on('keypress', function (e) {
            var keycodes = {
                'backspace': 8,
                'delete': 46,
                'leftArrow': 37,
                'rightArrow': 39,
                'number1': 48,
                'number9': 57
            };
            var charCode = e.which ? e.which :
                            (e.charCode ? e.charCode :
                            (e.keyCode ? e.keyCode : 0));
            if ((charCode < keycodes.number1 || charCode > keycodes.number9) &&
                charCode !== keycodes.delete &&
                charCode !== keycodes.backspace &&
                charCode !== keycodes.leftArrow &&
                charCode !== keycodes.rightArrow)
                e.preventDefault();
        });
        $(document).ready(function () {
            $('.js-embed-video').each(function (i, videoTag) {
                    var src = $(videoTag).attr('data-src');
                    $(videoTag).attr('src', src);
            });
        });
        $('.js-sidebar ul.collapse').on('show.bs.collapse',function(e){
            e.stopPropagation();
            var $parentContainer = $(this).parent().parent();
            var $currentContainer = $(this).prev();
            var headerHeight = $('.m-header .m-header__content').outerHeight();

            var $prevOpenContainer = $parentContainer.find('.collapse.in');
            if ($prevOpenContainer.length > 0) {
                $prevOpenContainer.css('height', '0px').collapse('hide').promise().done(function () {
                    var topOffset = $currentContainer.offset().top - headerHeight;
                    $('.js-sidebar').animate({
                       scrollTop: topOffset
                    },100);
                });
            } else {
                var topOffset = $currentContainer.offset().top - headerHeight;
                $('.js-sidebar').animate({
                   scrollTop: topOffset
                },100);
            }
        });
        window.enquire.register("screen and (max-width: " + window.screenXsMax + ")", {
            match : function() { $('.js-sidebar-bottom-links').html($('.js-header-top-links').html()); },
            unmatch : function() { $('.js-sidebar-bottom-links').html(''); },
        });
        $('.js-close-mega-menu').click(function () {
            $(this).closest('.dropdown.yamm-fullwidth').removeClass('open');
        });
        $('body').on("click", ".dropdown.yamm-fullwidth .dropdown-menu", function (e) {
            $(this).parent().is(".open") && e.stopPropagation();
        });
        $('body').on('click', '.js-menu-toggle, .zimmer--primary-nav', function () {

            $(".js-sidebar").toggleClass('in').toggleClass('out');

            if (!$(".js-sidebar").hasClass('in')) {
                $('html, body').css({
                    overflow: 'auto',
                    height: 'auto'
                });
                ACC.minicart.setBackdropVisibility(false);
            } else {
                $('html, body').css({
                    overflow: 'hidden',
                    height: '100%'
                });
                ACC.minicart.setBackdropVisibility(true, true);
            }
        });

        $('.js-sidebar .nav span a').click(function (e) {
            e.preventDefault();
        });
    },
    bindCartQuantityFieldValidation: function() {
        $(".js-update-entry-quantity-input").keypress(function(event) {

            if (!$(this).is(":empty")) {
                var key = window.event ? event.keyCode : event.which;

                if (event.keyCode === 8 || event.keyCode === 46) {
                    return true;
                } else if ( key < 48 || key > 57 ) {
                    return false;
                } else {
                    return false;
                }
            }
        });
    },
    bindResetPasswordBtnEnable: function() {
        var $form = $("#profile-update-pwd-form");
        if ($form.length > 0) {
            $form.find("button[type='submit']").prop("disabled", true);
            $form.on("input", "#current-password, #new-password, #confirm-new-password", function () {
                ACC.thyssenkruppeustorefront.fnEnableOrDisableUpdatePasswordForm();
            });
        }
    },
    fnEnableOrDisableUpdatePasswordForm: function() {
        var $form = $("#profile-update-pwd-form");
        $form.validate({
            rules: {
                "currentPassword": {
                    required: true
                },
                "newPassword": {
                    required: true
                },
                "checkNewPassword": {
                    required: true
                }
            },
            showErrors: function(/* errorMap, errorList */) {
            }
        });
        var $submitButton = $form.find("button[type='submit']");
        if( $("#current-password").valid() === 1 && $("#new-password").valid() === 1 && $("#confirm-new-password").valid() === 1) {
            $submitButton.attr("disabled", false);
            $('.js-search-submit').removeClass('btn-default');
        } else {
            $submitButton.attr("disabled", true);
            $('.js-search-submit').addClass('btn-default');
        }
    },
    bindPasswordValidation: function() {
        if ($('.js-update-password-validation-form').length > 0) {

            $('.js-password-rule-check').popover({
                html : true,
                trigger: 'focus',
                content: function() {
                    $(this).closest('form').valid();
                    return $('.js-password-messages').html();
                }
            });

            $('.js-confirm-password-rule-check').popover({
                html : true,
                trigger: 'focus',
                content: function() {
                    $(this).closest('form').valid();
                    return $('.js-confirm-password-messages').html();
                }
            });

            if ($('#completeRegistrationForm').length > 0) {
                var $updateForm = $('#completeRegistrationForm');
                var $submitButton = $updateForm.find("button[type='submit']");
                var rules = {};
                rules['password'] = {
                    required: true,
                    atLeastOneDigitLowerUpperSpecialLetter: true
                };
                rules['confirmPassword'] = {
                    required: true,
                    samePassword: true
                };
                rules['termsAndConditions'] = {
                    required: true,
                };
                $updateForm.validate({
                    rules: rules,
                    showErrors: function () {
                    },
                });
                $(document).off("input", '#currentPassword, #password-cr, #confirm-password-cr');

                $(document).on("input change", '#termsAndConditionsCheckBox, .form-control', function () {
                    if ($updateForm.valid()) {
                        $submitButton.attr("disabled", false);
                        $submitButton.addClass('btn-default');
                    }
                    else {
                        $submitButton.attr("disabled", true);
                        $submitButton.removeClass('btn-default');
                    }
                });
            }
        }
    },
    bindForms: function() {
        $(document).on("click", ".addressSubmit", function(e) {
            e.preventDefault();

            ACC.thyssenkruppeustorefront.selectedAddress = $("#selectShippingAddress").val();
            if (ACC.thyssenkruppeustorefront.selectedAddress != "") {
                $("#selectedAddressId").val(ACC.thyssenkruppeustorefront.selectedAddress);
                $('#tkShippingAddress').submit();
            }
        });

        $(document).on("click", ".tkBillingAddressSubmit", function(e) {
            e.preventDefault();

            ACC.thyssenkruppeustorefront.selectedBillingAddress = $("#selectBillingAddress").val();
            if (ACC.thyssenkruppeustorefront.selectedBillingAddress != "") {
                $("#selectedBillingAddressId").val(ACC.thyssenkruppeustorefront.selectedBillingAddress);
                $('#tkBillingAddress').submit();
            }
        });

        $(document).on("click",".termsAndConditionsForRegistrationLink",function(e) {
            e.preventDefault();
             ACC.colorbox.open($(this).attr("data-popup-title"), {
                width: '946px',
                href: $(this).attr("href"),
                onComplete: function() {
                    ACC.colorbox.resize();
                    ACC.common.refreshScreenReaderBuffer();
                },
                onClosed: function() {
                    ACC.common.refreshScreenReaderBuffer();
                    ACC.colorbox.close();
                }
            });
        }).on("click", ".termsandconditionclosebutton", function() {
            $.colorbox.close();
        });

        $(document).on("click", "#forgotten-password-cancel-button", function() {
            ACC.colorbox.close();
        });

        if ($('.js-checkout-shipping-comments').length > 0) {
            $('.js-checkout-shipping-comments').keyup(function() {
                $('.js-comment-counter-value').html(this.value.length);
            });
        }
        if ($('.js-checkout-order-name').length > 0) {
            $('.js-checkout-order-name').keyup(function() {
                $('.js-checkout-order-name-counter-value').html(this.value.length);
            });
        }
        if ($('.js-checkout-po-number').length > 0) {
            $('.js-checkout-po-number').keyup(function() {
                $('.js-checkout-po-number-counter-value').html(this.value.length);
            });
        }
        if ($('.js-select-checkout-address').length > 0) {
            $('.js-select-checkout-address').selectpicker();
            ACC.thyssenkruppeustorefront.isCheckoutAddressValid();
            $('.js-select-checkout-address').on('change', function () {
                ACC.thyssenkruppeustorefront.isCheckoutAddressValid();
            });
        }
        $('.js-popup-link').click(function(e) {
            e.preventDefault();

            ACC.colorbox.open($(this).attr("data-popup-title"), {
                href: $(this).attr("href"),
                width: '946px',
                onComplete: function() {
                    ACC.colorbox.resize();
                    $(document).on('click', '.js-popup-close', function(e) {
                        e.preventDefault();
                        ACC.colorbox.close();
                    });
                },
                onClosed: function() {
                    ACC.colorbox.close();
                }
            });
        });
        if($('.js-order-cancel-form-popup-link').length > 0) {

            $('.js-order-cancel-form-popup-link').click(function() {

                var $popupDom = $('.js-order-cancel-form-popup');
                var popupBody = $popupDom.html();
                var popupTitle = $popupDom.data('dialogue-title');

                ACC.colorbox.open(popupTitle, {
                    html: popupBody,
                    className: "js-dialogueModel m-popup m-popup--dialogue",
                    width: '488px',
                    onComplete: function() {
                        ACC.colorbox.resize();
                        $(document).on('click', '#confirmYes', function() {
                            $('.js-order-cancel-form').submit();
                        });
                        $(document).on('click', '#confirmNo', function() {
                            ACC.colorbox.close();
                        });
                    },
                    onClosed: function() { ACC.colorbox.close(); }
                });
            });
        }
        if($('.js-search-input').length > 0) {

            ACC.thyssenkruppeustorefront.isSearchTextPresent();

            $(document).on('keyup', '.js-search-input', function () {
                ACC.thyssenkruppeustorefront.isSearchTextPresent();
            });
        }
        $('.js-form-control-clear').click(function() {
            $(this).siblings('input[type="text"]').val('')
            .trigger('propertychange').focus();
            $(this).closest('.input-group').find('.js_search_button').prop('disabled', 'disabled').removeClass('active');
            $(this).hide();
        });
        if($('.js-comment-counter-value').length > 0) {
            $('.js-comment-counter-value').html($('.js-checkout-shipping-comments').val().length);
        }
        if($('form').length > 0) {
            $('form').submit(function() {
                if($(this).attr('id')!== 'loginForm') {
                    $(this).find('button[type="submit"], input[type="submit"]').attr('disabled', 'disabled');
                }
            });
        }
    },
    isSearchTextPresent: function () {
        var searchText = $('.js-search-input').val().trim();

        if(searchText) {
            $('.js-search-submit').attr('disabled', false);
            $('.js-search-submit').addClass('btn-primary');
        } else {
            $('.js-search-submit').attr('disabled', true);
            $('.js-search-submit').removeClass('btn-primary');
        }
    },
    isCheckoutAddressValid: function () {

        if ($('.js-select-checkout-address').find("option:selected").val() === "") {
            $('.js-checkout-next-step').attr('disabled', 'disabled');
            $('.js-checkout-next-step').removeClass('btn-primary');
        } else {
            $('.js-checkout-next-step').removeAttr('disabled');
            $('.js-checkout-next-step').addClass('btn-primary');
        }
    },
    enableDisableMakePrimary: function () {
        if($(".js-shipping-address-checkbox").is(":checked")) {
            $('.js-primary-address-checkbox').prop("disabled", false);
        } else {
            $('.js-primary-address-checkbox').prop("disabled", true);
            $('.js-primary-address-checkbox').prop("checked", false);
        }
    },
    bindAddCheckoutAddressValidation: function (getFormName) {
        $(".js-shipping-address-checkbox").click(function() {
            ACC.thyssenkruppeustorefront.enableDisableMakePrimary();
        });
        ACC.thyssenkruppeustorefront.enableDisableMakePrimary();
        var $form;

        if (getFormName === "seletShippingAddressForm") {
                $form = $('.js-choose-shipping-address');
                $form.validate({
                    rules: {
                        "selectedAddressCode": {
                            required: true
                        },
                    },
                    showErrors: function() {
                    }
                });
                $('.js-add-address-submit').attr('disabled', !$form.valid());

        } else if (getFormName === "addSippingAddressForm") {

            $form = $('.js-shipping-address-add-form').closest('form');
            $form.validate({
                ignore: function (index, el) {
                    var $el = $(el);
                    if ($el.hasClass('always-validate')) {
                        return false;
                    }
                    return $el.is(':hidden');
                },
                rules: {
                    "companyName": {
                        required: true
                    },
                    "firstName": {
                        required: true
                    },
                    "lastName": {
                        required: true
                    },
                    "line1": {
                        required: true
                    },
                    "line2": {
                        required: true
                    },
                    "postcode": {
                        required: true,
                        number: true
                    },
                    "townCity": {
                        required: true
                    },
                },
                showErrors: function() {
                }
            });

            $('.js-add-address-submit').attr('disabled', !$form.valid());
            $(document).on("input | change", '.js-input-mandatory .form-control', function () {
                $('.js-add-address-submit, .js-checkout-next-step').attr('disabled', !$form.valid());
            });
        }
    },
    triggerScrollUp: function() {
        $('html, body').animate({scrollTop: 0}, 500, 'linear');
    },
    bindLoginEnable: function() {

        $(document).on("input", "#j_username, #j_password", function() {
            ACC.thyssenkruppeustorefront.fnEnableOrDisableLoginSubmit();
        });
    },
    fnEnableOrDisableLoginSubmit: function() {
        if ($("#loginForm").length > 0) {
            $("#loginForm").validate({
                rules: {
                    "j_username": { required: true, email: true },
                    "j_password": { required: true }
                },
                 messages: {
                    "j_username": {
                        required: ACC.loginEmailRequired,
                        email: ACC.loginEmailRequired
                    },
                    "j_password": {
                        required: ACC.loginPwdRequired
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }
    },
    bindRegistrationForm: function() {
        $('.js-complete-registration-btn-submit').attr('disabled','disabled');
        $('.js-complete-registration-btn-submit').addClass('btn-primary');

        $('.js-password, .js-confirm-password').bind("cut copy paste",function(e) {
            e.preventDefault();
        });
        $('.js-terms-and-conditions-checkbox').change(function() {
            if($(this).is(":checked")) {
                $('.js-complete-registration-btn-submit').removeAttr('disabled');
                $('.js-complete-registration-btn-submit').removeClass('btn-primary');
            } else {
                $('.js-complete-registration-btn-submit').attr('disabled', 'disabled');
                $('.js-complete-registration-btn-submit').addClass('btn-primary');
            }
        });
    },
    bindContactEnable: function() {
        var $submitButton = $("#TkEuContactForm").find("button[type='submit']");
        $submitButton.attr("disabled", true);

        var messageLimit = 5000;
        var existingMessageLength = $('.js-contact-message').val().length;
        $('.js-contact-message-limit').html((messageLimit - existingMessageLength) + '/' + messageLimit);
        $(document).on('keyup', '.js-contact-message', function() {
            var textLength = $(this).val().length;
            var textRemaining = messageLimit - textLength;
            $('.js-contact-message-limit').html(textRemaining + '/' + messageLimit);
        });
        $(document).on("input | change", "#TkEuContactForm input, #TkEuContactForm textarea, #TkEuContactForm select", function() {
            ACC.thyssenkruppeustorefront.fnEnableOrDisableContactSubmit();
        }).on("change", "#acceptCheck, #customerType1, #customerType2", function(){
            ACC.thyssenkruppeustorefront.fnEnableOrDisableContactSubmit();
        });
    },
    fnRemoveWhitespaces: function() {
        $(document).on("change", "#TkEuRegisterForm input, #TkEuRegisterForm textarea, #TkEuRegisterForm select", function() {
            var val = $(this).val();
            var trimval = val.trim();
            $(this).val(trimval);
        });
    },
    fnEnableOrDisableContactSubmit: function() {
        if ($("#TkEuContactForm").length > 0) {
            var $submitButton = $("#TkEuContactForm").find("button[type='submit']");
            $("#TkEuContactForm").validate({
                ignore: function (index, el) {
                    var $el = $(el);
                    if ($el.hasClass('always-validate')) {
                        return false;
                    }
                    return $el.is(':hidden');
                },
                rules: {
                    "firstname": { required: true },
                    "lastname": { required: true },
                    "email": { required: true, email: true },
                    "topic": { required: true },
                    "message": { required: true },
                    "acceptCheck": { required: true }
                },
                showErrors: function(/* errorMap, errorList */) {
                }
            });
            if($("#acceptCheck").valid() === 1 && $("#contact-firstname").valid() === 1 && $("#contact-firstname").valid() === 1 && $("#contact-lastname").valid() === 1 && $("#contact-email").valid() === 1 && $("#contact-topic").valid() === 1 && $("#contact-message").valid() === 1) {
                $submitButton.attr("disabled", false);
                $submitButton.addClass('btn-primary');
            } else {
                $submitButton.attr("disabled", true);
                $submitButton.removeClass('btn-primary');
            }
        }
    },
    bindRegisterEnable: function () {
        $('.js-register-type').change(function () {
            ACC.thyssenkruppeustorefront.fnCheckRegisterType();
        });
        ACC.thyssenkruppeustorefront.fnCheckRegisterType();
        var phoneNumberValidate = function(value) {
            if (value) {
                var PHONE_NUMBER_REGEX =  /[0-9]*\/*(\+49)*[ ]*(\([0-9]+\))*([ ]*(-|–)*[ ]*[0-9]+)*/g;
                var matches = value.match(PHONE_NUMBER_REGEX);
                return matches ? value === matches[0] : false;
            }
            return true;
        }
        $.validator.addMethod("tk_phone_format", function(value) {
            return phoneNumberValidate(value);
        }, '');
        var customerNumberValidate = function() {
            var registerType = $("input[name='customerType']:checked").val();
            if(registerType === 'Existing') {
                var customerNo = $('#registration-customerNumber').val();
                return customerNo && /^[0-9]+$/.test(customerNo) ? true : false;
            } else if(registerType === 'New'){
                var companyName = $('#registration-company').val();
                var buisName = $('#register-business').val();
                return companyName && buisName ? true : false;
            }
            return true;
        }
        $.validator.addMethod("customer_number_validate", function(value) {
            return customerNumberValidate(value);
        }, '');
        $(document).on("input | change", "#TkEuRegisterForm input, #TkEuRegisterForm select", function() {ACC.thyssenkruppeustorefront.fnEnableOrDisableRegisterSubmit();});
        var inputArr = ["#registration-customerNumber", "#registration-firstName", "#registration-lastName", "#registration-email", "#registration-phoneNumber", "#registration-company", "[data-id='register-business']"];
        inputArr.forEach(function(item){
            $(document).on("blur", item, function() { ACC.thyssenkruppeustorefront.fnRegistrationValidation(item);});
        });

        ACC.thyssenkruppeustorefront.fnEnableOrDisableRegisterSubmit();
        if (window.mediator !== undefined) {
         window.mediator.subscribe('triggerGreptchaFormValidation', function () {
            ACC.thyssenkruppeustorefront.fnEnableOrDisableRegisterSubmit();
         });
        }
    },
    fnRegistrationValidation: function(id){
        if(id == "[data-id='register-business']" || id == "#registration-company"){
            if($("input[name='customerType']:checked").val() === 'New'){
            if(id == "[data-id='register-business']") id = "#register-business";
                if($(id).val() == ""){
                    $(id).closest(".form-group").next(".err-msg").removeClass("hidden");
                } else {
                    $(id).closest(".form-group").next(".err-msg").addClass("hidden");
                }
            }
        } else {
            if($(id).valid() === 0){
                $(id).parent().next(".err-msg").removeClass("hidden");
                $(id).parent().addClass("has-error");
            } else {
                $(id).parent().next(".err-msg").addClass("hidden");
                $(id).parent().removeClass("has-error");
            }
        }
    },
    fnCheckRegisterType: function() {
        var registerType = $("input[name='customerType']:checked").val();
        if(registerType == 'Existing') {
            $('.js-existing-member-field').show();
            $('.js-non-existing-member-field').hide();

        } else {
            $('.js-existing-member-field').hide();
            $('.js-non-existing-member-field').show();
        }
    },
    fnEnableOrDisableRegisterSubmit: function() {
        if ($("#TkEuRegisterForm").length > 0) {
            var $submitButton = $("#TkEuRegisterForm").find("button[type='submit']");
            $("#TkEuRegisterForm").validate({
                ignore: function (index, el) {
                    var $el = $(el);
                    if ($el.hasClass('always-validate')) {
                        return false;
                    }
                    return $el.is(':hidden');
                },
                rules: {
                    "customerType": { customer_number_validate: true },
                    "customerNumber": { required: true, digits: true },
                    "firstName": { required: true },
                    "lastName": { required: true },
                    "email": { required: true, email: true },
                    "phoneNumber": { tk_phone_format: true },
                    "acceptanceCheck": { required: true }
                },
                showErrors: function() {}
            });
            if($("#acceptanceCheck").valid() === 1 && $("#customerType1").valid() === 1  && $("#customerType2").valid() === 1
            && $("#registration-firstName").valid() === 1 && $("#registration-lastName").valid() === 1
            && $("#registration-email").valid() === 1 && $("#registration-phoneNumber").valid() === 1
            ) {
                $submitButton.attr("disabled", false);
                $submitButton.addClass('btn-default');
            } else {
                $submitButton.attr("disabled", true);
                $submitButton.removeClass('btn-default');
            }
        }
    },
    bindPageToolTips: function() {
        $('[data-toggle="tooltip"]').tooltip();
    },
    bindAnoymousLoginPopup: function () {

        $(document).on('click', '.js-login-popup-link', function (e) {
            e.preventDefault();
            var title = $(this).attr('dialogue-title');

            $.get(ACC.config.loginUrl,function (pageDom) {
                var loginHTML = $(pageDom).find('.l-page__content').html();
                ACC.thyssenkruppeustorefront.showLoginPopup(loginHTML, title);
            });
        });
    },
    showLoginPopup: function (popupBody, title) {
        var description = $(popupBody).find('.l-page__description').html();
        var loginForm = $(popupBody).find('#loginForm')[0].outerHTML;

        popupBody = '<div class="m-login-popup js-user-content">'
                       + '<div class="m-login-popup__description" data-qa-id="login-popup-msg">'+ description +'</div>'
                       + '<div class="alert alert-warning js-login-form-alert display-none alert--mini" data-qa-id="login-popup-error"> </div>'
                       + '<div class="m-login-popup__form">'+ loginForm +'</div>'
                       + '<hr class="m-login-popup__form-separator" />'
                       + '<div class="m-login-popup__link" data-qa-id="login-popup-register-lnk"> '
                       + '<a href="' + ACC.config.registerUrl + '" data-qa-id="login-popup-register-lnk">'+ ACC.config.text.registerLabel + '</a>'
                       +'</div>'
                       + '</div>';
        ACC.colorbox.open(title, {
            html: popupBody,
            className: "js-dialogueModel m-popup m-popup--dialogue",
            close: '<span class="u-icon-tk u-icon-tk--close"></span>',
            title: '<div class="h4 m-colorbox__title" data-qa-id="login-popup-header">{title}</div>',
            width: '488px',
            onComplete: function() {
                ACC.colorbox.resize();
                ACC.thyssenkruppeustorefront.bindLoginEnable();
                ACC.thyssenkruppeustorefront.bindForgotPasswordPopup();
                var $form = $('#loginForm');
                var actionUrl = $form.attr('action');
                $('#loginForm').submit( function(e) {
                    e.preventDefault();
                    $form.css('opacity', '0.3');
                    $('.js-login-form-alert').hide();
                    $.post( actionUrl, $form.serialize(), function(data) {
                        $form.css('opacity', '1');
                        var checkErrorMsg = $(data).find('.m-global-alerts .alert.alert-danger').text().trim();
                        if (checkErrorMsg) {
                            $('.js-login-form-alert').html(checkErrorMsg);
                            $('.js-login-form-alert').show();
                            ACC.colorbox.resize();
                        } else {
                            window.location.reload(true);
                        }
                    });
                });
            },
            onClosed: function() { ACC.colorbox.close(); }
        });
    },
    bindForgotPasswordPopup: function () {
        $(document).off('click', '.js-password-forgotten');
        $(document).on("click","#cboxLoadedContent .js-password-forgotten", function(e) {
            e.preventDefault();
            $('#cboxTitle').text($(this).data("cboxTitle"));
            $.get($(this).data("link"), function(data) {
                $('.js-user-content').replaceWith(data);
                ACC.thyssenkruppeustorefront.setForgotPasswordFormSubmit();
            });
        });
    },
    setForgotPasswordFormSubmit: function () {
        $('form#forgottenPwdForm').ajaxForm({
            success: function(data)
            {
                if ($(data).closest('#validEmail').length) {
                    if ($('#validEmail').length === 0)
                    {
                        $(".forgotten-password").replaceWith(data);
                        ACC.colorbox.resize();
                    }
                }
                else {
                    $("#forgottenPwdForm .control-group").replaceWith($(data).find('.control-group'));
                    ACC.colorbox.resize();
                }
            }
        });
    },
    bindProductVariantFormAjax: function() {
        $(document).on( 'click', '.js-product-variant-submit', function(e) {
            e.preventDefault();
            var $form = $(this).closest('form');
            var actionUrl = $form.attr('action');
            var isVariantStatusInactive = $(this).data('variant-status') === 'inactive';
            var isVariantAlertShown = Cookie.read('alert-variant-shown');

            if (isVariantStatusInactive && !isVariantAlertShown) {
                ACC.product.showVariantDialogueModel(actionUrl, $form);
            } else {
                ACC.thyssenkruppeustorefront.fetchProductVariantsAjax(actionUrl, $form);
            }
        });
        $(document).on( 'click', '#sawing_href_link', function(e) {
            e.preventDefault();
            if ($('.js-cut-to-length-form-map-reset').length > 0) {
                var $form = $('form[id=variantSelectionSawingForm]');
                var actionUrl = $form.attr('action');
                var isVariantStatusInactive = $(this).data('variant-status') === 'inactive';
                var isVariantAlertShown = Cookie.read('alert-variant-shown');
                if (isVariantStatusInactive && !isVariantAlertShown) {
                    ACC.product.showVariantDialogueModel(actionUrl, $form);
                } else {
                    ACC.thyssenkruppeustorefront.fetchProductVariantsAjax(actionUrl, $form);
                }
            } else {
                $('#cutToLengthForm').collapse('toggle');
            }
        });
        $(document).on('click', '.js-reset-variant-selection', function(e) {
            e.preventDefault();

            $('.js-product-variants').css('opacity', '0.2');
            var actionUrl = $(this).attr('href');
            $.get(actionUrl, function(data) {
                ACC.thyssenkruppeustorefront.loadNewVariantDataAjax(data);
                ACC.thyssenkruppeustorefront.showHideProductDetails();
                ACC.thyssenkruppeustorefront.setResetLinkVisibility();
                ACC.thyssenkruppeustorefront.toggleSawingButtonState();
                ACC.thyssenkruppeustorefront.fnBindCutToLengthValidation();
                $('.js-product-variants').css('opacity', '1');
            });
            $('.selectpicker--sm').selectpicker({
                style: 'btn-default btn-sm'
            });
        });
        ACC.thyssenkruppeustorefront.toggleSawingButtonState();
        ACC.thyssenkruppeustorefront.showHideProductDetails();
        ACC.thyssenkruppeustorefront.setResetLinkVisibility();
        ACC.thyssenkruppeustorefront.setInitialVariantSelection();
    },
    setInitialVariantSelection: function () {
        if ($('.js-product-variant-submit.active').length >= 1) {
            var $selectedVariant = $('.js-product-variant-submit.active').last();
            this.setForm = $selectedVariant.closest('form');
            this.setActionUrl = this.setForm.attr('action');
        }
    },
    fnUpdateCutToLengthRangeValidationRules: function () {
        var minRange = parseInt($('.js-cuttolength-min-range').val());
        var maxRange = parseInt($('.js-cuttolength-max-range').val());
        var $cutToLengthInput = $('.js-cuttolength-length');
        $cutToLengthInput.rules('add', {
            range: [minRange, maxRange]
        });
    },
    fnBindCutToLengthValidation: function () {
        var $cutToLengthForm = $(".js-cuttolength-form");
        var minRange = parseInt($('.js-cuttolength-min-range').val());
        var maxRange = parseInt($('.js-cuttolength-max-range').val());
        $cutToLengthForm.validate({
            rules: {
                "selectedCutToLengthRange": {
                    required: true,
                    range: [minRange, maxRange]
                },
                "selectedCutToLengthTolerance": {
                    required: true
                }
            },
            showErrors: function(/* errorMap, errorList */) {
            },
        });
    },
    fnEnableDisableCuttolengthForm: function () {
        ACC.thyssenkruppeustorefront.fnBindCutToLengthValidation();
        $(document).on('select | change', '.js-cuttolength-tolerance', function () {
            var selectedTolerance = $(this).val();
            if (selectedTolerance.length > 0) {
                ACC.thyssenkruppeustorefront.fetchCutToLengthOptionsAjax('tolerance');
            } else {
                $('#cutToLengthFormSubmitButton').attr("disabled", true);
                $('#cutToLengthFormSubmitButton').removeClass("btn-primary");
            }
        });
        $(document).on('change', '.js-cuttolength-length', function () {
            var $cutToLengthRangeInput = $('.js-cuttolength-length');
            if ($cutToLengthRangeInput.valid()) {
                ACC.thyssenkruppeustorefront.fetchCutToLengthOptionsAjax('range');
            } else {
                $('#cutToLengthFormSubmitButton').attr("disabled", true);
                $('#cutToLengthFormSubmitButton').removeClass("btn-primary");
            }
            $cutToLengthRangeInput.closest('.form-group').toggleClass('has-error', (!$cutToLengthRangeInput.valid() == 1));
            $('.js-cuttolength-validate-msg').toggleClass('help-block--error', (!$cutToLengthRangeInput.valid() == 1));
        });
    },
    bindCartCertificateSelectAjax: function () {
        $(document).on('change', '.js-cart-certificate-select', function() {
            var certificateCode = $(this).val();
            var entryNumber = $(this).closest('.js-cert-select').find('#cartEntryNumber').val();
            $.ajax({
                url:         $(this).closest('.js-cert-select').attr('action'),
                type:        'POST',
                data: { 'certificateCode' : certificateCode, 'entryNumber' : entryNumber},
                success: function(){
                    window.location.reload();
                },
            });
        });
        $(document).on('click', '.cert-selected__remove', function() {
        var certificateCode = "";
        var entryNumber = $(this).closest('.m-item-group__value').find('#cartEntryNumber').val();

        $.ajax({
            url:         $(this).closest('.m-item-group__value').find('.js-cert-select').attr('action'),
            type:'POST',
            data: { 'certificateCode' : certificateCode, 'entryNumber' : entryNumber},
            success: function(){
                window.location.reload();
            },
        });

        });
    },
    bindProductCertificateSelectAjax: function () {
        $(document).on('change', '.js-product-certificate-select', function() {
            var $form = $('#configureForm');
            var actionUrl = $form.attr('action');
            ACC.thyssenkruppeustorefront.updateCertOption($(this).val());
            ACC.thyssenkruppeustorefront.updateAddToCartQty($form);
            var $target = $('.js-product-cart-entry-price-panel');
            var $addToCartActionPanel = $('.js-addtocart-action');
            $target.css('opacity', '0.2');
            if( $('.js-hidden-isProductPurchasable-selector').val() === 'true' ) {
                $.post( actionUrl, $form.serialize(), function(data) {
                    if($(data).find('.js-addtocart-request-availabilty').html()) {
                        $addToCartActionPanel.replaceWith($(data).html());
                    } else {
                        $target.replaceWith(data);
                        $target.css('opacity', '1');
                    }
                    ACC.product.checkAvailibility('showLoadingIcon');
                    ACC.product.enableAddToCartButton();
                }).fail(function () {
                    $('button.js-add-to-cart').attr('disabled', 'disabled');
                    $('button.js-add-to-cart').removeClass('btn-primary');
                });
            }
        });
    },
    bindProductDetails: function () {
        $('.custom-tab .tab-content .tab-pane').on('shown.bs.collapse', function () {
            var panel = $(this).find('.in');
            $('html, body').animate({
                scrollTop: panel.offset().top - 77
            }, 500);
        });
    },
    bindProductUomSelect: function () {
        var buttonUomValue = $('.js-dropdown-uom-button').data('value');
        ACC.thyssenkruppeustorefront.updateUomOption(buttonUomValue);
        $('.js-dropdown-uom-values > li > a').click(function(ev) {
            ev.preventDefault();
            var $form = $('#uomSelectorForm');
            $('.js-selected-uom').text($(this).text());
            var selectedUom = $(this).data('value');
            var actionUrl = $form.attr('action');
            ACC.thyssenkruppeustorefront.updateAddToCartQty($form);
            ACC.thyssenkruppeustorefront.updateUomOption(selectedUom);
            if ($('#anonymous-user').length > 0) {
                ACC.thyssenkruppeustorefront.updateWeightPanelAjax(actionUrl, $form);
            } else if( $('.js-hidden-isProductPurchasable-selector').val() === 'true' ) {
                ACC.thyssenkruppeustorefront.fetchProductUomAjax(actionUrl, $form);
            }
        });
    },
    calcCartFromUom: function () {
        var $form = $('#uomSelectorForm');
        var actionUrl = $form.attr('action');
        ACC.thyssenkruppeustorefront.updateAddToCartQty($form);
        if ($('#anonymous-user').length > 0) {
            ACC.thyssenkruppeustorefront.updateWeightPanelAjax(actionUrl, $form);
        } else if( $('.js-hidden-isProductPurchasable-selector').val() === 'true' ) {
            ACC.thyssenkruppeustorefront.fetchProductUomAjax(actionUrl, $form);
        }
    },
    updateAddToCartQty : function (form) {
        var addToCartQty = $("#uomSelectorForm").find("#pdpAddtoCartInput");
        $(form).find(".qty.js-qty-selector-input").val(addToCartQty.val());
    },
    updateCertOption : function (value) {
        $('.js-hidden-certOption-selector-input').each(function () {
            $(this).val(value)
        });
    },
    updateUomOption : function (value) {
        $(".js-hidden-uom-selector-input").each(function() {
            $(this).val(value)
        });
    },
    updateTotalWeight : function () {
        var totalWeight = $('.js-addtocart-total-weight-panel span[data-total-weight]').data('total-weight');
        $('#uomSelectorForm').find('.totalWeight.js-total-weight-selector-input').val(totalWeight);
    },
    updateWeightPanelAjax: function (actionUrl, $form) {
        var $addToCartWeightPanel = $('.js-addtocart-weight-panel');
        $addToCartWeightPanel.css('opacity', '0.2');
        $.post(actionUrl, $form.serialize(), function (data) {
            $addToCartWeightPanel.replaceWith(data);
            $addToCartWeightPanel.css('opacity', '1');
            $('.js-addtocart-weight-panel').show();
        });
    },
    fetchProductUomAjax: function(actionUrl, $form) {
        var $cartEntryPricePanel = $('.js-product-cart-entry-price-panel');
        var $addToCartWeightPanel = $('.js-addtocart-weight-panel');
        var $addToCartLengthPanel = $('.js-addtocart-length-panel');
        var $addToCartTotalWeightPanel = $('.js-addtocart-total-weight-panel');
        var $addToCartBasePricePanel = $('.js-addtocart-baseprice-panel');
        var $addToCartActionPanel = $('.js-addtocart-action');
        $cartEntryPricePanel.css('opacity', '0.2');
        $addToCartWeightPanel.css('opacity', '0.2');
        $addToCartLengthPanel.css('opacity', '0.2');
        $addToCartTotalWeightPanel.css('opacity', '0.2');
        $addToCartBasePricePanel.css('opacity', '0.2');
        $.post( actionUrl, $form.serialize(), function(data) {
            var resultDom = $.parseHTML(data);
            if($(resultDom).find('.js-addtocart-request-availabilty').html()) {
                $addToCartActionPanel.replaceWith($(resultDom).html());
            } else {
                $cartEntryPricePanel.html($(resultDom).find('.js-product-cart-entry-price-panel').html());
                $addToCartWeightPanel.html($(resultDom).find('.js-addtocart-weight-panel').html());
                $addToCartLengthPanel.html($(resultDom).find('.js-addtocart-length-panel').html());
                $addToCartTotalWeightPanel.html($(resultDom).find('.js-addtocart-total-weight-panel').html());
                $addToCartBasePricePanel.html($(resultDom).find('.js-addtocart-baseprice-panel').html());
                ACC.thyssenkruppeustorefront.updateTotalWeight();
                $cartEntryPricePanel.css('opacity', '1');
                $addToCartWeightPanel.css('opacity', '1');
                $addToCartTotalWeightPanel.css('opacity', '1');
                $addToCartLengthPanel.css('opacity', '1');
                $addToCartBasePricePanel.css('opacity', '1');
                if($('.js-addtocart-price-panel').length != 0) {
                    ACC.product.checkAvailibility('fadeOutText');
                }
            }
            ACC.product.enableAddToCartButton();
        }).fail(function (jqXHR) {
            if (jqXHR.status === 405) {
                window.location.href = "/login";
            }
            $('button.js-add-to-cart').attr('disabled', 'disabled');
            $('button.js-add-to-cart').removeClass('btn-primary');
        });
    },
    fetchProductVariantsAjax: function(actionUrl,form) {
        $('.js-product-variants').css('opacity', '0.2');
        this.setActionUrl = actionUrl;
        this.setForm = form;
        $.post( actionUrl, $(form).serialize(), function(data) {
            ACC.thyssenkruppeustorefront.loadNewVariantDataAjax(data);
            ACC.thyssenkruppeustorefront.setResetLinkVisibility();
            ACC.thyssenkruppeustorefront.toggleSawingButtonState();
            ACC.thyssenkruppeustorefront.fnBindCutToLengthValidation();
            ACC.thyssenkruppeustorefront.setInitialVariantSelection();
            $('.js-product-variants').css('opacity', '1');
        });
    },
    fetchCutToLengthOptionsAjax: function(sawingType) {
        var form = $('form[id=cutToLengthSelectionForm]');
        form.find('input[name="sawingType"]').val(sawingType);
        var actionUrl = form.attr('action') + '/cutToLengthSelection';
        $('.js-product-variants').css('opacity', '0.2');
        if (!$('.js-cuttolength-length').valid()) {
            form.find('input[name="selectedCutToLengthRangeInvalid"]').val("true");
        }
        $.post(actionUrl, $(form).serialize(), function (data) {
            ACC.thyssenkruppeustorefront.loadNewCutToLengthDataAjax(actionUrl, data);
            ACC.thyssenkruppeustorefront.fnUpdateCutToLengthRangeValidationRules();
            var $cutToLengthRangeInput = $('.js-cuttolength-length');
            var $cuttolengthSubmitButton = $('#cutToLengthFormSubmitButton');
            if ($cutToLengthRangeInput.val() != null) {
                $cutToLengthRangeInput.closest('.form-group').toggleClass('has-error', (!$cutToLengthRangeInput.valid() == 1));
                $('.js-cuttolength-validate-msg').toggleClass('help-block--error', (!$cutToLengthRangeInput.valid() == 1));
                $cuttolengthSubmitButton.attr("disabled", !($cutToLengthRangeInput.valid() == 1 && $('.js-cuttolength-tolerance').find(':selected').val().length > 0));
                $cuttolengthSubmitButton.toggleClass('btn-primary',($cutToLengthRangeInput.valid() == 1 && $('.js-cuttolength-tolerance').find(':selected').val().length > 0) );
            }
            $('.js-product-variants').css('opacity', '1');
        });
    },
    loadNewCutToLengthDataAjax: function(actionUrl,htmlContent) {
        var resultDom = $.parseHTML(htmlContent);
        $(resultDom).find('form[id=cutToLengthSelectionForm]').attr('action', actionUrl);
        var selectedRange = $(resultDom).find('#selectedCutToLengthRange').val();
        var selectedTolerance = $(resultDom).find('#selectedCutToLengthTolerance').val();
        if($.isNumeric(selectedRange) && selectedTolerance.length > 0){
            $(resultDom).find('#cutToLengthFormSubmitButton').attr("disabled", false);
            $(resultDom).find('#cutToLengthFormSubmitButton').addClass('btn-primary');
        }
        $('.js-cuttolength-form').html($(resultDom).html());
        $('.selectpicker--sm').selectpicker({
            style: 'btn-default btn-sm'
        });
    },
    toggleSawingButtonState: function () {
        $('#cutToLengthForm').on('show.bs.collapse', function() {
            $('.js-varient-sawing').toggleClass('btn-default active', true);
            $('.js-varient-sawing').toggleClass('btn-info', false);
        }).on('hide.bs.collapse.bs.collapse', function() {
            $('.js-varient-sawing').toggleClass('btn-default active', false);
            $('.js-varient-sawing').toggleClass('btn-info', true);
            if($('#sawing_href_link').data('set-sawing-value')) {
                $('.js-varient-sawing').toggleClass('btn-default active', true);
                $('.js-varient-sawing').toggleClass('btn-info', false);
            }
        });
        if ($('#isTradeLengthAlreadySelected').val()) {
            $('.js-varient-sawing').toggleClass('m-vcs__content__item__btn--inactive', true);
        }
        $('.selectpicker--sm').selectpicker({
            style: 'btn-default btn-sm'
        });
    },
    setResetLinkVisibility: function () {
        if($('.m-vcs__content__item__btn--active').length == 0) {
            $('.js-reset-variant-selection').hide();
        } else $('.js-reset-variant-selection').show();
    },
    loadNewVariantDataAjax: function(htmlContent) {
        var resultDom = $.parseHTML(htmlContent);
        $('.js-product-main-classifications').html( $(resultDom).find('.js-product-main-classifications').html());
        $('.js-product-detail-price-panel').html( $(resultDom).find('.js-product-detail-price-panel').html());
        $('.js-product-informations').html( $(resultDom).find('.js-product-informations').html());
        $('.js-product-specifications').html( $(resultDom).find('.js-product-specifications').html());
        $('.js-product-variants').html( $(resultDom).find('.js-product-variants').html() );
        $('.m-header').find('.alert').remove();
        $('.m-header').append($(resultDom).find('.m-header .alert'));
        if($('.m-header').find('.alert').attr("data-qa-id")=="noscript-warning-message")$('.m-header').find('.alert').remove();
        var scripts = $(htmlContent).find('.js-gtm-data script');
        window.products = [];
        for (var n = 0; n < scripts.length; n++){
            $.globalEval(scripts[n].innerHTML);
        }
        ACC.thyssenkruppeustorefront.bootProductDetailPageJs();
        ACC.thyssenkruppeustorefront.showHideProductDetails();
    },
    showHideProductDetails: function() {
        var totalVariants = $('.js-total-variant-value').val();
        var selectedVariants = $('.js-product-variant-submit.active').length;
        var selectedSawing = $('.js-varient-sawing.active').length;
        var selectedCount = selectedSawing + selectedVariants;
        var productClassifications = $('.js-product-classifications').length;
        if(totalVariants > 0 || selectedSawing > 0) {
            if((totalVariants == selectedCount)) {
                $('.js-addtocart-price-panel').show();
                $('.js-qty-selector').show();
                $('.js-addtocart-weight-panel').show();
                $('.js-addtocart-length-panel').show();
                $('.js-addtocart-total-weight-panel').show();
                $('.js-product-order-form').show();
                $('.js-default-addtocart-text').show();
                $('.js-addtocart-price-panel-text').hide();
                ACC.product.enableAddToCartButton();
                if ($('.js-addtocart-price-panel').length != 0) {
                    if ($('.js-hidden-isProductPurchasable-selector').val() === 'true') {
                        ACC.product.checkAvailibility('showLoadingIcon');
                    } else {
                        var availabilityStatus = $('.js-hidden-stockNotAvailableStatus-selector').val();
                        var availabilityMessage = $('.js-hidden-stockNotAvailableMessage-selector').val();
                        var availabilityToolTip = $('.js-hidden-stockNotAvailableToolTip-selector').val();
                        ACC.product.showAvailabilityDialogueModel("RED", availabilityStatus, availabilityMessage, availabilityToolTip);
                        $('.js-default-addtocart-text').hide();
                    }
                }
            } else {
                $('.js-addtocart-price-panel').hide();
                $('.js-qty-selector').hide();
                $('.js-addtocart-weight-panel').hide();
                $('.js-addtocart-length-panel').hide();
                $('.js-addtocart-total-weight-panel').hide();
                $('.js-product-order-form').hide();
                $('.js-default-addtocart-text').hide();
                $('.js-addtocart-price-panel-text').show();
                $('button.js-add-to-cart').attr('disabled', 'disabled');
                $('button.js-add-to-cart').removeClass('btn-primary');
            }
            if(productClassifications > 0) {
                $('.js-tabs').show();
            } else {
                $('.js-tabs').hide();
            }
        } else {
            $('.js-tabs').show();
            $('.js-addtocart-price-panel').show();
            $('.js-qty-selector').show();
            $('.js-addtocart-weight-panel').show();
            $('.js-addtocart-length-panel').show();
            $('.js-addtocart-total-weight-panel').show();
            $('.js-product-order-form').show();
            $('.js-addtocart-price-panel-text').hide();
            ACC.product.enableAddToCartButton();
        }
    },
    bootProductDetailPageJs: function() {
        ACC.product.bindToAddToCartForm();
        ACC.product.enableStorePickupButton();
        ACC.product.enableVariantSelectors();
        ACC.product.bindFacets();
        ACC.product.bindToAddToCompareForm();
        ACC.product.enableAddToCartButton();
        ACC.thyssenkruppeustorefront.bindProductUomSelect();
        ACC.thyssenkruppeustorefront.bindPageToolTips();
        ACC.thyssenkruppeustorefront.bindVolumePriceToggle();
        ACC.thyssenkruppeustorefront.bindSelectShippingAddressPopup();
    },
    bindVolumePriceToggle: function() {
        $('.js-price-volume-toggle').click(function(e) {
            e.preventDefault();
            $(this).prev('hr').toggleClass('hide');
        });
    },
    bindCookieTermsAndCondition: function () {
        setTimeout(function(){
            $('.js-cookie-banner').addClass('animate-slide-up');
        }, 500);
        $('.js-cookie-legal-terms-popup-link').click(function (e) {
            e.preventDefault();
            var popupTitle = $(this).attr('data-cbox-title');
            ACC.colorbox.open(popupTitle, {
                href: $(this).attr('href'),
                className: "js-dialogueModel cookie-legal-terms-popup",
                width: '946px',
                onComplete: function () {
                    ACC.colorbox.resize();
                    $(document).on('click', '.js-cookie-legal-term-popup-close-button', function(){
                        ACC.colorbox.close();
                    });
                },
                onClosed: function() { ACC.colorbox.close(); }
            });
        });
        $('.js-accept-cookie-terms-button').click(function (e) {
            e.preventDefault();
            Cookie.create('is-cookie-terms-accepted', 1, 30);
            $('.js-cookie-banner').remove();

        });
    },
    bindReOrderPopup: function () {
         $(document).on('click', '.js-reorder-confirmation-popup-link', function (e) {
            e.preventDefault();
            var title = $(this).attr('data-dialogue-title');
            var popupBody = $('.js-reorder-confirmation-form').html();
            ACC.thyssenkruppeustorefront.showReOrderPopup(popupBody, title);
        });
    },
    showReOrderPopup: function (popupBody, title) {
        ACC.colorbox.open(title, {
            html: popupBody,
            className: "js-dialogueModel m-popup m-popup--dialogue",
            close: '<span class="u-icon-tk u-icon-tk--close"></span>',
            title: '<div class="h4 m-colorbox__title" data-qa-id="login-popup-header">{title}</div>',
            width: '488px',
            onComplete: function() {
                ACC.colorbox.resize();
                $('.js-cancel-button').click (function () {
                    ACC.colorbox.close();
                });
            },
            onClosed: function() { ACC.colorbox.close(); }
        });
    },
    bindCartCheckStatus: function(){
        ACC.product.bindCartCheckstatus();
    },
    bindSelectShippingAddressPopup : function () {
        $('.js-add-address-link').on('click', function (e) {
            e.preventDefault();
            var subtitle = $(this).attr('data-modal-subtitle');
            var title = $(this).attr('data-modal-title');
            var actionUrl = '/delivery-address/new-shipping-address';
            ACC.colorbox.open(title, {
                href: $(this).attr('href'),
                width: '974px',
                close: '<span class="u-icon-tk u-icon-tk--close"></span>',
                title: '<div class="h4 m-colorbox__title" data-qa-id="add-address-popup-header">'+title+'</div><p class="m-item-group__label m-colorbox__dec">'+subtitle+'</p>',
                className: 'm-popup__shipping-address',
                onComplete: function() {
                    ACC.colorbox.resize();
                    $(document).off('click', '.js-add-address-submit');
                    $('#cboxLoadedContent,#cboxWrapper').css('overflow','visible');
                    ACC.thyssenkruppeustorefront.bindAddCheckoutAddressValidation('seletShippingAddressForm');

                    $('.js-shippingaddress-tab:nth-child(2)').one( "click", function() {
                        $.get(actionUrl, function (data) {
                            $('.js-shippingaddress-form').html(data);
                            $('.selectpicker').selectpicker({dropupAuto: false});
                            ACC.colorbox.resize();
                            if ($(this).find('form')) {
                                ACC.thyssenkruppeustorefront.bindAddCheckoutAddressValidation('addSippingAddressForm');
                            }
                        });
                    });
                    $(document).on('select | change', '.js-select-shippingaddress', function () {
                       $('.js-add-address-submit, .js-checkout-next-step').attr('disabled', !$('.js-choose-shipping-address').valid());
                    });
                    $('.selectpicker').selectpicker({dropupAuto: false});
                    $('.js-cancel-button').click (function () {
                        ACC.colorbox.close();
                    });
                    $('.js-shippingaddress-tab').on('shown.bs.tab', function () {
                        ACC.colorbox.resize();
                        var containerId = $(this).find('a').attr('href');
                        if($(containerId).html()) {
                            $('.js-add-address-submit').attr('disabled', !$(containerId).find('form').valid());
                        }
                    });
                    ACC.colorbox.resize();
                    $(document).on('click', '.js-add-address-submit', function(e) {
                        e.preventDefault();
                        var $form = $(this).closest('.m-popup__shipping-address').find('.tab-pane.active.in');
                        $form.find('button[type="submit"], input[type="submit"]').attr('disabled', 'disabled');
                        var $currentform = $form.find('form');
                        var actionUrl = $currentform.attr('action');
                        $.post(actionUrl, $currentform.serialize(), function (data) {
                            if (data.indexOf('has-error') === -1) {
                                var msg = $(data).find('.js-alert-msg').text().trim();
                                if (msg && msg.length > 0) {
                                    ACC.thyssenkruppeustorefront.setGlobalMessage(msg);
                                }
                                $('.js-equal-height').syncHeight({ 'updateOnResize': true});
                                ACC.colorbox.close();
                                if ($('.js-product-detail-price-panel').length > 0) {
                                    ACC.thyssenkruppeustorefront.setForm.find('#isSelectionIgnore').val(true);
                                    ACC.thyssenkruppeustorefront.fetchProductVariantsAjax(ACC.thyssenkruppeustorefront.setActionUrl, ACC.thyssenkruppeustorefront.setForm);
                                } else {
                                    window.location.reload();
                                }
                            } else {
                                $('#cboxLoadedContent .js-shippingaddress-form .js-shipping-address-modal').replaceWith(data);
                                $('.selectpicker').selectpicker();
                                $('.js-cancel-button').click (function () {
                                    ACC.colorbox.close();
                                });
                                ACC.colorbox.resize();
                            }
                        });
                    });
                },
                onClosed: function() {
                    $('.js-checkout-next-step').attr('disabled', false);
                    ACC.colorbox.close();
                }
            });
        });
        $('.js-confirm-address-link').on('click', function () {
            var actionUrl = "/delivery-address/choose-primary-shipping-address";
            $.post(actionUrl,'', function (data) {
                if(data) {
                    ACC.thyssenkruppeustorefront.setForm.find('#isSelectionIgnore').val(true);
                    ACC.thyssenkruppeustorefront.fetchProductVariantsAjax(ACC.thyssenkruppeustorefront.setActionUrl, ACC.thyssenkruppeustorefront.setForm);
                }
            });
        });
    },
};
$(document).ready(function(){
    if(localStorage.getItem("outOfStockProducts"))
    {
        var outOfStockProducts= localStorage.getItem("outOfStockProducts");
        var outOfStockProductsObj= JSON.parse(outOfStockProducts);
        var outOfStockTxt = "<span>Folgende Produkte wurden aufgrund von Nichtverfügbarkeit aus Ihrem Warenkorb entfernt:</span><br>";
        $.each( outOfStockProductsObj, function( key, value ) {
              outOfStockTxt += "<span><a href=' "+ value.url +" ' target='_blank'>"+ value.name +"("+ key +")"+"</a></span><br>";
            });
        ACC.thyssenkruppeustorefront.setGlobalMessage(outOfStockTxt);
        localStorage.clear();
    }
});

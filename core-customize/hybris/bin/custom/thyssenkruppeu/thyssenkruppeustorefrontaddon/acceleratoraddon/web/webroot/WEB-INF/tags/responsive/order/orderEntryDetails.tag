<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="consignmentEntry" required="false" type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData" %>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer" %>
<%@ attribute name="targetUrl" required="false" type="java.lang.String" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showViewConfigurationInfos" required="false" type="java.lang.Boolean" %>
<%@ attribute name="viewConfigurationInfosBaseUrl" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="varShowStock" value="${(empty showStock) ? true : showStock}" />
<c:set var="defaultViewConfigurationInfosBaseUrl" value="/my-account/order" />
<c:url value="${orderEntry.product.url}" var="productUrl"/>
<c:set var="entryStock" value="${fn:escapeXml(orderEntry.product.stock.stockLevelStatus.code)}"/>
<li class="m-order-detail__item-list--item item__list--item">
    <%--NOTE: Accelerator inherited fields, unhide if required.
    <div class="hidden-xs hidden-sm item__toggle hide">
        <c:if test="${orderEntry.product.multidimensional}">
            <div class="js-show-multiD-grid-in-order" data-index="${itemIndex}">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </div>
        </c:if>
    </div>
    <div class="m-line-item__position item__position hide" data-qa-id="order-details-item-position">${fn:escapeXml(orderEntry.positionNumber)}</div> --%>
    <div class="m-line-item__image item__image" data-qa-id="order-details-item-img">
        <a href="${orderEntry.product.purchasable ? productUrl : 'javascript:void(0);'}">
            <product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
        </a>
    </div>
    <div class="item__product" data-qa-id="order-details-item-product">
        <a href="${orderEntry.product.purchasable ? productUrl : 'javascript:void(0);'}"><span class="m-text-color--black" data-qa-id="confirmation-product-name">${fn:escapeXml(orderEntry.product.name)}</span></a>
        <%--NOTE: Accelerator inherited fields, unhide if required.
        <p class="item__code" data-qa-id="order-details-item-code">
           <small>${fn:escapeXml(orderEntry.product.code)}</small>
        </p>
        <c:if test="${varShowStock}">
           <p class="item__stock" data-qa-id="order-details-item-stock-details">
               <c:choose>
                   <c:when test="${orderEntry.product.multidimensional}">
                       <small class="stock"><spring:theme code="product.variants.in.stock"/></small>
                   </c:when>
                   <c:when test="${not empty entryStock and entryStock ne 'outOfStock'}">
                       <small class="stock"><spring:theme code="product.variants.in.stock"/></small>
                   </c:when>
                   <c:when test="${orderEntry.deliveryPointOfService eq null}">
                       <small class="out-of-stock"><spring:theme code="product.variants.out.of.stock"/></small>
                   </c:when>
               </c:choose>
           </p>
        </c:if> --%>
        <div class="item__attributes">
            <c:if test="${not empty orderEntry.product.variantValues}">
                <div class="item__attributes">
                    <c:forEach items="${orderEntry.product.variantValues}" var="variantValue" varStatus="loop">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="m-item-group__label small m-text-color--grey-dark" data-qa-id="variant-attribute-label" >${fn:escapeXml(variantValue.name)}:</p>
                            </div>
                            <div class="col-sm-7">
                                <p class="m-item-group__value small" data-qa-id="variant-attribute-value" >${fn:escapeXml(variantValue.value)}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </div>
    <div class="item__quantity hidden-xs hidden-sm">
        <c:forEach items="${orderEntry.product.baseOptions}" var="option">
            <c:if test="${not empty option.selected and option.selected.url eq orderEntry.product.url}">
                <c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
                    <div>
                        <span>${fn:escapeXml(selectedOption.name)}:</span>
                        <span>${fn:escapeXml(selectedOption.value)}</span>
                    </div>
                    <c:set var="entryStock" value="${fn:escapeXml(option.selected.stock.stockLevelStatus.code)}"/>
                </c:forEach>
            </c:if>
        </c:forEach>
        <label class="visible-xs visible-sm">
            <spring:theme code="text.account.order.qty"/>
            :
        </label>
        <span class="qtyValue" data-qa-id="confirmation-quantity-value">
            <c:choose>
                <c:when test="${consignmentEntry ne null }">
                    ${consignmentEntry.quantity}
                </c:when>
                <c:otherwise>
                    ${orderEntry.quantity}
                </c:otherwise>
            </c:choose>
            <c:if test="${not empty orderEntry.unitName}">
                &nbsp;${orderEntry.unitName}
            </c:if>
        </span>
    </div>
    <div class="item__price" data-qa-id="order-details-item-price">
        <span class="visible-xs visible-sm">
            <spring:theme code="basket.page.itemPrice"/>
            :
        </span>
        <span class="item__price__sales-value" data-qa-id="confirmation-item-price-sales-uom-value">
            <tk-eu-format:price priceData="${orderEntry.pricePerBaseUnit}" withUnit="${true}" displayFreeForZero="true"/>
        </span>
    </div>
    <div class="item__weight text-right">
        <span class="item__weight__per-unit" data-qa-id="confirmation-weight-sales-uom-value">
            <c:if test="${not empty orderEntry.salesUnitData && !orderEntry.salesUnitData.isWeightedUnit}">
                <tk-eu-format:price priceData="${orderEntry.kgPerSalesUnit}" withUnit="${true}" isWeight= "${true}"/>
            </c:if>
        </span>
        <p class="item__weight__total small" data-qa-id="confirmation-total-weight-value"><tk-eu-format:price priceData="${orderEntry.totalWeightInKg}" withUnit="${true}" isWeight= "${true}"/></p>
    </div>
    <div class="item__total hidden-xs hidden-sm text-right" data-qa-id="order-details-item-price">
        <format:price priceData="${orderEntry.subTotal}" displayFreeForZero="true"/>
    </div>
    <div class="item__quantity__total visible-xs visible-sm text-right">
        <c:set var="showEditableGridClass" value=""/>
        <c:if test="${orderEntry.product.multidimensional}">
            <c:set var="showEditableGridClass" value="js-show-multiD-grid-in-order"/>
        </c:if>
        <c:set var="orderEntryQuantity" value="0"/>
        <div class="details ${showEditableGridClass}" data-index="${itemIndex}">
            <div class="qty">
                <label>
                    <spring:theme code="text.account.order.qty"/>
                    :
                </label>
                <span class="qtyValue">
                    <c:choose>
                        <c:when test="${consignmentEntry ne null }">
                            ${consignmentEntry.quantity}
                            <c:set var="orderEntryQuantity" value="${consignmentEntry.quantity}"/>
                        </c:when>
                        <c:otherwise>
                            ${orderEntry.quantity}
                            <c:set var="orderEntryQuantity" value="${orderEntry.quantity}"/>
                        </c:otherwise>
                    </c:choose>
                </span>
                <c:if test="${orderEntry.product.multidimensional}">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </c:if>
                <div class="item__total" data-qa-id="confirmation-price-value">
                    <format:price priceData="${orderEntry.totalPrice}" displayFreeForZero="true"/>
                </div>
            </div>
        </div>
    </div>
</li>
<li class="item__list--options">
    <div class="item__options">
        <c:set var="certificate" value="" scope="request"/>
        <common:configurationInfos entry="${orderEntry}" certificates="${certificate}" />
        <%--NOTE: Accelerator inherited display configuration feature. Unhide when required.
        <c:if test="${not empty orderEntry.configurationInfos and not isTradeLength}">
            <div class="row item__options__row">
                <div class="col-md-6">
                    <div class="item__attributes">
                        <div class="m-item-group--horizontal">
                            <p class="m-item-group__label"><small>&nbsp;</small></p>
                            <div class="m-item-group__value">
                                <c:url value="${empty viewConfigurationInfosBaseUrl ? defaultViewConfigurationInfosBaseUrl : viewConfigurationInfosBaseUrl}/${order.code}/${orderEntry.entryNumber}/configurationDisplay/${orderEntry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
                                <a href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if> --%>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-7">
            <div class="m-order-detail__subtotal">
             <div class="row">
                         <div class="col-sm-12">
                             <div class="pali-gray-hr1 m-b-10"></div>
                         </div>
                     </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="pali-copy primary semi-strong" data-qa-id="confirmation-total-label">
                            <spring:theme code="basket.page.item.total"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="pali-copy primary semi-strong text-right" data-qa-id="confirmation-total-value">

                        <c:choose>
                                    <c:when test="${not empty orderEntry.entryTotalDiscount}">
                                         <strike> <format:price priceData="${orderEntry.entryTotalPriceWithoutDiscount}" displayFreeForZero="true"/></strike>
                                    </c:when><c:otherwise>
                                    <div class="pali-copy primary semi-strong text-right" data-qa-id="cart-item-grand-total-value">
                                        <format:price priceData="${orderEntry.totalPrice}" displayFreeForZero="true"/>
                                    </div>
                                    </c:otherwise>
                                    </c:choose>
                        </div>
                    </div>
                </div>
                <c:if test="${not empty orderEntry.entryTotalDiscount}">
                <div class="row p-b-5">
                    <div class="col-xs-6">
                        <div class="pali-copy references" data-qa-id="confirmation-total-label">
                             <format:price priceData="${orderEntry.entryTotalDiscount}"/>&nbsp;<spring:theme code="discount.on.item" text="Material discount"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="pali-copy primary red-text semi-strong text-right" data-qa-id="confirmation-total-value">
                           <format:price priceData="${orderEntry.totalPrice}" displayFreeForZero="true"/>
                        </div>
                    </div>
                </div></c:if>
            </div>
        </div>
    </div>
    <%--NOTE: Baseshop inherited promotion feature, use when required.--%>
   <%--  <c:if test="${not empty order.appliedProductPromotions}">
        <ul>
            <c:forEach items="${order.appliedProductPromotions}" var="promotion">
                <c:set var="displayed" value="false"/>
                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                    <c:if test="${not displayed and consumedEntry.orderEntryNumber == orderEntry.entryNumber}">
                        <c:set var="displayed" value="true"/> ${consumedEntry.adjustedUnitPrice}
                        <li><small>${ycommerce:sanitizeHTML(promotion.description)}</small></li>
                    </c:if>
                </c:forEach>
            </c:forEach>
        </ul>
        </div>
    </c:if>
    --%> <c:forEach items="${orderEntry.product.variantValues}" var="variantValue" varStatus="loop">
        <c:set var="variantCode" value="${variantValue.code}"/>
        <c:if test = "${fn:contains( variantCode , 'Trade_Length')}">
            <c:set var="dimension2"  value="${variantValue.value}"/>
        </c:if>
        <c:if test = "${fn:contains( variantCode , 'CUTTOLENGTH_PRODINFO')}">
            <c:set var="dimension3"  value="${variantValue.value}"/>
        </c:if>
    </c:forEach>
    <gtm:checkoutProductGTM product="${orderEntry.product}" price="${orderEntry.pricePerBaseUnit.value}" unitName="${orderEntry.unitName}" quantity="${orderEntryQuantity}" dimension1="${certificate}" showPrice="${true}" dimension2="${dimension2}" dimension3="${dimension3}"/>
</li>
<li class="hide">
    <c:if test="${empty targetUrl}">
        <spring:url value="/my-account/order/{/orderCode}/getReadOnlyProductVariantMatrix" var="targetUrl">
            <spring:param name="orderCode" value="${order.code}"/>
        </spring:url>
    </c:if>
    <grid:gridWrapper entry="${orderEntry}" index="${itemIndex}" styleClass="hide add-to-cart-order-form-wrap" targetUrl="${targetUrl}"/>
</li>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<spring:url value="/register/termsAndConditionsRegistration" var="getTermsAndConditionsRegistrationUrl"/>
<template:page pageTitle="${pageTitle}" class="l-page l-page__checkout" hideHeaderLinks="true" isMiniHeaderFooter="true" >
    <div class="container l-page__content">
        <h3 class="l-page__title--no-description"  data-qa-id="checkout-header">
            <spring:theme code="checkout.multi.secure.checkout"></spring:theme>
        </h3>
        <div class="row">
            <div class="col-sm-6">
                <tk-b2b-multi-checkout:shippingCheckoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                    <jsp:body>
                        <c:set var="addresses" value="${deliveryAddresses}" scope="request"/>
                        <tk-b2b-multi-checkout:shippingAddressTab disableStatus="disabled" cartData="${cartData}"/>
                    </jsp:body>
                </tk-b2b-multi-checkout:shippingCheckoutSteps>
                <tk-b2b-multi-checkout:billingCheckoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                    <jsp:body>
                        <c:set var="addresses" value="${billingAddresses}" scope="request" />
                        <tk-b2b-multi-checkout:billingAddressTab disableStatus="disabled"/>
                    </jsp:body>
                </tk-b2b-multi-checkout:billingCheckoutSteps>
                <tk-b2b-multi-checkout:billingAndFinalCheckoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                    <span class="m-text-color--grey-dark">
                        <spring:theme code="text.checkout.finaltab"/>
                    </span>
                    <hr class="m-checkout-tab__separator" />
                </tk-b2b-multi-checkout:billingAndFinalCheckoutSteps>
            </div>
            <div class="col-sm-6">
                <tk-b2b-multi-checkout:checkoutOrderSummary cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="true" showTaxEstimate="true" showTax="true" showPlaceOrderForm="${true}" />
            </div>
        </div>
    </div>
</template:page>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class='m-cookie-banner__legal-terms__wrapper js-cookie-legal-term-content'>
    <div class='m-cookie-banner__legal-terms'>
        <div class='m-cookie-banner__legal-terms__content' data-qa-id='accept-cookies-legal-notes-section'>
            <cms:pageSlot position="Section2B" var="feature" class="termsAndConditionsRegistration-section" element="div">
                <cms:component component="${feature}" element="div" class="clearfix"/>
            </cms:pageSlot>
        </div>
        <div class="m-cookie-banner__legal-terms__actions text-center">
            <button type="button" class="btn btn-default js-cookie-legal-term-popup-close-button" data-qa-id="accept-cookies-legal-notes-close">
                <spring:theme code="cookieNotification.legalNotes.close" text="Close" />
            </button>
        </div>
    </div>
</div>


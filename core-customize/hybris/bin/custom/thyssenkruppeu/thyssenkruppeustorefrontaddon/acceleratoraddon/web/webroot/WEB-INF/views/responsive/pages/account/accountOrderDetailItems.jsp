<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tk-b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty orderData.unconsignedEntries}">
    <tk-b2b-order:orderUnconsignedEntries order="${orderData}"/>
</c:if>
<c:forEach items="${orderData.consignments}" var="consignment">
    <c:if test="${consignment.status.code eq 'WAITING' or consignment.status.code eq 'PICKPACK' or consignment.status.code eq 'READY'}">
        <div class="fulfilment-states-${fn:escapeXml(consignment.status.code)}">
            <order:accountOrderDetailsItem order="${orderData}" consignment="${consignment}" inProgress="true"/>
        </div>
    </c:if>
</c:forEach>
<c:forEach items="${orderData.consignments}" var="consignment">
    <c:if test="${consignment.status.code ne 'WAITING' and consignment.status.code ne 'PICKPACK' and consignment.status.code ne 'READY'}">
        <div class="fulfilment-states-${fn:escapeXml(consignment.status.code)}">
            <order:accountOrderDetailsItem order="${orderData}" consignment="${consignment}"/>
        </div>
    </c:if>
</c:forEach>

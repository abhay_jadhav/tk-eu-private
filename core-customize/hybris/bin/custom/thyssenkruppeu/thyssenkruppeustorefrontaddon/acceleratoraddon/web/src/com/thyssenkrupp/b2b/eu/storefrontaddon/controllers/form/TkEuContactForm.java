package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TkEuContactForm {

    private String title;
    private String firstname;
    private String lastname;
    private String company;
    private String email;
    private String topic;
    private String message;
    private String acceptCheck;
    private String productCode;
    private String certOption;
    private String salesUom;
    private String selectedTradeLength;
    private String selectedCutToLengthRange;
    private String selectedCutToLengthTolerance;
    private String selectedCutToLengthRangeUnit;
    private long   qty;

    @NotNull(message = "{contact.title.invalid}")
    @Size(min = 1, max = 255, message = "{contact.title.invalid}")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull(message = "{contact.firstname.invalid}")
    @Size(min = 1, max = 255, message = "{contact.firstname.invalid}")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @NotNull(message = "{contact.lastname.invalid}")
    @Size(min = 1, max = 255, message = "{contact.lastname.invalid}")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @NotNull(message = "{contact.email.invalid}")
    @Size(min = 1, max = 255, message = "{contact.email.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull(message = "{contact.topic.invalid}")
    @Size(min = 1, max = 255, message = "{contact.topic.invalid}")
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @NotNull(message = "{contact.message.invalid}")
    @Size(min = 1, max = 5000, message = "{contact.message.invalid}")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAcceptCheck() {
        return acceptCheck;
    }

    public void setAcceptCheck(String acceptCheck) {
        this.acceptCheck = acceptCheck;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCertOption() {
        return certOption;
    }

    public void setCertOption(String certOption) {
        this.certOption = certOption;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public String getSalesUom() {
        return salesUom;
    }

    public void setSalesUom(String salesUom) {
        this.salesUom = salesUom;
    }

    public String getSelectedTradeLength() {
        return selectedTradeLength;
    }

    public void setSelectedTradeLength(String selectedTradeLength) {
        this.selectedTradeLength = selectedTradeLength;
    }

    public String getSelectedCutToLengthRange() {
        return selectedCutToLengthRange;
    }

    public void setSelectedCutToLengthRange(String selectedCutToLengthRange) {
        this.selectedCutToLengthRange = selectedCutToLengthRange;
    }

    public String getSelectedCutToLengthTolerance() {
        return selectedCutToLengthTolerance;
    }

    public void setSelectedCutToLengthTolerance(String selectedCutToLengthTolerance) {
        this.selectedCutToLengthTolerance = selectedCutToLengthTolerance;
    }

    public String getSelectedCutToLengthRangeUnit() {
        return selectedCutToLengthRangeUnit;
    }

    public void setSelectedCutToLengthRangeUnit(String selectedCutToLengthRangeUnit) {
        this.selectedCutToLengthRangeUnit = selectedCutToLengthRangeUnit;
    }
}

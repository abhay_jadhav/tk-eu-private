<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="label-order"><spring:theme code="checkout.multi.billing.to"/></div>
<ul>
    <li>
        <c:if test="${not empty orderData.billingAddress.title}">
            ${fn:escapeXml(orderData.billingAddress.title)}&nbsp;
        </c:if>
        ${fn:escapeXml(orderData.billingAddress.firstName)}&nbsp;${fn:escapeXml(orderData.billingAddress.lastName)}
    </li>
    <li>${fn:escapeXml(orderData.billingAddress.line1)}</li>
    <li>${fn:escapeXml(orderData.billingAddress.line2)}</li>
    <li>
        <c:if test="${not empty orderData.billingAddress.region.name}">
            ${fn:escapeXml(orderData.billingAddress.region.name)}&nbsp;
        </c:if>
        ${fn:escapeXml(orderData.billingAddress.postalCode)}&nbsp;${fn:escapeXml(orderData.billingAddress.town)}
    </li>
    <li>${fn:escapeXml(orderData.billingAddress.country.name)}</li>
</ul>

<div class="label-order"><spring:theme code="text.paymentDetails" text="Payment Details"/></div>
<ul>
    <li><spring:theme code="text.account.paymentType"/>:&nbsp;<spring:theme code="text.account.paymentType.${order.paymentType.code}"/></li>
    <li><spring:theme code="checkout.multi.summary.orderPlacedBy"/>:&nbsp;<spring:theme code="text.company.user.${order.b2bCustomerData.titleCode}.name" text=""/>&nbsp;${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</li>
    <c:if test="${(not empty order.costCenter) and (not empty order.costCenter.code)}">
        <li><spring:theme code="checkout.multi.costCenter.label" />:&nbsp;${fn:escapeXml(order.costCenter.name)}</li>
    </c:if>
    <c:if test="${not empty order.purchaseOrderNumber}">
        <li><spring:theme code="checkout.multi.purchaseOrderNumber.label" />:&nbsp;${fn:escapeXml(order.purchaseOrderNumber)}</li>
    </c:if>
</ul>

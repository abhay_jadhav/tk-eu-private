<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="m-carousel-product m-carousel-product--multi js-owl-carousel js-owl-multi-products" data-qa-id="product-multi-carousel">
    <c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="productReference">
        <c:url value="${productReference.target.url}" var="productUrl"/>

        <div class="item multi--item">
            <div class="m-encourage-blocks m-encourage-blocks--has-bottom-link">
                <div class="m-encourage-blocks__content">
                    <div class="m-encourage-blocks__content__img">
                        <a href="${productUrl}">
                            <product:productPrimaryReferenceImage product="${productReference.target}" format="product" />
                        </a>
                    </div>
                    <c:if test="${component.displayProductTitles}">
                        <div class="item-name" data-qa-id="pdp-carousel-name">

                            <c:set var="productTitle" value="${fn:escapeXml(productReference.target.name)}" />
                             <a class="m-text-color--black" href="${productUrl}">
                                <c:set var="tooLong" value="${fn:length(productTitle) > 60}" />
                                ${tooLong ? fn:substring(productTitle, 0, 60) : productTitle}${tooLong ? '...' : ''}
                              </a>
                        </div>
                    </c:if>
                    <c:if test="${not empty productReference.tkEuPLPDisplayList}">
                        <div class="m-product-attributes">
                            <c:forEach items="${productReference.tkEuPLPDisplayList}" var="listElement">
                                <div class="m-product-attributes__item">
                                    <div class="m-product-attributes__item__key">
                                        ${listElement.key}
                                    </div>
                                    <div class="m-product-attributes__item__value">
                                        ${listElement.value}
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
                <div class="m-encourage-blocks__bottom">
                    <a href="${productReference.target.url}" class="m-encourage-blocks__bottom-link" data-qa-id="pdp-carousel-view-product-link">
                        <spring:theme code="carousel.view.label"/>
                    </a>
                    <span class="u-icon-tk u-icon-tk--arrow-right"></span>
                </div>
            </div>
        </div>
    </c:forEach>
</div>

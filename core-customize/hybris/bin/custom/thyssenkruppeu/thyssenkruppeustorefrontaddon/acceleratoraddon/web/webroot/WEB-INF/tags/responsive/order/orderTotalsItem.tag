<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" media="all" href="/_ui/responsive/patternlibrary/css/generic.min.css?v=${buildTimestamp}"/>

<%@ attribute name="containerCSS" required="false" type="java.lang.String" %>

<div class="orderTotal m-order-detail__subtotal">
<c:if test="${not empty order.appliedProductPromotions}">
<div class="row">
    <div class="col-xs-12">
        <p class="pali-copy references p-b-10">
            <format:price priceData="${order.productDiscounts}"/> <spring:theme code="basket.total.saving"/>
        </p>
        <div class="pali-gray-hr1 m-b-10"></div>
    </div>
</div>
</c:if>

    <div class="row">
        <div class="col-xs-6">
            <div class="pali-copy primary semi-strong p-b-10">
                <spring:theme code="text.account.order.subtotal" />
            </div>
        </div>
        <div class="col-xs-6">
            <div class="text-right pali-copy primary semi-strong p-b-10" data-qa-id="confirmation-subtotal">
                <format:price priceData="${order.subTotalWithoutDiscounts}" />
            </div>
        </div>
    </div>
    <div class="pali-black-hr1 shadow m-b-15"></div>

      <c:if test="${not empty order.appliedOrderPromotions}">
    <div class="row">
        <div class="col-sm-12">
    <div class="order-savings m-b-15">
        <ycommerce:testId code="order_recievedPromotions_label">
            <div class="pali-copy secondary semi-bold">
            <spring:theme code="text.account.order.receivedPromotions"/>
            </div>

            <c:forEach items="${order.appliedOrderPromotions}" var="promotion">
                <div class="pali-copy references grey-dark-text">
                    <ycommerce:testId code="orderDetails_orderPromotion_label">
                        <c:choose>
                            <c:when test="${not empty promotion.description}">
                                ${ycommerce:sanitizeHTML(promotion.description)}
                            </c:when>
                            <c:otherwise>
                                ${ycommerce:sanitizeHTML(promotion.promotionData.description)}
                            </c:otherwise>
                        </c:choose>
                    </ycommerce:testId>
                </div>
            </c:forEach>
            <c:forEach items="${order.appliedProductPromotions}" var="promotion">
                <div class="pali-copy references grey-dark-text">
                    <ycommerce:testId code="orderDetails_orderPromotion_label">
                        <c:choose>
                            <c:when test="${not empty promotion.description}">
                                ${ycommerce:sanitizeHTML(promotion.description)}
                            </c:when>
                            <c:otherwise>
                                ${ycommerce:sanitizeHTML(promotion.promotionData.description)}
                            </c:otherwise>
                        </c:choose>
                    </ycommerce:testId>
                </div>
            </c:forEach>
        </ycommerce:testId>
    </div>

        </div>
    </div>
        <div class="pali-gray-hr1"></div>
        </c:if>

   <c:if test="${not empty order.appliedVouchers}">
    <div class="row">
        <div class="col-sm-12">
            <div class="cart-voucher m-t-5">
                    <c:forEach items="${order.appliedVouchers}" var="voucher">
                    <div class="voucher-list__item pali-copy primary semi-bold p-tb-10">
                        <span class="coupon-code">${fn:escapeXml(voucher)}</span>
                    </div>
                </c:forEach>
                </div>
        </div>
    </div>
<div class="pali-black-hr1 shadow m-b-15"></div>
    </c:if>


    <c:if test="${order.quoteDiscounts.value > 0}">
        <div class="row">
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references grey-dark-text">
                    <spring:theme code="basket.page.quote.discounts" />
                </p>
            </div>
            <div class="col-xs-6 m-b-5 text-right">
                <p class="pali-copy references grey-dark-text">
                    <format:price priceData="${order.quoteDiscounts}" />
                </p>
            </div>
         </div>
    </c:if>
    <c:if test="${order.orderDiscounts.value > 0}">
        <div class="row">
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references semi-bold red-text">
                    <spring:theme code="text.account.order.discount" />
                </p>
            </div>
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references semi-bold red-text text-right">
                    <format:price priceData="${order.orderDiscounts}" displayFreeForZero="false" />
                </p>
            </div>
        </div>
    </c:if>
    <c:if test="${not empty order.deliveryCost && order.deliveryCost.value > 0}">
        <div class="row">
            <div class="col-xs-6 m-b-5">
                <spring:theme code="checkout.shipping.tooltip.text" var="shippingToolTip"/>
                <p class="pali-copy references grey-dark-text">
                    <spring:theme code="text.account.order.shipping" />
                    <i data-toggle="tooltip" title="${shippingToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
                </p>
            </div>
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references grey-dark-text text-right" data-qa-id="confirmation-shipping-cost">
                    <format:price priceData="${order.deliveryCost}" displayFreeForZero="true" />
                </p>
            </div>
        </div>
    </c:if>
    <div class="row">
        <c:if test="${not empty order.handlingCosts && order.handlingCosts.value > 0}">
        <div class="col-xs-8 m-b-5">
            <spring:theme code="checkout.handling.tooltip.text" var="handlingToolTip"/>
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="text.account.order.handling" />
                <i data-toggle="tooltip" title="${handlingToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
            </p>
        </div>
        <div class="col-xs-4 m-b-5">
            <p class="pali-copy references grey-dark-text text-right" data-qa-id="confirmation-handling-value">
                <format:price priceData="${order.handlingCosts}" displayFreeForZero="true" />
            </p>
        </div>
        </c:if>
        <c:if test="${not empty order.minimumOrderValueSurcharge && order.minimumOrderValueSurcharge.value > 0}">
            <spring:theme code="cart.minimumSurcharge.tooltip" var="surchargeToolTip" arguments="${order.minimumOrderValueThresholdValue}###${order.minimumSurchargeDisplayToolTipValue}"  argumentSeparator="###"/>
            <div class="col-md-7 m-b-5">
                <p class="pali-copy references grey-dark-text" data-qa-id="confirmation-minimum-surcharge-label">
                    <spring:theme code="cart.minimumSurcharge.label"/>
                    <i data-toggle="tooltip" title="${surchargeToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
                </p>
            </div>
            <div class="col-md-5 m-b-5">
                <p class="pali-copy references grey-dark-text text-right" data-qa-id="confirmation-minimum-surcharge-value">
                    <format:price priceData="${order.minimumOrderValueSurcharge}"/>
                </p>
            </div>
        </c:if>

        <c:if test="${not empty order.packagingCosts}">
            <div class="col-md-7 m-b-5">
                <p class="pali-copy references grey-dark-text">
                    <spring:theme code="checkout.summary.page.totals.packaging" /><i data-toggle="tooltip" title="<spring:theme code="checkout.packaging.flat.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                </p>
            </div>
            <div class="col-md-5 m-b-5">
                <p class="pali-copy references grey-dark-text text-right">
                <format:price priceData="${order.packagingCosts}" />
                </p>
            </div>
        </c:if>

        <c:if test="${not empty order.asmServiceFee}">
            <spring:theme code="checkout.serviceFee.tooltip.text" var="servicefeeTooltip"/>
            <div class="col-md-7 m-b-5">
                <p class="pali-copy references grey-dark-text">
                        <spring:theme code="checkout.serviceFee.text"/>
                        <i data-toggle="tooltip" title="${servicefeeTooltip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
                </p>
            </div>
            <div class="col-md-5 m-b-5">
                <p class="pali-copy references grey-dark-text text-right">
                        <format:price priceData="${order.asmServiceFee}" displayFreeForZero="TRUE"/>
                </p>
            </div>
        </c:if>
    </div>

    <c:if test="${not empty order.totalPrice}">
        <div class="row">
            <div class="col-xs-6 m-b-5">
                <div class="pali-copy references grey-dark-text">
                    <spring:theme code="basket.page.totals.withoutTax" />
                </div>
            </div>
            <div class="col-xs-6 m-b-5">
                <div class="pali-copy references grey-dark-text text-right" data-qa-id="confirmation-netprice-cost">
                    <format:price priceData="${order.totalPrice}" />
                </div>
            </div>
        </div>
    </c:if>
    <c:if test="${order.net}">
        <div class="row">
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references grey-dark-text">
                    <spring:theme code="checkout.orderConfirmation.tax.percentage.text" />
                </p>
            </div>
            <div class="col-xs-6 m-b-5">
                <p class="pali-copy references grey-dark-text text-right" data-qa-id="confirmation-tax">
                    <format:price priceData="${order.totalTax}" />
                </p>
            </div>
        </div>
    </c:if>
 <div class="pali-black-hr1 shadow m-b-15"></div>
    <div class="row">
        <div class="col-xs-6 m-b-5">
            <div class="pali-h3">
                <spring:theme code="text.account.order.orderTotals" />
            </div>
        </div>
        <c:choose>
            <c:when test="${order.net}">
                <div class="col-xs-6 m-b-5">
                    <div class="pali-h3 text-right primary-text">
                        <format:price priceData="${order.totalPriceWithTax}" />
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="col-xs-6 m-b-5">
                    <div class="pali-h3 text-right primary-text">
                        <format:price priceData="${order.totalPrice}" />
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="col-xs-6 m-b-5">
            <p class="pali-copy references grey-dark-text"><spring:theme code="text.account.orderHistory.totalWeight"/></p>
        </div>
        <div class="col-xs-6 m-b-5">
            <p class="pali-copy references grey-dark-text text-right"><tk-eu-format:price priceData="${order.totalWeightInkg}" withUnit="${true}" isWeight="${true}"/></p>
        </div>
    </div>
</div>
<div class="account-orderdetail-orderTotalDiscount-section">
    <c:if test="${not order.net}">
        <div class="order-total__taxes">
            <spring:theme code="text.account.order.total.includesTax" argumentSeparator=";" arguments="${order.totalTax.formattedValue}" />
        </div>
        <br />
    </c:if>
</div>
<gtm:orderConfirmationGTM order="${order}" />

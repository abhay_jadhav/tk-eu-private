<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="container l-page__content">
    <div class="row">
        <div class="col-md-7">
            <h3 class="l-page__title">${headerText}</h3>
            <p class="l-page__description">
              <span class="u-icon-tk u-icon-tk--message-important u-icon-tk--context-warning"></span>&nbsp;${headerDetailText}
            </p>
            <form method="post" action="<c:url value='${resendEmaiUrl}'/>">
                <input name="token" type="hidden" value="${token}"/>
                <input name="email" type="hidden" value="${email}"/>
                <button class="btn btn-primary" type="submit">${button}</button>
            </form>
        </div>
    </div>
    <c:if test="${emailStatus}">
        <div class="row">
            <div class="col-md-4">
                <div class="l-page__token-expired__msg-container">
                    <div class="l-page__token-expired__msg-container__message">
                        ${confirmationMessage}
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</div>

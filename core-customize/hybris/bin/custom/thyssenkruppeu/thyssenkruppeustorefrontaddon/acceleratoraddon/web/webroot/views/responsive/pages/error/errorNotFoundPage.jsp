<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<c:url value="/" var="homePageUrl" />

<template:page pageTitle="${pageTitle}" class="l-page l-page__error">
    <div class="container l-page__content">
        <div class="row">
            <div class="col-md-12">
                <cms:pageSlot position="MiddleContent" var="comp" >
                    <cms:component component="${comp}" />
                </cms:pageSlot>
                <cms:pageSlot position="BottomContent" var="comp" >
                    <cms:component component="${comp}" />
                </cms:pageSlot>
                <cms:pageSlot position="SideContent" var="feature" >
                    <cms:component component="${feature}" />
                </cms:pageSlot>

                <div class="error-page">
                    <a class="btn btn-default js-shopping-button" href="${homePageUrl}">
                        <spring:theme text="Continue Shopping" code="general.continue.shopping"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</template:page>

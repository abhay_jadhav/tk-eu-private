<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="m-page-section">
    <h3 class="h5 m-page-section__title" data-qa-id="order-details-contract-condition-header"><spring:theme code="checkout.orderConfirmation.contractConditions" text="Contract conditions" /></h3>
    <div class="m-page-section__content">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-method-label"><spring:theme code="checkout.orderConfirmation.shippingMethod" /></p>
                    <p class="m-item-group__value" data-qa-id="order-details-shipping-method-value">${fn:escapeXml(orderData.deliveryMode.name)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-terms-label"><spring:theme code="checkout.orderConfirmation.shippingTerms" /></p>
                    <p class="m-item-group__value" data-qa-id="order-details-shipping-terms-value">${fn:escapeXml(orderData.shippingCondition)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-payment-terms-label"><spring:theme code="myaccount.company.payment.terms"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-payment-terms-value">${orderData.paymentTerms}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-incoterms-label"><spring:theme code="text.account.orderHistory.incoTerms"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-incoterms-value">${fn:escapeXml(orderData.incoTerms)}&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</div>

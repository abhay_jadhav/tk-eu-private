package com.thyssenkrupp.b2b.eu.storefrontaddon.tags;

import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;

public class TkEuFunctions extends Functions {

    public static String getCategoryUrl(final CategoryModel category) {
        return getCategoryUrlConverter(getCurrentRequest()).convert(category).getUrl();
    }
}


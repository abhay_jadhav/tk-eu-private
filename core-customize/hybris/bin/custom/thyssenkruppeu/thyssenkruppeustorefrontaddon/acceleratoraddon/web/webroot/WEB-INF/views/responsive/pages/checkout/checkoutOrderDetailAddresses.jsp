<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="m-accordion">
    <div class="m-accordion__header collapsed" data-toggle="collapse" data-target="#order-addresses" data-qa-id="order-details-address-header">
        <h3 class="h5 m-accordion__header__title m-text-color--blue"><spring:theme code="text.order.detail.addresses.heading" text="Addresses" /></h3>
    </div>
    <div id="order-addresses" class="m-accordion__content collapse">
        <div class="row">
            <div class="col-md-6">
                <p class="m-order-address__title" data-qa-id="order-details-shipping-address-label">
                    <spring:theme code="checkout.orderConfirmation.shipping.address.title" text="Shippping address"/>
                </p>
                <div class="m-order-address__block" data-qa-id="order-details-shipping-address-value">
                    <common:address address="${orderData.deliveryAddress}"/>
                </div>
            </div>
            <div class="col-md-6">
                <p class="m-order-address__title" data-qa-id="order-details-billing-address-label">
                    <spring:theme code="checkout.orderConfirmation.billing.address.title" text="Billing address"/>
                </p>
                <div class="m-order-address__block" data-qa-id="order-details-billing-address-value">
                    <common:address address="${orderData.billingAddress}"/>
                </div>
            </div>
        </div>
    </div>
</div>

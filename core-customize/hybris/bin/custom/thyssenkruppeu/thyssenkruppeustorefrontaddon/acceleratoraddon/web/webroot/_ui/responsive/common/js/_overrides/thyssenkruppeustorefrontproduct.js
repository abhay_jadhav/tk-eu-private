/*global $, ACC, window, document, Cookie, setTimeout , localStorage, Promise */

ACC.productDetail.updateQtyValue = function (self, value) {
    var input = $(self).parents(".js-qty-selector").find(".js-qty-selector-input");
    input.val(value);
    $(".qty.js-qty-selector-input").each(function() {
        $(this).val(value)
    });
};

ACC.productDetail.initPageEvents = function () {
    $(document).on("keydown", '.js-qty-selector .js-qty-selector-input', function (e) {

        if (($(this).val() != " " && ((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105))) || e.which == 8 || e.which == 46 || e.which == 37 || e.which == 39 || e.which == 9) {
            // DO NOTHING
        }
        else if (e.which == 38) {
            ACC.productDetail.checkQtySelector(this, "plus");
        }
        else if (e.which == 40) {
            ACC.productDetail.checkQtySelector(this, "minus");
        }
        else {
            e.preventDefault();
        }
    })

    $(document).on("keyup", '.js-qty-selector .js-qty-selector-input', function (e) {
        ACC.productDetail.checkQtySelector(this, "input");
        ACC.productDetail.updateQtyValue(this, $(this).val());
        if (e.which === 13 && $(this).val() !== this.previousQty) {
            if ($(this).val() === "") {
                ACC.productDetail.updateQtyValue(this, 1);
                ACC.thyssenkruppeustorefront.calcCartFromUom();
            }
            else {
                ACC.thyssenkruppeustorefront.calcCartFromUom();
            }
            this.previousQty = $(this).val();
        }
    })

    $(document).on("focusin", '.js-qty-selector .js-qty-selector-input', function () {
        this.previousQty = $("#uomSelectorForm").find("#uomSelectorForm-qty").val();
    });
    $(document).on("focusout", '.js-qty-selector .js-qty-selector-input', function () {
        if ($(this).val() != this.previousQty) {
            ACC.productDetail.checkQtySelector(this, "focusout");
            ACC.productDetail.updateQtyValue(this, $(this).val());
            ACC.thyssenkruppeustorefront.calcCartFromUom()
        }
    });

    $("#Size").change(function () {
        changeOnVariantOptionSelection($("#Size option:selected"));
    });

    $("#variant").change(function () {
        changeOnVariantOptionSelection($("#variant option:selected"));
    });

    $(".selectPriority").change(function () {
        window.location.href = $(this[this.selectedIndex]).val();
    });

    function changeOnVariantOptionSelection(optionSelected) {
        window.location.href = optionSelected.attr('value');
    }

    $(document).on("click", '.js-on-request-availability', function () {
        $("#tkEuContactForm").submit();
    });
}

ACC.product.displayAddToCartPopup = function(cartResult, statusText, xhr, formElement) {
    window.$ajaxCallEvent=true;
    $('#addToCartLayer').remove();
    $('.js-add-to-cart').removeAttr('disabled');

    if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
        ACC.minicart.updateMiniCartDisplay();
    }

    var productCode = $('[name=productCodePost]', formElement).val();
    var quantityField = $('[name=qty]', formElement).val();
    var unit = $('.js-dropdown-uom-button').text().trim();
    var certificate = $('input[name=certOption]:checked').next().text().trim()
    var quantity = 1;
    if (quantityField != undefined) {
        quantity = quantityField;
    }
    if (ACC.minicart.isMiniCartActive) {
        ACC.minicart.displayMiniCartPanel(cartResult.cartHtml, cartResult.cartClosingTime);
        ACC.minicart.setBackdropVisibility(true);
        ACC.thyssenkruppeustorefront.triggerScrollUp();
    }
    else if (!ACC.minicart.isMiniCartActive) {
        ACC.product.displayCartPopup();
    }

    if (cartResult.variants) {
        var selectedVarients = cartResult.variants;
    }
    var cartAnalyticsData = cartResult.cartAnalyticsData;

    var cartData = {
        "cartCode": cartAnalyticsData.cartCode,
        "productCode": productCode, "quantity": quantity,
        "productPrice": cartAnalyticsData.productPostPrice,
        "productName": cartAnalyticsData.productName,
        "productUnit": unit,
        "productCertificate": certificate,
        "variants": selectedVarients

    };
    ACC.track.trackAddToCart(productCode, quantity, cartData);
};

ACC.checkoutsteps.permeateLinks = function() { };

ACC.checkoutsummary.bindAllButtons = function() {
    ACC.checkoutsummary.toggleActionButtons('.js-checkout-place-order');
};

ACC.product.checkAvailibility = function(animation) {
    if (animation === 'showLoadingIcon') {
        $('.js-addtocart-atp-detail').html('<hr class="m-product-detail-panel__separator--top m-product-detail-panel__loading__separator--top"/><div class="m-loading-icon"></div>');
    } else if (animation === 'fadeOutText') {
        $('.js-addtocart-atp-detail').css('opacity', '0.2');
    }
    var params = {
      "productCode" : $("input[name='productCodePost']").val(),
      "unit" : $('.js-hidden-uom-selector-input').val(),
      "qty": $('.js-qty-selector-input').val(),
      "totalWeight": $('.js-total-weight-selector-input').val()
    };
    $.ajax({
        type: "POST",
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        url: "/api/atp/pdp/checkStatus",
        data: JSON.stringify(params),
        success :function(data) {
            if(data.entries[0]) {
                ACC.product.showAvailabilityDialogueModel(data.entries[0].availabilityColor, data.entries[0].availabilityStatus, data.entries[0].availabilityMessage, data.entries[0].availabilityTooltip);
            }
        }
    });
};

ACC.product.showAvailabilityDialogueModel = function (availabilityColor, availabilityStatus, availabilityMessage, availabilityTooltip) {
    $('.js-addtocart-atp-detail').css('opacity', '1');
    var tooltip = '<i class="u-icon-tk u-icon-tk--info js-atp-detail-tooltip" data-toggle="tooltip" data-placement="top" title="' + availabilityTooltip + '" ></i>';
    var icon = '<i class="u-icon-tk u-icon-tk--icon-check js-atp-desc-icon m-product-detail-panel__atp-detail__icon m-text-color--' + availabilityColor + '"></i>';
    var title = '<hr class="m-product-detail-panel__separator--top"/><p data-qa-id="stock-status" class="m-product-detail-panel__atp-detail__status m-text-color--' + availabilityColor + '">' + icon + availabilityStatus + tooltip + '</p>';
    var desc = '<p data-qa-id="stock-status-description" class="m-product-detail-panel__atp-detail__description small">' + availabilityMessage + '</p>';
    $('.js-addtocart-atp-detail').html(title + desc);
    $('.js-atp-detail-tooltip').tooltip();

    if (availabilityColor == "RED") {
        $('.js-atp-desc-icon').removeClass('u-icon-tk--icon-check');
        $('.js-atp-desc-icon').addClass('u-icon-tk--icon-cross');
        $("#onRequestBtn").removeClass("hidden-lg");
        $("#addToCartButton").hide();
    } else{
        $("#onRequestBtn").addClass("hidden-lg");
        $("#addToCartButton").show();
    }
};


ACC.product.showVariantDialogueModel = function(actionUrl, form) {

    var popupDom = $('.js-unselect-variant-dialogue');
    var popupBody = $(popupDom).html();
    var popupTitle = $(popupDom).data('dialogue-title');
    ACC.colorbox.open(popupTitle, {
        html: popupBody,
        className: "js-dialogueModel m-popup m-popup--dialogue",
        width: '488px',
        onComplete: function() {
            $(document).on('click', '#confirmYes', function() {
                Cookie.create('alert-variant-shown', 1, 30);
                ACC.thyssenkruppeustorefront.fetchProductVariantsAjax(actionUrl, form);
                ACC.colorbox.close();
            });

            $(document).on('click', '#confirmNo', function() {
                ACC.colorbox.close();
            });
        }
    });
};

ACC.product.displayCartPopup = function() {
    var title = $('.js-cart-popup-mobile').attr('data-title');
    var popupBody = $('.js-cart-popup-mobile').html();
    ACC.colorbox.open(title, {
        html: popupBody,
        width: '331px',
        close: '<span class="u-icon-tk u-icon-tk--close-single m-text-color--grey-middle"></span>',
        title: '<p class="m-popup__cart__title m-text-color--black">'+title+'</p>',
        className: 'm-popup__cart',
        onComplete: function() {
            ACC.colorbox.resize();
        }
    });
};

ACC.product.bindCartCheckstatus = function() {
    var promiseArr = [];
    $(".js-checkStatus-item").each(function (){
        var params = {
            "entryNumber" : $(this).find(".js-entryNumber").val(),
            "productCode" : $(this).find(".js-productCode").val(),
        };

        var promise = new Promise(function(resolve, reject) {
            $.ajax('/api/atp/cart/checkStatus', {
                type: 'POST',
                data: JSON.stringify(params),
                dataType: 'json',
                contentType: 'application/json',
                success: function(data) {
                    if(data.outOfStockProducts && data.outOfStockProducts !== 'null' && data.outOfStockProducts !== 'undefined'){
                        reject("Out of stock");
                        localStorage.setItem("outOfStockProducts", JSON.stringify(data.outOfStockProducts));
                    }else{
                        resolve("atp succesful");
                    }

                    for (var i = 0; i < data.entries.length; i++) {
                        $('#checkStatus'+data.entries[i].position+' ').text(data.entries[i].availabilityMessage);
                        $('#checkStatus'+data.entries[i].position+' ').addClass('m-text-color--'+data.entries[i].availabilityColor+'');
                        $('#checkStatus'+data.entries[i].position+' ').removeClass('m-loading-icon');
                    }
                }
            });
        });
        promiseArr.push(promise);
    });
    Promise.all(promiseArr).then(function() {
        $.ajax('/api/atp/cart/getConsolidateDate', {
            type: 'POST',
            data:  '',
            dataType: 'json',
            contentType: 'application/json',
            success: function(data) {
                if(data.outOfStockProducts && data.outOfStockProducts !== 'null' && data.outOfStockProducts !== 'undefined'){
                    localStorage.setItem("outOfStockProducts", JSON.stringify(data.outOfStockProducts));
                    setTimeout(function(){ window.location.reload(); }, 3000);
                }
                $('.js-cart-consolidatedMessage').text(data.consolidatedDateLabel);
                $('.js-cart-consolidatedMessage').removeClass('m-loading-icon');
            }
        });
    })
    .catch(function (){
        setTimeout(function(){ window.location.reload(); }, 3000);
    });
};

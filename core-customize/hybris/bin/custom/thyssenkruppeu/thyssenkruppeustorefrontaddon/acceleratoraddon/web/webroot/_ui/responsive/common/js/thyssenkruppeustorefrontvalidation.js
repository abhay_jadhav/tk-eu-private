/*global ACC, document*/

ACC.eupasswordrules = {

    passwordMinScore: 3,
    passwordMinLength: 8,

    passwordValidation: function(password) {

        var passwordLength = new RegExp("(?=.{"+ ACC.eupasswordrules.passwordMinLength +",})");
        if (!passwordLength.test(password)) {
            return false;
        }

        var passwordScore = 0;

        var upperCase = new RegExp(/[A-Z]/);
        if (upperCase.test(password)) {
            passwordScore++;
        }

        var lowerCase = new RegExp(/[a-z]/);
        if (lowerCase.test(password)) {
            passwordScore++;
        }

        var specialChar  = new RegExp(/[°§!"#$%&'()*+,\-./:;<=>?@[\\\]^_`´{|}~]/);
        if (specialChar.test(password)) {
            passwordScore++;
        }

        var number = new RegExp(/[0-9]/);
        if (number.test(password)) {
            passwordScore++;
        }

        return passwordScore >= ACC.eupasswordrules.passwordMinScore;
    }
};

var Cookie = {

    create: function (name, value, days) {

        var expires = "";

         if (days) {
             var date = new Date();
             date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
             expires = "; expires=" + date.toGMTString();
         }

         document.cookie = name + "=" + value + expires + "; path=/";
    },

    read: function (name) {

        var nameEQ = name + "=";
        var ca = document.cookie.split(";");

        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }

        return null;
    },

    erase: function (name) {

        Cookie.create(name, "", -1);
    }

};

package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuCalculationException;
import com.thyssenkrupp.b2b.eu.core.exception.TkEuInvalidCartException;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCartFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.UpdateCertificateForm;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.CartPageController;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.VoucherForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.session.SessionService;


@RequestMapping(value = "/cart")
public class TkEuCartPageController extends CartPageController {

    private static final Logger LOG                     = LoggerFactory.getLogger(TkEuCartPageController.class);
    private static final String REDIRECT_PREFIX         = "redirect:";
    private static final String REDIRECT_CART_URL       = REDIRECT_PREFIX + "/cart";
    private static final String REDIRECT_QUOTE_EDIT_URL = REDIRECT_PREFIX + "/quote/%s/edit/";
    private static final String CART_CMS_PAGE_LABEL = "cart";
    private static final String VOUCHER_FORM = "voucherForm";

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    @Resource(name = "siteBaseUrlResolutionService")
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Resource(name = "redirectStrategy")
    private RedirectStrategy redirectStrategy;

    @Resource(name = "cartFacade")
    private TkEuCartFacade cartFacade;
    
    @Resource(name = "voucherFacade")
    private VoucherFacade voucherFacade;

    private String defaultLoginUri;

    private SessionService sessionService;

    @RequestMapping(value = "/updateCertificate", method = RequestMethod.POST)
    @RequireHardLogIn
    public String updateCertificate(final Model model, @ModelAttribute("updateCertificateForm") final UpdateCertificateForm updateCertificateForm, final BindingResult bindingResult, final HttpServletRequest request,
        final RedirectAttributes redirectModel) {
        if (!cartFacade.hasEntries()) {
            LOG.info("Missing or empty cart");
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.checkout.empty.cart", null);
            return REDIRECT_CART_URL;
        }
        CommerceCartModification modifiedCartEntry = cartFacade.updateCertificateInCartEntry(updateCertificateForm.getEntryNumber(), updateCertificateForm.getCertificateCode());
        if(modifiedCartEntry.getStatusCode().equals("success")){
            List<AbstractOrderEntryProductInfoModel> productInfos = modifiedCartEntry.getEntry().getProductInfos();
            for (AbstractOrderEntryProductInfoModel productInfo : productInfos) {
                if(productInfo instanceof CertificateConfiguredProductInfoModel && ((CertificateConfiguredProductInfoModel)productInfo).isChecked()){
                    redirectModel.addFlashAttribute("configurationLabel", ((CertificateConfiguredProductInfoModel) productInfo).getName());
                    model.addAttribute("configurationLabel", ((CertificateConfiguredProductInfoModel)productInfo).getName());
                }
            }
        }
        return getCartPageRedirectUrl();
    }

    @RequestMapping(value = "/updateCertificateByAjax", method = RequestMethod.POST)
    @RequireHardLogIn
    public String updateCertificateByAjax(final Model model, @ModelAttribute("updateCertificateForm") final UpdateCertificateForm updateCertificateForm, final BindingResult bindingResult, final HttpServletRequest request,
        final RedirectAttributes redirectModel) {
        if (!cartFacade.hasEntries()) {
            LOG.info("Missing or empty cart");
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.checkout.empty.cart", null);
            return REDIRECT_CART_URL;
        }
        cartFacade.updateCertificateInCartEntry(updateCertificateForm.getEntryNumber(), updateCertificateForm.getCertificateCode());
        fillVouchers(model);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_CART_ITEMS_AND_SUMMARY;
    }

    protected void fillVouchers(final Model model) {
        try {
            model.addAttribute("appliedVouchers", voucherFacade.getVouchersForCart());
        } catch (Exception e) {
            LOG.error("Exception occur while adding vouchersForCart in model"+e);
        }
        if (!model.containsAttribute(VOUCHER_FORM)) {
            model.addAttribute(VOUCHER_FORM, new VoucherForm());
        }
        model.addAttribute("disableUpdate", Boolean.TRUE);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String showCart(final Model model) throws CMSItemNotFoundException {
        final ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if (userFacade.isAnonymousUser() && StringUtils.isBlank(sessionService.getAttribute(CMSFilter.PREVIEW_TICKET_ID_PARAM))) {
            redirectToLoginPage();
        }
        model.addAttribute("updateCertificateForm", new UpdateCertificateForm());
        return super.showCart(model);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model, @Valid final UpdateQuantityForm form, final BindingResult bindingResult,
      final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            for (final ObjectError error : bindingResult.getAllErrors()) {
                setCartPageErrorMessage(model, error);
            }
        } else if (getCartFacade().hasEntries()) {
            try {
                final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber, form.getQuantity().longValue());
                addFlashMessage(form, request, redirectModel, cartModification);
                return getCartPageRedirectUrl();
            } catch (final CommerceCartModificationException ex) {
                LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
            } catch (final TkEuInvalidCartException ex) {
                Throwable exception = ex.getCause();
                if (exception instanceof TkEuCalculationException) {
                    TkEuCalculationException tkEuCalculationException = (TkEuCalculationException) exception;
                    LOG.error("Could not update cart due to invalid cart entries {}", tkEuCalculationException.getInvalidItem());
                }
                GlobalMessages.addErrorMessage(model, "checkout.order.product.price.null");
            }
        }
        return prepareCartUrl(model);
    }

    private void setCartPageErrorMessage(Model model, ObjectError error) {
        if ("typeMismatch".equals(error.getCode())) {
            GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
        } else {
            GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
        }
    }

    protected String prepareCartUrl(final Model model) throws CMSItemNotFoundException {
        final Optional<String> quoteEditUrl = getQuoteUrl();
        if (quoteEditUrl.isPresent()) {
            return quoteEditUrl.get();
        } else {
            prepareDataForPage(model);

            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_CART_PAGE;
        }
    }

    protected String getCartPageRedirectUrl() {
        final QuoteData quoteData = getCartFacade().getSessionCart().getQuoteData();
        return quoteData != null ? String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteData.getCode())) : REDIRECT_CART_URL;
    }

    private void redirectToLoginPage() {
        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            final HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
            setReferrer(request);
            redirect(request, response, getRedirectUrl(defaultLoginUri, true));
        }
    }

    private void setReferrer(final HttpServletRequest request) {
        request.getSession().setAttribute("REDIRECT_REFERER", request.getRequestURL().toString());
    }

    protected String getRedirectUrl(final String mapping, final boolean secured) {
        return siteBaseUrlResolutionService.getWebsiteUrlForSite(cmsSiteService.getCurrentSite(), secured, mapping);
    }

    protected void redirect(final HttpServletRequest request, final HttpServletResponse response, final String targetUrl) {
        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Redirecting to url '%s'.", targetUrl));
            }
            redirectStrategy.sendRedirect(request, response, targetUrl);
        } catch (final IOException ex) {
            LOG.error("Unable to redirect.", ex);
        }
    }

    public String getDefaultLoginUri() {
        return defaultLoginUri;
    }

    public void setDefaultLoginUri(String defaultLoginUri) {
        this.defaultLoginUri = defaultLoginUri;
    }

    public RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }
    
    
    @Override
    protected void createProductList(final Model model) throws CMSItemNotFoundException {
        boolean isCartUpdated=cartFacade.removeInactiveProductsFromCart();
        if(isCartUpdated) {
            GlobalMessages.addErrorMessage(model, "basket.page.message.remove");
        }
        final CartData cartData = cartFacade.getSessionCartWithEntryOrdering(false);
        createProductEntryList(model, cartData);

        storeCmsPageInModel(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
    }

}

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${url}" var="browseCatalogUrl"/>

<span class="navigation-button">
    <button class="btn btn-primary js-continue-shopping-button" data-continue-shopping-url="${browseCatalogUrl}">
        ${browseCatalogueButtonLabel}
    </button>
</span>


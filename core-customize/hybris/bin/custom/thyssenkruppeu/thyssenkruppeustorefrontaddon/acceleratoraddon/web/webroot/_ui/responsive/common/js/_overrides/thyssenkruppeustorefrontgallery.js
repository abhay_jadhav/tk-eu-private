/*global $, ACC */

ACC.carousel.autoHeight = function ($el) {
    $el.find('.js-equal-height').syncHeight({ 'updateOnResize': true});
}

ACC.carousel.setScrollIconTop = function ($el) {
    var imgHeight = 17 + ($('.js-product-carousel-img').height() / 2);
    $('.js-owl-multi-products-lg').find('.owl-next,.owl-prev').attr('style','top: '+imgHeight+'px !important');
    ACC.carousel.autoHeight($el);
}

ACC.carousel.updateScollIcons = function () {
    if (this.options.items >= this.$owlItems.length) {
        this.owlControls.show();
        this.owlControls.find('.u-icon-tk').attr('disabled', true);
    }
}

ACC.carousel.carouselConfig = {
    "default": {
        navigation: true,
        navigationText: ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
        pagination: false,
        itemsCustom: [[0, 2], [640, 4], [1024, 5], [1400, 7]],
    },
    "rotating-image": {
        navigation: false,
        pagination: true,
        singleItem: true,
    },
    "lazy-reference": {
        navigation: true,
        navigationText:  ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
        pagination: false,
        itemsDesktop: [5000,7],
        itemsDesktopSmall: [1200,5],
        itemsTablet: [768,4],
        itemsMobile: [480,3],
        lazyLoad: true,
        autoHeight: true,
    },
    "multi-products": {
        navigation: true,
        navigationText:  ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
        pagination: false,
        itemsDesktop: [5000,4],
        itemsDesktopSmall: [1200,4],
        itemsTablet: [971,3],
        itemsMobile: [645,2],
        lazyLoad: true,
        rewindNav: false,
        afterAction: function() {
            var leftArrows = $('.js-owl-multi-products .owl-buttons .owl-prev'),
            rightArrows = $('.js-owl-multi-products .owl-buttons .owl-next');
            if(leftArrows.hasClass('disabled')) {
                leftArrows.find('.u-icon-tk').addClass('disabled');
            } else {
                leftArrows.find('.u-icon-tk').removeClass('disabled');
            }

            if(rightArrows.hasClass('disabled')) {
                rightArrows.find('.u-icon-tk').addClass('disabled');
            } else {
                rightArrows.find('.u-icon-tk').removeClass('disabled');
            }
        },
        afterLazyLoad: ACC.carousel.autoHeight
    },
    "multi-products-lg": {
        navigation: true,
        navigationText:  ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
        pagination: false,
        itemsDesktop: [5000,6],
        itemsDesktopSmall: [1140,5],
        itemsTablet: [971,3],
        itemsMobile: [645,2],
        lazyLoad: true,
        rewindNav: false,
        afterAction: function() {
            var leftArrows = $('.js-owl-multi-products-lg .owl-buttons .owl-prev'),
            rightArrows = $('.js-owl-multi-products-lg .owl-buttons .owl-next');
            if(leftArrows.hasClass('disabled')) {
                leftArrows.find('.u-icon-tk').addClass('disabled');
            } else {
                leftArrows.find('.u-icon-tk').removeClass('disabled');
            }

            if(rightArrows.hasClass('disabled')) {
                rightArrows.find('.u-icon-tk').addClass('disabled');
            } else {
                rightArrows.find('.u-icon-tk').removeClass('disabled');
            }
        },
        afterLazyLoad: ACC.carousel.setScrollIconTop,
        afterInit: ACC.carousel.updateScollIcons
    }

}


ACC.imagegallery.bindImageGallery = function () {

    $(".js-gallery").each(function(){
        var navigationFlag = false;
        var $image = $(this).find(".js-gallery-image");
        var $carousel = $(this).find(".js-gallery-carousel");

        var slideCount = $($image).find('.item').length;
        if (slideCount > 3) {
            navigationFlag = true
        }

        $image.owlCarousel({
            singleItem: true,
            pagination: true,
            navigation: false,
            lazyLoad: true,
            navigationText: ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
            afterAction: function(){
                ACC.imagegallery.syncPosition($image,$carousel,this.currentItem)
                $image.data("zoomEnable",true)
            },
            startDragging: function(){

                $image.data("zoomEnable",false)
            },
            afterLazyLoad:function(){

                var b = $image.data("owlCarousel") || {}
                if(!b.currentItem){
                    b.currentItem = 0
                }

                var $e=$($image.find("img.lazyOwl")[b.currentItem]);
                startZoom($e.parent())
            }
        });

        var productCount = $($carousel).find('.item').length;
        $carousel.owlCarousel({
            navigation: navigationFlag,
            navigationText: ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
            pagination: false,
            margin: 18,
            items: 3,
            itemsDesktop: [5000,3],
            itemsDesktopSmall: [1200,3],
            itemsTablet: [768,3],
            itemsMobile: [480,3],
            lazyLoad: true,
            afterAction: function(){
                var b = $carousel.data("owlCarousel") || {}
                if (!b.currentItem) {
                    b.currentItem = 0;
                }

                if(productCount > 2) {
                    var prev = $('.js-gallery-carousel .owl-prev');
                    var next = $('.js-gallery-carousel .owl-next');

                    if(b.currentItem == 0){
                        $(prev).hide();
                    } else {
                        $(prev).show();
                    }

                    if(b.currentItem == (productCount - 3)){
                        $(next).hide();
                    } else {
                        $(next).show();
                    }
                }
            },
        });

        ACC.imagegallery.highlightCarouselItem(0, $carousel);

        $carousel.on("click","a.item",function(e){
            e.preventDefault();
            $(".owl-item").each(function(){
                var $this = $(this);
                $this.css({"border": "none"})
            });
            $(this).css({"border": "1px solid #00a0f0"});
            $image.trigger("owl.goTo",$(this).parent(".owl-item").data("owlItem"));
        })


        function startZoom(e){



            $(e).zoom({
                url: $(e).find("img.lazyOwl").data("zoomImage"),
                touch: true,
                on: "grab",
                touchduration: 300,

                onZoomIn:function(){

                },

                onZoomOut:function(){
                    var owl = $image.data('owlCarousel');
                    owl.dragging(true)
                    $image.data("zoomEnable",true)
                },

                zoomEnableCallBack:function(){
                    var bool = $image.data("zoomEnable")

                    var owl = $image.data('owlCarousel');
                    if(bool==false){
                        owl.dragging(true)
                    } else {
                        owl.dragging(false)
                    }
                    return bool;
                }
            });
        }
    });
};


ACC.imagegallery.syncPosition = function($image,$carousel,currentItem){
    $carousel.trigger("owl.goTo",currentItem);
    $carousel.find('.owl-item .item').css('border', 'none');
    ACC.imagegallery.highlightCarouselItem(currentItem, $carousel);
};

ACC.imagegallery.highlightCarouselItem = function($owl_item, $carousel){
    $carousel.find('.owl-item:eq('+ $owl_item +') .item').css({"border": "1px solid #00a0f0"});
};

/* global document, window */
$(document).ready(function() {

    $(".banner-slider").each(function (){
        if($(this).attr("data-slider-type") == "HERO_BANNER_MAX") $(this).addClass("max-slider");

        $(this).owlCarousel({
            navigation : true, // Show next and prev buttons
            navigationText: ["<span class='u-icon-tk u-icon-tk--arrow-left'></span>", "<span class='u-icon-tk u-icon-tk--arrow-right'></span>"],
            singleItem:true,
            pagination:true,
            autoHeight:true,
            slideSpeed: 500,
            autoPlay: $(this).attr("data-carousel-timer")*1000,
            stopOnHover: true,
            afterAction:function(){
                $(".m-banner").each(function(){
                    if($(window).width() < 646){
                        if($(this).hasClass("m-banner--hero-banner-content-max") || $(this).hasClass("m-banner--hero-banner-content-normal")){
                            $(this).closest(".banner-slider").find(".owl-controls").css("position", "absolute");
                            $(this).closest(".banner-slider").find(".owl-controls").css("top", $(this).find(".js-responsive-image").height() + 10);
                        }
                    }
                });
            },
        });

    });
    
    $(".teaser-container").each(function (){

        if(parseInt($(this).attr("data-count")) == 2){
            $(this).find(".m-column").addClass("col-sm-6");
        }
        else if (parseInt($(this).attr("data-count")) == 3){
            $(this).addClass("teaser-more-than-two");
            $(this).find(".m-column").addClass("col-md-4 col-sm-6");
        }
        else if (parseInt($(this).attr("data-count")) >= 4){
            $(this).addClass("teaser-more-than-two");
            $(this).find(".m-column").addClass("col-md-3 col-sm-6");
        }
        else {
            $(this).find(".m-column").addClass("col-sm-12");
        }
    });
    
    $(".js-cart-voucher__label").click(function(){
        if($(".js-voucher-form").is(":visible")){
            $(".js-voucher-form").slideUp("slow");
            $(this).find(".pali-u-icon-tk").toggleClass("pali-u-icon-tk--arrow-up pali-u-icon-tk--arrow-down");
        }
        else{
            $(".js-voucher-form").slideDown("slow");
            $(this).find(".pali-u-icon-tk").toggleClass("pali-u-icon-tk--arrow-down pali-u-icon-tk--arrow-up");
        }
    });

    $(".js-voucher-form").each(function(){
        if(!$(".js-voucher-code").val()){
            $(".js-voucher-form").slideUp("slow");
            $(".js-voucher-form").find(".pali-u-icon-tk").addClass("pali-u-icon-tk--arrow-down").removeClass("pali-u-icon-tk--arrow-up");
        }else{
            $(".js-voucher-form").slideDown("slow");
            $(".js-voucher-form").find(".pali-u-icon-tk").addClass("pali-u-icon-tk--arrow-up").removeClass("pali-u-icon-tk--arrow-down");
        }
    });

});


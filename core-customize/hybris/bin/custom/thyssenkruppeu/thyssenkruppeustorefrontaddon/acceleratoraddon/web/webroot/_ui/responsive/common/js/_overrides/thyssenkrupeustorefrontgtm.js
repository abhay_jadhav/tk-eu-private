/*global window, ACC, dataLayer, products*/
ACC.gtm.fnBindCartOperations = function () {

    if (window.mediator !== undefined) {
    window.mediator.subscribe('trackAddToCart', function (data) {
        for (var i = 0; i < products.length; i++) {
            if (data.productCode === products[i].id) {
                products[i].dimension1 = data.cartData.productCertificate;
                if ( data.cartData.variants.dimension2 ) {
                    products[i].dimension2 = data.cartData.variants.dimension2;
                }
                else if (data.cartData.variants.dimension3 ) {
                    products[i].dimension3 = data.cartData.variants.dimension3;
                }
                products[i].quantity = parseInt(data.quantity);
                dataLayer.push( {
                    'event': 'eec.add',
                    'ecommerce': {
                        'add': {
                            actionField: {
                                list: 'Product Page'
                            },
                            'products': [ products[i] ],
                        }
                    }
                });
            }
        }
    });

    window.mediator.subscribe('trackRemoveFromCart', function (data) {
        ACC.gtm.fnRemoveCartItemGtmEvent(data);
    } );

    window.mediator.subscribe('trackUpdateCart', function (data) {
        if (data.newCartQuantity == 0) {
            ACC.gtm.fnRemoveCartItemGtmEvent(data);
        }
    });
}
};

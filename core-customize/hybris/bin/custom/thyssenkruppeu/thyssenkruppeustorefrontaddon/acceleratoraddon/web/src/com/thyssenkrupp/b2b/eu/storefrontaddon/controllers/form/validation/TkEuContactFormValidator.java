package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkEuContactForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;

@Component("tkEuContactFormValidator")
public class TkEuContactFormValidator implements Validator {
    private static final int MAX_FIELD_LENGTH    = 255;
    private static final int MAX_MESSAGE_LENGTH  = 5000;

    @Resource(name = "textAreaPatternValidator")
    private DefaultTextAreaValidator textAreaPatternValidator;

    @Override
    public boolean supports(final Class<?> aClass) {
        return TkEuContactForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        final TkEuContactForm tkEuContactForm = (TkEuContactForm) object;
        validateTkEuContactFields(tkEuContactForm, errors);
    }

    protected void validateTkEuContactFields(final TkEuContactForm tkEuContactForm, final Errors errors) {

        validateStringField(tkEuContactForm.getFirstname(), ContactField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
        validateStringField(tkEuContactForm.getLastname(), ContactField.LASTNAME, MAX_FIELD_LENGTH, errors);
        validateStringField(tkEuContactForm.getEmail(), ContactField.EMAIL, MAX_FIELD_LENGTH, errors);
        validateStringField(tkEuContactForm.getTopic(), ContactField.TOPIC, MAX_FIELD_LENGTH, errors);
        validateMessageField(tkEuContactForm.getMessage(), ContactField.MESSAGE, MAX_MESSAGE_LENGTH, errors);
        validateCompanyField(tkEuContactForm.getCompany(), ContactField.COMPANY, errors);
    }

    private void validateCompanyField(String company, ContactField fieldType, Errors errors) {
        if(StringUtils.isNotBlank(company) && !textAreaPatternValidator.validateTextAreaPattern(company)){
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }

    protected void validateStringField(final String contactField, final ContactField fieldType,
        final int maxFieldLength, final Errors errors) {
        if (contactField == null || StringUtils.isEmpty(contactField) || (StringUtils.length(contactField) > maxFieldLength)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        } else if (!textAreaPatternValidator.validateTextAreaPattern(contactField)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }

    protected void validateMessageField(final String contactField, final ContactField fieldType,
      final int maxFieldLength, final Errors errors) {
        if (StringUtils.isEmpty(contactField) || (StringUtils.length(contactField) > maxFieldLength)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }

    protected enum ContactField {
        TITLE("title", "contact.title.invalid"), FIRSTNAME("firstname", "contact.firstname.invalid"), LASTNAME("lastname", "contact.lastname.invalid"),
        EMAIL("email", "contact.email.invalid"), TOPIC("topic", "contact.topic.invalid"), MESSAGE("message", "contact.message.invalid"), COMPANY("company", "contact.company.invalid");
        private String fieldKey;
        private String errorKey;

        ContactField(final String fieldKey, final String errorKey) {
            this.fieldKey = fieldKey;
            this.errorKey = errorKey;
        }

        public String getFieldKey() {
            return fieldKey;
        }

        public String getErrorKey() {
            return errorKey;
        }
    }
}

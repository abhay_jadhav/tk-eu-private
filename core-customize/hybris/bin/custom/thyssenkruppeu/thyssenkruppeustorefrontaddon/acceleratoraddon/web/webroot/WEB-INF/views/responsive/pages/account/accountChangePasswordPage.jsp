<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="b2b-common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>

<spring:theme code="profile.currentPassword" text="Choose your password" var="currentPasswordPlaceholder"/>
<spring:theme code="profile.newPassword" text="New password" var="newPasswordPlaceholder"/>
<spring:theme code="profile.checkNewPassword" text="Confirm your password" var="confirmNewPasswordPlaceholder"/>

<form:form role="form" action="profile" method="post" commandName="updatePasswordForm" id="updatePasswordForm" class="m-change-password__form js-update-password-validation-form">
    <tkEuformElement:formPasswordBox idKey="currentPassword" labelKey="${currentPasswordPlaceholder}" path="currentPassword" inputCSS="form-control" mandatory="true" labelQaAttribute="myaccount-profile-login-current-password-label" qaAttribute="myaccount-profile-login-current-password-value" placeholder="${currentPasswordPlaceholder}"/>
    <tkEuformElement:formPasswordBox idKey="newPassword" labelKey="${newPasswordPlaceholder}" path="newPassword" inputCSS="form-control js-password-rule-check" mandatory="true" labelQaAttribute="myaccount-profile-login-new-password-label" qaAttribute="myaccount-profile-login-new-password-value" placeholder="${newPasswordPlaceholder}"/>
    <tkEuformElement:formPasswordBox idKey="confirmNewPassword" labelKey="${confirmNewPasswordPlaceholder}" path="checkNewPassword" labelQaAttribute="myaccount-profile-login-confirm-password-label" inputCSS="form-control js-confirm-password-rule-check" mandatory="true" qaAttribute="myaccount-profile-login-confirm-password-value" placeholder="${confirmNewPasswordPlaceholder}"/>
    <div class="m-change-password__form__actions form-group">
        <a class="m-change-password__form__actions__cancel" href="${request.contextPath}${currentUrl}" data-qa-id="myaccount-profile-login-cancel-btn"><spring:theme code="text.account.profile.cancel" text="Cancel"/></a>
        <button type="submit" data-qa-id="myaccount-profile-login-set-password-btn" class="btn btn-default btn-sm m-change-password__form__actions__submit pull-right"><spring:theme code="text.account.profile.updatePasswordForm" text="Set password"/></button>
    </div>
    <div class="hidden js-validation-message">
        <b2b-common:passwordSuggestions />
    </div>
</form:form>

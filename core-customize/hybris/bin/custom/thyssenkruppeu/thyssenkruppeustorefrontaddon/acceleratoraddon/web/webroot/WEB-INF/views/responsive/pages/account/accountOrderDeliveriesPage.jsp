<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<c:set var="searchResultCount" value="${searchCount}"/>
<c:url value="/my-account/my-deliveries/search" var="searchActionUrl"/>
<c:set var="searchUrl" value="/my-account/my-deliveries/search?init=true&searchKey=${searchKey}&sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
<c:url var="resetUrl" value="/my-account/my-deliveries?init=true"/>
<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:theme code="myaccount.deliveries.search.startover" var="resetUrlLabel"/>
<spring:theme code="myaccount.deliveries.search.placeholder" var="searchPlaceholder"/>
<spring:theme code="myaccount.deliveries.button.search" var="searchBtnText"/>
<spring:theme code="myaccount.deliveries.button.reset" var="resetBtnText"/>
<b2b-order:accountOrderSearchSection formID="orderDeliveriesSearchForm" searchActionUrl="${searchActionUrl}"
                               searchResultCount="${searchResultCount}" searchKey="${searchKey}"
                               searchPlaceholder="${searchPlaceholder}" searchBtnText="${searchBtnText}" resetUrlLabel="${resetUrlLabel}"
                               resetBtnText="${resetBtnText}" resetUrl="${resetUrl}" qaAttributeTitle="${'order-deliveries-title'}" qaAttributeSearchBtn="${'myaccount-searchbox'}" qaAttributeTextbox="${'order-deliveries-search-textbox'}"
                               qaAttributeResetBtn="${'myaccount-deliveries-reset-button'}" currentPage="${'deliveries'}">
                               </b2b-order:accountOrderSearchSection>
<c:if test="${searchResultCount ne 0 or not empty searchKey}">
    <b2b-order:orderDeliveriesListing searchUrl="${searchUrl}" messageKey="text.account.orderHistory.page"></b2b-order:orderDeliveriesListing>
</c:if>

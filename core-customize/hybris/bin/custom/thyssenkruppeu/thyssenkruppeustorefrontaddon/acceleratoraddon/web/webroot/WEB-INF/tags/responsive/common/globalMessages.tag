<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<c:if test="${(not empty accConfMsgs) || (not empty accInfoMsgs) || (not empty accErrorMsgs)}">
    <div class="m-global-alerts js-alert-msg">
        <%-- Information (confirmation) messages --%>
        <c:if test="${not empty accConfMsgs}">
            <c:forEach items="${accConfMsgs}" var="msg">
                <div class="alert alert-info" data-qa-id="info-message">
                    <div class="container">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button"><span class="u-icon-tk u-icon-tk--close-single"></span></button>
                        <spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
                    </div>
                </div>
            </c:forEach>
        </c:if>
        <%-- Warning messages --%>
        <c:if test="${not empty accInfoMsgs}">
            <c:forEach items="${accInfoMsgs}" var="msg">
                <div class="alert alert-warning" data-qa-id="warning-message">
                    <div class="container">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button"><span class="u-icon-tk u-icon-tk--close-single"></span></button>
                        <spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
                    </div>
                </div>
            </c:forEach>
        </c:if>
        <%-- Error messages (includes spring validation messages)--%>
        <c:if test="${not empty accErrorMsgs}">
            <c:forEach items="${accErrorMsgs}" var="msg">
                <div class="alert alert-danger" data-qa-id="error-message">
                    <div class="container">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button"><span class="u-icon-tk u-icon-tk--close-single"></span></button>
                        <spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
                    </div>
                </div>
            </c:forEach>
        </c:if>
    </div>
</c:if>

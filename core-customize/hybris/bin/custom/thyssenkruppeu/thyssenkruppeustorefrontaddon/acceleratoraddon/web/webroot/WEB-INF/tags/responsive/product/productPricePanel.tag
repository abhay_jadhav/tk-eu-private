<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="isOrderForm" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="volumeUom" value=""/>
<c:if test="${not empty product.volumePriceRangeUnit}">
    <c:set var="volumeUom" value="${product.volumePriceRangeUnit}"/>
</c:if>
<c:if test="${product.purchasable}">
    <c:choose>
        <c:when test="${empty product.volumePrices}">
            <c:choose>
                <c:when test="${(not empty product.priceRange) and (product.priceRange.minPrice.value ne product.priceRange.maxPrice.value) and ((empty product.baseProduct) or (not empty isOrderForm and isOrderForm))}">
                <span>
                    <format:price priceData="${product.priceRange.minPrice}"/>
                </span>
                    <span>-</span>
                    <span>
                    <format:price priceData="${product.priceRange.maxPrice}"/>
                </span>
                    <c:out value=" ${volumeUom}"/>
                </c:when>
                <c:otherwise>
                    <div class="m-volume-prices">
                        <div class="m-volume-prices__heading">
                            <div class="row">
                                <div class="col-xs-4 text-left">
                                <span class="m-volume-prices__heading__price-label">
                                    <spring:theme code="basket.page.price"/>
                                </span>
                                </div>
                                <div class="col-xs-8 text-right">
                              <%--   <span class="m-volume-prices__heading__price-value" data-qa-id="pdp-main-price">
                                    <tk-eu-format:fromPrice priceData="${product.price}" withUnit="${true}"/>
                                </span> --%>
                                <tk-eu-product:productBasePricePanel/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <div class="m-volume-prices">
                <div class="m-volume-prices__heading">
                    <div class="row">
                        <div class="col-xs-4 text-left">
                        <span class="m-volume-prices__heading__price-label">
                            <spring:theme code="product.detail.sidebar.price"/>
                        </span>
                        </div>
                        <div class="col-xs-8 text-right">
                            <tk-eu-product:productBasePricePanel/>
                        </div>
                    </div>
                </div>
                <a href="#m-volume-prices-list" data-toggle="collapse" class="small m-volume-prices__list__toggle collapsed js-price-volume-toggle" data-qa-id="pdp-variant-discounts-lnk">
                    <spring:theme code="basket.volume.discount"/><i class="u-icon-tk"></i>
                </a>
                <div id="m-volume-prices-list" class="m-volume-prices__list collapse">
                    <c:forEach var="volPrice" items="${product.volumePrices}">
                        <div class="row">
                            <div class="col-xs-6">
                                <span class="m-volume-prices__price-item-label" data-qa-id="price-sec-variant-name">
                                    <c:choose>
                                        <c:when test="${empty volPrice.maxQuantity}">
                                            ${volPrice.minQuantity}+
                                        </c:when>
                                        <c:otherwise>
                                            ${volPrice.minQuantity}-${volPrice.maxQuantity}
                                        </c:otherwise>
                                    </c:choose>
                                    <span><c:out value=" ${volumeUom}" escapeXml="false" /></span>
                                </span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <span class="m-volume-prices__price-item-value" data-qa-id="price-sec-variant-price">
                                    <tk-eu-format:price priceData="${volPrice}" withUnit="${true}"/>
                                </span>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>

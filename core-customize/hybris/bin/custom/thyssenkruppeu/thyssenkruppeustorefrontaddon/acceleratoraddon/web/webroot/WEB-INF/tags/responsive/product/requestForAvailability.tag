<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="salesUom" value="${not empty cartEntryData? cartEntryData.unit : selectedUom}"/>
<c:set var="selectedQty" value="${not empty cartEntryData? cartEntryData.quantity : selectedQty}"/>
<c:url value="/productAvailabilityRequest" var="contactUrl"/>
    <form:form method="post" id="tkEuContactForm" action="${contactUrl}" class="js-addtocart-request-availabilty">
        <input type="hidden" name="productCode" value="${fn:escapeXml(product.code)}"/>
        <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="${not empty selectedQty ? selectedQty : 1}">
        <input type="hidden" name="certOption" class="certOption js-hidden-certOption-selector-input" id="tkEuContactForm-certOption" value="none" />
        <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
        <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
        <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
        <input type="hidden" name="salesUom" class="salesUom js-hidden-uom-selector-input" id="tkEuContactForm-salesUom" value="${salesUom}"/>
        <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${selectedTradelength}" />
        <button type="submit" data-qa-id="pdp-requestavailable-button" class="btn btn-primary btn-block js-add-to-cart out-of-stock"><spring:theme code="product.stock.availability.requestAvailability"/></button>
    </form:form>


package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkMiniCartComponentModel;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;

@Controller("TkMiniCartComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_TKMINICARTCOMPONENT)
public class TkMiniCartComponentController extends AbstractCMSComponentController<TkMiniCartComponentModel> {
    public static final String TOTAL_PRICE       = "totalPrice";
    public static final String TOTAL_ITEMS       = "totalItems";
    public static final String TOTAL_DISPLAY     = "totalDisplay";
    public static final String TOTAL_NO_DELIVERY = "totalNoDelivery";
    public static final String SUB_TOTAL         = "subTotal";

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final TkMiniCartComponentModel component) {
        final CartData cartData = cartFacade.getMiniCart();
        model.addAttribute(SUB_TOTAL, cartData.getSubTotal());
        if (cartData.getDeliveryCost() != null) {
            final PriceData withoutDelivery = cartData.getDeliveryCost();
            withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
            model.addAttribute(TOTAL_NO_DELIVERY, withoutDelivery);
        } else {
            model.addAttribute(TOTAL_NO_DELIVERY, cartData.getTotalPrice());
        }
        model.addAttribute(TOTAL_PRICE, cartData.getTotalPrice());
        model.addAttribute(TOTAL_DISPLAY, component.getTotalDisplay());
        model.addAttribute(TOTAL_ITEMS, cartData.getTotalItems());
    }

    @Override
    protected String getView(TkMiniCartComponentModel component) {

        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX + StringUtils.lowerCase(TkMiniCartComponentModel._TYPECODE);
    }
}


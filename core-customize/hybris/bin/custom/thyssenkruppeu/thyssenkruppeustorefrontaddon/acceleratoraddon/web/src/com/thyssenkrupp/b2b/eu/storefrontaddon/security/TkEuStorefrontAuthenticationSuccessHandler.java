package com.thyssenkrupp.b2b.eu.storefrontaddon.security;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.StorefrontAuthenticationSuccessHandler;
import de.hybris.platform.core.Constants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;
import com.thyssenkrupp.b2b.eu.core.service.TkEuLoginDetailsService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class TkEuStorefrontAuthenticationSuccessHandler extends StorefrontAuthenticationSuccessHandler {

    private static final Logger LOG                = Logger.getLogger(TkEuStorefrontAuthenticationSuccessHandler.class);
    private static final String CHECKOUT_URL       = "/checkout";
    private static final String CART_URL           = "/cart";
    private static final String REDIRECT_ATTRIBUTE = "REDIRECT_REFERER";
    private TkEuLoginDetailsService tkEuLoginDetailsService;
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
                                        final Authentication authentication) throws IOException, ServletException{
        // Check if the user is in role admingroup
        if (!isAdminAuthority(authentication)) {
            super.onAuthenticationSuccess(request, response, authentication);
            tkEuLoginDetailsService.setLoginDetails("",LoginType.CUSTOMER);
        } else {
            LOG.warn("Invalidating session for user in the " + Constants.USER.ADMIN_USERGROUP + " group");
            invalidateSession(request, response);
        }
    }

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {

        String targetUrl = hasRedirectUrlInSession(request) && !isAlwaysUseDefaultTargetUrl() ? getRedirectUrlFromSession(request) : super.determineTargetUrl(request, response);
        targetUrl = hasRestrictedPage(targetUrl);

        if (StringUtils.equals(targetUrl, CHECKOUT_URL) && BooleanUtils.toBoolean((Boolean) request.getAttribute(WebConstants.CART_MERGED))) {
            targetUrl = CART_URL;
        }

        LOG.debug("The redirect url is :" + targetUrl);
        return targetUrl;
    }

    private String hasRestrictedPage(final String targetUrl) {
        if (CollectionUtils.isNotEmpty(getRestrictedPages())) {
            for (final String restrictedPage : getRestrictedPages()) {
                if (targetUrl.contains(restrictedPage)) {
                    return super.getDefaultTargetUrl();
                }
            }
        }
        return targetUrl;
    }

    private boolean hasRedirectUrlInSession(HttpServletRequest request) {
        return (request.getSession(false) != null && request.getSession(false).getAttribute(REDIRECT_ATTRIBUTE) != null);
    }

    private String getRedirectUrlFromSession(HttpServletRequest request) {
        String redirectUrl = "";
        HttpSession session = request.getSession(false);
        if (session != null) {
            redirectUrl = (String) session.getAttribute(REDIRECT_ATTRIBUTE);
            clearRedirectUrlFromSession(session);
        }
        return redirectUrl;
    }

    private void clearRedirectUrlFromSession(HttpSession session) {
        session.removeAttribute(REDIRECT_ATTRIBUTE);
    }

    /**
     * @return the tkEuLoginDetailsService
     */
    public TkEuLoginDetailsService getTkEuLoginDetailsService() {
        return tkEuLoginDetailsService;
    }

    /**
     * @param tkEuLoginDetailsService the tkEuLoginDetailsService to set
     */
    public void setTkEuLoginDetailsService(TkEuLoginDetailsService tkEuLoginDetailsService) {
        this.tkEuLoginDetailsService = tkEuLoginDetailsService;
    }

}

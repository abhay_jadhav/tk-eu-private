<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<ul class="m-list-group list-group" data-qa-id="my-account-left-nav">
    <c:forEach items="${navigationNode.children}" var="childLevel1">
       <c:forEach items="${childLevel1.entries}" var="childlink">
           <cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="${not empty currentUrl and fn:contains(childlink.item.url, currentUrl) ? 'active' : ''} m-list-group__item list-group-item" data-qa-id="my-account-left-nav-item" />
       </c:forEach>
    </c:forEach>
</ul>

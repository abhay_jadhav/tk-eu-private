<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="m-search-results">

    <div class="m-search-results__content">
        <ul class="m-product-list">
            <c:forEach items="${searchPageData.results}" var="product">
                <product:productListerItem product="${product}"/>
            </c:forEach>
        </ul>
    </div>
    <div class="m-search-results__bottom">
        <div id="addToCartTitle" class="display-none">
            <div class="add-to-cart-header">
                <div class="headline">
                    <span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
                </div>
            </div>
        </div>
        <nav:pagination top="false"  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/>
    </div>
</div>

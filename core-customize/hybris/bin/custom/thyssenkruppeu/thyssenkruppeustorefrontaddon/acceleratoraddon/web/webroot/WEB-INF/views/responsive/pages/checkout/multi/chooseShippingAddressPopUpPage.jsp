<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%@ taglib prefix="my-account" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/account" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="selectAddressUrl" value="/delivery-address/choose-shipping-address"/>

<spring:theme code="request.availability.address.lightbox.newAddress" var="newAddressTab" />
<spring:theme code="request.availability.address.lightbox.selectAddress" var="selectAddressTab" />
<ul class="nav nav-tabs nav-justified" role="tablist">
   <li role="presentation" aria-controls="selectAddress" data-toggle="tab" class="active js-shippingaddress-tab"><a class="nav-link m-text-color--black" href="#selectAddress" data-toggle="tab" data-qa-id="select-new-address">${selectAddressTab}</a></li>
   <li role="presentation" aria-controls="addAddress" data-toggle="tab" class="js-shippingaddress-tab"><a class="nav-link m-text-color--black" href="#addAddress" data-toggle="tab" data-qa-id="add-new-address">${newAddressTab}</a></li>
</ul>
<div class="tab-content m-checkout-shipping__form__add--popup">
   <div role="tabpanel" class="tab-pane fade in js-choose-shippingaddress active" id="selectAddress" >
        <form:form id="tkShippingAddressForm" class="js-choose-shipping-address" action="${selectAddressUrl}" method="POST">
            <div class="js-shipping-address-modal">
                <select id="${param.selectAddress}" name="selectedAddressCode" class="form-control m-checkout-form__select-address__values selectpicker js-select-shippingaddress" data-qa-id="select-new-address-dropdown">
                    <option value="" >
                        <spring:theme  code="checkout.multi.billingAddress.select"></spring:theme>
                    </option>
                    <c:forEach var="address" items="${deliveryAddresses}">
                        <c:choose>
                            <c:when test="${selectedDeliveryAddress.id eq address.id}">
                                <option value="${selectedDeliveryAddress.id}" selected>
                                    <c:set var="address" value="${address}" scope="request"/>
                                    <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                </option>
                            </c:when>
                            <c:otherwise>
                                 <c:choose>
                                    <c:when test="${empty selectedDeliveryAddress and primaryShippingAddress.id eq address.id}">
                                        <option value="${primaryShippingASddress.id}" selected>
                                            <c:set var="address" value="${address}" scope="request"/>
                                            <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                        </option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${address.id}">
                                            <c:set var="address" value="${address}" scope="request"/>
                                            <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                        </option>
                                    </c:otherwise>
                                  </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </form:form>
   </div>
<div role="tabpanel" class="tab-pane fade js-shippingaddress-form" id="addAddress"></div>
</div>
    <div class="m-popup__body__actions choose-add-actions">
        <div class="m-popup__body__actions__item" data-qa-id="shippingaddress-popup-cancel-link">
            <a href="javascript:void(0)" class="js-cancel-button">
                <spring:theme code="myaccount.shipping.button.cancel" text="Cancel"/>
            </a>
        </div>
        <div class="m-popup__body__actions__item">
            <button data-qa-id="shippingaddress-popup-save-btn" type="submit" class="btn btn-primary js-add-address-submit">
            <c:choose>
                <c:when test="${addAddressPage eq true}">
                    <spring:theme code="myaccount.shipping.button.add" text="Add"/>
                </c:when>
                <c:otherwise>
                    <spring:theme code="myaccount.shipping.button.save" text="Save"/>
                </c:otherwise>
            </c:choose>
            </button>
        </div>
    </div>
</div>

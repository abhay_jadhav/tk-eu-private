<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="b2b-common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>

<c:url value="/register/completeRegistration" var="completeRegistrationAction"/>
<div class="container l-page__content">
    <div class="row">
        <div class="col-sm-7">
            <h3 class="l-page__title">${headerText}</h3>
            <p class="l-page__description">${headerDetailText}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form:form role="form" id="completeRegistrationForm" name="registrationForm" action="${completeRegistrationAction}" commandName="registrationForm" class="l-page__complete-registration__form js-update-password-validation-form">
                <form:hidden id="token" path="token"/>
                <tkEuformElement:formPasswordBox idKey="password-cr" labelKey="${choosePassword}" path="password" inputCSS="form-control js-password-rule-check" mandatory="true" placeholder="${choosePassword}"/>
                <tkEuformElement:formPasswordBox idKey="confirm-password-cr" labelKey="${confirmPassword}" path="confirmPassword" inputCSS="form-control js-confirm-password-rule-check" mandatory="true" placeholder="${confirmPassword}"/>
                <div class="m-checkbox checkbox l-page__complete-registration__form__checkbox-container">
                    <label for="termsAndConditionsCheckBox">
                        <input type="hidden" name="_termsAndConditions" value="on">
                        <input id="termsAndConditionsCheckBox" name="termsAndConditions" class="js-terms-and-conditions-checkbox m-checkbox__input sr-only" type="checkbox">
                        <span class="m-checkbox__label">
                            <span class="m-checkbox__label__box"></span>
                        </span>
                    ${termsAndConditions}</label>
                </div>
                <form:errors path="termsAndConditions" cssClass="error-termsAndConditions"/>
                <div class="l-page__complete-registration__form__btn-container">
                    <button id="completeRegistrationFormSubmit" type="submit" class="btn btn-default complete-registration_form__btn-submit js-complete-registration-btn-submit">${completeRegistrationButton}</button>
                </div>
            </form:form>
        </div>
        <div class="col-sm-4">
            <div class="l-page__complete-registration__password-requirement-note">
                <b2b-common:passwordSuggestions />
            </div>
        </div>
    </div>
</div>

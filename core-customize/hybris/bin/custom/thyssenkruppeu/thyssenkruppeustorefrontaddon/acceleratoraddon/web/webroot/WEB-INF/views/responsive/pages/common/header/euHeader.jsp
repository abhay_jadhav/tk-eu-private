<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="m-header__content__top__menu hidden-xs js-header-top-links" data-qa-id="header-links">
    <c:choose>
        <c:when test="${user.uid eq 'anonymous' and cmsPage.name ne 'Login Page'}">
            <div class="li">
                <spring:theme code="header.link.login" var="loginLabel"/>
                <a class="js-login-popup-link" data-qa-id="login-btn" href="<c:url value='/login'/>" dialogue-title="${loginLabel}">
                        ${loginLabel}
                </a>
            </div>
        </c:when>
        <c:when test="${user.uid ne 'anonymous'}">
                <cms:pageSlot position="TkEuMyAccountLinks" var="myAccountLink">
                    <div class="li">
                   <span data-qa-id="logged-in-btn">
                       <spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" htmlEscape="true"/><span class="hidden-xs">:</span>
                   </span>
                    </div>
                    <%-- NOTE: The JSP template tkeumyaccountnavcomponent.jsp not used anymore --%>
                    <c:forEach items="${myAccountLink.navigationNode.children}" var="childLevel">
                        <c:forEach items="${childLevel.entries}" var="childlink">
                            <cms:component component="${childlink.item}" evaluateRestriction="true" class="li" element="div"/>
                        </c:forEach>
                    </c:forEach>
                    <div class="li">
                        <a data-qa-id="logout-btn" href="<c:url value='/logout'/>" class="logout-link">
                            <spring:theme code="header.link.logout"/>
                        </a>
                    </div>
                </cms:pageSlot>
        </c:when>
    </c:choose>
</div>

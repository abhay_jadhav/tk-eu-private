/*global $, ACC, window  */

ACC.thyssenkruppwishlist.displayAddToWishListPopup = function(wishListForm) {
    window.$ajaxCallEvent=true;

    $('#addToWishListLayer').remove();
    var titleHeader = $('.add_to_wishlist_form button[type="submit"]').attr('data-popup-title');

    ACC.colorbox.open(titleHeader, {
        html: wishListForm.addToWishListLayer,
        width: "460px",
        onComplete: function() {
            $('.selectpicker').selectpicker({
                style: 'btn-default',
                size: 4
            });
            ACC.colorbox.resize();
        }
    });

    $('#cboxContent').on('input change', '#wishListName', function() {
        $('#addToWishListFormSubmit').prop('disabled', this.value.trim() == "" ? true : false);
    });

    $('#cboxContent').on('click', '.js-add-wishlist-close-button', function(e) {
        e.preventDefault();
        ACC.colorbox.close()
    });
};

ACC.thyssenkruppwishlist.bindWishListForm = function() {
    $('#wishListName').on('input',function() {
        $('#saveWishListForm').prop('disabled', this.value.trim() == "" ? true : false);
    });

    $('li.add-wishlist-form form').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        window.highLightForm(false);
        var wishListName = $(form).find("input[name='wishListName']").val().trim();
        if (window.validateWishlistForm(wishListName) == false) {
            var errorMsg = $('.error.error-message').text();
            window.showErrorMessage(errorMsg);
        } else {
            $.post($(form).attr('action'), $(form).serialize())
            .done(function(data) {
                window.highLightForm(true);

                if (data.errorMessages.length > 0) {
                    window.showErrorMessage(data.errorMessages.join('<br/>'));
                } else {
                    window.location.reload();
                }
            });
        }
    });

    $('#saveWishListForm').prop('disabled', $('#wishListName').val().trim() == "" ? true : false);
};

<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script>
    var products = products || [];
    if (typeof dataLayer !== 'undefined') {
        dataLayer.push( {
            'ecommerce': {
                'currencyCode': '${order.totalPrice.currencyIso}',
                'purchase': {
                    'actionField': {
                        'id': '${order.code}',
                        'revenue': '${order.totalPriceWithTax.value}',
                        'tax':'${order.totalTax.value}',
                        'shipping': '${order.deliveryCost.value}',
                    },
                    'products': products,
                }
            }
        });
    }
</script>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}" class="l-page l-page__order-confirmation">
    <cms:pageSlot position="TopContent" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <div class="container l-page__content">
        <div class="row">
            <c:set var="hasSideNavContent" value="${false}" />
            <cms:pageSlot position="SideContent" var="feature" element="div" class="col-md-3 col-sm-12 col-xs-12 l-page__content__body-left">
                <c:set var="hasSideNavContent" value="${not empty feature}" />
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            <cms:pageSlot position="BodyContent" var="feature" element="div" class="col-md-${hasSideNavContent ? '9' : '12'} col-sm-12 col-xs-12 l-page__content__body-right">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
    </div>
    <cms:pageSlot position="BottomContent" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</template:page>

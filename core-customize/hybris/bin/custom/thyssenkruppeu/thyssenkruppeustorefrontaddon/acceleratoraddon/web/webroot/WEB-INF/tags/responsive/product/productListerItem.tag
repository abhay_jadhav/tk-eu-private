<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>
<li class="m-product-list__item">
        <a class="m-product-list__item__thumb" href="${productUrl}" title="${fn:escapeXml(product.name)}" data-qa-id="plp-product-img">
            <product:productPrimaryImage product="${product}" format="thumbnail"/>
        </a>

        <div class="m-product-list__item__content">
            <div class="row">
                <div class="col-sm-8">
                    <h6 class="m-product-list__item__content__title">
                        <a class="m-text-color--black" data-qa-id="plp-product-name" href="${productUrl}">${fn:escapeXml(product.name)}</a>
                    </h6>
                    <div class="m-product-attributes">
                        <div class="row">
                            <c:if test="${not empty product.tkEuPLPDisplayList}">
                                <c:forEach items="${product.tkEuPLPDisplayList}" var="listElement">
                                    <div class="col-sm-3 m-product-attributes__item">
                                        <div class="m-product-attributes__item__key">
                                             <c:if test="${not empty listElement && not empty listElement.key}">
                                                ${listElement.key}
                                             </c:if>
                                        </div>
                                        <div class="m-product-attributes__item__value">
                                            <c:if test="${not empty listElement && not empty listElement.value}">
                                                ${listElement.value}
                                             </c:if>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="actions-container-for-${fn:escapeXml(component.uid)}">
                        <a class="btn btn-sm btn-primary pull-right" data-qa-id="plp-product-details" href="${productUrl}"> <spring:theme code="product.view.label" /></a>
                    </div>
                </div>
            </div>
        </div>
</li>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:theme code="label.password.reset.choose" text="Choose your password" var="chooseYourPassword"/>
<spring:theme code="label.password.reset.confirm" text="Confirm your password" var="confirmYourPassword"/>
<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <h3 class="page__title"><spring:theme code="password.reset.main.header" text="Set your password"/></h3>
            <p class="page__description"><spring:theme code="password.reset.header.description" text="Please enter your new password"/></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form:form role="form" method="post" commandName="updatePwdForm" id="update-pwd-form" class="page__update-password__form js-update-password-validation-form">
                <template:errorSpanField path="pwd">
                    <label class="control-label" for="password"><spring:theme code="label.password.reset.set" text="Set your password"/></label>
                    <form:password cssClass="form-control" id="password" path="pwd" placeholder="${chooseYourPassword}" autocomplete="off"/>
                </template:errorSpanField>
                <template:errorSpanField path="checkPwd">
                    <label class="control-label" for="confirm-password"><spring:theme code="label.password.reset.confirm" text="Confirm your password"/></label>
                    <form:password cssClass="form-control" id="confirm-password" placeholder="${confirmYourPassword}" path="checkPwd" autocomplete="off"/>
                </template:errorSpanField>
                <div class="page__update-password__form__btn-container">
                    <button type="submit" class="btn btn-default page__update-password__form__btn-submit"><spring:theme code="label.password.reset.change" text="Change Password"/></button>
                </div>
            </form:form>
        </div>
        <div class="col-md-4">
            <div class="page__update-password__password-requirement-note m-callout m-callout--right">
                <spring:theme code="password.reset.policy" text="Password must be at least 8 characters and include at least 1 number"/>
            </div>
        </div>
    </div>
</div>

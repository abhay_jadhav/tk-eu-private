<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>

<spring:url value="/my-account/my-deliveries/" var="deliveryDetailsUrl" htmlEscape="false"/>

<div class="m-page-section">
    <h3 class="h5 m-page-section__title m-page-section__title--bottom-s6 m-page-section__title--no-bottom-border" data-qa-id="order-details-contract-condition-header"><spring:theme code="myaccount.confirmation.details.items" text="Order items" arguments="${fn:length(orderData.unconsignedEntries)-orderData.cancelledEntryCount}" /></h3>
<div class="m-page-section__content">
    <table class="m-order-history m-order-history--bottom-s6 table">
        <thead>
            <tr class=" hidden-xs">
                <th id="header1" class="m-order-history__th" data-qa-id="confirmation-column-header-pos">
                    <spring:theme code="myaccount.confirmation.details.position"/>
                </th>
                <th id="header2" class="m-order-history__th" data-qa-id="order-history-header-po-number">
                    <spring:theme code="myaccount.confirmation.details.quantity"/>
                </th>
                <th id="header3" class="m-order-history__th" data-qa-id="confirmation-column-header-product">
                    <spring:theme code="myaccount.confirmation.details.productName"/>
                </th>
                <th id="header5" class="m-order-history__th" data-qa-id="confirmation-column-header-product-code">
                    <spring:theme code="myaccount.confirmation.details.articleNumber"/>
                </th>
                <th id="header6" class="m-order-history__th m-order-history__th__total">
                    <spring:theme code="myaccount.confirmation.details.sum"/>
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${orderData.entries}" var="order" varStatus="loop">
                <c:if test="${(empty maxItems or (not empty maxItems and loop.count le maxItems)) and not order.isCancelledEntry}">
                    <tr class="m-text-color--black ${not empty order.consignments ? 'm-order-history__tr--confirmed-orders--has-details' : ''}">
                         <td headers="header5" class="m-order-history__td" data-qa-id="confirmation-pos-value">
                             ${fn:escapeXml(order.positionNumber)}
                        </td>
                        <td headers="header5" class="m-order-history__td m-order-history__td--qty" data-qa-id="confirmation-qty">
                             ${order.quantity} ${order.unitName}
                        </td>
                        <td headers="header5" class="m-order-history__td m-order-history__td--product-name" data-qa-id="confirmation-pdt">
                             ${fn:escapeXml(order.product.name)}
                        </td>
                        <td headers="header5" class="m-order-history__td m-order-history__td--product-number" data-qa-id="confirmation-pos-value">
                             ${fn:escapeXml(order.product.code)}
                        </td>
                        <td headers="header5" class="m-order-history__td text-right" data-qa-id="order-history-order-status">
                             <tk-eu-format:price priceData="${order.totalPrice}" displayFreeForZero="true"/>
                        </td>
                    </tr>

                    <c:if test="${not empty order.consignments}">
                        <tr class="m-order-history__tr--confirmed-orders m-order-history__tr--confirmed-order-details">
                            <td headers="header5" class="m-order-history__td" colspan="2">
                            </td>
                            <td headers="header5" class="m-order-history__td">
                                <table class="m-order-history__tr__inner-table">
                                    <thead>
                                        <tr>
                                            <th data-qa-id="ocdetails-deliverynumberheading-text">
                                                <spring:theme code="myaccount.confirmation.details.delivery"/>
                                            </th>
                                            <th data-qa-id="ocdetails-deliverydateheading-text">
                                                <spring:theme code="myaccount.confirmation.details.shippingdate"/>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${order.consignments}" var="consignment" varStatus="loop">
                                            <c:url value="${deliveryDetailsUrl}${ycommerce:encodeUrl(consignment.code)}" var="deliverDetailPath"/>
                                            <tr>
                                                <td>
                                                    <a href="${deliverDetailPath}" data-qa-id="ocdetails-deliverynumber">
                                                        ${consignment.code}
                                                    </a>
                                                </td>
                                                <td data-qa-id="ocdetails-deliverydate">
                                                    <fmt:formatDate value="${consignment.shippingDate}" dateStyle="medium" timeStyle="short" type="date"/>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </td>
                            <td headers="header5" class="m-order-history__td" colspan="3">
                            </td>
                        </tr>
                    </c:if>
                </c:if>
            </c:forEach>
        </tbody>
    </table>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-7">
            <div class="orderTotal m-order-detail__subtotal m-order-detail__subtotal--no-padding-y">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="small">
                            <spring:theme code="myaccount.confirmation.details.netPrice"/>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right small" data-qa-id="confirmation-subtotal-value">
                            <tk-eu-format:price priceData="${orderData.subTotal}" />
                        </p>
                    </div>
                    <%-- <c:if test="${not empty orderData.packagingCosts}">
                    <div class="col-xs-6">
                        <p class="small">
                          <spring:theme code="checkout.summary.page.totals.packaging" /><i data-toggle="tooltip" title="<spring:theme code="checkout.packaging.flat.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                        </p>
                    </div>
                     <div class="col-xs-6 text-right">
                        <p class="small">
                            <format:price priceData="${orderData.packagingCosts}"/>
                        </p>
                     </div>
                     </c:if>

                    <spring:theme code="checkout.serviceFee.tooltip.text" var="servicefeeTooltip"/>
                    <c:if test="${not empty orderData.asmServiceFee}">
                        <div class="col-md-7">
                            <p>
                                <small>
                                    <spring:theme code="checkout.serviceFee.text"/>
                                    <i data-toggle="tooltip" title="${servicefeeTooltip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
                                </small>
                            </p>
                        </div>
                        <div class="col-md-5">
                            <p class="text-right">
                                <small>
                                    <format:price priceData="${orderData.asmServiceFee}" displayFreeForZero="TRUE"/>
                                </small>
                            </p>
                        </div>
                    </c:if> --%>
                    <div class="col-xs-6">
                        <p class="small">
                            <spring:theme code="myaccount.confirmation.details.taxes"/>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right small" data-qa-id="confirmation-tax">
                            <tk-eu-format:price priceData="${orderData.totalTax}" />
                        </p>
                    </div>
                </div>
                <hr class="m-order-detail__subtotal__separator m-order-detail__subtotal__separator--thick">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="totals h5 m-order-detail__subtotal__total-amount-label m-order-detail__subtotal__total-amount-label--no-margin-bottom">
                            <spring:theme code="myaccount.confirmation.details.orderTotal"/>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="totals h5 m-order-detail__subtotal__total-amount-value m-order-detail__subtotal__total-amount-value--no-margin-bottom">
                            <tk-eu-format:price priceData="${orderData.totalPriceWithTax}" /></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:url value="${url}" var="continueActionUrl"/>
<a href="${continueActionUrl}" data-qa-id="continue-shopping-link">${title}</a>

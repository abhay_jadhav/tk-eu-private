<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row">
    <div class="pull-left col-xs-12 col-sm-6 col-md-5 col-lg-4">
        <div class="continue__shopping">
            <action:actions element="div" parentComponent="${component}"/>
        </div>
    </div>
</div>


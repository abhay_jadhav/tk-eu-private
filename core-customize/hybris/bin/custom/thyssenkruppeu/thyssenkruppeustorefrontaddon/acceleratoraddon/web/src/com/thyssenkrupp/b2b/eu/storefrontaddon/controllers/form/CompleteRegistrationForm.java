package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

import java.io.Serializable;

public class CompleteRegistrationForm implements Serializable {

    private String  userId;
    private String  token;
    private String  password;
    private String  confirmPassword;
    private long    timeStampInMillis;
    private Boolean termsAndConditions;

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTimeStampInMillis() {
        return timeStampInMillis;
    }

    public void setTimeStampInMillis(long timeStampInMillis) {
        this.timeStampInMillis = timeStampInMillis;
    }

    public Boolean getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(Boolean termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }
}


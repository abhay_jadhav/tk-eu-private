/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers;

import com.thyssenkrupp.b2b.eu.core.model.TkEuCollapsableParagraphComponentModel;
import com.thyssenkrupp.b2b.eu.core.model.process.SetYourPasswordComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkMiniCartComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.components.TkEuFaqCmsParagraphComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.CompleteRegistrationComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.ContactConfirmationPageComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.ExpiredTokenComponentModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.cms2.components.RegistrationConfirmationComponentModel;

public final class ThyssenkruppeustorefrontaddonControllerConstants {

    public static final String ADDON_PREFIX      = "addon:/thyssenkruppeustorefrontaddon/";
    public static final String STOREFRONT_PREFIX = "/";
    public static final String SUFFIX            = "Controller";

    public static final String VIEWS_CMS_COMPONENTPREFIX          = "cms/";
    public static final String VIEWS_CMS_REGISTRATIONCOMPONENT    = "/view/" + CompleteRegistrationComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_REGISTRATIONCONFIRMATION = "/view/" + RegistrationConfirmationComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_EXPIREDTOKENCOMPONENT    = "/view/" + ExpiredTokenComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_SETYOURPASSWORDCOMPONENT = "/view/" + SetYourPasswordComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_TKMINICARTCOMPONENT      = "/view/" + TkMiniCartComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_FAQCMSPARAGRAPHCOMPONENT = "/view/" + TkEuFaqCmsParagraphComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_CONTACTCONFIRMATIONCOMPONENT    = "/view/" + ContactConfirmationPageComponentModel._TYPECODE + SUFFIX;
    public static final String VIEWS_CMS_TKEUCOLLAPSABLEPARAGRAPHCOMPONENT = "/view/" + TkEuCollapsableParagraphComponentModel._TYPECODE + SUFFIX;

    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHOOSE_SHIPPING_METHOD_PAGE    = ADDON_PREFIX + "pages/checkout/multi/chooseDeliveryMethodPage";
    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SUMMARY_PAGE          = ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_DELIVERY_ADDRESS_PAGE = ADDON_PREFIX + "pages/checkout/multi/addEditDeliveryAddressPage";
    public static final String VIEWS_PAGES_CONTACT_FORM                                     = ADDON_PREFIX + "pages/contact/contactFormPage";
    public static final String VIEWS_PAGES_CONTACT_CONFIRMATION_FORM                        = ADDON_PREFIX + "pages/contact/contactConfirmationPage";
    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_BILLING_ADDRESS_PAGE  = ADDON_PREFIX + "pages/checkout/multi/billingAddressPage";
    public static final String VIEWS_PAGES_FAQ_PAGE                                         = ADDON_PREFIX + "pages/faq/tkEuFaqPage";

    public static final String VIEWS_PAGES_CHECKOUT_CHECKOUT_LOGIN_PAGE           = STOREFRONT_PREFIX + "pages/checkout/checkoutLoginPage";
    public static final String VIEWS_PAGES_CHECKOUT_READ_ONLY_EXPANDED_ORDER_FORM = STOREFRONT_PREFIX + "fragments/checkout/readOnlyExpandedOrderForm";

    public static final String VIEWS_PAGES_REGISTER_COMPLETE_REGISTRATION_PAGE              = ADDON_PREFIX + "pages/registration/completeRegistration";
    public static final String VIEWS_PAGES_REGISTER_REGISTRATION_CONFIRMATION_PAGE = ADDON_PREFIX + "pages/registration/registrationConfirmation";
    public static final String VIEWS_PAGES_REGISTER_TERMS_AND_CONDITIONS           = ADDON_PREFIX + "fragments/registration/termsAndConditionsRegistrationPopup";
    public static final String VIEWS_PAGES_REGISTER_EXPIREDTOKEN_PAGE              = ADDON_PREFIX + "pages/registration/expiredToken";
    public static final String VIEWS_PAGES_REGISTRATION_PAGE              = ADDON_PREFIX + "pages/registration/registration";

    public static final String VIEWS_PAGES_QUICKORDER_QUICK_ORDER_PAGE = ADDON_PREFIX + "pages/quickOrder/quickOrderPage";

    public static final String VIEWS_PAGES_PASSWORD_TOKEN_EXPIRED_PAGE         = ADDON_PREFIX + "pages/password/tokenExpiredPage";
    public static final String VIEWS_PAGES_PASSWORD_PASSWORD_RESET_CHANGE_PAGE = ADDON_PREFIX + "pages/password/passwordResetChangePage";
    public static final String VIEWS_PAGES_FORGOT_PASSWORD_VALIDATION_MESSAGE_PAGE = ADDON_PREFIX + "pages/password/forgotPasswordValidationMessage";
    public static final String VIEWS_PAGES_MINI_CART_POPUP                     = ADDON_PREFIX + "fragments/registration/cart/cartPopup";
    public static final String VIEWS_CONTACT_PAGES_POPUP                       = ADDON_PREFIX + "fragments/registration/contactAcceptDataProtection";

    public static final String VIEWS_PAGES_COOKIE_NOTIFICATION_LEGAL_NOTES = ADDON_PREFIX + "fragments/registration/cookieNotificationLegalNotesPopup";

    public static final String VIEWS_FRAGMENT_PRODUCT_CART_ENTRY_PRICE_PANEL = ADDON_PREFIX + "fragments/product/productCartEntryPricePanel";
    public static final String VIEWS_FRAGMENT_PRODUCT_REQUEST_FOR_AVAILABILITY = ADDON_PREFIX + "fragments/product/requestForAvailability";
    public static final String VIEWS_FRAGMENT_PRODUCT_SAWING_PANEL = ADDON_PREFIX + "fragments/product/productSawingPanel";

    public static final String VIEWS_PAGES_SECURE_LOGIN_PAGE = ADDON_PREFIX + "pages/secureLogin/secureLogin";
    public static final String VIEWS_PAGES_CART_PAGE         = ADDON_PREFIX + "pages/cart/cartPage";
    public static final String VIEWS_FRAGMENT_CART_ITEMS_AND_SUMMARY = ADDON_PREFIX + "fragments/cart/updateCertificate";

    public static final String VIEWS_FRAGMENT_PRODUCT_DETAILS_PANEL = ADDON_PREFIX + "fragments/product/productDetailsPanel";
    public static final String VIEWS_FRAGMENT_PRODUCT_WEIGHT_PANEL = ADDON_PREFIX + "fragments/product/productAddToCartWeightPanel";
    public static final String VIEWS_PAGES_CUSTOMER_REGISTRATION_THANKYOU_PAGE = ADDON_PREFIX + "pages/registration/customerRegistrationThankYou";

    public static final String VIEWS_PAGES_ACCOUNT_ADD_ADDRESS_POPUP = ADDON_PREFIX + "fragments/account/addAccountShippingAddress";

    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SELECT_DELIVERY_ADDRESS_POPUP_PAGE = ADDON_PREFIX + "pages/checkout/multi/chooseShippingAddressPopUpPage";
    public static final String VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_ADD_NEW_DELIVERY_ADDRESS_POPUP_PAGE = ADDON_PREFIX + "pages/checkout/multi/addShippingAddressPopUpPage";

    private ThyssenkruppeustorefrontaddonControllerConstants() {
    }
}

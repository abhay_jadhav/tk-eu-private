<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
<%@ attribute name="favouriteFacetData" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty facetData.values}">
<ycommerce:testId code="facetNav_title_${facetData.name}">
    <div id="${facetData.code}" class="m-facet-tab m-facet-tab--hide js-facet">
        <div class="m-facet-tab__heading">
            <div class="m-facet-tab__heading__name ${not empty facetData.resetUrl ? 'm-facet-tab__heading__name--bold-on-collapsed' : ''} js-facet-name">
                ${facetData.name}
            </div>
            <div class="m-facet-tab__heading__op">
                 <c:if test="${not empty facetData.resetUrl}">
                     <c:url value="${facetData.resetUrl}" var="resetUrl"/>
                     <a class="m-facet-tab__heading__op__reset" href="${resetUrl}"> <small><spring:theme code="search.nav.facetReset"/> </small></a>
                </c:if>
                <a class="m-facet-tab__heading__op__toggle js-facet-tab-toggle collapsed" data-toggle="collapse" href="#m-tab-collapse-${facetData.code}"> <span class="m-facet-tab__heading__op__toggle__ico u-icon-tk u-icon-tk--plus pull-right "></span> </a>
            </div>
        </div>

        <div id="m-tab-collapse-${facetData.code}" class="collapse m-facet-tab__content js-facet-content">
            <div class="js-facet-values js-facet-form" data-scroll="${facetData.facetSearchAndScroll}">
            <div class="m-facet-tab__filter"><input type="text" class="form-control js-filter-facet" placeholder="Filter"></div>
                
                <c:if test="${not empty facetData.topValues}">
                <div class="m-facet-tab__contentdiv">
                    <ul class="m-facet-tab__content__list js-facet-list js-facet-top-values">
                        <c:forEach items="${facetData.topValues}" var="facetValue">
                            <li class="m-facet-tab__content__list__item">
                                <c:if test="${facetData.multiSelect}">
                                    <form action="#" method="get">
                                        <input type="hidden" name="q" value="${facetValue.query.query.value}"/>
                                        <input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
                                        <input type="hidden" name="favourites" value="${favouriteFacetData.selected}"/>
                                        <label class="m-facet-tab__content__list__item__label js-facet-label">
                                            <div class="m-checkbox">
                                                <input class="m-checkbox__input" type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''} class="facet-checkbox" />
                                                <span class="m-checkbox__label">
                                                    <span class="m-checkbox__label__box"></span>
                                                    <span class="m-checkbox__label__text" data-qa-id="plp-facet-label">
                                                        ${fn:escapeXml(facetValue.name)}
                                                        <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                                    </span>
                                                </span>
                                            </div>
                                        </label>
                                    </form>
                                </c:if>
                                <c:if test="${not facetData.multiSelect}">
                                    <c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
                                    <c:choose>
                                        <c:when test="${facetValue.selected}">
                                            <label class="m-facet-tab__content__list__item__label js-facet-label">
                                                <div class="m-checkbox">
                                                    <input type="checkbox" checked="checked" class="m-checkbox__input js-facet-checkbox-link sr-only" />
                                                    <span class="m-checkbox__label">
                                                        <span class="m-checkbox__label__box"></span>
                                                        <a href="${facetValueQueryUrl}&amp;favourites=${fn:escapeXml(favouriteFacetData.selected)}" class="m-checkbox__label__text js-facet-link-trigger" data-qa-id="plp-facet-label">
                                                            ${fn:escapeXml(facetValue.name)}&nbsp;
                                                            <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                                        </a>                                                        
                                                    </span>
                                                </div>
                                            </label>
                                        </c:when>
                                        <c:otherwise>
                                            <label class="m-facet-tab__content__list__item__label js-facet-label">
                                                <div class="m-checkbox">
                                                    <input type="radio" class="m-checkbox__input js-facet-radio-link sr-only" />
                                                    <span class="m-checkbox__label">
                                                        <span class="m-checkbox__label__box m-checkbox__label__box--radio"></span>
                                                        <a href="${facetValueQueryUrl}&amp;favourites=${fn:escapeXml(favouriteFacetData.selected)}" class="m-checkbox__label__text js-facet-link-trigger" data-qa-id="plp-facet-label">
                                                            ${fn:escapeXml(facetValue.name)}&nbsp;
                                                            <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                                        </a>                                                        
                                                    </span>
                                                </div>
                                            </label>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </li>
                        </c:forEach>
                    </ul>
                    <p class="m-facet-tab__filter__msg"><spring:theme code="search.nav.facetValue.notFound"/></p>
                </div>
                </c:if>                

                <div class="m-facet-tab__contentdiv">
                <ul class="m-facet-tab__content__list js-facet-list <c:if test="${not empty facetData.topValues}">m-facet-tab__content__list--hide js-facet-list-hidden</c:if>">
                    <c:forEach items="${facetData.values}" var="facetValue">
                        <li class="m-facet-tab__content__list__item">
                            <c:if test="${facetData.multiSelect}">
                                <ycommerce:testId code="facetNav_selectForm">
                                <form action="#" method="get">
                                    <input type="hidden" name="q" value="${facetValue.query.query.value}"/>
                                    <input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
                                    <input type="hidden" name="favourites" value="${favouriteFacetData.selected}"/>
                                    <label class="m-facet-tab__content__list__item__label js-facet-label">
                                        <div class="m-checkbox">
                                        <input type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''}  class="m-checkbox__input js-facet-checkbox sr-only" />
                                        <span class="m-checkbox__label">
                                            <span class="m-checkbox__label__box"></span>
                                            <span class="m-checkbox__label__text" data-qa-id="plp-facet-label">
                                                ${fn:escapeXml(facetValue.name)}&nbsp;
                                                <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                            </span>                                            
                                        </span>
                                        </div>
                                    </label>
                                </form>
                                </ycommerce:testId>
                            </c:if>

                            <c:if test="${not facetData.multiSelect}">
                                <c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
                                <c:choose>
                                    <c:when test="${facetValue.selected}">
                                        <div class="m-checkbox">
                                            <label class="m-facet-tab__content__list__item__label js-facet-label">
                                                <input type="checkbox" checked="checked" class="m-checkbox__input js-facet-radio-link sr-only" />
                                                <span class="m-checkbox__label">
                                                    <span class="m-checkbox__label__box"></span>
                                                    <a href="${facetValueQueryUrl}&amp;favourites=${fn:escapeXml(favouriteFacetData.selected)}" class="m-checkbox__label__text js-facet-link-trigger" data-qa-id="plp-facet-label">
                                                        ${fn:escapeXml(facetValue.name)}&nbsp;
                                                        <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                                    </a>                                                    
                                                </span>
                                            </label>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <label class="m-facet-tab__content__list__item__label js-facet-label">
                                            <div class="m-checkbox">
                                                <input type="radio" class="m-checkbox__input js-facet-checkbox-link sr-only" />
                                                <span class="m-checkbox__label facet__list__label">
                                                    <span class="m-checkbox__label__box m-checkbox__label__box--radio"></span>
                                                    <a href="${facetValueQueryUrl}&amp;favourites=${fn:escapeXml(favouriteFacetData.selected)}" class="m-checkbox__label__text js-facet-link-trigger" data-qa-id="plp-facet-label">
                                                        ${fn:escapeXml(facetValue.name)}&nbsp;
                                                        <spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/>
                                                    </a>                                                    
                                                </span>
                                            </div>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </li>
                    </c:forEach>
                </ul>
                <p class="m-facet-tab__filter__msg"><spring:theme code="search.nav.facetValue.notFound"/></p>
                </div>
                
                <c:if test="${not empty facetData.topValues}">
                    <span class="m-facet-tab__content__toggle m-facet-tab__content__toggle--more js-more-facet-values">
                        <a href="#" class="js-more-facet-values-link" > <small><spring:theme code="search.nav.facetShowMore" /> </small></a>
                    </span>
                    <span class="m-facet-tab__content__toggle m-facet-tab__content__toggle--less js-less-facet-values">
                        <a href="#" class="js-less-facet-values-link"> <small><spring:theme code="search.nav.facetShowLess" /> </small></a>
                    </span>
                </c:if>
            </div>
        </div>
    </div>
</ycommerce:testId>
</c:if>

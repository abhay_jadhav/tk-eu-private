<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/cart" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="row">
    <div class="col-xs-12 col-md-5 col-lg-6">
    </div>
    <div class="col-xs-12 col-md-7 col-lg-6">
        <div class="cart-totals">
            <div class="js-cart-totals">
                <checkout:tkOrderTotals cartData="${cartData}" showTax="true"/>
            </div>
            <cart:ajaxCartTotals/>
        </div>
    </div>
</div>

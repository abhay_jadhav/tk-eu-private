package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cookieNotification")
public class TkEuCookieNotificationController extends AbstractPageController {

    private static final String PAGE_UID = "cookieNotificationLegalNotes";

    @RequestMapping(value = "/legalNotes")
    public String getCookieNotificationLegalNotes(final Model model) throws CMSItemNotFoundException {
        final ContentPageModel pageForRequest = getContentPageForLabelOrId(PAGE_UID);
        storeCmsPageInModel(model, pageForRequest);
        setUpMetaDataForContentPage(model, pageForRequest);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_COOKIE_NOTIFICATION_LEGAL_NOTES;
    }
}

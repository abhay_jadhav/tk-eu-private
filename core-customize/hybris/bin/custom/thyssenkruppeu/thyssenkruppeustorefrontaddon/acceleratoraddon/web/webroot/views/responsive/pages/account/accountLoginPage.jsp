<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}" class="l-page l-page__login">
    <div class="container l-page__content">
        <div class="row">
            <div class="col-md-7">
                <h3 class="l-page__title" data-qa-id="login-header">
                    <spring:theme code="login.title"/>
                </h3>
                <p class="l-page__description" data-qa-id="login-explanation">
                    <spring:theme code="login.description"/>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <cms:pageSlot position="LeftContentSlot" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
            <div class="col-md-8">
                <cms:pageSlot position="RightContentSlot" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
        </div>
    </div>
</template:page>

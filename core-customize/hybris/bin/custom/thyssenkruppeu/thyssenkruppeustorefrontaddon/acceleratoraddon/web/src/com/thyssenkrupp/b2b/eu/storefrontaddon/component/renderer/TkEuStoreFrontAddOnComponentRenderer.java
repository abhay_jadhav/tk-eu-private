package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonConstants;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

public class TkEuStoreFrontAddOnComponentRenderer<C extends AbstractCMSComponentModel> extends DefaultAddOnCMSComponentRenderer {

    @Override
    protected String getAddonUiExtensionName(AbstractCMSComponentModel component) {
        return ThyssenkruppeustorefrontaddonConstants.EXTENSIONNAME;
    }
}


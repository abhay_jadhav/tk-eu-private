<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url value="/my-account/order/cancel" var="cancelOrderUrl" />
<c:url value="/cart/re-order" var="reorderUrl" />
<c:url value="/my-account/order/confirmation/" var="sapOrderConfirmationUrl" />
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="m-order-ack">
    <div class="row">
        <div class="col-md-10 col-sm-8 col-xs-12">
            <h4 data-qa-id="order-acknowledge-details-headline"> <spring:theme code="myaccount.acknowledge.details.headline.orderAcknowledge" arguments="${fn:escapeXml(orderData.code)}" text="Order information"/>&nbsp;(${orderData.orderStatusName})</h4>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-12">
            <spring:theme code="myaccount.acknowledge.details.popup.headline" var="reorderPopupTitle"/>
            <a href="javascript::void(0)" data-dialogue-title="${reorderPopupTitle}" class="btn btn-primary btn-sm pull-right js-reorder-confirmation-popup-link" data-qa-id="order-details-reorder-btn"> Reorder </a>
            <div class="display-none js-reorder-confirmation-form">
                <c:set var="orderCode" value="${orderData.code}" scope="request"/>
                <action:actions parentComponent="${component}"/>
            </div>
        </div>
    </div>
    <hr class="m-order-ack__separator m-order-ack__separator--thick"/>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="order-details-date-submitted-label"><spring:theme code="checkout.orderConfirmation.datePlaced"/></p>
                <p class="m-item-group__value" data-qa-id="order-details-date-submitted-value">${order.formattedSubmittedDate}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="order-details-order-total-label"><spring:theme code="checkout.orderConfirmation.total"/></p>
                <p class="m-item-group__value" data-qa-id="order-details-order-total-value"><format:price priceData="${order.totalPrice}"/></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <c:if test="${orderData.status.code ne 'CANCELLED'}">
                    <p class="m-item-group__label" data-qa-id="order-acknowledge-details-confirmation-label"><spring:theme code="myaccount.acknowledge.details.orderConfirmation" text="Order confirmation"/>
                        <c:if test="${not empty currentUserOrder }">
                            <spring:theme code="order.cancel.text" var="cancelOrderText"/>
                            <a href="javascript:void(0);" class="js-order-cancel-form-popup-link smaller" >&nbsp; ${cancelOrderText} </a>
                        </c:if>
                    </p>
                    <c:if test="${not empty currentUserOrder }">
                        <div class="js-order-cancel-form-popup hide" data-dialogue-title="${cancelOrderText}">
                            <p>
                                <spring:theme code="order.cancel.dialogue.message1"/>
                            </p>
                            <p>
                                <spring:theme code="order.cancel.dialogue.message2"/>
                            </p>
                            <form:form id="TkOrderCancelForm" class="js-order-cancel-form" method="POST" action="${cancelOrderUrl}">
                                <input type="hidden"  name="orderCode" value="${orderData.code}" />
                            </form:form>

                            <div class="actions">
                                <div class="col-sm-4">
                                    <button id="confirmNo" type="button" class="btn btn-link btn-block">
                                        <spring:theme code="unselect.dialogue.btn.no" />
                                    </button>
                                </div>
                                <div class="col-sm-8">
                                    <button id="confirmYes" type="button" class="btn btn-default btn-block">
                                        <spring:theme code="unselect.dialogue.btn.order.yes" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:choose>
                        <c:when test="${not empty orderConfirmationId}">
                            <p class="m-item-group__value" data-qa-id="order-acknowledge-details-confirmation-value"><a href="${sapOrderConfirmationUrl}${orderConfirmationId}">${orderConfirmationId}</a><br/>
                               <small><spring:theme code="myaccount.acknowledge.details.orderConfirmation.description1" text=""/></small>
                            </p>
                       
                        </c:when>
                        <c:otherwise>
                            <p class="m-item-group__value"><spring:theme code="myaccount.acknowledge.details.orderConfirmation.description2" text=""/></p>
                        </c:otherwise>
                    </c:choose>
                    </p>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="m-accordion">
    <div class="m-accordion__header collapsed" data-qa-id="order-information-header" data-toggle="collapse" data-target="#order-information">
        <h3 class="h5 m-accordion__header__title m-text-color--blue">
            <spring:theme code="myaccount.acknowledge.details.headline.moreInfo" text=""/>
        </h3>
    </div>
    <div id="order-information" class="m-accordion__content collapse">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-order-name-label"><spring:theme code="text.account.order.orderDetails.OrderName"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-order-name-value">${fn:escapeXml(orderData.name)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-po-number-label"><spring:theme code="text.account.order.orderDetails.purchaseOrderNumber"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-po-number-value">${orderData.paymentType.code=='ACCOUNT' and not empty orderData.purchaseOrderNumber ? fn:escapeXml(orderData.purchaseOrderNumber) : ""}&nbsp;</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-comment-label"><spring:theme code="checkout.orderConfirmation.shippingComment" /></p>
                    <p class="m-item-group__value m-checkout-form__value--has-no-formatting" data-qa-id="order-details-shipping-comment-value">${fn:escapeXml(order.customerShippingNotes)}&nbsp;</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-submitted-by-label"><spring:theme code="checkout.orderConfirmation.orderPlacedBy"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-submitted-by-value">${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <c:if test="${not empty orderData.placedBy}">
                    <div class="m-item-group">
                        <p class="m-item-group__label"><spring:theme code="text.order.placedByText"/></p>
                        <p class="m-item-group__value"><spring:theme code="text.order.customerService"/></p>
                    </div>
                </c:if>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-method-label"><spring:theme code="checkout.orderConfirmation.shippingMethod" /></p>
                    <p class="m-item-group__value" data-qa-id="order-details-shipping-method-value">${fn:escapeXml(orderData.deliveryMode.name)}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-shipping-terms-label"><spring:theme code="checkout.orderConfirmation.shippingTerms" /></p>
                    <p class="m-item-group__value" data-qa-id="order-details-shipping-terms-value">${fn:escapeXml(orderData.shippingCondition)}&nbsp;</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-payment-terms-label"><spring:theme code="myaccount.company.payment.terms"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-payment-terms-value">${orderData.paymentTerms}&nbsp;</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="m-item-group">
                    <p class="m-item-group__label" data-qa-id="order-details-incoterms-label"><spring:theme code="text.account.orderHistory.incoTerms"/></p>
                    <p class="m-item-group__value" data-qa-id="order-details-incoterms-value">${fn:escapeXml(orderData.incoTerms)}&nbsp;</p>
                </div>
            </div>
        </div>
        <%--NOTE: Remove if not required -- start  --%>
        <c:if test="${orderData.paymentType.code eq 'ACCOUNT' and orderData.costCenter}">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label"><spring:theme code="text.account.order.orderDetails.CostCenter"/></p>
                        <p class="m-item-group__value">${fn:escapeXml(orderData.costCenter.name)}</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="m-item-group">
                        <p class="m-item-group__label"><spring:theme code="text.account.order.orderDetails.ParentBusinessUnit"/></p>
                        <p class="m-item-group__value">${fn:escapeXml(orderData.costCenter.unit.name)}</p>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <c:if test="${orderData.quoteCode ne null}">
                    <div class="m-item-group">
                        <spring:url htmlEscape="false" value="/my-account/my-quotes/${orderData.quoteCode}" var="quoteDetailUrl"/>
                        <p class="m-item-group__label"><spring:theme code="text.account.quote.code"/></p>
                        <p class="m-item-group__value"><a href="${quoteDetailUrl}" >${fn:escapeXml(orderData.quoteCode)}</a></p>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>

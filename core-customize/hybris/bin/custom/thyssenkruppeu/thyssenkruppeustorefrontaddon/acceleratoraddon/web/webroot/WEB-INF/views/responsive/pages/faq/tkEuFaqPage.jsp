<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib  prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <div class="container">
        <div class="row">
            <cms:pageSlot position="Section2Navigation" var="feature" element="div" class="col-md-3">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            <c:set var="isFirstTitle" value="${true}" />
            <cms:pageSlot position="Section2Content" var="feature" element="div" class="col-md-9">
                <c:if test="${fn:contains(feature.uid, 'TkEuFaqPageTitleComponent')}">
                    <c:if test="${not isFirstTitle}">
                        </div>
                    </c:if>
                    <div id="${feature.uid}" class="l-faq__topics__content ${isFirstTitle ? 'active' : ''}" >
                    <c:set var="isFirstTitle" value="${false}"/>
                </c:if>
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            </div>
        </div>
    </div>
    <cms:pageSlot position="Section3" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</template:page>

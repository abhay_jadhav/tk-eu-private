<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false" %>
<%@ attribute name="isMiniHeader" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<link rel="stylesheet" type="text/css" media="all" href="/_ui/responsive/patternlibrary/css/generic.min.css?v=${buildTimestamp}"/>


<cms:pageSlot position="TopHeaderSlot" var="component">
    <cms:component component="${component}" />
</cms:pageSlot>

<cms:pageSlot position="GoogleTagManagerContentSlot" var="component" element="div" class="container">
    <cms:component component="${component}" />
</cms:pageSlot>

<header class="m-header ${isMiniHeader ? 'm-header--minimal' : ''}">
    <div class="m-header__content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 ${isMiniHeader ? 'col-sm-5' : 'col-sm-2'}">
                    <cms:pageSlot position="SiteLogo" var="logo" limit="1">
                        <a href="${fn:escapeXml(logo.urlLink)}" data-qa-id="header-logo">
                            <h1 class="m-header__content__logo">thyssenkrupp</h1>
                        </a>
                    </cms:pageSlot>
                </div>
                <div class="col-xs-6  ${isMiniHeader ? 'col-sm-6 col-sm-offset-1' : 'col-sm-10'}">
                    <c:if test="${!isMiniHeader}">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="m-header__content__top">
                                    <cms:pageSlot position="JspHeader" var="component">
                                        <cms:component component="${component}"/>
                                    </cms:pageSlot>
                                </nav>
                            </div>
                        </div>
                    </c:if>
                    <div class="row">
                        <c:if test="${!isMiniHeader}">
                            <div class="col-sm-8 col-md-9 hidden-xs">
                                <cms:pageSlot position="SearchBox" var="component">
                                    <cms:component component="${component}"/>
                                </cms:pageSlot>
                            </div>
                        </c:if>
                        <div class="col-xs-12 ${isMiniHeader ? 'col-sm-8 col-md-9 col-sm-offset-4 col-md-offset-3' : 'col-sm-4 col-md-3'}" data-qa-id="header-name-shop">
                            <cms:pageSlot position="TkEuDivisionName" var="feature">
                                <cms:component component="${feature}" element="h2" class="h6 m-header__content__department" data-qa-id="header-name-shop"/>
                            </cms:pageSlot>
                            <cms:pageSlot position="TkEuDepartmentName" var="feature">
                                <cms:component component="${feature}" element="h3" class="h5 m-header__content__country" data-qa-id="header-name-shop"/>
                            </cms:pageSlot>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${!isMiniHeader}">
        <div class="m-header__primary-nav yamm">
            <nav class="navbar navbar-primary">
                <div class="container">
                    <div class="navbar__content">
                        <a href="#" class=" m-header__primary-nav__menu-toggle js-menu-toggle hidden-md hidden-lg"><i class="u-icon-tk u-icon-tk--menu"> </i></a>
                        <a href="#search-form-mobile" class="m-header__primary-nav__search-toggle js-search-toggle visible-xs-inline-block" data-toggle="collapse"><i class="u-icon-tk u-icon-tk--search-alt"> </i></a>
                        <nav:topNavigation/>
                        <cms:pageSlot position="MiniCart" var="cart" element="ul" class="nav navbar-nav navbar-right m-header__primary-nav__menu-right">
                            <cms:component component="${cart}" element="li" class="dropdown m-mini-cart js-mini-cart-dropdown"/>
                        </cms:pageSlot>
                        <hr class="navbar__divider visible-md visible-lg"/>
                    </div>
                </div>
            </nav>

        </div>
        <div id="search-form-mobile" class="m-header__search-panel collapse">
            <cms:pageSlot position="SearchBox" var="component">
                <cms:component component="${component}"/>
            </cms:pageSlot>
        </div>
    </c:if>
    <common:noscriptWarning/>
    <common:globalMessages />
</header>
<cms:pageSlot position="BottomHeaderSlot" var="component">
    <cms:component component="${component}" />
</cms:pageSlot>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tkeucommerce" uri="http://tkeustorefront/tld/tkeucommerce"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ attribute name="parentHref" required="false" type="java.lang.String" %>
<%@ attribute name="parentNode" required="true" type="de.hybris.platform.core.model.ItemModel" %>
<%@ attribute name="categories" required="true" type="java.util.Collection" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="facetName" required="false" type="java.lang.String" %>
<%@ attribute name="maxDepth" required="false" type="java.lang.Number" %>
<%@ attribute name="depth" required="false" type="java.lang.Number" %>

<spring:theme code="text.header.navigation.overview" text="Overview" var="overview"/>
<c:set var="parentNodeId" value="${parentNode.pk}"/>
<div class="dropdown col-md-3 m-nav-node__container" id="${parentNodeId}" maxDepth="${maxDepth}" depth="${depth}">
    <ul class="dropdown-menu m-nav-node__container__list">
        <c:if test="${not empty title}">
            <li class="title-submenu m-nav-node__container__list__heading">
                <a href="${parentHref}" class="m-nav-node__container__list__heading__link">
                    <small>${overview}</small><br/>
                    <strong>${title}</strong>
                </a>
            </li>
        </c:if>
        <c:forEach items="${categories}" var="category">
            <c:set var="leafLevelClass" value="${empty category.categories or depth lt maxDepth ? 'has-facet-leaf-menu' : ''}"/>
            <li class="dropdown-submenu ${leafLevelClass} m-nav-node__container__list__item ${not empty category.categories ? 'm-nav-node__container__list__item--has-children' : ''}" data-toggle="dropdown" data-target="#${category.pk}" data-parent-id="#${parentNodeId}">
                <c:url value="${tkeucommerce:categoryUrl(category)}" var="linkUrl"/>
                <spring:url value="/api/facets${tkeucommerce:categoryUrl(category)}" var="linkApiUrl">
                    <spring:param name="facetName" value="${facetName}"/>
                </spring:url>
                <a class="m-nav-node__container__list__item__link" data-href="${linkApiUrl}" href="${linkUrl}" data-qa-id="select-category-link">
                    <c:if test="${not empty category.picture.URL}"><img class="icon" src="${category.picture.URL}" alt="${fn:escapeXml(category.name)}" title="${fn:escapeXml(category.name)}"/></c:if>
                    <span class="name">${category.name}</span>
                </a>
            </li>
        </c:forEach>
    </ul>
</div>

<c:set var="depth" value="${depth + 1}" />
<c:forEach items="${categories}" var="category">
    <c:choose>
        <c:when test="${not empty category.categories and depth lt maxDepth}">
            <c:url value="${tkeucommerce:categoryUrl(category)}" var="linkUrl"/>
            <common:megamenu categories="${category.categories}" title="${category.name}" parentNode="${category}" facetName="${facetName}" maxDepth="${maxDepth}" depth="${depth}" parentHref="${linkUrl}"/>
        </c:when>
        <c:otherwise>
            <div class="dropdown col-md-3 m-nav-node__container" id="${category.pk}" depth="${depth}">
                <ul class="dropdown-menu m-nav-node__container__list">
                    <li class="title-submenu m-nav-node__container__list__heading">
                        <a href="${parentHref}" class="m-nav-node__container__list__heading__link">
                            <small>${overview}</small><br/>
                            <strong>${category.name}</strong>
                        </a>
                    </li>
                </ul>
            </div>
        </c:otherwise>
    </c:choose>
</c:forEach>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tkeucommerce" uri="http://tkeustorefront/tld/tkeucommerce"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ attribute name="parentNode" required="true" type="de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel" %>
<%@ attribute name="nodes" required="true" type="java.util.Collection" %>
<%@ attribute name="isRoot" required="false" type="java.lang.Boolean" %>

<c:set var="parentNodeId" value="${parentNode.pk}" />
<c:set var="containerClass" value="" />
<c:if test="${isRoot}">
    <c:set var="containerClass" value="--open" />
</c:if>
<div class="dropdown${containerClass} col-md-3 m-nav-node__container" id="${parentNodeId}">
    <ul class="dropdown-menu m-nav-node__container__list">
        <c:forEach items="${nodes}" var="node">
            <c:set var="hasChildren" value="${false}"/>
            <c:choose>
                <c:when test="${node.isDynamic}">
                    <c:set var="category" value="${node.category}"/>
                    <c:if test="${not empty category}">
                        <c:set var="categories" value="${category.categories}" />
                    </c:if>
                    <c:if test="${fn:length(node.childCategories) gt 0}">
                        <c:set var="categories" value="${node.childCategories}" />
                    </c:if>
                    <c:if test="${not empty categories and node.categoryDepth gt 0}">
                        <c:set var="hasChildren" value="${true}"/>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <c:if test="${fn:length(node.children) gt 0}">
                        <c:set var="hasChildren" value="${true}"/>
                    </c:if>
                </c:otherwise>
            </c:choose>


            <c:set var="leafLevelClass" value=""/>
            <c:set var="category" value=""/>
            <c:set var="linkName" value="${node.title}" />
            <c:set var="categoryImageUrl" value="" />
            <c:set var="linkUrl" value="#" />
            <c:if test="${fn:length(node.entries) gt 0}">
                <%--NOTE: If the node has entires show first node entry item linkName and url --%>
                <c:set var="linkName" value="${node.entries[0].item.linkName}" />
                <c:set var="linkUrl" value="${node.entries[0].item.url}" />
                <c:if test="${not empty node.entries[0].item.category}">
                    <c:set var="category" value="${node.entries[0].item.category}"/>
                    <%--NOTE: If the node entry is a category, show category name and category url --%>
                    <c:set var="linkName" value="${category.name}" />
                    <c:url var="categoryImageUrl" value="${category.picture.URL}" />
                    <c:url var="linkUrl" value="${tkeucommerce:categoryUrl(category)}" />
                </c:if>
            </c:if>

            <%--NOTE: If the node is a dynamic node--%>
            <c:if test="${node.isDynamic}">
                <c:set var="linkName" value="${node.title}" /><%--NOTE: Reset the item name to node title --%>
                <c:set var="linkUrl" value="${fn:length(node.links) gt 0 ? node.links[0].url : ''}" /> <%--NOTE: Set the node link from first of nodes link if available --%>
                <c:if test="${not empty node.category}">
                    <c:set var="category" value="${node.category}"/>
                    <%--NOTE: If the node has a category attached, show the category name and url--%>
                    <c:url var="linkUrl" value="${not empty linkUrl ? linkUrl : tkeucommerce:categoryUrl(category)}" />
                    <c:set var="linkName" value="${not empty linkName ? linkName : category.name}" />
                    <%--NOTE: Get the facet list for async loading of facets--%>
                    <spring:url var="linkApiUrl" value="/api/facets${tkeucommerce:categoryUrl(category)}" >
                        <spring:param name="facetName" value="${node.facetName}"/>
                    </spring:url>
                    <c:if test="${not hasChildren}">
                        <%--NOTE: Set the node to load facets async if empty categories and childCategories or categoryDepth is set to 0--%>
                        <c:set var="leafLevelClass" value="has-facet-leaf-menu"/>
                    </c:if>
                </c:if>
            </c:if>
            <li class="dropdown-submenu ${leafLevelClass} m-nav-node__container__list__item ${hasChildren ? 'm-nav-node__container__list__item--has-children' : ''}" data-toggle="dropdown" data-target="#${node.pk}" data-parent-id="#${parentNodeId}">
                <a class="m-nav-node__container__list__item__link" data-href="${linkApiUrl}" href="${not empty linkUrl ? linkUrl : '#'}"  data-qa-id="select-category-link">
                    <c:if test="${not empty categoryImageUrl}">
                        <img class="icon" src="${categoryImageUrl}" alt="${fn:escapeXml(linkName)}" title="${fn:escapeXml(linkName)}"/>
                    </c:if>
                    <span class="name">${linkName}</span>
                </a>
            </li>
        </c:forEach>
    </ul>
</div>

<c:forEach items="${nodes}" var="node">
    <c:set var="categories" value=""/>
    <c:choose>
        <c:when test="${node.isDynamic}">
            <c:set var="category" value="${node.category}"/>
            <c:if test="${not empty category}">
                <c:set var="categories" value="${category.categories}" />
            </c:if>
            <c:if test="${fn:length(node.childCategories) gt 0}">
                <c:set var="categories" value="${node.childCategories}" />
            </c:if>
            <c:choose>
                <c:when test="${not empty categories and node.categoryDepth gt 0}">
                    <c:url value="${tkeucommerce:categoryUrl(category)}" var="linkUrl"/>
                    <common:megamenu categories="${categories}" title="${node.title}" facetName="${node.facetName}" parentNode="${node}" maxDepth="${node.categoryDepth}" depth="${0}" parentHref="${linkUrl}"/>
                </c:when>
                <c:otherwise>
                    <c:if test="${not empty category}">
                        <div class="dropdown col-md-3 m-nav-node__container" id="${node.pk}">
                            <ul class="dropdown-menu m-nav-node__container__list">
                                <li class="title-submenu m-nav-node__container__list__heading">
                                    <spring:theme code="text.header.navigation.overview" text="Overview" var="overview"/>
                                    <small>${overview}</small><br/>
                                    <strong>${node.title}</strong>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:if test="${fn:length(node.children) gt 0}">
                <common:navNodes nodes="${node.children}" parentNode="${node}" isRoot="${false}" />
            </c:if>
        </c:otherwise>
    </c:choose>
</c:forEach>

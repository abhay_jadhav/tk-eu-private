<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<div>
    <tk-eu-product:productAddToCartWeightPanel/>
    <tk-eu-product:productAddToCartLengthPanel/>
    <tk-eu-product:productCartEntryPricePanel/>
    <tk-eu-product:productBasePricePanel/>
</div>

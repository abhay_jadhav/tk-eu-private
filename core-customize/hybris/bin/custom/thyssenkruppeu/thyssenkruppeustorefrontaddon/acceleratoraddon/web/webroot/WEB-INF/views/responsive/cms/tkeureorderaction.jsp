<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="reorderUrl" value="${fn:escapeXml(url)}" scope="page"/>

<form:form id="tkEuReorderForm" name="tkEuReorderForm" cssClass="m-order-ack__popup-form js-reorder-form" action="${reorderUrl}" method="post">
    <input type="hidden" id="orderCode" name="orderCode"  value="${fn:escapeXml(orderData.code)}">
    <input name="token" type="hidden" value="${token}"/>
    <div class="control-group m-order-ack__popup-form__content" data-qa-id="order-details-reorder-popup-message">
        <spring:theme code="myaccount.acknowledge.details.popup.description" htmlEscape="false"/>
    </div>
    <div class="m-order-ack__popup-form__action">
        <a href="javascript::void(0);"  class="js-cancel-button" data-qa-id="order-details-reorder-popup-cancel-btn" >
            <spring:theme code="myaccount.acknowledge.details.popup.no"/></a>
        <button class="btn btn-default pull-right" data-qa-id="order-details-reorder-popup-yes-btn" type="submit">
            <spring:theme code="myaccount.acknowledge.details.popup.yes"/></button>
    </div>
</form:form>

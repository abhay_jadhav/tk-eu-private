<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/cart/miniCart/{/totalDisplay}" var="refreshMiniCartUrl" htmlEscape="false">
    <spring:param name="totalDisplay"  value="${totalDisplay}"/>
</spring:url>
<spring:url value="/cart/rollover/{/componentUid}" var="rolloverPopupUrl" htmlEscape="false">
    <spring:param name="componentUid"  value="${component.uid}"/>
</spring:url>
<c:url value="/cart" var="cartUrl"/>
<a href="${cartUrl}"
    data-qa-id="mini-cart-btn"
    class="m-mini-cart__link m-mini-cart__link--has-icon-right js-mini-cart-link dropdown-toggle"
    data-mini-cart-url="${rolloverPopupUrl}"
    data-mini-cart-refresh-url="${refreshMiniCartUrl}"
    data-mini-cart-name="<spring:theme code="text.cart"/>"
    data-mini-cart-empty-name="<spring:theme code="popup.cart.empty"/>"
    data-mini-cart-items-text="<spring:theme code="basket.items"/>"
    >
    <span class="m-mini-cart__count js-mini-cart-count"  data-qa-id="mini-cart-number">${totalItems lt 100 ? totalItems : "99+"}</span><span class="m-mini-cart__price js-mini-cart-price"><%--
        --%><c:if test="${totalDisplay == 'TOTAL'}"><%--
            --%><format:price priceData="${totalPrice}" /><%--
        --%></c:if><%--
        --%><c:if test="${totalDisplay == 'SUBTOTAL'}"><%--
            --%><format:price priceData="${subTotal}" /><%--
        --%></c:if><%--
        --%><c:if test="${totalDisplay == 'TOTAL_WITHOUT_DELIVERY'}"><%--
            --%><format:price priceData="${totalNoDelivery}" /><%--
        --%></c:if><%--
--%></span>
</a>
<ul class="dropdown-menu m-mini-cart__dropdown js-mini-cart-dropdown-menu"><li><div class="yamm-content"></div></li></ul>
<div class="mini-cart-container js-mini-cart-container"></div>

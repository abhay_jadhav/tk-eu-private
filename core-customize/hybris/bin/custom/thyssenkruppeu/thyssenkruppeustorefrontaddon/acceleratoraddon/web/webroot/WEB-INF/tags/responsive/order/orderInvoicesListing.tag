<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hidePagination" required="false" type="String" %>
<%@ attribute name="hideSorting" required="false" type="String" %>
<%@ attribute name="searchUrl" required="true" type="String" %>
<%@ attribute name="messageKey" required="true" type="String" %>
<%@ attribute name="maxItems" required="false" type="String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/my-invoices/" var="orderInvoiceDetailsUrl" htmlEscape="false"/>
<c:url value="/" var="homeUrl" />

<c:if test="${empty searchPageData.results}">
    <cms:pageSlot position="AccountOrderInvoiceNotFound" var="feature">
        <cms:component component="${feature}" element="p"/>
    </cms:pageSlot>
</c:if>

<c:if test="${not empty searchPageData.results}">
    <div class="hidden">
        <nav:pagination top="true" msgKey="${messageKey}" hideRefineButton="${true}" hideFavoriteFlag = "${true}"
                                   searchKey="${searchKey}"
                                   showTopTotals="${false}"
                                   supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                   searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                   numberPagesShown="${numberPagesShown}"/>
    </div>

    <c:forEach items="${searchPageData.sorts}" var="sort">
        <c:if test="${sort.selected}">
            <c:set var="currentSort" value="${sort.code}" />
        </c:if>
    </c:forEach>
    <c:set var="orderToSortBy" value="${'Asc'}"/>
    <c:if test="${not empty currentSort}">
        <c:set var="orderToSortBy" value="${fn:contains(currentSort, 'Asc') ? 'Desc' : 'Asc'}"/>
        <c:set var="sortClassPrefix" value="${fn:contains(currentSort, 'Asc') ? '-asc' : '-desc'}" />
    </c:if>
    <div class="table-responsive">
    <table class="m-order-history table">
        <thead>
            <tr>
                <c:choose>
                    <c:when test="${hideSorting}">
                        <th id="header1" class="m-order-history__th" data-qa-id="myaccount-invoice-number-title-text">
                            <spring:theme code="myaccount.invoices.invoiceNumber"/>
                        </th>
                        <th id="header2" class="m-order-history__th" data-qa-id="myaccount-invoice-date-title-text">
                            <spring:theme code="myaccount.invoices.invoiceDate"/>
                        </th>
                           <th id="header3" class="m-order-history__th" data-qa-id="myaccount-invoice-due-date-title-text">
                            <spring:theme code="myaccount.invoices.dueDate"/>
                        </th>
                        <th id="header4" class="m-order-history__th" data-qa-id="myaccount-billing-address-title-text">
                            <spring:theme code="myaccount.invoices.billingAddress"/>
                        </th>
                        <th id="header5" class="m-order-history__th" data-qa-id="myaccount-total-title-text">
                            <spring:theme code="myaccount.invoices.total"/>
                        </th>
                    </c:when>
                    <c:otherwise>
                        <th onclick="$('#sortOptions1').val('byInvoiceNo${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byInvoiceNo') ? sortClassPrefix : ''}" id="header1" data-qa-id="myaccount-invoice-number-title-text">
                            <spring:theme code="myaccount.invoices.invoiceNumber"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byInvoiceDate${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byInvoiceDate') ? sortClassPrefix : ''}" id="header2" data-qa-id="myaccount-invoice-date-title-text">
                            <spring:theme code="myaccount.invoices.invoiceDate"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byDueDate${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byDueDate') ? sortClassPrefix : ''}" id="header3" data-qa-id="myaccount-invoice-due-date-title-text">
                            <spring:theme code="myaccount.invoices.dueDate"/>
                        </th>
                        <th class="m-order-history__th " id="header4" data-qa-id="myaccount-billing-address-title-text">
                            <spring:theme code="myaccount.invoices.billingAddress"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byTotal${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byTotal') ? sortClassPrefix : ''}" id="header5" data-qa-id="myaccount-total-title-text">
                            <spring:theme code="myaccount.invoices.total"/>
                        </th>
                    </c:otherwise>
                </c:choose>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${searchPageData.results}" var="invoiceData" varStatus="loop">
                <c:if test="${empty maxItems or (not empty maxItems and loop.count le maxItems)}">
                    <tr class="">
                        <td headers="header1" class="m-order-history__td" data-qa-id="myaccount-invoice-number">
                            <a href="${orderInvoiceDetailsUrl}${ycommerce:encodeUrl(invoiceData.invoiceNumber)}">${fn:escapeXml(invoiceData.invoiceNumber)}</a>
                        </td>
                        <td headers="header2" class="m-order-history__td" data-qa-id="myaccount-invoice-invoicedate"><fmt:formatDate value="${invoiceData.invoiceDate}" dateStyle="medium" type="date"/></td>
                        <td headers="header3" class="m-order-history__td" data-qa-id="myaccount-invoice-duedate"><fmt:formatDate value="${invoiceData.dueDate}" dateStyle="medium" type="date"/></td>
                        <td headers="header4" class="m-order-history__td" data-qa-id="myaccount-invoice-billingaddress">
                            <common:address address="${invoiceData.billingAddress}" isCommaRequired="true"></common:address>
                        </td>
                        <td headers="header5" class="m-order-history__td m-text-color--black m-order-history__td__total" data-qa-id="myaccount-invoice-total">
                            <format:price priceData="${invoiceData.total}" />
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
    </div>

    <c:if test="${not hidePagination}">
        <nav:pagination top="false" msgKey="${messageKey}" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}" numberPagesShown="${numberPagesShown}"/>
    </c:if>
</c:if>

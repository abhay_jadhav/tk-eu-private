<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach items="${navigationNode.children}" var="childLevel1">
    <c:forEach items="${childLevel1.entries}" var="childlink">
        <cms:component component="${childlink.item}" evaluateRestriction="true" element="li" />
    </c:forEach>
</c:forEach>


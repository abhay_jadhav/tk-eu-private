<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/contact" %>
<template:page pageTitle="${pageTitle}" class="l-page l-page__contact">
    <div class="container">
            <h2 class="h3 l-page__title l-page__title--no-description" data-qa-id="contact-form-header">
                <spring:theme code="contact.page"/>
            </h2>
            <div class="row">
                <div class="col-md-8">
                    <div class="l-page__content__left">
                        <h3 class="h4 l-page__sub-title" data-qa-id="contact-form-header-sub">
                            <spring:theme code="contact.page.headline"/>
                        </h4>
                        <div data-qa-id="contact-form-header-statement">
                            <cms:pageSlot position="Section2A" var="feature">
                                <cms:component component="${feature}"/>
                            </cms:pageSlot>
                        </div>
                        <c:url value="/contact" var="contactUrl"/>
                        <form:form id="TkEuContactForm" method="POST" action = "${contactUrl}" commandName="tkEuContactForm" cssClass="l-page__contact__form">
                            <tk-b2b-multi-checkout:contactPageDetails/>
                            <spring:url value="/contact/data-protection" var="urlLink" />
                            <div class="checkbox checkbox--accept-terms">
                                <form:checkbox id="acceptCheck" cssClass="always-validate" path="acceptCheck" value="true" data-qa-id="contact-form-data-protection"/>
                                <label for="acceptCheck"><span data-qa-id="contact-form-data-protection-lnk"> <spring:theme code="contact.page.data.protection.statement" arguments="${urlLink}"/></span></label>
                                <span class="mandatory"><spring:theme code="form.mandatory"></spring:theme></span>
                            </div>
                            <div data-qa-id="contact-form-send-msg-btn" class="clearfix">
                                <p class="help-block help-block--required-fields pull-left"><small><spring:theme code="contact.page.required" text="* Pflichtfeld"/></small></p>
                                <button type="submit" class="btn btn-default pull-right l-page__contact__form__submit">
                                    <spring:theme code="contact.page.send.message.button"/>
                                </button>
                            </div>
                        </form:form>
                    </div>
                </div>
                <cms:pageSlot position="Section2B" var="feature" element="div" class="col-md-3 col-md-offset-1" data-qa-id="contact-form-contact-address">
                    <cms:component component="${feature}" element="div" class="l-page__content__right"/>
                </cms:pageSlot>
            </div>
    </div>
</template:page>

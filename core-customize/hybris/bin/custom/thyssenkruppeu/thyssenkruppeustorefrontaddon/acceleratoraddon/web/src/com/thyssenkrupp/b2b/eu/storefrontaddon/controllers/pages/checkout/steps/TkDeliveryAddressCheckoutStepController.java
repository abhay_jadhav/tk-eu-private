package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages.checkout.steps;

import com.thyssenkrupp.b2b.eu.core.utils.TkEuCommonUtils;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.UpdateCertificateForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.DefaultTextAreaValidator;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.TkEuNewAddressValidator;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.checkout.steps.DeliveryAddressCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.VoucherForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/checkout/multi/delivery-address")
public class TkDeliveryAddressCheckoutStepController extends DeliveryAddressCheckoutStepController {

    public static final  String REDIRECT_PREFIX  = "redirect:";
    private static final String DELIVERY_ADDRESS = "delivery-address";
    private static final Logger LOG              = LoggerFactory.getLogger(TkDeliveryAddressCheckoutStepController.class);
    private static final String VOUCHER_FORM = "voucherForm";

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade tkEuCheckoutFacade;

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "b2bCheckoutFacade")
    private CheckoutFacade checkoutfacades;

    @Resource(name = "tkEuNewAddressValidator")
    private TkEuNewAddressValidator tkEuNewAddressValidator;

    @Resource(name = "textAreaPatternValidator")
    private DefaultTextAreaValidator textAreaPatternValidator;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;
    
    @Resource(name = "voucherFacade")
    private VoucherFacade voucherFacade;

    @Override
    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateQuoteCheckoutStep
    @PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        final CartData cartData = checkoutfacades.getCheckoutCart();
        setUpDeliveryData();
        populateCommonModelAttributes(model, cartData, new TkShippingAddressForm());
        model.addAttribute("updateCertificateForm", new UpdateCertificateForm());
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_DELIVERY_ADDRESS_PAGE;
    }

    protected void populateCommonModelAttributes(final Model model, final CartData cartData, final TkShippingAddressForm tkShippingAddressForm) throws CMSItemNotFoundException {

        model.addAttribute("newDeliveryAddress", cartData.getDeliveryAddress());
        super.populateCommonModelAttributes(model, cartData, tkShippingAddressForm);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(""));

        List<AddressData> customerAddressBook = new ArrayList<>();
        customerAddressBook.addAll(userFacade.getAddressBook());
        customerAddressBook.addAll(tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress());
        model.addAttribute("deliveryAddresses", customerAddressBook);
        model.addAttribute("shippingTerms", tkEuCheckoutFacade.checkShippingTerms(cartData));
        model.addAttribute("comment", cartData.getCustomerShippingNotes());
        fillVouchers(model);
    }
    
    protected void fillVouchers(final Model model) {
        try {
        model.addAttribute("appliedVouchers", voucherFacade.getVouchersForCart());
        }catch(Exception e) {
            LOG.error("Exception occur while adding vouchersForCart in model"+e);
        }
         if (!model.containsAttribute(VOUCHER_FORM)) {
             model.addAttribute(VOUCHER_FORM, new VoucherForm());
         }
         model.addAttribute("disableUpdate", Boolean.TRUE);
    }

    @Override
    protected CartFacade getCartFacade() {
        return cartFacade;
    }

    private void setUpDeliveryData() {
        getTkEuCheckoutFacade().setDefaultPaymentInfoForCheckout();
        getCheckoutFacade().setDeliveryModeIfAvailable();
    }

    public TkEuCheckoutFacade getTkEuCheckoutFacade() {
        return tkEuCheckoutFacade;
    }

    public void setTkEuCheckoutFacade(TkEuCheckoutFacade tkEuCheckoutFacade) {
        this.tkEuCheckoutFacade = tkEuCheckoutFacade;
    }

    @RequestMapping(value = "/choose", method = RequestMethod.POST)
    @RequireHardLogIn
    public String submitDeliveryAddress(final Model model, final TkShippingAddressForm tkShippingAddress, final RedirectAttributes redirectAttributes,
        final BindingResult bindingResult) {
        final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
        try {
            final CartData cartData = checkoutfacades.getCheckoutCart();

            if (!tkShippingAddress.isAddressStatus()) {
                if (getCheckoutStep().checkIfValidationErrors(validationResults)) {
                    return getCheckoutStep().onValidation(validationResults);
                }
                tkEuCheckoutFacade.setCartPaymentType();
                final String comment = tkShippingAddress.getComment();
                if (!hasValidateShippingComment(comment, bindingResult)) {
                    prepareRedirectAttributes(model, cartData, tkShippingAddress, "checkout.shipping.comment.invalid");
                    getTkEuCheckoutFacade().setCustomerShippingNotes(comment);
                    return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_DELIVERY_ADDRESS_PAGE;
                }
                String editedComment = TkEuCommonUtils.truncateMultipleEmptyLinesToSingle(comment);
                getTkEuCheckoutFacade().setCustomerShippingNotes(editedComment);
            }
        } catch (Exception e) {
            LOG.info("TkDeliveryAddressCheckoutStepController:" + e);
        }
        return getCheckoutStep().nextStep();
    }

    private boolean hasValidateShippingComment(String comment, BindingResult bindingResult) {
        if (StringUtils.isBlank(comment)) {
            return true;
        }
        tkEuNewAddressValidator.validateShippingComment(comment, bindingResult);
        return bindingResult.hasErrors() ? false : true;
    }

    private void prepareRedirectAttributes(Model model, CartData cartData, TkShippingAddressForm tkShippingAddress, String errorMsg) throws CMSItemNotFoundException {
        setUpDeliveryData();
        populateCommonModelAttributes(model, cartData, tkShippingAddress);
        GlobalMessages.addErrorMessage(model, errorMsg);
    }

    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }
}


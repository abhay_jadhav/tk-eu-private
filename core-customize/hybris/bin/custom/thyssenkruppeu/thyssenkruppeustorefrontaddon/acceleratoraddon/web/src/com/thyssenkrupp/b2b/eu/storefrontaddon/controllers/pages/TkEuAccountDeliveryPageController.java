/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuDeliveryFacade;
import com.thyssenkrupp.b2b.eu.facades.order.data.MyAccountConsignmentDataWrapper;
import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

@Controller
@RequestMapping("/my-account")
public class TkEuAccountDeliveryPageController extends TkEuMyAccountPageController {

    protected static final String ORDER_DELIVERY_CMS_PAGE = "my-deliveries";
    protected static final String ORDER_DELIVERY_DETAILS_CMS_PAGE = "my-deliveries-details";
    protected static final String BREADCRUMBS_ATTR = "breadcrumbs";
    private static final String ORDER_DELIVERIES_URL = "/my-account/my-deliveries";
    private static final String DELIVERY_NO_PATH_VARIABLE_PATTERN = "{deliveryNo:.*}";
    private static final String CONSIGNMENT_NOT_FOUND_FOR_USER_AND_BASE_STORE = "Consignment not found for current user and B2BUnit";
    private static final Logger LOGGER = Logger.getLogger(TkEuAccountDeliveryPageController.class);

    @Resource(name = "accountBreadcrumbBuilder")
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "deliveryFacade")
    private TkEuDeliveryFacade deliveryFacade;

    @Resource(name = "enumUtils")
    private TkEnumUtils enumUtils;

    @RequestMapping(value = "/my-deliveries", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderDeliveries(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode, final Model model, final RedirectAttributes redirectModel) {

        try {
            getOrderDeliveriesSearchList(page, showMode, sortCode, model, "");
            model.addAttribute("searchCount", getDeliveryFacade().orderDeliveriesSearchListCount(""));
            model.addAttribute("currentUrl", ORDER_DELIVERIES_URL);
            storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DELIVERY_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DELIVERY_CMS_PAGE));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

            createBreadcrumb(model, ORDER_DELIVERY_CMS_PAGE);
        } catch (Exception e) {
            LOGGER.error("Error in consignment data",e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.consignment.page.not.found", null);
            return REDIRECT_PREFIX + MY_ACCOUNT;
        }
        return getViewForPage(model);
    }

    @RequestMapping(value = "/my-deliveries/search", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderDeliveriesSearch(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode, final Model model, @RequestParam(value = "searchKey", required = false) final String searchKey) throws CMSItemNotFoundException {

        getOrderDeliveriesSearchList(page, showMode, sortCode, model, searchKey);
        model.addAttribute("searchCount", getDeliveryFacade().orderDeliveriesSearchListCount(searchKey));
        model.addAttribute("currentUrl", ORDER_DELIVERIES_URL);
        model.addAttribute("searchKey", searchKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DELIVERY_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DELIVERY_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        createBreadcrumb(model, ORDER_DELIVERY_CMS_PAGE);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/my-deliveries/" + DELIVERY_NO_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderDeliveryDetail(@PathVariable("deliveryNo") final String deliveryNo, final Model model, final RedirectAttributes redirectModel, HttpSession httpSession) throws CMSItemNotFoundException {
        try {
            MyAccountConsignmentDataWrapper consignmentData = getDeliveryFacade().fetchDeliveryByCode(deliveryNo);
            if(consignmentData==null){
                throw new UnknownIdentifierException(CONSIGNMENT_NOT_FOUND_FOR_USER_AND_BASE_STORE);
            }
            model.addAttribute("consignmentDataWrapper", consignmentData);
            model.addAttribute("currentUrl", ORDER_DELIVERIES_URL);
            model.addAttribute("CSRFTokenSession",getCSRFTokenFromSession(httpSession));
            createDetailPageBreadcrumb(model, ORDER_DELIVERY_DETAILS_CMS_PAGE, deliveryNo, ORDER_DELIVERIES_URL, "myaccount.deliveries.details.breadcrumb.deliveryNumber");
        } catch (final Exception e) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.consignment.page.not.found", null);
            return REDIRECT_PREFIX + ORDER_DELIVERIES_URL;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DELIVERY_DETAILS_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DELIVERY_DETAILS_CMS_PAGE));

        return getViewForPage(model);
    }

    private void getOrderDeliveriesSearchList(final int page, final ShowMode showMode, final String sortCode, final Model model, String searchKey) {
        final PageableData pageableData = getPageableData(page, showMode, sortCode);
        final SearchPageData<ConsignmentData> searchPageData = getDeliveryFacade().orderDeliveriesSearchList(searchKey, pageableData);
        populateModel(model, searchPageData, showMode);
    }

    private String getCurrentServletPath() {
        return retrieveRequest().getServletPath();
    }

    public TkEuDeliveryFacade getDeliveryFacade() {
        return deliveryFacade;
    }

    private String getCSRFTokenFromSession(HttpSession httpSession){
        final String defaultCsrfTokenAttributeName = HttpSessionCsrfTokenRepository.class.getName().concat(".CSRF_TOKEN");
        CsrfToken sessionToken = (CsrfToken) httpSession.getAttribute(defaultCsrfTokenAttributeName);
        return sessionToken.getToken();
    }
}

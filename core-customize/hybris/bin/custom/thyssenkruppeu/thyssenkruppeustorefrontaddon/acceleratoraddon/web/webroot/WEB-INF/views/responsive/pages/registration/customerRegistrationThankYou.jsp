<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:set var="isContinueShoppingButton" value="${true}" scope="request"/>

<template:page pageTitle="${pageTitle}" class="l-page l-page__registration-confirmation">
    <div class="container l-page__content">

        <h3 class="l-page__title l-page__title--no-description">
            <spring:theme code="register.confirmation.headline" arguments="${FirstName},${LastName},${ShopName}" />
        </h3>

        <div class="l-page__sub-title"  data-qa-id="register-header">
            <cms:pageSlot position="TkEuCustomerRegMsgContentSlotName" var="feature">
                    <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>

        <cms:pageSlot position="TkEuCustomerRegContinueShopSlotName" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
    </div>
</template:page>

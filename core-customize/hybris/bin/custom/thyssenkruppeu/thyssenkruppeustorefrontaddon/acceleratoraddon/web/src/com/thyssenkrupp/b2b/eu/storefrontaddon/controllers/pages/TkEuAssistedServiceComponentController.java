package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thyssenkrupp.b2b.global.baseshop.assistedservicestorefront.controllers.cms.AssistedServiceComponentController;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

@RequestMapping(value = "/assisted-service")
public class TkEuAssistedServiceComponentController extends AssistedServiceComponentController {

    private static final Logger LOG = Logger.getLogger(TkEuAssistedServiceComponentController.class);

    @Resource(name = "assistedServiceFacade")
    private AssistedServiceFacade assistedServiceFacade;

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(value = "/autocomplete", method = RequestMethod.GET)
    @ResponseBody
    public String autocomplete(@RequestParam("customerId") final String customerId, @RequestParam("callback") final String callback) {
        final StringBuilder autocompleteResult = new StringBuilder();
        try {
            final List<CustomerData> customers = assistedServiceFacade.getSuggestedCustomerList(customerId);

            // because of jsonp callback parameter - I have to construct JSON manually (
            autocompleteResult.append(this.encodeValue(callback)).append("([");
            if (CollectionUtils.isNotEmpty(customers)) {
                for (final CustomerData customer : customers) {
                    validateCustomer(customer, autocompleteResult);
                }
                autocompleteResult.deleteCharAt(autocompleteResult.length() - 1);
            } else {
                autocompleteResult.append("{label:\"No results found\", value: \"\" }");
            }
            autocompleteResult.append("])");
        } catch (final AssistedServiceException e) {
            // just do nothing and return empty string
            LOG.debug(e.getMessage(), e);
        }
        return autocompleteResult.toString();
    }

    private void validateCustomer(final CustomerData customer, final StringBuilder autocompleteResult) {
        try {
            final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer.getUid());
            appendCustomer(autocompleteResult, customerModel, customer);

            final Collection<CartData> carts = assistedServiceFacade.getCartListForCustomer(customerModel);
            if (CollectionUtils.isNotEmpty(carts)) {
                autocompleteResult.append(", carts:[");
                final Collection<CartData> cartsForCustomer = carts;
                for (final CartData cart : cartsForCustomer) {
                    autocompleteResult.append("\"").append(cart.getCode()).append("\",");
                }
                autocompleteResult.deleteCharAt(autocompleteResult.length() - 1);
                autocompleteResult.append("]");
            }
            autocompleteResult.append("},");
        } catch (final Exception e) {
            LOG.debug("Could not find customer with uid " + customer.getUid());
        }
    }

    private void appendCustomer(StringBuilder autocompleteResult, CustomerModel customerModel, CustomerData customer) {
        try {
            autocompleteResult.append(getCustomerJSON(customerModel));
        } catch (final UnsupportedEncodingException e) {
            LOG.error("Error occured during encoding customer data: " + customer.getUid(), e);
        }
    }

}

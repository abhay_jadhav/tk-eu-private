package com.thyssenkrupp.b2b.eu.storefrontaddon.component.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.thyssenkrupp.b2b.eu.core.model.contents.components.TksTeaserContainerComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.model.TkGenericBannerComponentModel;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

public class TkEuTeaserContainerComponentRenderer<C extends TksTeaserContainerComponentModel>
       extends DefaultAddOnCMSComponentRenderer<C>    {

    private static final String MODULE_TEASER = "MODULE_TEASER";

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        List<TkGenericBannerComponentModel> tkGenericComponents = new ArrayList<TkGenericBannerComponentModel>();
        if (!component.getGenericBannerComponents().isEmpty()) {
            for (TkGenericBannerComponentModel genricComponent : component.getGenericBannerComponents()) {
                if (genricComponent.getVisible() && genricComponent.getRenderOption().getCode().startsWith(MODULE_TEASER)) {
                    tkGenericComponents.add(genricComponent);
                }
            }
            model.put("title", component.getTitle());
            model.put("titleUnderlined", component.getTitleUnderlined());
            model.put("highliting", component.getHighliting());
            model.put("tkGenericComponentList", tkGenericComponents);
            model.put("renderOption", component.getRenderOption());
            model.put("size", tkGenericComponents.size());
            }
        return model;
    }
}

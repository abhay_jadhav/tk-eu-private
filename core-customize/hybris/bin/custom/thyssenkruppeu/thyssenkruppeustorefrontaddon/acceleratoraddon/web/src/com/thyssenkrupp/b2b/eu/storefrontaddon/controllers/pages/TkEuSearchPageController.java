package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.core.model.components.TKSearchBoxComponentModel;
import com.thyssenkrupp.b2b.eu.facades.search.solrfacetsearch.impl.TkEuSolrProductSearchFacade;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.SearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

public class TkEuSearchPageController extends SearchPageController {

    private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";

    @Resource(name = "tkEuSolrProductSearchFacade")
    private TkEuSolrProductSearchFacade<ProductData> productSearchFacade;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Override
    @ResponseBody
    @RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid, @RequestParam("term") final String term) throws CMSItemNotFoundException {
        final AutocompleteResultData resultData = new AutocompleteResultData();
        final TKSearchBoxComponentModel component = cmsComponentService.getSimpleCMSComponent(componentUid);
        String encodedSearchTerm = XSSFilterUtil.filter(term);
        if (component.isDisplaySuggestions()) {
            resultData.setSuggestions(super.subList(productSearchFacade.getAutocompleteSuggestions(encodedSearchTerm), component.getMaxSuggestions()));
        }
        if (component.isDisplayCategorySuggestions()) {
            resultData.setCategorySuggestions(super.subList(productSearchFacade.getCategorySuggestions(encodedSearchTerm), component.getMaxCategorySuggestions()));
        }
        if (component.isDisplayProducts()) {
            resultData.setProducts(super.subList(productSearchFacade.textSearch(encodedSearchTerm, SearchQueryContext.SUGGESTIONS).getResults(), component.getMaxProducts()));
        }
        return resultData;
    }
}

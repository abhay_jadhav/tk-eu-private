<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hidePagination" required="false" type="String" %>
<%@ attribute name="hideSorting" required="false" type="String" %>
<%@ attribute name="searchUrl" required="true" type="String" %>
<%@ attribute name="messageKey" required="true" type="String" %>
<%@ attribute name="maxItems" required="false" type="String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/order/confirmation/" var="orderConfirmationDetailsUrl" htmlEscape="false"/>
<c:url value="/" var="homeUrl" />

<c:if test="${empty searchPageData.results}">
    <cms:pageSlot position="TkEuNoRecentOrderSlotName" var="feature">
        <cms:component component="${feature}" element="p"/>
    </cms:pageSlot>
</c:if>

<c:if test="${not empty searchPageData.results}">
    <div class="hidden">
        <nav:pagination top="true" msgKey="${messageKey}" hideRefineButton="${true}" hideFavoriteFlag = "${true}"
                                   searchKey="${searchKey}"
                                   showTopTotals="${false}"
                                   supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                   searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                   numberPagesShown="${numberPagesShown}"/>
    </div>

    <c:forEach items="${searchPageData.sorts}" var="sort">
        <c:if test="${sort.selected}">
            <c:set var="currentSort" value="${sort.code}" />
        </c:if>
    </c:forEach>
    <c:set var="orderToSortBy" value="${'Asc'}"/>
    <c:if test="${not empty currentSort}">
        <c:set var="orderToSortBy" value="${fn:contains(currentSort, 'Asc') ? 'Desc' : 'Asc'}"/>
        <c:set var="sortClassPrefix" value="${fn:contains(currentSort, 'Asc') ? '-asc' : '-desc'}" />
    </c:if>
    <div class="table-responsive">
    <table class="m-order-history table">
        <thead>
            <tr>
                <c:choose>
                    <c:when test="${hideSorting}">
                        <th id="header1" class="m-order-history__th" data-qa-id="order-history-header-order-number">
                            <spring:theme code="myaccount.confirmation.orderNumber"/>
                        </th>
                        <th id="header2" class="m-order-history__th" data-qa-id="order-history-header-po-number">
                            <spring:theme code="myaccount.confirmation.poNumber"/>
                        </th>
                        <th id="header3" class="m-order-history__th" data-qa-id="order-history-header-order-name">
                            <spring:theme code="myaccount.confirmation.orderName"/>
                        </th>
                        <th id="header5" class="m-order-history__th" data-qa-id="order-history-header-order-status">
                            <spring:theme code="myaccount.confirmation.status"/>
                        </th>
                        <th id="header4" class="m-order-history__th" data-qa-id="order-history-header-order-date">
                            <spring:theme code="myaccount.confirmation.deliveryDate"/>
                        </th>
                        <th id="header6" class="m-order-history__th m-order-history__th__total">
                            <spring:theme code="myaccount.confirmation.totalPrice"/>
                        </th>
                    </c:when>
                    <c:otherwise>
                        <th onclick="$('#sortOptions1').val('byOrderNumber${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byOrderNumber') ? sortClassPrefix : ''}" id="header1" data-qa-id="order-history-header-order-number">
                            <spring:theme code="myaccount.confirmation.orderNumber"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byOrderPO${orderToSortBy}').change()" class="hidden-xs m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byOrderPO') ? sortClassPrefix : ''}" id="header2" data-qa-id="order-history-header-po-number">
                            <spring:theme code="myaccount.confirmation.poNumber"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byOrderName${orderToSortBy}').change()" class="hidden-xs m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byOrderName') ? sortClassPrefix : ''}" id="header3" data-qa-id="order-history-header-order-name">
                            <spring:theme code="myaccount.confirmation.orderName"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byOrderStatus${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byOrderStatus') ? sortClassPrefix : ''}" id="header5" data-qa-id="order-history-header-order-status">
                            <spring:theme code="myaccount.confirmation.status"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byDate${orderToSortBy}').change()" class="m-order-history__th m-order-history__th--sort${fn:startsWith(currentSort, 'byDate') ? sortClassPrefix : ''}" id="header4" data-qa-id="order-history-header-order-date">
                            <spring:theme code="myaccount.confirmation.deliveryDate"/>
                        </th>
                        <th onclick="$('#sortOptions1').val('byOrderPrice${orderToSortBy}').change()" class="m-order-history__th m-order-history__th__total m-order-history__th--sort${fn:startsWith(currentSort, 'byOrderPrice') ? sortClassPrefix : ''}" id="header6">
                            <spring:theme code="myaccount.confirmation.totalPrice"/>
                        </th>
                    </c:otherwise>
                </c:choose>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${searchPageData.results}" var="order" varStatus="loop">
                <c:if test="${empty maxItems or (not empty maxItems and loop.count le maxItems)}">
                    <tr class="">
                        <%-- <td class="m-order-history__td hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderNumber"/></td> --%>
                        <td headers="header1" class="m-order-history__td" data-qa-id="order-history-order-number">
                            <a href="${orderConfirmationDetailsUrl}${ycommerce:encodeUrl(order.sapOrderId)}">
                                <c:choose>
                                    <c:when test="${empty order.sapOrderId}">${fn:escapeXml(order.code)}</c:when>
                                    <c:otherwise>${fn:escapeXml(order.sapOrderId)}</c:otherwise>
                                </c:choose>
                            </a>
                        </td>
                        <%-- <td class="m-order-history__td hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.poNumber"/></td> --%>
                        <td headers="header2" class="m-order-history__td hidden-xs" data-qa-id="order-history-po-number">${fn:escapeXml(order.purchaseOrderNumber)}</td>
                        <%-- <td class="m-order-history__td hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderName"/></td> --%>
                        <td headers="header3" class="m-order-history__td hidden-xs" data-qa-id="order-history-order-name">${fn:escapeXml(order.name)}</td>
                        <%-- <td class="m-order-history__td hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderStatus"/></td> --%>
                        <td headers="header5" class="m-order-history__td" data-qa-id="order-history-order-status">
                             ${order.orderStatusName}
                        </td>
                        <%-- <td class="m-order-history__td hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.datePlaced"/></td> --%>
                        <td headers="header4" class="m-order-history__td" data-qa-id="order-history-date-placed">
                            <fmt:formatDate value="${order.deliveryDate}" dateStyle="medium" type="date"/>
                        </td>
                        <%-- <td class="m-order-history__td m-order-history__td__total hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.total"/></td> --%>
                        <td headers="header6" class="m-order-history__td m-order-history__td__total" data-qa-id="order-history-order-total">${fn:escapeXml(order.total.formattedValue)}</td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
    </div>
    
    <c:if test="${not hidePagination}">
        <nav:pagination top="false" msgKey="${messageKey}" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}" numberPagesShown="${numberPagesShown}"/>
    </c:if>
</c:if>

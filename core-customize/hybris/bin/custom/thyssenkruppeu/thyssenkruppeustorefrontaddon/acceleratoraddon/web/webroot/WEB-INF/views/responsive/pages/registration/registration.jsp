<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="registration" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/registration" %>

<template:page pageTitle="${pageTitle}" class="l-page l-page__registration">
    <div class="container l-page__content">

        <h3 class="l-page__title--no-description" data-qa-id="register-header">
            <cms:pageSlot position="TkEuRegistrationHeadlineContentSlotName" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </h3>

        <div class="row">
            <div class="col-md-8">
                <div class="m-register">
                    <div class="m-register__content" data-qa-id="register-details">
                        <h4>
                        <cms:pageSlot position="TkEuRegistrationParagraphContentSlotName" var="feature">
                            <cms:component component="${feature}"/>
                        </cms:pageSlot>
                        </h4>
                    </div>
                    <spring:url value="/register/submitRegistration" var="actionUrl"/>
                    <form:form id="TkEuRegisterForm" class="m-register__form js-register-form" action="${actionUrl}" method="POST" commandName="registrationForm">

                        <div class="m-register__form__fields">
                            <registration:registrationDetails/>
                        </div>

                        <div class="m-register__form__accept-terms">
                            <spring:url value="/contact/data-protection" var="urlLink"/>
                            <label for="acceptanceCheck">
                                <div class="m-checkbox m-register__form__accept-terms__input">
                                    <input type="hidden" name="_termsCheck" value="on" />
                                    <input id="acceptanceCheck" name="acceptanceCheck" data-qa-id="register-terms-checkbox" class="m-checkbox__input sr-only" type="checkbox" />
                                    <span class="m-checkbox__label">
                                        <span class="m-checkbox__label__box"></span>
                                    </span>
                                </div>
                                <span class="m-register__form__accept-terms__text" data-qa-id="register-terms-link">
                                    <spring:theme code="register.readTermsAndConditions" arguments="${urlLink}"/>
                                </span>
                            </label>
                            <span class="mandatory">&nbsp;<spring:theme code="form.mandatory"></spring:theme></span>
                        </div>

                        <div class="m-register__form__actions" class="clearfix">
                            <p class="help-block help-block--required-fields pull-left">
                                <small>
                                    <spring:theme code="register.required.field"/>
                                </small>
                            </p>
                            <button type="submit" data-qa-id="registration-form-send-msg-btn" class="btn btn-default pull-right js-register-submit"
                                data-qa-id="register-confirm-btn">
                                <spring:theme code="register.submit"/>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </form:form>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="m-register-block">
                    <cms:pageSlot position="TkEuRegistrationBenefits" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
                </div>
            </div>
        </div>

    </div>
</template:page>

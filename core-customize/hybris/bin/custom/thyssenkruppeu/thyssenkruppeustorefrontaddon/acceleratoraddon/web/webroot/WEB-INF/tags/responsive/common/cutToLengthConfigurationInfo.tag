<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="configurationInfo" required="true" type="de.hybris.platform.commercefacades.order.data.ConfigurationInfoData" %>
<%@ attribute name="styleClass" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${configurationInfo.configurationValue eq 'true'}">
    <p class="${styleClass}" data-qa-id="cart-cutTolength-label"><small class="m-certificate"><spring:theme code="sawing.cuttingCost.plus.symbol"/><spring:theme code="product.variant.sawing.cuttingCost"/></small></p>
</c:if>

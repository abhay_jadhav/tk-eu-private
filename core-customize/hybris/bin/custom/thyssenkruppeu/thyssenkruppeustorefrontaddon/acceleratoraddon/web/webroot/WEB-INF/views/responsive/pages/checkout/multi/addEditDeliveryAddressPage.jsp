<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>

<template:page pageTitle="${pageTitle}" class="l-page l-page__checkout" isMiniHeaderFooter="true" >
    <div class="container l-page__content">
        <h3 class="l-page__title--no-description" data-qa-id="checkout-header">
            <spring:theme code="checkout.multi.secure.checkout"/>
        </h3>
        <div class="row">
            <div class="col-sm-6">
                <tk-b2b-multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}" >
                    <jsp:body>
                        <c:set var="addresses" value="${deliveryAddresses}" scope="request"/>
                        <tk-b2b-multi-checkout:shippingAddressTab disableStatus="enabled" cartData="${cartData}"/>
                    </jsp:body>
                </tk-b2b-multi-checkout:checkoutSteps>
            </div>
            <div class="col-sm-6 hidden-xs">
                <tk-b2b-multi-checkout:checkoutOrderSummary cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="true" showTaxEstimate="false" showTax="true"/>
            </div>
        </div>
    </div>
</template:page>

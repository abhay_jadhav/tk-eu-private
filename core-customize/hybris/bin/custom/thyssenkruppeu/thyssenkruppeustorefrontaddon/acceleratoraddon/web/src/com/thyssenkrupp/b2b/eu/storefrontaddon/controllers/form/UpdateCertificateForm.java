package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

public class UpdateCertificateForm {
    private int   entryNumber;
    private String certificateCode;

    public int getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(int entryNumber) {
        this.entryNumber = entryNumber;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public void setCertificateCode(String certificateCode) {
        this.certificateCode = certificateCode;
    }
}

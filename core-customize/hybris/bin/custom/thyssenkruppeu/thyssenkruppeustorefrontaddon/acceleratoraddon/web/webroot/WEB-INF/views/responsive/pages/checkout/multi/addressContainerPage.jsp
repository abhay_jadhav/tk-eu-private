<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>

<c:set scope="page"  var="shippingAddressTab"  value="shipping"/>
<c:set scope="page"  var="billingAddressTab"  value="paymentAndBilling"/>
<c:url value="/checkout/multi/delivery-address/modify-address" var="editAddressUrl" />
<form:form id="${param.formName}" class="m-checkout-form m-page-section" action="${param.action}" method="POST" commandName="tkShippingAddressForm">
    <c:set var="showAddAddressCancelButton" value="${param.disableStatus ne 'disabled' and param.status eq shippingAddressTab and (newAddressStatus eq 'true' || addAddressButton eq 'true')}" />

    <c:choose>
        <c:when test="${param.formName eq 'tkShippingAddress'}">

            <c:if test="${param.disableStatus eq 'enabled'}">

                <div class="row m-checkout-form__select-address">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="${param.selectAddress}" class="control-label m-checkout-form__label small">
                                    <spring:theme code="${param.springCode}" text="${param.springText}"></spring:theme>
                                </label>
                            </div>
                            <div class="col-md-6 text-right">
                                <spring:theme code="checkout.shipping.address.add" var="addAddress" />
                                <spring:theme code="request.availability.address.lightbox.title" var="modalTitle" />
                                <spring:theme code="request.availability.address.lightbox.subtitle" var="modalSubTitle" />
                                <c:url value="/delivery-address/choose-shipping-address" var="addAddressUrl"/>
                                <a class="js-add-address-link small m-checkout-form__link" href="${addAddressUrl}" data-qa-id="change-address" data-modal-title="${modalTitle}" data-modal-subtitle="${modalSubTitle}">${addAddress}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="${param.checkoutAddressLabel}" data-qa-id="checkout-shiptoaddresssection">
                                    <c:set var="address" value="${newDeliveryAddress}" scope="request"/>
                                    <common:address address="${address}"/>
                                </div>
                            </div>
                        </div>

                        <hr class="m-checkout-form__seperator">
                   </div>
                    <div class="col-md-6">
                        <c:if test="${not showAddAddressCancelButton and not empty param.terms}">
                            <div class="">
                                <label class="m-checkout-form__key m-checkout-form__label small control-label">
                                    <spring:theme code="${param.termsSpringCode}" text="${param.termsSpringText}"></spring:theme>
                                </label>
                                <p class="m-checkout-form__value m-checkout-form__value--has-margin-bottom m-text-color--black" data-qa-id="checkout-shipping-terms">${fn:escapeXml(param.terms)}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
                <input type="hidden" id="${param.selectedAddressCodeId}" name="selectedAddressCode"/>
                <div class="row m-checkout-form__comments">
                    <div class="col-md-12">
                        <label class="control-label" data-qa-id="checkout-comment-label">
                            <spring:theme text="${param.shippingComments}"></spring:theme>
                        </label>
                        <spring:theme code="checkout.shipping.comment.placeholder" var="shippingCommentPlaceholderText" />

                        <textarea maxlength="1000" placeholder="${shippingCommentPlaceholderText}" class="m-checkout-form__comments__text-area form-control js-checkout-shipping-comments" data-qa-id="checkout-comment-value" rows="3" name="comment" ${param.disableStatus}>${fn:escapeXml(param.comment)}</textarea>
                    </div>
                </div>
                <div class="m-checkout-form__date-expected" >
                    <div class="row">
                        <c:if test="${param.disableStatus ne 'disabled'}">
                            <div class="col-md-12 text-right">
                                 <div class="js-comment-counter m-checkout-form__comment-counter" data-qa-id="checkout-comment-counter"><span class="js-comment-counter-value"></span>/1000</div>
                            </div>
                        </c:if>
                        <div class="col-md-12" data-qa-id="checkout-atp-order-info">
                            <div class="m-text-color--black m-page-section">
                                <c:if test="${not empty param.overallAtpDate}">
                                    <div class="text-bold m-checkout-form__delivery-date">
                                        <spring:theme code="cart.availability.total.expectedDate" arguments="${param.overallAtpDate }"/>
                                    </div>
                                </c:if>
                                <spring:theme text="${param.finalDateConfirmation}"></spring:theme>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${param.disableStatus eq 'disabled'}">
                <div class="row m-checkout-form__select-address">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="${param.selectAddress}" class="control-label m-checkout-form__label small">
                                <spring:theme code="${param.springCode}" text="${param.springText}"></spring:theme>
                            </label>
                            <div class="${param.checkoutAddressLabel}" data-qa-id="checkout-shiptoaddresssection">
                                <c:set var="address" value="${newDeliveryAddress}" scope="request"/>
                                <common:address address="${address}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <c:if test="${not showAddAddressCancelButton and not empty param.terms}">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small">
                                    <spring:theme code="${param.termsSpringCode}" text="${param.termsSpringText}"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-shipping-terms">${fn:escapeXml(param.terms)}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
                <c:if test="${not empty param.comment}">
                    <div class="row m-checkout-form__select-address">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small" data-qa-id="checkout-comment-label">
                                    <spring:theme text="${param.shippingComments}"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value m-checkout-form__value--has-no-formatting" data-qa-id="checkout-comment-value">${fn:escapeXml(param.comment)}</p>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="m-checkout-form__date-expected" data-qa-id="checkout-atp-order-info">
                    <div class="row">
                        <div class="col-md-12">
                            <c:if test="${not empty param.overallAtpDate}">
                                <spring:theme code="cart.availability.total.expectedDate" arguments="${param.overallAtpDate }"/>
                                <br/>
                            </c:if>
                            <spring:theme text="${param.finalDateConfirmation}"></spring:theme>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
                <c:forEach items="${entry.product.variantValues}" var="variantValue" varStatus="loop">
                    <c:set var="variantCode" value="${variantValue.code}"/>
                    <c:if test = "${fn:contains( variantCode , 'Trade_Length')}">
                        <c:set var="dimension2"  value="${variantValue.value}"/>
                    </c:if>
                    <c:if test = "${fn:contains( variantCode , 'CUTTOLENGTH_PRODINFO')}">
                        <c:set var="dimension3"  value="${variantValue.value}"/>
                    </c:if>
                </c:forEach>
                <c:forEach var="config" items="${entry.configurationInfos}" varStatus="configLoop">
                    <spring:eval expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificate" />
                    <c:if test="${isCertificate}">
                        <c:if test="${config.configurationValue eq 'true'}">
                            <c:set var="certificate" value="${config.configurationLabel}" />
                        </c:if>
                    </c:if>
                </c:forEach>
                <c:set var="currentUrl" value="${ fn:split(requestScope['javax.servlet.forward.request_uri'], '/')}"/>${currentUrl$[fn:length(myArray)-2]}
                <c:forEach items="${currentUrl}" var="item" varStatus="status" >
                   <c:if test="${(fn:length(currentUrl) - 1) == status.count}" >
                        <c:set var="actionField"  value="${item}"/>
                   </c:if>
                </c:forEach>
                <gtm:checkoutProductGTM product="${entry.product}" quantity="${entry.quantity}" showPrice="${false}" dimension2="${dimension2}" dimension3="${dimension3}" dimension1="${certificate}" />
            </c:forEach>
            <gtm:checkoutGTM checkoutStep="1" actionField="${actionField}" showProducts="${true}"/>
        </c:when> <%--end of shipping tab --%>


        <c:when test="${param.formName eq 'tkBillingAddress'}">
            <c:if test="${param.disableStatus eq 'enabled'}">
                <div class="row m-checkout-form__select-address">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="${param.selectAddress}" class="control-label">
                                    <spring:theme code="${param.springCode}" text="${param.springText}"></spring:theme>
                                </label>
                                <span class="mandatory"><spring:theme code="form.mandatory"></spring:theme></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select data-qa-id="${param.addressDropDown}" id="${param.selectAddress}" name="selectedAddressCode" class="form-control m-checkout-form__select-address__values js-select-checkout-address" ${param.disableStatus}>
                                        <option value=""><spring:theme  code="checkout.multi.billingAddress.select"></spring:theme> </option>
                                        <c:forEach var="address" items="${addresses}">
                                            <c:choose>
                                                <c:when test="${selectedBillingAddress.id eq address.id}">
                                                    <option value="${address.id}" selected>
                                                        <c:set var="address" value="${address}" scope="request"/>
                                                        <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                                    </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${not empty primaryBillingAddress and primaryBillingAddress.id eq address.id}">
                                                            <option value="${primaryBillingAddress.id}" selected>
                                                                <c:set var="address" value="${address}" scope="request"/>
                                                                <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                                            </option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${address.id}">
                                                                <c:set var="address" value="${address}" scope="request"/>
                                                                <common:addressInlinePlaintext address="${address}" isCommaRequired="true"/>
                                                            </option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" data-qa-id="checkout-order-name-label">
                                <spring:theme text="${param.orderName}"></spring:theme>
                            </label>
                            <input type="text"  maxlength="35" class="form-control js-checkout-order-name" data-qa-id="checkout-order-name-value" name="orderName" value="${fn:escapeXml(param.billingOrderName)}" ${param.disableStatus}>
                            <span class="help-block text-right">
                                <span class="js-checkout-order-name-counter" data-qa-id="checkout-ordername-counter">
                                    <span class="js-checkout-order-name-counter-value">${fn:length(param.billingOrderName)}</span>/35
                                </span>
                            </span>
                        </div>
                        <div class="form-group form-group--no-margin-bottom">
                            <label class="control-label" data-qa-id="checkout-po-number-label">
                                <spring:theme text="${param.poNumber}"></spring:theme>
                            </label>
                            <input type="text" maxlength="35" class="form-control js-checkout-po-number" data-qa-id="checkout-po-number-value" name="poNumber" value="${fn:escapeXml(param.billingPoNumber)}" ${param.disableStatus}>
                            <span class="help-block text-right">
                                <span class="js-checkout-po-number-counter" data-qa-id="checkout-po-number-counter">
                                    <span class="js-checkout-po-number-counter-value">${fn:length(param.billingPoNumber)}</span>/35
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <c:if test="${not empty param.terms}">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small">
                                    <spring:theme code="checkout.paymentTerms"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-billing-terms">${fn:escapeXml(param.terms)}</p>
                            </div>
                        </c:if>

                        <c:if test="${not empty param.incoterms}">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small" data-qa-id="checkout-incoterms-label">
                                    <spring:theme text="${param.incotermsLabel}"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-incoterms-value">${fn:escapeXml(param.incoterms)}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
            </c:if>
            <c:if test="${param.disableStatus eq 'disabled'}">
                <div class="row m-checkout-form__select-address">
                    <div class="col-md-6">
                        <c:forEach var="address" items="${addresses}">
                            <c:if test="${selectedBillingAddress.id eq address.id}">
                                <div class="form-group">
                                    <label for="${param.selectAddress}" class="control-label m-checkout-form__label small">
                                        <spring:theme code="${param.springCode}" text="${param.springText}"></spring:theme>
                                    </label>
                                    <div data-qa-id="checkout-billtoaddresssection" class="${checkoutAddressLabel}">
                                        <c:set var="address" value="${address}" scope="request" />
                                        <common:address address="${address}"/>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                        <div class="form-group form-group--no-margin-bottom">
                            <p class="control-label m-checkout-form__label small"><spring:theme text="${param.orderName}"></spring:theme> </p>
                            <p class="m-checkout-form__value">${fn:escapeXml(param.billingOrderName)}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <c:if test="${not empty param.terms}">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small">
                                    <spring:theme code="checkout.paymentTerms"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-billing-terms">${fn:escapeXml(param.terms)}</p>
                            </div>
                        </c:if>

                        <c:if test="${not empty param.incoterms}">
                            <div class="form-group">
                                <p class="control-label m-checkout-form__label small" data-qa-id="checkout-incoterms-label">
                                    <spring:theme text="${param.incotermsLabel}"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-incoterms-value">${fn:escapeXml(param.incoterms)}</p>
                            </div>
                        </c:if>

                        <c:if test="${param.disableStatus eq 'disabled' && not empty param.billingPoNumber}">
                            <div class="form-group">
                                <p class="m-checkout-form__key" data-qa-id="checkout-po-number-label">
                                    <spring:theme text="${param.poNumber}"></spring:theme>
                                </p>
                                <p class="m-checkout-form__value" data-qa-id="checkout-po-number-value">${fn:escapeXml(param.billingPoNumber)}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
            </c:if>
            <c:set var="currentUrl" value="${ fn:split(requestScope['javax.servlet.forward.request_uri'], '/')}"/>${currentUrl$[fn:length(myArray)-2]}
            <c:forEach items="${currentUrl}" var="item" varStatus="status" >
               <c:if test="${(fn:length(currentUrl) - 1) == status.count}" >
                    <c:set var="actionField"  value="${item}"/>
               </c:if>
            </c:forEach>
            <gtm:checkoutGTM checkoutStep="2" actionField="${actionField}" showProducts="${false}"/>
        </c:when> <%--end of billing tab --%>
    </c:choose>

    <c:if test="${progressBarId eq param.status}">
        <div class="row m-checkout-form__actions">
            <div class="col-md-offset-8 col-md-4">
                <button id="" data-qa-id="${param.nextButtonId}" type="submit" class="btn btn-primary btn-block checkout-next ${param.nextButton} js-checkout-next-step">
                    <spring:theme code="checkout.multi.deliveryAddress.continue" text="Next"/>
                </button>
            </div>
        </div>
    </c:if>

</form:form>
<div class="js-shippingaddress-actions hidden">
<div class="m-popup__body__actions">
    <div class="m-popup__body__actions__item" data-qa-id="shippingaddress-popup-cancel-link">
        <a href="javascript:void(0)" class="js-cancel-button">
            <spring:theme code="myaccount.shipping.button.cancel" text="Cancel"/>
        </a>
    </div>
    <div class="m-popup__body__actions__item">
        <button data-qa-id="shippingaddress-popup-save-btn" type="button" class="btn btn-default js-add-address-submit">
        <c:choose>
            <c:when test="${addAddressPage eq true}">
                <spring:theme code="myaccount.shipping.button.add" text="Add"/>
            </c:when>
            <c:otherwise>
                <spring:theme code="myaccount.shipping.button.save" text="Save"/>
            </c:otherwise>
        </c:choose>
        </button>
    </div>
</div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class='m-popup__content'>
    <div class='m-popup__content__middle' data-qa-id="data-protection-statement">
        <cms:pageSlot position="Section1" var="feature" element="div">
            <cms:component component="${feature}" element="div" class="clearfix"/>
        </cms:pageSlot>
    </div>
    <div class="m-popup__content__bottom text-center">
        <a href="#" class="btn btn-default js-popup-close">
            <spring:theme code="registration.readTermsAndConditions.close" />
        </a>
    </div>
</div>

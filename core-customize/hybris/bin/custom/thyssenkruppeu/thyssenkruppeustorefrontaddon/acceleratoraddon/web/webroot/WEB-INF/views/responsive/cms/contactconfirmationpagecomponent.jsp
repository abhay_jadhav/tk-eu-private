<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:url value="/register/completeRegistration" var="completeRegistrationAction"/>
<div class="l-page__content">
    <h3 data-qa-id="contact-form-thank-you" class="l-page__title--no-description">${headline}</h3>
    <div class="m-page-section--no-bottom-border l-page__content__left">
        <p data-qa-id="contact-form-explanation" class="m-block m-block--no-border">
            ${explanationText}
        </p>
        <div class="l-page__contact__continue-shopping">
            <a href="${buttonUrl}" data-qa-id="contact-form-continue-shopping"><spring:theme code="action.page.continue" /></a>
        </div>
    </div>
</div>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib  prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:choose>
    <c:when test = "${renderOption eq 'DEFAULT'}">
        <a href="${url}" class="m-cmslink__default" target="${target}"><em>${linkName}</em></a>
    </c:when>
    <c:when test = "${renderOption eq 'BUTTON'}">
        <c:if test="${not empty dataIcon}">
            <a href="${url}" class="btn btn-default m-cmslink__button m-cmslink__button--has-icon" target="${target}"><span class="u-icon-tk ${dataIcon}"></span><span class="m-cmslink__button__link">${linkName}</span></a>
        </c:if>
        <c:if test="${empty dataIcon}">
            <a href="${url}" class="btn btn-default m-cmslink__button" target="${target}">${linkName}</a>
        </c:if>
    </c:when>
    <c:when test = "${renderOption eq 'BOX'}">
        <c:if test="${not empty dataIcon}">
            <a href="${url}" class="btn m-cmslink__box m-cmslink__box--has-icon" target="${target}"><span class="u-icon-tk ${dataIcon}"></span><span class="m-cmslink__box__link">${linkName}</span></a>
        </c:if>
        <c:if test="${empty dataIcon}">
            <a href="${url}" class="btn m-cmslink__box" target="${target}">${linkName}</a>
        </c:if>
    </c:when>
    <c:otherwise>
        <a href="${url}" target="${target}">${linkName}</a>
    </c:otherwise>
</c:choose>

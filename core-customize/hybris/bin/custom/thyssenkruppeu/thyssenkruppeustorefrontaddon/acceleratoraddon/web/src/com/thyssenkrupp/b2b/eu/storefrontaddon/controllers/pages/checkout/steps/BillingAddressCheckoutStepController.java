package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages.checkout.steps;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkBillingAddressForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.UpdateCertificateForm;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.VoucherForm;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/checkout/multi/billing-address")
public class BillingAddressCheckoutStepController extends AbstractCheckoutStepController {

    private static final String BILLING_ADDRESS = "billing-address";
    private static final String VOUCHER_FORM = "voucherForm";
    private static final Logger LOG                     = LoggerFactory.getLogger(BillingAddressCheckoutStepController.class);

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade tkEuCheckoutFacade;

    @Resource(name = "b2bUnitFacade")
    private B2BUnitFacade b2bUnitFacade;

    @Resource
    private SessionService sessionService;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;
    
    @Resource(name = "voucherFacade")
    private VoucherFacade voucherFacade;

    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateQuoteCheckoutStep
    @PreValidateCheckoutStep(checkoutStep = BILLING_ADDRESS)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        CartData cartData = getCheckoutFacade().getCheckoutCart();

        if (getTkEuCheckoutFacade().getNewShippingAddress().getNewShippingAddress() == null && cartData.getDeliveryAddress() == null) {

            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.deliveryAddress.notSelected");
            return REDIRECT_PREFIX + "/checkout/multi/delivery-address/choose";
        }
        setUpModelAttributes(model, cartData);
        cartData = setUpDeliveryData(cartData);
        populateCommonModelAttributes(model, cartData, new TkShippingAddressForm());
        model.addAttribute("updateCertificateForm", new UpdateCertificateForm());
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_BILLING_ADDRESS_PAGE;
    }
    protected void populateCommonModelAttributes(final Model model, final CartData cartData, final TkShippingAddressForm tkShippingAddressForm) throws CMSItemNotFoundException {

        final CartData newCartData = getTkEuCheckoutFacade().getNewShippingAddress();
        model.addAttribute("newDeliveryAddress", newCartData.getNewShippingAddress());
        model.addAttribute("deliveryAddresses", getTkEuCheckoutFacade().getB2bUnitShippingAddress());
        model.addAttribute("shippingTerms", tkEuCheckoutFacade.checkShippingTerms(cartData));
        model.addAttribute("comment", cartData.getCustomerShippingNotes());
        model.addAttribute("selectedDeliveryAddress", cartData.getDeliveryAddress());
        model.addAttribute("tkShippingAddressForm", tkShippingAddressForm);
        model.addAttribute("cartData", cartData);
        model.addAttribute("newAddressStatus", sessionService.getCurrentSession().getAttribute("newAddressStatus"));

        if(sessionService.getCurrentSession().getAttribute("newShippingAddressIsSavedStatus") != null){
            boolean status=sessionService.getCurrentSession().getAttribute("newShippingAddressIsSavedStatus");
            if(status){
                sessionService.getCurrentSession().setAttribute("newAddressStatus", true);
                sessionService.getCurrentSession().setAttribute("addAddressButton", true);
            }else{
                sessionService.getCurrentSession().setAttribute("newAddressStatus", false);
                sessionService.getCurrentSession().setAttribute("addAddressButton", false);
            }
        }
        
        fillVouchers(model);

    }
    
    protected void fillVouchers(final Model model) {
        try {
            model.addAttribute("appliedVouchers", voucherFacade.getVouchersForCart());

        } catch (Exception e) {
            LOG.error("Exception occur while adding vouchersForCart in model"+e);
        }
        if (!model.containsAttribute(VOUCHER_FORM)) {
            model.addAttribute(VOUCHER_FORM, new VoucherForm());
        }
        model.addAttribute("disableUpdate", Boolean.TRUE);

    }

    private void setUpPaymentData(final CartData cartData) {
        getTkEuCheckoutFacade().setDefaultPaymentInfoForCheckout();
        getTkEuCheckoutFacade().updateCheckoutCart(cartData);
    }
    private CartData setUpDeliveryData(CartData cartData) {
        setUpPaymentData(cartData);
        getCheckoutFacade().setDeliveryModeIfAvailable();
        return getTkEuCheckoutFacade().updateCheckoutCart(cartData);

    }
    @RequestMapping(value = "/choose", method = RequestMethod.POST)
    @RequireHardLogIn
    public String submitBillingAddress(@ModelAttribute final TkBillingAddressForm tkBillingAddressForm, final RedirectAttributes redirectAttributes) {
        final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
        if (getCheckoutStep().checkIfValidationErrors(validationResults)) {
            return getCheckoutStep().onValidation(validationResults);
        }
        if (StringUtils.isNotBlank(tkBillingAddressForm.getSelectedAddressCode())) {
            getTkEuCheckoutFacade().setBillingAddress(tkBillingAddressForm.getSelectedAddressCode());
        }
        getTkEuCheckoutFacade().setBillingDetails(tkBillingAddressForm.getOrderName(), tkBillingAddressForm.getPoNumber());
        return getCheckoutStep(BILLING_ADDRESS).nextStep();
    }

    protected CheckoutStep getCheckoutStep() {
        return getCheckoutStep(BILLING_ADDRESS);
    }

    protected void setUpModelAttributes(final Model model, final CartData cartData) throws CMSItemNotFoundException {
        setUpPaymentAndBillingData(model, cartData);
        setUpBaseModelData(model);
        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setCheckoutStepLinksForModel(model, getCheckoutStep());
    }

    private void setUpBaseModelData(final Model model) {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(""));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
    }
    private void setUpPaymentAndBillingData(final Model model, final CartData cartData) {
        final CustomerData customerData = customerFacade.getCurrentCustomer();
        model.addAttribute("billingAddresses", getTkEuCheckoutFacade().getB2bUnitBillingAddress());
        model.addAttribute("cartData", cartData);
        model.addAttribute("termsOfPayment", tkEuCheckoutFacade.checkPaymentTerms(cartData));
        model.addAttribute("incoterms", tkEuCheckoutFacade.checkIncoterms(cartData));
        model.addAttribute("poNumber", cartData.getPurchaseOrderNumber());
        model.addAttribute("orderName", cartData.getName());
        model.addAttribute("primaryBillingAddress", customerData.getPrimaryBillingAddress());
        model.addAttribute("selectedBillingAddress", cartData.getBillingAddress());
    }
    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().nextStep();
    }

    public TkEuCheckoutFacade getTkEuCheckoutFacade() {
        return tkEuCheckoutFacade;
    }

    public void setTkEuCheckoutFacade(TkEuCheckoutFacade tkEuCheckoutFacade) {
        this.tkEuCheckoutFacade = tkEuCheckoutFacade;
    }

    public B2BUnitFacade getB2bUnitFacade() {
        return b2bUnitFacade;
    }

    public void setB2bUnitFacade(B2BUnitFacade b2bUnitFacade) {
        this.b2bUnitFacade = b2bUnitFacade;
    }

    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }
}


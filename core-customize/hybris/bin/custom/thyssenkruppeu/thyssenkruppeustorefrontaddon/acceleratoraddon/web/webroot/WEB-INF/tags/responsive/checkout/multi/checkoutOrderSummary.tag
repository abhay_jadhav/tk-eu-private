<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showPlaceOrderForm" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="tk-b2b-multi-checkout" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/checkout/multi" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/gtm" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<spring:url value="/register/termsAndConditionsRegistration" var="getTermsAndConditionsRegistrationUrl"/>
<spring:url value="/cart" var="cartUrl"/>
<div class="m-checkout-tab">
    <div class="m-checkout-tab__heading">
        <div class="m-checkout-tab__heading__title">
             <spring:theme code="checkout.multi.order.summary"/>
        </div>
        <a class="m-checkout-tab__heading__edit" href="${cartUrl}" data-qa-id="checkout-edit">
            <spring:theme code="checkout.summary.edit.cart.link.text"/>
        </a>
    </div>
    <div class="m-checkout-tab__content">
        <div class="m-checkout-order">
            <tk-b2b-multi-checkout:deliveryCartItems cartData="${cartData}" />
            <%--NOTE: Accelerator inherited features. Unhide if required.
            <c:forEach items="${cartData.pickupOrderGroups}" var="groupData" varStatus="status">
                <multi-checkout:pickupCartItems cartData="${cartData}" groupData="${groupData}" showHead="true"/>
            </c:forEach>
            <order:appliedVouchers order="${cartData}"/> --%>
            <hr class="m-order-detail__subtotal__separator m-order-detail__subtotal__separator--last" />
            <div class="m-checkout-order__bottom p-0 p-l-15">
                <div class="m-checkout-order__bottom__totals">
                    <tk-b2b-multi-checkout:tkOrderTotals cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" showTax="${showTax}"/>
                </div>
                <c:if test="${showPlaceOrderForm}">
                    <div class="js-checkout-place-order">
                        <form:form action="${placeOrderUrl}" class="m-checkout-order__bottom__form" id="placeOrderForm1" commandName="placeOrderForm">
                            <label for="Terms1" class="m-checkout-order__bottom__form__checkbox control-label">
                                <div class="m-checkbox">
                                    <input type="hidden" name="_termsCheck" value="on" />
                                    <input id="Terms1" name="termsCheck" data-qa-id="checkout-accept-terms-checkbox" class="m-checkbox__input sr-only" type="checkbox" />
                                    <span class="m-checkbox__label">
                                        <span class="m-checkbox__label__box"></span>
                                    </span>
                                </div>
                                <div class="m-checkout-order__bottom__form__checkbox__text m-text-color--grey-dark">
                                    <p class="pali-copy references"> 
                                        <spring:theme code="terms.customer.agreement.text" arguments="${getTermsAndConditionsRegistrationUrl}"/> 
                                        <span class="mandatory"><spring:theme code="form.mandatory"></spring:theme></span>
                                    </p>
                                </div>
                            </label>
                            <button id="placeOrder" type="submit" class="btn btn-primary btn-block btn-place-order btn-block btn-lg checkoutSummaryButton m-checkout-order__bottom__form__submit" disabled="disabled" data-qa-id="checkout-place-order-btn">
                                <spring:theme code="checkout.summary.placeOrder"/>
                            </button>
                        </form:form>
                    </div>
                    <c:set var="currentUrl" value="${ fn:split(requestScope['javax.servlet.forward.request_uri'], '/')}"/>${currentUrl$[fn:length(myArray)-2]}
                    <c:forEach items="${currentUrl}" var="item" varStatus="status" >
                       <c:if test="${(fn:length(currentUrl) - 1) == status.count}" >
                            <c:set var="actionField"  value="${item}"/>
                       </c:if>
                    </c:forEach>
                    <gtm:checkoutGTM checkoutStep="3" actionField="${actionField}" showProducts="${false}"/>
                </c:if>
            </div>
        </div>
    </div>
</div>

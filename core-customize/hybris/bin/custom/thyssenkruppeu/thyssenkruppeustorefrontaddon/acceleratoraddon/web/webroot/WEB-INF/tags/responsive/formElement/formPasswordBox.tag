<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="idKey" required="true" type="java.lang.String"%>
<%@ attribute name="labelKey" required="true" type="java.lang.String"%>
<%@ attribute name="path" required="true" type="java.lang.String"%>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@ attribute name="labelCSS" required="false" type="java.lang.String"%>
<%@ attribute name="inputCSS" required="false" type="java.lang.String"%>
<%@ attribute name="placeholder" required="false" type="java.lang.String"%>
<%@ attribute name="errorPath" required="false" type="java.lang.String"%>
<%@ attribute name="qaAttribute" type="java.lang.String"%>
<%@ attribute name="labelQaAttribute" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<template:errorSpanField path="${path}" errorPath="${errorPath}">
    <label class="control-label ${labelCSS}" for="${idKey}" data-qa-id="${labelQaAttribute}">
        <spring:theme code="${labelKey}" />
        <c:if test="${mandatory != null && mandatory == true && not empty labelKey}">
            <span class="mandatory">&nbsp;<spring:theme code="form.mandatory"></spring:theme></span>
        </c:if>
        <c:if test="${mandatory != null && mandatory == false}">
            <span>&nbsp;<spring:theme code="login.optional" /></span>
        </c:if>
    </label>
    <spring:theme code="${placeholder}" var="placeHolderMessage" />
    <form:password cssClass="${inputCSS}" id="${idKey}" path="${path}" autocomplete="off" data-qa-id="${qaAttribute}" placeholder="${placeHolderMessage}"/>
</template:errorSpanField>

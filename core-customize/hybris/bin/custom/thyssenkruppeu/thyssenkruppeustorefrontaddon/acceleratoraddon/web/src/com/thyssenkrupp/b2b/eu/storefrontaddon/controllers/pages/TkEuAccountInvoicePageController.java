package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.facades.order.TkEuInvoiceFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.apache.log4j.Logger;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my-account")
public class TkEuAccountInvoicePageController extends TkEuMyAccountPageController {

    protected static final String ORDER_INVOICE_CMS_PAGE = "my-invoices";
    protected static final String ORDER_INVOICE_DETAILS_CMS_PAGE = "my-invoices-details";
    private static final String INVOICE_NO_PATH_VARIABLE_PATTERN = "{invoiceNo:.*}";
    private static final String ORDER_INVOICES_URL = "/my-account/my-invoices";
    private static final String INVOICE_NOT_FOUND_FOR_USER_AND_BASE_STORE = "SAP Invoice ID not found for current user and B2BUnit";
    private static final Logger LOGGER = Logger.getLogger(TkEuAccountInvoicePageController.class);

    @Resource(name = "defaultTkEuInvoiceFacade")
    private TkEuInvoiceFacade invoiceFacade;

    @RequestMapping(value = "/my-invoices", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderInvoices(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode, final Model model, final RedirectAttributes redirectModel) {

        try {
            getOrderInvoicesSearchList(page, showMode, sortCode, model, "");
            model.addAttribute("searchCount", getInvoiceFacade().invoiceSearchListCount(""));

            model.addAttribute("currentUrl", ORDER_INVOICES_URL);
            storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_INVOICE_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_INVOICE_CMS_PAGE));

            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
            createBreadcrumb(model, ORDER_INVOICE_CMS_PAGE);
        } catch (Exception e) {
            LOGGER.error("Error in invoice data",e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.invoice.page.not.found", null);
            return REDIRECT_PREFIX + MY_ACCOUNT;
        }
        return getViewForPage(model);
    }

    @RequestMapping(value = "/my-invoices/search", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderInvoicesSearch(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, @RequestParam(value = "sort", required = false) final String sortCode, final Model model, @RequestParam(value = "searchKey", required = false) final String searchKey) throws CMSItemNotFoundException {

        getOrderInvoicesSearchList(page, showMode, sortCode, model, searchKey);
        model.addAttribute("searchCount", getInvoiceFacade().invoiceSearchListCount(searchKey));
        model.addAttribute("currentUrl", ORDER_INVOICES_URL);
        model.addAttribute("searchKey", searchKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_INVOICE_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_INVOICE_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        createBreadcrumb(model, ORDER_INVOICE_CMS_PAGE);
        return getViewForPage(model);
    }

    private void getOrderInvoicesSearchList(final int page, final ShowMode showMode, final String sortCode, final Model model, String searchKey) {
        final PageableData pageableData = getPageableData(page, showMode, sortCode);
        final SearchPageData<TkEuInvoiceData> searchPageData = getInvoiceFacade().invoiceSearchList(searchKey, pageableData);
        populateModel(model, searchPageData, showMode);
    }

    @RequestMapping(value = "/my-invoices/" + INVOICE_NO_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderInvoiceDetail(@PathVariable("invoiceNo") final String invoiceNo, final Model model, final RedirectAttributes redirectModel, HttpSession httpSession) throws CMSItemNotFoundException {
        try{
        TkEuInvoiceData invoiceData = getInvoiceFacade().fetchInvoiceByCode(invoiceNo);
        if(invoiceData==null){
            throw new UnknownIdentifierException(INVOICE_NOT_FOUND_FOR_USER_AND_BASE_STORE);
        }
        model.addAttribute("invoiceData", invoiceData);
        model.addAttribute("currentUrl", ORDER_INVOICES_URL);
        model.addAttribute("CSRFTokenSession",getCSRFTokenFromSession(httpSession));
        createDetailPageBreadcrumb(model, ORDER_INVOICE_DETAILS_CMS_PAGE, invoiceNo, ORDER_INVOICES_URL, "myaccount.invoices.details.breadcrumb.invoiceNumber");
        } catch (final Exception e) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.invoice.page.not.found", null);
            return REDIRECT_PREFIX + ORDER_INVOICES_URL;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_INVOICE_DETAILS_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_INVOICE_DETAILS_CMS_PAGE));
        return getViewForPage(model);

    }

    private String getCurrentServletPath() {
        return retrieveRequest().getServletPath();
    }

    public TkEuInvoiceFacade getInvoiceFacade() {
        return invoiceFacade;
    }

    private String getCSRFTokenFromSession(HttpSession httpSession){
        final String defaultCsrfTokenAttributeName = HttpSessionCsrfTokenRepository.class.getName().concat(".CSRF_TOKEN");
        CsrfToken sessionToken = (CsrfToken) httpSession.getAttribute(defaultCsrfTokenAttributeName);
        return sessionToken.getToken();
    }

}

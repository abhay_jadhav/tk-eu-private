<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/cart" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="row">
    <div class="col-md-7">
        <h3 class="l-page__title l-page__title--no-description cart--description hidden-xs hidden-sm" data-qa-id="cart-header">
            <spring:theme code="text.cart"/>
        </h3>
        <h5 class="l-page__title l-page__title--no-description cart--description visible-xs visible-sm" data-qa-id="cart-header">
            <spring:theme code="text.cart"/>
        </h5>
        
    </div>
</div>
<c:if test="${not empty cartData.entries}">
    <div class="m-cart__actions m-cart__actions--top hidden-xs hidden-sm">
        <action:actions parentComponent="${component}"/>
    </div>
    <cart:cartItems cartData="${cartData}"/>
</c:if>
<cart:ajaxCartTopTotalSection/>

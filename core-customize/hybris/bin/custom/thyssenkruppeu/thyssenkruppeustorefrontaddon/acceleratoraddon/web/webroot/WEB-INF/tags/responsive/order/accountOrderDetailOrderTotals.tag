<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="tk-order-total" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:formatDate value="${order.overallAtpDate}" pattern="dd.MM.yyyy" var="overallAtpDate" />
<spring:htmlEscape defaultHtmlEscape="true" />
<c:if test="${not empty order}">
    <div class="m-order-detail--totals-section">
        <hr class="m-order-detail__subtotal__separator m-order-detail__subtotal__separator--last">
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="m-cart__voucher">
                    <p class="small" data-qa-id="order-confirmation-atp-order-info" >
                        <em>
                            <c:if test="${not empty overallAtpDate}">
                                <spring:theme code="cart.availability.total.expectedDate" arguments="${overallAtpDate}"/>
                                <br/>
                            </c:if>
                            <spring:theme code="text.shipping.date.info"/>
                        </em>
                    </p>
                    <p class="small"  data-qa-id="cart-estimated-price">
                        <em>
                            <spring:theme code="text.account.orderHistory.weight.price.text"/>
                        </em>
                    </p>
                    <p class="small"  data-qa-id="disclaimer-text">
                        <cms:pageSlot position="TkEuOrderDisclaimerText" var="component">
                            <cms:component component="${component}"/>
                        </cms:pageSlot>
                    </p>
                    <!--<order:appliedVouchers order="${order}" />-->
                </div>
                <!--<order:receivedPromotions order="${order}" />-->
            </div>
            <div class="col-sm-6 col-md-5">
                <tk-order-total:orderTotalsItem order="${order}" />
            </div>
        </div>
    </div>
</c:if>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="videoCSSClass" value="col-md-12" />
<c:set var="textCSSClass" value="col-md-12" />
<c:url var="url" value="" />
<c:if test="${platform eq 'YOUTUBE'}">
    <c:set var="url" value="https://www.youtube.com/embed/${videoIdentifier}" />
</c:if>

<c:choose>
    <c:when test="${rendering eq 'WITHOUT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-12" />
    </c:when>
    <c:when test="${rendering eq 'RIGHT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-6" />
        <c:set var="textCSSClass" value="col-md-6" />
    </c:when>
    <c:when test="${rendering eq 'LEFT_TEXT'}">
        <c:set var="videoCSSClass" value="col-md-6 pull-right" />
        <c:set var="textCSSClass" value="col-md-6" />
    </c:when>
    <c:otherwise>
        <c:set var="videoCSSClass" value="col-md-12" />
        <c:set var="textCSSClass" value="col-md-12" />
    </c:otherwise>
</c:choose>

<div class="video__component ${(rendering eq 'LEFT_TEXT' or rendering eq 'RIGHT_TEXT' ? 'video__component--has-text' : '')} container m-page-section--has-margin-bottom">
    <div class="row">
        <div class="${videoCSSClass}">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item js-embed-video" data-src="${url}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
        <c:if test="${rendering ne 'WITHOUT_TEXT'}">
            <div class="${textCSSClass}">${text}</div>
        </c:if>
    </div>
</div>

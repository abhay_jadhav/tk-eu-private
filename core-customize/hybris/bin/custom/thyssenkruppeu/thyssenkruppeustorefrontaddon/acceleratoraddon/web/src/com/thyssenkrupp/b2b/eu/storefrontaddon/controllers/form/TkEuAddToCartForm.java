package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;

import java.io.Serializable;

public class TkEuAddToCartForm extends AddToCartForm implements Serializable {

    private String certOption;
    private String salesUom;
    private String selectedCutToLengthRange;
    private String selectedCutToLengthRangeUnit;
    private String selectedCutToLengthTolerance;
    private String selectedTradeLength;

    public String getCertOption() {
        return certOption;
    }

    public void setCertOption(String certOption) {
        this.certOption = certOption;
    }

    public String getSalesUom() {
        return salesUom;
    }

    public void setSalesUom(String salesUom) {
        this.salesUom = salesUom;
    }

    public String getSelectedCutToLengthRange() {
        return selectedCutToLengthRange;
    }

    public void setSelectedCutToLengthRange(String selectedCutToLengthRange) {
        this.selectedCutToLengthRange = selectedCutToLengthRange;
    }

    public String getSelectedCutToLengthRangeUnit() {
        return selectedCutToLengthRangeUnit;
    }

    public void setSelectedCutToLengthRangeUnit(String selectedCutToLengthRangeUnit) {
        this.selectedCutToLengthRangeUnit = selectedCutToLengthRangeUnit;
    }

    public String getSelectedCutToLengthTolerance() {
        return selectedCutToLengthTolerance;
    }

    public void setSelectedCutToLengthTolerance(String selectedCutToLengthTolerance) {
        this.selectedCutToLengthTolerance = selectedCutToLengthTolerance;
    }

    public String getSelectedTradeLength() {
        return selectedTradeLength;
    }

    public void setSelectedTradeLength(String selectedTradeLength) {
        this.selectedTradeLength = selectedTradeLength;
    }
}

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="my-account" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/account" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="/my-account/shipping-address/add-address" var="addNewShippingAddress" />
<c:url value="/my-account/shipping-address/make-primary" var="makePrimaryAddress" />
<c:set scope="page" var="b2bUnitAddress"  value="true"/>
<div class="js-address-content">
    <div class="m-page-section m-page-section--has-address-blocks">
        <h3 class="h4 m-page-section__title" data-qa-id="myaccount-shipping-addresses-primary-address-label">
            <spring:theme code="myaccount.shipping.primary.address"/>
        </h3>
        <div class="m-page-section__content">
            <div class="row aligned-row">
                <c:if test="${not empty primaryShippingAddress}">
                   <c:forEach var="shippingAddress" items="${shippingAddress}" varStatus="loop">
                   <c:set scope="page"  var="primaryAddressCompanyName"  value="null"/>
                        <c:if test="${shippingAddress.id eq primaryShippingAddress.id}" >
                            <c:set scope="page"  var="b2bUnitAddress"  value="false"/>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <c:if test="${shippingAddress.companyName ne null}">
                                    <c:set scope="page"  var="primaryAddressCompanyName"  value="${shippingAddress.companyName}"/>
                                </c:if>
                                <my-account:myAccountAddressBlock address="${primaryShippingAddress}" editAddressUrl="/my-account/shipping-address/edit-address/${fn:escapeXml(primaryShippingAddress.id)}"
                                 deleteAddressUrl="/my-account/shipping-address/remove-address/${fn:escapeXml(primaryShippingAddress.id)}/${primaryAddressCompanyName}" isPrimaryAddress="true"/>
                            </div>
                        </c:if>
                  </c:forEach>
                  <c:if test="${b2bUnitAddress}">
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          <my-account:myAccountAddressBlock address="${primaryShippingAddress}"/>
                      </div>
                  </c:if>
                </c:if>

                <c:if test="${empty primaryShippingAddress}">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="m-block m-block--no-border js-equal-height" data-qa-id="myaccount-shipping-addresses-primary-address-value">
                            <div class="m-block__content">
                                <small class="m-text-color--black"><spring:theme code="myaccount.shipping.primaryAddressMissing"/></small>
                            </div>
                         </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>

    <div class="m-page-section m-page-section--has-address-blocks">
        <h3 class="h4 m-page-section__title" data-qa-id="myaccount-company-details-otheraddress-label">
            <spring:theme code="myaccount.company.other.address"/>
            <spring:theme code="myaccount.shipping.popup.headline" var="addPopupTitleLabel"/>
            <c:url value="/my-account/shipping-address/add-address" var="addAddressUrl"/>
            <a data-title="${addPopupTitleLabel}" href="${addAddressUrl}" class="js-add-shipping-address-link m-page-section__title__action">
                <i class="u-icon-tk u-icon-tk--plus "></i>
                <spring:theme code="myaccount.shipping.AddNew"/>
            </a>
        </h3>
        <div class="clearfix"></div>
        <div class="m-page-section__content">
            <div class="row aligned-row">
                <c:forEach var="b2bUnitShippingAddress" items="${b2bUnitShippingAddress}" varStatus="loop">
                    <c:if test="${primaryShippingAddress.id ne b2bUnitShippingAddress.id}" >
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <my-account:myAccountAddressBlock address="${b2bUnitShippingAddress}" makePrimaryUrl="/my-account/shipping-address/set-primary-address/${fn:escapeXml(b2bUnitShippingAddress.id)}" isPrimaryAddress="false"/>
                        </div>
                    </c:if>
                </c:forEach>
                <c:forEach var="shippingAddress" items="${shippingAddress}" varStatus="loop">
                <c:set scope="page"  var="addressCompanyName"  value="null"/>
                    <c:if test="${primaryShippingAddress.id ne shippingAddress.id}" >
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <c:if test="${shippingAddress.companyName ne null}">
                                <c:set scope="page"  var="addressCompanyName"  value="${shippingAddress.companyName}"/>
                            </c:if>
                            <my-account:myAccountAddressBlock address="${shippingAddress}" editAddressUrl="/my-account/shipping-address/edit-address/${fn:escapeXml(shippingAddress.id)}"
                            deleteAddressUrl="/my-account/shipping-address/remove-address/${fn:escapeXml(shippingAddress.id)}/${addressCompanyName}"
                            makePrimaryUrl="/my-account/shipping-address/set-primary-address/${fn:escapeXml(shippingAddress.id)}" isPrimaryAddress="false"/>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

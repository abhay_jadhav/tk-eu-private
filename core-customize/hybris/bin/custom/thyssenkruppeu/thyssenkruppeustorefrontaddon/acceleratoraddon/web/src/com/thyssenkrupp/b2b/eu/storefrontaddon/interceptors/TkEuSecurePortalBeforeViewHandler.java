
package com.thyssenkrupp.b2b.eu.storefrontaddon.interceptors;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.secureportaladdon.interceptors.SecurePortalBeforeViewHandler;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController.CMS_PAGE_MODEL;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController.CMS_PAGE_TITLE;

public class TkEuSecurePortalBeforeViewHandler extends SecurePortalBeforeViewHandler {

    private static final String ENABLE_REGISTRATION = "enableRegistration";
    private CMSSiteService                   cmsSiteService;
    private CMSPageService                   cmsPageService;
    private PageTitleResolver                pageTitleResolver;
    private Map<String, Map<String, String>> spViewMap;

    @Required
    public void setSpViewMap(final Map<String, Map<String, String>> spViewMap) {
        this.spViewMap = spViewMap;
    }

    @Required
    public void setPageTitleResolver(final PageTitleResolver pageTitleResolver) {
        this.pageTitleResolver = pageTitleResolver;
    }

    @Required
    public void setCmsPageService(final CMSPageService cmsPageService) {
        this.cmsPageService = cmsPageService;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    protected boolean isSiteSecured() {
        final CMSSiteModel site = cmsSiteService.getCurrentSite();
        return site.isRequiresAuthentication();
    }

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView) throws Exception {
        final ModelMap model = modelAndView.getModelMap();
        final String viewName = modelAndView.getViewName();

        model.addAttribute(ENABLE_REGISTRATION, cmsSiteService.getCurrentSite().isEnableRegistration());

        if (isSiteSecured() && spViewMap.containsKey(viewName)) {

            model.remove(CMS_PAGE_MODEL);
            final ContentPageModel scpPageModel = cmsPageService.getPageForLabelOrId(spViewMap.get(viewName).get(CMS_PAGE_ID_MAP_KEY));
            model.addAttribute(CMS_PAGE_MODEL, scpPageModel);
            model.addAttribute(CMS_PAGE_TITLE, pageTitleResolver.resolveContentPageTitle(scpPageModel.getTitle()));
            replaceModelMetaData(model, scpPageModel.getKeywords(), scpPageModel.getDescription());
            replaceSideContentSlotComponents(scpPageModel, model);

            modelAndView.setViewName(spViewMap.get(viewName).get(VIEW_NAME_MAP_KEY));
        }
    }
}

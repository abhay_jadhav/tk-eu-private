<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<%@ attribute name="formID" required="true" type="String" %>
<%@ attribute name="searchActionUrl" required="true" type="String" %>
<%@ attribute name="searchResultCount" required="false" type="Integer" %>
<%@ attribute name="searchKey" required="false" type="String" %>
<%@ attribute name="searchPlaceholder" required="false" type="String" %>
<%@ attribute name="searchBtnText" required="false" type="String" %>
<%@ attribute name="resetBtnText" required="false" type="String" %>
<%@ attribute name="resetUrl" required="false" type="String" %>
<%@ attribute name="resetUrlLabel" required="false" type="String" %>
<%@ attribute name="qaAttributeTitle" required="false" type="String" %>
<%@ attribute name="qaAttributeSearchBtn" required="false" type="String" %>
<%@ attribute name="qaAttributeTextbox" required="false" type="String" %>
<%@ attribute name="qaAttributeResetBtn" required="false" type="String" %>
<%@ attribute name="currentPage" required="false" type="String" %>

<c:choose>
    <c:when test="${searchResultCount gt 1 && empty searchKey}">
        <spring:theme code="${'myaccount.'}${currentPage}${'.headline'}" arguments="${searchResultCount}" var="titleOrder"/>
    </c:when>
    <c:when test="${searchResultCount eq 1 && empty searchKey}">
        <spring:theme code="${'myaccount.'}${currentPage}${'.headline.one'}" arguments="${searchResultCount}" var="titleOrder"/>
    </c:when>
    <c:when test="${searchResultCount gt 1 && not empty searchKey }">
        <spring:theme code="${'myaccount.'}${currentPage}${'.search.results'}" arguments="${searchResultCount},${searchKey}" var="titleOrder"/>
    </c:when>
    <c:when test="${searchResultCount eq 1 && not empty searchKey}">
        <spring:theme code="${'myaccount.'}${currentPage}${'.search.results.one'}" arguments="${searchResultCount},${searchKey}" var="titleOrder"/>
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${searchResultCount eq 0 && empty searchKey}">
                <h4 class="m-order-form__title" data-qa-id="myaccount-noresultsheading-text">
                    <spring:theme code="${'myaccount.'}${currentPage}${'.headline.noDocuments'}"/>
                </h4>
                <div class="m-order-form__description">
                    <spring:theme htmlEscape="false" code="${'myaccount.'}${currentPage}${'.headline.noDocumentsDescription'}"/>
                </div>
            </c:when>
            <c:otherwise>
                <spring:theme code="${'myaccount.'}${currentPage}${'.search.noresults'}" arguments="${searchKey}" var="titleOrder"/>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>
<c:if test="${searchResultCount ne 0 or not empty searchKey}">
    <b2b-order:searchOrderForm formID="${formID}" formActionUrl="${searchActionUrl}"
                               formTitle="${titleOrder}" searchResultCount="${searchResultCount}" searchKey="${searchKey}"
                               searchPlaceholder="${searchPlaceholder}" searchButtonText="${searchBtnText}"
                               resetButtonText="${resetBtnText}" resetUrl="${resetUrl}" resetUrlLabel="${resetUrlLabel}"
                               qaAttributeTitle="${qaAttributeTitle}" qaAttributeSearchBtn="${qaAttributeSearchBtn}" qaAttributeTextbox="${qaAttributeTextbox}"
                               qaAttributeResetBtn="${qaAttributeResetBtn}">
                               </b2b-order:searchOrderForm>
</c:if>

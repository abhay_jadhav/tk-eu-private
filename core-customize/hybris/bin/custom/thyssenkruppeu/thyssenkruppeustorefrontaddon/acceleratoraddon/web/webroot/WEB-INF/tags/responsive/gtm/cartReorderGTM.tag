<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="checkoutStep" required="false" type="java.lang.String"%>
<%@ attribute name="actionField" required="false" type="java.lang.String"%>
<%@ attribute name="showProducts" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="basePrice" value="${product.price.value}" />

<script>
    dataLayer.push( {
        'event': 'eec.add',
        'ecommerce': {
            'checkout': {
                'actionField': {
                    'list': 'Reorder'
                },
                'products': products,
            }
        }
    });
</script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
    <div class="js-product-cart-entry-price-panel m-product-detail-panel__totals">
        <c:if test="${!empty cartEntryData}">
            <div class="row">
                <div class="col-xs-6">
                    <p class="m-product-total__subtotal-label">
                        <spring:theme code="order.totals.subtotal"/>
                    </p>
                </div>
                <div class="col-xs-6">
                    <p class="m-product-total__subtotal-value text-right m-text-color--black" data-qa-id="pdp-subtotal-price">
                        <format:price priceData="${cartEntryData.subTotal}"/>
                    </p>
                </div>
            </div>
            <c:if test="${certificateProduct}">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="m-product-total__certificates-label">
                            <spring:theme code="product.product.certificates"/>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="m-product-total__certificates-value text-right m-text-color--black">
                            <format:price priceData="${cartEntryData.certificatePrice}"/>
                        </p>
                    </div>
                </div>
            </c:if>
            <c:if test="${cartEntryData.sawingCost ne null}">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="m-product-total__sawing-label">
                            <spring:theme code="product.variant.sawing.cuttingCost"/>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="m-product-total__sawing-value text-right m-text-color--black">
                            <format:price priceData="${cartEntryData.sawingCost}"/>
                        </p>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <hr class="m-product-total__seperator" />
                <div class="col-xs-6">
                    <span class="m-product-total__grand-total-label"><spring:theme code="order.totals.total"/></span>
                </div>
                <div class="col-xs-6 text-right">
                    <span class="m-product-total__grand-total-value" data-qa-id="pdp-total-price"><format:price priceData="${cartEntryData.totalPrice}"/></span>
                </div>
            </div>
        </c:if>
    </div>
</sec:authorize>

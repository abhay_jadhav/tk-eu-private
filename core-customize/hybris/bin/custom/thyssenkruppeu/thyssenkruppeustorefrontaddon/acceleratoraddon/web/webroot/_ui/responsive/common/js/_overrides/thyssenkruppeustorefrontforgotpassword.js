/*global $, ACC, document,  */

ACC.forgottenpassword = {

    _autoload: [
        "bindLink"
    ],

    bindLink: function() {
        $(document).on("click", ".js-password-forgotten", function(e) {
            e.preventDefault();

            ACC.colorbox.open(
                $(this).data("cboxTitle"), {
                    href: $(this).data("link"),
                    width: "488px",
                    className: "js-dialogueModel m-popup m-popup--dialogue",
                    top: 150,
                    onOpen: function() {
                        $('#validEmail').remove();
                    },
                    onComplete: function() {
                        $("input[name='email'").val($('#j_username').val());
                        $('form#forgottenPwdForm').ajaxForm({
                            success: function(data) {
                                if ($(data).closest('#validEmail').length) {

                                    if ($('#validEmail').length === 0) {
                                        $(".forgotten-password").replaceWith(data);
                                        ACC.colorbox.resize();
                                    }
                                } else {
                                    $("#forgottenPwdForm .control-group").replaceWith($(data).find('.control-group'));
                                    ACC.colorbox.resize();
                                }
                            }
                        });

                        $(document).on("click", "#js-reset-password-confirm", function() {
                            ACC.colorbox.close();
                        });
                    },
                    onClosed: function() {
                        ACC.colorbox.close();
                    }
                }
            );
        });
    }

};

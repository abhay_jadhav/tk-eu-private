<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty product.mediaData}">
    <c:forEach items="${product.mediaData}" var="mediaData">
        <a href="${mediaData.downloadUrl}"><i class="u-icon-tk u-icon-tk--md u-icon-tk--file-pdf"></i><span>${mediaData.documentTitle}</span></a>
    </c:forEach>
</c:if>

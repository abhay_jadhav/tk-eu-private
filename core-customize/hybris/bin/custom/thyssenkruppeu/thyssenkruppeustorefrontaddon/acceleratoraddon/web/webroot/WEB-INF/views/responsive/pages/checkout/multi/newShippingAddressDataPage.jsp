<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div data-theme="b" data-role="content">
    <div data-theme="d">
        <ul class="list-unstyled">
            <li><strong>${address.companyName}</strong></li>
            <li><strong>${address.department}</strong></li>
            <li class="m-text-color--grey-dark">${address.title}${address.firstName}&nbsp;${address.lastName}</li>
            <li class="m-text-color--grey-dark">${address.line1}</li>
            <c:if test="${not empty address.line2}">
                <li class="m-text-color--grey-dark">${address.line2}</li>
            </c:if>
            <li class="m-text-color--grey-dark">${address.postalCode}&nbsp${address.town}</li>
            <li class="m-text-color--grey-dark">${address.country.name}</li>
        </ul>
    </div>
</div>

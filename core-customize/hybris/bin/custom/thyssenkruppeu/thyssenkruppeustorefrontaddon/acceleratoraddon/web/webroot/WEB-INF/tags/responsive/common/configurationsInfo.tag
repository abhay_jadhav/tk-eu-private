<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="entry" required="true"
    type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ attribute name="certificates" required="false"
    type="java.lang.String"%>
<%@ attribute name="separatorClass" required="false" type="String"%>
<%@ taglib prefix="common"
    tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="isCutToLengthProduct" value="false" />
<c:url value="/cart/updateCertificateByAjax" var="actionUrl" />
<spring:eval expression="T(com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants).HANDLING_COSTS_FIXED" var="handlingAbs"/>
<spring:eval expression="T(com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants).HANDLING_COSTS" var="handling"/>

<c:set var="availableConfigurations" value="${0}" />
<c:forEach var="config" items="${entry.configurationInfos}"
    varStatus="configLoop">
    <spring:eval
        expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).TKTRADELENGTH"
        var="isTradeLength" />
    <spring:eval
        expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE"
        var="isCertificate" />
    <spring:eval
        expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CUTTOLENGTH_PRODINFO"
        var="isCutToLength" />

<c:choose>
    <c:when test="${isCertificate}">
        <c:set var="availableConfigurations"
            value="${availableConfigurations + 1}" />
        <c:if test="${availableConfigurations eq 1}">
            <hr class="${separatorClass}" />
        </c:if>
        <c:set var="certificate"
            value="${certificates},${config.configurationLabel}" scope="request" />
        <div class="row item__options__row">
            <div
                class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                <c:if test="${availableConfigurations lt 2 || (isCutToLengthProduct eq true && availableConfigurations lt 3)}">
                <div
                    class="m-item-group--horizontal small js-certificate-remove-icon">
                    <p class="small m-item-group__label"
                        data-qa-id="cart-configurations-label">
                        <c:if test="${availableConfigurations eq 1}">
                            <spring:theme code="basket.page.item.options" text="Options:" />
                        </c:if>
                        &nbsp;
                    </p>
                    <!-- TAG file content starts-->
                    <div class="m-item-group__value small dropdown">

                        <c:set var="certificateSelected" value="${0}" />
                        <c:forEach var="configInfo2" items="${entry.configurationInfos}">
                            <spring:eval expression="configInfo2.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="certificateType" />
                            <c:if test="${configInfo2.configurationValue && certificateType}">
                                <c:set var="certificateSelected" value="${1}" />
                            </c:if>
                        </c:forEach>

                        <c:if test="${certificateSelected == 0}">
                            <a href="#" data-qa-id="cart-certificate-label"
                                data-toggle="dropdown"
                                class="m-item-group__value small dropdown-toggle m-certificate js-m-certificate"><spring:theme
                                    code="basket.certificate.none" />:&nbsp;${configurationInfo.configurationLabel}</a>
                        </c:if>

                        <c:if test="${certificateSelected == 1}">
                            <c:forEach var="configInfos" items="${entry.configurationInfos}">
                                <spring:eval expression="configInfos.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificateType" />
                                <c:if test="${configInfos.configurationValue && isCertificateType}">
                                    <div class="cert-selected">
                                        <p class="cert-selected__title">
                                            <spring:theme code="basket.certificate" />
                                            <span class="cert-selected__title-val">${configInfos.configurationLabel}</span>
                                        </p>
                                        <a href="JavaScript:Void(0)" class="cert-selected__remove">
                                            <span class="u-icon-tk u-icon-tk--close-single"></span>
                                        </a>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>

                        <ul class="dropdown-menu pull-right">
                            <li><p class="small">
                                    <spring:theme code="basket.certificate.select" />
                                </p></li>

                            <c:forEach var="config" items="${entry.configurationInfos}"
                                varStatus="configLoop">
                                <spring:eval
                                    expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE"
                                    var="isCertificateVal" />
                                <c:if test="${isCertificateVal}">

                                    <form:form
                                        id="certificateConfigurationForm${entry.entryNumber}${configLoop.index}"
                                        modelAttribute="updateCertificateForm" method="post"
                                        action="${actionUrl}" class="js-cert-select">
                                        <li><form:radiobutton
                                                id="certificateCode${entry.entryNumber}${configLoop.index}"
                                                name="certificateCode" path="certificateCode"
                                                cssClass="js-cart-certificate-select m-certificate-config__radio"
                                                value="${config.uniqueId}" /> <label
                                            for="certificateCode${entry.entryNumber}${configLoop.index}"
                                            class="m-certificate-config__label"> <a> <span
                                                    class="m-certificate-config__label__text small">${config.configurationLabel}</span>
                                            </a> <span class="price-value">${config.priceData.formattedValue}</span>
                                                <p class="description small">${config.description}</p>
                                        </label></li>
                                        <form:hidden path="entryNumber" value="${entry.entryNumber}"
                                            id="cartEntryNumber" />
                                    </form:form>

                                </c:if>
                            </c:forEach>
                        </ul>
                    </div>

                    <!-- TAG file content ENDS-->
                    <div class="total-testimony">
                       <div class="col-md-6 total-testimony__data">
                           <div class="total-testimony__data-title">
                               <c:if test="${certificateSelected == 0}">
                                   <spring:theme code="basket.certificate.unselected" />
                               </c:if>
                               <c:if test="${certificateSelected == 1}">
                                   <spring:theme code="basket.certificate.selected" />
                               </c:if></div>
                       </div>
                       <div class="col-md-6 total-testimony__data">
                           <p class="small item__options__total text-right js-certificate-price">
                               <format:price priceData="${entry.certificatePrice}" />
                           </p>
                       </div>
                    </div>
                </div>
                </c:if>
            </div>
        </div>
    </c:when>
    <c:when test="${isTradeLength}">
        <%--Do not render here --%>
    </c:when>
    <c:when test="${isCutToLength}">
        <c:if test="${config.configurationValue eq 'true'}">
            <c:set var="availableConfigurations"
                value="${availableConfigurations + 1}" />
                <c:set var="isCutToLengthProduct" value="true" />
            <c:if test="${availableConfigurations eq 1}">
                <hr class="${separatorClass}" />
            </c:if>
            <c:set var="certificate"
                value="${certificates},${config.configurationLabel}" />
            <div class="row item__options__row">
                <div
                    class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                    <div class="m-item-group--horizontal small">
                        <p class="m-item-group__label small"
                            data-qa-id="cart-configurations-label">
                            <c:if test="${availableConfigurations eq 1}">
                                <spring:theme code="basket.page.item.options" text="Options:" />
                            </c:if>
                            &nbsp;
                        </p>
                        <common:cutToLengthConfigurationInfo configurationInfo="${config}"
                            styleClass="m-item-group__value small" />
                        <p class="item__options__total text-right small"
                            data-qa-id="cart-cutting-cost">
                            <format:price priceData="${entry.sawingCost}" />
                        </p>
                    </div>
                </div>
            </div>
        </c:if>
    </c:when>

    <c:otherwise>
        <c:if
            test="${not empty config.configurationLabel and not empty config.configurationValue}">
            <c:set var="availableConfigurations"
                value="${availableConfigurations + 1}" />
            <c:if test="${availableConfigurations eq 1}">
                <hr class="${separatorClass}" />
            </c:if>
            <div class="row item__options__row">
                <div
                    class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                    <div class="m-item-group--horizontal small">
                        <p class="m-item-group__label small"
                            data-qa-id="cart-configurations-label">
                            <c:if test="${configLoop.first}">
                                <spring:theme code="basket.page.item.options" text="Options:" />
                            </c:if>
                            &nbsp;
                        </p>
                        <p class="m-item-group__value small"
                            data-qa-id="cart-configurations-value">
                            <span class="m-certificate">${config.configurationLabel}${not empty config.configurationLabel ? ':' : ''}${config.configurationValue}</span>
                        </p>
                        <p class="small item__options__total text-right"
                            data-qa-id="cart-cofiguration-price">&nbsp;</p>
                    </div>
                </div>
            </div>
        </c:if>
    </c:otherwise>
</c:choose>
</c:forEach>
<c:if test="${not empty entry.handlingCost}">
<div class="row item__options__row cart-handling">
    <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
        <div class="m-item-group--horizontal small">
            <p class="m-item-group__label small hidden-xs hidden-sm"
                data-qa-id="cart-configurations-label">
                <c:if test="${availableConfigurations eq 0}">
                    <spring:theme code="basket.page.item.options" text="Options:" />
                </c:if>
                &nbsp;
            </p>
            <p class="m-item-group__value small">
            <c:if test="${entry.handlingCostType eq handling}">
                <span class="m-handling-text"><spring:theme
                            code="basket.page.item.handlingCost" text="+Handling" /></span>
               <i data-toggle="tooltip" title="<spring:theme code="checkout.handling.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                </c:if>
                <c:if test="${entry.handlingCostType eq handlingAbs}">
                               <span class="m-handling-text"><spring:theme
                            code="basket.page.item.absHandlingCost" text="+Abs handling" /></span>
                <i data-toggle="tooltip" title="<spring:theme code="checkout.handling.abs.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                </c:if>
            </p>
            <p class="small item__options__total text-right"
                data-qa-id="cart-cofiguration-price">
                <format:price priceData="${entry.handlingCost}" />
            </p>
        </div>
    </div>
</div>
</c:if>
<c:if test="${not empty entry.packagingCosts}">
    <c:if test="${availableConfigurations eq 0}">
        <hr class="${separatorClass}" />
    </c:if>
    <div class="row item__options__row">
        <div
            class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
            <div class="m-item-group--horizontal small">
                <p class="m-item-group__label small hidden-xs hidden-sm"
                    data-qa-id="cart-configurations-label">
                    <c:if test="${availableConfigurations eq 0}">
                        <spring:theme code="basket.page.item.options" text="Options:" />
                    </c:if>
                    &nbsp;
                </p>
                <p class="m-item-group__value small"
                    data-qa-id="cart-configurations-value">
                    <span class="m-certificate"><spring:theme
                            code="basket.page.item.packagingCost" text="+Verpackung" /></span>
                </p>
                <p class="small item__options__total text-right"
                    data-qa-id="cart-cofiguration-price">
                    <format:price priceData="${entry.packagingCosts}" />
                </p>
            </div>
        </div>
    </div>
</c:if>

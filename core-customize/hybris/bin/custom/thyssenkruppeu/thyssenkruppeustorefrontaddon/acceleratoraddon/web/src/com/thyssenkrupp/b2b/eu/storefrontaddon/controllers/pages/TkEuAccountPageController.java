package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.facades.account.TkEuB2bMyAccountFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuOrderFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.constants.ThyssenkruppeustorefrontaddonWebConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.TkEuNewAddressValidator;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.TkEuPasswordValidator;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.AccountPageController;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static de.hybris.platform.core.enums.ExportStatus.NOTEXPORTED;
import static de.hybris.platform.core.enums.OrderStatus.CREATED;

@RequestMapping("/my-account")
public class TkEuAccountPageController extends AccountPageController {

    protected static final String REDIRECT_TO_UPDATE_PASSWORD_PAGE = REDIRECT_PREFIX + "/my-account/profile";
    protected static final String PROFILE_CURRENT_PASSWORD_INVALID = "profile.currentPassword.invalid";
    protected static final String FORM_GLOBAL_ERROR                = "form.global.error";
    protected static final String ORDER_HISTORY_CMS_PAGE           = "orders";
    protected static final String ORDER_CONFIRMATION_CMS_PAGE      = "my-confirmations";
    protected static final String BREADCRUMBS_ATTR                 = "breadcrumbs";

    private static final String ORDER_CODE_PATH_VARIABLE_PATTERN    = "{orderCode:.*}";
    private static final String TEXT_ACCOUNT_PROFILE                = "text.account.profile";
    private static final String TEXT_MY_ACCOUNT                     = "text.account.yourAccount";
    private static final String PROFILE_CMS_PAGE                    = "profile";
    private static final String ORDER_DETAIL_CMS_PAGE               = "order";
    private static final String COMPANY_CMS_PAGE                    = "company-details";
    private static final String MY_ACCOUNT_DASHBOARD_URL            = "/my-account/dashboard";
    private static final String ORDER_HISTORY_PAGE_URL              = "/my-account/orders";
    private static final String ORDER_DETAIL_PAGE_URL_PREFIX        = "/my-account/order";
    private static final String TEXT_SHIPPING_ADDRESS               = "text.account.shipping.address";
    private static final String TEXT_COMPANY_DETAILS                = "text.account.company.details";
    private static final String SHIPPING_CMS_PAGE                   = "shipping-address";
    private static final String UPDATE_PASSWORD_FORM_PARAM          = "enableUpdatePasswordForm";
    private static final String REDIRECT_TO_ORDER_DETAIL_PAGE       = REDIRECT_PREFIX + "/my-account/order/";
    private static final String REDIRECT_TO_ORDER_CONFIRMATION_PAGE = REDIRECT_PREFIX + "/my-account/my-confirmations";
    private static final String REDIRECT_TO_ORDER_HISTORY_PAGE_URL  = REDIRECT_PREFIX + "/my-account/orders?init=true";
    private static final String ORDER_CONFIRMATION_DETAIL_CMSPAGE   = "orderConfirmationDetails";
    private static final String ORDER_CONFIRMATION_URL              = "/my-account/my-confirmations";

    private static final String REDIRECT_TO_COMPANY_ADDRESS_PAGE          = REDIRECT_PREFIX + "/my-account/company-details";
    private static final String REDIRECT_TO_SHIPPING_ADDRESS_PAGE         = REDIRECT_PREFIX + "/my-account/shipping-address";
    private static final String REDIRECT_TO_EDIT_SHIPPING_ADDRESS_PAGE    = REDIRECT_PREFIX + "/my-account/shipping-address/edit-address/";
    private static final String ADDRESS_CODE_DELECT_PATH_VARIABLE_PATTERN = "{addressCode:.*}/{companyName:.*}/{primaryAddress:.*}";
    private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN        = "{addressCode:.*}/{companyName:.*}";
    private static final String ADDRESS_CODE_EDIT_PATH_VARIABLE_PATTERN   = "{addressCode:.*}";
    private static final String MY_ACCOUNT                                = REDIRECT_PREFIX + "/my-account/dashboard?init=true";

    private static final Logger LOGGER = Logger.getLogger(TkEuAccountPageController.class);

    @Resource(name = "orderFacade")
    protected TkEuOrderFacade orderFacade;

    @Resource(name = "accountBreadcrumbBuilder")
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "baseStoreService")
    protected BaseStoreService baseStoreService;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Resource(name = "b2bUnitFacade")
    private B2BUnitFacade b2bUnitFacade;

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade tkEuCheckoutFacade;

    @Resource(name = "tkPasswordValidator")
    private TkEuPasswordValidator tkPasswordValidator;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "b2bOrderFacade")
    private B2BOrderFacade b2bOrderFacade;

    @Resource(name = "tkEuNewAddressValidator")
    private TkEuNewAddressValidator tkEuNewAddressValidator;

    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Resource(name = "defaultTkEuB2bMyAccountFacade")
    private TkEuB2bMyAccountFacade tkEuB2bMyAccountFacade;

    @Resource(name = "b2bCheckoutFacade")
    private CheckoutFacade checkoutfacades;

    @Resource(name = "addressDataUtil")
    private AddressDataUtil addressDataUtil;

    @Resource(name = "productService")
    private ProductService productService;

    @Override
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orders(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
        @RequestParam(value = "sort", required = false) final String sortCode, final Model model, final RedirectAttributes redirectModel) {
        try {
            getOrderData(page, showMode, sortCode, model);
            model.addAttribute("searchCount", orderFacade.findOrderAcknowledgementListCount(""));
            model.addAttribute("currentUrl", getCurrentServletPath());
            storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

            createBreadcrumb(model, ORDER_HISTORY_CMS_PAGE);
        } catch (Exception e) {
            LOGGER.error("Error in Order data",e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.order.error.page.not.found", null);
            return MY_ACCOUNT;
        }
        return getViewForPage(model);
    }

    @RequestMapping(value = "/orders/acknowledgement/search", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderAcknowledgementSearch(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
        @RequestParam(value = "sort", required = false) final String sortCode, final Model model, @RequestParam(value = "searchKey", required = false) final String searchKey) throws CMSItemNotFoundException {
        getOrderAcknowledgementSearchList(page, showMode, sortCode, model, searchKey);
        model.addAttribute("searchCount", orderFacade.findOrderAcknowledgementListCount(searchKey));
        model.addAttribute("currentUrl", ORDER_HISTORY_PAGE_URL);
        model.addAttribute("searchKey", searchKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        createBreadcrumb(model, ORDER_HISTORY_CMS_PAGE);
        return getViewForPage(model);
    }

    private void createBreadcrumbConfirmation(Model model, String cmsPage, String code) throws CMSItemNotFoundException {

        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        String[] strArray = { code };
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage("myaccount.landingpage.title", null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getContentPageForLabelOrId(cmsPage).getTitle(), null));
        breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("myaccount.confirmation.details.breadcrumb.orderNumber", strArray, getI18nService().getCurrentLocale()), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    private void createBreadcrumb(Model model, String cmsPage) throws CMSItemNotFoundException {

        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage("myaccount.landingpage.title", null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getContentPageForLabelOrId(cmsPage).getTitle(), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    private void getOrderAcknowledgementSearchList(final int page, final ShowMode showMode, final String sortCode, final Model model, String searchKey) {
        final PageableData pageableData = getPageableData(page, showMode, sortCode);
        final SearchPageData<OrderHistoryData> searchPageData = orderFacade.orderAcknowledgementSearchList(searchKey, pageableData);
        setOrderStatusName(searchPageData);
        populateModel(model, searchPageData, showMode);
    }

    private void getOrderData(final int page, final ShowMode showMode, final String sortCode, final Model model) {
        final PageableData pageableData = getPageableData(page, showMode, sortCode);
        final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
        setOrderStatusName(searchPageData);
        populateModel(model, searchPageData, showMode);
    }

    private PageableData getPageableData(final int page, final ShowMode showMode, final String sortCode) {
        initSessionStoredInfo();

        String storedSortCode = loadStoredSortCode(sortCode);
        if (StringUtils.isEmpty(storedSortCode)) {
            storedSortCode = sortCode;
        }

        int storedPage = page;
        Integer storedPageNumber = loadStoredPage();
        if (storedPageNumber != null) {
            storedPage = storedPageNumber.intValue();
        }

        final PageableData pageableData = createPageableData(storedPage, loadPageSize(), storedSortCode, showMode);
        return pageableData;
    }

    @RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String order(@PathVariable("orderCode") final String orderCode, final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        try {
            final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
            if (userService.getCurrentUser() != null) {
                checkCancelOrder(model, orderDetails);
            }
            setOrderStatusName(orderDetails);
            final List<OrderEntryData> orderEntries = orderDetails.getUnconsignedEntries();
            for (final OrderEntryData orderEntry : orderEntries) {
                try {
                    if (StringUtils.isNotEmpty(orderEntry.getProduct().getBaseProduct())) {
                        productService.getProductForCode(orderEntry.getProduct().getBaseProduct());
                    }
                } catch (final UnknownIdentifierException e) {
                    ProductData productdata = orderEntry.getProduct();
                    productdata.setPurchasable(false);
                }
            }
            model.addAttribute("orderData", orderDetails);
            String sapOrderId = null;
            if (OrderStatus.CONFIRMED == orderDetails.getStatus()) {
                sapOrderId = orderFacade.getSapOrderIdFromConfirmedOrder(orderCode);
            } else if (OrderStatus.CANCELLED == orderDetails.getStatus()) {
                sapOrderId = orderFacade.getSapOrderIdFromCancelledOrder(orderCode);
            }
            model.addAttribute("orderConfirmationId", sapOrderId);
            model.addAttribute("currentUrl", ORDER_DETAIL_PAGE_URL_PREFIX);
            final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
            breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage("myaccount.landingpage.title", null, getI18nService().getCurrentLocale()), null));
            breadcrumbs.add(new Breadcrumb(ORDER_HISTORY_PAGE_URL, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE).getTitle(), null));
            breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("myaccount.acknowledge.details.breadcrumb.orderNumber", new Object[] { orderDetails.getCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));
            model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
        } catch (final Exception e) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.order.error.page.not.found", null);
            return REDIRECT_TO_ORDER_HISTORY_PAGE_URL;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
        return getViewForPage(model);
    }

    @RequestMapping(value = "/order/confirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderConfirmationDetail(@PathVariable("orderCode") final String sapOrderId, final Model model, final RedirectAttributes redirectModel, HttpSession httpSession) throws CMSItemNotFoundException {
        try {
            final OrderData orderDetails = orderFacade.getOrderConfirmationDetailsForCode(sapOrderId);
            model.addAttribute("CSRFTokenSession", getCSRFTokenFromSession(httpSession));
            model.addAttribute("orderData", orderDetails);
            setOrderStatusName(orderDetails);
            model.addAttribute("currentUrl", ORDER_CONFIRMATION_URL);
            createBreadcrumbConfirmation(model, ORDER_CONFIRMATION_DETAIL_CMSPAGE, sapOrderId);
        } catch (final UnknownIdentifierException e) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
            return REDIRECT_TO_ORDER_CONFIRMATION_PAGE;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_DETAIL_CMSPAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_DETAIL_CMSPAGE));

        return getViewForPage(model);
    }

    private void checkCancelOrder(final Model model, final OrderData orderDetails) {
        if (orderDetails.getUser().getUid() == userService.getCurrentUser().getUid() && orderDetails.getStatus() == CREATED && (orderDetails.getExportStatus() == NOTEXPORTED || orderDetails.getExportStatus() == null)) {
            model.addAttribute("currentUserOrder", true);
        }
    }

    @RequestMapping(value = "/order/cancel", method = RequestMethod.POST)
    @RequireHardLogIn
    public String orderCancel(@RequestParam(value = "orderCode", required = false) final String orderCode, final RedirectAttributes redirectModel, final HttpServletRequest request) throws CMSItemNotFoundException {
        try {

            final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
            if (orderDetails.getExportStatus() == NOTEXPORTED || orderDetails.getExportStatus() == null) {
                b2bOrderFacade.cancelOrder(orderCode, "");
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "order.cancel.success.message");
            } else {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "order.cancel.export.message");
            }
        } catch (Exception ex) {
            LOGGER.error("Error occuring while cancel the order ", ex);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "order.cancel.error.message");
        }
        return REDIRECT_TO_ORDER_DETAIL_PAGE + orderCode;
    }

    @Override
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    @RequireHardLogIn
    public String profile(final Model model) throws CMSItemNotFoundException {

        if (openUpdatePasswordForm()) {
            UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();
            model.addAttribute("updatePasswordForm", updatePasswordForm);
            model.addAttribute(UPDATE_PASSWORD_FORM_PARAM, true);
        }
        updateProfileAttributesToModel(model);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    @RequireHardLogIn
    public String resetPassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult, final Model model,
        final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        getTkPasswordValidator().validate(updatePasswordForm, bindingResult);
        if (!bindingResult.hasErrors()) {
            if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword())) {
                try {
                    customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
                } catch (final PasswordMismatchException localException) {
                    bindingResult.rejectValue("currentPassword", PROFILE_CURRENT_PASSWORD_INVALID, new Object[] {}, PROFILE_CURRENT_PASSWORD_INVALID);
                }
            } else {
                bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {}, "validation.checkPwd.equals");
            }
        }

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
            updateProfileAttributesToModel(model);
            model.addAttribute(UPDATE_PASSWORD_FORM_PARAM, true);
            return getViewForPage(model);
        } else {
            model.addAttribute(UPDATE_PASSWORD_FORM_PARAM, false);
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "text.account.confirmation.password.updated", null);
            return REDIRECT_TO_UPDATE_PASSWORD_PAGE;
        }
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    @RequireHardLogIn
    public String dashBoard(final Model model) throws CMSItemNotFoundException {
        final List<TitleData> titles = userFacade.getTitles();
        final CustomerData customerData = customerFacade.getCurrentCustomer();
        if (customerData.getTitleCode() != null) {
            model.addAttribute("title", findTitleForCode(titles, customerData.getTitleCode()));
        }
        model.addAttribute("customerData", customerData);
        model.addAttribute("billingAddresses", tkEuCheckoutFacade.getCustomerB2bUnitBillingAddress());
        model.addAttribute("shippingAddress", tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress());
        getOrderData(0, ShowMode.Page, null, model);
        storeCmsPageInModel(model, getContentPageForLabelOrId("my-account-dashboard"));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId("my-account-dashboard"));
        model.addAttribute("currentUrl", getCurrentServletPath());
        model.addAttribute(BREADCRUMBS_ATTR, buildMyAccountBreadcrumbs(null));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        model.addAttribute("primaryBillingAddress", customerData.getPrimaryBillingAddress());
        model.addAttribute("primaryShippingAddress", customerData.getPrimaryShippingAddress());
        return getViewForPage(model);
    }

    @RequestMapping(value = "/company-details", method = RequestMethod.GET)
    @RequireHardLogIn
    public String company(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("companyName", b2bUnitFacade.getParentUnit().getName());
        model.addAttribute("companyAddress", tkEuCheckoutFacade.getB2bUnitCompanyAddress());
        model.addAttribute("termsOfPayment", getEnumUtils().getNameFromEnumCode(b2bUnitFacade.getParentUnit().getPaymentTerms()));
        model.addAttribute("incoterms", getEnumUtils().getNameFromEnumCode(b2bUnitFacade.getParentUnit().getIncoTerms()));
        model.addAttribute("shippingTerms", getEnumUtils().getNameFromEnumCode(b2bUnitFacade.getParentUnit().getShippingCondition()));
        model.addAttribute("billingAddresses", tkEuCheckoutFacade.getCustomerB2bUnitBillingAddress());
        model.addAttribute("shippingAddress", tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress());
        model.addAttribute("contactAddress", tkEuCheckoutFacade.getB2bUnitContactAddress());
        storeCmsPageInModel(model, getContentPageForLabelOrId(COMPANY_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(COMPANY_CMS_PAGE));
        model.addAttribute("currentUrl", getCurrentServletPath());
        model.addAttribute(BREADCRUMBS_ATTR, buildMyAccountBreadcrumbs(TEXT_COMPANY_DETAILS));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        final CustomerData customerData = customerFacade.getCurrentCustomer();
        model.addAttribute("primaryBillingAddress", customerData.getPrimaryBillingAddress());

        return getViewForPage(model);
    }

    @RequestMapping(value = "/company-details/set-primary-address/" + ADDRESS_CODE_EDIT_PATH_VARIABLE_PATTERN, method = { RequestMethod.GET, RequestMethod.POST })
    @RequireHardLogIn
    public String setPrimaryBillingAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel) {

        final List<AddressData> b2bUnitBillingAddress = tkEuCheckoutFacade.getCustomerB2bUnitBillingAddress();
        for (final AddressData addressData : b2bUnitBillingAddress) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)) {
                tkEuB2bMyAccountFacade.setB2BAddressAsPrimaryBillingAddress(addressData);
            }
        }
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "myaccount.company.billing.message.confirmation");
        return REDIRECT_TO_COMPANY_ADDRESS_PAGE;
    }

    //SHIPPING ADDRESS START
    @RequestMapping(value = "/shipping-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String shippingaddress(final Model model) throws CMSItemNotFoundException {

        model.addAttribute("shippingAddress", userFacade.getAddressBook());
        model.addAttribute("b2bUnitShippingAddress", tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress());
        final CustomerData customerData = customerFacade.getCurrentCustomer();
        model.addAttribute("primaryShippingAddress", customerData.getPrimaryShippingAddress());

        storeCmsPageInModel(model, getContentPageForLabelOrId(SHIPPING_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SHIPPING_CMS_PAGE));
        model.addAttribute("currentUrl", getCurrentServletPath());
        createBreadcrumbAddress(model, SHIPPING_CMS_PAGE);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        return getViewForPage(model);
    }

    private void createBreadcrumbAddress(Model model, String cmsPage) throws CMSItemNotFoundException {

        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_DASHBOARD_URL, getMessageSource().getMessage("myaccount.landingpage.title", null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getContentPageForLabelOrId(cmsPage).getTitle(), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
    }

    @RequestMapping(value = "/shipping-address/add-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String addNewShippingaddress(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        model.addAttribute("tkShippingAddressForm", new TkShippingAddressForm());
        model.addAttribute("country", checkoutfacades.getDeliveryCountries());
        model.addAttribute("title", userFacade.getTitles());
        model.addAttribute("addAddressPage", true);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_ACCOUNT_ADD_ADDRESS_POPUP;
    }

    @RequestMapping(value = "/shipping-address/add-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String addNewShippingaddress(final Model model, final TkShippingAddressForm myAccountShippingAddressForm, final BindingResult bindingResult, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

        model.addAttribute("tkShippingAddressForm", myAccountShippingAddressForm);
        if (validateNewShippingAddress(model, myAccountShippingAddressForm, bindingResult)) {

            model.addAttribute("tkShippingAddressForm", myAccountShippingAddressForm);
            model.addAttribute("country", checkoutfacades.getDeliveryCountries());
            model.addAttribute("title", userFacade.getTitles());
            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_ACCOUNT_ADD_ADDRESS_POPUP;
        } else {

            setNewShippingaddress(myAccountShippingAddressForm);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "myaccount.shipping.message.saved", new Object[] { myAccountShippingAddressForm.getCompanyName() });
        }
        return REDIRECT_TO_SHIPPING_ADDRESS_PAGE;
    }

    @RequestMapping(value = "/shipping-address/edit-address/" + ADDRESS_CODE_EDIT_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String editMyAccountShippingAddress(@PathVariable("addressCode") final String addressCode, final TkShippingAddressForm myAccountShippingAddressForm,
        final Model model) throws CMSItemNotFoundException {

        model.addAttribute("country", checkoutfacades.getDeliveryCountries());
        model.addAttribute("title", userFacade.getTitles());
        model.addAttribute("addAddressPage", false);
        final List<AddressData> addressBook = userFacade.getAddressBook();

        for (final AddressData addressData : addressBook) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)) {

                myAccountShippingAddressForm.setCompanyName(addressData.getCompanyName());
                myAccountShippingAddressForm.setDepartmentName(addressData.getDepartment());
                myAccountShippingAddressForm.setFirstName(addressData.getFirstName());
                myAccountShippingAddressForm.setLastName(addressData.getLastName());
                myAccountShippingAddressForm.setTitleCode(addressData.getTitleCode());
                myAccountShippingAddressForm.setPostcode(addressData.getPostalCode());
                myAccountShippingAddressForm.setTownCity(addressData.getTown());
                myAccountShippingAddressForm.setLine1(addressData.getLine1());
                myAccountShippingAddressForm.setLine2(addressData.getLine2());
                model.addAttribute("addressData", addressData);
                model.addAttribute("addressId", addressCode);
                if (customerFacade.getCurrentCustomer().getPrimaryShippingAddress() != null) {
                    if (customerFacade.getCurrentCustomer().getPrimaryShippingAddress().getId().equals(addressCode)) {
                        myAccountShippingAddressForm.setMakePrimaryCheckBox(true);
                    }
                }
            }
        }
        model.addAttribute("myAccountShippingAddressForm", myAccountShippingAddressForm);

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_ACCOUNT_ADD_ADDRESS_POPUP;
    }

    @RequestMapping(value = "/shipping-address/edit-address/" + ADDRESS_CODE_EDIT_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
    @RequireHardLogIn
    public String editMyAccountShippingAddress(@PathVariable("addressCode") final String addressCode, final Model model, TkShippingAddressForm myAccountShippingAddressForm,
        final RedirectAttributes redirectModel, final BindingResult bindingResult) throws CMSItemNotFoundException {

        if (validateNewShippingAddress(model, myAccountShippingAddressForm, bindingResult)) {
            model.addAttribute("tkShippingAddressForm", myAccountShippingAddressForm);
            model.addAttribute("country", checkoutfacades.getDeliveryCountries());
            model.addAttribute("title", userFacade.getTitles());
            model.addAttribute("addAddressPage", false);
            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_ACCOUNT_ADD_ADDRESS_POPUP;
        }
        final AddressData newAddress = addressDataUtil.convertToVisibleAddressData(myAccountShippingAddressForm);
        newAddress.setId(addressCode);
        newAddress.setCompanyName(myAccountShippingAddressForm.getCompanyName());
        newAddress.setDepartment(myAccountShippingAddressForm.getDepartmentName());
        userFacade.editAddress(newAddress);
        //Check address as primary address or not
        tkEuB2bMyAccountFacade.checkPrimaryShippingAddress(myAccountShippingAddressForm.isMakePrimaryCheckBox(), newAddress);
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "myaccount.shipping.message.saved", new Object[] { newAddress.getCompanyName() });
        return REDIRECT_TO_SHIPPING_ADDRESS_PAGE;
    }

    @RequestMapping(value = "/shipping-address/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = { RequestMethod.GET, RequestMethod.POST })
    @RequireHardLogIn
    public String deleteShippingAddress(@PathVariable("addressCode") final String addressCode, @PathVariable("companyName") final String companyName, TkShippingAddressForm myAccountShippingAddressForm, final RedirectAttributes redirectModel) {

        final AddressData addressData = new AddressData();
        addressData.setId(addressCode);
        userFacade.removeAddress(addressData);
        if (myAccountShippingAddressForm.isAddressStatus()) {

            tkEuB2bMyAccountFacade.removePrimaryShippingAddress();
        }
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "myaccount.shipping.message.delete", new Object[] { companyName });
        return REDIRECT_TO_SHIPPING_ADDRESS_PAGE;
    }

    @RequestMapping(value = "/shipping-address/set-primary-address/" + ADDRESS_CODE_EDIT_PATH_VARIABLE_PATTERN, method = { RequestMethod.GET, RequestMethod.POST })
    @RequireHardLogIn
    public String setPrimaryAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel) {

        //Set Customer address as primary address
        boolean addressStatus = true;
        final List<AddressData> customerAddressBook = userFacade.getAddressBook();
        for (final AddressData addressData : customerAddressBook) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)) {
                addressStatus = false;
                tkEuB2bMyAccountFacade.setCustomerAddressAsPrimaryAddress(addressData);
            }
        }
        //Set B2B unit address as primary address
        if (addressStatus) {
            setB2BAddressAsPrimaryAddress(addressCode);
        }
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "myaccount.shipping.message.confirmation");
        return REDIRECT_TO_SHIPPING_ADDRESS_PAGE;
    }

    private void setB2BAddressAsPrimaryAddress(@PathVariable("addressCode") String addressCode) {
        final List<AddressData> b2bUnitAddressBook = tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress();
        for (final AddressData addressData : b2bUnitAddressBook) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)) {
                tkEuB2bMyAccountFacade.setB2BAddressAsPrimaryAddress(addressData);
            }
        }
    }

    private boolean validateNewShippingAddress(Model model, TkShippingAddressForm myAccountShippingAddressForm, BindingResult bindingResult) throws CMSItemNotFoundException {
        tkEuNewAddressValidator.validate(myAccountShippingAddressForm, bindingResult);
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            return true;
        }
        return false;
    }

    public void setNewShippingaddress(TkShippingAddressForm tkShippingAddressForm) {

        final AddressModel addressModel = new AddressModel();
        addressModel.setCompany(tkShippingAddressForm.getCompanyName());
        addressModel.setDepartment(tkShippingAddressForm.getDepartmentName());
        if (StringUtils.isNotBlank(tkShippingAddressForm.getTitleCode())) {
            setTitleCode(tkShippingAddressForm, addressModel);
        }
        addressModel.setFirstname(tkShippingAddressForm.getFirstName());
        addressModel.setLastname(tkShippingAddressForm.getLastName());
        addressModel.setStreetname(tkShippingAddressForm.getLine1());
        addressModel.setStreetnumber(tkShippingAddressForm.getLine2());
        addressModel.setPostalcode(tkShippingAddressForm.getPostcode());
        addressModel.setTown(tkShippingAddressForm.getTownCity());
        if (tkShippingAddressForm.getCountryIso() != null) {

            setCountryCode(tkShippingAddressForm, addressModel);
        }
        addressModel.setShippingAddress(true);
        addressModel.setUnloadingAddress(true);
        addressModel.setBillingAddress(false);
        addressModel.setContactAddress(false);
        addressModel.setVisibleInAddressBook(true);
        addressModel.setDuplicate(false);
        addressModel.setOwner(userService.getCurrentUser());

        tkEuB2bMyAccountFacade.saveCustomerShippingAddress(addressModel);
        if (tkShippingAddressForm.isMakePrimaryCheckBox()) {

            tkEuB2bMyAccountFacade.savePrimaryShippingAddress(addressModel);
        }
    }

    private void setTitleCode(TkShippingAddressForm tkShippingAddressForm, AddressModel addressModel) {
        final TitleModel title = new TitleModel();
        title.setCode(tkShippingAddressForm.getTitleCode());
        addressModel.setTitle(flexibleSearchService.getModelByExample(title));
    }

    private void setCountryCode(TkShippingAddressForm tkShippingAddressForm, AddressModel addressModel) {
        final String isocode = tkShippingAddressForm.getCountryIso();
        addressModel.setCountry(commonI18NService.getCountry(isocode));
    }

    @RequestMapping(value = "/my-confirmations", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderConfirmations(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
        @RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException {

        getOrderConfirmationSearchList(page, showMode, sortCode, model, "");
        model.addAttribute("searchCount", orderFacade.findOrderConfirmationListCount(""));
        model.addAttribute("currentUrl", getCurrentServletPath());
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        createBreadcrumb(model, ORDER_CONFIRMATION_CMS_PAGE);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/my-confirmations/search", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderConfirmationSearch(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
        @RequestParam(value = "sort", required = false) final String sortCode, final Model model,
        @RequestParam(value = "searchKey", required = false) final String searchKey) throws CMSItemNotFoundException {

        getOrderConfirmationSearchList(page, showMode, sortCode, model, searchKey);
        model.addAttribute("searchCount", orderFacade.findOrderConfirmationListCount(searchKey));
        model.addAttribute("currentUrl", ORDER_CONFIRMATION_URL);
        model.addAttribute("searchKey", searchKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_CONFIRMATION_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        createBreadcrumb(model, ORDER_CONFIRMATION_CMS_PAGE);
        return getViewForPage(model);
    }

    private void getOrderConfirmationSearchList(final int page, final ShowMode showMode, final String sortCode, final Model model, String searchKey) {
        final PageableData pageableData = getPageableData(page, showMode, sortCode);
        final SearchPageData<OrderHistoryData> searchPageData = orderFacade.orderConfirmationSearchList(searchKey, pageableData);
        setOrderStatusName(searchPageData);
        populateModel(model, searchPageData, showMode);
    }

    private Set<String> getUniquePhoneNumbersOrFax(List<AddressData> addressBooks, String phoneNoOrFax) {
        Set<String> phoneNumbersOrFax = new HashSet();
        if (CollectionUtils.isNotEmpty(addressBooks)) {
            for (AddressData addressBook : addressBooks) {
                if ("phoneNumbers".equals(phoneNoOrFax)) {
                    phoneNumbersOrFax.add(addressBook.getCellPhone());
                    phoneNumbersOrFax.add(addressBook.getPhone());
                    phoneNumbersOrFax.add(addressBook.getPhone2());
                } else {
                    phoneNumbersOrFax.add(addressBook.getFax());
                }
            }
        }
        return phoneNumbersOrFax;
    }

    private List<Breadcrumb> buildMyAccountBreadcrumbs(String breadcrumbsValue) {
        final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("/my-account/dashboard", getMessageSource().getMessage(TEXT_MY_ACCOUNT, null, getI18nService().getCurrentLocale()), null));
        if (breadcrumbsValue != null) {
            breadcrumbs.add(new Breadcrumb("", getMessageSource().getMessage(breadcrumbsValue, null, getI18nService().getCurrentLocale()), null));
        }
        return breadcrumbs;
    }

    protected int loadPageSize() {
        int pageSize = 10;

        BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
        if (baseStoreModel != null && baseStoreModel.getNumberOfOrdersPerPage() != null) {
            pageSize = baseStoreModel.getNumberOfOrdersPerPage();
        }

        return pageSize;
    }

    protected HttpServletRequest retrieveRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getRequest();
    }

    protected void initSessionStoredInfo() {
        HttpServletRequest request = retrieveRequest();
        if (StringUtils.isNotEmpty((String) request.getParameter(ThyssenkruppeustorefrontaddonWebConstants.INIT_SORT_CODE_PARAMETER))) {
            getSessionService().removeAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE);
            getSessionService().removeAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER);
        }
    }

    protected String loadStoredSortCode(final String sortCode) {
        String sessionSortCode = getSessionService().getAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE);

        if (StringUtils.isNotEmpty(sessionSortCode) && sortCode == null) {
            return sessionSortCode;
        }

        if (StringUtils.isNotEmpty(sortCode)) {
            getSessionService().setAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_SORT_CODE, sortCode);
        }

        return null;
    }

    protected Integer loadStoredPage() {
        Integer page = retrievePageParameterValue();
        Integer sessionPage = getSessionService().getAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER);

        if (sessionPage != null && page == null) {
            return sessionPage;
        }

        if (page != null) {
            getSessionService().setAttribute(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_NUMBER, page);
        }

        return null;
    }

    private Integer retrievePageParameterValue() {
        HttpServletRequest request = retrieveRequest();

        String page = request.getParameter(ThyssenkruppeustorefrontaddonWebConstants.ORDER_LIST_PAGE_PARAMETER);
        if (page != null) {
            return new Integer(page);
        }

        return null;
    }

    private boolean openUpdatePasswordForm() {
        final String enableUpdatePasswordForm = (String) retrieveRequest().getParameter(UPDATE_PASSWORD_FORM_PARAM);
        return StringUtils.isBlank(enableUpdatePasswordForm) ? false : Boolean.parseBoolean(enableUpdatePasswordForm);
    }

    private void updateProfileAttributesToModel(Model model) throws CMSItemNotFoundException {
        if (model != null) {
            final List<TitleData> titles = userFacade.getTitles();
            final CustomerData customerData = customerFacade.getCurrentCustomer();
            final List<AddressData> addressBooks = userFacade.getAddressBook();

            if (customerData.getTitleCode() != null) {
                model.addAttribute("title", findTitleForCode(titles, customerData.getTitleCode()));
            }
            model.addAttribute("faxNumbers", getUniquePhoneNumbersOrFax(addressBooks, "faxNumbers"));
            model.addAttribute("phoneNumbers", getUniquePhoneNumbersOrFax(addressBooks, "phoneNumbers"));
            model.addAttribute("customerData", customerData);
            model.addAttribute("currentUrl", getCurrentServletPath());
            storeCmsPageInModel(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
            model.addAttribute(BREADCRUMBS_ATTR, buildMyAccountBreadcrumbs(TEXT_ACCOUNT_PROFILE));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        }
    }

    private String getCurrentServletPath() {
        return retrieveRequest().getServletPath();
    }

    public TkEuPasswordValidator getTkPasswordValidator() {
        return tkPasswordValidator;
    }

    public void setTkPasswordValidator(TkEuPasswordValidator tkPasswordValidator) {
        this.tkPasswordValidator = tkPasswordValidator;
    }

    private void setOrderStatusName(SearchPageData<OrderHistoryData> searchPageData) {
        List<OrderHistoryData> orderHistoryData = searchPageData.getResults();
        for (OrderHistoryData data : orderHistoryData) {
            data.setOrderStatusName(getEnumUtils().getNameFromEnumCode(data.getStatus()));
        }
    }

    private void setOrderStatusName(OrderData orderData) {
        orderData.setOrderStatusName(getEnumUtils().getNameFromEnumCode(orderData.getStatus()));
    }

    private String getCSRFTokenFromSession(HttpSession httpSession) {
        final String defaultCsrfTokenAttributeName = HttpSessionCsrfTokenRepository.class.getName().concat(".CSRF_TOKEN");
        CsrfToken sessionToken = (CsrfToken) httpSession.getAttribute(defaultCsrfTokenAttributeName);
        return sessionToken.getToken();
    }
}

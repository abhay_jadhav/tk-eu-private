<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>

<div class="row">
    <div class="col-md-12">
        <ul class="list-inline register-options">
            <li>
                <label>
                    <div class="m-checkbox">
                        <input id="customerType1" type="radio" name="customerType" checked value="Existing" class="m-checkbox__input js-register-type sr-only" data-qa-id="register-existing-checkbox" />
                        <span class="m-checkbox__label">
                            <span class="m-checkbox__label__box m-checkbox__label__box--radio"></span>
                            <span class="m-checkbox__label__text" for="customerType1">
                                <spring:theme code="register.existingCustomer"></spring:theme>
                            </span>
                        </span>
                    </div>
                </label>
            </li><li><label>
                    <div class="m-checkbox">
                        <input id="customerType2" type="radio" name="customerType" value="New" class="m-checkbox__input js-register-type sr-only" data-qa-id="register-newcustomer-checkbox" />
                        <span class="m-checkbox__label">
                            <span class="m-checkbox__label__box m-checkbox__label__box--radio"></span>
                            <label class="m-checkbox__label__text" for="customerType2">
                                <spring:theme code="register.newCustomer"></spring:theme>
                            </label>
                        </span>
                    </div>
                </label>
            </li>
        </ul>
    </div>
</div>
<div class="row js-existing-member-field">
    <div class="col-md-6">
        <spring:theme code="register.customerNumber" var="customerNumber"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-customerNumber" labelKey="${customerNumber}" path="customerNumber"
                                  mandatory="true" qaAttribute="register-customer-number"/>
                                  <span class="err-msg help-block hidden"><spring:theme code="register.compNoMsg"></spring:theme></span>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <spring:theme code="register.firstName" var="firstName"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-firstName" labelKey="${firstName}" path="firstName"
                                  mandatory="true" qaAttribute="register-firstname"/>
                                  <span class="err-msg help-block hidden"><spring:theme code="register.firstNameMsg"></spring:theme></span>
    </div>
    <div class="col-md-6">
        <spring:theme code="register.lastName" var="lastName"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-lastName" labelKey="${lastName}" path="lastName" mandatory="true"
                                  qaAttribute="register-lastname"/>
                                  <span class="err-msg help-block hidden"><spring:theme code="register.lastNameMsg"></spring:theme></span>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <spring:theme code="register.email" var="email"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-email" labelKey="${email}" path="email" mandatory="true"
                                  qaAttribute="register-email"/>
                                  <span class="err-msg help-block hidden"><spring:theme code="register.emailMsg"></spring:theme></span>
    </div>
    <div class="col-md-6">
        <spring:theme code="register.phone" var="phoneNumber"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-phoneNumber" labelKey="${phoneNumber}" path="phoneNumber"
                                      qaAttribute="register-telephone"/>
                                      <span class="err-msg help-block hidden"><spring:theme code="register.phoneNumberMsg"></spring:theme></span>
    </div>
</div>
<div class="row js-non-existing-member-field">
    <div class="col-md-6">
        <spring:theme code="register.company" var="company"></spring:theme>
        <tkEuformElement:formInputBox idKey="registration-company" labelKey="${company}" path="company" mandatory="true"
                                  qaAttribute="register-company"/>
                                  <span class="err-msg help-block hidden"><spring:theme code="register.companyNameMsg"></spring:theme></span>
    </div>
    <div class="col-md-6">
        <spring:theme code="register.business" var="business"></spring:theme>
        <tkEuformElement:formSelectBox idKey="register-business" labelKey="register.business" path="business"
                                   mandatory="true" skipBlank="false" qaAttribute="register-business"
                                   skipBlankMessageKey="register.business.pleaseSelect" items="${tkEuBusinessTypes}"
                                   selectCSSClass="form-control selectpicker always-validate"/>
                                   <span class="err-msg help-block hidden"><spring:theme code="register.businessMsg"></spring:theme></span>
    </div>
</div>



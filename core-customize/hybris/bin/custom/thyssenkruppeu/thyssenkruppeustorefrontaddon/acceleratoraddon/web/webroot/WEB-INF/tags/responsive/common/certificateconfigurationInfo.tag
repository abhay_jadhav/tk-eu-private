<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="configurationInfo" required="true" type="de.hybris.platform.commercefacades.order.data.ConfigurationInfoData" %>
<%@ attribute name="styleClass" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${configurationInfo.configurationValue eq 'true'}">
    <p class="${styleClass}" data-qa-id="cart-certificate-label"><small class="m-certificate"><spring:theme code="checkout.item.certificate.label"/>:&nbsp;${configurationInfo.configurationLabel}</small></p>
</c:if>

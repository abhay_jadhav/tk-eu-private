<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:choose>
    <c:when test="${not empty productData}">
    <div class="container">
        <div class="row">
            <div class="col-md-12 owl-home">
                <div class="m-page-section m-page-section--no-bottom-border">
                    <h3 class="m-page-section__title m-page-section__title--no-bottom-border" data-qa-id="accessory-header-section">${fn:escapeXml(title)}</h3>
                    <div class="m-carousel-product m-carousel-product--multi js-owl-carousel js-owl-multi-products-lg" data-qa-id="product-multi-carousel">
                        <c:forEach items="${productData}" var="product">
                            <c:url value="${product.url}" var="productUrl"/>

                            <div class="item multi--item">
                                <div class="m-encourage-blocks m-encourage-blocks--has-bottom-link">
                                    <div class="m-encourage-blocks__content">
                                        <div class="m-encourage-blocks__content__img js-product-carousel-img">
                                            <a href="${productUrl}">
                                                <product:productPrimaryReferenceImage product="${product}" format="product" />
                                            </a>
                                        </div>
                                        <div class="item-name" data-qa-id="pdp-carousel-name">
                                            <c:set var="productTitle" value="${fn:escapeXml(product.name)}" />
                                             <a class="m-text-color--black" href="${productUrl}">
                                                <c:set var="tooLong" value="${fn:length(productTitle) > 60}" />
                                                ${tooLong ? fn:substring(productTitle, 0, 60) : productTitle}${tooLong ? '...' : ''}
                                              </a>
                                        </div>
                                        <c:if test="${not empty product.tkEuPLPDisplayList}">
                                            <div class="m-product-attributes">
                                                <c:forEach items="${product.tkEuPLPDisplayList}" var="listElement">
                                                    <div class="m-product-attributes__item">
                                                        <div class="m-product-attributes__item__key">
                                                            ${listElement.key}
                                                        </div>
                                                        <div class="m-product-attributes__item__value">
                                                            ${listElement.value}
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="m-encourage-blocks__bottom">
                                        <a href="${productUrl}" class="m-encourage-blocks__bottom-link" data-qa-id="pdp-carousel-view-product-link">
                                            <spring:theme code="carousel.view.label"/>
                                        </a>
                                        <span class="u-icon-tk u-icon-tk--arrow-right"></span>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </c:when>

    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" class="l-page l-page__my-account">
    <div class="container l-page__content">
        <div class="row">
            <div class="col-md-12  l-page__content__head">
                <h3 class="l-page__title--no-description" data-qa-id="dashboard-header">
                    <spring:theme code="myaccount.landingpage.title"/>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 l-page__content__body-left">
                <cms:pageSlot position="SideContent" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
            <div class="col-md-9 l-page__content__body-right">
                <cms:pageSlot position="BodyContent" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
        </div>
    </div>
    <cms:pageSlot position="BottomContent" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
</template:page>

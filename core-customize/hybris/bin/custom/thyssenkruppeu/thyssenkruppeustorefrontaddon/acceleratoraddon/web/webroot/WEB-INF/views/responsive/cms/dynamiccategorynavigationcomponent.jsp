<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<spring:theme code="text.header.navigation.processing" text="Processing" var="processing"/>
<spring:theme code="text.header.navigation.support" text="Support" var="support"/>
<c:if test="${component.visible}">
    <ul class="nav navbar-nav primary-menu">
        <li class="dropdown yamm-fullwidth" id="${component.navigationNode.category.code}" data-target="#${component.navigationNode.category.code}" data-toggle="dropdown">
            <a data-qa-id="header-cms-lnk" href="#" class="nav-mega-menu dropdown-toggle" aria-expanded="false" data-qa-id="main-menu-shop-for-products-link">
                ${component.navigationNode.title}
            </a>
            <ul class="dropdown-menu">
                <li>
                    <div class="yamm-content" data-qa-id="category-list-section">
                        <div class="container">
                            <div class="mn-sidebar row">
                                <common:megamenu childItems="${component.navigationNode.category.categories}" isRootCategories="${true}"  parentCategory="${component.navigationNode.category}"/>
                            </div>
                            <div class="show-more-wrapper">
                                <a class="js-close-show-more-wrapper" href="#"><spring:theme code="text.header.navigation.goback" text="Go Back"/></a>
                                <div class="show-more-inner"></div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</c:if>


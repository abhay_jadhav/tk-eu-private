/*global ACC, $ */

ACC.colorbox.config = {
    maxWidth: "100%",
    opacity: 0.7,
    width: "auto",
    transition: "none",
    close: '<span class="u-icon-tk u-icon-tk--close"></span>',
    title: '<div class="h4 m-colorbox__title">{title}</div>',
    onComplete: function () {
        $.colorbox.resize();
        ACC.common.refreshScreenReaderBuffer();
    },
    onClosed: function () {
        ACC.common.refreshScreenReaderBuffer();
    }
}

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<ul class='l-faq__topics__list list-group m-list-group'>
    <c:forEach var="navComponent" items="${navComponent}" varStatus="loop">
        <c:if test="${not empty navComponent.value}">
            <li class="l-faq__topics__list__item list-group-item m-list-group__item ${loop.first ? 'active' : ''}">
                <a href="#${navComponent.key}" data-qa-id="faq-topic" data-toggle="tab" class="no-format">${navComponent.value}</a>
            </li>
        </c:if>
    </c:forEach>
</ul>

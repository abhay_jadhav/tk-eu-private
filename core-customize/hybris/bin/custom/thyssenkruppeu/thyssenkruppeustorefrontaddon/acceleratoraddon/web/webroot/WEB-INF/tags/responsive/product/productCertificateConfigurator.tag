<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="configurations" required="true" type="java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url value="${product.url}/CERTIFICATE" var="actionUrl"/>
<div class="m-certificate-config">
    <form:form id="configureForm" name="certificateConfigurationForm" commandName="certificateConfigurationForm" action="${actionUrl}" method="post" class="m-certificate-config__form js-certificate-config-form">
        <input type="hidden" name="qty" id="quantity" class="qty js-qty-selector-input" value="1" />
        <input type="hidden" name="salesUom" id="configureForm_salesUom" class="salesUom js-hidden-uom-selector-input" value="${selectedUom}" />
        <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
        <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
        <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
        <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${variantSelectionForm.selectedTradeLength}"/>
        <div class="form-group m-certificate-config__options">
            <div class="radio m-certificate-config__option">
                <input class="js-product-certificate-select m-certificate-config__radio" type="radio" id="certOptionNone" name="certOption" value="none" checked />
                <label for="certOptionNone" class="m-certificate-config__label"><span class="m-certificate-config__label__text"><spring:theme code="product.certificate.none"/></span></label>
                <i class="u-icon-tk u-icon-tk--info m-certificate-config__tooltip" data-toggle="tooltip" data-placement="top" title="<spring:theme code='product.certificate.none.tooltip' text='If you dont need a certificate then its free of charge'/>"></i>
                <span class="m-certificate-config__price m-text-color--grey-middle"><format:price priceData="${ZeroPrice}"/></span>
            </div>
            <c:forEach var="configuration" items="${configurations}" varStatus="loop">
                <spring:eval expression="configuration.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificate"/>
                <c:if test="${isCertificate}">
                    <div class="radio m-certificate-config__option">
                        <c:set var="isCertificateSelected" value="false"/>
                        <c:if test="${!empty selectedCertificateId && selectedCertificateId eq configuration.uniqueId}">
                            <c:set var="isCertificateSelected" value="true"/>
                        </c:if>
                        <input class="js-product-certificate-select m-certificate-config__radio" type="radio" id="certOption${loop.index}" name="certOption" value="${configuration.uniqueId}" ${isCertificateSelected ? 'checked="checked"' : ''}/>
                        <label for="certOption${loop.index}" class="m-certificate-config__label"><span class="m-certificate-config__label__text">${configuration.configurationLabel}</span></label>
                        <i class="u-icon-tk u-icon-tk--info m-certificate-config__tooltip" data-toggle="tooltip" data-placement="top" title="${configuration.description}"></i>
                        <c:if test="${!empty configuration.priceData}">
                            <span class="m-certificate-config__price m-text-color--grey-middle"><format:price priceData="${configuration.priceData}"/></span>
                        </c:if>
                    </div>
                </c:if>
            </c:forEach>
        </form:form>
    </div>
</div>

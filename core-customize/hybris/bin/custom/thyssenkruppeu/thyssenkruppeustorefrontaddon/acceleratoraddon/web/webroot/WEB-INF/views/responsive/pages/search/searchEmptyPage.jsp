<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tk-nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>

<c:set var="isContinueShoppingButton" value="${true}" scope="request"/>

<template:page pageTitle="${pageTitle}" class="l-page l-page__search-empty">

    <c:url value="/" var="homePageUrl" />

    <div class="container l-page__content">

        <cms:pageSlot position="SideContent" var="feature" element="div" class="side-content-slot cms_disp-img_slot searchEmptyPageTop">
            <cms:component component="${feature}" element="div" class="no-space yComponentWrapper searchEmptyPageTop-component"/>
        </cms:pageSlot>

        <div class="row">
            <div class="col-sm-9">
                <h3 class="l-page__title--no-description">
                    <spring:theme code="search.page.results.label" arguments="${searchPageData.freeTextSearch}"/>
                </h3>
                <div class="m-search-empty__content">

                    <div class="m-search-empty__content__info">
                        <h4 data-qa-id="plp-no-results-msg"><spring:theme code="search.no.results" arguments="${searchPageData.freeTextSearch}"/></h4>
                        <tk-nav:searchSpellingSuggestion spellingSuggestion="${searchPageData.spellingSuggestion}"/>
                     </div>

                    <cms:pageSlot position="SearchMiddleContent" var="comp" element="div">
                        <cms:component component="${comp}" element="div"/>
                    </cms:pageSlot>

                </div>
            </div>

            <div class="col-sm-3">
                <cms:pageSlot position="EncouragingSearchEmptySlot" var="feature" >
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <cms:pageSlot position="BottomContent" var="comp" >
                    <cms:component component="${comp}" />
                </cms:pageSlot>
            </div>
        </div>
    </div>
</template:page>

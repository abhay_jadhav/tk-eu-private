<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:htmlEscape defaultHtmlEscape="true" />
    <div class="container l-page__content">
        <div class="row">
            <div class="col-md-7">
                <c:choose>
                    <c:when test="${user.titleCode ne null && not empty user.lastName}">
                        <c:set var="userName" value="${user.lastName}"/>
                        <h3 class="l-page__title"><spring:theme code="text.reset.password.thank.message" arguments="${user.titleName},${userName}"/></h3>
                    </c:when>
                    <c:otherwise>
                        <h3 class="l-page__title"><spring:theme code="text.reset.password.thank.withoutTitle.message"/></h3>
                    </c:otherwise>
                </c:choose>
                <p class="l-page__description">
                    <spring:theme code="text.reset.password.thank.purchase.products"/>
                </p>
                <action:actions element="div" parentComponent="${component}"/>
            </div>
        </div>
    </div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="/my-account/order/cancel" var="cancelOrderUrl" />
<c:url value="/my-account/order/${orderData.sapOrderId}" var="sapOrderConfirmationUrl" />
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="m-order-ack">
    <div class="row">
        <div class="col-xs-12">
            <h4 data-qa-id="order-details-header"> <spring:theme code="myaccount.confirmation.details.headline.orderConfirmation" arguments="${fn:escapeXml(orderData.sapOrderId)}" text="Order conformation"/>&nbsp;<spring:theme code="myaccount.confirmation.details.orderStatus" arguments="${orderData.orderStatusName}" /></h4>
        </div>
    </div>
    <hr class="m-order-ack__separator m-order-ack__separator--thick"/>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id=""><spring:theme code="myaccount.confirmation.details.orderName"/></p>
                <p class="m-item-group__value" data-qa-id="">${fn:escapeXml(orderData.name)}&nbsp;</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id=""><spring:theme code="myaccount.confirmation.header.deliveryDate"/></p>
                <p class="m-item-group__value" data-qa-id=""><fmt:formatDate value="${orderData.deliveryDate}" dateStyle="medium" type="date"/></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id=""><spring:theme code="myaccount.confirmation.details.poNumber"/></p>
                <p class="m-item-group__value" data-qa-id="">${orderData.purchaseOrderNumber}&nbsp;</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 m-order-ack--doc-download">
            <div class="m-item-group">
                <p class="m-item-group__label" data-qa-id="order-details-document-label"><spring:theme code="myaccount.confirmation.details.orderConfirmationDocument"/></p>
                <p class="m-item-group__value m-download" data-qa-id="confirmationdetails-document-value">
                    <a id="doc${orderData.code}" class="m-download--link js-document-primary js-document-download disabled" href="/document/download" disabled="disabled" data-document-obj='{"documentNo": "${orderData.sapOrderId}","positionNo":"","batchNo":"","lineItemNo":"","documenType":"ORDER"}' data-qa-id="order-details-document-value">
                        <spring:theme code="myaccount.confirmation.details.orderConfirmationDocument.notAvailable.Tooltip" var="documentTooltip"/>
                        <i class="u-icon-tk u-icon-tk--md  m-loading-icon" data-toggle="tooltip" title="${documentTooltip}"></i>
                    </a>
                    <input type="hidden" value="${CSRFTokenSession}" id="csrfId"/>
                </p>
            </div>
        </div>
    </div>
</div>

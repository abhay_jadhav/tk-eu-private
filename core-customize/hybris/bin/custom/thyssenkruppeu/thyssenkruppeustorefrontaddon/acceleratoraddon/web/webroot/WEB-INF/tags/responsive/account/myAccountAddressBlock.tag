<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData"%>
<%@ attribute name="makePrimaryLabel" required="false" type="java.lang.String"%>
<%@ attribute name="makePrimaryUrl" required="false" type="java.lang.String"%>
<%@ attribute name="editAddressUrl" required="false" type="java.lang.String"%>
<%@ attribute name="deleteAddressUrl" required="false" type="java.lang.String"%>
<%@ attribute name="isPrimaryAddress" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="my-account" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/account" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<c:if test="${empty makePrimaryLabel}">
    <spring:theme code="myaccount.shipping.makePrimary" var="makePrimaryLabel" />
</c:if>

<div class="m-block m-block--has-bottom-links js-equal-height" data-qa-id="myaccount-shipping-addresses-primary-address-value">
    <div class="m-block__content">
        <common:address address="${address}"/>
    </div>
    <div class="m-block__bottom-links">
        <div class="m-block__bottom-links__left">
            <c:if test="${not empty editAddressUrl}">
                <c:url value="${editAddressUrl}" var="editAddressUrl"/>
                <spring:theme code="myaccount.shipping.popup.headline.edit" text="Edit shipping address" var="editPopupTitleLabel"/>
                <a class="js-edit-shipping-address-link" data-title="${editPopupTitleLabel}" href="${editAddressUrl}" data-qa-id="shippingaddress-edit-link">
                    <spring:theme code="myaccount.shipping.edit" text="Edit"/>
                </a>
            </c:if>
            <c:if test="${not empty deleteAddressUrl}">
                <c:url value="${deleteAddressUrl}" var="deleteAddressUrl" />
                <spring:theme code="myaccount.shipping.delete.headline" var="deletePopupTitleLabel"/>
                <spring:theme code="myaccount.shipping.delete" var="deleteLinkLabel" />
                <form data-title="${deletePopupTitleLabel}" class="js-delete-shipping-address-form" type="POST" action="${deleteAddressUrl}">
                    <div class="display-none js-popup-content">
                        <div class="m-popup__body">
                            <p><spring:theme code="myaccount.shipping.delete.description" arguments="${address.companyName}"/></p>

                            <div class="m-popup__body__actions">
                                <div class="m-popup__body__actions__item" data-qa-id="shippingaddress-popup-no-link">
                                    <a href="javascript:void(0);" class="js-cancel-button"> <spring:theme code="myaccount.shipping.delete.button.no" /> </a>
                                </div>
                                <div class="m-popup__body__actions__item" data-qa-id="shippingaddress-popup-delete-this-address-btn">
                                    <button class="btn btn-default js-submit-button"> <spring:theme code="myaccount.shipping.delete.button.yes" /> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="addressStatus" value="isPrimaryAddress"/>
                    <input type="submit" class="btn btn-link js-delete-shipping-address-link" value="${deleteLinkLabel}" data-qa-id="shippingaddress-delete-link"/>
                </form>
            </c:if>
        </div>
        <div class="m-block__bottom-links__right" >
            <c:if test="${not empty makePrimaryUrl}">
                <c:url value="${makePrimaryUrl}" var="makePrimaryUrl"/>
                <a class="js-make-primary-shipping-address-link" href="${makePrimaryUrl}" data-qa-id="shippingaddress-popup-makeprimary-link">
                    ${makePrimaryLabel}
                </a>
            </c:if>
        </div>
    </div>
</div>

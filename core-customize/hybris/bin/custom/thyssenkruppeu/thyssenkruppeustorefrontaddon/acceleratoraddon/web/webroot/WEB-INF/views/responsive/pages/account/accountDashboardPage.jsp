<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>

<c:url value="/my-account/profile" var="viewAllProfile" />
<c:url value="/my-account/shipping-address" var="viewAllShippingAddress" />
<c:url value="/my-account/company-details" var="viewAllBillingAddress" />
<c:url value="/my-account/orders?init=true" var="viewAllOrders" />
<spring:htmlEscape defaultHtmlEscape="true" />
<div class="row">
    <div class="col-md-4">
        <div class="m-page-section m-page-section--no-bottom-border js-equal-height">
            <h3 class="h5 m-page-section__title" data-qa-id="myaccount-dashboard-shipping-address-label"><spring:theme code="text.account.shipping.address" /><a class="m-page-section__title__action" href="${viewAllShippingAddress}" data-qa-id="myaccount-dashboard-shipping-address-viewall">
                <spring:theme text="View All" code="text.account.dashboard.viewAll" /></a>
            </h3>
            <div class="m-page-section__content">
                <div class="m-item-group">
                    <div class="m-item-group__value" data-qa-id="myaccount-dashboard-shipping-address-value">
                         <c:choose>
                             <c:when test="${primaryShippingAddress ne null and not empty primaryShippingAddress}">
                                 <common:address address="${primaryShippingAddress}" />
                             </c:when>
                             <c:otherwise>
                                 <c:forEach var="shippingAddress" items="${shippingAddress}" varStatus="loop">
                                     <c:if test="${loop.first}">
                                         <common:address address="${shippingAddress}" />
                                     </c:if>
                                 </c:forEach>
                             </c:otherwise>
                         </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="m-page-section m-page-section--no-bottom-border js-equal-height">
            <h3 class="h5 m-page-section__title" data-qa-id="myaccount-dashboard-billing-address-label"><spring:theme code="myaccount.company.billing.address" />
                <a class="m-page-section__title__action" href="${viewAllBillingAddress}" data-qa-id="myaccount-dashboard-billing-address-viewall">
                    <spring:theme text="View All" code="text.account.dashboard.viewAll" />
                </a>
            </h3>
            <div class="m-page-section__content">
                <c:choose>
                    <c:when test="${primaryBillingAddress ne null and not empty primaryBillingAddress}">
                        <div class="m-item-group">
                            <div class="m-item-group__value" data-qa-id="myaccount-dashboard-billing-address-value" >
                                <common:address address="${primaryBillingAddress}" />
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="billingAddress" items="${billingAddresses}" varStatus="loop">
                            <div class="m-item-group">
                                <div class="m-item-group__value" data-qa-id="myaccount-dashboard-billing-address-value" >
                                    <c:if test="${loop.first}">
                                        <common:address address="${billingAddress}" />
                                    </c:if>
                                </div>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>
            </div>
        </div>
    <div class="col-md-4">
        <div class="m-page-section m-page-section--no-bottom-border">
            <h4 class=" h5 m-page-section__title" data-qa-id="myaccount-dashboard-profile-info-label"><spring:theme code="myaccount.dashboard.personalinfo.header" />
                <a class="m-page-section__title__action" href="${viewAllProfile}" data-qa-id="myaccount-dashboard-profile-info-viewall">
                    <spring:theme text="View All" code="text.account.dashboard.viewAll" />
                </a>
                </h4>
            <div class="m-page-section__content">
                <div class="m-item-group" data-qa-id="myaccount-dashboard-profile-info-value">
                    <p class="m-item-group__label" ><spring:theme code="myaccount.profile.user.name" /></p>
                    <p class="m-item-group__value">
                        <span data-qa-id="myaccount-dashboard-profile-firstname">${customerData.firstName}</span>
                        <span data-qa-id="myaccount-dashboard-profile-lastname">${customerData.lastName}</span>
                    <p>
                </div>
                <div class="m-item-group">
                    <p class="m-item-group__label" ><spring:theme code="myaccount.profile.login.name" /></p>
                    <p class="m-item-group__value" data-qa-id="myaccount-dashboard-profile-email">
                        ${customerData.email}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-page-section${fn:length(searchPageData.results) gt 0 ? '--no-bottom-border' : ''}">
    <h4 class="m-page-section__title h5" data-qa-id="myaccount-dashboard-recent-orders-label"><spring:theme code="myaccount.dashboard.recent.order" /><c:if test="${not empty searchPageData.results}"><a class="m-page-section__title__action" href="${viewAllOrders}" data-qa-id="myaccount-dashboard-recent-orders-lnks">
        <spring:theme text="View All" code="text.account.dashboard.viewAll" />
    </a></c:if></h4>
    <div class="m-page-section__content">
        <c:set var="searchUrl" value="/my-account/orders?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
        <b2b-order:orderListing searchUrl="${searchUrl}" hidePagination="${true}" hideSorting="${true}" messageKey="text.account.orderHistory.page" maxItems="${5}"/>
    </div>
</div>

<%@ attribute name="supportedCountries" required="false" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>
<div class="m-my-account-shipping__form__add m-my-account-shipping__form__add--popup js-shipping-address-add-form">
    <div class="row m-my-account-shipping__form__add__company">
        <div class="col-md-6 m-my-account-shipping__form__add__company__name js-input-mandatory">
            <div data-qa-id="shippingaddress-popup-company-label">
                <spring:theme  code="myaccount.shipping.companyName " var="companyName"></spring:theme>
            </div>
            <tkEuformElement:formInputBox idKey="address-companyName" labelKey="${companyName}" path="companyName" maxlength="255" inputCSS="form-control" mandatory="true" qaAttribute="shippingaddress-popup-company-textbox" placeholder="myaccount.shipping.companyName.placeholder"/>
        </div>
        <div class="col-md-6 m-my-account-shipping__form__add__company__department-name">
            <div data-qa-id="shippingaddress-popup-department-name-label">
                <spring:theme  code="myaccount.shipping.departmentName" var="departmentName"></spring:theme>
            </div>
            <tkEuformElement:formInputBox idKey="address-department" labelKey="${departmentName}" path="departmentName" maxlength="255" inputCSS="form-control" qaAttribute="shippingaddress-popup-department-textbox" placeholder="myaccount.shipping.departmentName.placeholder"/>
        </div>
    </div>
    <div class="row m-my-account-shipping__form__add__person">
        <div class="col-md-2 m-my-account-shipping__form__add__person__salutation">
            <div data-qa-id="shippingaddress-popup-name-label">
                <spring:theme  code="myaccount.shipping.title" var="titleLabel"></spring:theme>
            </div>
            <tkEuformElement:formSelectBox idKey="address-title" labelKey="${titleLabel}" path="titleCode" mandatory="false" skipBlank="false" skipBlankMessageKey="myaccount.shipping.title.placeholder" items="${titles}" qaAttribute="shippingaddress-popup-title-dropdwon" selectCSSClass="form-control selectpicker"/>
        </div>
        <div class="col-md-5 m-my-account-shipping__form__add__person__first-name js-input-mandatory">
            <spring:theme  code="myaccount.shipping.name" var="nameLabel"></spring:theme>
            <tkEuformElement:formInputBox idKey="address-firstName" labelKey="${nameLabel}" path="firstName" inputCSS="form-control" maxlength="255" mandatory="true" qaAttribute="shippingaddress-popup-firstname-textbox" placeholder="myaccount.shipping.firstName.placeholder" />
        </div>
        <div class="col-md-5 m-my-account-shipping__form__add__person__last-name js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-lastName" labelKey="" path="lastName" inputCSS="form-control" maxlength="255" mandatory="true" qaAttribute="shippingaddress-popup-lastname-textbox" placeholder="myaccount.shipping.lastName.placeholder"/>
        </div>
    </div>
    <div class="row m-my-account-shipping__form__add__address">
        <div class="col-md-10 m-my-account-shipping__form__add__address__line1 js-input-mandatory">
            <div data-qa-id="shippingaddress-popup-adress-label">
                <spring:theme  code="myaccount.shipping.address" var="addressLabel"></spring:theme>
            </div>
            <tkEuformElement:formInputBox idKey="address-line1" labelKey="${addressLabel}" path="line1" maxlength="255" inputCSS="form-control" mandatory="true" qaAttribute="shippingaddress-popup-address-textbox" placeholder="myaccount.shipping.street.placeholder" />
        </div>
        <div class="col-md-2 m-my-account-shipping__form__add__address__line2 js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-line2" labelKey="" path="line2" maxlength="255" inputCSS="form-control" mandatory="true" qaAttribute="shippingaddress-popup-addressno-textbox" placeholder="myaccount.shipping.housenumber.placeholder" />
        </div>
    </div>
    <div class="row m-my-account-shipping__form__add__city">
        <div class="col-md-3 m-my-account-shipping__form__add__city__zipcode js-input-mandatory">
            <div data-qa-id="shippingaddress-popup-city-label">
                <spring:theme  code="myaccount.shipping.city" var="cityLabel"></spring:theme>
            </div>
            <tkEuformElement:formInputBox idKey="address-postalCode" labelKey="${cityLabel}" path="postcode" maxlength="10" inputCSS="form-control" mandatory="true" qaAttribute="shippingaddress-popup-zip-textbox" placeholder="myaccount.shipping.zipcode.placeholder" />
        </div>
        <div class="col-md-5 m-my-account-shipping__form__add__city__name js-input-mandatory">
            <tkEuformElement:formInputBox idKey="address-townCity" labelKey="" path="townCity" maxlength="255" inputCSS="form-control" mandatory="true" qaAttribute="shippingaddress-popup-city-textbox" placeholder="myaccount.shipping.city.placeholder" />
        </div>
        <div class="col-md-3 m-my-account-shipping__form__add__city__country">
            <label class="control-label" data-qa-id="checkout-add-address-country-label" data-qa-id="shippingaddress-popup-country-label">
                <spring:theme  code="myaccount.shipping.country"></spring:theme>
            </label>
            <div data-qa-id="checkoust-add-address-country">
                <c:forEach var="supportedCountries" items="${supportedCountries}">
                    <input type="hidden" name="countryIso" value="${supportedCountries.isocode}"/>
                    <p class="form-control-static">${supportedCountries.name}</p>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="row m-my-account-shipping__form__add__make-primary">
        <div class="col-md-6" data-qa-id="shippingaddress-popup-primarycheckbox">
            <tkEuformElement:formThemeCheckbox idKey="makePrimaryCheckBox" labelKey="myaccount.shipping.makePrimary" path="makePrimaryCheckBox" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
        </div>
    </div>
</div>
<div class="m-popup__body__actions">
    <div class="m-popup__body__actions__item" data-qa-id="shippingaddress-popup-cancel-link">
        <a href="javascript:void(0)" class="js-cancel-button">
            <spring:theme code="myaccount.shipping.button.cancel" text="Cancel"/>
        </a>
    </div>
    <div class="m-popup__body__actions__item">
        <button data-qa-id="shippingaddress-popup-save-btn" type="submit" class="btn btn-default js-add-address-submit">
        <c:choose>
            <c:when test="${addAddressPage eq true}">
                <spring:theme code="myaccount.shipping.button.add" text="Add"/>
            </c:when>
            <c:otherwise>
                <spring:theme code="myaccount.shipping.button.save" text="Save"/>
            </c:otherwise>
        </c:choose>
        </button>
    </div>
</div>

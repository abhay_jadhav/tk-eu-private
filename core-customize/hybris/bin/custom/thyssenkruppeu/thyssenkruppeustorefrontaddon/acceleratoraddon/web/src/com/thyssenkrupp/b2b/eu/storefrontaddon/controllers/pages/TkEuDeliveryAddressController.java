package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import com.thyssenkrupp.b2b.eu.facades.account.TkEuB2bMyAccountFacade;
import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCheckoutFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation.TkEuNewAddressValidator;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isNotBlank;

@Controller
@RequestMapping(value = "/delivery-address")
public class TkEuDeliveryAddressController extends AbstractPageController {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuDeliveryAddressController.class);

    @Resource(name = "b2bCheckoutFacade")
    private TkEuCheckoutFacade tkEuCheckoutFacade;

    @Resource(name = "b2bCheckoutFacade")
    private CheckoutFacade checkoutfacades;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Resource(name = "defaultTkEuB2bMyAccountFacade")
    private TkEuB2bMyAccountFacade tkEuB2bMyAccountFacade;

    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    @Resource(name = "tkEuNewAddressValidator")
    private TkEuNewAddressValidator tkEuNewAddressValidator;

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "b2bCustomerFacade")
    private TkEuB2bCustomerFacade b2bCustomerFacade;

    @RequestMapping(value = "/choose-shipping-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String chooseShippingAddress(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        final CartData cartData = checkoutfacades.getCheckoutCart();
        final CustomerData customerData = customerFacade.getCurrentCustomer();
        final AddressData deliveryAddress = cartData.getDeliveryAddress();
        model.addAttribute("selectedDeliveryAddress", deliveryAddress);
        List<AddressData> customerAddressBook = new ArrayList<>();
        customerAddressBook.addAll(userFacade.getAddressBook());
        customerAddressBook.addAll(tkEuCheckoutFacade.getCustomerB2bUnitShippingAddress());
        addSessionDeliveryAddressToList(deliveryAddress, customerAddressBook);
        model.addAttribute("deliveryAddresses", customerAddressBook);
        model.addAttribute("primaryShippingAddress", customerData.getPrimaryShippingAddress());

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SELECT_DELIVERY_ADDRESS_POPUP_PAGE;
    }

    private void addSessionDeliveryAddressToList(AddressData deliveryAddress, @NotNull List<AddressData> customerAddressBook) {
        if (deliveryAddress != null) {
            final boolean isDeliveryAddressPresent = customerAddressBook.stream().map(AddressData::getId).anyMatch(addressCode -> StringUtils.equals(addressCode, deliveryAddress.getId()));
            if (!isDeliveryAddressPresent) {
                customerAddressBook.addAll(Collections.singletonList(deliveryAddress));
            }
        }
    }

    @RequestMapping(value = "/choose-shipping-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String chooseShippingAddress(final Model model, final TkShippingAddressForm tkShippingAddress, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        if (tkShippingAddress.getSelectedAddressCode() != null)
            tkEuCheckoutFacade.setCartPaymentType();
        setSelectedDeliveryAddress(tkShippingAddress.getSelectedAddressCode());

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SELECT_DELIVERY_ADDRESS_POPUP_PAGE;
    }

    @RequestMapping(value = "/choose-primary-shipping-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String choosePrimaryShippingAddress(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        getCheckoutfacades().setDeliveryAddress(b2bCustomerFacade.getPrimaryShippingAddressOfCurrentUser());
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_SELECT_DELIVERY_ADDRESS_POPUP_PAGE;
    }

    @RequestMapping(value = "/new-shipping-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String addNewShippingAddress(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        model.addAttribute("tkShippingAddressForm", new TkShippingAddressForm());
        model.addAttribute("supportedCountries", cartFacade.getDeliveryCountries());
        model.addAttribute("titles", userFacade.getTitles());

        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_ADD_NEW_DELIVERY_ADDRESS_POPUP_PAGE;
    }

    @RequestMapping(value = "/new-shipping-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String saveNewShippingAddress(final Model model, final TkShippingAddressForm tkShippingAddress, final RedirectAttributes redirectAttributes, final BindingResult bindingResult) {

        try {

            final CartData cartData = checkoutfacades.getCheckoutCart();
            if (validateNewShippingAddress(model, tkShippingAddress, bindingResult, cartData)) {

                model.addAttribute("tkShippingAddressForm", tkShippingAddress);
                model.addAttribute("supportedCountries", checkoutfacades.getDeliveryCountries());
                model.addAttribute("title", userFacade.getTitles());

                return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_ADD_NEW_DELIVERY_ADDRESS_POPUP_PAGE;
            } else
                setNewShippingaddress(tkShippingAddress);
        } catch (Exception ex) {
            LOG.error("Exception while processing the address data :", ex);
        }
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_MULTISTEPCHECKOUT_CHECKOUT_ADD_NEW_DELIVERY_ADDRESS_POPUP_PAGE;
    }

    private boolean validateNewShippingAddress(Model model, TkShippingAddressForm tkShippingAddress, BindingResult bindingResult, CartData cartData) throws CMSItemNotFoundException {
        tkEuNewAddressValidator.validate(tkShippingAddress, bindingResult);
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, setErrorMessage(bindingResult));
            return true;
        }
        return false;
    }

    private String setErrorMessage(BindingResult bindingResult) {
        String errorMessageKey = "address.error.formentry.invalid";
        if (bindingResult.getFieldError("comment") != null && bindingResult.getErrorCount() == 1)
            errorMessageKey = "checkout.shipping.comment.invalid";
        return errorMessageKey;
    }

    public void setNewShippingaddress(TkShippingAddressForm tkShippingAddressForm) {

        final AddressModel addressModel = new AddressModel();
        addressModel.setCompany(tkShippingAddressForm.getCompanyName());
        addressModel.setDepartment(tkShippingAddressForm.getDepartmentName());
        if (isNotBlank(tkShippingAddressForm.getTitleCode())) {
            setTitleCode(tkShippingAddressForm, addressModel);
        }
        addressModel.setFirstname(tkShippingAddressForm.getFirstName());
        addressModel.setLastname(tkShippingAddressForm.getLastName());
        addressModel.setStreetname(tkShippingAddressForm.getLine1());
        addressModel.setStreetnumber(tkShippingAddressForm.getLine2());
        addressModel.setPostalcode(tkShippingAddressForm.getPostcode());
        addressModel.setTown(tkShippingAddressForm.getTownCity());
        if (tkShippingAddressForm.getCountryIso() != null) {

            setCountryCode(tkShippingAddressForm, addressModel);
        }
        addressModel.setShippingAddress(true);
        addressModel.setUnloadingAddress(true);
        addressModel.setBillingAddress(false);
        addressModel.setContactAddress(false);
        addressModel.setVisibleInAddressBook(true);
        addressModel.setDuplicate(false);

        saveCustomerAddressCheck(tkShippingAddressForm, addressModel);
        tkEuCheckoutFacade.saveNewShippingAddress(addressModel);
    }

    private void saveCustomerAddressCheck(TkShippingAddressForm tkShippingAddressForm, AddressModel addressModel) {
        if (tkShippingAddressForm.isSaveCustomerAddressCheckBox()) {

            addressModel.setOwner(userService.getCurrentUser());
            if (tkShippingAddressForm.isMakePrimaryCheckBox()) {
                tkEuB2bMyAccountFacade.savePrimaryShippingAddress(addressModel);
            }
        } else {
            addressModel.setOwner(userService.getAnonymousUser());
        }
    }

    private void setCountryCode(TkShippingAddressForm tkShippingAddressForm, AddressModel addressModel) {
        final String isocode = tkShippingAddressForm.getCountryIso();
        addressModel.setCountry(commonI18NService.getCountry(isocode));
    }

    private void setTitleCode(TkShippingAddressForm tkShippingAddressForm, AddressModel addressModel) {
        final TitleModel title = new TitleModel();
        title.setCode(tkShippingAddressForm.getTitleCode());
        addressModel.setTitle(flexibleSearchService.getModelByExample(title));
    }

    private void setSelectedDeliveryAddress(final String selectedAddressCode) {
        if (isNotBlank(selectedAddressCode)) {
            final AddressData selectedAddressData = getCheckoutfacades().getDeliveryAddressForCode(selectedAddressCode);
            if (selectedAddressData != null) {
                final AddressData cartCheckoutDeliveryAddress = getCheckoutfacades().getCheckoutCart().getDeliveryAddress();
                if (isAddressChanged(cartCheckoutDeliveryAddress, selectedAddressData)) {
                    getCheckoutfacades().setDeliveryAddress(selectedAddressData);
                    if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook()) { // temporary address should  be removed
                        getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
                    }
                }
            }
        }
    }

    protected boolean isAddressChanged(final AddressData cartDeliveryAddress, final AddressData selectedAddressData) {
        return cartDeliveryAddress == null || !selectedAddressData.getId().equals(cartDeliveryAddress.getId());
    }

    public CheckoutFacade getCheckoutfacades() {
        return checkoutfacades;
    }

    public void setCheckoutfacades(CheckoutFacade checkoutfacades) {
        this.checkoutfacades = checkoutfacades;
    }
}

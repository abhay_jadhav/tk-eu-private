<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
    <span class="js-addtocart-baseprice-panel m-volume-prices__heading__price-value" data-qa-id="pdp-main-price">
        <c:choose>
            <c:when test="${not empty cartEntryData and not empty cartEntryData.pricePerBaseUnit}">
                <tk-eu-format:fromPrice priceData="${cartEntryData.pricePerBaseUnit}" withUnit="${true}"/>
            </c:when>
            <c:otherwise>
                <tk-eu-format:fromPrice priceData="${product.price}" withUnit="${true}"/>
            </c:otherwise>
        </c:choose>
    </span>
</sec:authorize>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="visibleFeatures" required="true" type="java.util.Map" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:eval expression="T(de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum).VISIBLE_MAIN" var="VISIBLE_MAIN"/>
<c:set var="mainVisibleFeatures" value="${visibleFeatures[VISIBLE_MAIN]}"/>
<c:set var="featureLength" value="${not empty mainVisibleFeatures?fn:length(mainVisibleFeatures) : 0}"/>
<c:set var="featureEnd" value="5"/>
<c:set var="featureBegin" value="6"/>
<c:set var="featureHalfLength" value="${fn:substringBefore((featureLength/2), '.')}"/>

<c:choose>
    <c:when test="${(featureLength > 12) && (featureLength%2 == 1)}">
        <c:set var="featureEnd" value="${featureHalfLength+1}"/>
        <c:set var="featureBegin" value="${featureHalfLength}"/>
    </c:when>
    <c:when test="${(featureLength > 12) && (featureLength%2 == 0)}">
        <c:set var="featureEnd" value="${featureHalfLength}"/>
        <c:set var="featureBegin" value="${featureHalfLength}"/>
    </c:when>
</c:choose>

<div class="m-product-specification">
    <ul class="m-product-specification__column list-unstyled">
        <c:forEach items="${mainVisibleFeatures}" end="${featureEnd}" var="feature" varStatus="loop">
            <li class="m-product-specification__item">
                <div class="m-product-specification__item__name text-medium"><span data-toggle="tooltip" title="${fn:escapeXml(feature.name)}">${fn:escapeXml(feature.name)}: </span></div>
                <div class="m-product-specification__item__value">
                    <c:forEach items="${feature.featureValues}" var="value" varStatus="status">
                        ${fn:escapeXml(value.value)}
                        <c:choose>
                            <c:when test="${feature.range}">
                                ${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
                            </c:when>
                            <c:otherwise>
                                ${fn:escapeXml(feature.featureUnit.symbol)}
                                ${not status.last ? '<br/>' : ''}
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </div>
            </li>
        </c:forEach>
    </ul>


    <c:if test="${featureLength > 6}">
        <ul class="m-product-specification__column list-unstyled">
            <c:forEach items="${mainVisibleFeatures}" begin="${featureBegin}" var="feature">
                <li class="m-product-specification__item">
                    <div class="m-product-specification__item__name text-medium"><span data-toggle="tooltip" title="${fn:escapeXml(feature.name)}">${fn:escapeXml(feature.name)}: </span></div>
                    <div class="m-product-specification__item__value">
                        <c:forEach items="${feature.featureValues}" var="value" varStatus="status">
                            ${fn:escapeXml(value.value)}
                            <c:choose>
                                <c:when test="${feature.range}">
                                    ${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
                                </c:when>
                                <c:otherwise>
                                    ${fn:escapeXml(feature.featureUnit.symbol)}
                                    ${not status.last ? '<br/>' : ''}
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </c:if>
</div>

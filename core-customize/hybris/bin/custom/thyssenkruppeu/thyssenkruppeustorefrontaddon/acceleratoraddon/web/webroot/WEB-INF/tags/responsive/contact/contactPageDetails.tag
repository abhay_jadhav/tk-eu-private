<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>

<div class="row">
    <div class="col-md-3">
        <spring:theme  code="contact.page.title" var="title1"></spring:theme>
        <tkEuformElement:formSelectBox idKey="contact-title" labelKey="${title1}" path="title" mandatory="false" skipBlank="false" qaAttribute="contact-form-title" skipBlankMessageKey="contact.title.pleaseSelect" items="${titles}" selectCSSClass="form-control selectpicker" />
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <spring:theme  code="contact.page.first.name" var="firstName"></spring:theme>
        <tkEuformElement:formInputBox idKey="contact-firstname" labelKey="${firstName}" path="firstname" mandatory="true" qaAttribute="contact-form-firstname"/>
    </div>
    <div class="col-md-6">
        <spring:theme  code="contact.page.last.name" var="lastName"></spring:theme>
        <tkEuformElement:formInputBox idKey="contact-lastname" labelKey="${lastName}" path="lastname" mandatory="true" qaAttribute="contact-form-lastname"/>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <spring:theme  code="contact.page.email" var="email"></spring:theme>
        <tkEuformElement:formInputBox idKey="contact-email" labelKey="${email}" path="email" mandatory="true" qaAttribute="contact-form-email"/>
    </div>
    <div class="col-md-6">
        <spring:theme  code="contact.page.company" var="company"></spring:theme>
        <tkEuformElement:formInputBox idKey="contact-company" labelKey="${company}" path="company" qaAttribute="contact-form-company"/>
    </div>
</div>
<div class="row">
    <div class="col-md-12" data-qa-id="contact-form-topic">
        <template:errorSpanField path="topic">
            <div class="control">
                <spring:theme code="contact.page.topic" var="topic"></spring:theme>
                <tkEuformElement:formSelectBox idKey="contact-topic" labelKey="${topic}" path="topic" mandatory="true" skipBlank="false" qaAttribute="contact-form-title" skipBlankMessageKey="contact.topic.pleaseSelect" items="${topics}" selectCSSClass="form-control selectpicker always-validate" />
            </div>
        </template:errorSpanField>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <spring:theme code="contact.page.message" var="messageLabel" />
        <tkEuformElement:formTextArea idKey="contact-message" labelKey="${messageLabel}" labelCSS="control-label" path="message" mandatory="true" maxlength="5000" areaCSS="form-control js-contact-message" qaAttribute="contact-form-message"/>
        <p class="js-contact-message-limit help-block help-block--counter pull-right"></p>
    </div>
</div>

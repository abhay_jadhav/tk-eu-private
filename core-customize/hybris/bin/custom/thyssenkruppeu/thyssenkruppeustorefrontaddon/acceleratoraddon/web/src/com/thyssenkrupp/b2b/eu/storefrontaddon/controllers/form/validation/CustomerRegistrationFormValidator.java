package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.RegistrationForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkEuContactForm;


@Component("customerRegistrationFormValidator")
public class CustomerRegistrationFormValidator implements Validator {

    private static final int MAX_FIELD_LENGTH = 255;
    private static final String CUSTOMER_TYPE = "New";

    @Resource(name = "textAreaPatternValidator")
    private DefaultTextAreaValidator textAreaPatternValidator;

    @Override
    public boolean supports(final Class<?> aClass) {
        return TkEuContactForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        final RegistrationForm customerRegistrationForm = (RegistrationForm) object;
        validateTkEuContactFields(customerRegistrationForm, errors);
    }

    protected void validateTkEuContactFields(final RegistrationForm customerReistrationForm, final Errors errors) {
        validateStringField(customerReistrationForm.getFirstName(), ContactField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
        validateStringField(customerReistrationForm.getLastName(), ContactField.LASTNAME, MAX_FIELD_LENGTH, errors);
        validateStringField(customerReistrationForm.getEmail(), ContactField.EMAIL, MAX_FIELD_LENGTH, errors);
        validateXssScriptField(customerReistrationForm.getPhoneNumber(), ContactField.PHONE_NO, errors);
        validateNotEmptyField(customerReistrationForm.getAcceptanceCheck(), ContactField.ACCEPTANCE, errors);
        if(StringUtils.isNotEmpty(customerReistrationForm.getCustomerType()) && customerReistrationForm.getCustomerType().equals(CUSTOMER_TYPE)) {
            validateStringField(customerReistrationForm.getCompany(), ContactField.COMPANY,MAX_FIELD_LENGTH, errors);
            validateNotEmptyField(customerReistrationForm.getBusiness(), ContactField.BUSINESS, errors);
        }else {
            validateStringField(customerReistrationForm.getCustomerNumber(), ContactField.CUSTOMERNUMBER, MAX_FIELD_LENGTH, errors);
        }
    }

    
    private void validateNotEmptyField(String field, ContactField fieldType, Errors errors) {
        if (StringUtils.isEmpty(field)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }
    private void validateXssScriptField(String field, ContactField fieldType, Errors errors) {
        if (!textAreaPatternValidator.validateTextAreaPattern(field)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }
    protected void validateStringField(final String contactField, final ContactField fieldType, final int maxFieldLength, final Errors errors) {
        if (StringUtils.isEmpty(contactField) || (StringUtils.length(contactField) > maxFieldLength)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        } else if (!textAreaPatternValidator.validateTextAreaPattern(contactField)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }
    
    protected enum ContactField {
        CUSTOMERNUMBER("customerNumber", "register.customerNumber.invalid"),
        FIRSTNAME("firstName", "register.firstname.invalid"), 
        LASTNAME("lastName", "register.lastname.invalid"), 
        EMAIL("email", "register.email.invalid"), 
        COMPANY("company", "register.company.invalid"),
        PHONE_NO("phoneNumber", "register.phone.invalid"),
        BUSINESS("business", "register.business.invalid"),
        ACCEPTANCE("acceptanceCheck", "register.acceptance.invalid");
        
        private String fieldKey;
        private String errorKey;

        ContactField(final String fieldKey, final String errorKey) {
            this.fieldKey = fieldKey;
            this.errorKey = errorKey;
        }

        public String getFieldKey() {
            return fieldKey;
        }

        public String getErrorKey() {
            return errorKey;
        }
    }
}

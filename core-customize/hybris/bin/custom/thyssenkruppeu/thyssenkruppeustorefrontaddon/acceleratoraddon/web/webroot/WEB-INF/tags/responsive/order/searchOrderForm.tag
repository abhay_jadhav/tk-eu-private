<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="formID" required="true" type="String" %>
<%@ attribute name="formActionUrl" required="true" type="String" %>
<%@ attribute name="formTitle" required="true" type="String" %>
<%@ attribute name="searchResultCount" required="false" type="Integer" %>
<%@ attribute name="searchKey" required="false" type="String" %>
<%@ attribute name="searchPlaceholder" required="false" type="String" %>
<%@ attribute name="searchButtonText" required="false" type="String" %>
<%@ attribute name="resetButtonText" required="false" type="String" %>
<%@ attribute name="resetUrl" required="false" type="String" %>
<%@ attribute name="resetUrlLabel" required="false" type="String" %>
<%@ attribute name="qaAttributeTitle" required="false" type="String" %>
<%@ attribute name="qaAttributeSearchBtn" required="false" type="String" %>
<%@ attribute name="qaAttributeTextbox" required="false" type="String" %>
<%@ attribute name="qaAttributeResetBtn" required="false" type="String" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:form id="${formID}" cssClass="m-order-form" action="${formActionUrl}" method="GET">
    <h4 class="m-order-form__title" data-qa-id="${qaAttributeTitle}">
            ${formTitle}&nbsp;
        <c:if test="${searchResultCount eq 0 and searchKey ne null}">
            <a href="${resetUrl}">
                ${resetUrlLabel}
            </a>
        </c:if>
    </h4>
    <c:if test="${searchResultCount ne 0 or not empty searchKey}">
        <input type="hidden" name="init" value="true"/>
        <div class="row">
            <div class="col-md-12">
                <div class="input-group input-group-sm">
                    <input class="form-control text-sm m-order-form__input js-search-input" id="orderDeliverySearch" name="searchKey" value="${searchKey}" type="text" placeholder="${searchPlaceholder}" data-qa-id="${qaAttributeTextbox}"/>
                    <span class="input-group-btn">
                        <button id="search" type="submit" class="btn btn-sm btn-primary m-order-form__submit js-search-submit" data-qa-id="${qaAttributeSearchBtn}">
                            ${searchButtonText}
                        </button>
                        <c:if test="${not empty searchKey}">
                            <a href="${resetUrl}" class="btn btn-sm btn-info m-order-form__submit" data-qa-id="${qaAttributeResetBtn}">
                                ${resetButtonText}
                            </a>
                        </c:if>
                    </span>
                </div>
            </div>
        </div>
    </c:if>
</form:form>

/*
 * Put your copyright text here
 */
package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import static com.thyssenkrupp.b2b.eu.core.utils.TkEuProductUtils.mergeSelectedVvc;
import static com.thyssenkrupp.b2b.eu.core.utils.TkEuProductUtils.validateAndSetSelection;
import static com.thyssenkrupp.b2b.eu.facades.constants.ThyssenkruppeufacadesConstants.ORDER_ENTRY_PARAM;
import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_PRODUCT_CART_ENTRY_PRICE_PANEL;
import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_PRODUCT_DETAILS_PANEL;
import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_PRODUCT_WEIGHT_PANEL;
import static com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.controllers.pages.ProductCertificateConfiguratorController.CERTIFICATE_CONFIGURATOR_TYPE;
import static de.hybris.platform.commercefacades.product.ProductOption.BASIC;
import static de.hybris.platform.commercefacades.product.ProductOption.CATEGORIES;
import static de.hybris.platform.commercefacades.product.ProductOption.CLASSIFICATION;
import static de.hybris.platform.commercefacades.product.ProductOption.CUT_TO_LENGTH;
import static de.hybris.platform.commercefacades.product.ProductOption.DESCRIPTION;
import static de.hybris.platform.commercefacades.product.ProductOption.GALLERY;
import static de.hybris.platform.commercefacades.product.ProductOption.PRICE;
import static de.hybris.platform.commercefacades.product.ProductOption.STOCK;
import static de.hybris.platform.commercefacades.product.ProductOption.SUMMARY;
import static de.hybris.platform.commercefacades.product.ProductOption.UOM_CONVERSION;
import static de.hybris.platform.commercefacades.product.ProductOption.URL;
import static de.hybris.platform.commercefacades.product.ProductOption.VARIANT_FIRST_VARIANT;
import static de.hybris.platform.commercefacades.product.ProductOption.VARIANT_MATRIX;
import static de.hybris.platform.commercefacades.product.ProductOption.VARIANT_MATRIX_BASE;
import static de.hybris.platform.commercefacades.product.ProductOption.VARIANT_MATRIX_MEDIA;
import static de.hybris.platform.commercefacades.product.ProductOption.VARIANT_MATRIX_URL;
import static de.hybris.platform.commercefacades.product.ProductOption.VOLUME_PRICES;
import static java.util.Objects.isNull;
import static org.apache.commons.collections.CollectionUtils.size;
import static org.apache.commons.lang.BooleanUtils.isFalse;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuInvalidCartException;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuProductUtils;
import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;
import com.thyssenkrupp.b2b.eu.facades.order.TkEuCartFacade;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuLastViewedProductsFacade;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuPriceDataFactory;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductClassificationFacade;
import com.thyssenkrupp.b2b.eu.facades.product.TkEuProductFacade;
import com.thyssenkrupp.b2b.eu.facades.product.data.SalesUnitData;
import com.thyssenkrupp.b2b.eu.storefrontaddon.builders.TkEuProductBreadcrumbBuilder;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkEuAddToCartForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.ConfigurationSelectionDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.dto.CutToLengthSelectionDto;
import com.thyssenkrupp.b2b.eu.storefrontaddon.forms.VariantSelectionForm;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.CutToLengthSelectionHelper;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.ProductPageHelper;
import com.thyssenkrupp.b2b.eu.storefrontaddon.service.VariantAttributeSelectionHelper;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.ProductPageController;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductCutToLengthData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

@RequestMapping(value = "/**/p")
public class TkEuProductPageController extends ProductPageController {

    private static final String ERROR_MSG_TYPE = "errorMsg";
    private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
    private static final String BASE_PRODUCT = "baseProductData";
    private static final String PRODUCT = "product";
    private static final String VARIANT_SELECTION_FORM = "variantSelectionForm";
    private static final String VARIANT_MATRIX_MODEL = "unselectedVariantMatrixListModel";
    private static final String VARIANT_RESET_FLAG = "variantResetFlag";
    private static final String MERGED_VISIBLE_FEATURE_MAP = "mergedVisibleFeatureMap";
    private static final String GALLERY_IMAGES = "galleryImages";
    private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
    private static final String CERTIFICATE_PRODUCT = "certificateProduct";
    private static final String SELECTED_CERTIFICATE_ID = "selectedCertificateId";
    private static final String SELECTED_TRADELENGTH = "selectedTradelength";
    private static final String SELECTED_UOM = "selectedUom";
    private static final String SELECTED_QTY = "selectedQty";
    private static final String AUTO_SELECTED_VARIANT_CODE = "autoSelectedVariantCode";
    private static final String IS_VARIANT_AUTO_SELECTED = "isVariantAutoSelected";
    private static final Logger LOGGER = Logger.getLogger(TkEuProductPageController.class);
    private static final String PROMO_SALE = "promo_sale";
    private static final String SALES_CATEGORY_LIST = "category.pdp.autoselect.exclude.vc_names";
    private static final String ATP_SAP_WNAE_ENABLED = "atp.sap.type.wnae.enabled";

    @Resource(name = "b2bProductService")
    private TkEuB2bProductService b2bProductService;

    @Resource(name = "productDataUrlResolver")
    private UrlResolver<ProductData> productDataUrlResolver;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "tkB2bCategoryService")
    private TkB2bCategoryService tkB2bCategoryService;

    @Resource(name = "productVariantFacade")
    private TkEuProductFacade productFacade;

    @Resource(name = "tkEuProductBreadcrumbBuilder")
    private TkEuProductBreadcrumbBuilder tkEuProductBreadcrumbBuilder;

    @Resource(name = "tkEuLastViewedProductsFacade")
    private TkEuLastViewedProductsFacade tkEuLastViewedProductsFacade;

    @Resource(name = "tkEuProductClassificationFacade")
    private TkEuProductClassificationFacade tkEuProductClassificationFacade;

    @Resource(name = "cartFacade")
    private TkEuCartFacade cartFacade;

    @Resource(name = "tkEuPriceDataFactory")
    private TkEuPriceDataFactory priceDataFactory;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "variantAttributeSelectionHelper")
    private VariantAttributeSelectionHelper attributeSelectionHelper;

    @Resource(name = "cutToLengthSelectionHelper")
    private CutToLengthSelectionHelper cutToLengthSelectionHelper;

    @Resource(name = "productPageHelper")
    private ProductPageHelper productPageHelper;

    @Resource(name = "cartService")
    private CartService cartService;

    @Resource(name = "b2bCustomerFacade")
    private TkEuB2bCustomerFacade b2bCustomerFacade;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Override
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final List<ProductOption> extraOptions = Arrays.asList(VARIANT_MATRIX_BASE, VARIANT_MATRIX_URL,
                VARIANT_MATRIX_MEDIA, CATEGORIES, CLASSIFICATION, SUMMARY, DESCRIPTION, CUT_TO_LENGTH);

        String baseProductCode = productCode;
        if (!productFacade.isBaseProduct(productCode)) {
            baseProductCode = productFacade.getBaseProductCodeForVariant(productCode);
        }

        final Optional<AbstractOrderEntryModel> maybeOrderEntry = getOrderEntryFromRequest(request);
        if (maybeOrderEntry.isPresent()) {
            return fetchVariantSelectionFromDeepLink(productCode, model, maybeOrderEntry, request, response);
        }

        ProductData productData = productFacade.getProductForCodeAndOptions(baseProductCode, extraOptions);
        final Optional<VariantProductModel> singleVariantProductModel = productFacade
                .singleVariantProduct(baseProductCode);

        if (singleVariantProductModel.isPresent()) {
            return fetchSingleVariantAttributes(productData.getUnselectedVariantMatrixList(),
                    singleVariantProductModel.get().getCode(), model, request, response);
        }

        tkEuLastViewedProductsFacade.storeViewedProduct(baseProductCode);
        tkEuProductClassificationFacade.extractVisibleClassificationFeatures(productData);
        model.addAttribute(MERGED_VISIBLE_FEATURE_MAP, productData.getVisibleFeatures());
        model.addAttribute(BASE_PRODUCT, productData);
        final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
        if (isNotEmpty(redirection)) {
            return redirection;
        }
        updatePageTitle(baseProductCode, model);
        populateModelData(baseProductCode, model, request, extraOptions);
        if (!model.containsAttribute(VARIANT_SELECTION_FORM)) {
            VariantSelectionForm variantSelectionForm = new VariantSelectionForm();
            Map<String, String> variantSelectionMap = new HashMap<>();
            variantSelectionForm.setVariantSelectionMap(variantSelectionMap);
            model.addAttribute(variantSelectionForm);
        }
        if (productData.getCategories() != null) {
            model.addAttribute("totalVariantValues", size(productData.getCategories()));
        }
        setUpMetaData(model, baseProductCode, productData);
        return getViewForPage(model);
    }

    private String fetchVariantSelectionFromDeepLink(String productCode, Model model,
            Optional<AbstractOrderEntryModel> maybeOrderEntry, final HttpServletRequest request,
            final HttpServletResponse response) throws CMSItemNotFoundException, UnsupportedEncodingException {
        AbstractOrderEntryModel orderEntry = maybeOrderEntry.get();
        final Optional<Map<String, String>> selectedVariantMap = attributeSelectionHelper
                .generateSelectedVariantAttribute(orderEntry);
        final Optional<CertificateConfiguredProductInfoModel> selectedCertificate = attributeSelectionHelper
                .getSelectedCertificateFromProductInfo(orderEntry);
        if (selectedVariantMap.isPresent() || selectedCertificate.isPresent()) {
            VariantSelectionForm form = new VariantSelectionForm();
            setSelectedCutToLengthConfigurationToForm(form, orderEntry);
            selectedVariantMap.ifPresent(form::setVariantSelectionMap);
            setSelectedCertificatesToFormAndModel(selectedCertificate, form, model);
            attributeSelectionHelper.getSelectedTradeLengthVariantValueCategory(orderEntry)
                    .ifPresent(form::setSelectedTradeLength);
            return productDetailPost(productCode, form, null, model, request, response);
        }
        return getViewForPage(model);
    }

    private void setUpMetaData(Model model, String baseProductCode, ProductData productData) {
        final ProductModel productModel = productService.getProductForCode(baseProductCode);
        cutToLengthSelectionHelper.generateRangeAndToleranceData(productModel.getVariants())
                .ifPresent(o -> setCutToLengthDataToModel(model, new VariantSelectionForm(), o));
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
        super.setUpMetaData(model, metaKeywords, metaDescription);
    }

    private void setSelectedCertificatesToFormAndModel(
            final Optional<CertificateConfiguredProductInfoModel> selectedCertificate, final VariantSelectionForm form,
            final Model model) {
        selectedCertificate.ifPresent(certificate -> {
            form.setSelectedCertificateId(certificate.getCertificateId());
            model.addAttribute(CERTIFICATE_PRODUCT, Boolean.TRUE);
            model.addAttribute(SELECTED_CERTIFICATE_ID, certificate.getCertificateId());
        });
    }

    private void setSelectedCutToLengthConfigurationToForm(final VariantSelectionForm form,
            final AbstractOrderEntryModel orderEntry) {
        final Optional<TkCutToLengthConfiguredProductInfoModel> selectedCutToLength = cutToLengthSelectionHelper
                .getSelectedCutToLengthConfiguredProductInfoModelFromProductInfo(orderEntry);
        if (selectedCutToLength.isPresent()) {
            populateSelectedCutToLengthDataToForm(form, selectedCutToLength.get());
        }
    }

    private void populateSelectedCutToLengthDataToForm(final VariantSelectionForm form,
            final TkCutToLengthConfiguredProductInfoModel tkCutToLengthProductInfoModel) {
        final Double rangeValue = tkCutToLengthProductInfoModel.getValue();
        if (Objects.nonNull(rangeValue)) {
            form.setSelectedCutToLengthRange(TkCommonUtil.stripTrailingZeros(rangeValue));
        }
        form.setSelectedCutToLengthTolerance(tkCutToLengthProductInfoModel.getToleranceValue());
        final UnitModel unit = tkCutToLengthProductInfoModel.getUnit();
        if (Objects.nonNull(unit)) {
            form.setSelectedCutToLengthRangeUnit(unit.getSapCode());
        }
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/updateWeightPanel", method = { RequestMethod.POST })
    public String updateWeightPanel(@PathVariable final String productCode,
            @ModelAttribute("uomSelectorForm") final TkEuAddToCartForm uomSelectorForm, final Model model) {
        final List<ProductOption> options = new ArrayList<>(Arrays.asList(BASIC));
        final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, options);
        model.addAttribute(PRODUCT, productData);
        if (productData.getSupportedSalesUnits() != null) {
            SalesUnitData selectedSalasUnitData = productFacade
                    .findSelectedSalasUnitData(productData.getSupportedSalesUnits(), uomSelectorForm.getSalesUom());
            populateSalesUnitData(uomSelectorForm.getQty(), model, selectedSalasUnitData);
        }
        return VIEWS_FRAGMENT_PRODUCT_WEIGHT_PANEL;
    }

    private void populateSalesUnitData(@ModelAttribute("uomSelectorForm") long qty, Model model,
            SalesUnitData selectedSalasUnitData) {
        model.addAttribute("selectedSalesUnitData", selectedSalasUnitData);
        if (selectedSalasUnitData != null) {
            double totalWeight = selectedSalasUnitData.getWeightPerSalesUnit() * qty;
            // CTKS-4714 , use unit.name instead of string literal "kg"
            PriceData totalWeightPriceData = priceDataFactory.createWithoutCurrency(PriceDataType.BUY,
                    BigDecimal.valueOf(totalWeight), " kg");
            model.addAttribute("totalWeightInKg", totalWeightPriceData);
        }
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/" + CERTIFICATE_CONFIGURATOR_TYPE, method = {
            RequestMethod.POST })
    @RequireHardLogIn
    public String updateProductConfigurations(@PathVariable final String productCode,
            @ModelAttribute("certificateConfigurationForm") final TkEuAddToCartForm tkEuAddToCartForm,
            final Model model, HttpServletResponse response) {
        boolean isValidCart = prepareCart(productCode, tkEuAddToCartForm, model);
        return updateResponseForInvalidCart(isValidCart, model, VIEWS_FRAGMENT_PRODUCT_CART_ENTRY_PRICE_PANEL,
                tkEuAddToCartForm);
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/updateSalesUom", method = { RequestMethod.POST })
    @RequireHardLogIn
    public String updateSalesUom(@PathVariable final String productCode,
            @ModelAttribute("uomSelectorForm") final TkEuAddToCartForm tkEuAddToCartForm, final Model model,
            HttpServletResponse response) {
        boolean isValidCart = prepareCart(productCode, tkEuAddToCartForm, model);
        return updateResponseForInvalidCart(isValidCart, model, VIEWS_FRAGMENT_PRODUCT_DETAILS_PANEL,
                tkEuAddToCartForm);
    }

    private boolean prepareCart(@PathVariable String productCode,
            @ModelAttribute("certificateConfigurationForm") TkEuAddToCartForm tkEuAddToCartForm, Model model) {
        final ConfigurationSelectionDto dto = ConfigurationSelectionDto.builder()
                .withCertificateId(tkEuAddToCartForm.getCertOption())
                .withRange(tkEuAddToCartForm.getSelectedCutToLengthRange())
                .withRangeUnit(tkEuAddToCartForm.getSelectedCutToLengthRangeUnit())
                .withTolerance(tkEuAddToCartForm.getSelectedCutToLengthTolerance())
                .withTradeLength(tkEuAddToCartForm.getSelectedTradeLength()).build();
        final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(BASIC));
        model.addAttribute(PRODUCT, productData);
        final List<ConfigurationInfoData> configurationInfoData = productPageHelper.generateConfigurationInfoData(dto);
        return setInMemoryCart(model, productCode, tkEuAddToCartForm.getSalesUom(), tkEuAddToCartForm.getQty(),
                configurationInfoData, false);
    }

    private String updateResponseForInvalidCart(boolean isValidCart, Model model, String targetPath,
            TkEuAddToCartForm uomSelectorForm) {
        model.addAttribute(CERTIFICATE_PRODUCT, Boolean.TRUE);
        if (!isValidCart) {
            model.addAttribute("requestForAvailability", Boolean.TRUE);
            model.addAttribute(SELECTED_TRADELENGTH, uomSelectorForm.getSelectedTradeLength());
            model.addAttribute(SELECTED_UOM, uomSelectorForm.getSalesUom());
            model.addAttribute(SELECTED_QTY, uomSelectorForm.getQty());

            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_PRODUCT_REQUEST_FOR_AVAILABILITY;
        }
        return targetPath;
    }

    // TODO : Refactor the usage of ProductModel and ProductData, it is been called
    // so frequently

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = { RequestMethod.POST })
    public String productDetailPost(@PathVariable final String productCode,
            @ModelAttribute final VariantSelectionForm variantSelectionForm, final BindingResult result,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException {

        List<ProductOption> baseOptions = Arrays.asList(VARIANT_MATRIX, VARIANT_MATRIX_URL, VARIANT_MATRIX_MEDIA,
                CATEGORIES, SUMMARY, DESCRIPTION, CUT_TO_LENGTH);
        tkEuLastViewedProductsFacade.storeViewedProduct(productCode);
        final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, baseOptions);
        productData.setBaseProduct(productFacade.getBaseProductCodeForVariant(productCode));
        setSelectionsOnVariantCategoryValues(model, variantSelectionForm, productData);
        String selectedVariantCode = isVariantAutoSelected(model)
                ? (String) model.asMap().get(AUTO_SELECTED_VARIANT_CODE)
                : productCode;
        final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
        if (isNotEmpty(redirection)) {
            return redirection;
        }
        updatePageTitle(selectedVariantCode, model);
        int valueCategoryCount = productFacade.getVariantValueCategoriesCount(selectedVariantCode);
        model.addAttribute("totalVariantValues", valueCategoryCount);
        model.addAttribute(SELECTED_TRADELENGTH, variantSelectionForm.getSelectedTradeLength());
        model.addAttribute("isSubmitTriggeredBySawing", variantSelectionForm.isSubmitTriggeredBySawing());

        UserModel currentUser = userService.getCurrentUser();
        boolean isAnonymousUser = userService.isAnonymousUser(currentUser);
        int vvcCount = isSawingAlreadySelected(model) ? valueCategoryCount - 1 : valueCategoryCount;
        if (productFacade.isVariantSelected(variantSelectionForm.getVariantSelectionMap(), vvcCount)) {
            model.addAttribute("isVariantSelected", Boolean.TRUE);
            CartModel sessionCart = cartService.getSessionCart();
            boolean isAddressAvailableInCart = (sessionCart.getDeliveryAddress() != null);
            model.addAttribute("primaryShippingAddressData",
                    b2bCustomerFacade.getPrimaryShippingAddressOfCurrentUser());
            model.addAttribute("isAddressAvailableInCart", isAddressAvailableInCart);

            final ConfigurationSelectionDto dto = ConfigurationSelectionDto.builder()
                    .withCertificateId(variantSelectionForm.getSelectedCertificateId())
                    .withRange(variantSelectionForm.getSelectedCutToLengthRange())
                    .withRangeUnit(variantSelectionForm.getSelectedCutToLengthRangeUnit())
                    .withTolerance(variantSelectionForm.getSelectedCutToLengthTolerance())
                    .withTradeLength(variantSelectionForm.getSelectedTradeLength()).build();
            final List<ConfigurationInfoData> configurationInfoData = productPageHelper
                    .generateConfigurationInfoData(dto);
            if (variantSelectionForm.getSelectedCertificateId() != null
                    && model.asMap().get(SELECTED_CERTIFICATE_ID) == null) {
                model.addAttribute(CERTIFICATE_PRODUCT, Boolean.TRUE);
                model.addAttribute(SELECTED_CERTIFICATE_ID, variantSelectionForm.getSelectedCertificateId());
            }
            productVariantSelected(selectedVariantCode, model, request, baseOptions, productData, isAnonymousUser,
                    configurationInfoData);
        } else {
            populateModelData(selectedVariantCode, model, request, baseOptions);
        }
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);
        return getViewForPage(model);
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/" + "cutToLengthSelection", method = {
            RequestMethod.POST })
    public String updateCutToLengthConfigurations(@PathVariable final String productCode,
            @ModelAttribute("cutToLengthSelectionForm") final VariantSelectionForm variantSelectionForm,
            final BindingResult result, final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws CMSItemNotFoundException, UnsupportedEncodingException {
        final String selectedType = variantSelectionForm.getSawingType();
        final Map<String, String> variantSelectionMap = MapUtils.isNotEmpty(
                variantSelectionForm.getVariantSelectionMap()) ? variantSelectionForm.getVariantSelectionMap()
                        : Collections.emptyMap();
        final Double selectedRange = isNotEmpty(variantSelectionForm.getSelectedCutToLengthRange())
                ? Double.valueOf(variantSelectionForm.getSelectedCutToLengthRange())
                : null;
        final String selectedTolerance = variantSelectionForm.getSelectedCutToLengthTolerance();
        if (Objects.nonNull(selectedType)) {
            Optional<ProductModel> maybeBaseProduct = productFacade.getBaseProductForCode(productCode);
            if (maybeBaseProduct.isPresent()) {
                final CutToLengthSelectionDto dto = cutToLengthSelectionHelper.createCutToLengthSelectionDto(
                        selectedRange, selectedTolerance, selectedType,
                        variantSelectionForm.isSelectedCutToLengthRangeInvalid());
                final List<VariantProductModel> variantsFilteredBySelectedVvc = productFacade
                        .getAllVariantsFilteredBySelectedVvc(maybeBaseProduct.get(), variantSelectionMap);
                final Optional<ProductCutToLengthData> maybeCutToLengthData = cutToLengthSelectionHelper
                        .generateRangeAndToleranceData(variantsFilteredBySelectedVvc, dto);
                maybeCutToLengthData.ifPresent(o -> setCutToLengthDataToModel(model, variantSelectionForm, o));
            }
        }
        if (MapUtils.isNotEmpty(variantSelectionMap)) {
            variantSelectionForm.setVariantSelectionMap(variantSelectionMap);
            model.addAttribute(VARIANT_RESET_FLAG, TkEuProductUtils.areAllValuesEmpty(variantSelectionMap));
        }
        model.addAttribute(VARIANT_SELECTION_FORM, variantSelectionForm);
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_FRAGMENT_PRODUCT_SAWING_PANEL;
    }

    private void productVariantSelected(@PathVariable String productCode, Model model, HttpServletRequest request,
            List<ProductOption> baseOptions, ProductData productData, boolean isAnonymousUser,
            List<ConfigurationInfoData> configurationInfoData) throws CMSItemNotFoundException {
        LOGGER.info("Variant Selected. Proceeding with data population and fetching prices");
        List<ProductOption> extraOptions = new ArrayList<>(
                Arrays.asList(URL, PRICE, CLASSIFICATION, VOLUME_PRICES, STOCK, UOM_CONVERSION));
        extraOptions.addAll(baseOptions);
        populateModelData(productCode, model, request, extraOptions);
        final ProductData product = (ProductData) model.asMap().get(PRODUCT);
        populateProductConfiguration(product, model);
        if (product.getPurchasable()) {
            setInMemoryCart(model, productCode, "", 1L, configurationInfoData, isAnonymousUser);
        } else if (!isAnonymousUser) {
            LOGGER.error("Product Not purchasable");
        }
    }

    private boolean setInMemoryCart(final Model model, String productCode, String salesUom, long productQuantity,
            List<ConfigurationInfoData> configurationInfoData, boolean isAnonymousUser) {
        if (productFacade.isPurchasableProduct(productCode) && !isAnonymousUser) {
            try {
                AddToCartParams addToCartParams = prepareAddToCartParams(productCode, salesUom, productQuantity,
                        configurationInfoData);
                CartData cartData = createInMemoryCartAndPerformAddToCart(addToCartParams, model);
                if (cartData != null && CollectionUtils.isNotEmpty(cartData.getEntries())) {
                    if (TkEuConfigKeyValueUtils.getBoolean(ATP_SAP_WNAE_ENABLED, false)) {
                        sessionService.getCurrentSession().setAttribute("InMemoryCartEntry",
                                updateConfigurationInfo(cartData.getEntries().get(0), configurationInfoData));
                    }
                    model.addAttribute("cartEntryData", cartData.getEntries().get(0));
                    model.addAttribute("ZeroPrice", priceDataFactory.create(PriceDataType.BUY, BigDecimal.ZERO,
                            cartData.getTotalPrice().getCurrencyIso()));
                }
            } catch (CommerceCartModificationException | TkEuInvalidCartException ex) {
                model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
                LOGGER.error("Error occurred while addToCart", ex);
                return false;
            } catch (Exception ex) {
                LOGGER.error("Error occurred while addToCart", ex);
                return false;
            }
        } else if (!isAnonymousUser) {
            LOGGER.error("Product Not purchasable");
            return false;
        }
        return true;
    }

    private Optional<AbstractOrderEntryModel> getOrderEntryFromRequest(final HttpServletRequest request) {
        return attributeSelectionHelper.getOrderEntryForId(request.getParameter(ORDER_ENTRY_PARAM));
    }

    private AddToCartParams prepareAddToCartParams(String productCode, String salesUom, long productQuantity,
            List<ConfigurationInfoData> configurationInfoData) {
        final AddToCartParams addToCartParams = new AddToCartParams();
        addToCartParams.setProductCode(productCode);
        addToCartParams.setQuantity(productQuantity);
        addToCartParams.setProductConfiguration(configurationInfoData);
        addToCartParams.setSalesUom(salesUom);
        return addToCartParams;
    }

    private CartData createInMemoryCartAndPerformAddToCart(AddToCartParams addToCartParams, Model model)
            throws CommerceCartModificationException {
        final CartModel inMemoryCart = createInMemoryCart();
        final CommerceCartModification modification = cartFacade.inMemoryAddToCart(addToCartParams, inMemoryCart);
        if (modification != null && !CommerceCartModificationStatus.SUCCESS.equals(modification.getStatusCode())) {
            model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
            LOGGER.error(String.format("Error while adding product:%s to InMemoryCart, statusCode:%s",
                    addToCartParams.getProductCode(), modification.getStatusCode()));
        }
        return cartFacade.converterInMemoryCart(inMemoryCart);
    }

    private CartModel createInMemoryCart() {
        final CartModel inMemoryCart = cartFacade.createInMemoryCart();
        inMemoryCart.setEntries(Collections.emptyList());
        inMemoryCart.setNet(true);
        return inMemoryCart;
    }

    protected void populateModelData(final String productCode, final Model model, final HttpServletRequest request,
            final List<ProductOption> extraOptions) throws CMSItemNotFoundException {
        populateProductDetailForDisplay(productCode, model, request, extraOptions);
        model.addAttribute(new ReviewForm());
        model.addAttribute("pageType", PageType.PRODUCT.name());
        model.addAttribute("futureStockEnabled", Boolean.valueOf(Config.getBoolean(FUTURE_STOCK_ENABLED, false)));
    }

    @Override
    protected void populateProductDetailForDisplay(final String productCode, final Model model,
            final HttpServletRequest request, final List<ProductOption> extraOptions) throws CMSItemNotFoundException {
        final ProductModel productModel = productService.getProductForCode(productCode);
        getRequestContextData(request).setProduct(productModel);
        final List<ProductOption> options = new ArrayList<>(Arrays.asList(VARIANT_FIRST_VARIANT, BASIC, GALLERY));
        options.addAll(extraOptions);
        final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, options);
        sortVariantOptionData(productData);
        tkEuProductClassificationFacade.extractVisibleClassificationFeatures(productData);
        storeCmsPageInModel(model, getPageForProduct(productCode));
        populateProductData(productData, model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, tkEuProductBreadcrumbBuilder.getBreadcrumbs(productCode));
        productData.setBaseProduct(productFacade.getBaseProductCodeForVariant(productCode));
        if (productFacade.isBaseProduct(productCode) && !isSawingAlreadySelected(model)) {
            productData.setVariantResetFlag(true);
            model.addAttribute(VARIANT_MATRIX_MODEL, productData.getUnselectedVariantMatrixList());
            model.addAttribute(VARIANT_RESET_FLAG, true);
        } else {
            populateBaseProductData(model, productData);
        }
    }

    @Override
    protected void populateProductData(final ProductData productData, final Model model) {
        model.addAttribute("galleryImages", getGalleryImages(productData));
        model.addAttribute(PRODUCT, productData);
    }

    private void populateProductConfiguration(@NotNull ProductData productData, Model model) {
        if (productData.getConfigurable()) {
            final List<ConfigurationInfoData> configurations = productFacade
                    .getConfiguratorSettingsForCode(productData.getCode());
            if (CollectionUtils.isNotEmpty(configurations)) {
                model.addAttribute("configuratorType", configurations.get(0).getConfiguratorType());
                model.addAttribute("certificateProduct",
                        configurations.stream().anyMatch(configurationInfoData -> configurationInfoData
                                .getConfiguratorType() == ConfiguratorType.CERTIFICATE));
                model.addAttribute("configurations", configurations);
            }
        }
    }

    private void setSelectionsOnVariantCategoryValues(final Model model, VariantSelectionForm variantSelectionForm,
            ProductData productData) {
        Map<String, String> selectionMap = instantiateSelectionMap(variantSelectionForm);
        String selectedVariantCategory = variantSelectionForm.getSelectedVariantCategory();
        String selectedVariantValueCategory = variantSelectionForm.getSelectedVariantValueCategory();
        final ProductModel baseProductModel = productService.getProductForCode(productData.getBaseProduct());
        validateAndSetSelection(selectionMap, selectedVariantCategory, selectedVariantValueCategory,
                variantSelectionForm.isSelectionIgnore());
        List<VariantProductModel> filteredVariants = productFacade.getAllVariantsFilteredBySelectedVvc(baseProductModel,
                selectionMap);

        final Optional<List<VariantProductModel>> maybeVariantsFilteredBySawing = filterVariantsOnSawingSelection(
                variantSelectionForm, filteredVariants);
        if (maybeVariantsFilteredBySawing.isPresent()) {
            filteredVariants = maybeVariantsFilteredBySawing.get();
            setSawingSelectionToModel(model);
        }

        if (isNotDeSelection(selectionMap, selectedVariantCategory) || maybeVariantsFilteredBySawing.isPresent()) {
            selectionMap = autoSelectAndReduceVariantSelection(model, baseProductModel, variantSelectionForm,
                    selectionMap, filteredVariants, maybeVariantsFilteredBySawing);
        }

        final Optional<ProductCutToLengthData> maybeCutToLengthData = cutToLengthSelectionHelper
                .generateRangeAndToleranceData(filteredVariants);
        maybeCutToLengthData.ifPresent(o -> setCutToLengthDataToModel(model, variantSelectionForm, o));
        productFacade.populateActiveVariantCategoryValues(filteredVariants, productData);
        validatePreviousSelections(selectionMap, selectedVariantCategory, selectedVariantValueCategory, productData);

        variantSelectionForm.setVariantSelectionMap(selectionMap);

        model.addAttribute(VARIANT_RESET_FLAG,
                TkEuProductUtils.areAllValuesEmpty(selectionMap) && !isSawingAlreadySelected(model));
        model.addAttribute(VARIANT_SELECTION_FORM, variantSelectionForm);
        model.addAttribute(VARIANT_MATRIX_MODEL, productData.getUnselectedVariantMatrixList());
    }

    private boolean isNotDeSelection(Map<String, String> selectionMap, String selectedVariantCategory) {
        return isNotEmpty(selectedVariantCategory) && isNotEmpty(selectionMap.get(selectedVariantCategory));
    }

    private Map<String, String> instantiateSelectionMap(VariantSelectionForm variantSelectionForm) {
        if (variantSelectionForm.isMapResetFlag()) {
            cleanUpCutToLengthFromForm(variantSelectionForm);
            variantSelectionForm.setVariantSelectionMap(new HashMap<>());
        }

        return isNull(variantSelectionForm.getVariantSelectionMap()) ? new HashMap<>()
                : variantSelectionForm.getVariantSelectionMap();
    }

    private Map<String, String> autoSelectAndReduceVariantSelection(Model model, ProductModel baseProduct,
            VariantSelectionForm variantSelectionForm, Map<String, String> paramSelectionMap,
            List<VariantProductModel> filteredVariants,
            Optional<List<VariantProductModel>> maybeVariantsFilteredBySawing) {
        Map<String, String> selectionMap = paramSelectionMap;
        variantSelectionForm.setVariantSelectionMap(paramSelectionMap);
        final boolean isSawingAvailable = maybeVariantsFilteredBySawing.isPresent()
                || cutToLengthSelectionHelper.generateRangeAndToleranceData(filteredVariants).isPresent();
        if (size(filteredVariants) == 1) {
            final VariantProductModel selectedVariant = filteredVariants.get(0);
            return generateSingleVariantAttributeMap(model, variantSelectionForm, selectionMap, selectedVariant);
        } else {
            final Map<String, List<String>> vcToVvcRelationMap = attributeSelectionHelper
                    .generateVcToVvcRelationsMapForVariants(filteredVariants);
            final Map<String, String> singleVcToVvcMap = reduceMapForSingleVcToVvcRelation(vcToVvcRelationMap);
            if (MapUtils.isNotEmpty(singleVcToVvcMap)) {
                for (Map.Entry<String, String> entry : singleVcToVvcMap.entrySet()) {
                    validateTradeLengthVvcAndResetSelection(variantSelectionForm, selectionMap, isSawingAvailable,
                            entry);
                }
                List<VariantProductModel> filteredVariantsByAutoSelectedVvc = productFacade
                        .getAllVariantsFilteredBySelectedVvc(baseProduct, selectionMap);
                if (filteredVariants.removeIf(
                        variantProductModel -> !filteredVariantsByAutoSelectedVvc.contains(variantProductModel))) {
                    selectionMap = autoSelectAndReduceVariantSelection(model, baseProduct, variantSelectionForm,
                            selectionMap, filteredVariants, maybeVariantsFilteredBySawing);
                }
            }
        }
        return selectionMap;
    }

    private void validateTradeLengthVvcAndResetSelection(VariantSelectionForm variantSelectionForm,
            Map<String, String> selectionMap, boolean isSawingAvailable, Map.Entry<String, String> entry) {
        final boolean isTradeLengthVariantCategory = tkB2bCategoryService.isTradeLengthVariantCategory(entry.getKey());

        if (isFalse(isTradeLengthVariantCategory)) {
            selectionMap.put(entry.getKey(), entry.getValue());
        } else if (isTradeLengthVariantCategory && isFalse(isSawingAvailable)) {
            selectionMap.put(entry.getKey(), entry.getValue());
            variantSelectionForm.setSelectedTradeLength(entry.getValue());
        }
    }

    private Map<String, String> reduceMapForSingleVcToVvcRelation(Map<String, List<String>> vcToVvcRelationMap) {
        return vcToVvcRelationMap.entrySet().stream()
                .filter(map -> (map.getValue().size() == 1 && isValidVcCategory(map.getKey())))
                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue().get(0)));
    }

    private boolean isValidVcCategory(String key) {
        boolean isValid = true;
        try {
            List<String> saleCatList = Arrays
                    .asList(TkEuConfigKeyValueUtils.getString(SALES_CATEGORY_LIST, PROMO_SALE).split(","));
            if (CollectionUtils.isNotEmpty(saleCatList)) {
                for (String salesCat : saleCatList) {
                    if (StringUtils.containsIgnoreCase(key, salesCat)) {
                        isValid = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in Checking Sales Category !!", e);
        }
        return isValid;
    }

    private Map<String, String> generateSingleVariantAttributeMap(final Model model,
            VariantSelectionForm variantSelectionForm, final Map<String, String> selectionMap,
            VariantProductModel selectedVariant) {
        final String selectedVariantCategory = variantSelectionForm.getSelectedVariantCategory();
        final String selectedVariantValueCategory = variantSelectionForm.getSelectedVariantValueCategory();
        final Optional<Map<String, String>> selectedVariantAttribute = attributeSelectionHelper
                .generateSelectedVariantAttribute(selectedVariant);
        attributeSelectionHelper.getSelectedTradeLengthVariantValueCategory(selectedVariant)
                .ifPresent(variantSelectionForm::setSelectedTradeLength);
        selectedVariantAttribute
                .ifPresent(autoSelectedMap -> variantSelectionForm.setVariantSelectionMap(mergeSelectedVvc(selectionMap,
                        autoSelectedMap, selectedVariantCategory, selectedVariantValueCategory)));

        model.addAttribute(IS_VARIANT_AUTO_SELECTED, Boolean.TRUE);
        model.addAttribute(AUTO_SELECTED_VARIANT_CODE, selectedVariant.getCode());
        return variantSelectionForm.getVariantSelectionMap();
    }

    void validatePreviousSelections(Map<String, String> selectionMap, String key, String value,
            ProductData productData) {
        for (Map.Entry<String, String> entry : selectionMap.entrySet()) {
            if (!StringUtils.equals(entry.getKey(), key) && !productFacade.isElementStatusActive(productData, value)) {
                selectionMap.replace(key, "");
            }
        }
    }

    private void populateBaseProductData(Model model, ProductData productData) {
        model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT, Boolean.TRUE);
        final List<ProductOption> masterProductOptions = new ArrayList<>(
                Arrays.asList(BASIC, URL, SUMMARY, DESCRIPTION, GALLERY, CLASSIFICATION));
        final ProductData baseProductData = productFacade.getProductForCodeAndOptions(productData.getBaseProduct(),
                masterProductOptions);
        tkEuProductClassificationFacade.extractVisibleClassificationFeatures(baseProductData);
        Map<ClassificationAttributeVisibilityEnum, List<FeatureData>> mergedVisibleFeatureMap = tkEuProductClassificationFacade
                .mergeClassificationFeatures(baseProductData, productData);
        model.addAttribute(MERGED_VISIBLE_FEATURE_MAP, mergedVisibleFeatureMap);
        model.addAttribute(BASE_PRODUCT, baseProductData);
        model.addAttribute(GALLERY_IMAGES, getGalleryImages(baseProductData));
    }

    private String fetchSingleVariantAttributes(List<VariantMatrixElementData> unselectedVariantMatrixList,
            String productCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException {

        VariantSelectionForm variantSelectionForm = new VariantSelectionForm();
        variantSelectionForm.setMapResetFlag(false);
        variantSelectionForm.setSelectedTradeLength(null);

        for (VariantMatrixElementData variantMatrixElementData : unselectedVariantMatrixList) {
            if (!variantMatrixElementData.getParentVariantCategory().isIsTradeLengthCategory()) {
                variantSelectionForm
                        .setSelectedVariantCategory(variantMatrixElementData.getParentVariantCategory().getCode());
                variantSelectionForm
                        .setSelectedVariantValueCategory(variantMatrixElementData.getVariantValueCategory().getCode());
                break;
            }
        }
        variantSelectionForm.setVariantSelectionMap(new HashMap<>());

        model.addAttribute(VARIANT_SELECTION_FORM, variantSelectionForm);
        return productDetailPost(productCode, variantSelectionForm, null, model, request, response);
    }

    private void setCutToLengthDataToModel(final Model model, final VariantSelectionForm form,
            final ProductCutToLengthData cutToLengthData) {
        model.addAttribute("isCutToLengthAvailable", true);
        model.addAttribute("cutToLengthData", cutToLengthData);
        if (isNotEmpty(form.getSelectedCutToLengthRange()) && isNotEmpty(form.getSelectedCutToLengthTolerance())
                && MapUtils.isNotEmpty(cutToLengthData.getTolerances())) {
            model.addAttribute("alreadySelectedRange", form.getSelectedCutToLengthRange());
            model.addAttribute("alreadySelectedRangeUnit", cutToLengthData.getRangeUnit());
            model.addAttribute("alreadySelectedTolerance",
                    cutToLengthData.getTolerances().get(form.getSelectedCutToLengthTolerance()));
        }
    }

    private Optional<List<VariantProductModel>> filterVariantsOnSawingSelection(
            final VariantSelectionForm variantSelectionForm, final Collection<VariantProductModel> variants) {
        if (isNotEmpty(variantSelectionForm.getSelectedCutToLengthRange())
                && isNotEmpty(variantSelectionForm.getSelectedCutToLengthTolerance())) {
            final Double selectedRange = Double.valueOf(variantSelectionForm.getSelectedCutToLengthRange());
            final String selectedTolerance = variantSelectionForm.getSelectedCutToLengthTolerance();
            final CutToLengthSelectionDto dto = cutToLengthSelectionHelper.createCutToLengthSelectionDto(selectedRange,
                    selectedTolerance, null, false);
            final List<VariantProductModel> filteredList = cutToLengthSelectionHelper
                    .getVariantsFilteredByCutToLengthSelection(variants, dto);
            return CollectionUtils.isNotEmpty(filteredList) ? Optional.of(filteredList) : Optional.empty();
        }
        return Optional.empty();
    }

    private void cleanUpCutToLengthFromForm(final VariantSelectionForm form) {
        form.setSelectedCutToLengthRange(null);
        form.setSelectedCutToLengthRangeUnit(null);
        form.setSelectedCutToLengthTolerance(null);
    }

    private void setSawingSelectionToModel(final Model model) {
        model.addAttribute("isSawingAlreadySelected", true);
    }

    private boolean isSawingAlreadySelected(final Model model) {
        return model.containsAttribute("isSawingAlreadySelected");
    }

    private boolean isVariantAutoSelected(final Model model) {
        return model.containsAttribute(IS_VARIANT_AUTO_SELECTED);
    }

    private OrderEntryData updateConfigurationInfo(final OrderEntryData orderEntry,
            List<ConfigurationInfoData> configurationInfoData) {

        if (!orderEntry.getConfigurationInfos().isEmpty()) {
            List<ConfigurationInfoData> configInfos = new ArrayList<ConfigurationInfoData>();
            for (ConfigurationInfoData entryConfigInfo : orderEntry.getConfigurationInfos()) {
                if (!configurationInfoData.isEmpty() && entryConfigInfo.getConfiguratorType().equals(ConfiguratorType.CERTIFICATE)) {

                    for (ConfigurationInfoData configInfo : configurationInfoData) {

                        if (configInfo.getUniqueId().equals(entryConfigInfo.getUniqueId())) {
                            entryConfigInfo.setConfigurationValue("true");
                            configInfos.add(entryConfigInfo);
                            break;
                        }
                    }
                }
                if (entryConfigInfo.getConfiguratorType().equals(ConfiguratorType.TKTRADELENGTH)
                        || entryConfigInfo.getConfiguratorType().equals(ConfiguratorType.CUTTOLENGTH_PRODINFO)) {
                    entryConfigInfo.setConfigurationValue("true");
                    configInfos.add(entryConfigInfo);
                    }
                if (entryConfigInfo.getConfiguratorType().equals(ConfiguratorType.TKCCLENGTH)) {
                    getWidthForCCLength(orderEntry,entryConfigInfo);
                    entryConfigInfo.setConfigurationValue("true");
                    configInfos.add(entryConfigInfo);
                    }

            }

            orderEntry.setConfigurationInfos(configInfos);
        }
        return orderEntry;
    }

    public TkEuB2bProductService getB2bProductService() {
        return b2bProductService;
    }

    public void setB2bProductService(TkEuB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }

    private void getWidthForCCLength(final OrderEntryData orderEntry, final ConfigurationInfoData entryConfigInfo) {
        final ProductModel productModel = productService.getProductForCode(orderEntry.getProduct().getCode());
        if (productModel != null) {
            final Optional<FeatureValue> productWidthFeatureValue = getB2bProductService().getProductWidth(productModel,
                    "XX_WIDTH");
            if (productWidthFeatureValue.isPresent()) {
                entryConfigInfo.setWidthValue(productWidthFeatureValue.get().getValue().toString());
            }
        }
    }
}

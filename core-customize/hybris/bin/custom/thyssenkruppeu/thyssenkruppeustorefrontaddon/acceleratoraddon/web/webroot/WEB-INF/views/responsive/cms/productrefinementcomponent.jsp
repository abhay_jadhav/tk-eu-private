<%@ taglib prefix="tk-nav" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav" %>
<div class="hidden-sm hidden-xs">
    <div id="product-facet" class="m-facet js-product-facet">
        <tk-nav:facetNavAppliedFilters pageData="${searchPageData}" favouriteFacetData="${favouriteFacetData}"/>
        <tk-nav:facetNavRefinements pageData="${searchPageData}" favouriteFacetData="${favouriteFacetData}"/>
    </div>
</div>

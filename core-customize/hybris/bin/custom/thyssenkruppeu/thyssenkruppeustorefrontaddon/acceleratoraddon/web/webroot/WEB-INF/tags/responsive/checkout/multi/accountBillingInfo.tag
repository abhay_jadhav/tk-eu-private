<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common"%>
<c:if test="${not empty cartData.billingAddress}">
    <li class="checkout-order-summary-list-heading col-md-6">
        <div class="title"><spring:theme code="checkout.multi.billing.to" />:</div><br/>
        <div class="address" data-qa-id="order-summary-billing-address">
            <order:address address="${cartData.billingAddress}" />
        </div>
    </li>
</c:if>

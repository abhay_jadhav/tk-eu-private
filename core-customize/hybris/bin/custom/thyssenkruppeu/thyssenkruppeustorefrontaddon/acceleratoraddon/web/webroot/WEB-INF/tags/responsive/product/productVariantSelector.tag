<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>

<c:set var="showAddToCart" value="" scope="session" />
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:theme code="product.variants.inactive.tooltip.text" var="inactiveVariantTooltipText" />

<c:if test="${product.multidimensional}">
    <c:url value="${baseProductData.url}" var="baseProductUrl" />
    <c:url value="${variantSelectionForm.variantSelectionMap}" var="variantSelectionMap" />
    <div class="m-vcs js-product-variants">
        <h3 class="h6 m-vcs__label">
            <spring:theme code="product.variants.select.title" />
            <a class="m-vcs__label__clear js-reset-variant-selection" data-qa-id="pdp-reset-selection-btn" href="${baseProductUrl}">
                <spring:theme code="product.variants.reset.title" />
                <i class="u-icon-tk u-icon-tk--close-single"> </i>
            </a>
        </h3>
        <c:set var="variantCategoryMap" />
        <c:set var="isSawingAlreadySelectedVar" value="${false}"/>
        <c:set var="isResetVaraintForSawingButton" value="${false}"/>
        <c:if test="${isSawingAlreadySelected eq true}">
            <c:set var="isSawingAlreadySelectedVar" value="${true}"/>
        </c:if>
        <ul class="m-vcs__content">
            <c:forEach items="${product.categories}" var="cat">
                <c:set var="i" value="0" />
                <c:set var="displaySawingOption" value="${product.baseProductHasCutToLengthCategory and cat.isTradeLengthCategory}"/>
                <c:set var="hideTradeLengthIfSawingAvailable" value="${(isSawingAlreadySelectedVar eq true) and (cat.isTradeLengthCategory)}"/>
                <li class="m-vcs__content__item js-variant-category">
                    <c:set var="variantNodes" value="${unselectedVariantMatrixListModel}" />

                    <div class="m-vcs__content__item__variant-name text-medium" data-qa-id="variant-category-label"> ${fn:escapeXml(cat.name)}:
                    </div><div class="m-vcs__content__item__variant-list">
                    <ul class="list-inline">
                        <c:if test="${displaySawingOption}">
                            <c:set var="classForSawingButton" value="btn-info m-vcs__content__item__btn"/>
                            <spring:theme var="sawingButtonTooltipText" code="product.variant.sawing.tooltip"/>
                            <c:if test="${(isCutToLengthAvailable && (isSubmitTriggeredBySawing || isSawingAlreadySelectedVar))}">
                                <c:set var="classForSawingButton" value="btn-default active m-vcs__content__item__btn--active"/>
                            </c:if>
                            <c:if test="${isCutToLengthAvailable ne true}">
                                <c:set var="classForSawingButton" value="btn-info m-vcs__content__item__btn--inactive"/>
                                <spring:theme var="sawingButtonTooltipText" code="product.variants.inactive.tooltip.text"/>
                                <c:set var="isResetVaraintForSawingButton" value="${true}"/>
                            </c:if>
                            <li>
                                <button id="sawing_href_link" data-qa-id="pdp-sawing-icon" data-variant-status="inactive" class="btn ${classForSawingButton} btn-sm btn--cut-to-length outline js-varient-sawing" data-set-sawing-value="${isSawingAlreadySelectedVar}">
                                    <c:choose>
                                        <c:when test="${isSawingAlreadySelectedVar eq true}">
                                            <span data-qa-id="pdp-sawing-button" title="${sawingButtonTooltipText}" data-toggle="tooltip" ><i class="u-icon-tk u-icon-tk--saw"></i>&nbsp${alreadySelectedRange}${fn:toLowerCase(alreadySelectedRangeUnit)}&nbsp${alreadySelectedTolerance}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <span data-qa-id="pdp-sawing-button" title="${sawingButtonTooltipText}" data-toggle="tooltip" ><i class="u-icon-tk u-icon-tk--saw"></i>&nbsp<spring:theme code="product.variant.sawing.text"/></span>
                                        </c:otherwise>
                                    </c:choose>
                                </button>
                            </li>
                        </c:if>
                        <c:forEach items="${variantNodes}" var="variantNode"><c:if test="${variantNode.parentVariantCategory.name  eq fn:escapeXml(cat.name)}"><li>
                                <c:url value="${variantNode.variantOption.url}" var="productStyleUrl" />
                                <form:form id="variantSelectionForm" name="variantSelectionForm" commandName="variantSelectionForm" action="${productStyleUrl}" method="post" class="m-vcs__content__form">
                                    <c:url value="${variantNode.parentVariantCategory.code}" var="variantCategory" />
                                    <c:url value="${variantNode.variantValueCategory.code}" var="variantValueCategory" />

                                    <input type="hidden" id="totalVariantValues"  class="js-total-variant-value" value="${totalVariantValues}" />
                                    <input type="hidden" name="selectedVariantCategory" id="selectedVariantCategory" value="${variantCategory}" />
                                    <input type="hidden" name="selectedVariantValueCategory" id="selectedVariantValueCategory" value="${variantValueCategory}" />
                                    <c:forEach var="selectedVVCs" items="${variantSelectionForm.variantSelectionMap}">
                                        <input type="hidden"  name="variantSelectionMap[${selectedVVCs.key}]" id="variantSelectionMap[${selectedVVCs.key}]" value="${selectedVVCs.value}" />
                                    </c:forEach>
                                    <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
                                    <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
                                    <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
                                    <input type="hidden" name="selectionIgnore" id="isSelectionIgnore" value="false" />


                                    <c:choose>
                                        <c:when test="${variantNode.parentVariantCategory.isTradeLengthCategory eq true}">
                                            <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${variantValueCategory}" />
                                        </c:when>
                                        <c:otherwise>
                                            <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${selectedTradelength}" />
                                        </c:otherwise>
                                    </c:choose>
                                    <c:set var="mapValue" value="${variantSelectionForm.variantSelectionMap[variantNode.parentVariantCategory.code]}" />
                                    <c:choose>
                                        <c:when test="${ variantResetFlag eq 'true'}">
                                            <button type="submit" id="selectedVariantValueCategory" class="btn btn-sm btn-info outline m-vcs__content__item__btn js-product-variant-submit" data-variant-status="unselected"
                                                    data-qa-id="variant-category-value-btn">
                                                    ${fn:escapeXml(variantNode.variantValueCategory.name)}
                                            </button>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${mapValue eq fn:escapeXml(variantNode.variantValueCategory.code)}">
                                                    <button type="submit" class="btn btn-sm outline btn-default m-vcs__content__item__btn--active js-product-variant-submit active" data-variant-status="selected" data-qa-id="variant-category-value-btn">
                                                            ${fn:escapeXml(variantNode.variantValueCategory.name)}
                                                    </button>
                                                    <c:if test="${variantNode.parentVariantCategory.isTradeLengthCategory}">
                                                        <c:set var="isTradeLengthAlreadySelected" value="${true}"/>
                                                    </c:if>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${'ACTIVE' eq fn:escapeXml(variantNode.variantValueCategory.displayStatus) and (mapValue eq null or mapValue eq '') and (hideTradeLengthIfSawingAvailable ne true)}">
                                                            <button type="submit" class="btn btn-sm outline btn-info m-vcs__content__item__btn--unselected js-product-variant-submit" data-variant-status="unselected"
                                                                    data-qa-id="variant-category-value-btn">
                                                                    ${fn:escapeXml(variantNode.variantValueCategory.name)}
                                                            </button>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input type="hidden" name="mapResetFlag" id="mapResetFlag" value="${true}" />
                                                            <button type="submit" id="unSelectedVariantValueCategory" class="btn btn-sm btn-info outline m-vcs__content__item__btn--inactive js-product-variant-submit" data-toggle="tooltip" data-variant-status="inactive"
                                                                    title="${inactiveVariantTooltipText}" data-qa-id="variant-category-value-btn">
                                                                    ${fn:escapeXml(variantNode.variantValueCategory.name)}
                                                            </button>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </form:form>
                            </li></c:if></c:forEach>
                    </ul>

                    <c:if test="${displaySawingOption and isCutToLengthAvailable ne null}">
                        <div id="js-cuttolenght-panel" class="js-cuttolenght-panel">
                            <tk-eu-product:productSawingPanel/>
                        </div>
                    </c:if>
                    <%-- This form is submitted when user clicks sawing button and tradelength is already selected or When the sawing button is inactive --%>
                    <form:form id="variantSelectionSawingForm" name="variantSelectionForm" commandName="variantSelectionForm" action="${product.url}" method="post" class="m-vcs__content__form">
                        <input type="hidden" id="totalVariantValues" class="js-total-variant-value" value="${totalVariantValues}"/>
                        <c:forEach var="selectedVVCs" items="${variantSelectionForm.variantSelectionMap}">
                            <input type="hidden"  name="variantSelectionMap[${selectedVVCs.key}]" id="variantSelectionMap[${selectedVVCs.key}]" value="${selectedVVCs.value}" />
                        </c:forEach>
                        <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${selectedTradelength}"/>
                        <input type="hidden" name="submitTriggeredBySawing" id="submitTriggeredBySawing" value="${true}"/>
                        <input type="hidden" name="isTradeLengthAlreadySelected" id="isTradeLengthAlreadySelected" value="${isTradeLengthAlreadySelected}"/>
                        <c:if test="${isTradeLengthAlreadySelected eq true or isResetVaraintForSawingButton eq true}">
                            <input class="js-cut-to-length-form-map-reset" type="hidden" class="js-cut-to-length-form-map-reset" name="mapResetFlag" id="mapResetFlag" value="${true}"/>
                        </c:if>
                    </form:form>
                </div>
                </li>
            </c:forEach>
        </ul>
        <spring:theme code="product.variant.unselect.dialogue.title" var="selectVariantDialogTitle" />

        <div class="js-unselect-variant-dialogue hide" data-dialogue-title="${selectVariantDialogTitle}">
            <p>
                <spring:theme code="product.variant.unselect.dialogue.text" />
            </p>
            <div class="actions">
                <div class="col-sm-6">
                    <button id="confirmNo" type="button" class="btn btn-link btn-block">
                        <spring:theme code="unselect.dialogue.btn.no" />
                    </button>
                </div>
                <div class="col-sm-6">
                    <button id="confirmYes" type="button" class="btn btn-primary btn-block">
                        <spring:theme code="unselect.dialogue.btn.yes" />
                    </button>
                </div>
            </div>
        </div>
    </div>
</c:if>

<%@ attribute name="consignmentDataWrapper" required="true" type="com.thyssenkrupp.b2b.eu.facades.order.data.MyAccountConsignmentDataWrapper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:set var="consignmentData" value="${consignmentDataWrapper.consignmentData}"/>
<c:set var="invoiceNumber" value=""/>
<c:url value="/my-account/my-deliveries/${consignmentData.code}" var="sapOrderConfirmationUrl"/>
<c:url value="/my-account/my-invoices" var="sapOrderInvoiceUrl"/>
<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="m-order-ack">
    <h4 class="order-details-header" data-qa-id="deliverydocument-noheading-label"><spring:theme code="myaccount.deliveries.details.headline.invoice" arguments="${fn:escapeXml(consignmentData.code)}" text="Delivery Nr. "/>&nbsp;</h4>
    <hr class="m-order-ack__separator m-order-ack__separator--thick">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group m-item-group--no-bottom-margin">
                <p class="m-item-group__label" data-qa-id="deliverydetails-shippingdate-label"><spring:theme code="myaccount.deliveries.details.shippingDate"/></p>
                <p class="m-item-group__value" data-qa-id="deliverydetails-shippingdate"><fmt:formatDate value="${consignmentData.shippingDate}" dateStyle="medium" type="date"/></p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 m-order-ack--doc-download">
            <div class="m-item-group">
                <p class="m-item-group__label"><spring:theme code="myaccount.deliveries.details.deliveryDocument"/></p>
                <p class="m-item-group__value m-download" data-qa-id="confirmationdetails-document-value">
                    <spring:theme code="myaccount.deliveries.details.deliveryDocument.Tooltip.NotAvailable" var="deliveriesTooltip"/>
                    <a id="doc${consignmentData.code}" class="m-download--link js-document-primary js-document-download disabled" href="/document/download" disabled="disabled" data-document-obj='{"documentNo": "${consignmentData.code}","positionNo":"","batchNo":"","lineItemNo":"","documenType":"DELIVERY"}' data-qa-id="" >
                        <i class="u-icon-tk u-icon-tk--md m-loading-icon" data-toggle="tooltip" title="${deliveriesTooltip}"></i>
                    </a>
                    <input type="hidden" value="${CSRFTokenSession}" id="csrfId"/>
                </p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="m-item-group m-item-group--no-bottom-margin">
                <p class="m-item-group__label" data-qa-id="deliverydetails-deliverydate-label"><spring:theme code="myaccount.deliveries.details.deliveryDate"/></p>
                <p class="m-item-group__value" data-qa-id="deliverydetails-deliverydate"><fmt:formatDate value="${consignmentData.deliveryDate}" dateStyle="medium" type="date"/></p>
            </div>
        </div>
    </div>
</div>

<div class="m-accordion">
    <div class="m-accordion__header collapsed" data-toggle="collapse" data-target="#order-addresses" data-qa-id="deliverydetails-addresses-heading-label">
        <h3 class="h5 m-accordion__header__title m-text-color--blue"><spring:theme code="myaccount.deliveries.details.addresses"/></h3>
    </div>
    <div id="order-addresses" class="m-accordion__content collapse">
        <div class="row">
            <div class="col-md-6">
                <p class="m-order-address__title" data-qa-id="deliverydetails-shippingaddress-label"><spring:theme code="myaccount.deliveries.details.invoiceAddress"/></p>
                <div class="m-order-address__block" data-qa-id="order-details-shipping-address-value">
                    <common:address address="${consignmentData.shippingAddress}"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-page-section">
    <h3 class="h5 m-page-section__title m-page-section__title--bottom-s6" data-qa-id="deliverydetails-itemsheading-label"><spring:theme code="myaccount.deliveries.details.items" text="Items" arguments="${fn:length(consignmentData.entries)}"/></h3>
<div class="m-page-section__content">
    <table class="m-order-history m-order-history--bottom-s5 table ">
        <thead>
        <tr class=" hidden-xs">
            <th id="header1" class="m-order-history__th" data-qa-id="deliverydetails-posheading-label"><spring:theme code="myaccount.deliveries.details.pos"/></th>
            <th id="header2" class="m-order-history__th" data-qa-id="deliverydetails-qtyheading-label"><spring:theme code="myaccount.deliveries.details.qty"/></th>
            <th id="header3" class="m-order-history__th" data-qa-id="deliverydetails-productname-label"><spring:theme code="myaccount.deliveries.details.productName"/></th>
            <th id="header4" class="m-order-history__th" data-qa-id="deliverydetails-artnumber-label"><spring:theme code="myaccount.deliveries.details.articleNumber"/></th>
            <th id="header6" class="m-order-history__th" data-qa-id="invoice-number-heading-text"><spring:theme code="myaccount.deliveries.details.invoice"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="consignmentEntryWrapper" items="${consignmentDataWrapper.consignmentEntriesWrapper}">
            <c:if test="${consignmentEntryWrapper.consignmentEntry[0] ne null}">
                <c:set var="invoiceNumber" value="${consignmentEntryWrapper.consignmentEntry[0].invoice}"/>
            </c:if>
            <tr class="m-text-color--black m-order-history__tr ${(not empty consignmentEntryWrapper.consignmentEntry) ? 'm-order-history__tr--has-details' : ''}">
                <td headers="header5" class="m-order-history__td" data-qa-id="deliverydetails-posh-value">
                    ${consignmentEntryWrapper.batchGroupNumber}
                </td>
                <td headers="header5" class="m-order-history__td m-order-history__td--qty" data-qa-id="deliverydetails-qtyheading-value">
                    ${consignmentEntryWrapper.batchQty} ${consignmentEntryWrapper.batchUnit.name}
                </td>
                <td headers="header5" class="m-order-history__td m-order-history__td--product-name" data-qa-id="deliverydetails-productname-value">
                    <c:if
                                test="${not empty consignmentEntryWrapper.consignmentEntry}">
                    ${consignmentEntryWrapper.consignmentEntry[0].productName}
                    </c:if>
                </td>
                <td headers="header5" class="m-order-history__td m-order-history__td--product-number" data-qa-id="deliverydetails-artnumber-value">
                    <c:if
                                test="${not empty consignmentEntryWrapper.consignmentEntry}">
                    ${consignmentEntryWrapper.consignmentEntry[0].productCode}
                    </c:if>
                </td>
                <td headers="header5" class="m-order-history__td text-right" data-qa-id="invoice-number-value">
                    <a href="${sapOrderInvoiceUrl}/${invoiceNumber}">${invoiceNumber}</a>
                </td>
            </tr>
            <spring:theme code="myaccount.deliveries.details.certificate.Tooltip.NotAvailable" var="certificateTooltip"/>
            <c:if test="${not empty consignmentEntryWrapper.consignmentEntry}">
                <tr class="m-text-color--black m-order-history__tr m-order-history__tr--confirmed-products  m-order-history__tr--details-line-item">
                    <td headers="header5" class="m-order-history__td"></td>
                    <td headers="header5" class="m-order-history__td " > </td>
                    <td headers="header5" class="m-order-history__td ">
                        <div class="m-order-history__inner-table">
                            <div class="col-md-4 m-order-history__inner-table m-order-history__th small text-medium m-order-history__td--has-gray-dark-text" data-qa-id="deliverydetails-lotnumber-label"><spring:theme code="myaccount.deliveries.details.batchNumber"/></div>
                            <div class="col-md-4 m-order-history__th small text-medium m-order-history__td--has-gray-dark-text" data-qa-id="deliverydetails-batchnumber-label"><spring:theme code="myaccount.deliveries.details.supplierLotNumber"/></div>
                            <div class="col-md-4 m-order-history__th small text-medium m-order-history__td--has-gray-dark-text m-order-ack--doc-download" data-qa-id="deliverydetails-certificate-label"><spring:theme code="myaccount.deliveries.details.certificate"/></div>
                        </div>
                    </td>
                    <td colspan="3" headers="header5" class="m-order-history__td "></td>
                </tr>
                <c:forEach items="${consignmentEntryWrapper.consignmentEntry}" var="consignmentEntry"  varStatus="status">
                    <tr class="m-text-color--black m-order-history__tr m-order-history__tr--confirmed-products m-order-history__tr--details-line-item ${status.last ? 'm-order-history__tr--details-line-item--last' : ''}">
                        <td headers="header5" class="m-order-history__td"></td>
                        <td headers="header5" class="m-order-history__td m-order-history__td--has-gray-dark-text small" data-qa-id="deliverydetails-batchnumber-value"> ${consignmentEntry.shippedQuantity}&nbsp;${consignmentEntry.shippedQuantityUnit.name} </td>
                        <td headers="header5" class="m-order-history__td">
                            <div class="m-order-history__inner-table">
                                <div class="col-md-4 small m-order-history__td m-order-history__td--has-gray-dark-text" data-qa-id="deliverydetails-batchnumber-value">${consignmentEntry.batchNumber}</div>
                                <div class="col-md-4 small m-order-history__td m-order-history__td--has-gray-dark-text" data-qa-id="deliverydetails-lotnumber-value">${consignmentEntry.supplierLotNumber}</div>
                                <c:if test="${consignmentEntry.isCertificateExist}">
                                <div class="col-md-4 small m-order-history__td m-order-history__td--has-gray-dark-text m-download m-order-ack--doc-download" data-qa-id="deliverydetails-certificate-value">
                                    <a id="docLine${consignmentEntry.entryNo}" class="m-download--link js-document-download disabled" href="/document/download" disabled="disabled" data-document-obj='{"documentNo": "${consignmentData.code}","positionNo":"","batchNo":"${consignmentEntry.batchNumber}","lineItemNo":"${consignmentEntry.entryNo}","documenType":"CERTIFICATE"}' data-qa-id="invoicedetails-document" >
                                        <i class="u-icon-tk u-icon-tk--md m-loading-icon" data-toggle="tooltip" title="${certificateTooltip}"></i>
                                    </a>
                                </div>
                              </c:if>

                            </div>
                        </td>
                        <td colspan="3" headers="header5" class="m-order-history__td"></td>
                    </tr>
                </c:forEach>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
    </div>
</div>

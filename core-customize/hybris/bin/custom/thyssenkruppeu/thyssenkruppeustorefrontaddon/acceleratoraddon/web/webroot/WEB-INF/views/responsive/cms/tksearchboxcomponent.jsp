<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="/search/" var="searchUrl" />
<spring:url value="/search/autocomplete/{/componentuid}" var="autocompleteUrl" htmlEscape="false">
     <spring:param name="componentuid"  value="${component.uid}"/>
</spring:url>
<c:set var="searchPlaceholder" value="${component.searchPlaceHolder}" />
<form class="m-header__content__search" name="search_form_${fn:escapeXml(component.uid)}" method="get" action="${searchUrl}">
    <div class="input-group">
        <ycommerce:testId code="header_search_input">
            <div class="form-group has-feedback has-clear">
                <input type="text" id="js-site-search-input"
                    class="form-control m-header__content__search__input js-site-search-input"
                    name="text" value=""
                    maxlength="100" placeholder="${searchPlaceholder}"
                    data-qa-id="search-txt-box"
                    data-options='{"autocompleteUrl" : "${autocompleteUrl}","minCharactersBeforeRequest" : "${component.minCharactersBeforeRequest}","waitTimeBeforeRequest" : "${component.waitTimeBeforeRequest}","displayProductImages" : ${component.displayProductImages}}'
                    data-qa-id="search-text-box">
                <span class="form-control-clear js-form-control-clear u-icon-tk u-icon-tk--close-single form-control-feedback"></span>
            </div>
        </ycommerce:testId>
        <span class="input-group-btn">
            <ycommerce:testId code="header_search_button">
                <button class="btn js_search_button btn-link" type="submit" disabled="true" data-qa-id="search-btn"></button>
            </ycommerce:testId>
        </span>
    </div>
</form>

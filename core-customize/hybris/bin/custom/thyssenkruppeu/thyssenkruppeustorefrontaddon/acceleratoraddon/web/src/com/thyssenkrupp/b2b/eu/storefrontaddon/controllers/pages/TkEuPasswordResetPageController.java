/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import static com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_PASSWORD_PASSWORD_RESET_CHANGE_PAGE;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;
import com.thyssenkrupp.b2b.eu.facades.customer.TkEuB2bCustomerFacade;
import com.thyssenkrupp.b2b.eu.facades.forgotpassword.ForgotPasswordFacade;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.ControllerConstants;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.PasswordResetPageController;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CartRestorationStrategy;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

@RequestMapping(value = "/login/pw")
public class TkEuPasswordResetPageController extends PasswordResetPageController {

    private static final Logger LOGGER = Logger.getLogger(TkEuPasswordResetPageController.class);

    private static final String UPDATE_PWD_CMS_PAGE                   = "updatePassword";
    private static final String REDIRECT_HOME                         = "redirect:/";
    private static final String REDIRECT_TOKEN_EXPIRED_EMAIL_REQ_CONF = "redirect:/login/pw/request/token/conf";
    private static final String REDIRECT_PASSWORD_RESET               = "redirect:/resetPasswordConfirmation";

    private static final String SET_YOUR_PASSWORD_CMS_PAGE = "SetYourPasswordContentPage";

    @Resource(name = "b2bCustomerFacade")
    private TkEuB2bCustomerFacade tkEuCustomerFacade;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "cartRestorationStrategy")
    private CartRestorationStrategy cartRestorationStrategy;

    @Resource(name = "b2bCustomerFacade")
    private DefaultB2BCustomerFacade b2bCustomerFacade;

    @Resource(name = "secureTokenService")
    private SecureTokenService secureTokenService;

    @Resource(name = "forgotPasswordFacade")
    private ForgotPasswordFacade forgotPasswordFacade;

    @Resource(name = "b2bCustomerService")
    private TkEuB2bCustomerService b2bCustomerService;

    @Resource(name = "modelService")
    private ModelService modelService;

    @Resource(name = "bruteForceAttackCounter")
    private BruteForceAttackCounter bruteForceAttackCounter;

    @Resource(name = "eventService")
    private EventService eventService;

    @Override
    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public String getChangePassword(@RequestParam(required = false) final String token, final Model model) throws CMSItemNotFoundException {
        if (StringUtils.isBlank(token) || tkEuCustomerFacade.isTokenInvalid(token)) {
            return REDIRECT_HOME;
        }
        final UpdatePwdForm form = new UpdatePwdForm();
        form.setToken(token);
        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        model.addAttribute(form);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("password.reset.title"));
        if (tkEuCustomerFacade.isTokenExpired(token)) {
            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_PASSWORD_TOKEN_EXPIRED_PAGE;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(SET_YOUR_PASSWORD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SET_YOUR_PASSWORD_CMS_PAGE));

        return VIEWS_PAGES_PASSWORD_PASSWORD_RESET_CHANGE_PAGE;
    }

    @RequestMapping(value = "/resendEmail", method = RequestMethod.GET)
    public String resendEmail(@RequestParam(value = "token") final String token, final HttpServletRequest request) throws CMSItemNotFoundException {
        if (!StringUtils.isBlank(token) && tkEuCustomerFacade.isTokenExpired(token)) {
            String email = tkEuCustomerFacade.getEmailIdFromToken(token);
            tkEuCustomerFacade.forgottenPassword(email);
            request.getSession().setAttribute("redirectToken", token);
        }
        return REDIRECT_TOKEN_EXPIRED_EMAIL_REQ_CONF;
    }

    @RequestMapping(value = "/request/token/conf", method = RequestMethod.GET)
    public String getResendConfirmationWhenTokenExpired(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
        return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_PASSWORD_TOKEN_EXPIRED_PAGE;
    }

    @Override
    @RequestMapping(value = "/change", method = RequestMethod.POST)
    public String changePassword(@Valid final UpdatePwdForm form, final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException {
        getUpdatePasswordFormValidator().validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "form.global.error");
            storeCmsPageInModel(model, getContentPageForLabelOrId(SET_YOUR_PASSWORD_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SET_YOUR_PASSWORD_CMS_PAGE));
            return VIEWS_PAGES_PASSWORD_PASSWORD_RESET_CHANGE_PAGE;
        }
        if (StringUtils.isNotBlank(form.getToken())) {
            try {
                b2bCustomerFacade.updatePassword(form.getToken(), form.getPwd());
                SecureToken secureToken = secureTokenService.decryptData(form.getToken());
                resetUserDisabledLoginAttributes(getUserBySecureToken(secureToken));
                getAutoLoginStrategy().login(secureToken.getData(), form.getPwd(), request, response);
                cartRestorationStrategy.restoreCart(request);
                return REDIRECT_PASSWORD_RESET;
            } catch (final TokenInvalidatedException ex) {
                LOGGER.error("Token invalid", ex);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalidated");
            } catch (final RuntimeException ex) {
                LOGGER.error("Exception while updating password", ex);
                prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "updatePwd.pwd.invalid");
                return VIEWS_PAGES_PASSWORD_PASSWORD_RESET_CHANGE_PAGE;
            }
        }
        return REDIRECT_HOME;
    }

    private void resetUserDisabledLoginAttributes(UserModel userModel) {
        if (userModel != null) {
            userModel.setTokens(null);
            userModel.setLoginDisabledTime(null);
            userModel.setLoginDisabled(false);
            modelService.save(userModel);
        }
    }

    public UserModel getUserBySecureToken(SecureToken secureToken) {
        return b2bCustomerService.getAllUserForUID(secureToken.getData(), CustomerModel.class);
    }

    @Override
    @RequestMapping(value = "/request", method = RequestMethod.POST)
    public String passwordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException {
        model.addAttribute("isPopUp", true);
        if (bindingResult.hasErrors()) {
            return ControllerConstants.Views.Fragments.Password.PasswordResetRequestPopup;
        } else {
            try {
                forgotPasswordFacade.forgottenPassword(form.getEmail());
            } catch (final UnknownIdentifierException unknownIdentifierException) {
                LOGGER.warn("Email: " + form.getEmail() + " does not exist in the database.");
            } catch (final AmbiguousIdentifierException ambiguousEx) {
                LOGGER.warn("Email: " + form.getEmail() + " Ambiguous user found :" + ambiguousEx.getMessage());
            }
            return ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_PAGES_FORGOT_PASSWORD_VALIDATION_MESSAGE_PAGE;
        }
    }
}

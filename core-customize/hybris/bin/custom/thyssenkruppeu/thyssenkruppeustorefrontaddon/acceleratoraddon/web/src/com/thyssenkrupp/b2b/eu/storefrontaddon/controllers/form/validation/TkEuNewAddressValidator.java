package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.validation;

import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form.TkShippingAddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.annotation.Resource;

@Component("tkEuNewAddressValidator")
public class TkEuNewAddressValidator extends AddressValidator {

    private static final int MAX_FIELD_LENGTH    = 255;
    private static final int MAX_POSTCODE_LENGTH = 10;
    private static final String DEFAULT_COUNTRY = "DE";
    private static final String DEFAULT_LANGUAGE_VALIDATION_REGEX = "validation.postalcode.regex.DE";
    private static final String OTHER_LANGUAGE_VALIDATION_REGEX = "validation.postalcode.regex.default";

    @Resource(name = "textAreaPatternValidator")
    private DefaultTextAreaValidator textAreaPatternValidator;

    private ConfigurationService configurationService;

    @Override
    public boolean supports(final Class<?> aClass) {
        return AddressForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        final TkShippingAddressForm addressForm = (TkShippingAddressForm) object;
        validateTkEuaddresFields(addressForm, errors);
        validateStandardFields(addressForm, errors);
        validateShippingComment(addressForm.getComment(),errors);
        validatePostalCode(addressForm,errors);
    }

    public void validatePostalCode(final AddressForm addressForm, final Errors errors) {
        final String isoCode = addressForm.getCountryIso();
        final String postCode = addressForm.getPostcode();
        final String postCodeValidationPattern;

        if (postCode != null && isoCode != null) {
            postCodeValidationPattern = getPostCodeValidationPattern(isoCode);
            if (!vaildatePostalCodePattern(postCode, postCodeValidationPattern)) {
                errors.rejectValue("postcode", "myaccount.shipping.zipcode.error");
            }
        }
    }

    public void validateShippingComment(String shippingComments, Errors errors) {
        if (!textAreaPatternValidator.validateTextAreaPattern(shippingComments)) {
            final AddressField commentFieldType = AddressField.COMMENT;
            errors.rejectValue(commentFieldType.getFieldKey(), commentFieldType.getErrorKey());
        }
    }

    protected void validateTkEuaddresFields(final TkShippingAddressForm addressForm, final Errors errors) {
        validateStringField(addressForm.getCompanyName(), AddressField.COMPANYNAME, MAX_FIELD_LENGTH, errors);
        validateStringField(addressForm.getLine2(), AddressField.HOUSENUMBER, MAX_FIELD_LENGTH, errors);
    }

    protected static void validateStringField(final String addressField, final AddressField fieldType,
        final int maxFieldLength, final Errors errors) {
        if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength)) {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }

    protected enum AddressField {
        COMPANYNAME("companyName", "address.companyName.invalid"), HOUSENUMBER("line2", "address.houseNumber.invalid"), COMMENT("comment", "checkout.shipping.comment.invalid");
        private String fieldKey;
        private String errorKey;

        AddressField(final String fieldKey, final String errorKey) {
            this.fieldKey = fieldKey;
            this.errorKey = errorKey;
        }
        public String getFieldKey() {
            return fieldKey;
        }

        public String getErrorKey() {
            return errorKey;
        }
    }

    private boolean vaildatePostalCodePattern(String postCode,String postalCodeValidationPattern){
        return postCode.matches(postalCodeValidationPattern);
    }

    private String getPostCodeValidationPattern(String isoCode) {
        final String postCodeValidationPattern;
        if (isoCode.equals(DEFAULT_COUNTRY))
            postCodeValidationPattern = configurationService.getConfiguration().getString(DEFAULT_LANGUAGE_VALIDATION_REGEX);
        else
            postCodeValidationPattern = configurationService.getConfiguration().getString(OTHER_LANGUAGE_VALIDATION_REGEX);
        return postCodeValidationPattern;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set var="token" value="${( not empty updatePwdForm.token) ? updatePwdForm.token : redirectToken}" />
<template:page pageTitle="${pageTitle}" class="l-page l-page__token-expired">
    <div class="container l-page__content">
        <div class="row">
            <div class="col-sm-7">
                <h3 class="l-page__title"><spring:theme code="text.account.profile.changePassword"/></h3>
                <p class="l-page__description">
                    <span class="u-icon-tk u-icon-tk--message-important u-icon-tk--context-warning"></span>&nbsp;<spring:theme code="change.password.token.expired"/>
                </p>

                <form method="get" action="<c:url value='/login/pw/resendEmail'/>">
                    <input name="token" type="hidden" value="${token}"/>
                    <button class="btn btn-primary" type="submit"><spring:theme code="token.expired.resend.email"/></button>
                </form>

            </div>
        </div>
        <c:if test="${not empty redirectToken}">
            <div class="row">
                <div class="col-md-4">
                    <div class="l-page__token-expired__msg-container">
                        <div class="l-page__token-expired__msg-container__message">
                            <spring:theme code="change.password.resend.email.confirmation.message" />
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</template:page>

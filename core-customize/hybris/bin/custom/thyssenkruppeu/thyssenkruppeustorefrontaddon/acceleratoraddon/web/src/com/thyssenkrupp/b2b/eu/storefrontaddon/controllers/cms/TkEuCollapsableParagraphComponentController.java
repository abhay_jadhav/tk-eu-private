package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.cms;

import com.thyssenkrupp.b2b.eu.core.model.TkEuCollapsableParagraphComponentModel;
import com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.ThyssenkruppeustorefrontaddonControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller("TkEuCollapsableParagraphComponentController")
@RequestMapping(value = ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_TKEUCOLLAPSABLEPARAGRAPHCOMPONENT)
public class TkEuCollapsableParagraphComponentController extends AbstractCMSComponentController<TkEuCollapsableParagraphComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, TkEuCollapsableParagraphComponentModel component) {
        if (component != null) {
            model.addAttribute("isExpanded", component.getIsExpanded());
            model.addAttribute("title", component.getTitle());
            model.addAttribute("content", component.getContent());
        }
    }

    @Override
    protected String getView(TkEuCollapsableParagraphComponentModel component) {
        return ThyssenkruppeustorefrontaddonControllerConstants.ADDON_PREFIX + ThyssenkruppeustorefrontaddonControllerConstants.VIEWS_CMS_COMPONENTPREFIX +
            StringUtils.lowerCase(TkEuCollapsableParagraphComponentModel._TYPECODE);
    }
}

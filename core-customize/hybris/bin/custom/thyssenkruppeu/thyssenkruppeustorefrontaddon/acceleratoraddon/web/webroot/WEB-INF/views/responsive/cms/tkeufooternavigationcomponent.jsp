<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="footerLinksInARow" value="3"/>
<c:set var="footerLinkNodes" value="${navigationNode.children[0].children}" />
<c:set var="footerLinksSize" value="${fn:length(footerLinkNodes)}"/>
<c:set var="footerLinksRemainder" value="${footerLinksSize%footerLinksInARow eq 0 ? footerLinksInARow : footerLinksSize%footerLinksInARow}"/>
<c:if test="${footerLinksSize gt 0}">
    <div class="m-footer__content__links" data-qa-id="cms-footer">
        <div class="row">
            <c:forEach items="${footerLinkNodes}" var="linkNode" varStatus="i">
                <c:set var="isLastRowItem" value="${i.count > (footerLinksSize - footerLinksRemainder)}" />
                <div class="col-md-4 m-footer__content__links__item  ${isLastRowItem ? 'm-footer__content__links__item--last-row-item' : ''}">
                    <cms:component component="${linkNode.entries[0].item}" evaluateRestriction="true" />
                    <c:if test="${not isLastRowItem}">
                        <hr/>
                    </c:if>
                </div>
            </c:forEach>
        </div>
    </div>
</c:if>


<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}" class="l-page l-page__login l-page__secure-login" isMiniHeaderFooter="true">
    <cms:pageSlot position="SecureLoginBanner" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <div class="container l-page__content">
        <div class="row">
            <cms:pageSlot position="LeftContentSlot" var="feature" element="div" class="col-md-4">
                <cms:component component="${feature}" element="div" class="l-page__content__left"/>
            </cms:pageSlot>
            <cms:pageSlot position="SecureLoginCenterContent" var="feature" element="div" class="col-md-4">
               <cms:component component="${feature}" element="div" class="l-page__content__right"/>
           </cms:pageSlot>
           <cms:pageSlot position="SecureLoginAddress" var="feature" element="div" class="col-md-4">
               <cms:component component="${feature}" element="div" class="l-page__content__right"/>
           </cms:pageSlot>
        </div>
    </div>
</template:page>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tk-eu-product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:url value="${url}/configurations" var="addToCartUrl"/>

<product:addToCartTitle/>

<form:form method="post" id="addToCartForm" class="add_to_cart_form js-addtocart-action" action="${addToCartUrl}">
    <c:if test="${product.purchasable}">
        <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
    </c:if>
    <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}" data-qa-id="pdp-item-code"/>
    <input type="hidden" name="salesUom" class="salesUom js-hidden-uom-selector-input" id="addToCartForm-salesUom" value=""/>
    <input type="hidden" name="certOption" class="certOption js-hidden-certOption-selector-input" id="addToCartForm-certOption" data-qa-id="pdp-cert-code" value="${selectedCertificateId != null ? selectedCertificateId: 'none'}" />
    <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
    <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
    <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
    <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${variantSelectionForm.selectedTradeLength}"/>
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        <c:if test="${empty showAddToCart ? true : showAddToCart}">
            <c:set var="buttonType">button</c:set>
            <c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'notAvailable' }">
                <c:set var="buttonType">submit</c:set>
            </c:if>
            <c:choose>
                 <c:when test="${(!product.purchasable and isVariantSelected) or (!cartEntryData.isSupportedUomPresent and isVariantSelected)}">
                    <c:set var="requestForAvailability" value="true"/>
                </c:when>
                <c:when test="${fn:contains(buttonType, 'button') and isVariantSelected}">
                    <c:set var="requestForAvailability" value="true"/>
                </c:when>
                <c:otherwise>
                    <c:set var="requestForAvailability" value="false"/>
                    <button id="addToCartButton" type="${buttonType}" data-qa-id="pdp-add-to-cart-btn" class="btn btn-primary btn-block js-add-to-cart js-enable-btn" disabled="disabled">
                        <spring:theme code="basket.add.to.basket"/>
                    </button>
                    <button type="button" data-qa-id="pdp-requestavailable-button" id="onRequestBtn" class="btn btn-primary btn-block js-on-request-availability btn-icon hidden-lg"><spring:theme code="product.stock.availability.requestAvailability"/></button>
                </c:otherwise>
            </c:choose>
        </c:if>
    </sec:authorize>
    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
        <button id="addToCartButton" type="${buttonType}" data-qa-id="pdp-add-to-cart-btn" class="btn btn-primary btn-block" disabled="disabled">
           <spring:theme code="basket.add.to.basket"/>
        </button>
    </sec:authorize>
</form:form>
<c:if test="${requestForAvailability}">
    <tk-eu-product:requestForAvailability/>
</c:if>
<c:url value="/productAvailabilityRequest" var="contactUrl"/>
    <form:form method="post" id="tkEuContactForm" action="${contactUrl}" class="js-addtocart-request-availabilty">
        <input type="hidden" name="productCode" value="${fn:escapeXml(product.code)}"/>
        <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="${not empty selectedQty ? selectedQty : 1}">
        <input type="hidden" name="certOption" class="certOption js-hidden-certOption-selector-input" id="tkEuContactForm-certOption" value="none" />
        <input type="hidden" name="selectedCutToLengthRange" id="selectedCutToLengthRange" value="${variantSelectionForm.selectedCutToLengthRange}"/>
        <input type="hidden" name="selectedCutToLengthTolerance" id="selectedCutToLengthTolerance" value="${variantSelectionForm.selectedCutToLengthTolerance}"/>
        <input type="hidden" name="selectedCutToLengthRangeUnit" id="selectedCutToLengthRangeUnit" value="${variantSelectionForm.selectedCutToLengthRangeUnit}"/>
        <input type="hidden" name="salesUom" class="salesUom js-hidden-uom-selector-input" id="tkEuContactForm-salesUom" value="${salesUom}"/>
        <input type="hidden" name="selectedTradeLength" id="selectedTradeLength" value="${selectedTradelength}" />

        ${tkEuContactForm}
</form:form>

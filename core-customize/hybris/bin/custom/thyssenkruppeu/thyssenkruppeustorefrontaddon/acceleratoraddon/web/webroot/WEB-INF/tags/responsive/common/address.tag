<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData"%>
<%@ attribute name="isCommaRequired" required="false" type="java.lang.Boolean"%>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showPhoneNumber" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
    <c:when test="${isCommaRequired}">
        <c:set var="setCssClass" value="m-address--inline m-address--has-comma-character" />
        <c:set var="blackTextCssClass" value="" />
    </c:when>
    <c:otherwise>
        <c:set var="setCssClass" value="m-address--has-break" />
        <c:set var="blackTextCssClass" value="m-text-color--black" />
    </c:otherwise>
</c:choose>
<address class="m-address ${setCssClass}">
    <c:if test="${not storeAddress}">
        <c:if test="${not empty address.companyName}">
            <span class="m-address__company ${blackTextCssClass}">${fn:escapeXml(address.companyName)}</span>
        </c:if>
        <c:if test="${not empty address.department}">
            <span class="m-address__department ${blackTextCssClass}">${fn:escapeXml(address.department)}</span>
        </c:if>
        <c:if test="${not empty address.firstName or not empty address.lastName}">
            <span class="m-text-color--grey-dark m-address__name"><c:if test="${not empty address.title}">${fn:escapeXml(address.title)}&nbsp;</c:if>${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}</span>
        </c:if>
    </c:if>
    <c:if test="${not empty address.streetName or not empty address.streetNumber}">
        <span class="m-text-color--grey-dark m-address__street-name"><c:if test="${not empty address.streetName}">${fn:escapeXml(address.streetName)}</c:if><c:if test="${not empty address.streetNumber}"> ${fn:escapeXml(address.streetNumber)}</c:if></span>
    </c:if>
    <c:if test="${not empty address.postalCode or not empty address.town}">
        <span class="m-text-color--grey-dark m-address__post-city-region">${fn:escapeXml(address.postalCode)}<c:if test="${not empty address.town}"> ${fn:escapeXml(address.town)}</c:if><c:if test="${not empty address.region}"> ${fn:escapeXml(address.region.name)}</c:if></span>
    </c:if>
    <c:if test="${not empty address.country}">
        <span class="m-text-color--grey-dark m-address__country">${fn:escapeXml(address.country.name)}</span>
    </c:if>
    <c:if test="${showPhoneNumber and not empty address.phone}">
        <span class="m-text-color--grey-dark m-address__phone">${fn:escapeXml(address.phone)}</span>
    </c:if>
</address>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${not empty title}">
    <div class="m-accordion">
        <a class="m-accordion__header ${isExpanded ? '' : 'collapsed'}" data-toggle="collapse" href="#${component.uid}">
            <h3 class="h5 m-accordion__header__title no-format">${title}</h3>
        </a>
        <div id="${component.uid}" class="m-accordion__content collapse ${isExpanded ? 'in':''}">
            ${content}
        </div>
     </div>
</c:if>

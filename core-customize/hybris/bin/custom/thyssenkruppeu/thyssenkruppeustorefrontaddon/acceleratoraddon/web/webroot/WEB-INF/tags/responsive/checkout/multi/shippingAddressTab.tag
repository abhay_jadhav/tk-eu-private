<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="disableStatus" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<spring:theme code="checkout.order.shipping.address.text" var="shippingAddresText"/>
<spring:theme code="checkout.order.shipping.terms.text" var="shippingTermsText"/>
<spring:theme code="checkout.order.shipping.address.comment.text" var="shippingComments"/>
<spring:theme code="checkout.order.shipping.address.addaddress.text" var="addAddress"/>
<spring:theme code="checkout.order.final.date.confirmation.text" var="finalDateConfirmation"/>
<fmt:formatDate value="${cartData.overallAtpDate}" pattern="dd.MM.yyyy" var="overallAtpDate" />

<jsp:include page="addressContainerPage.jsp" >
    <jsp:param name="checkoutId" value="" />
    <jsp:param name="formName" value="tkShippingAddress" />
    <jsp:param name="action" value="${request.contextPath}/checkout/multi/delivery-address/choose" />
    <jsp:param name="selectedAddressCodeId" value="selectedAddressId" />
    <jsp:param name="terms" value="${shippingTerms}" />
    <jsp:param name="selectAddress" value="selectShippingAddress" />
    <jsp:param name="springCode" value="checkout.summary.ship.to" />
    <jsp:param name="springText" value="${shippingAddresText}" /> 
    <jsp:param name="addressDropDown" value="checkout-address-book-btn" />
    <jsp:param name="selectedAddressId" value="selectShippingAddress" />
    <jsp:param name="checkoutAddress" value="checkout-shipping-address" />
    <jsp:param name="checkoutAddressLabel" value="control-label checkout-shipping-address" />
    <jsp:param name="termsSpringCode" value="checkout.multi.shippingTerms" />
    <jsp:param name="termsSpringText" value="${shippingTermsText}" /> 
    <jsp:param name="checkoutTermsId" value="checkout-delivery-method-select" />
    <jsp:param name="checkoutTermsClass" value="form-control-static" />
    <jsp:param name="nextButton" value="addressSubmit" />
    <jsp:param name="nextButtonId" value="checkout-delivery-method-continue-btn" />
    <jsp:param name="shippingArea" value="shipping" />
    <jsp:param name="shippingComments" value="${shippingComments}" />
    <jsp:param name="addAddress" value="${addAddress}" />
    <jsp:param name="estimatedShippingDate" value="${estimatedShippingDate }" />
    <jsp:param name="finalDateConfirmation" value="${finalDateConfirmation }" />
    <jsp:param name="status" value="shipping" />
    <jsp:param name="comment" value="${comment}" />
    <jsp:param name="disableStatus" value="${disableStatus}" />
    <jsp:param name="overallAtpDate" value="${overallAtpDate}" />
</jsp:include>

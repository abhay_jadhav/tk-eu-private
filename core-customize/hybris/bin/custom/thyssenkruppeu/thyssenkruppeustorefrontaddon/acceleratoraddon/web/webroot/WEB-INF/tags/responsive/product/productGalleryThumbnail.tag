<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="carousel m-carousel-product__thumbnails js-gallery-carousel">
    <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
        <a href="#" class="m-carousel-product__thumbnails__item item">
            <c:choose>
                <c:when test="${container.thumbnail.url == null || container.product.url == null}">
                    <spring:theme code="img.missingProductImage.responsive.product" text="/" var="imagePath"/>
                    <c:choose>
                        <c:when test="${originalContextPath ne null}">
                            <c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
                        </c:when>
                        <c:otherwise>
                            <c:url value="${imagePath}" var="imageUrl" />
                        </c:otherwise>
                    </c:choose>
                    <img class="lazyOwl m-carousel-product__thumbnails__item__img" data-src="${imageUrl}"/>
                </c:when>
                <c:otherwise>
                    <img class="lazyOwl m-carousel-product__thumbnails__item__img" data-src="${container.thumbnail.url}" alt="${fn:escapeXml(container.thumbnail.altText)}">
                </c:otherwise>
            </c:choose>
        </a>
    </c:forEach>
</div>

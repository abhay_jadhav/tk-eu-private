<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="hideFavoriteFlag" required="false" type="java.lang.Boolean"%>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="top" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showTopTotals" required="false" type="java.lang.Boolean" %>
<%@ attribute name="supportShowAll" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowPaged" required="true" type="java.lang.Boolean" %>
<%@ attribute name="additionalParams" required="false" type="java.util.HashMap" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="searchKey" required="false" type="java.lang.String" %>
<%@ attribute name="showCurrentPageInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideRefineButton" required="false" type="java.lang.Boolean" %>
<%@ attribute name="numberPagesShown" required="false" type="java.lang.Integer" %>
<%@ attribute name="paginationXPos" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/nav/pagination" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>
<c:set var="showCurrPage" value="${not empty showCurrentPageInfo ? showCurrentPageInfo : false}"/>
<c:set var="hideRefBtn" value="${hideRefineButton ? true : false}"/>
<c:set var="showTotals" value="${empty showTopTotals ? true : showTopTotals}"/>
<c:set var="hideFavoriteFlag" value="${empty hideFavoriteFlag ? false : hideFavoriteFlag}"/>
<c:if test="${searchPageData.pagination.totalNumberOfResults == 0 && top && showTotals}">
    <spring:theme code="${themeMsgKey}.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}"/>
</c:if>
<c:if test="${searchPageData.pagination.totalNumberOfResults > 0}">
    <div class="m-page-tool-bar ${(top) ? 'm-page-tool-bar--top' : 'm-page-tool-bar--bottom'}">
        <c:if test="${not empty searchPageData.sorts}">
            <c:if test="${top or not hideRefBtn}">
                <div class="row">
                    <c:if test="${top}">
                        <c:if test="${showTotals}">
                            <div class="col-sm-7">
                                <div class="m-page-tool-bar__results-text" data-qa-id="plp-item-count" >
                                    <c:choose>
                                        <c:when test="${showCurrPage}">
                                            <c:choose>
                                                <c:when test="${searchPageData.pagination.totalNumberOfResults eq 1}">
                                                    <spring:theme code="${themeMsgKey}.totalResultsSingleOrder"/>
                                                </c:when>
                                                <c:when test="${searchPageData.pagination.numberOfPages le 1}" >
                                                    <spring:theme code="${themeMsgKey}.totalResultsSinglePag" arguments="${searchPageData.pagination.totalNumberOfResults}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="currentPageItems" value="${(searchPageData.pagination.currentPage + 1) * searchPageData.pagination.pageSize}"/>
                                                    <c:set var="upTo" value="${(currentPageItems > searchPageData.pagination.totalNumberOfResults ? searchPageData.pagination.totalNumberOfResults : currentPageItems)}"/>
                                                    <c:set var="currentPage" value="${searchPageData.pagination.currentPage * searchPageData.pagination.pageSize + 1} - ${upTo}"/>
                                                    <spring:theme code="${themeMsgKey}.totalResultsCurrPage" arguments="${currentPage},${searchPageData.pagination.totalNumberOfResults}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <spring:theme code="${themeMsgKey}.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </c:if>

                        <div class="col-sm-5">
                            <form id="sortForm${top ? '1' : '2'}" name="sortForm${top ? '1' : '2'}" method="get" action="#" class="form-inline text-right m-page-tool-bar__form">
                                <div class="form-group">
                                    <label class="m-text-color--grey-dark control-label m-page-tool-bar__form__label" data-qa-id="plp-sort-label" for="sortForm${top ? '1' : '2'}">
                                        <spring:theme code="${themeMsgKey}.sortTitle"/>
                                    </label>
                                    <select id="sortOptions${top ? '1' : '2'}" data-qa-id="plp-sort-dropdown" name="sort" class="form-control m-text-color--blue m-page-tool-bar__form__input js-sort-options">
                                        <option disabled><spring:theme code="${themeMsgKey}.sortTitle"/></option>
                                        <c:forEach items="${searchPageData.sorts}" var="sort">
                                            <option value="${fn:escapeXml(sort.code)}" ${sort.selected? 'selected="selected"' : ''}>
                                                <c:choose>
                                                    <c:when test="${not empty sort.name}">
                                                        ${fn:escapeXml(sort.name)}
                                                    </c:when>
                                                    <c:otherwise>
                                                        <spring:theme code="${themeMsgKey}.sort.${sort.code}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <c:catch var="errorException">
                                    <spring:eval expression="searchPageData.currentQuery.query" var="dummyVar"/><%-- This will throw an exception is it is not supported --%>
                                    <input type="hidden" name="q" value="${searchPageData.currentQuery.query.value}"/>
                                </c:catch>
                                <c:if test="${supportShowAll}">
                                    <input type="hidden" name="show" value="Page"/>
                                </c:if>
                                <c:if test="${supportShowPaged}">
                                    <input type="hidden" name="show" value="All"/>
                                </c:if>
                                <c:if test="${not empty additionalParams}">
                                    <c:forEach items="${additionalParams}" var="entry">
                                        <input type="hidden" name="${fn:escapeXml(entry.key)}" value="${fn:escapeXml(entry.value)}"/>
                                    </c:forEach>
                                </c:if>

                                <c:if test="${not hideFavoriteFlag}">
                                    <input type="hidden" name="favourites" value="${favouriteFacetData.selected}"/>
                                </c:if>

                                <c:if test="${not empty  searchKey}">
                                    <input type="hidden" class="js-search-keyword" name="searchKey" value="${searchKey}"/>
                                </c:if>
                                <input type="hidden"  name="page" value="0"/>
                            </form>
                        </div>
                    </c:if>
                    <c:if test="${not hideRefBtn}">
                        <div class="col-xs-12 col-sm-2 col-md-4 hidden-md hidden-lg">
                            <product:productRefineButton styleClass="btn btn-default js-show-facets"/>
                        </div>
                    </c:if>
                </div>
            </c:if>
            <c:if test="${not top}">
                <c:url var="searchUrl" value="${searchUrl}" context="/"><c:param name="favourites" value="${favouriteFacetData.selected}"/></c:url>
                <div class="m-pagination text-${not empty paginationXPos ? paginationXPos : 'center'}">
                    <pagination:pageSelectionPagination searchUrl="${searchUrl}" searchPageData="${searchPageData}" numberPagesShown="${numberPagesShown}" themeMsgKey="${themeMsgKey}"/>
                </div>
            </c:if>
        </c:if>
    </div>
</c:if>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="teaser-container" data-highlight="${highliting}" data-count="${size}" data-render-option="${renderOption}">
<div class="container">
    <h3 data-underline="${titleUnderlined}">${title}</h3>
    <div class="row">                
        <c:forEach items="${tkGenericComponentList}" var="tkEuGenericBannercomponent"
        varStatus="loop">
        <div class="item m-column">
            <cms:component component="${tkEuGenericBannercomponent}" />
        </div>
    </c:forEach>
    </div>
</div>
</div>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="searchResultCount" value="${searchCount}"/>
<c:url value="/my-account/my-confirmations/search" var="searchActionUrl" />
<c:set var="searchUrl" value="/my-account/my-confirmations/search?init=true&searchKey=${searchKey}&sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>
<c:url var="resetUrl" value="/my-account/my-confirmations?init=true" />
<c:choose>
    <c:when test="${searchResultCount gt 1 && empty searchKey}">
        <spring:theme code="myaccount.confirmation.headline" arguments="${searchResultCount}" var="titleOrderAck"/>
    </c:when>
    <c:when test="${searchResultCount eq 1 && empty searchKey}">
        <spring:theme code="myaccount.confirmation.headline.one" arguments="${searchResultCount}" var="titleOrderAck"/>
    </c:when>
    <c:when test="${searchResultCount gt 1 && not empty searchKey }">
        <spring:theme code="myaccount.confirmation.search.results" arguments="${searchResultCount},${searchKey}" var="titleOrderAck" />
    </c:when>
    <c:when test="${searchResultCount eq 1 && not empty searchKey}">
        <spring:theme code="myaccount.confirmation.search.results.one" arguments="${searchResultCount},${searchKey}" var="titleOrderAck" />
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${searchKey eq null}">
                <h4 class="m-order-form__title">
                    <spring:theme code="myaccount.confirmation.headline.noDocuments"/>
                </h4>
                <div class="m-order-form__description">
                    <spring:theme htmlEscape="false" code="myaccount.confirmation.headline.noDocumentsDescription"/>
                </div>
            </c:when>
            <c:otherwise>
                <spring:theme code="myaccount.orderconfirmation.search.noresults" arguments="${searchKey}" var="titleOrderAck"/>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>
<spring:theme code="myaccount.orderacknowledgement.search.startover" var="resetUrlLabel"/>
<spring:theme code="myaccount.confirmation.search.placeholder" var="searchPlaceholder"/>
<spring:theme code="myaccount.confirmation.button.search" var="searchBtnText"/>
<spring:theme code="myaccount.confirmation.button.reset" var="resetBtnText"/>
<c:if test="${searchResultCount ne 0 or not empty searchKey}">
    <b2b-order:searchOrderForm formID="orderAcknowledgementSearchForm" formActionUrl="${searchActionUrl}"
                           formTitle="${titleOrderAck}" searchResultCount="${searchResultCount}" searchKey="${searchKey}"
                           searchPlaceholder="${searchPlaceholder}" searchButtonText="${searchBtnText}"
                           resetButtonText="${resetBtnText}" resetUrl="${resetUrl}" resetUrlLabel="${resetUrlLabel}"
                           qaAttributeSearchBtn="${'order-confirmation-search-btn'}" qaAttributeTextbox="${'order-confirmation-search-textbox'}"
                           qaAttributeResetBtn="${'order-confirmation-reset-btn'}">
                           </b2b-order:searchOrderForm>
    <b2b-order:orderConfirmationListing searchUrl="${searchUrl}" messageKey="text.account.orderHistory.page"></b2b-order:orderConfirmationListing>
</c:if>

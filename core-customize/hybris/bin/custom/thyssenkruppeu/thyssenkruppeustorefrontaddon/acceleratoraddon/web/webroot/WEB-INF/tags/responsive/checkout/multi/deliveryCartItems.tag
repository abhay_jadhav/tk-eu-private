<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>

<c:if test="${cartData.deliveryItemsQuantity eq 0}">
    <spring:theme code="checkout.pickup.no.delivery.required"/>
</c:if>
<ul class="m-checkout-order-list item__list" data-qa-id="order-summary-table">
    <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
        <c:if test="${entry.deliveryPointOfService == null}">
            <c:url value="${entry.product.url}" var="productUrl"/>
            <li class="m-checkout-order-list__item item__list--item">
                <div class="m-checkout-order-list__item__thumb item__image">
                    <a href="${productUrl}">
                        <product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
                    </a>
                </div>
                <div class="m-checkout-order-list__item__name item__product" data-qa-id="order-summary-item-name">
                    <a href="${productUrl}"><span class="m-text-color--black">${fn:escapeXml(entry.product.name)}</span></a>
                    <c:if test="${not empty entry.product.variantValues}">
                        <div class="item__attributes">
                            <c:forEach items="${entry.product.variantValues}" var="variantValue" varStatus="loop">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <p class="m-item-group__label small m-text-color--grey-dark" data-qa-id="item-variant-attribute-label" >${fn:escapeXml(variantValue.name)}</p>
                                    </div>
                                    <div class="col-sm-7">
                                        <p class="m-item-group__value small" data-qa-id="item-variant-attribute-value" >${fn:escapeXml(variantValue.value)}</p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                    <div class="m-checkout-order-list__item__details item__calculations">
                        <hr class="m-checkout-order-list__item__details__cost__separator"/>
                        <div class="m-checkout-order-list__item__details__cost">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="m-checkout-order-list__item__details__cost__label small" data-qa-id="order-summary-base-uom-unit-name">
                                        <c:if test="${entry.unit == entry.product.baseUnit.code}">
                                            <span data-qa-id="order-summary-base-uom-unit-quantity" class="text-lowercase m-text-color--grey-dark">${entry.quantity}&nbsp;${entry.unitName}</span> x <span class="m-text-color--grey-dark" data-qa-id="order-summary-base-uom-unit-price"><tk-eu-format:price priceData="${entry.basePrice}" withUnit="${true}" displayFreeForZero="true"/></span>
                                        </c:if>
                                        <c:if test="${entry.unit ne entry.product.baseUnit.code}">
                                            <span data-qa-id="order-summary-base-uom-unit-quantity">${entry.quantity}&nbsp;${entry.unitName}</span> x <span class="m-text-color--grey-dark" data-qa-id="order-summary-sales-to-base-factor"><tk-eu-format:price priceData="${entry.salesUnitData.salesToBaseUnitFactor}" withUnit="${true}" isWeight= "${true}"/></span> x <span class="m-text-color--grey-dark" data-qa-id="order-summary-base-uom-unit-price"><tk-eu-format:price priceData="${entry.pricePerBaseUnit}" withUnit="${true}" displayFreeForZero="true"/></span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="col-sm-5 text-right">
                                    <p class="m-checkout-order-list__item__details__cost__label small m-text-color--grey-dark" data-qa-id="order-summary-base-uom-unit-name"> <span class="m-text-color--grey-dark" data-qa-id="order-summary-base-uom-unit-price"> <format:price priceData="${entry.subTotal}" displayFreeForZero="true"/> </span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item__list--options">
                <c:set var="certificates" value=""/>
                <c:if test="${entry.product.configurable}">
                    <div class="item__options">
                        <common:configurationInfos entry="${entry}" separatorClass="m-checkout-order-list__item__details__certificates__separator"/>
                        <%--NOTE: Accelerator inherited modify feature. Unhide when required.
                        <c:if test="${not empty entry.configurationInfos and not isTradeLength}">
                            <div class="row item__options__row">
                                <div class="col-md-6">
                                    <div class="item__attributes">
                                        <div class="m-item-group--horizontal">
                                            <p class="m-item-group__label"><small>&nbsp;</small></p>
                                            <div class="m-item-group__value">
                                                <c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
                                                <a href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        --%>
                    </div>
                </c:if>
                <div class="m-order-detail__subtotal m-b-5">
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="pali-gray-hr1 m-b-10"></div>
                         </div>
                     </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="pali-copy primary semi-strong">
                                <spring:theme code="checkout.item.total.label"/>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <c:choose>
                            <c:when test="${not empty entry.entryTotalDiscount}">
                                    <div class="pali-copy primary semi-strong text-right" data-qa-id="cart-item-grand-total-value">

                                         <strike> <format:price priceData="${entry.entryTotalPriceWithoutDiscount}" displayFreeForZero="true"/></strike>
                                    </div>
                                    </c:when><c:otherwise>
                                    <div class="pali-copy primary semi-strong text-right" data-qa-id="cart-item-grand-total-value">
                                        <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                                    </div>
                                    </c:otherwise>
                                    </c:choose>
                        </div>
                    </div>
                    <c:if test="${not empty entry.entryTotalDiscount}">
                       <div class="row p-b-5">
                        <div class="col-xs-6">
                            <div class="pali-copy references">
                                <format:price priceData="${entry.entryTotalDiscount}"/>&nbsp;<spring:theme code="discount.on.item" text="Material discount"/>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="pali-copy primary semi-strong red-text text-right" data-qa-id="order-summary-item-total-price">
                                <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                            </div>
                        </div>
                    </div>
                    </c:if>
                </div>
                <%--NOTE: Accelerator inherited feature, unhide if requied.
                <div>
                    <c:forEach items="${entry.product.baseOptions}" var="option">
                        <c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
                            <c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
                                <div>${selectedOption.name}: ${selectedOption.value}</div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                    <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber) && showPotentialPromotions}">
                        <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>
                            <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
                                    <c:set var="displayed" value="true"/>
                                    <span class="promotion">${promotion.description}</span>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </c:if>
                    <c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                        <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>
                            <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
                                    <c:set var="displayed" value="true"/>
                                    <span class="promotion">${promotion.description}</span>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </c:if>
                </div> --%>
                <spring:url value="/checkout/multi/getReadOnlyProductVariantMatrix" var="targetUrl"/>
                <grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="hide" targetUrl="${targetUrl}"/>
            </li>
        </c:if>
    </c:forEach>
</ul>

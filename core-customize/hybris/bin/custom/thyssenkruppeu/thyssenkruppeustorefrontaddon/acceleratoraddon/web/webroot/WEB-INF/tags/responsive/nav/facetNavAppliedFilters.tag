<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>
<%@ attribute name="favouriteFacetData" type="com.thyssenkrupp.b2b.global.baseshop.facades.search.data.FavouriteFacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${not empty pageData.breadcrumbs || favouriteFacetData.selected}">

        <div class="m-facet-tab m-facet-tab--applied js-facet">
            <div class="m-facet-tab__heading m-facet-tab__heading--bg">
                <div class="m-facet-tab__heading__h5 h5 js-facet-name">
                    <spring:theme code="search.nav.applied.facets"/>
                </div>
                <c:if test="${not empty pageData.resetAllUrl}">
                    <div class="m-facet-tab__heading__op">
                        <c:url value="${pageData.resetAllUrl}" var="resetAllUrl"/>
                        <a class="m-facet-tab__heading__op__reset" href="${resetAllUrl}">
                            <small>
                                <spring:theme code="search.nav.applied.resetAll"/>
                            </small>
                        </a>
                    </div>
                </c:if>
            </div>

            <div class="m-facet-tab__content">
                <div class="js-facet-values">
                    <ul class="m-facet-tab__content__list">
                        <c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
                            <li class="m-facet-tab__content__list__item">
                                <label class="m-facet-tab__content__list__item__label m-facet-tab__content__list__item__label--applied-tag">
                                    <c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
                                    ${fn:escapeXml(breadcrumb.facetName)}&colon;${fn:escapeXml(breadcrumb.facetValueName)}&nbsp;
                                </label>

                                <a href="${removeQueryUrl}&amp;favourites=${favouriteFacetData.selected}">
                                    <span class="u-icon-tk u-icon-tk--close-single pull-right"></span>
                                </a>
                            </li>
                        </c:forEach>

                        <c:if test="${favouriteFacetData.selected}">
                            <li class="m-facet-tab__content__list__item">
                                <label class="m-facet-tab__content__list__item__label m-facet-tab__content__list__item__label--applied-tag">
                                    <spring:theme code="search.nav.applied.favourites" text="Favourites"/>&nbsp;
                                </label>

                                <c:url value="${favouriteFacetData.removeUrl}" var="removeQueryUrl"/>
                                <a href="${removeQueryUrl}"><span class="u-icon-tk u-icon-tk--close-single"></span></a>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <div class="m-facet-tab m-facet-tab--applied js-facet">
            <div class="m-facet-tab__heading m-facet-tab__heading--bg m-facet-tab__heading--no-margin-bottom">
                <div class="m-facet-tab__heading__h5 h5 js-facet-name">
                    <spring:theme code="search.nav.filters"/>
                </div>
                <div class="m-facet-tab__heading__op">
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>

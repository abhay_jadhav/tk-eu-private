<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="idKey" required="true" type="java.lang.String"%>
<%@ attribute name="labelKey" required="true" type="java.lang.String"%>
<%@ attribute name="path" required="true" type="java.lang.String"%>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@ attribute name="labelCSS" required="false" type="java.lang.String"%>
<%@ attribute name="inputCSS" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.String"%>
<%@ attribute name="qaAttribute" type="java.lang.String"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:errorSpanField path="${path}">
    <spring:theme code="${idKey}" var="themeIdKey"/>
        <div class="m-checkbox">
            <label class="control-label ${labelCSS}" for="${themeIdKey}">
                <form:checkbox cssClass="m-checkbox__input sr-only ${inputCSS}" id="${themeIdKey}" path="${path}" tabindex="${tabindex}" disabled="${disabled}" data-qa-id="${qaAttribute}"/>
                <spring:theme code="${labelKey}"/>
                <span class="m-checkbox__label">
                    <span class="m-checkbox__label__box"></span>
                </span>
                <c:if test="${mandatory != null && mandatory == true && not empty labelKey}">
                    <span class="mandatory">&nbsp;<spring:theme code="form.mandatory"></spring:theme></span>
                </c:if>
                <c:if test="${mandatory != null && mandatory == true}">
                    <span class="mandatory">
                        <spring:theme code="login.required" var="loginrequiredText" />
                    </span>
                </c:if>
                <span class="skip"><form:errors path="${path}"/></span>
            </label>
        </div>
</template:errorSpanField>

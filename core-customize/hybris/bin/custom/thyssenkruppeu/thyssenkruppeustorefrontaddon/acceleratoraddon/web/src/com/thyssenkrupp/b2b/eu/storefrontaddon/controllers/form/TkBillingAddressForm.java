package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

public class TkBillingAddressForm {

    private String poNumber;
    private String orderName;
    private String selectedAddressCode;

    public String getSelectedAddressCode() {
        return selectedAddressCode;
    }

    public void setSelectedAddressCode(String selectedAddressCode) {
        this.selectedAddressCode = selectedAddressCode;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }
}

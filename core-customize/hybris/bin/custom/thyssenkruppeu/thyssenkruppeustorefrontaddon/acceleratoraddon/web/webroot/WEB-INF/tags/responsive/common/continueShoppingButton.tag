<%@ attribute name="browseCatalogUrl" required="true" type="java.lang.String" %>
<%@ attribute name="browseCatalogueButtonLabel" required="true" type="java.lang.String" %>
<%@ attribute name="isButton" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<a href="${browseCatalogUrl}" class="${isButton ? 'btn btn-default' : '' }" data-qa-id="confirmation-continue-btn">${browseCatalogueButtonLabel}</a>

/*global ACC, document, window, Cookie, $ */

//NOTE: Unlink refinments init method  to prevent browser prompt for geolocation.
ACC.refinements.init = $.noop;
ACC.refinements.bindMoreLessToggles = function (){
    $(document).on("click",".js-shop-stores-facet .js-facet-change-link",function(e){
        e.preventDefault();
        $(".js-shop-stores-facet .js-facet-container").hide();
        $(".js-shop-stores-facet .js-facet-form").show();
    })

    $(document).on("change",".js-product-facet .js-facet-checkbox",function(){
        $(this).parents("form").submit();
    })

    $(document).on("click",".js-product-facet .js-more-facet-values-link",function(e){
        e.preventDefault();
        $(this).parents(".js-facet").find(".js-facet-top-values").removeClass('m-facet-tab__content__list--show').addClass('m-facet-tab__content__list--hide');
        $(this).parents(".js-facet").find(".js-facet-list-hidden").removeClass('m-facet-tab__content__list--hide').addClass('m-facet-tab__content__list--show');
        $(this).parents(".js-facet").find(".js-more-facet-values").hide();
        $(this).parents(".js-facet").find(".js-less-facet-values").show();
    })

    $(document).on("click",".js-product-facet .js-less-facet-values-link",function(e){
        e.preventDefault();
        $(this).parents(".js-facet").find(".js-facet-top-values").removeClass('m-facet-tab__content__list--hide').addClass('m-facet-tab__content__list--show');
        $(this).parents(".js-facet").find(".js-facet-list-hidden").removeClass('m-facet-tab__content__list--show').addClass('m-facet-tab__content__list--hide');
        $(this).parents(".js-facet").find(".js-more-facet-values").show();
        $(this).parents(".js-facet").find(".js-less-facet-values").hide();
    })

    $('.js-facet-tab-toggle').click(function (){
        var $facetTabContent = $(this).closest('.js-facet');

        if($facetTabContent.hasClass('m-facet-tab--show')) {
            $facetTabContent.removeClass('m-facet-tab--show').addClass('m-facet-tab--hide');
            $(this).find('.u-icon-tk').removeClass('u-icon-tk--minus').addClass('u-icon-tk--plus');
            Cookie.erase($facetTabContent.attr('id'));
        } else {
            $facetTabContent.removeClass('m-facet-tab--hide').addClass('m-facet-tab--show');
            $(this).find('.u-icon-tk').addClass('u-icon-tk--minus').removeClass('u-icon-tk--plus');
            Cookie.create($facetTabContent.attr('id'), 1, 30);
        }
    })

    $('.js-facet-radio-link, .js-facet-checkbox-link').click(function () {

        var hrefLink = $(this).next().find('a').attr('href');
        window.location.href = hrefLink;
    })

    $('.js-facet-link-trigger').click(function () {

        var $input = $(this).closest('.js-facet-label').find('.js-facet-radio-link, .js-facet-checkbox-link');
        $input.prop('checked', !$input.prop('checked'));
    })

    $('.js-facet').each(function (i, facet) {
        var id = $(facet).attr('id');
        if (id && Cookie.read(id) === "1") {
            $(facet).find('.js-facet-content').collapse('show');
            $(facet).removeClass('m-facet-tab--hide').addClass('m-facet-tab--show');
            $(facet).find('.u-icon-tk').addClass('u-icon-tk--minus').removeClass('u-icon-tk--plus');
        }
    });

    $(".js-facet-form").each(function() {
        if($(this).attr("data-scroll") == "true"){
            
            $(this).find(".m-facet-tab__filter").show();
            $(this).find(".m-facet-tab__contentdiv").addClass("custom-scrollbar");
        }else{
            $(this).find(".m-facet-tab__filter").hide();
            $(this).find(".m-facet-tab__contentdiv").removeClass("custom-scrollbar");
        }
    });

    $(window).on("load", function() {
        $(".custom-scrollbar").mCustomScrollbar({theme:"dark-3"});
    });

    $('.js-filter-facet').on('keyup', function() {
        
        var searchTerm = $(this).val().toLowerCase();
        var txtValue;
        var nextAll = $(this).parent().nextAll(".m-facet-tab__contentdiv");
        nextAll.each(function(){
    
            $(this).find('.js-facet-list').children("li").each(function(){
                txtValue = $(this).find(".m-checkbox__label__text").html().trim();
                if (txtValue.toLowerCase().indexOf(searchTerm) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        
    
        if(parseInt($(this).find('.js-facet-list > li:visible').length) <= 0){
            $(this).find(".m-facet-tab__filter__msg").show();                    
        }else{                    
            $(this).find(".m-facet-tab__filter__msg").hide();
        }
        
        });

    });
           
}

$(document).on("click",".js-page-title [data-toggle='collapse']",function(){
    $(".panel-collapse").removeClass("in");
    $(this).closest(".panel").find(".panel-collapse").addClass("in");
    $(this).closest(".panel").find(".panel-collapse").css("height", "");
});

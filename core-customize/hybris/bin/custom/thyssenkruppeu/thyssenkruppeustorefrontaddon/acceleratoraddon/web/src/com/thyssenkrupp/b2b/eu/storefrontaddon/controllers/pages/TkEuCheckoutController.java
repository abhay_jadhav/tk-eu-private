/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.pages;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thyssenkrupp.b2b.eu.facades.utils.TkEnumUtils;
import com.thyssenkrupp.b2b.global.baseshop.storefront.controllers.pages.CheckoutController;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.servicelayer.session.SessionService;

@RequestMapping(value = "/checkout")
public class TkEuCheckoutController extends CheckoutController {

    private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
    private static final Logger LOG                              = Logger.getLogger(TkEuCheckoutController.class);
    private static final String REDIRECT_PREFIX                  = "redirect:";

    @Resource(name = "b2bUnitFacade")
    protected B2BUnitFacade b2bUnitFacade;

    @Resource(name = "checkoutFacade")
    private CheckoutFacade checkoutFacade;

    @Resource
    private SessionService sessionService;
    
    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource( name = "enumUtils")
    private TkEnumUtils enumUtils;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String checkout(final RedirectAttributes redirectModel) {

        sessionService.getCurrentSession().setAttribute("newAddressStatus", false);
        sessionService.getCurrentSession().setAttribute("addAddressButton", false);
        if (getCheckoutFlowFacade().hasValidCart()) {
            if (validateCart(redirectModel)) {
                return REDIRECT_PREFIX + "/cart";
            } else {
                checkoutFacade.prepareCartForCheckout();
                return getCheckoutRedirectUrl();
            }
        }
        LOG.info("Missing, empty or unsupported cart");
        // No session cart or empty session cart. Bounce back to the cart page.
        return REDIRECT_PREFIX + "/cart";
    }

    @Override
    @RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request, final Model model,
        final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
        setAdditionalParameters(orderCode, model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("checkout.order.confirmation.text"));
        return processOrderCode(orderCode, model, request, redirectModel);
    }

    private void setAdditionalParameters(final String orderCode, final Model model) {
        final B2BUnitData b2bUnitData = b2bUnitFacade.getParentUnit();
        if (b2bUnitData != null) {
            model.addAttribute("b2bUnitData", b2bUnitData);
        }
    }

    @Override
    protected void processEmailAddress(final Model model, final OrderData orderDetails) {
        final String uid;

        if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm")) {
            final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
            guestRegisterForm.setOrderCode(orderDetails.getGuid());
            uid = orderDetails.getPaymentInfo().getBillingAddress().getEmail();
            guestRegisterForm.setUid(uid);
            model.addAttribute(guestRegisterForm);
        } else {
            uid = orderDetails.getUser().getUid().contains("|") ? StringUtils.substringAfter(orderDetails.getUser().getUid(), "|") : orderDetails.getUser().getUid();
        }
        model.addAttribute("email", uid);
    }

    @Override
    protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        String returnOrderConfirmationPage = super.processOrderCode(orderCode, model, request, redirectModel);
        OrderData orderData = (OrderData) model.asMap().get("orderData");
        orderData.setOrderStatusName(getEnumUtils().getNameFromEnumCode(orderData.getStatus()));
        return returnOrderConfirmationPage;
    }

    public TkEnumUtils getEnumUtils() {
        return enumUtils;
    }

    public void setEnumUtils(TkEnumUtils enumUtils) {
        this.enumUtils = enumUtils;
    }
}

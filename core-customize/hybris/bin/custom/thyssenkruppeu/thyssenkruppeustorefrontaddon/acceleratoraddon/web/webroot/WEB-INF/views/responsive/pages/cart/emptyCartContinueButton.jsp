<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<div class="cart__actions">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-6 col-sm-5">
            <div class="col-md-6 col-lg-5">
                <action:actions element="div" parentComponent="${component}"/>
            </div>
        </div>
    </div>
</div>


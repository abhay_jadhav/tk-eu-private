<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="certificates" required="false" type="java.lang.String"%>
<%@ attribute name="separatorClass" required="false" type="String" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="availableConfigurations" value="${0}"/>
<spring:eval expression="T(com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants).HANDLING_COSTS_FIXED" var="absHandling"/>
<spring:eval expression="T(com.thyssenkrupp.b2b.eu.pricedata.constants.ThyssenkruppeupricedataConstants).HANDLING_COSTS" var="handling"/>
<c:forEach var="config" items="${entry.configurationInfos}" varStatus="configLoop">
    <spring:eval expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).TKTRADELENGTH" var="isTradeLength" />
    <spring:eval expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CERTIFICATE" var="isCertificate" />
    <spring:eval expression="config.configuratorType == T(de.hybris.platform.catalog.enums.ConfiguratorType).CUTTOLENGTH_PRODINFO" var="isCutToLength" />
    <c:choose>
        <c:when test="${isCertificate}">
            <c:if test="${config.configurationValue eq 'true'}">
                <c:set var="availableConfigurations" value="${availableConfigurations + 1}"/>
                <c:if test="${availableConfigurations eq 1}">
                    <hr class="${separatorClass}"/>
                </c:if>
                <c:set var="certificate" value="${certificates},${config.configurationLabel}" scope="request"/>
                <div class="row item__options__row">
                    <div class="col-md-3">
                        <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                            <div class="small">
                                <p class="small m-item-group__label" data-qa-id="cart-configurations-label">
                                    <c:if test="${availableConfigurations eq 1}"><spring:theme code="basket.page.item.options" text="Options:"/></c:if>&nbsp;
                                </p>
                                <%-- <common:certificateconfigurationInfo configurationInfo="${config}" styleClass="m-item-group__value small"/> --%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <common:certificateconfigurationInfo configurationInfo="${config}" styleClass="small"/>
                    </div>
                    <div class="col-md-4">
                        <p class="small item__options__total text-right">
                            <format:price priceData="${entry.certificatePrice}"/>
                        </p>
                    </div>
                </div>
            </c:if>
        </c:when>
        <c:when test="${isTradeLength}">
             <%--Do not render here --%>
        </c:when>
        <c:when test="${isCutToLength}">
             <c:if test="${config.configurationValue eq 'true'}">
                <c:set var="availableConfigurations" value="${availableConfigurations + 1}"/>
                <c:if test="${availableConfigurations eq 1}">
                    <hr class="${separatorClass}"/>
                </c:if>
                <c:set var="certificate" value="${certificates},${config.configurationLabel}" />
                <div class="row item__options__row">
                    <div class="col-md-8">
                        <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                            <div class="m-item-group--horizontal small">
                                <p class="m-item-group__label small" data-qa-id="cart-configurations-label">
                                    <c:if test="${availableConfigurations eq 1}"><spring:theme code="basket.page.item.options" text="Options:"/></c:if>&nbsp;
                                </p>
                                <common:cutToLengthConfigurationInfo configurationInfo="${config}" styleClass="m-item-group__value small"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="item__options__total text-right small" data-qa-id="cart-cutting-cost">
                            <format:price priceData="${entry.sawingCost}"/>
                        </p>
                    </div>
                </div>
            </c:if>
        </c:when>

        <c:otherwise>
            <c:if test="${not empty config.configurationLabel and not empty config.configurationValue}">
                <c:set var="availableConfigurations" value="${availableConfigurations + 1}"/>
                <c:if test="${availableConfigurations eq 1}">
                    <hr class="${separatorClass}"/>
                </c:if>
                <div class="row item__options__row">
                    <div class="col-md-8">
                        <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                            <div class="m-item-group--horizontal small">
                                <p class="m-item-group__label small" data-qa-id="cart-configurations-label">
                                    <c:if test="${configLoop.first}"><spring:theme code="basket.page.item.options" text="Options:"/></c:if>&nbsp;
                                </p>
                                <p class="m-item-group__value small" data-qa-id="cart-configurations-value">
                                    <span class="m-certificate">${config.configurationLabel}${not empty config.configurationLabel ? ':' : ''}${config.configurationValue}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="small item__options__total text-right" data-qa-id="cart-cofiguration-price">
                            &nbsp;
                        </p>
                    </div>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
</c:forEach>
<c:if test="${not empty entry.handlingCost}">
 <c:if test="${availableConfigurations eq 0}">
        <hr class="${separatorClass}"/>
  </c:if>
<div class="row item__options__row cart-handling">
    <div class="col-md-8">
        <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
            <div class="m-item-group--horizontal small">
               <p class="m-item-group__label small" data-qa-id="cart-configurations-label">
                    <c:if test="${availableConfigurations eq 0}"><spring:theme code="basket.page.item.options" text="Options:"/></c:if>&nbsp;
                    <c:set var="availableConfigurations" value="${availableConfigurations + 1}"/>
                </p>
                <p class="m-item-group__value small" data-qa-id="cart-configurations-value">
                    <c:if test="${entry.handlingCostType eq handling}">
                    <span class="m-handling-text"><spring:theme
                            code="basket.page.item.handlingCost" text="+Handling" /></span>
                    <i data-toggle="tooltip" title="<spring:theme code="checkout.handling.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                    </c:if>
                    <c:if test="${entry.handlingCostType eq absHandling}">
                    <span class="m-handling-text"><spring:theme
                            code="basket.page.item.absHandlingCost" text="+Handling abs" /></span>
                    <i data-toggle="tooltip" title="<spring:theme code="checkout.handling.abs.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
                    </c:if>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <p class="small item__options__total text-right">
            <format:price priceData="${entry.handlingCost}" />
       </p>
    </div>
</div>
</c:if>
<c:if test="${not empty entry.packagingCosts}">
    <c:if test="${availableConfigurations eq 0}">
        <hr class="${separatorClass}"/>
    </c:if>
    <div class="row item__options__row">
        <div class="col-md-8">
            <div class="item__attributes ${config.status eq errorStatus ? 'has-error' : ''}">
                <div class="m-item-group--horizontal small">
                    <p class="m-item-group__label small" data-qa-id="cart-configurations-label">
                        <c:if test="${availableConfigurations eq 0}"><spring:theme code="basket.page.item.options" text="Options:"/></c:if>&nbsp;
                    </p>
                    <p class="m-item-group__value small" data-qa-id="cart-configurations-value">
                        <span class="m-certificate"><spring:theme code="basket.page.item.packagingCost" text="+Verpackung"/></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <p class="small item__options__total text-right" data-qa-id="cart-cofiguration-price">
                <format:price priceData="${entry.packagingCosts}"/>
            </p>
        </div>
    </div>
</c:if>

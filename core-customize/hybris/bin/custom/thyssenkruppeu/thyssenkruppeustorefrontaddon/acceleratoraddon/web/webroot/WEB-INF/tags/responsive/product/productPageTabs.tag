<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="device" required="true" type="java.lang.String" %>
<%@ taglib prefix="productdownload" tagdir="/WEB-INF/tags/responsive/product"%>

<spring:eval expression="T(de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum).VISIBLE_SPECS" var="VISIBLE_SPECS"/>
<spring:eval expression="T(de.hybris.platform.catalog.enums.ClassificationAttributeVisibilityEnum).VISIBLE_INFO" var="VISIBLE_INFO"/>

<ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" aria-controls="product-primary-specs" data-toggle="tab" class="active"><a class="nav-link h5 m-text-color--black" href="#product-primary-specs${device}" data-toggle="tab"><spring:theme code="product.product.specification"  text="Produkt Specification"/></a></li>
    <li role="presentation" aria-controls="product-info" data-toggle="tab"><a class="nav-link h5 m-text-color--black" href="#product-info${device}" data-toggle="tab"><spring:theme code="product.product.info" /></a></li>
    <c:if test="${not empty product.mediaData}">
        <li role="presentation" aria-controls="product-downloads" data-toggle="tab"><a class="nav-link h5 m-text-color--black" href="#product-downloads${device}" data-toggle="tab"><spring:theme code="pdp.product.download.tabheader" /></a></li>
    </c:if>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane in active js-product-primary-specifications" id="product-primary-specs${device}" >
        <div class="panel panel-default">
            <div class="custom-heading">
                <h4 class="panel-title js-page-title">
                    <a data-toggle="collapse" data-target="#collapseOne${device}" data-parent=".tab-pane" href="#collapseOne${device}" aria-expanded="true" aria-controls="collapseOne${device}">
                        <spring:theme code="product.product.specification" text="Produkt Specification"/>
                    </a>
                </h4>
            </div>
            <div id="collapseOne${device}" class="panel-collapse collapse in">
                <div class="panel-body product_classifications js-product-main-classifications">
                    <c:set var="visibleFeatures" value="${mergedVisibleFeatureMap}"/>
                    <c:if test="${not empty visibleFeatures}">
                        <product:productSpecificationDetails visibleFeatures="${visibleFeatures}"/>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane js-product-informations" id="product-info${device}" data-qa-id="pdp-item-specs-section">
        <div class="panel panel-default">
            <div class="custom-heading">
                <h4 class="panel-title js-page-title">
                    <a data-toggle="collapse" data-target="#collapseTwo${device}" data-parent=".tab-pane" href="#collapseTwo${device}" aria-controls="collapseTwo${device}">
                        <spring:theme code="product.product.info" />
                    </a>
                </h4>
            </div>
            <div id="collapseTwo${device}" class="panel-collapse collapse">
                <div class="panel-body">
                    <product:productDetailsClassifications product="${product}" baseProductData="${baseProductData}" visibilityEnum="${VISIBLE_INFO}" showDescription="${true}"/>
                </div>
            </div>
        </div>
    </div>
    <c:if test="${not empty product.mediaData}">
        <div role="tabpanel" class="tab-pane js-product-downloads" id="product-downloads${device}">
            <div class="panel panel-default">
                <div class="custom-heading">
                    <h4 class="panel-title js-page-title">
                        <a data-toggle="collapse" data-target="#collapseThree${device}" data-parent=".tab-pane" href="#collapseThree${device}" aria-controls="collapseThree${device}">
                            <spring:theme code="pdp.product.download.tabheader" />
                        </a>
                    </h4>
                </div>
                <div id="collapseThree${device}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="m-product-download">
                            <p class="m-text-color--black m-product-download__title"> <spring:theme code="pdp.product.download.header" /></p>
                            <product:ProductDocumentDownload product="${product}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</div>

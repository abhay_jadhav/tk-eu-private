<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="m-product-detail-panel__weight js-addtocart-length-panel display-none">
    <p class="text-right">
        <c:if test="${not empty cartEntryData.productTotalLength}">
            <em>
                <span data-qa-id="total-length-label"><spring:theme code="pdp.product.totalLength"/></span>:
                <span data-qa-id="total-length-value"><tk-eu-format:price priceData="${cartEntryData.productTotalLength}" isWeight= "${false}" withUnit="${true}"/><br/></span>
            </em>
        </c:if>
    </p>
</div>

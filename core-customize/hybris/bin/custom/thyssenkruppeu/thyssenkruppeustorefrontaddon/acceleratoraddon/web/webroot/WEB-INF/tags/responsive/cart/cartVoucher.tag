<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:url value="/cart/voucher/apply" var="applyVoucherAction"/>
<spring:url value="/cart/voucher/remove" var="removeVoucherAction"/>

<c:set var="containerClass">
    <c:choose>
        <c:when test="${not empty errorMsg}">has-error</c:when>
        <c:when test="${not empty successMsg}">has-success</c:when>
        <c:otherwise></c:otherwise>
    </c:choose>
</c:set>
<c:if test="${empty cartData.quoteData}">
<c:if test="${!disableUpdate}">

<div class="form-group m-b-15 js-voucher-respond ${containerClass}">
    <spring:theme code="text.voucher.apply.input.placeholder" var="voucherInputPlaceholder"/>
       <a href="javascript:void();" class="js-cart-voucher__label pali-a link-secondary semi-bold p-b-10 d-block"><spring:theme
            code="text.coupon.apply.input.label"/>
            <i title="" class="pali-u-icon-tk pali-u-icon-tk--arrow-down float-right"></i></a>

    <form:form id="applyVoucherForm" class="js-voucher-form m_voucher-form" action="${applyVoucherAction}" method="post" commandName="voucherForm">
        <form:input cssClass="js-voucher-code cart-voucher__input pali-input form-control m-b-10" name="voucher-code"
                    id="js-voucher-code-text" maxlength="100" placeholder="${voucherInputPlaceholder}"
                    path="voucherCode" disabled="${disableUpdate}"/>
         <div class="js-voucher-validation-container pali-copy references m-b-10 error-txt">
            ${errorMsg}
         </div>

        <c:if test="${not disableUpdate}">
                <button type="button" id="js-voucher-apply-btn"
                    class="pali-btn second-prio d-block w-100 cart-voucher__btn m-b-10">
                    <spring:theme code="text.voucher.apply.button.label" />
                </button>
            </c:if>
    </form:form>
    <div class="pali-gray-hr1"></div>

<c:if test="${not empty successMsg}">
    <div class="js-voucher-validation-container">
        <div class="pali-alert-msg alert-success m-t-10">
            <p> ${successMsg}</p>
        </div>
    </div>
</c:if>

</div>
</c:if>

<!--  fired massages code -->
<c:if test="${not empty cartData.appliedVouchers}">
    <div class="cartproline m-b-15">
        <p class="cartproline pali-copy secondary semi-bold">
        <spring:theme code="basket.received.promotions" />
        </p>
        <ycommerce:testId code="cart_recievedPromotions_labels">
                <c:forEach items="${cartData.appliedOrderPromotions}"
                    var="promotion">
                    <div class="promotion pali-copy references grey-dark-text">${promotion.description}</div>
                </c:forEach>
                <c:forEach items="${cartData.appliedProductPromotions}"
                    var="promotion">
                    <div class="promotion pali-copy references grey-dark-text">${promotion.description}</div>
                </c:forEach>

            </ycommerce:testId>
    </div>
    <div class="pali-gray-hr1"></div>
</c:if>
<script id="cartPromotionSectionTemplate" type="text/x-jquery-tmpl">
    {{if appliedOrderPromotions.length > 0}}
        <div class="cartproline">
             <spring:theme code="basket.received.promotions" />
             <ycommerce:testId code="cart_recievedPromotions_labels">
             {{each(index, appliedOrderPromotion) appliedOrderPromotions}}
                 <div class="promotion">{{= appliedOrderPromotion.description}}</div>
             {{/each}}
             </ycommerce:testId>
         </div>
    {{/if}}
</script>

<div id="ajaxCartPromotionSection">
</div>

<c:if test="${not empty cartData.appliedVouchers}">
<div id="js-applied-vouchers" class="selected_product_ids clearfix voucher-list">
    <c:forEach items="${cartData.appliedVouchers}" var="voucher" varStatus="loop">
        <div class="voucher-list__item pali-copy primary semi-bold p-tb-10">
            <form:form id="removeVoucherForm${loop.index}" action="${removeVoucherAction}" method="post"
                       commandName="voucherForm">

                       <c:if test="${!disableUpdate}">
                       <a href="javascript:void();" class="pali-a link-references-red float-right js-release-voucher-remove-btn">Delete
                       <i title="" class="pali-u-icon-tk pali-u-icon-tk--close-single"></i></a>
                       </c:if>
               <span class="js-release-voucher voucher-list__item-box" id="voucher-code-${voucher}">
                     ${voucher}
                     <form:input hidden="hidden" value="${voucher}" path="voucherCode"/>

                </span>

            </form:form>
            <span class="clearfix"></span>
            <!-- <p class="pali-copy references">You have got 200 flat discount on cart total</p> -->
        </div>
        <div class="pali-gray-hr1"></div>
    </c:forEach>
    <div class="pali-black-hr1"></div>
</div>
</c:if>

<!-- <div class="help-block">
    <em>
        <spring:theme code="text.shipping.date.expected"/>
        <br/>
        <spring:theme code="text.shipping.date.info"/>
    </em>
</div> -->

</c:if>


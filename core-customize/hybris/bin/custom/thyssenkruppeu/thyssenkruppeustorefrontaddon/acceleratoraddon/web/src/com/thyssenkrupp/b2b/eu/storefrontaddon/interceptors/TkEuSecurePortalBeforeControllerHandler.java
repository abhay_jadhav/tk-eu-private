
package com.thyssenkrupp.b2b.eu.storefrontaddon.interceptors;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.secureportaladdon.interceptors.SecurePortalBeforeControllerHandler;
import de.hybris.platform.secureportaladdon.interceptors.SecurePortalRequestProcessor;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class TkEuSecurePortalBeforeControllerHandler extends SecurePortalBeforeControllerHandler {

    private static final Logger LOG = Logger.getLogger(TkEuSecurePortalBeforeControllerHandler.class);
    protected SessionService                   sessionService;
    private   Set<String>                      unsecuredUris;
    private   Set<String>                      controlUris;
    private   CMSSiteService                   cmsSiteService;
    private   UserService                      userService;
    private   String                           defaultLoginUri;
    private   String                           checkoutLoginUri;
    private   SiteBaseUrlResolutionService     siteBaseUrlResolutionService;
    private   SiteBaseUrlResolutionService     tkEuSiteBaseUrlResolutionService;
    private   RedirectStrategy                 redirectStrategy;
    private   SecurePortalRequestProcessor     requestProcessor;

    @Override
    public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response, final HandlerMethod handler) throws Exception {
        boolean returning = true;

        if (isSiteSecured() && !requestProcessor.skipSecureCheck()) {
            final boolean isUserAnonymous = userService.isAnonymousUser(userService.getCurrentUser());

            final String otherRequestParam = requestProcessor.getOtherRequestParameters(request);

            if (isUriPartOfSet(request, unsecuredUris)) {
                returning = checkAuthentication(request, response, isUserAnonymous, otherRequestParam);
            } else if (isUserAnonymous && isNotLoginRequest(request)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("Anonymous user is accessing the URI '%s' which is secured. Redirecting to login page.", request.getRequestURI()));
                }

                redirect(request, response, getRedirectUrl(defaultLoginUri, true, otherRequestParam));
                returning = false;
            }
        }

        return returning;
    }

    private boolean checkAuthentication(final HttpServletRequest request, final HttpServletResponse response, final boolean isUserAnonymous, final String otherRequestParam) {
        boolean returning = true;
        if (!isUserAnonymous && !isUriPartOfSet(request, controlUris)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("User is already authenticated and accessing a safe-mapping, hence a useless operation. Redirect to home page instead.");
            }

            redirect(request, response, getRedirectUrlIfAuthenticated(otherRequestParam));
            returning = false;
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("The request URI '%s' does not require security. Interceptor is done.", request.getRequestURI()));
            }
        }
        return returning;
    }

    @Override
    protected boolean isSiteSecured() {
        if (StringUtils.isNotBlank(sessionService.getAttribute(CMSFilter.PREVIEW_TICKET_ID_PARAM))) {
            return false;
        }
        final CMSSiteModel site = cmsSiteService.getCurrentSite();
        return site.isRequiresAuthentication();
    }

    @Override
    protected boolean isNotLoginRequest(final HttpServletRequest request) {
        return !request.getRequestURI().contains(defaultLoginUri) || request.getRequestURI().contains(checkoutLoginUri);
    }

    @Override
    protected String getRedirectUrl(final String mapping, final boolean secured, final String otherParameters) {
        if (otherParameters != null) {
            return siteBaseUrlResolutionService.getWebsiteUrlForSite(cmsSiteService.getCurrentSite(), secured, mapping, otherParameters);
        }

        return tkEuSiteBaseUrlResolutionService.getWebsiteUrlForSite(cmsSiteService.getCurrentSite(), secured, mapping);
    }

    @Override
    protected void redirect(final HttpServletRequest request, final HttpServletResponse response, final String targetUrl) {

        try {

            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Redirecting to url '%s'.", targetUrl));
            }

            redirectStrategy.sendRedirect(request, response, targetUrl);

        } catch (final IOException ex) {
            LOG.error("Unable to redirect.", ex);
        }

    }

    @Override
    public void setUnsecuredUris(Set<String> unsecuredUris) {
        this.unsecuredUris = unsecuredUris;
    }

    @Override
    public void setControlUris(Set<String> controlUris) {
        this.controlUris = controlUris;
    }

    @Override
    public void setCmsSiteService(CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    @Override
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void setDefaultLoginUri(String defaultLoginUri) {
        this.defaultLoginUri = defaultLoginUri;
    }

    @Override
    public void setCheckoutLoginUri(String checkoutLoginUri) {
        this.checkoutLoginUri = checkoutLoginUri;
    }

    @Override
    public void setSiteBaseUrlResolutionService(SiteBaseUrlResolutionService siteBaseUrlResolutionService) {
        this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
    }

    @Override
    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    @Override
    public void setRequestProcessor(SecurePortalRequestProcessor requestProcessor) {
        this.requestProcessor = requestProcessor;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setTkEuSiteBaseUrlResolutionService(SiteBaseUrlResolutionService tkEuSiteBaseUrlResolutionService) {
        this.tkEuSiteBaseUrlResolutionService = tkEuSiteBaseUrlResolutionService;
    }
}

<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="tkEuformElement" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/formElement" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<form:form action="${action}" method="post" commandName="loginForm" class="login-form">
    <c:if test="${not empty message}"><span class="has-error"><spring:theme code="${message}"/></span></c:if>
    <tkEuformElement:formInputBox idKey="j_username" labelKey="login.email" path="j_username" mandatory="true" qaAttribute="username-textbox" placeholder="login.email"/>
    <tkEuformElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password" inputCSS="form-control" mandatory="true" qaAttribute="password-textbox" placeholder="login.password"/>
    <div class="row">
        <div class="col-md-6">
            <a href="#" data-link="<c:url value='/login/pw/request'/>" class="login-form__link-forgot-password js-password-forgotten" data-cbox-title="<spring:theme code="forgottenPwd.title"/>" data-qa-id="forgotten-password-link">
                <spring:theme code="login.link.forgottenPwd"/>
            </a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-default login-form__btn login-form__btn__login" data-qa-id="login-btn">
                <spring:theme code="${actionNameKey}"/>
            </button>
        </div>
    </div>
    <c:if test="${expressCheckoutAllowed}">
        <button type="submit" class="btn btn-default login-form__btn login-form__btn__login_express-checkout expressCheckoutButton" data-qa-id="login-btn">
            <spring:theme code="text.expresscheckout.header"/>
        </button>
        <input id="expressCheckoutCheckbox" name="expressCheckoutEnabled" type="checkbox" class="form left doExpressCheckout display-none"/>
    </c:if>
</form:form>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ attribute name="subtotalsCssClasses" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="tk-eu-format" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/cart" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" media="all" href="/_ui/responsive/patternlibrary/css/generic.min.css?v=${buildTimestamp}"/>

<c:if test="${not empty cartData.appliedProductPromotions}">
<div class="row">
    <div class="col-xs-12">
        <p class="pali-copy references p-b-10">
            <format:price priceData="${cartData.productDiscounts}"/> <spring:theme code="basket.total.saving"/>
        </p>
        <div class="pali-gray-hr1 m-b-10"></div>
    </div>
</div>
</c:if>

<div class="row">
<div class="col-xs-7">
        <div class="pali-copy primary semi-strong p-b-10">
            <spring:theme code="basket.page.totals.subtotal"/>
        </div>
    </div>
    <div class="col-xs-5">
        <div class="text-right pali-copy primary semi-strong p-b-10" data-qa-id="checkout-subtotal">
            <format:price priceData="${cartData.subTotalWithoutDiscounts}"/>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="pali-black-hr1 shadow m-b-10"></div>

<div class="row">
<!-- div for cart vocuher section on cart page -->
<div class="col-sm-12">
<c:if test="${empty cartData.quoteData}">
<div class="cart-voucher m-b-15">
             <cart:cartVoucher cartData="${cartData}"/>
        </div>
</c:if>
     </div>

    <c:if test="${cartData.quoteDiscounts.value > 0}">
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="basket.page.quote.discounts"/>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right">
                <format:price priceData="${cartData.quoteDiscounts}"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>
   <c:if test="${cartData.orderDiscounts.value > 0}">
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references semi-bold red-text">
                <spring:theme code="text.account.order.discount"/>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references semi-bold red-text text-right">
                <format:price priceData="${cartData.orderDiscounts}"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>

    <c:if test="${not empty cartData.deliveryCost && cartData.deliveryCost.value > 0}">
        <spring:theme code="checkout.shipping.tooltip.text" var="shippingToolTip"/>
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="checkout.summary.page.totals.delivery"/>
                <i data-toggle="tooltip" title="${shippingToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right" data-qa-id="order-summary-shipping">
                <format:price priceData="${cartData.deliveryCost}" displayFreeForZero="TRUE"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>

    <c:if test="${not empty cartData.handlingCosts}">
        <spring:theme code="checkout.handling.tooltip.text" var="handlingToolTip"/>
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="checkout.summary.page.totals.handling"/>
                <i data-toggle="tooltip" title="${handlingToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right" data-qa-id="checkout-handling-value">
                <format:price priceData="${cartData.handlingCosts}" displayFreeForZero="TRUE"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>

    <c:if test="${not empty cartData.minimumOrderValueSurcharge && cartData.minimumOrderValueSurcharge.value > 0}">
        <spring:theme code="cart.minimumSurcharge.tooltip" var="surchargeToolTip" arguments="${cartData.minimumOrderValueThresholdValue}###${cartData.minimumSurchargeDisplayToolTipValue}" argumentSeparator="###"/>
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text" data-qa-id="cart-minimum-surcharge-label">
                <spring:theme code="cart.minimumSurcharge.label"/>
                <i data-toggle="tooltip" title="${surchargeToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right" data-qa-id="cart-minimum-surcharge-value">
                <format:price priceData="${cartData.minimumOrderValueSurcharge}"/>
            </p>
        </div>
        <div class="clearfix"></div>
     </c:if>
    <c:if test="${not empty cartData.asmServiceFee}">
        <spring:theme code="checkout.serviceFee.tooltip.text" var="handlingToolTip"/>
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                    <spring:theme code="checkout.serviceFee.text"/>
                    <i data-toggle="tooltip" title="${handlingToolTip}" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico"></i>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right" data-qa-id="checkout-asm-value">
                <small>
                    <format:price priceData="${cartData.asmServiceFee}" displayFreeForZero="TRUE"/>
                </small>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>

    <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="checkout.summary.page.totals.packaging" /><i data-toggle="tooltip" title="<spring:theme code="checkout.packaging.flat.tooltip.text" />" class="u-icon-tk u-icon-tk--info m-order-detail__subtotal__amount-label__ico 555555"></i>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p class="pali-copy references grey-dark-text text-right">
                <format:price priceData="${cartData.packagingCosts}"/>
            </p>
    </div>
    <div class="clearfix"></div>

    <c:if test="${not empty cartData.totalPrice && cartData.totalPrice.value > 0}">
        <div class="col-xs-7 m-b-5">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="basket.page.totals.withoutTax"/>
            </p>
        </div>
        <div class="col-xs-5 m-b-5">
            <p data-qa-id="order-summary-net-price" class="pali-copy references grey-dark-text text-right">
                <format:price priceData="${cartData.totalPrice}"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>


    <c:if test="${not empty cartData.totalPrice && cartData.totalPrice.value > 0}">
        <div class="col-xs-7">
            <p class="pali-copy references grey-dark-text">
                <spring:theme code="basket.page.totals.netEuTax"/>
            </p>
        </div>
        <div class="col-xs-5">
            <p data-qa-id="order-summary-tax" class="pali-copy references grey-dark-text text-right">
                <format:price priceData="${cartData.totalTax}"/>
            </p>
        </div>
        <div class="clearfix"></div>
    </c:if>
</div>

<div class="pali-black-hr1 m-tb-15"></div>

<div class="row">
    <div class="col-xs-6">
        <p class="pali-h3">
            <spring:theme code="basket.page.totals.total"/>
        </p>
    </div>
    <div class="col-xs-6" data-qa-id="order-summary-order-total">
        <p data-qa-id="order-summary-net-price" class="pali-h3 text-right">
           <span class="primary-text">
            <c:choose>
                <c:when test="${showTax}">
                    <format:price priceData="${cartData.totalPriceWithTax}"/>
                </c:when>
                <c:otherwise>
                    <format:price priceData="${cartData.totalPrice}"/>
                </c:otherwise>
            </c:choose>
            </span>
        </p>
    </div>
    <div class="clearfix"></div>

    <div class="col-xs-6">
        <p class="pali-copy references grey-dark-text">
            <spring:theme code="text.order.totalWeight" />
        </p>
    </div>
    <div class="col-xs-6">
        <p class="pali-copy references grey-dark-text text-right">
            <tk-eu-format:price priceData="${cartData.totalWeightInkg}" withUnit="${true}" isWeight= "${true}"/>
        </p>
    </div>
    <div class="clearfix"></div>
</div>

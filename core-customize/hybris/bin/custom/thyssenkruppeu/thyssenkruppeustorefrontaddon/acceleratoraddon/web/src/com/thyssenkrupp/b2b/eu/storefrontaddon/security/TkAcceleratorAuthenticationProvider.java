/*
 * Put your copyright text here
 */
 package com.thyssenkrupp.b2b.eu.storefrontaddon.security;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus;
import com.thyssenkrupp.b2b.eu.core.exception.TkEuUserActivationPendingException;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratoraddon.security.B2BAcceleratorAuthenticationProvider;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;

public class TkAcceleratorAuthenticationProvider extends B2BAcceleratorAuthenticationProvider {
    private static final Logger                        LOG                                     = Logger.getLogger(TkAcceleratorAuthenticationProvider.class);
    private static final String                        LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME = "LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME";
    private static final String                        PENDING_ACTIVATION_EMAIL_SEND_DURATION  = "tk.eu.pending.activation.email.duration";
    private              TkEuB2bCustomerAccountService b2bCustomerAccountService;
    private              SessionService                sessionService;
    private              ConfigurationService          configurationService;
    private              EventService                  eventService;
    
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
        UserModel userModel = null;
        UsernamePasswordAuthenticationToken newAuthentication = null;

        // throw BadCredentialsException if user does not exist
        try {
            userModel = getUserService().getUserForUID(StringUtils.lowerCase(username));
            if (userModel instanceof B2BCustomerModel) {
                B2BCustomerModel b2bUser = (B2BCustomerModel) userModel;
                sendPendingActivationEmail(b2bUser);
                if (getSessionService().getCurrentSession().getAttribute(LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME + getEmail(b2bUser)) != null) {
                    getSessionService().getCurrentSession().removeAttribute(LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME + getEmail(b2bUser));
                }
            }
            newAuthentication = new UsernamePasswordAuthenticationToken(userModel.getUid(), authentication.getCredentials(), authentication.getAuthorities());
            newAuthentication.setDetails(authentication.getDetails());
            SecurityContextHolder.getContext().setAuthentication(newAuthentication);
        } catch (UnknownIdentifierException | AmbiguousIdentifierException e) {
            throw new BadCredentialsException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, BAD_CREDENTIALS), e);
        }

        // throw BadCredentialsException if the user does not belong to customer user group
        if (!getUserService().isMemberOfGroup(userModel, getUserService().getUserGroupForUID(Constants.USER.CUSTOMER_USERGROUP))) {
            throw new BadCredentialsException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, BAD_CREDENTIALS));
        }

        return super.authenticate(newAuthentication);
    }

    private void sendPendingActivationEmail(B2BCustomerModel b2bUser) {
        if (B2bRegistrationStatus.PENDING_ACTIVATION.equals(b2bUser.getRegistrationStatus())) {
            Calendar calender = Calendar.getInstance();
            if (getSessionService().getCurrentSession().getAttribute(LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME + getEmail(b2bUser)) == null) {
                triggerEmail(calender, b2bUser);
            } else {
                Calendar lastPendingActivationEmailSentTime = getSessionService().getCurrentSession().getAttribute(LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME + getEmail(b2bUser));
                Calendar cl = (Calendar) lastPendingActivationEmailSentTime.clone();
                cl.add(Calendar.MINUTE, getConfigurationService().getConfiguration().getInt(PENDING_ACTIVATION_EMAIL_SEND_DURATION));
                if (calender.getTime().after(cl.getTime())) {
                    triggerEmail(calender, b2bUser);
                }
            }
            throw new TkEuUserActivationPendingException(messages.getMessage("login.inactivatedUser.message", "login inactivatedUser"));
        }
    }

    private void triggerEmail(Calendar calender, B2BCustomerModel b2bUser) {
        getSessionService().getCurrentSession().setAttribute(LAST_PENDING_ACTIVATION_EMAIL_SENT_TIME + getEmail(b2bUser), calender);
        getB2bCustomerAccountService().sendRegistrationEmail(b2bUser);
        LOG.info("Pending activation email sent to the user :- " + b2bUser.getUid());
    }

    private String getEmail(B2BCustomerModel b2bUser) {
        return b2bUser.getUid().contains("|") ? StringUtils.substringAfter(b2bUser.getUid(), "|") : b2bUser.getUid();
    }

    public TkEuB2bCustomerAccountService getB2bCustomerAccountService() {
        return b2bCustomerAccountService;
    }

    public void setB2bCustomerAccountService(TkEuB2bCustomerAccountService b2bCustomerAccountService) {
        this.b2bCustomerAccountService = b2bCustomerAccountService;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public EventService getEventService() {
        return eventService;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}

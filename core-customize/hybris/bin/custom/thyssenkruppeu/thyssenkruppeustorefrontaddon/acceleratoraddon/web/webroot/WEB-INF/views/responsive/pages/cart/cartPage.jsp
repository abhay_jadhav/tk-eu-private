<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}" class="l-page l-page__cart">
    <div class="container">
        <cart:cartValidation/>
        <cart:cartPickupValidation/>
        <%-- NOTE: Baseshop inherited feature. Unhide if required.
        <div class="m-cart__help-block hide ">
            <div class="text-right">
                <a href="" class="help js-cart-help" data-help="<spring:theme code="text.help" />">
                    <spring:theme code="text.help" />
                    <span class="glyphicon glyphicon-info-sign"></span>
                </a>
                <div class="help-popup-content-holder js-help-popup-content">
                    <div class="help-popup-content">
                        <strong>${fn:escapeXml(cartData.code)}</strong>
                        <spring:theme code="basket.page.cartHelpContent" htmlEscape="false" />
                    </div>
                </div>
            </div>
        </div> --%>
        <cms:pageSlot position="TopContent" var="feature">
            <cms:component component="${feature}"/>
        </cms:pageSlot>

        <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </c:if>

        <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterRightContentSlot" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            <cms:pageSlot position="BottomContentSlot" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </c:if>

        <c:if test="${empty cartData.rootGroups}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
                <cms:component component="${feature}" element="div" class="m-page-section--no-bottom-border"  data-qa-id="cart-empty-cart-msg"/>
            </cms:pageSlot>
            <div class="m-page-section m-page-section--no-bottom-border">
                <h4 class="m-page-section__title m-page-section__title--no-bottom-border">
                    <spring:theme code="cart.shopformaterials"/>
                </h4>
                <cms:pageSlot position="EmptyCartShopForMaterialsContent" var="feature" element="div" class="row">
                    <cms:component component="${feature}" class="col-md-3" element="div"/>
                </cms:pageSlot>
            </div>
        </c:if>
    </div>
</template:page>

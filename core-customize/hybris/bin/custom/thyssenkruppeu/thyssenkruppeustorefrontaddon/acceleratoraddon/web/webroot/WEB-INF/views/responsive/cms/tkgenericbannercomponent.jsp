<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="tk-eu-banner"
    tagdir="/WEB-INF/tags/addons/thyssenkruppeustorefrontaddon/responsive/template/cms"%>
    
<c:choose>
    <c:when test="${renderOption.code == 'HERO_BANNER_MAX'}">
        <div href="" class="m-banner m-banner--hero-banner-max">
            <div class="container">
                <div class="container-column">
                <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="false"></tk-eu-banner:bannerImageComponent>
                <div class="m-banner__content">
                    <h2 class="h2 m-banner__content__title hidden-xs hidden-sm"><a href="${urlLink}" target="${targetUrl}">${title}</a></h2>                                        
                    <h3 class="h3 m-banner__content__title visible-sm"><a href="${urlLink}" target="${targetUrl}">${title}</a></h3>
                    <h4 class="h4 m-banner__content__title visible-xs"><a href="${urlLink}" target="${targetUrl}">${title}</a></h4>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs"><a href="${urlLink}" target="${targetUrl}">${subtitle}</a></h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs"><a href="${urlLink}" target="${targetUrl}">${subtitle}</a></h6>                   
                </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:when test="${renderOption.code == 'HERO_BANNER_NORMAL'}">
        <div href="" class="m-banner m-banner--hero-banner-normal">
            <div class="container">
                <div class="container-column">
                <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="false"></tk-eu-banner:bannerImageComponent>
                <div class="m-banner__content">
                    <h2 class="h2 m-banner__content__title hidden-xs hidden-sm"><a href="${urlLink}" target="${targetUrl}">${title}</a></h2>                                        
                    <h3 class="h3 m-banner__content__title visible-sm"><a href="${urlLink}" target="${targetUrl}">${title}</a></h3>
                    <h4 class="h4 m-banner__content__title visible-xs"><a href="${urlLink}" target="${targetUrl}">${title}</a></h4>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs"><a href="${urlLink}" target="${targetUrl}">${subtitle}</a></h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs"><a href="${urlLink}" target="${targetUrl}">${subtitle}</a></h6>
                </div>
                </div>
            </div>
        </div>
    </c:when>

    <c:when test="${renderOption.code == 'HERO_BANNER_WITH_CONTENT_MAX'}">
        <div href="" class="m-banner m-banner--hero-banner-content-max">
            <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="false"></tk-eu-banner:bannerImageComponent>
            <div class="container-column">    
            <div class="container">
                
                <div class="m-banner__content col-lg-6 col-sm-10 col-sm-offset-1 col-lg-offset-0">
                    <c:choose>
                    <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
                    <h1 class="h1 m-banner__content__title hidden-xs hidden-sm"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h1>                                        
                    <h2 class="h2 m-banner__content__title visible-sm"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h2>
                    <h3 class="h3 m-banner__content__title visible-xs"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h3>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></h6>
                    </c:when>
                    <c:otherwise>
                    <h1 class="h1 m-banner__content__title hidden-xs hidden-sm">${title}</h1>                                        
                    <h2 class="h2 m-banner__content__title visible-sm">${title}</h2>
                    <h3 class="h3 m-banner__content__title visible-xs">${title}</h3>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs">${subtitle}</h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs">${subtitle}</h6>
                    </c:otherwise>
                    </c:choose>                    
                     
                    <p class="m-banner__content__para">${content}</p>                
                    <div class="m-banner-btn"><tk-eu-banner:bannerURLLinkComponent urlLink="${urlLink}"
                        urlLinkRenderingOption="${urlLinkRenderingOption}"
                        urlLinkTarget="${urlLinkTarget}"></tk-eu-banner:bannerURLLinkComponent></div>
                </div>
                </div>
            </div>
        </div>
    </c:when>

    <c:when
        test="${renderOption.code == 'HERO_BANNER_WITH_CONTENT_NORMAL'}">
        <div href="" class="m-banner m-banner--hero-banner-content-normal">
            <div class="container">
                <div class="container-column">
                <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="false"></tk-eu-banner:bannerImageComponent>
                <div class="m-banner__content col-lg-6 col-sm-10 col-sm-offset-1 col-lg-offset-0">
                    <c:choose>
                    <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
                    <h1 class="h1 m-banner__content__title hidden-xs hidden-sm"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h1>                                        
                    <h2 class="h2 m-banner__content__title visible-sm"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h2>
                    <h3 class="h3 m-banner__content__title visible-xs"><a href="${urlLink}" target="${urlLinkTarget}">${title}</a></h3>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></h6>
                    </c:when>
                    <c:otherwise>
                    <h1 class="h1 m-banner__content__title hidden-xs hidden-sm">${title}</h1>                                        
                    <h2 class="h2 m-banner__content__title visible-sm">${title}</h2>
                    <h3 class="h3 m-banner__content__title visible-xs">${title}</h3>
                    <h5 class="h5 m-banner__content__sub-title hidden-xs">${subtitle}</h5>
                    <h6 class="h6 m-banner__content__sub-title visible-xs">${subtitle}</h6>
                    </c:otherwise>
                    </c:choose> 
                    <p class="m-banner__content__para">${content}</p>                
                    <div class="m-banner-btn"><tk-eu-banner:bannerURLLinkComponent urlLink="${urlLink}"
                        urlLinkRenderingOption="${urlLinkRenderingOption}"
                        urlLinkTarget="${urlLinkTarget}"></tk-eu-banner:bannerURLLinkComponent></div>
                </div>
                </div>
            </div>
        </div>
    </c:when>

    <c:when test="${renderOption.code == 'MODULE_TEASER_TOP'}">
        <div href="" class="m-teaser m-teaser--module-teaser-top">
            <div class="container">
                <div class="container-column">
                <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="true"></tk-eu-banner:bannerImageComponent>
                
                <div class="m-teaser__content">
                    <c:choose>
                     <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
                    <h4 class="h4 m-teaser__content__title">
                    <a href="${urlLink}" target="${urlLinkTarget}">
                    ${title}</a>
                    </h4>
                    <p class="m-teaser__content__sub-title"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></p>
                    </c:when>
                    <c:otherwise>
                    <h4 class="h4 m-teaser__content__title">${title}</h4>
                    <p class="m-teaser__content__sub-title">${subtitle}</p>
                    </c:otherwise>
                    </c:choose>
                    
                    <p class="m-teaser__content__para">${content}</p>
                    <tk-eu-banner:bannerURLLinkComponent urlLink="${urlLink}"
                        urlLinkRenderingOption="${urlLinkRenderingOption}"
                        urlLinkTarget="${urlLinkTarget}"></tk-eu-banner:bannerURLLinkComponent>

                </div>
                </div>
            </div>
        </div>
    </c:when>

    <c:when test="${renderOption.code == 'MODULE_TEASER_LEFT'}">
        <div href="" class="m-teaser m-teaser--module-teaser-left">
            <div class="container">
                <div class="container-column"><tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="true"></tk-eu-banner:bannerImageComponent>
                    <div class="m-teaser__content">
                    <c:choose>
                     <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
                    <h4 class="h4 m-teaser__content__title">
                    <a href="${urlLink}" target="${urlLinkTarget}">
                    ${title}</a>
                    </h4>
                    <p class="m-teaser__content__sub-title"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></p>
                    </c:when>
                    <c:otherwise>
                    <h4 class="h4 m-teaser__content__title">${title}</h4>
                    <p class="m-teaser__content__sub-title">${subtitle}</p>
                    </c:otherwise>
                    </c:choose>
                    
                    <p class="m-teaser__content__para">${content}</p>
                    <tk-eu-banner:bannerURLLinkComponent urlLink="${urlLink}"
                        urlLinkRenderingOption="${urlLinkRenderingOption}"
                        urlLinkTarget="${urlLinkTarget}"></tk-eu-banner:bannerURLLinkComponent>
                </div>
                </div>
            </div>
        </div>
    </c:when>

    <c:when test="${renderOption.code == 'MODULE_TEASER_RIGHT'}">
        <div href="" class="m-teaser m-teaser--module-teaser-right">
            <div class="container">
                <div class="container-column">
                <tk-eu-banner:bannerImageComponent medias="${medias}"
                    mediaRenderingOption="${mediaRenderingOption}" isModuleTeaser="true"></tk-eu-banner:bannerImageComponent>
                <div class="m-teaser__content">
                   <c:choose>
                     <c:when test="${urlLinkRenderingOption.code == 'NO_LINK'}">
                    <h4 class="h4 m-teaser__content__title">
                    <a href="${urlLink}" target="${urlLinkTarget}">
                    ${title}</a>
                    </h4>
                    <p class="m-teaser__content__sub-title"><a href="${urlLink}" target="${urlLinkTarget}">${subtitle}</a></p>
                    </c:when>
                    <c:otherwise>
                    <h4 class="h4 m-teaser__content__title">${title}</h4>
                    <p class="m-teaser__content__sub-title">${subtitle}</p>
                    </c:otherwise>
                    </c:choose>
                    
                    <p class="m-teaser__content__para">${content}</p>
                    <tk-eu-banner:bannerURLLinkComponent urlLink="${urlLink}"
                        urlLinkRenderingOption="${urlLinkRenderingOption}"
                        urlLinkTarget="${urlLinkTarget}"></tk-eu-banner:bannerURLLinkComponent>

                </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
    </c:otherwise>
</c:choose>

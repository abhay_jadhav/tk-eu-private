package com.thyssenkrupp.b2b.eu.storefrontaddon.controllers.form;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TkShippingAddressForm extends AddressForm {

    private String comment;

    private String selectedAddressCode;

    private String companyName;

    private String departmentName;

    private String selectBoxAddressCode;

    private boolean addressStatus;

    private boolean makePrimaryCheckBox;

    private boolean saveCustomerAddressCheckBox;

    private String newShippingAddressId;

    public String getNewShippingAddressId() {
        return newShippingAddressId;
    }

    public void setNewShippingAddressId(String newShippingAddressId) {
        this.newShippingAddressId = newShippingAddressId;
    }

    public boolean isSaveCustomerAddressCheckBox() {
        return saveCustomerAddressCheckBox;
    }

    public void setSaveCustomerAddressCheckBox(boolean saveCustomerAddressCheckBox) {
        this.saveCustomerAddressCheckBox = saveCustomerAddressCheckBox;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSelectedAddressCode() {
        return selectedAddressCode;
    }

    public void setSelectedAddressCode(String selectedAddressCode) {
        this.selectedAddressCode = selectedAddressCode;
    }

    @NotNull(message = "{address.companyName.invalid}")
    @Size(min = 1, max = 255, message = "{address.companyName.invalid}")
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @NotNull(message = "{address.DepartmentName.invalid}")
    @Size(min = 1, max = 255, message = "{address.DepartmentName.invalid}")
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public boolean isAddressStatus() {
        return addressStatus;
    }

    public void setAddressStatus(boolean addressStatus) {
        this.addressStatus = addressStatus;
    }

    public String getSelectBoxAddressCode() {
        return selectBoxAddressCode;
    }

    public void setSelectBoxAddressCode(String selectBoxAddressCode) {
        this.selectBoxAddressCode = selectBoxAddressCode;
    }

    public boolean isMakePrimaryCheckBox() {
        return makePrimaryCheckBox;
    }

    public void setMakePrimaryCheckBox(boolean makePrimaryCheckBox) {
        this.makePrimaryCheckBox = makePrimaryCheckBox;
    }


}

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row">
    <div class="col-md-4">
        <div class="m-page-section m-page-section--no-border-bottom">
            <h4 class="m-page-section__title" data-qa-id="profile-login-info-label"><spring:theme code="myaccount.profile.logininfo.header"/></h4>
            <div class="m-page-section__content">
                <div>
                    <p class="m-item-group__label" data-qa-id="profile-login-label"><spring:theme code="myaccount.profile.login.name"/></p>
                    <p class="m-item-group__value" data-qa-id="profile-login-username">
                        ${customerData.email}
                    </p>
                    <c:url var="profileLink" value="${currentUrl}">
                        <c:param name="enableUpdatePasswordForm" value="${true}"/>
                    </c:url>
                </div>
                <div class="m-item-group">
                    <c:choose>
                        <c:when test="${enableUpdatePasswordForm}">
                            <div class="m-item-group__value"><jsp:include page="accountChangePasswordPage.jsp"/></div>
                        </c:when>
                        <c:otherwise>
                             <p class="m-item-group__value"><a  data-qa-id="profile-change-password-link" href="${profileLink}"><spring:theme code="myaccount.profile.changepassword.label"/></a></p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <div class="m-page-section m-page-section--no-border-bottom">
                    <h4 class="m-page-section__title" data-qa-id="profile-personal-info-label"><spring:theme code="myaccount.profile.personalinfo.header"/></h4>
                    <div class="m-page-section__content">
                        <div class="m-item-group">
                            <p class="m-item-group__label" data-qa-id="profile-personal-info-name-label"><spring:theme code="myaccount.profile.user.name"/></p>
                            <p class="m-item-group__value" data-qa-id="profile-personal-info-name-value">${customerData.firstName}&nbsp;${customerData.lastName}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <c:if test="${enableUpdatePasswordForm}">
                    <div class="hidden js-validation-message">
                        <div class="password-rules js-password-messages display-none" data-js-password-instruction="password">
                            <ul class="password-rules__list">
                                <li class="password-rules__list__item has_min_length"> <spring:theme code="SetPassword.NewPassword.Criteria.MinCharsText"/> </li>
                                <li class="password-rules__list__item password-rules__list__item--text"> <spring:theme code="SetPassword.NewPassword.Criteria.Text"/> </li>
                                <li class="password-rules__list__item has_uppercase"> <spring:theme code="SetPassword.NewPassword.Criteria.UpperCaseText"/> </li>
                                <li class="password-rules__list__item has_lowercase"> <spring:theme code="SetPassword.NewPassword.Criteria.SmallCaseText"/> </li>
                                <li class="password-rules__list__item has_number"> <spring:theme code="SetPassword.NewPassword.Criteria.NumberText"/> </li>
                                <li class="password-rules__list__item has_special_char"> <spring:theme code="SetPassword.NewPassword.Criteria.SpecialCharsText"/> </li>
                            </ul>
                        </div>

                        <div class="password-rules js-confirm-password-messages display-none" data-js-password-instruction="confirm-password">
                            <ul class="password-rules__list">
                                <li class="password-rules__list__item has_same_password"> <spring:theme code="SetPassword.ConfirmPassword.Criteria"/> </li>
                            </ul>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="m-page-section m-page-section--no-border-bottom">
            <h4 class="m-page-section__title" data-qa-id="profile-contact-info-label"><spring:theme code="myaccount.profile.contactinfo.header"/></h4>
            <div class="m-page-section__content">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="m-item-group">
                            <p class="m-item-group__label" data-qa-id="profile-contact-info-email-label"><spring:theme code="myaccount.profile.email.name"/></p>
                            <p class="m-item-group__value" data-qa-id="profile-contact-info-email-value">${customerData.email}</p>
                        </div>
                    </div>
                    <c:if test="${not empty phoneNumbers and phoneNumbers ne '[null]'}">
                        <div class="col-sm-4">
                            <div class="m-item-group">
                                <p class="m-item-group__label" data-qa-id="myaccount-profile-phone-number-label"><spring:theme code="myaccount.profile.phonenumber.name"/></p>
                                <p class="m-item-group__value" data-qa-id="profile-phone-number-value">
                                    <c:forEach items="${phoneNumbers}" var="phoneNumber" varStatus="loop">
                                        ${phoneNumber}<c:if test="${not loop.last && phoneNumber ne null}"><br/></c:if>
                                    </c:forEach>
                                </p>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${not empty faxNumbers and faxNumbers ne '[null]'}">
                        <div class="col-sm-4">
                            <div class="m-item-group">
                                <p class="m-item-group__label" data-qa-id="profile-fax-number-label"><spring:theme code="myaccount.profile.faxnumber.name"/></p>
                                <p class="m-item-group__value" data-qa-id="profile-fax-number-value">
                                    <c:forEach items="${faxNumbers}" var="faxNumber" varStatus="loop">
                                        ${faxNumber}<c:if test="${not loop.last && phoneNumber ne null}"><br/></c:if>
                                    </c:forEach>
                                </p>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

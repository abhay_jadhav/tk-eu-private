package com.thyssenkrupp.b2b.eu.core.constants;

/**
 * Global class for all Thyssenkruppeucore constants. You can add global constants for your extension into this class.
 */
public final class ThyssenkruppeucoreConstants extends GeneratedThyssenkruppeucoreConstants {
    public static final String EXTENSIONNAME      = "thyssenkruppeucore";
    public static final String PLATFORM_LOGO_CODE = "thyssenkruppeucorePlatformLogo";

    public static final String SAP_MTR_UNIT_CODE = "MTR";

    public static final String SAP_MTK_UNIT_CODE = "MTK";

    public static final String SAP_PIECE_TYPE_CODE = "PCE";

    public static final String TONNES_SAP_UNIT_CODE = "TNE";

    public static final String SAP_KGM_UNIT_CODE = "KGM";

    public static final String SAP_YKH_UNIT_CODE = "YKH";

    public static final String SAP_WEIGHTED_TYPE_CODE = "SAP-MASS";

    public static final String SAP_MMT_UNIT_TYPE_CODE = "MMT";

    public static final String SAP_ATP_COMMAND_KEY = "callAtpCheck";

    public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
    public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
    public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
    public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
    public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
    public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
    public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
    public static final String VARIANT_CATEGORY_CODE_PREFIX = "VC_";
    public static final String VARIANT_VALUE_CATEGORY_CODE_PREFIX = "VVC_";
    public static final String STRING_ARRAY_BRACKETS_REGEX_PATTERN = ", |\\[|\\]";
    public static final String REGEX_COMMA = ",";
    public static final char CHARACTER_UNDERSCORE = '_';
    public static final char CHARACTER_HYPHEN = '-';
    public static final char LIKE_CHARACTER = '%';
    public static final String C2L_RANGE_CATEGORY_PREFIX = "C2L_RANGE_";
    public static final String C2L_CONFIGURATION_CATEGORY = "CUTTOLENGTH";
    public static final int C2L_RANGE_MIN_VALUE_IN_MTR = 1;
    public static final String TK_EU_CATEGORY_CATALOG_ID = "tkSchulteCategoryCatalog";
    public static final String TK_EU_PRODUCT_CATALOG_ID = "tkSchulteProductCatalog";
    public static final String PRODUCT_REPLICATION_RULE_FALLBACK_KEY = "FALLBACK";
    public static final String TRADE_LENGTH_CATEGORY_CODE = "Trade_Length";
    public static final String LENGTH_VARIANT_CATEGORY_CODE = "XX_LENGTH";

    private ThyssenkruppeucoreConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}


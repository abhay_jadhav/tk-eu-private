package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;


import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryPathValueProvider;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class TkEuCategoryPathValueProvider extends CategoryPathValueProvider {

    private TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService;

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {

        Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model);
        return CollectionUtils.isNotEmpty(categories) ? processCategoryPath(categories, indexedProperty) : Collections.emptyList();

    }

    private Collection<FieldValue> processCategoryPath(Collection<CategoryModel> categories, IndexedProperty indexedProperty) {
        final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();
        final Set<String> categoryPaths = getCategoryPaths(filterVariantCategory(categories));
        fieldValues.addAll(createFieldValue(categoryPaths, indexedProperty));

        return fieldValues;
    }

    private Collection<CategoryModel> filterVariantCategory(Collection<CategoryModel> categories) {
        return tkStorefrontCategoryProviderService.filterStorefrontCategories(categories);
    }

    public TkEuStorefrontCategoryProviderService getTkStorefrontCategoryProviderService() {
        return tkStorefrontCategoryProviderService;
    }

    public void setTkStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService) {
        this.tkStorefrontCategoryProviderService = tkStorefrontCategoryProviderService;
    }
}

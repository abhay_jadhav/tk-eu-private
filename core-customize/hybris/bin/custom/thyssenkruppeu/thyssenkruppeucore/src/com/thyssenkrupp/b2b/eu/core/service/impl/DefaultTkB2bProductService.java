
package com.thyssenkrupp.b2b.eu.core.service.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

import de.hybris.platform.b2bacceleratorservices.product.impl.DefaultB2BProductService;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.Lists;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bProductService;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;

public class DefaultTkB2bProductService extends DefaultB2BProductService implements TkB2bProductService {

    private static final Logger LOG = Logger.getLogger(DefaultTkB2bProductService.class);
    private static final String CLASSIFICATION_CATEGORY_CATALOG_VERSION_LIST = "classificationclass.category.catalog.version.ignored";
    private static final String DEFAULT_VALUE = "tkSExtendedClassification";
    private ModelService modelService;
    private I18NService i18nService;

    public List<Map<String, String>> fetchProductInfo(final ProductModel product) {
        if (product == null || StringUtils.isEmpty(product.getTkProductInfo()) || product.getTkProductInfo().indexOf("[") == -1) {
            return null;
        }

        final Locale currentLocale = i18nService.getCurrentLocale();
        String productInfo = product.getTkProductInfo(currentLocale);
        productInfo = productInfo.substring(productInfo.indexOf("[") + 1);
        productInfo = productInfo.substring(0, productInfo.indexOf("]")).replaceAll("\"", "");
        final List<Map<String, String>> productInfoList = new ArrayList<Map<String, String>>();

        while (true) {
            final int starting = productInfo.indexOf("{");
            final int finishing = productInfo.indexOf("}");
            if (starting == -1 || finishing == -1) {
                break;
            }

            final String line = productInfo.substring(starting + 1, finishing);
            if (StringUtils.isNotEmpty(line)) {
                final Map<String, String> elementData = loadProductInfoElement(line);
                productInfoList.add(elementData);
            }

            productInfo = productInfo.substring(finishing + 1);
        }

        return productInfoList;
    }

    private Map<String, String> loadProductInfoElement(final String line) {
        final Map<String, String> result = new HashMap<String, String>();
        final String[] elements = line.split(",(?=\\w+:)");
        for (final String element : elements) {
            final String[] values = element.split(":");
            result.put(values[0], values[1]);
        }
        return result;
    }

    @Override
    public CategoryModel getClassificationCategoryForProduct(final ProductModel product) {
        final Collection<CategoryModel> supercategories = product.getSupercategories();
        if(supercategories != null) {
            final List<String> classficationCatVersionList = Arrays.asList(TkEuConfigKeyValueUtils.getString(CLASSIFICATION_CATEGORY_CATALOG_VERSION_LIST, DEFAULT_VALUE).split(","));
            for (final CategoryModel category : supercategories) {
                if (category instanceof ClassificationClassModel && null != category.getCatalogVersion() && null != category.getCatalogVersion().getCatalog() && isValidClassificationClassCatalogVersion(category.getCatalogVersion().getCatalog().getId(), classficationCatVersionList)) {
                    return category;

                }
            }
        }
        return null;
    }

    private boolean isValidClassificationClassCatalogVersion(final String key, final List<String> classficationCatVersionList) {
        boolean isValid = true;
        try {
            if (CollectionUtils.isNotEmpty(classficationCatVersionList)) {
                for (final String classCat : classficationCatVersionList) {
                    if (StringUtils.containsIgnoreCase(key, classCat)) {
                        isValid = false;
                        break;
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("Exception in Checking Classification Catalog version !!", e);
        }
        return isValid;
    }

    @Override
    public boolean isClassificationCategoryEmpty(final ProductModel product) {
        final CategoryModel classificationClassCategory = getClassificationCategoryForProduct(product);
        if (classificationClassCategory == null) {
            LOG.warn("Classification Class is empty for " + product.getCode() + ". Skipping entry");
            return true;
        }
        return false;
    }

    @Override
    public String getFeatureValue(final FeatureValue value) {
        if (value != null) {
            final Object featureValue = value.getValue();
            if (featureValue instanceof ClassificationAttributeValueModel) {
                final ClassificationAttributeValueModel attributeValueModel = (ClassificationAttributeValueModel) featureValue;
                LOG.debug(" Feature Value from ClassificationAttributeValueModel: " + attributeValueModel.getName());
                return attributeValueModel.getName();
            } else {
                LOG.debug(" Feature Value " + featureValue.toString());
                return featureValue.toString();
            }
        }
        LOG.warn(" Empty Feature Value found. VariantValueCategory will not be created");
        return StringUtils.EMPTY;
    }

    @Override
    public void updateVariantProduct(final VariantProductModel variant, final List<VariantValueCategoryModel> variantSuperCategories) {
        final List<CategoryModel> existingCategories = (List) variant.getSupercategories();
        List<CategoryModel> updatedCategories = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(variantSuperCategories)) {
            for (final CategoryModel category : variantSuperCategories) {
                updatedCategories.add(category);
            }
            cleanUpOldVariantValueCategories(updatedCategories, existingCategories);
        } else {
            updatedCategories = existingCategories;
        }
        variant.setSupercategories(Lists.newArrayList(updatedCategories));
        variant.setCategoryCreationStatus(Boolean.TRUE);
        modelService.save(variant);
        LOG.debug("Finished setting superCategories to Variant:" + variant.getCode());
    }

    private void cleanUpOldVariantValueCategories(final List<CategoryModel> updatedCategories, final List<CategoryModel> existingCategories) {
        for (final CategoryModel category : existingCategories) {
            if (category instanceof VariantValueCategoryModel) {
                // Do not add old existing VVC
            } else {
                updatedCategories.add(category);
            }
        }
    }

    @Override
    public void updateVariantProductConfigurationCategory(final VariantProductModel variant, final ConfigurationCategoryModel configurationCategory) {
        final List<CategoryModel> existingCategories = (List) variant.getSupercategories();
        List<CategoryModel> updatedCategories = new ArrayList<>();
        if (configurationCategory != null) {

            updatedCategories.add(configurationCategory);

            cleanUpOldVariantValueCategories(updatedCategories, existingCategories);
        } else {
            updatedCategories = existingCategories;
        }
        variant.setSupercategories(Lists.newArrayList(updatedCategories));
        variant.setCategoryCreationStatus(Boolean.TRUE);
        modelService.save(variant);
        LOG.info(variant.getCode() + " is updated with super Categories");
    }

    @Override
    public boolean doesCategoryAlreadyExist(final String code, final List<CategoryModel> categories) {
        if (CollectionUtils.isNotEmpty(categories)) {
            for (final CategoryModel category : categories) {
                if (StringUtils.equals(code, category.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isBaseProduct(final ProductModel product) {
        return (ProductModel.class.isInstance(product) && !VariantProductModel.class.isInstance(product));
    }

    @Override
    public void setCategoryCreationStatusToTrue(final ProductModel product) {
        product.setCategoryCreationStatus(Boolean.TRUE);
        modelService.save(product);
    }

    @Override
    public void replaceExistingVariantCategoriesForProduct(final ProductModel product, final List<VariantCategoryModel> variantCategories) {
        final Collection<CategoryModel> allSuperCategories = product.getSupercategories();
        if (CollectionUtils.isEmpty(allSuperCategories)) {
            product.setSupercategories(Lists.newArrayList(variantCategories));
        } else {
            final List<CategoryModel> newSuperCategories = new ArrayList<>(allSuperCategories);
            newSuperCategories.removeAll(getAllVariantCategoriesForProduct(product));
            newSuperCategories.addAll(variantCategories);
            product.setSupercategories(newSuperCategories);
        }
        modelService.save(product);
    }

    @Override
    public List<VariantCategoryModel> getAllVariantCategoriesForProduct(final ProductModel product) {
        return emptyIfNull(product.getSupercategories()).stream().filter(VariantCategoryModel.class::isInstance).map(VariantCategoryModel.class::cast).collect(Collectors.toList());
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }

}

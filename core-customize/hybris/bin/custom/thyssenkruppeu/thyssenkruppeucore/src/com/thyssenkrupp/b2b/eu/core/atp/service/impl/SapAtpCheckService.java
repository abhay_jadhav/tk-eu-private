package com.thyssenkrupp.b2b.eu.core.atp.service.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.SapAtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.response.SapAtpResponse;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpCheckService;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.atp.service.strategies.SapAtpCheckStrategy;
import com.thyssenkrupp.b2b.eu.core.atp.service.strategies.SapAtpRequestStrategy;
import com.thyssenkrupp.b2b.eu.core.atp.service.strategies.SapAtpRequestValidationStrategy;
import com.thyssenkrupp.b2b.eu.core.atp.service.strategies.SapAtpResponseStrategy;
import com.thyssenkrupp.b2b.eu.core.cache.impl.DefaultTkEuSapAtpCartCacheService;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;

public class SapAtpCheckService implements AtpCheckService {
    private static final Logger                            LOG = Logger.getLogger(SapAtpCheckService.class);
    private static final String ATP_SAP_WNAE_ENABLED = "atp.sap.type.wnae.enabled";
    private static final String CERTIFICATE = "CERTIFICATE";
    private static final String TKTRADELENGTH = "TKTRADELENGTH";
    private static final String TKCCLENGTH = "TKCCLENGTH";
    private static final String CUTTOLENGTH_PRODINFO = "CUTTOLENGTH_PRODINFO";
    private boolean                           isMockEnabled;
    private SapAtpRequestStrategy             sapAtpRequestStrategy;
    private SapAtpRequestValidationStrategy   sapAtpRequestValidationStrategy;
    private SapAtpCheckStrategy               sapAtpCheckStrategy;
    private SapAtpResponseStrategy            sapAtpResponseStrategy;
    private DefaultB2BCommerceUnitService     defaultB2BCommerceUnitService;
    private UserService                       userService;
    private CartService                       cartService;
    private DefaultTkEuSapAtpCartCacheService sapAtpCacheService;

    @Override
    public List<AtpResponseData> doAtpCheck(final AtpRequestData request, final BaseStoreModel baseStore) {
        return generateAtpResponses(request);
    }

    private List<AtpResponseData> generateAtpResponses(final AtpRequestData request) {
        if (isMockEnabled()) {
            LOG.debug("Mock enabled, generating mock response data");
            return generateMockResponses(request);
        }
        return generateSapRequestAndTriggerSapAtpCheck(request);
    }

    private List<AtpResponseData> generateSapRequestAndTriggerSapAtpCheck(final AtpRequestData request) {
        try {
            final SapAtpRequest sapAtpRequest = getSapAtpRequestStrategy().buildSapAtpRequest(request);
            final boolean isValidSapRequest = getSapAtpRequestValidationStrategy().validateSapAtpRequest(sapAtpRequest);
            if (isValidSapRequest) {
                final CacheKey cacheKey = getCacheKey(request, sapAtpRequest);
                final SapAtpResponse sapAtpResponse = getSapAtpCheckStrategy().callAtpCheck(sapAtpRequest,request,cacheKey);
                return getSapAtpResponseStrategy().buildSapAtpResponse(sapAtpResponse);
            }
        } catch (final Exception e) {
            LOG.error("Error while generating sap request and triggering sap call", e);
        }
        return Collections.emptyList();
    }

    private CacheKey getCacheKey(final AtpRequestData request, final SapAtpRequest sapAtpRequest) {
        final AtpRequestEntryData requestEntryData = request.getEntries().get(0);
        final B2BUnitModel b2bunit = getParentUnit();
        final boolean newRFCEnableFlag = TkEuConfigKeyValueUtils.getBoolean(ATP_SAP_WNAE_ENABLED, false);
        CacheKey cacheKey = null;
        String key = null;
        String entryDetails = null;

        final AtpRequest atpRequest = sapAtpRequest.getAtpRequest();
        entryDetails = requestEntryData.getProductCode() + requestEntryData.getPosition().toString() + requestEntryData.getQuantity() + requestEntryData.getSalesUnit() + atpRequest.getShipToPostalCode();
        if (newRFCEnableFlag) {
            entryDetails = entryDetails + getConfigurationKey(requestEntryData.getConfigurationInfos());
            }
        key = (b2bunit != null ? b2bunit.getUid() : "") + atpRequest.getShipToPostalCode().concat(entryDetails);
        cacheKey = sapAtpCacheService.getKey(key, getParentUnit().getUid());
        LOG.info(request.getIsPdpRequest().booleanValue() ? "PDPCacheKey : " : "CartCacheKey : " + cacheKey);

        return cacheKey;
    }

    protected B2BUnitModel getParentUnit() {
        final UserModel currentUser = userService.getCurrentUser();
        if (currentUser instanceof B2BCustomerModel) {
            final B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) currentUser;
            final B2BUnitModel defaultB2BUnit = b2BCustomerModel.getDefaultB2BUnit();
            if (defaultB2BUnit != null) {
                return defaultB2BUnit;
            }
        }
        return defaultB2BCommerceUnitService.getRootUnit();
    }

    private List<AtpResponseData> generateMockResponses(final AtpRequestData request) {
        return emptyIfNull(request.getEntries()).stream().map(mockResponseDataFunction()).collect(Collectors.toList());
    }

    private Function<AtpRequestEntryData, AtpResponseData> mockResponseDataFunction() {
        final Date mockDate = DateUtils.addDays(new Date(), 3);
        return entryData -> AtpResponseDataBuilder.builder().withPosition(entryData.getPosition().intValue()).withProductCode(entryData.getProductCode()).withSapResult(mockDate).build();
    }

    public boolean isMockEnabled() {
        return isMockEnabled;
    }

    @Required
    public void setMockEnabled(final boolean mockEnabled) {
        isMockEnabled = mockEnabled;
    }

    public SapAtpRequestStrategy getSapAtpRequestStrategy() {
        return sapAtpRequestStrategy;
    }

    @Required
    public void setSapAtpRequestStrategy(final SapAtpRequestStrategy sapAtpRequestStrategy) {
        this.sapAtpRequestStrategy = sapAtpRequestStrategy;
    }

    public SapAtpResponseStrategy getSapAtpResponseStrategy() {
        return sapAtpResponseStrategy;
    }

    @Required
    public void setSapAtpResponseStrategy(final SapAtpResponseStrategy sapAtpResponseStrategy) {
        this.sapAtpResponseStrategy = sapAtpResponseStrategy;
    }

    public SapAtpCheckStrategy getSapAtpCheckStrategy() {
        return sapAtpCheckStrategy;
    }

    @Required
    public void setSapAtpCheckStrategy(final SapAtpCheckStrategy sapAtpCheckStrategy) {
        this.sapAtpCheckStrategy = sapAtpCheckStrategy;
    }

    public SapAtpRequestValidationStrategy getSapAtpRequestValidationStrategy() {
        return sapAtpRequestValidationStrategy;
    }

    @Required
    public void setSapAtpRequestValidationStrategy(final SapAtpRequestValidationStrategy sapAtpRequestValidationStrategy) {
        this.sapAtpRequestValidationStrategy = sapAtpRequestValidationStrategy;
    }

    public void setDefaultB2BCommerceUnitService(final DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public CartService getCartService() {
        return cartService;
    }

    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }


    private String getConfigurationKey(final List<ConfigurationInfoData> configurationInfos) {
        try {
            String configurationKey = "";
            if (CollectionUtils.isNotEmpty(configurationInfos)) {
                for (final ConfigurationInfoData configData : configurationInfos) {
                    final String configureType = configData.getConfiguratorType().getCode();
                    if (isValidConfigureType(configureType, configData)) {
                        switch (configureType) {
                            case CERTIFICATE :
                                configurationKey = configurationKey.concat(configData.getUniqueId());
                                break;
                            case TKTRADELENGTH :
                                configurationKey = configurationKey.concat(configData.getUniqueId());
                                break;
                            case TKCCLENGTH :
                                configurationKey = configurationKey.concat(configData.getUniqueId());
                                break;
                            case CUTTOLENGTH_PRODINFO :
                                configurationKey = configurationKey.concat(configData.getUniqueId());
                                break;
                            default:
                                break;
                        }
                    }

                }
                return configurationKey;
            }

        } catch (final Exception e) {
            return "";
        }

        return "";
    }

    private boolean isValidConfigureType(final String configureType, final ConfigurationInfoData configData) {
        return ("1".equalsIgnoreCase(configData.getConfigurationValue()) || "true".equalsIgnoreCase(configData.getConfigurationValue()) || ConfiguratorType.TKCCLENGTH.getCode().equals(configureType));
    }

    public DefaultTkEuSapAtpCartCacheService getSapAtpCacheService() {
        return sapAtpCacheService;
    }

    public void setSapAtpCacheService(final DefaultTkEuSapAtpCartCacheService sapAtpCacheService) {
        this.sapAtpCacheService = sapAtpCacheService;
    }
}

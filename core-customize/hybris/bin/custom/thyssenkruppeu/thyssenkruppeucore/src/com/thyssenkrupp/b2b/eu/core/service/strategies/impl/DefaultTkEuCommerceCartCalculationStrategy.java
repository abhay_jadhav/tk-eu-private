package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import static de.hybris.platform.promotions.jalo.PromotionsManager.AutoApplyMode.APPLY_ALL;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import com.thyssenkrupp.b2b.eu.core.exception.TkEuInvalidCartException;
import com.thyssenkrupp.b2b.eu.core.service.strategies.TkEuCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;

import de.hybris.platform.promotions.jalo.PromotionsManager;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultTkEuCommerceCartCalculationStrategy extends DefaultCommerceCartCalculationStrategy implements TkEuCommerceCartCalculationStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCommerceCartCalculationStrategy.class);

    @Override
    public boolean calculateCart(final CommerceCartParameter parameter) {
        final CartModel cartModel = parameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        final CalculationService calculationService = getCalculationService();
        boolean recalculated = false;
        if (calculationService.requiresCalculation(cartModel)) {
            try {
                parameter.setRecalculate(false);
                LOG.debug("calling beforeCalculate hooks");
                beforeCalculate(parameter);
                if (BooleanUtils.isFalse(cartModel instanceof InMemoryCartModel)) {
                    // Recalculation of cart is always required since we are applying accumulation to cart excluding PDP.
                    calculationService.recalculate(cartModel);
                } else {
                    calculationService.calculate(cartModel);
                }
                if (!parameter.isSkipPromotionCalculation()) {
                    getPromotionsService().updatePromotions(getPromotionGroups(), cartModel, true, APPLY_ALL, APPLY_ALL, getTimeService().getCurrentTime());
                }
            } catch (final CalculationException calculationException) {
                throw new TkEuInvalidCartException(String.format("Cart model %s was not calculated due to: %s", cartModel.getCode(), calculationException.getMessage()), calculationException);
            } finally {
                LOG.debug("Calling afterCalculate hooks");
                afterCalculate(parameter);
            }
            recalculated = true;
        }
        if (isCalculateExternalTaxes()) {
            getExternalTaxesService().calculateExternalTaxes(cartModel);
        }
        return recalculated;
    }

    @Override
    public boolean recalculateCart(final CommerceCartParameter parameter) {

        final CartModel cartModel = parameter.getCart();

        if(parameter.isSkipCartCalculation()) {
            return true;
        }
        try {
            parameter.setRecalculate(true);
            beforeCalculate(parameter);
            getCalculationService().recalculate(cartModel);
            getPromotionsService().updatePromotions(getPromotionGroups(), cartModel, true, PromotionsManager.AutoApplyMode.APPLY_ALL,
              PromotionsManager.AutoApplyMode.APPLY_ALL, getTimeService().getCurrentTime());
        } catch (final CalculationException calculationException) {
            throw new TkEuInvalidCartException(String.format("Cart model %s was not calculated due to: %s", cartModel.getCode(), calculationException.getMessage()), calculationException);
        } finally {
            afterCalculate(parameter);
        }
        return true;
    }

    @Override
    public boolean calculateCartTotals(CommerceCartParameter parameter, CommerceCartModification commerceCartModification) {
        final CartModel cartModel = parameter.getCart();
        AbstractOrderEntryModel entry = commerceCartModification.getEntry();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        boolean recalculated = false;
        final CalculationService calculationService = getCalculationService();
        try {
            if (entry != null && BooleanUtils.isFalse(entry.getCalculated())) {
                calculationService.recalculate(entry);
            }
            calculationService.calculateTotals(cartModel, true);
            recalculated = true;
        } catch (final CalculationException calculationException) {
            throw new TkEuInvalidCartException(String.format("Cart model %s was not calculated due to: %s", cartModel.getCode(), calculationException.getMessage()), calculationException);
        }
        return recalculated;
    }
}

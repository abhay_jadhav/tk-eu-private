
package com.thyssenkrupp.b2b.eu.core.service.order.daos;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

public interface TkEuB2bOrderDao {
    List<OrderModel> getOrdersToExport(int exportOrderCount, Long graceTime);

    OrderModel findInitialOrderByCode(String orderCode);
}

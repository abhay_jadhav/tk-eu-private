package com.thyssenkrupp.b2b.eu.core.service;


import de.hybris.platform.category.model.CategoryModel;

import java.util.Collection;

public interface TkEuStorefrontCategoryProviderService {

    Collection<CategoryModel> filterStorefrontCategories(Collection<CategoryModel> categoryModels);

    boolean isStorefrontCategory(CategoryModel categoryModel);
}

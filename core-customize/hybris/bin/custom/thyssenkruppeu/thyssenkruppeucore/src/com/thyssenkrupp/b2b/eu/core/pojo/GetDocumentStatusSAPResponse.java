package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class GetDocumentStatusSAPResponse implements Serializable{

    @JsonProperty("Get_Document_Status")
    private List<GetDocumentStatusSAP> getDocumentStatusSAP;

    @JsonProperty("Get_Document_Status")
    public List<GetDocumentStatusSAP> getGetDocumentStatusSAP() {
        return getDocumentStatusSAP;
    }

    @JsonProperty("Get_Document_Status")
    public void setGetDocumentStatusSAP(List<GetDocumentStatusSAP> getDocumentStatusSAP) {
        this.getDocumentStatusSAP = getDocumentStatusSAP;
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.b2b.model.B2BCustomerModel.REGISTRATIONSTATUS;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.dao.PagedB2BCustomerDao;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;
import com.thyssenkrupp.b2b.eu.core.service.user.daos.TkEuUserDao;

public class DefaultTkEuB2bCustomerService extends DefaultB2BCustomerService implements TkEuB2bCustomerService<B2BCustomerModel, B2BUnitModel> {

    private PagedB2BCustomerDao<B2BCustomerModel> pagedB2BCustomerDao;
    private TkEuUserDao                           userDao;

    @Override
    public SearchPageData<B2BCustomerModel> getPagedCustomersByRegistrationStatus(final PageableData pageableData, final B2bRegistrationStatus statusCode) {
        validateParameterNotNullStandardMessage("pageableData", pageableData);
        validateParameterNotNullStandardMessage("statusCode", statusCode);
        final SearchPageData<B2BCustomerModel> b2bCustomers = getPagedB2BCustomerDao()
                .find(Collections.singletonMap(REGISTRATIONSTATUS, statusCode), pageableData);
        return b2bCustomers;
    }

    @Override
    public SearchPageData<B2BCustomerModel> getPagedCustomersByAllRegistrationStatus(final PageableData pageableData) {
        validateParameterNotNullStandardMessage("pageableData", pageableData);
        final SearchPageData<B2BCustomerModel> b2bCustomers = getPagedB2BCustomerDao()
                .find(pageableData);
        return b2bCustomers;
    }

    @Override
    public Optional<B2BUnitModel> getCurrentB2bUnit() {
        final UserModel currentUser = getUserService().getCurrentUser();
        if (currentUser != null && !getUserService().isAnonymousUser(currentUser) && currentUser instanceof B2BCustomerModel) {
            return Optional.ofNullable(((B2BCustomerModel) currentUser).getDefaultB2BUnit());
        }
        return Optional.empty();
    }

    @Override
    public <T extends UserModel> T getAllUserForUID(final String userId, final Class<T> clazz) {
        ServicesUtil.validateParameterNotNull(userId, "The given userID is null!");
        ServicesUtil.validateParameterNotNull(clazz, "The given clazz is null!");
        return (T) validateType(getAllUserForUID(userId, false), clazz);
    }

    private UserModel getAllUserForUID(final String userId, final boolean allStatuses) {
        UserModel user = null;
        if (allStatuses) {
            user = getUserDao().findAllUserAllStatusesByUID(userId);
        } else {
            user = getUserDao().findAllUserByUID(userId);
        }
        if (user == null) {
            throw new UnknownIdentifierException("Cannot find user with uid '" + userId + "'");
        } else {
            return user;
        }
    }

    private <T> T validateType(final T value, final Class type) {
        if (type.isInstance(value)) {
            return value;
        } else {
            throw new ClassMismatchException(type, value.getClass());
        }
    }

    public TkEuUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(final TkEuUserDao userDao) {
        this.userDao = userDao;
    }

    public PagedB2BCustomerDao<B2BCustomerModel> getPagedB2BCustomerDao() {
        return pagedB2BCustomerDao;
    }

    public void setPagedB2BCustomerDao(final PagedB2BCustomerDao<B2BCustomerModel> pagedB2BCustomerDao) {
        this.pagedB2BCustomerDao = pagedB2BCustomerDao;
    }

    @Override
    public <T extends UserModel> T getAllUserAllStatusesForUID(final String userId, final Class<T> clazz) {
        ServicesUtil.validateParameterNotNull(userId, "The given userID is null!");
        ServicesUtil.validateParameterNotNull(clazz, "The given clazz is null!");
        return (T) validateType(getAllUserForUID(userId, true), clazz);
    }
}

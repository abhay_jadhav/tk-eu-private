package com.thyssenkrupp.b2b.eu.core.service.impl;

import static java.lang.String.format;

import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Collection;
import java.util.function.Consumer;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkSynchronizationService;

public class DefaultTkSynchronizationService implements TkSynchronizationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkSynchronizationService.class);
    private SetupSyncJobService     setupSyncJobService;
    private SetupSolrIndexerService setupSolrIndexerService;

    @Override
    public void syncCatalogAndIndexForShops(final Collection<String> shopNames) {
        if (CollectionUtils.isNotEmpty(shopNames)) {
            for (final String shopName : shopNames) {
                synchronizeProductCatalog(shopName);
                synchronizeContentCatalog(shopName);
                runSolrIndex(shopName);
            }
        }
    }

    public void runSolrIndex(final String storeName) {
        getSetupSolrIndexerService().executeSolrIndexerCronJob(format("%sIndex", storeName), true);
        getSetupSolrIndexerService().activateSolrIndexerCronJobs(format("%sIndex", storeName));
    }

    public void synchronizeContentCatalog(final String catalogName) {
        LOG.info("Begin synchronizing Content Catalog {}", catalogName);
        final String syncJobName = format("%sContentCatalog", catalogName);
        final boolean result = synchronizeCatalog(syncJobName, getSetupSyncJobService()::createContentCatalogSyncJob);
        if (!result) {
            LOG.warn("Content Catalog synchronization for {} failed", catalogName);
        } else {
            LOG.info("Done synchronizing Content Catalog {}", catalogName);
        }
    }

    public void synchronizeProductCatalog(final String catalogName) {
        LOG.info("Begin synchronizing Product Catalog {}", catalogName);
        final String syncJobName = format("%sProductCatalog", catalogName);
        final boolean result = synchronizeCatalog(syncJobName, getSetupSyncJobService()::createProductCatalogSyncJob);
        if (!result) {
            LOG.warn("Product Catalog synchronization for {} failed", catalogName);
        } else {
            LOG.info("Done synchronizing Product Catalog {}", catalogName);
        }
    }

    public void synchronizeCategoryCatalog(final String catalogName) {
        LOG.info("Begin synchronizing Category Catalog {}", catalogName);
        final String syncJobName = format("%sCategoryCatalog", catalogName);
        final boolean result = synchronizeCatalog(syncJobName, getSetupSyncJobService()::createProductCatalogSyncJob);
        if (!result) {
            LOG.warn("Category Catalog synchronization for {} failed", catalogName);
        } else {
            LOG.info("Done synchronizing Category Catalog {}", catalogName);
        }
    }

    private boolean synchronizeCatalog(final String syncJobName, final Consumer<String> createSyncJobConsumer) {
        boolean result = true;
        createSyncJobConsumer.accept(syncJobName);
        final PerformResult syncCronJobResult = getSetupSyncJobService().executeCatalogSyncJob(syncJobName);
        if (isSyncRerunNeeded(syncCronJobResult)) {
            result = false;
        }
        return result;
    }

    private boolean isSyncRerunNeeded(final PerformResult syncCronJobResult) {
        return CronJobStatus.FINISHED.equals(syncCronJobResult.getStatus()) && !CronJobResult.SUCCESS.equals(syncCronJobResult.getResult());
    }

    public SetupSyncJobService getSetupSyncJobService() {
        return setupSyncJobService;
    }

    @Required
    public void setSetupSyncJobService(final SetupSyncJobService setupSyncJobService) {
        this.setupSyncJobService = setupSyncJobService;
    }

    public SetupSolrIndexerService getSetupSolrIndexerService() {
        return setupSolrIndexerService;
    }

    @Required
    public void setSetupSolrIndexerService(final SetupSolrIndexerService setupSolrIndexerService) {
        this.setupSolrIndexerService = setupSolrIndexerService;
    }
}

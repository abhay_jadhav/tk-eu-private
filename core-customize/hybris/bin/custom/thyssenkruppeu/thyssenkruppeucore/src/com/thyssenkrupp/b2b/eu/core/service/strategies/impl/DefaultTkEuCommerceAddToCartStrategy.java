package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import com.thyssenkrupp.b2b.eu.core.service.hooks.CommerceDoAddToCartMethodHook;
import com.thyssenkrupp.b2b.eu.core.service.strategies.TkEuCommerceCartCalculationStrategy;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.internal.model.order.InMemoryCartModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

public class DefaultTkEuCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy {

    private static final Logger                              LOG                      = LoggerFactory.getLogger(DefaultTkEuCommerceAddToCartStrategy.class);
    private static final String                              DOADDTOCART_HOOK_ENABLED = "tk.euCore.addToCartStrategy.doAddToCartMethodHooks.enabled";
    private              List<CommerceDoAddToCartMethodHook> commerceDoAddToCartMethodHooks;
    private              TkEuCommerceCartCalculationStrategy tkEuCommerceCartCalculationStrategy;
    private              BaseSiteService                     baseSiteService;
    private              TkUomConversionService              tkUomConversionService;

    @Override
    protected CommerceCartModification doAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException {
        beforeDoAddToCart(parameter);
        CommerceCartModification commerceCartModification = super.doAddToCart(parameter);
        afterDoAddToCart(parameter, commerceCartModification);
        return commerceCartModification;
    }

    protected void beforeDoAddToCart(final CommerceCartParameter parameters) {
        final List<CommerceDoAddToCartMethodHook> hooks = getCommerceDoAddToCartMethodHooks();
        if (isDoAddToCartHookEnabled() && isNotEmpty(hooks) && parameters.isEnableHooks()) {
            for (final CommerceDoAddToCartMethodHook hook : hooks) {
                LOG.debug(String.format("Executing hook:[%s] in beforeDoAddToCart", hook.getClass().getSimpleName()));
                hook.beforeDoAddToCart(parameters);
            }
        }
    }

    protected void afterDoAddToCart(CommerceCartParameter parameters, CommerceCartModification commerceCartModification) {
        final List<CommerceDoAddToCartMethodHook> hooks = getCommerceDoAddToCartMethodHooks();
        if (isDoAddToCartHookEnabled() && isNotEmpty(hooks) && parameters.isEnableHooks()) {
            for (final CommerceDoAddToCartMethodHook hook : hooks) {
                LOG.debug(String.format("Executing hook:[%s] in afterDoAddToCart", hook.getClass().getSimpleName()));
                hook.afterDoAddToCart(parameters, commerceCartModification);
            }
        }
    }

    @Override
    protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
        final long quantityToAdd, final PointOfServiceModel pointOfServiceModel) {
        final long cartLevel = checkCartLevel(productModel, cartModel, pointOfServiceModel);
        final long stockLevel = cartModel instanceof InMemoryCartModel ? getForceInStockMaxQuantity() : getAvailableStockLevel(productModel, pointOfServiceModel);

        // How many will we have in our cart if we add quantity
        final long newTotalQuantity = cartLevel + quantityToAdd;

        // Now limit that to the total available in stock
        final long newTotalQuantityAfterStockLimit = Math.min(newTotalQuantity, stockLevel);

        // So now work out what the maximum allowed to be added is (note that
        // this may be negative!)
        final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();

        if (isMaxOrderQuantitySet(maxOrderQuantity)) {
            final long newTotalQuantityAfterProductMaxOrder = Math
                .min(newTotalQuantityAfterStockLimit, maxOrderQuantity.longValue());
            return newTotalQuantityAfterProductMaxOrder - cartLevel;
        }
        return newTotalQuantityAfterStockLimit - cartLevel;
    }

    @Override
    public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException {
        final CommerceCartModification modification = doAddToCart(parameter);

        CartModel cart = parameter.getCart();

        setPricingSequenceId(cart);
        getCommerceCartCalculationStrategy().calculateCart(parameter);
        afterAddToCart(parameter, modification);
        mergeEntry(modification, parameter);
        getTkEuCommerceCartCalculationStrategy().calculateCartTotals(parameter, modification);

        return modification;
    }

    @Override
    protected UnitModel getUnit(final CommerceCartParameter parameter) throws CommerceCartModificationException {
        final ProductModel productModel = parameter.getProduct();
        try {
            final List<UnitModel> storeSalesUnits = getBaseStoreService().getCurrentBaseStore().getSalesUnits();
            final UnitModel firstAvailableSalesUnit = getFirstAvailableSalesUnit(productModel, storeSalesUnits);
            return firstAvailableSalesUnit == null ? productModel.getBaseUnit() : firstAvailableSalesUnit;
        } catch (final ModelNotFoundException ex) {
            throw new CommerceCartModificationException(ex.getMessage(), ex);
        }
    }

    private UnitModel getFirstAvailableSalesUnit(ProductModel productModel, List<UnitModel> storeSalesUnits) {
        UnitModel firstAvailableUnit = null;
        final UnitModel productSalesUnit = productModel.getUnit();
        if (isNotEmpty(storeSalesUnits)) {
            if (productSalesUnit != null && storeSalesUnits.contains(productSalesUnit)) {
                firstAvailableUnit = productSalesUnit;
            } else {
                final List<TkUomConversionModel> uomConversionByProductCode = emptyIfNull(getTkUomConversionService().getUomConversionByProductCode(productModel.getCode()));
                final Optional<UnitModel> firstAvailableSalesUnit = uomConversionByProductCode.stream()
                    .map(TkUomConversionModel::getSalesUnit).filter(tkUomConversionModel -> storeSalesUnits.contains(tkUomConversionModel))
                    .findFirst();
                if (firstAvailableSalesUnit.isPresent()) {
                    firstAvailableUnit = firstAvailableSalesUnit.get();
                }
            }
        }
        return firstAvailableUnit;
    }

    private void setPricingSequenceId(CartModel cart) {
        if (cart == null) {
            return;
        }

        BaseSiteModel baseSite = getBaseSiteService().getCurrentBaseSite();
        CMSSiteModel site = null;

        if (baseSite != null) {
            site = (CMSSiteModel) baseSite;
        }
        Long catalogPricingSequenceId = 0L;
        Long cartPricingSequenceId = cart.getPricingSequenceId();

        if (site != null && site.getDefaultCatalog() != null) {
            catalogPricingSequenceId = site.getDefaultCatalog().getActiveCatalogVersion().getPricingSequenceId();
        }
        if (cartPricingSequenceId == null || cartPricingSequenceId.longValue() == 0L
            || !cartPricingSequenceId.equals(catalogPricingSequenceId)) {
            cart.setPricingSequenceId(catalogPricingSequenceId);
        }
    }

    public TkUomConversionService getTkUomConversionService() {
        return tkUomConversionService;
    }

    @Required
    public void setTkUomConversionService(TkUomConversionService tkUomConversionService) {
        this.tkUomConversionService = tkUomConversionService;
    }

    private boolean isDoAddToCartHookEnabled() {
        return getConfigurationService().getConfiguration().getBoolean(DOADDTOCART_HOOK_ENABLED, false);
    }

    public List<CommerceDoAddToCartMethodHook> getCommerceDoAddToCartMethodHooks() {
        return commerceDoAddToCartMethodHooks;
    }

    public void setCommerceDoAddToCartMethodHooks(List<CommerceDoAddToCartMethodHook> commerceDoAddToCartMethodHooks) {
        this.commerceDoAddToCartMethodHooks = commerceDoAddToCartMethodHooks;
    }

    public TkEuCommerceCartCalculationStrategy getTkEuCommerceCartCalculationStrategy() {
        return tkEuCommerceCartCalculationStrategy;
    }

    public void setTkEuCommerceCartCalculationStrategy(TkEuCommerceCartCalculationStrategy tkEuCommerceCartCalculationStrategy) {
        this.tkEuCommerceCartCalculationStrategy = tkEuCommerceCartCalculationStrategy;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}

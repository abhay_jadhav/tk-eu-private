package com.thyssenkrupp.b2b.eu.core.process.strategies;

import static org.apache.commons.lang.ArrayUtils.isNotEmpty;

import de.hybris.platform.acceleratorservices.process.strategies.impl.DefaultEmailTemplateTranslationStrategy;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.i18n.impl.DefaultI18NService;

import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.core.io.Resource;

public class DefaultTkEmailTemplateTranslationStrategy extends DefaultEmailTemplateTranslationStrategy {

    private I18NService i18nService;

    @Override
    protected List<String> getPropertiesRootPath(final RendererTemplateModel renderTemplate, final String languageIso) {
        final List<String> propertiesRootPath = super.getPropertiesRootPath(renderTemplate, languageIso);
        return getFallBackPropertiesRootPath(propertiesRootPath, renderTemplate, languageIso);
    }

    private List<String> getFallBackPropertiesRootPath(final List<String> propertiesRootPath, final RendererTemplateModel renderTemplate, final String languageIso) {
        if (!isResourceBundleExists(propertiesRootPath)) {
            final Locale[] fallbackLocales = getFallBackLocalesForLanguage(languageIso);
            if (isNotEmpty(fallbackLocales)) {
                final String fallBackLanguage = fallbackLocales[0].toString();
                final List<String> rootPaths = super.getPropertiesRootPath(renderTemplate, fallBackLanguage);
                return getFallBackPropertiesRootPath(rootPaths, renderTemplate, fallBackLanguage);
            }
        }
        return propertiesRootPath;
    }

    private boolean isResourceBundleExists(final List<String> propertiesRootPaths) {
        if (CollectionUtils.isNotEmpty(propertiesRootPaths)) {
            for (final String path : propertiesRootPaths) {
                final Resource resource = getApplicationContext().getResource(path);
                if (resource == null || !resource.exists() || !resource.isReadable()) {
                    return false;
                }
            }
        }
        return true;
    }

    private Locale[] getFallBackLocalesForLanguage(final String languageIso) {
        final Locale localeForIsoCode = getCommonI18NService().getLocaleForIsoCode(languageIso);
        return getI18nService().getFallbackLocales(localeForIsoCode);
    }

    public void setI18nService(final DefaultI18NService i18nService) {
        this.i18nService = i18nService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }
}


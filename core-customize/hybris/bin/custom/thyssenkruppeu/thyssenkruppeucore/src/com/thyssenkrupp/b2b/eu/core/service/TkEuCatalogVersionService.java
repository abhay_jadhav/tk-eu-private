package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;

public interface TkEuCatalogVersionService extends CatalogVersionService {

    CatalogVersionModel getDependantCatalogVersion(CatalogVersionModel storeCatalogVersion);

    CatalogVersionModel getDependsOnCatalogVersion(CatalogVersionModel productCatalogVersion);

    boolean isProductCatalogVersion(CatalogVersionModel catalogVersion);

    boolean isProductCatalogVersion(String catalogId, String version);

    CatalogVersionModel getActiveCatalogVersionForCatalogId(String catalogId);

    CatalogVersionModel getCatalogVersionByIsActive(String catalogId, boolean isActive);

    CatalogVersionModel getCatalogVersionByIsActive(CatalogModel catalog, boolean isActive);
}

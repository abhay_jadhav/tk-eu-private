package com.thyssenkrupp.b2b.eu.core.cronjob;

import static com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus.DUPLICATE;
import static com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus.NEW;
import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.strategies.ActivateBaseSiteInSessionStrategy;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.model.TkEuRegistrationEmailCronJobModel;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;

public class TkEuRegistrationEmailJob extends AbstractJobPerformable<TkEuRegistrationEmailCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkEuRegistrationEmailJob.class);
    private static final String SEARCH_CUSTOMER_PAGINATION_SIZE = "registration.email.cronjob.search.customer.pagination.pageSize";
    private TkEuB2bCustomerService<B2BCustomerModel, B2BUnitModel> b2bCustomerService;
    private TkEuB2bCustomerAccountService b2bCustomerAccountService;
    private CMSSiteService cmsSiteService;
    private ActivateBaseSiteInSessionStrategy<CMSSiteModel> activateBaseSiteInSession;
    private ConfigurationService configurationService;
    private ModelService modelService;
    @Override
    public PerformResult perform(final TkEuRegistrationEmailCronJobModel cronJobModel) {
        try {
            LOG.info("Sending Registration emails");
            final SearchPageData<B2BCustomerModel> searchPageNewData = getB2bCustomerService()
                    .getPagedCustomersByRegistrationStatus(createPageableData(), NEW);
            if (searchPageNewData.getResults().isEmpty()) {
                LOG.info("No New Customer Registration found, skipping send of Registration mails");
                return new PerformResult(SUCCESS, FINISHED);
            }
            final CMSSiteModel contentSite = cronJobModel.getContentSite();
            getCmsSiteService().setCurrentSite(contentSite);
            getActivateBaseSiteInSession().activate(contentSite);

            final List<CustomerModel> duplicateUidEmails = new LinkedList<>();
            final List<CustomerModel> registrationEmails = new LinkedList<>();
            final Set<String> setEmail = new HashSet<>();
            final List<B2BCustomerModel> searchPageAllData = getB2bCustomerService().getAllUsers();

            searchPageAllData.stream().forEach(c -> {
            validateUid(duplicateUidEmails, registrationEmails, setEmail, c);
            });


            if (!duplicateUidEmails.isEmpty()) {
                LOG.info("Email Sending to Customer Support for Duplicate Email IDs: ");
                duplicateUidEmails.stream().forEach(customer -> {
                    LOG.info("For Customer " + customer.getUid());
                    if (null != ((B2BCustomerModel) customer).getRegistrationStatus()
                            && ((B2BCustomerModel) customer).getRegistrationStatus().equals(NEW)) {
                        ((B2BCustomerModel) customer).setRegistrationStatus(DUPLICATE);
                        modelService.save(customer);
                    }
                });


                getB2bCustomerAccountService().sendDuplicateUidFoundRegistrationEmail(duplicateUidEmails);
            }

            final List<CustomerModel> registrationNewEmails = registrationEmails.stream()
                    .filter(regEmail -> (null != ((B2BCustomerModel) regEmail).getRegistrationStatus()
                            &&
            ((B2BCustomerModel) regEmail).getRegistrationStatus().equals(NEW))).collect(Collectors.toList());

            if (!registrationNewEmails.isEmpty()) {
                LOG.info("Email Sending to valid Unique Users: ");
                registrationNewEmails.stream().forEach(c -> {
                    LOG.info("For Customer " + c.getUid());
                    getB2bCustomerAccountService().sendRegistrationEmail(c);
                });
            }
            LOG.info("Finished Sending Registration emails");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while Sending Registration emails", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void validateUid(final List<CustomerModel> duplicateUidEmails,
            final List<CustomerModel> registrationEmails, final Set<String> setEmail, final B2BCustomerModel customerModel) {

        if (null != customerModel.getRegistrationStatus()
                && !customerModel.getRegistrationStatus().equals(DUPLICATE)) {
        if (!setEmail.add(
                (customerModel.getUid()).contains("|") ? StringUtils.substringAfter(customerModel.getUid(), "|").trim()
                        : customerModel.getUid())) {
            LOG.debug("Duplicate UID found for customerUid:" + customerModel.getUid());
            removeFromRegistrationEmail(registrationEmails, customerModel,duplicateUidEmails);
        } else {
            registrationEmails.add(customerModel);
        }
        }
    }

    private void removeFromRegistrationEmail(final List<CustomerModel> registrationEmails, final CustomerModel customer,final List<CustomerModel> duplicateUidEmails) {
      final String email = customer.getUid().contains("|") ? StringUtils.substringAfter(customer.getUid(), "|").trim() : customer.getUid();
      try {

            final List<CustomerModel> duplicateRegistrationEmails = registrationEmails.stream()
                    .filter(model -> email.equals(
                            model.getUid().contains("|") ? StringUtils.substringAfter(model.getUid(), "|").trim()
                                    : model.getUid()))
                    .collect(Collectors.toList());
            duplicateRegistrationEmails.add(customer);

            for (final CustomerModel model : duplicateRegistrationEmails) {

                if (null != ((B2BCustomerModel) model).getRegistrationStatus()
                            && ((B2BCustomerModel) model).getRegistrationStatus().equals(NEW)) {
                        duplicateUidEmails.add(model);
                    if (null != ((B2BCustomerModel) customer).getRegistrationStatus()
                            && ((B2BCustomerModel) customer).getRegistrationStatus().equals(NEW)) {
                        duplicateUidEmails.add(customer);
                        }
                    break;
                } else {
                        duplicateUidEmails.add(customer);
                    break;
                    }
            }
            registrationEmails.removeAll(duplicateRegistrationEmails);
        } catch (final Exception e) {
          LOG.error("Exception occured in removeFromRegistrationEmail() method for customer:"+customer.getUid()+" with exception "+e);
      }

    }

    private PageableData createPageableData() {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(getConfigurationService().getConfiguration().getInt(SEARCH_CUSTOMER_PAGINATION_SIZE));
        return pageableData;
    }

    public TkEuB2bCustomerService<B2BCustomerModel, B2BUnitModel> getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(final TkEuB2bCustomerService<B2BCustomerModel, B2BUnitModel> b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }

    public TkEuB2bCustomerAccountService getB2bCustomerAccountService() {
        return b2bCustomerAccountService;
    }

    public void setB2bCustomerAccountService(final TkEuB2bCustomerAccountService b2bCustomerAccountService) {
        this.b2bCustomerAccountService = b2bCustomerAccountService;
    }

    public CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    public ActivateBaseSiteInSessionStrategy<CMSSiteModel> getActivateBaseSiteInSession() {
        return activateBaseSiteInSession;
    }

    public void setActivateBaseSiteInSession(final ActivateBaseSiteInSessionStrategy<CMSSiteModel> activateBaseSiteInSession) {
        this.activateBaseSiteInSession = activateBaseSiteInSession;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

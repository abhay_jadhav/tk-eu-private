package com.thyssenkrupp.b2b.eu.core.healthcheck;

import de.hybris.platform.healthcheck.HybrisHealthCheck;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.SolrIndexedTypeCodeResolver;
import de.hybris.platform.solrfacetsearch.model.SolrIndexModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexService;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProviderFactory;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.request.SolrPing;
import org.apache.solr.client.solrj.response.SolrPingResponse;

/**
 * Execute ping request to the SOLR server and check if it responses with "OK"
 */
public class SolrHealthCheck extends HybrisHealthCheck {

    private final String _solrStatusResponse = "status";

    private SolrSearchProviderFactory solrSearchProviderFactory;
    private SolrIndexService          solrIndexService;
    private FacetSearchConfigService  facetSearchConfigService;
    private SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver;

    @Override
    public Result check() {

        Result result;
        try {
            final SolrIndexedTypeModel solrIndexedType = solrIndexService.getAllIndexes().get(0).getIndexedType();
            final String configName = solrIndexedType.getSolrFacetSearchConfig().getName();
            final FacetSearchConfig facetSearchConfig = facetSearchConfigService.getConfiguration(configName);
            final IndexedType indexedType = facetSearchConfig.getIndexConfig().getIndexedTypes().get(this.solrIndexedTypeCodeResolver.resolveIndexedTypeCode(solrIndexedType));
            final SolrSearchProvider solrSearchProvider = solrSearchProviderFactory.getSearchProvider(facetSearchConfig, indexedType);
            final SolrIndexModel solrIndex = solrIndexService.getActiveIndex(facetSearchConfig.getName(), indexedType.getIdentifier());
            final Index index = solrSearchProvider.resolveIndex(facetSearchConfig, indexedType, solrIndex.getQualifier());
            final SolrClient solrClient = solrSearchProvider.getClient(index);

            final SolrPingResponse pingResult = (new SolrPing()).process(solrClient, index.getName());
            result = pingResult.getResponse().get(_solrStatusResponse).equals(true) ? Result.healthy() : Result.unhealthy("Solr health check failed");


        } catch (final Exception ex) {
            result = Result.unhealthy(ex);
        }

        return result;
    }

    public void setSolrSearchProviderFactory(final SolrSearchProviderFactory solrSearchProviderFactory) {
        this.solrSearchProviderFactory = solrSearchProviderFactory;
    }

    public void setSolrIndexService(final SolrIndexService solrIndexService) {
        this.solrIndexService = solrIndexService;
    }

    public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService) {
        this.facetSearchConfigService = facetSearchConfigService;
    }

    public void setSolrIndexedTypeCodeResolver(final SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver) {
        this.solrIndexedTypeCodeResolver = solrIndexedTypeCodeResolver;
    }
}

package com.thyssenkrupp.b2b.eu.core.atp.service.strategies;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequestItem;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequestItemConfiguration;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.SapAtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;

public class SapAtpRequestStrategy {

    private static final Logger LOG = Logger.getLogger(SapAtpRequestStrategy.class);
    private static final String ATP_SAP_WNAE_ENABLED = "atp.sap.type.wnae.enabled";
    private static final String TRADELEN_CODE = "LA_INT_ALPHA";
    private static final String CERTIFICATE_CODE = "ZGNIS";
    private static final String LONGPROD_CODE = "LA";
    private static final String LONGPROD_VAL = "HL";
    private static final String CUT_TO_LENGTH_PROCESSING_CODE = "KZANA";
    private static final String CUT_TO_LENGTH_PROCESSING_VALUE = "FS";
    private static final String TYPE_OF_LENGTH_CODE = "LA";
    private static final String TYPE_OF_LENGTH_VALUE = "FL";
    private static final String SAWING_LENGTH_CODE = "FX_1";
    private static final String NO_OF_PIECES_CODE = "FX_1_ST";
    private static final String TOLLARENCE_CODE = "LTOL_ALPHA";
    private static final String NO_OF_CUTS_CODE = "XX_AZSCH";
    private static final String TKLEN_CODE = "XX_LENGTH";
    private static final String TKWIDTH_CODE = "XX_WIDTH";
    private static final String CERTIFICATE = "CERTIFICATE";
    private static final String TKTRADELENGTH = "TKTRADELENGTH";
    private static final String TKCCLENGTH = "TKCCLENGTH";
    private static final String CUTTOLENGTH_PRODINFO = "CUTTOLENGTH_PRODINFO";


    private CartService cartService;
    private BaseStoreService baseStoreService;
    private String sapHybrisBusinessRegion;
    private ProductService productService;
    private TkUomConversionService uomConversionService;
    private UnitService unitService;
    private UserService userService;
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;


    public SapAtpRequest buildSapAtpRequest(final AtpRequestData requestData) {
        final B2BCustomerModel currentUser = getCurrentUser();
        final AddressModel address = getDeliveryAddressFromCart(getCartService().getSessionCart());
        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        return createSapAtpRequest(currentUser, address, baseStore, requestData);
    }

    private SapAtpRequest createSapAtpRequest(final B2BCustomerModel customer, final AddressModel address, final BaseStoreModel baseStore, final AtpRequestData requestData) {
        final AtpRequest atpRequest = new AtpRequest();
        atpRequest.setRegionBusinessDivisionEnvironment(getSapHybrisBusinessRegion());
        populateDeliveryAddressDetails(atpRequest, address, customer);
        populateBaseStoreDetails(atpRequest, baseStore);
        populateShippingCondition(atpRequest, customer);
        atpRequest.setAtpRequestItems(requestData.getEntries().stream().map(this::createRequestItem).collect(Collectors.toList()));
        atpRequest.setWnae(TkEuConfigKeyValueUtils.getBoolean(ATP_SAP_WNAE_ENABLED, false));
        if (atpRequest.getWnae()) {
            populateCustomerSoldToNumber(atpRequest, customer);
            atpRequest.setAtpRequestItemConfigurations(populateAndGetItemConfigurations(requestData.getEntries(), baseStore));
        }
        final SapAtpRequest sapAtpRequest = new SapAtpRequest();
        sapAtpRequest.setAtpRequest(atpRequest);
        return sapAtpRequest;
    }

    /**
     * @param atpRequest
     *
     */
    private void populateCustomerSoldToNumber(final AtpRequest atpRequest, final B2BCustomerModel customer) {

        getDefaultB2BUnitIdOfCustomer(customer).ifPresent(atpRequest::setCustomerSoldToNumber);
    }

    private AtpRequestItem createRequestItem(final AtpRequestEntryData entryData) {
        final AtpRequestItem atpRequestItem = new AtpRequestItem();
        atpRequestItem.setLineItemNumber(entryData.getPosition().toString());
        atpRequestItem.setMaterialNumber(entryData.getProductCode());
        populateConvertedBaseUnitDetails(atpRequestItem, entryData);
        return atpRequestItem;
    }

    private void populateConvertedBaseUnitDetails(final AtpRequestItem atpRequestItem, final AtpRequestEntryData entryData) {
        final ProductModel product = getProductService().getProductForCode(entryData.getProductCode());
        final UnitModel baseUnit = product.getBaseUnit();
        final UnitModel salesUnit = getUnitService().getUnitForCode(entryData.getSalesUnit());
        final Pair<Double, UnitModel> pair = getUomConversionService().calculateQuantityInTargetUnit(Pair.of(entryData.getQuantityInKg(), salesUnit), product.getBaseUnit(), product);
        if (nonNull(pair)) {
            atpRequestItem.setQuantity(pair.getKey().toString());
        }
        atpRequestItem.setUnitOfMeasure(baseUnit.getSapCode());
    }

    private void populateDeliveryAddressDetails(final AtpRequest atpRequest, final AddressModel address, final B2BCustomerModel customer) {
        if (nonNull(address)) {
            getCustomerShipToNumber(address, customer).ifPresent(atpRequest::setCustomerShipToNumber);
            getShipToCountryCode(address).ifPresent(atpRequest::setShipToCountryCode);
            getShipToPostalCode(address).ifPresent(atpRequest::setShipToPostalCode);
        }
    }

    private void populateBaseStoreDetails(final AtpRequest atpRequest, final BaseStoreModel baseStore) {
        if (nonNull(baseStore) && nonNull(baseStore.getSAPConfiguration())) {
            final SAPConfigurationModel sapConfiguration = baseStore.getSAPConfiguration();
            atpRequest.setDivision(sapConfiguration.getSapcommon_division());
            atpRequest.setSalesOrganization(sapConfiguration.getSapcommon_salesOrganization());
            atpRequest.setDistributionChannel(sapConfiguration.getSapcommon_distributionChannel());
        }
    }

    private void populateShippingCondition(final AtpRequest atpRequest, final B2BCustomerModel customer) {
        final B2BUnitModel parentUnit = getB2bUnitService().getParent(customer);
        if (nonNull(parentUnit) && nonNull(parentUnit.getShippingcondition())) {
            atpRequest.setShippingConditions(parentUnit.getShippingcondition().getCode());
        }
    }

    private Optional<String> getCustomerShipToNumber(final AddressModel address, final B2BCustomerModel customer) {
        final Optional<String> maybeSapCustomerId = getSapCustomerIdFromAddress(address);
        if (maybeSapCustomerId.isPresent()) {
            return maybeSapCustomerId;
        }
        return getDefaultB2BUnitIdOfCustomer(customer);
    }

    private Optional<String> getSapCustomerIdFromAddress(final AddressModel address) {
        return nonNull(address) ? Optional.ofNullable(address.getSapCustomerID()) : Optional.empty();
    }

    private Optional<String> getDefaultB2BUnitIdOfCustomer(final B2BCustomerModel customer) {
        return nonNull(customer) && nonNull(customer.getDefaultB2BUnit()) ? Optional.ofNullable(customer.getDefaultB2BUnit().getUid()) : Optional.empty();
    }

    private Optional<String> getShipToCountryCode(final AddressModel address) {
        return nonNull(address.getCountry()) ? Optional.ofNullable(address.getCountry().getIsocode()) : Optional.empty();
    }

    private Optional<String> getShipToPostalCode(final AddressModel address) {
        return Optional.ofNullable(address.getPostalcode());
    }

    private AddressModel getDeliveryAddressFromCart(final CartModel cart) {
        if (isNull(cart) || isNull(cart.getDeliveryAddress())) {
            LOG.warn("DeliveryAddress not available in session Cart");
            return null;
        }
        return cart.getDeliveryAddress();
    }

    private B2BCustomerModel getCurrentUser() {
        return (B2BCustomerModel) getUserService().getCurrentUser();
    }

    public CartService getCartService() {
        return cartService;
    }

    @Required
    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public String getSapHybrisBusinessRegion() {
        return sapHybrisBusinessRegion;
    }

    @Required
    public void setSapHybrisBusinessRegion(final String sapHybrisBusinessRegion) {
        this.sapHybrisBusinessRegion = sapHybrisBusinessRegion;
    }

    public TkUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    @Required
    public void setUomConversionService(final TkUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    public ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    public UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService() {
        return b2bUnitService;
    }

    @Required
    public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    private List<AtpRequestItemConfiguration> populateAndGetItemConfigurations(final List<AtpRequestEntryData> entries, final BaseStoreModel baseStore) {
        final List<AtpRequestItemConfiguration> allAtpRequestItemConfigurations = new ArrayList<AtpRequestItemConfiguration>();
        entries.forEach(eachEntry -> allAtpRequestItemConfigurations.addAll(createRequestItemConfiguration(eachEntry, baseStore.getSAPConfiguration())));
        return allAtpRequestItemConfigurations;
    }

    @SuppressWarnings("boxing")
    private List<AtpRequestItemConfiguration> createRequestItemConfiguration(final AtpRequestEntryData entryData, final SAPConfigurationModel sapConfigurationModel) {
        final List<AtpRequestItemConfiguration> atpRequestItemConfigurations = new ArrayList<AtpRequestItemConfiguration>();
        final String entryNumber = entryData.getPosition().toString();
        final String instanceNumber = "00000001";
        if (CollectionUtils.isNotEmpty(entryData.getConfigurationInfos())) {
            for (final ConfigurationInfoData configInfoData : entryData.getConfigurationInfos()) {
                final String configureType = configInfoData.getConfiguratorType().getCode();
                if ((isValidConfiguration(configureType, configInfoData))) {
                    switch (configureType) {
                        case CERTIFICATE:

                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, CERTIFICATE_CODE, configInfoData.getUniqueId()));
                            break;

                        case TKTRADELENGTH:

                            final Double length = Double.valueOf(configInfoData.getLengthValue());
                            final Double conversion = Double.valueOf(configInfoData.getUnit().getConversion());
                            final Double tkTradeLength = length * conversion;
                            final String tradeLength = tkTradeLength.toString();
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, LONGPROD_CODE, LONGPROD_VAL));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, TRADELEN_CODE, tradeLength));
                            break;

                        case TKCCLENGTH:

                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, TKLEN_CODE, configInfoData.getLengthValue()));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, TKWIDTH_CODE, configInfoData.getWidthValue()));
                            break;

                        case CUTTOLENGTH_PRODINFO:

                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, CUT_TO_LENGTH_PROCESSING_CODE, CUT_TO_LENGTH_PROCESSING_VALUE));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, TYPE_OF_LENGTH_CODE, TYPE_OF_LENGTH_VALUE));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, SAWING_LENGTH_CODE, configInfoData.getLengthValue()));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, NO_OF_PIECES_CODE, String.valueOf(entryData.getQuantity())));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, TOLLARENCE_CODE, configInfoData.getSapExportValue()));
                            atpRequestItemConfigurations.add(new AtpRequestItemConfiguration(entryNumber, instanceNumber, NO_OF_CUTS_CODE, String.valueOf(entryData.getQuantity())));

                            break;

                            default:
                                break;

                    }
                }

            }
        }
        return atpRequestItemConfigurations;
    }

    public CategoryModel getWidthCategoryForProduct(final ProductModel product) {
        final Collection<CategoryModel> supercategories = product.getSupercategories();
        if (supercategories != null) {
            for (final CategoryModel category : supercategories) {
                if (category instanceof ClassificationClassModel) {
                    return category;
                }
            }
        }
        return null;
    }

    private boolean isValidConfiguration(final String configureType, final ConfigurationInfoData configData) {
        return ("1".equalsIgnoreCase(configData.getConfigurationValue()) || "true".equalsIgnoreCase(configData.getConfigurationValue()) || ConfiguratorType.TKCCLENGTH.getCode().equals(configureType));
    }
}

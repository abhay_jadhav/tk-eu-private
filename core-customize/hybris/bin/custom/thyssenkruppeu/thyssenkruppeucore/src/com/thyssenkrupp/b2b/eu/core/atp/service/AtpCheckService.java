package com.thyssenkrupp.b2b.eu.core.atp.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

public interface AtpCheckService {
    List<AtpResponseData> doAtpCheck(AtpRequestData request, BaseStoreModel baseStore);
}

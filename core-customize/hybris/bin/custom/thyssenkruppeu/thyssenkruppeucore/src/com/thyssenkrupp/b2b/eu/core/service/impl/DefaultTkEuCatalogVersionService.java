package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Objects;
import java.util.Optional;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

public class DefaultTkEuCatalogVersionService extends DefaultCatalogVersionService implements TkEuCatalogVersionService {

    private static final Logger         LOG = LoggerFactory.getLogger(DefaultTkEuCatalogVersionService.class);
    private              CatalogService catalogService;

    @Override
    public CatalogVersionModel getDependantCatalogVersion(CatalogVersionModel storeCatalogVersion) {
        if (storeCatalogVersion != null) {
            return (CatalogVersionModel) storeCatalogVersion.getProperty("dependantCatalogVersion");
        }
        return null;
    }

    @Override
    public CatalogVersionModel getDependsOnCatalogVersion(CatalogVersionModel productCatalogVersion) {
        if (productCatalogVersion != null) {
            return (CatalogVersionModel) productCatalogVersion.getProperty("dependsOnCatalogVersion");
        }
        return null;
    }

    @Override
    public boolean isProductCatalogVersion(CatalogVersionModel catalogVersion) {
        return (catalogVersion != null && (boolean) catalogVersion.getProperty("isProductCatalog"));
    }

    @Override
    public boolean isProductCatalogVersion(String catalogId, String version) {
        final CatalogVersionModel catalogVersion = this.getCatalogVersion(catalogId, version);
        if (catalogVersion != null) {
            return isProductCatalogVersion(catalogVersion);
        }
        return false;
    }

    public CatalogVersionModel getActiveCatalogVersionForCatalogId(final String catalogId) {
        validateParameterNotNull(catalogId, "Parameter 'catalogId' must not be null!");
        final CatalogModel catalog = getCatalogService().getCatalogForId(catalogId);
        if (Objects.nonNull(catalog) && Objects.nonNull(catalog.getActiveCatalogVersion())) {
            return catalog.getActiveCatalogVersion();
        } else {
            LOG.error("No Active CatalogVersion found for catalog:" + catalogId);
            throw new UnknownIdentifierException("ActiveCatalogVersion not found for catalogId:" + catalogId);
        }
    }

    public CatalogVersionModel getCatalogVersionByIsActive(final String catalogId, final boolean isActive) {
        validateParameterNotNull(catalogId, "Parameter 'catalogId' must not be null!");
        return getCatalogVersionByIsActive(getCatalogService().getCatalogForId(catalogId), isActive);
    }

    public CatalogVersionModel getCatalogVersionByIsActive(final CatalogModel catalog, final boolean isActive) {
        validateParameterNotNull(catalog, "Parameter 'catalog' must not be null!");
        final Optional<CatalogVersionModel> maybeCatalogVersion = emptyIfNull(catalog.getCatalogVersions()).stream().filter(o -> isActive == o.getActive()).findFirst();
        if (!maybeCatalogVersion.isPresent()) {
            LOG.error("No CatalogVersion found for catalog:" + catalog.getId());
            throw new UnknownIdentifierException("CatalogVersion not found for catalog:" + catalog.getId() + "and isActive:" + isActive);
        }
        return maybeCatalogVersion.get();
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }

    @Required
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCommerceCartService;
import com.thyssenkrupp.b2b.eu.core.service.strategies.TkEuCommerceUpdateCartEntryStrategy;
import de.hybris.platform.b2bacceleratorservices.order.impl.DefaultB2BCommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import org.springframework.beans.factory.annotation.Required;

public class DefaultTkEuB2bCommerceCartService extends DefaultB2BCommerceCartService implements TkEuB2bCommerceCartService {

    private TkEuCommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategy;

    @Override
    public CommerceCartModification updateAtpInformation(CommerceCartParameter parameter) {
        return getCommerceUpdateCartEntryStrategy().updateAtpInformationForCartEntry(parameter);
    }

    @Override
    public CommerceCartModification updateAtpConsolidatedDate(CommerceCartParameter parameter) {
        return getCommerceUpdateCartEntryStrategy().updateAtpConsolidatedDate(parameter);
    }

    @Override
    public CommerceCartModification updateCertificateForCartEntry(CommerceCartParameter parameter) {
        return getCommerceUpdateCartEntryStrategy().updateCertificateForCartEntry(parameter);
    }

    @Override
    public TkEuCommerceUpdateCartEntryStrategy getCommerceUpdateCartEntryStrategy() {
        return commerceUpdateCartEntryStrategy;
    }

    @Required
    public void setCommerceUpdateCartEntryStrategy(TkEuCommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategy) {
        this.commerceUpdateCartEntryStrategy = commerceUpdateCartEntryStrategy;
    }
}

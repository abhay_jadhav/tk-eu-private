package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class TkEuEmailGenerationService extends DefaultEmailGenerationService {

    private static final String BCC_EMAILS = "bccEmails";

    @Override
    protected EmailMessageModel createEmailMessage(final String emailSubject, final String emailBody,
                                                   final AbstractEmailContext<BusinessProcessModel> emailContext) {
        final List<EmailAddressModel> toEmails = new ArrayList<EmailAddressModel>();
        final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getToEmail(),
                emailContext.getToDisplayName());
        toEmails.add(toAddress);
        final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
                emailContext.getFromDisplayName());
        final List<EmailAddressModel> bccEmails = setBccAddress(emailContext);

        return getEmailService().createEmailMessage(toEmails, new ArrayList<EmailAddressModel>(),
                bccEmails, fromAddress, emailContext.getFromEmail(), emailSubject, emailBody, null);
    }

    private List<EmailAddressModel> setBccAddress(final AbstractEmailContext<BusinessProcessModel> emailContext) {
        List<EmailAddressModel> emailAddressList = new ArrayList<>();
        if (null != emailContext.get(BCC_EMAILS) && CollectionUtils.isNotEmpty((Collection) emailContext.get(BCC_EMAILS))) {
            List<String> bccEmails = (List<String>) emailContext.get(BCC_EMAILS);
            EmailAddressModel emailAddressModel;
            for (String bccEmail : bccEmails) {
                emailAddressModel = getEmailService().getOrCreateEmailAddressForEmail(bccEmail, "");
                emailAddressList.add(emailAddressModel);
            }
        }

        return emailAddressList;
    }


}

package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.order.AbstractOrderEntryTypeService;
import de.hybris.platform.order.strategies.ordercloning.impl.DefaultCloneAbstractOrderStrategy;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator;
import de.hybris.platform.servicelayer.type.TypeService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultTkEuCloneAbstractOrderStrategy extends DefaultCloneAbstractOrderStrategy {

    private final TypeService                   typeService;
    private final ItemModelCloneCreator         itemModelCloneCreator;
    private final AbstractOrderEntryTypeService abstractOrderEntryTypeService;

    public DefaultTkEuCloneAbstractOrderStrategy(TypeService typeService, ItemModelCloneCreator itemModelCloneCreator, AbstractOrderEntryTypeService abstractOrderEntryTypeService) {
        super(typeService, itemModelCloneCreator, abstractOrderEntryTypeService);
        this.typeService = typeService;
        this.itemModelCloneCreator = itemModelCloneCreator;
        this.abstractOrderEntryTypeService = abstractOrderEntryTypeService;
    }

    @Override
    public <T extends AbstractOrderModel> T clone(final ComposedTypeModel sourceOrderType, final ComposedTypeModel sourceEntryType,
      final AbstractOrderModel original, final String code, final Class abstractOrderClassResult,
      final Class abstractOrderEntryClassResult) {
        validateParameterNotNull(original, "original must not be null!");
        validateParameterNotNull(abstractOrderClassResult, "abstractOrderClassResult must not be null!");
        validateParameterNotNull(abstractOrderEntryClassResult, "abstractOrderEntryClassResult must not be null!");

        final ComposedTypeModel orderType = getOrderType(sourceOrderType, original, abstractOrderClassResult);
        final ComposedTypeModel entryType = getOrderEntryType(sourceEntryType, original, abstractOrderClassResult,
          abstractOrderEntryClassResult);

        final ItemModelCloneCreator.CopyContext copyContext = createCopyContext(entryType);

        final T orderClone = (T) getItemModelCloneCreator().copy(orderType, original, copyContext);
        if (code != null) {
            orderClone.setCode(code);
        }
        postProcess(original, orderClone);
        return orderClone;
    }

    protected ItemModelCloneCreator.CopyContext createCopyContext(ComposedTypeModel entryType) {
        return new ItemModelCloneCreator.CopyContext() {
            @Override
            public ComposedTypeModel getTargetType(final ItemModel originalModel) {
                if (originalModel instanceof AbstractOrderEntryModel) {
                    return entryType;
                }
                return super.getTargetType(originalModel);
            }
        };
    }

    protected <T extends AbstractOrderModel> ComposedTypeModel getOrderType(final ComposedTypeModel orderType,
      final AbstractOrderModel original, final Class<T> clazz) {
        if (orderType != null) {
            return orderType;
        }

        if (clazz.isAssignableFrom(original.getClass())) {
            return getTypeService().getComposedTypeForClass(original.getClass());
        }

        return getTypeService().getComposedTypeForClass(clazz);
    }

    protected <E extends AbstractOrderEntryModel, T extends AbstractOrderModel> ComposedTypeModel getOrderEntryType(
      final ComposedTypeModel entryType, final AbstractOrderModel original, final Class<T> orderClazz, final Class<E> clazz) {
        if (entryType != null) {
            return entryType;
        }

        if (orderClazz.isAssignableFrom(original.getClass())) {
            return getAbstractOrderEntryTypeService().getAbstractOrderEntryType(original);
        }

        return getTypeService().getComposedTypeForClass(clazz);
    }

    public TypeService getTypeService() {
        return typeService;
    }

    public ItemModelCloneCreator getItemModelCloneCreator() {
        return itemModelCloneCreator;
    }

    public AbstractOrderEntryTypeService getAbstractOrderEntryTypeService() {
        return abstractOrderEntryTypeService;
    }
}

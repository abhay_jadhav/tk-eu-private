package com.thyssenkrupp.b2b.eu.core.atp.service.impl;

import static de.hybris.platform.basecommerce.enums.InStockStatus.NOTAVAILABLE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Objects.nonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.ObjectUtils.compare;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Stopwatch;
import com.thyssenkrupp.b2b.eu.core.atp.predicate.SapAtpCheckEnabledPredicate;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpCheckService;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpService;

public class AtpServiceImpl implements AtpService {

    private static final Logger                      LOG = Logger.getLogger(AtpServiceImpl.class);
    private              BaseStoreService            baseStoreService;
    private              AtpCheckService             hybrisAtpCheckService;
    private              AtpCheckService             sapAtpCheckService;
    private              SapAtpCheckEnabledPredicate sapAtpCheckEnabledPredicate;
    private              CartService                 cartService;

    private final Comparator<AtpResponseData> responseDataComparator = Comparator.comparingInt(AtpResponseData::getPosition).thenComparing(AtpResponseData::getProductCode)
            .thenComparing((o1, o2) -> compare(o1.isValidSAPAtpResult() ? Boolean.TRUE : Boolean.FALSE, o2.isValidSAPAtpResult() ? Boolean.TRUE : Boolean.FALSE)).thenComparing((o1, o2) -> compare(o1.getHybrisAtpResult(), o2.getHybrisAtpResult()));

    @Override
    public List<AtpResponseData> doAtpCheck(final AtpRequestData request) {
        validateParameterNotNullStandardMessage("atpRequest", request);
        if (isEmpty(request.getEntries())) {
            LOG.warn("Empty AtpRequest or No DeliveryAddress found in SessionCart, Skipping ATP check");
            return Collections.emptyList();
        }
        final Stopwatch stopWatch = Stopwatch.createStarted();
        logDebugRequest(request);
        final List<AtpResponseData> responseList = doPreHybrisCheckAndCallSap(request);
        stopWatch.stop();
        logDebugResponse(responseList, stopWatch);
        return responseList;
    }

    private List<AtpResponseData> doPreHybrisCheckAndCallSap(final AtpRequestData request) {
        final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
        final CartModel cartModel = getCartService().getSessionCart();
        if (nonNull(cartModel) && nonNull(cartModel.getDeliveryAddress()) && getSapAtpCheckEnabledPredicate().test(request, baseStore)) {
            final List<AtpResponseData> hybrisResponseList = getHybrisAtpCheckService().doAtpCheck(request, baseStore);
            final List<AtpRequestEntryData> requestsWithValidHybrisResponse = getRequestsWithValidHybrisResponse(request.getEntries(), hybrisResponseList);
            LOG.debug("SAP check enabled, delegating call to SAP. RequestEntry size:" + requestsWithValidHybrisResponse.size());
            final List<AtpResponseData> sapResponseList = callSapWithRequestsThatHasValidHybrisResponse(request, requestsWithValidHybrisResponse, baseStore);
            return mergeValidSapResponseAndHybrisResponse(sapResponseList, hybrisResponseList);
        }
        LOG.debug("SAP check disabled or DeliveryAddress missing in SessionCart, delegating call to Hybris. RequestEntry size:" + request.getEntries().size());
        return getHybrisAtpCheckService().doAtpCheck(request, baseStore);
    }

    private List<AtpResponseData> callSapWithRequestsThatHasValidHybrisResponse(final AtpRequestData originalRequest, final List<AtpRequestEntryData> requestsWithValidHybrisResponse,
        final BaseStoreModel baseStore) {
        if (isEmpty(requestsWithValidHybrisResponse)) {
            LOG.warn("No Request's found that has valid Hybris Response, Skipping call to SAP");
            return Collections.emptyList();
        }
        final AtpRequestData request = new AtpRequestData();
        request.setIsPdpRequest(originalRequest.getIsPdpRequest());
        request.setEntries(requestsWithValidHybrisResponse);
        return getSapAtpCheckService().doAtpCheck(request, baseStore);
    }

    private List<AtpResponseData> mergeValidSapResponseAndHybrisResponse(final List<AtpResponseData> sapResponseList, final List<AtpResponseData> hybrisResponseList) {
        final List<AtpResponseData> validSapResponseList = emptyIfNull(sapResponseList).stream().filter(s -> s.isValidSAPAtpResult()).collect(Collectors.toList());
        LOG.debug("Found " + validSapResponseList.size() + " valid sap responses");
        return new ArrayList<>(Stream.concat(emptyIfNull(hybrisResponseList).stream(), validSapResponseList.stream())
            .collect(Collectors.toMap(AtpResponseData::getPosition, Function.identity(), BinaryOperator.maxBy(responseDataComparator))).values());
    }

    private List<AtpRequestEntryData> getRequestsWithValidHybrisResponse(final List<AtpRequestEntryData> requestList, final List<AtpResponseData> hybrisResponseList) {
        return emptyIfNull(requestList).stream().filter(requestEntry -> requestHasValidHybrisResponsePredicate().test(requestEntry, hybrisResponseList)).collect(Collectors.toList());
    }

    private BiPredicate<AtpRequestEntryData, List<AtpResponseData>> requestHasValidHybrisResponsePredicate() {
        return (requestData, responseList) -> {
            final String productCode = requestData.getProductCode();
            final Integer position = requestData.getPosition();
            return emptyIfNull(responseList).stream().anyMatch(s -> Objects.equals(productCode, s.getProductCode()) && Objects.equals(position, s.getPosition())
                && nonNull(s.getHybrisAtpResult()) && NOTAVAILABLE != s.getHybrisAtpResult());
        };
    }

    private void logDebugRequest(final AtpRequestData request) {
        logDebug(request.getEntries(), e -> {
            final String source = isTrue(request.getIsPdpRequest()) ? "PDP" : "Cart";
            return "Request Entry [position:" + e.getPosition() + " productCode:" + e.getProductCode() + " quantity:" + e.getQuantity() + " salesUom:" + e.getSalesUnit() + " source:" + source + "]";
        });
    }

    private void logDebugResponse(final Collection<AtpResponseData> list, final Stopwatch stopWatch) {
        logDebug(list, e -> {
            final String responseResult = nonNull(e.getSapAtpResult()) ? " WebInterfaceSuccess:" + e.getSapAtpResult() : " Fallback:" + e.getHybrisAtpResult();
            return "Response Entry [position:" + e.getPosition() + " productCode:" + e.getProductCode() + responseResult + " duration:" + stopWatch.elapsed(MILLISECONDS) + "ms" + "]";
        });
    }

    private <T> void logDebug(final Collection<T> list, final Function<T, String> messageFunction) {
        if (LOG.isDebugEnabled()) {
            list.forEach(t -> LOG.debug(messageFunction.apply(t)));
        }
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public AtpCheckService getHybrisAtpCheckService() {
        return hybrisAtpCheckService;
    }

    @Required
    public void setHybrisAtpCheckService(final AtpCheckService hybrisAtpCheckService) {
        this.hybrisAtpCheckService = hybrisAtpCheckService;
    }

    public AtpCheckService getSapAtpCheckService() {
        return sapAtpCheckService;
    }

    @Required
    public void setSapAtpCheckService(final AtpCheckService sapAtpCheckService) {
        this.sapAtpCheckService = sapAtpCheckService;
    }

    public SapAtpCheckEnabledPredicate getSapAtpCheckEnabledPredicate() {
        return sapAtpCheckEnabledPredicate;
    }

    @Required
    public void setSapAtpCheckEnabledPredicate(final SapAtpCheckEnabledPredicate sapAtpCheckEnabledPredicate) {
        this.sapAtpCheckEnabledPredicate = sapAtpCheckEnabledPredicate;
    }

    public CartService getCartService() {
        return cartService;
    }

    @Required
    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }
}

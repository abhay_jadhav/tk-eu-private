package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.commerceservices.security.SecureToken;

public interface RegistrationService {

    boolean validateRegistrationToken(SecureToken data);
}


package com.thyssenkrupp.b2b.eu.core.service.hooks;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public interface CommerceDoAddToCartMethodHook {
    void beforeDoAddToCart(CommerceCartParameter parameters);

    void afterDoAddToCart(CommerceCartParameter parameters, CommerceCartModification commerceCartModification);
}

package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.ClassificationPropertyValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TkEuClassificationPropertyValueProvider extends ClassificationPropertyValueProvider {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuClassificationPropertyValueProvider.class);

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {
        if (model instanceof ProductModel) {
            List<FieldValue> fieldValues = new ArrayList<>();
            final ProductModel product = (ProductModel) model;
            updateIndexPropertyName(indexedProperty);
            processProductOrVariantsClassificationPropertyValues(indexConfig, indexedProperty, product, fieldValues);
            return getUniqueOrEmptyValues(fieldValues);
        } else {
            throw new FieldValueProviderException("Cannot provide classification property of non-product item");
        }
    }

    private void updateIndexPropertyName(IndexedProperty indexedProperty) {
        String indexProperty = indexedProperty.getValueProviderParameter();
        if (StringUtils.isNotBlank(indexProperty)) {
            indexedProperty.setName(indexProperty);
        }
    }

    private void processProductOrVariantsClassificationPropertyValues(IndexConfig indexConfig, IndexedProperty indexedProperty, ProductModel product, List<FieldValue> fieldValues) {
        extractClassificationFieldValues(indexConfig, indexedProperty, product, fieldValues);
        if (CollectionUtils.isEmpty(fieldValues)) {
            processVariantsClassificationPropertyValues(product, fieldValues, indexConfig, indexedProperty);
        }
    }

    private void processVariantsClassificationPropertyValues(ProductModel product, List<FieldValue> fieldValues, IndexConfig indexConfig, IndexedProperty indexedProperty) {
        final Collection<VariantProductModel> variantProducts = product.getVariants();
        if (CollectionUtils.isNotEmpty(variantProducts)) {
            variantProducts.forEach(variantProduct -> extractClassificationFieldValues(indexConfig, indexedProperty, variantProduct, fieldValues));
        }
    }

    private void extractClassificationFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, ProductModel product, List<FieldValue> fieldValues) {
        final Collection<FieldValue> classificationFieldValues;
        try {
            classificationFieldValues = super.getFieldValues(indexConfig, indexedProperty, product);
            fieldValues.addAll(classificationFieldValues);
        } catch (FieldValueProviderException exception) {
            LOG.error("Exception thrown while fetching field values", exception);
        }
    }

    private Collection<FieldValue> getUniqueOrEmptyValues(List<FieldValue> fieldValues) {
        if (CollectionUtils.isNotEmpty(fieldValues)) {
            return filterUniqueFieldValues(getFieldNames(fieldValues), fieldValues);
        }
        return fieldValues;
    }

    @SuppressWarnings("unchecked")
    private List<FieldValue> filterUniqueFieldValues(Set fieldNames, List<FieldValue> fieldValues) {
        List<FieldValue> uniqueFieldValues = new ArrayList<>();
        fieldNames.forEach(uniqueKey -> {
            Set classificationValues = new HashSet();
            fieldValues.forEach(fieldValue -> {
                if (uniqueKey.equals(fieldValue.getFieldName())) {
                    classificationValues.add(fieldValue.getValue());
                }
            });
            uniqueFieldValues.add(createFieldValueObject((String) uniqueKey, classificationValues));
        });
        return uniqueFieldValues;
    }

    @SuppressWarnings("unchecked")
    private Set getFieldNames(List<FieldValue> fieldValues) {
        Set<String> uniqueKeys = new HashSet();
        fieldValues.forEach(fieldValue -> uniqueKeys.add(fieldValue.getFieldName()));
        return uniqueKeys;
    }

    private FieldValue createFieldValueObject(String fieldKeyName, Object value) {
        return new FieldValue(fieldKeyName, value);
    }
}

package com.thyssenkrupp.b2b.eu.core.event;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;

public class TkEuB2bRegistrationAmbiguousEmailEvent extends AbstractEvent {
    private List<CustomerModel> ambiguousCustomerModelList;

    public List<CustomerModel> getAmbiguousCustomerModelList() {
        return ambiguousCustomerModelList;
    }

    public void setAmbiguousCustomerModelList(final List<CustomerModel> ambiguousCustomerModelList) {
        this.ambiguousCustomerModelList = ambiguousCustomerModelList;
    }

}

package com.thyssenkrupp.b2b.eu.core.service.hooks.impl;

import com.thyssenkrupp.b2b.eu.core.service.hooks.CommerceDoAddToCartMethodHook;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.model.ModelService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class ProcessingOptionsDoAddToCartMethodHook implements CommerceDoAddToCartMethodHook {

    private ModelService modelService;

    @Override
    public void beforeDoAddToCart(CommerceCartParameter parameters) {
        // Implementation not needed
    }

    @Override
    public void afterDoAddToCart(final CommerceCartParameter parameters, final CommerceCartModification cartModification) {
        validateParameterNotNullStandardMessage("commerceCartParameter", parameters);
        validateParameterNotNullStandardMessage("commerceCartModification", cartModification);
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}

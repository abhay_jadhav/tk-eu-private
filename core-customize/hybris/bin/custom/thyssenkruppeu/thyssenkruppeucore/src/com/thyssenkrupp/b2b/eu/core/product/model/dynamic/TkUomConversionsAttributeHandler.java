package com.thyssenkrupp.b2b.eu.core.product.model.dynamic;

import static java.util.Collections.emptyList;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;

public class TkUomConversionsAttributeHandler implements DynamicAttributeHandler<Collection<TkUomConversionModel>, ProductModel> {

    private TkUomConversionService tkUomConversionService;

    @Override
    public Collection<TkUomConversionModel> get(final ProductModel model) {
        if (model == null) {
            return emptyList();
        }
        return getTkUomConversionService().getUomConversionByProductCode(model.getCode());
    }

    @Override
    public void set(final ProductModel model, final Collection<TkUomConversionModel> tkUomConversionModels) {
        throw new UnsupportedOperationException("ProductModel.uomConversions is readonly attribute");
    }

    @Required
    public void setTkUomConversionService(final TkUomConversionService tkUomConversionService) {
        this.tkUomConversionService = tkUomConversionService;
    }

    public TkUomConversionService getTkUomConversionService() {
        return tkUomConversionService;
    }
}

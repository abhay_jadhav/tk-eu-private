package com.thyssenkrupp.b2b.eu.core.atp.exchanges.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AtpRequestItem implements Serializable {

    @JsonProperty("Line_Item_Number")
    private String lineItemNumber;
    @JsonProperty("Material_Number")
    private String materialNumber;
    @JsonProperty("Quantity")
    private String quantity;
    @JsonProperty("Unit_Of_Measure")
    private String unitOfMeasure;

    @JsonProperty("Line_Item_Number")
    public String getLineItemNumber() {
        return lineItemNumber;
    }

    @JsonProperty("Line_Item_Number")
    public void setLineItemNumber(String lineItemNumber) {
        this.lineItemNumber = lineItemNumber;
    }

    @JsonProperty("Material_Number")
    public String getMaterialNumber() {
        return materialNumber;
    }

    @JsonProperty("Material_Number")
    public void setMaterialNumber(String materialNumber) {
        this.materialNumber = materialNumber;
    }

    @JsonProperty("Quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("Quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("Unit_Of_Measure")
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    @JsonProperty("Unit_Of_Measure")
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Line_Item_Number", lineItemNumber).append("Material_Number", materialNumber)
            .append("Quantity", quantity).append("Unit_Of_Measure", unitOfMeasure).toString();
    }
}

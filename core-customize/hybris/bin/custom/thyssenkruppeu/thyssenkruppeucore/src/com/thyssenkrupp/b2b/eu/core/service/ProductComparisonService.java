package com.thyssenkrupp.b2b.eu.core.service;

import java.util.LinkedHashMap;
import java.util.List;

public interface ProductComparisonService {

    LinkedHashMap<String, String> getComparisonData(List<String> comparisonProductList, String[] attributeList);
}

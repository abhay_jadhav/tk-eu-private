package com.thyssenkrupp.b2b.eu.core;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppeucoreStandalone {

    public static void main(final String[] args) {
        new ThyssenkruppeucoreStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}


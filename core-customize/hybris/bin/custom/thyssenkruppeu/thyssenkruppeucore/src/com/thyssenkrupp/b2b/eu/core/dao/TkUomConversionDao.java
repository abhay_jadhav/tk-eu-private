package com.thyssenkrupp.b2b.eu.core.dao;

import java.util.List;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;

public interface TkUomConversionDao {

    List<TkUomConversionModel> findUomConversionByProductCode(String productCode);

}

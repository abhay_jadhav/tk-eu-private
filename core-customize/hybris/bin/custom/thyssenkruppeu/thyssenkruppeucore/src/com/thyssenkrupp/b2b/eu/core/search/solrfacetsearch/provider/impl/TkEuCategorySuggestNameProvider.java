package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

public class TkEuCategorySuggestNameProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private static final Logger LOG = Logger.getLogger(TkEuCategorySuggestNameProvider.class);
    private Long                   categorySuggestLevels;
    private FieldNameProvider      fieldNameProvider;
    private CommonI18NService      commonI18NService;
    private DefaultCategoryService categoryService;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        final ProductModel productModel = (ProductModel) model;
        final Collection<CategoryModel> categoryPathForProduct = getCategoryService().getCategoryPathForProduct(productModel, CategoryModel.class);
        return CollectionUtils.isEmpty(categoryPathForProduct) ? Collections.emptyList() : getFieldValues(indexConfig, indexedProperty, categoryPathForProduct);
    }

    private Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Collection<CategoryModel> categoryPathForProduct) {
        final Collection<FieldValue> fieldValues = new ArrayList<>();
        final Collection<CategoryModel> categorySuggests = categoryPathForProduct.stream().limit(getCategorySuggestLevels()).collect(Collectors.toList());
        if (indexedProperty.isLocalized()) {
            final Collection<LanguageModel> languages = indexConfig.getLanguages();
            languages.forEach(language -> createFieldValuesFromCategorySuggests(indexedProperty, fieldValues, categorySuggests, language));
        } else {
            createFieldValuesFromCategorySuggests(indexedProperty, fieldValues, categorySuggests, null);
        }
        return fieldValues;
    }

    private void createFieldValuesFromCategorySuggests(final IndexedProperty indexedProperty, final Collection<FieldValue> fieldValues, final Collection<CategoryModel> categorySuggests, final LanguageModel language) {
        categorySuggests.forEach(categorySuggest -> {
            //Filter for rootCategories
            if (!CollectionUtils.isEmpty(categorySuggest.getSupercategories())) {
                fieldValues.addAll(createFieldValue(categorySuggest, language, indexedProperty));
            }
        });
    }

    private List<FieldValue> createFieldValue(final CategoryModel category, final LanguageModel language, final IndexedProperty indexedProperty) {
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (language != null) {
            buildFieldValuesForLocale(category, language, indexedProperty, fieldValues);
        } else {
            buildFieldValuesForNonLocale(category, indexedProperty, fieldValues);
        }
        return fieldValues;
    }

    private void buildFieldValuesForLocale(final CategoryModel category, final LanguageModel language, final IndexedProperty indexedProperty, final List<FieldValue> fieldValues) {
        final String categoryName = getCategoryNameForLocale(category, language);
        if (StringUtils.isNotBlank(categoryName)) {
            LOG.debug("categoryName :" + categoryName + " for the locale : " + language.getIsocode());
            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
            buildFieldValues(fieldValues, categoryName, fieldNames);
        }
    }

    private void buildFieldValuesForNonLocale(final CategoryModel category, final IndexedProperty indexedProperty, final List<FieldValue> fieldValues) {
        final String categoryName = category.getName();
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
        buildFieldValues(fieldValues, categoryName, fieldNames);
    }

    private void buildFieldValues(final List<FieldValue> fieldValues, final String categoryName, final Collection<String> fieldNames) {
        fieldNames.forEach(fieldName -> fieldValues.add(new FieldValue(fieldName, categoryName)));
    }

    private String getCategoryNameForLocale(final CategoryModel category, LanguageModel language) {
        final Locale locale = i18nService.getCurrentLocale();
        try {
            i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
            final Optional<String> categoryNameForLocale = Optional.ofNullable(getCategoryName(category));
            return categoryNameForLocale.orElseGet(() -> category.getName(locale));
        } finally {
            i18nService.setCurrentLocale(locale);
        }
    }

    private String getCategoryName(final CategoryModel categoryModel) {
        return modelService.getAttributeValue(categoryModel, "name");
    }

    protected Long getCategorySuggestLevels() {
        return categorySuggestLevels;
    }

    public void setCategorySuggestLevels(final Long categorySuggestLevels) {
        this.categorySuggestLevels = categorySuggestLevels;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    protected DefaultCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(DefaultCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

package com.thyssenkrupp.b2b.eu.core.interceptor;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import static de.hybris.platform.basecommerce.enums.InStockStatus.FORCEINSTOCK;
import static de.hybris.platform.basecommerce.enums.InStockStatus.FORCEOUTOFSTOCK;
import static de.hybris.platform.basecommerce.enums.InStockStatus.NOTSPECIFIED;

public class TkEuStockLevelStatusPrepareInterceptor implements PrepareInterceptor<StockLevelModel> {

    @Override
    public void onPrepare(StockLevelModel stockLevelModel, InterceptorContext interceptorContext) throws InterceptorException {
        InStockStatus inStockStatus = stockLevelModel.getInStockStatus();
        if (isRemovedStockStatus(inStockStatus)) {
            throw new InterceptorException("Invalid Stock Level Status {forceInStock, forceOutOfStock, notSpecified}");
        }
    }

    protected boolean isRemovedStockStatus(InStockStatus inStockStatus) {
        return inStockStatus == FORCEINSTOCK || inStockStatus == FORCEOUTOFSTOCK || inStockStatus == NOTSPECIFIED;
    }
}


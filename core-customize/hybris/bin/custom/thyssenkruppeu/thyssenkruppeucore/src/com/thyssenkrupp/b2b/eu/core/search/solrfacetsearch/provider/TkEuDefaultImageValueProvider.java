package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ImageValueProvider;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class TkEuDefaultImageValueProvider extends ImageValueProvider {

    private static final Logger LOG = Logger.getLogger(TkEuDefaultImageValueProvider.class);
    private String defaultImage;

    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }

    @Override
    protected MediaModel findMedia(final ProductModel product, final MediaFormatModel mediaFormat) {
        final MediaModel media = super.findMedia(product, mediaFormat);
        if (media == null && StringUtils.isNotEmpty(getDefaultImage())) {
            try {
                MediaModel defaultMedia = getMediaService().getMedia(product.getCatalogVersion(), getDefaultImage());
                final MediaFormatModel mediaFormatModel = getMediaService().getFormat(getMediaFormat());
                return getMediaService().getMediaByFormat(defaultMedia, mediaFormatModel);
            } catch (final ModelNotFoundException ignore) {
                LOG.warn("Could not find default media for the code : " + getDefaultImage() + "");
            }
        }
        return media;
    }
}

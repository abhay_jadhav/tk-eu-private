package com.thyssenkrupp.b2b.eu.core.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

public class GetDocumenStatusRequest implements Serializable {

    private GetDocumentStatusContent getDocumentStatusContent;

    @JsonProperty("Get_Document_Status")
    public GetDocumentStatusContent getGetDocumentStatusContent() {
        return getDocumentStatusContent;
    }

    public void setGetDocumentStatusContent(GetDocumentStatusContent getDocumentStatusContent) {
        this.getDocumentStatusContent = getDocumentStatusContent;
    }

    @Override
    public String toString() {
        return "GetDocumenStatusRequest.getDocumentStatusContent = " + getDocumentStatusContent;
    }
}

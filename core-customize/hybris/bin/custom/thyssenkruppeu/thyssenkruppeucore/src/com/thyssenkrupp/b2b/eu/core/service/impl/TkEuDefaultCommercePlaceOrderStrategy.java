package com.thyssenkrupp.b2b.eu.core.service.impl;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.order.impl.DefaultCommercePlaceOrderStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import org.apache.log4j.Logger;
import java.util.Collections;
import java.util.Date;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class TkEuDefaultCommercePlaceOrderStrategy extends DefaultCommercePlaceOrderStrategy {

    private static final Logger LOG = Logger.getLogger(TkEuDefaultCommercePlaceOrderStrategy.class);

    @Override
    public CommerceOrderResult placeOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException {
        final CartModel cartModel = parameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        final CommerceOrderResult result = new CommerceOrderResult();
        try {
            beforePlaceOrder(parameter);
            logRequiresCalculation(cartModel);
            final CustomerModel customer = (CustomerModel) cartModel.getUser();
            validateParameterNotNull(customer, "Customer model cannot be null");
            final OrderModel orderModel = getOrderService().createOrderFromCart(cartModel);
            orderModel.setInitialOrderVersion(Boolean.TRUE);
            placeOrder(parameter, cartModel, result, customer, orderModel);
        } finally {
            getExternalTaxesService().clearSessionTaxDocument();
        }

        this.afterPlaceOrder(parameter, result);
        return result;
    }

    private void logRequiresCalculation(final CartModel cartModel) {
        if (getCalculationService().requiresCalculation(cartModel)) {
            LOG.error(String.format("CartModel's [%s] calculated flag was false", cartModel.getCode()));
        }
    }

    private void placeOrder(final CommerceCheckoutParameter parameter, final CartModel cartModel,
      final CommerceOrderResult result, final CustomerModel customer, final OrderModel orderModel) throws InvalidCartException {
        if (orderModel != null) {
            updateAndSubmitOrder(parameter, cartModel, result, customer, orderModel);
        } else {
            throw new IllegalArgumentException(String.format("Order was not properly created from cart %s", cartModel.getCode()));
        }
    }

    protected void updateAndSubmitOrder(final CommerceCheckoutParameter parameter, final CartModel cartModel,
      final CommerceOrderResult result, final CustomerModel customer, final OrderModel orderModel) throws InvalidCartException {
        setOrderAttributes(parameter, cartModel, customer, orderModel);
        submitOrder(parameter, cartModel, result, customer, orderModel);
    }

    private void setOrderAttributes(final CommerceCheckoutParameter parameter, final CartModel cartModel, final CustomerModel customerModel, final OrderModel orderModel) {
        orderModel.setDate(new Date());
        setStoreInfo(orderModel);
        setB2bUnitTerms(cartModel, orderModel);
        setSalesApplication(parameter, orderModel);
        setEmptyPromotionResults(orderModel);
        storePaymentInfo(cartModel, orderModel);
        getModelService().saveAll(customerModel, orderModel);
    }


    private void setB2bUnitTerms(final CartModel cartModel, final OrderModel orderModel) {
        final B2BUnitModel b2BUnitModel = cartModel.getUnit();
        if (b2BUnitModel != null) {
            orderModel.setPaymentterms(b2BUnitModel.getPaymentterms());
            orderModel.setShippingcondition(b2BUnitModel.getShippingcondition());
            orderModel.setIncoterms(b2BUnitModel.getIncoterms());
        }
    }

    private void setStoreInfo(final OrderModel orderModel) {
        orderModel.setSite(getBaseSiteService().getCurrentBaseSite());
        orderModel.setStore(getBaseStoreService().getCurrentBaseStore());
        orderModel.setLanguage(getCommonI18NService().getCurrentLanguage());
    }

    private void setSalesApplication(final CommerceCheckoutParameter parameter, final OrderModel orderModel) {
        if (parameter.getSalesApplication() != null) {
            orderModel.setSalesApplication(parameter.getSalesApplication());
        }
    }

    private void setEmptyPromotionResults(final OrderModel orderModel) {
        orderModel.setAllPromotionResults(Collections.emptySet());
    }

    protected void storePaymentInfo(final CartModel cartModel, final OrderModel orderModel) {
        final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        if (paymentInfo != null && paymentInfo.getBillingAddress() != null) {
            final AddressModel billingAddress = paymentInfo.getBillingAddress();
            orderModel.setPaymentAddress(billingAddress);
            orderModel.getPaymentInfo().setBillingAddress(billingAddress);
            getModelService().save(orderModel.getPaymentInfo());
        }
    }

    private void submitOrder(final CommerceCheckoutParameter parameter, final CartModel cartModel, final CommerceOrderResult result,
      final CustomerModel customerModel, final OrderModel orderModel) throws InvalidCartException {
        getPromotionsService().transferPromotionsToOrder(cartModel, orderModel, false);
        calculateOrderTotals(orderModel);
        refreshModels(customerModel, orderModel);
        result.setOrder(orderModel);
        this.beforeSubmitOrder(parameter, result);
        getOrderService().submitOrder(orderModel);
    }

    private void calculateOrderTotals(final OrderModel orderModel) {
        try {
            getCalculationService().calculateTotals(orderModel, false);
            getExternalTaxesService().calculateExternalTaxes(orderModel);
        } catch (final CalculationException ex) {
            LOG.error("Failed to calculate order [" + orderModel.getCode() + "]", ex);
        }
    }

    private void refreshModels(final CustomerModel customerModel, final OrderModel orderModel) {
        getModelService().refresh(orderModel);
        getModelService().refresh(customerModel);
    }

}


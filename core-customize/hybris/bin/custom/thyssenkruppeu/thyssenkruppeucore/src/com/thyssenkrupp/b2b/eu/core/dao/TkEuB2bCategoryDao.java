package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Collection;

public interface TkEuB2bCategoryDao extends CategoryDao {

    Collection<VariantValueCategoryModel> findCategoriesBySuperCategory(VariantCategoryModel superCategory, CatalogVersionModel catalogVersion);

    CatalogVersionModel fetchProductCatalogBySalesOrganization(String salesOrganization);

    String getVariantCategoryCodeKey(String parentCategoryCode);

    Collection<VariantCategoryModel> findAllVariantCategories(String ruleCode, CatalogVersionModel catalogVersion);

    Collection<VariantValueCategoryModel> findAllVariantValueCategories(String ruleCode, CatalogVersionModel catalogVersion);

    <T extends CategoryModel> Collection<T> findAllCategoriesWithoutIncludedProducts(String categoryPrefix, CatalogVersionModel catalogVersion);
}

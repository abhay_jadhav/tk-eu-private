package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CartAvailabilityStatusAttributeHandler implements DynamicAttributeHandler<StockLevelStatus, CartModel> {

    private CommerceStockService commerceStockService;

    private BaseStoreService baseStoreService;

    private List<StockLevelStatus> orderedStockLevelStatus;

    @Override
    public StockLevelStatus get(CartModel model) {

        if (model != null && CollectionUtils.isNotEmpty(model.getEntries())) {
            List<StockLevelStatus> stockLevelStatuses = collectStockLevelStatus(getBaseStoreService().getCurrentBaseStore(), model.getEntries());
            stockLevelStatuses.sort(Comparator.comparing(getOrderedStockLevelStatus()::indexOf).reversed());
            final Optional<StockLevelStatus> leastAvailableStock = stockLevelStatuses.stream().findFirst();
            return leastAvailableStock.isPresent() ? leastAvailableStock.get() : null;
        }

        return null;
    }

    private List<StockLevelStatus> collectStockLevelStatus(@NotNull BaseStoreModel currentBaseStore, @NotNull List<AbstractOrderEntryModel> entries) {
        return entries.stream()
          .filter(abstractOrderEntryModel -> abstractOrderEntryModel.getProduct() != null)
          .map(abstractOrderEntryModel -> getCommerceStockService().getStockLevelStatusForProductAndBaseStore(abstractOrderEntryModel.getProduct(), currentBaseStore))
          .collect(Collectors.toList());
    }

    @Override
    public void set(CartModel model, StockLevelStatus stockLevelStatus) {
        throw new UnsupportedOperationException();
    }

    public List<StockLevelStatus> getOrderedStockLevelStatus() {
        return orderedStockLevelStatus;
    }

    @Required
    public void setOrderedStockLevelStatus(List<StockLevelStatus> orderedStockLevelStatus) {
        this.orderedStockLevelStatus = orderedStockLevelStatus;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public CommerceStockService getCommerceStockService() {
        return commerceStockService;
    }

    @Required
    public void setCommerceStockService(CommerceStockService commerceStockService) {
        this.commerceStockService = commerceStockService;
    }
}

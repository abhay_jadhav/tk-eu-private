package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

public interface TkEuFacetSearchService extends FacetSearchService {

   SearchQuery createProductSearchQuery(FacetSearchConfig facetSearchConfig, IndexedType indexedType, String productCode);

}

package com.thyssenkrupp.b2b.eu.core.process.actions;

import static com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus.PENDING_ACTIVATION;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bRegistrationProcessModel;

public class SetCustomerRegistrationStatusAction extends AbstractSimpleDecisionAction<TkEuB2bRegistrationProcessModel> {

    private static final Logger LOG = Logger.getLogger(SetCustomerRegistrationStatusAction.class);

    @Override
    public Transition executeAction(final TkEuB2bRegistrationProcessModel processModel) {
        try {
            final CustomerModel customer = processModel.getCustomer();
            ((B2BCustomerModel) customer).setRegistrationStatus(PENDING_ACTIVATION);
            this.modelService.save(processModel.getCustomer());
            LOG.debug("Updated Registration Status as 'Pending' for customer:" + customer.getUid());
            return Transition.OK;
        } catch (final Exception e) {
            LOG.error("Error while updating Customer Registration Status", e);
            return Transition.NOK;
        }
    }
}


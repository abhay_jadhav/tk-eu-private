package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ItemDetails implements Serializable {

    @JsonProperty("Document_Number")
    private String documentNo;

    @JsonProperty("Line_Item_Number")
    private String lineItemNo;

    @JsonProperty("Material_Number")
    private String materialNo;

    @JsonProperty("Batch_Number")
    private String batchNo;

    @JsonProperty("Document_Number")
    public String getDocumentNo() {
        return documentNo;
    }

    @JsonProperty("Document_Number")
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    @JsonProperty("Line_Item_Number")
    public String getLineItemNo() {
        return lineItemNo;
    }

    @JsonProperty("Line_Item_Number")
    public void setLineItemNo(String lineItemNo) {
        this.lineItemNo = lineItemNo;
    }

    @JsonProperty("Material_Number")
    public String getMaterialNo() {
        return materialNo;
    }

    @JsonProperty("Material_Number")
    public void setMaterialNo(String materialNo) {
        this.materialNo = materialNo;
    }

    @JsonProperty("Batch_Number")
    public String getBatchNo() {
        return batchNo;
    }

    @JsonProperty("Batch_Number")
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }
}

package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.suggester;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.suggester.exceptions.SolrAutoSuggestException;

import java.util.Map;

public interface SolrCategoryAutoSuggestService {
    Map<String, Long> getCategoryAutoSuggestionsForQuery(LanguageModel language, SolrIndexedTypeModel indexType, String input) throws SolrAutoSuggestException;
}

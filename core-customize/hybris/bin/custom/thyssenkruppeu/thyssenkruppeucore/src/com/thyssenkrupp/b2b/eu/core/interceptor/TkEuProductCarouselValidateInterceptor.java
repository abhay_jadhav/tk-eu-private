package com.thyssenkrupp.b2b.eu.core.interceptor;

import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class TkEuProductCarouselValidateInterceptor implements ValidateInterceptor {

    private static final Logger LOG = Logger.getLogger(TkEuProductCarouselValidateInterceptor.class);
    private I18NService i18nService;

    @Override
    public void onValidate(final Object object, final InterceptorContext interceptorContext) throws InterceptorException {

        if (object instanceof ProductCarouselComponentModel) {
            final ResourceBundle javaBundle = ResourceBundle.getBundle("localization.thyssenkruppeucore-locales",
              getI18nService().getCurrentLocale(), getClass().getClassLoader());
            final ProductCarouselComponentModel productCarouselComponent = (ProductCarouselComponentModel) object;
            final List<ProductModel> products = new ArrayList<>();
            if(CollectionUtils.isNotEmpty(productCarouselComponent.getProducts())){
                products.addAll(productCarouselComponent.getProducts());
            }


            final List<ProductModel> categoryProducts = CollectionUtils.emptyIfNull(productCarouselComponent.getCategories()).stream()
              .flatMap(c -> c.getProducts().stream()).collect(Collectors.toList());

            products.addAll(categoryProducts);

            final List<String> erpProductCodes = products.stream().filter(p -> p instanceof ERPVariantProductModel)
              .map(p -> p.getCode()).collect(Collectors.toList());

            if (erpProductCodes != null && erpProductCodes.size() > 0) {

                throw new InterceptorException(
                  MessageFormat.format(javaBundle.getString("product.carousel.erpVariant.error.msg"), erpProductCodes));
            }
        }
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }
}

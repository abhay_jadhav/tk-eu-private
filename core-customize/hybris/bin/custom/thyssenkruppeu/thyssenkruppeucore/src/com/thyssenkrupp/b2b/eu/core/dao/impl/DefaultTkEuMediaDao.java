package com.thyssenkrupp.b2b.eu.core.dao.impl;

import de.hybris.platform.servicelayer.media.impl.DefaultMediaDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuMediaDao;
import com.thyssenkrupp.b2b.global.baseshop.core.model.MediaIconModel;

public class DefaultTkEuMediaDao extends DefaultMediaDao implements TkEuMediaDao {

    private static String GET_MEDIA_ICON_BY_MIME_TYPE_QUERY = "SELECT {pk} FROM {MediaIcon} WHERE {mediaMimeTypeReference}=?mediaMimeTypeReference";

    @Override
    public MediaIconModel findMediaIconByMimeType(final String mimeType) {
        final Map<String, Object> params = new HashMap();
        params.put(MediaIconModel.MEDIAMIMETYPEREFERENCE, mimeType);
        final SearchResult<MediaIconModel> result = getFlexibleSearchService().search(GET_MEDIA_ICON_BY_MIME_TYPE_QUERY, params);
        if (result != null && CollectionUtils.isNotEmpty(result.getResult())) {
            return result.getResult().get(0);
        }
        return null;
    }

}

package com.thyssenkrupp.b2b.eu.core.facade;

import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.CustomerModel;

public interface RegistrationFacade {

    boolean validateRegistrationToken(SecureToken data);

    void resendEmail(CustomerModel customerModel);
}


package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.catalog.model.CatalogModel;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

public interface TkB2bCatalogProductReplicationConfigService {

    Optional<Map<String, String>> getAllVariantGenerationRules(CatalogModel catalog);

    Optional<Map<String, String>> getAllIdGenerationRules(CatalogModel catalog);

    Optional<String> getNameGenerationRule(CatalogModel catalog, String classKey);

    Optional<String> getIdGenerationRule(CatalogModel catalog, String classKey);

    Optional<String> getAttributeConsolidationRule(CatalogModel catalog, String classKey);

    Optional<String> getVariantGenerationRuleOrDefault(CatalogModel catalog, String classKey);

    Optional<Pair<String, String>> getVariantGenerationRule(CatalogModel catalog, String classKey);
}

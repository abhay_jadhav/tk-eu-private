package com.thyssenkrupp.b2b.eu.core.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.PRODUCT_REPLICATION_RULE_FALLBACK_KEY;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.defaultString;

import de.hybris.platform.catalog.model.CatalogModel;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.global.baseshop.core.model.TkProductReplicationConfigurationModel;

public class TkB2bDefaultCatalogProductReplicationConfigService implements TkB2bCatalogProductReplicationConfigService {

    private static final Logger LOG = LoggerFactory.getLogger(TkB2bDefaultCatalogProductReplicationConfigService.class);

    @Override
    public Optional<Map<String, String>> getAllVariantGenerationRules(final CatalogModel catalog) {
        return getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getVariantGenerationConfig);
    }

    @Override
    public Optional<Map<String, String>> getAllIdGenerationRules(final CatalogModel catalog) {
        return getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getIdGenerationConfig);
    }

    @Override
    public Optional<String> getNameGenerationRule(final CatalogModel catalog, final String classKey) {
        final Optional<Map<String, String>> maybeConfigMap = getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getNameGenerationConfig);
        return maybeConfigMap.flatMap(map -> getValueFromMap(map, classKey));
    }

    @Override
    public Optional<String> getIdGenerationRule(final CatalogModel catalog, final String classKey) {
        final Optional<Map<String, String>> maybeConfigMap = getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getIdGenerationConfig);
        return maybeConfigMap.flatMap(map -> getValueFromMap(map, classKey));
    }

    @Override
    public Optional<String> getAttributeConsolidationRule(final CatalogModel catalog, final String classKey) {
        final Optional<Map<String, String>> maybeConfigMap = getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getAttributeConsolidationConfig);
        return maybeConfigMap.flatMap(map -> getValueFromMapOrDefault(map, classKey));
    }

    @Override
    public Optional<String> getVariantGenerationRuleOrDefault(final CatalogModel catalog, final String classKey) {
        final Optional<Map<String, String>> maybeConfigMap = getConfigurationMapForAttribute(catalog, TkProductReplicationConfigurationModel::getVariantGenerationConfig);
        return maybeConfigMap.flatMap(map -> {
                final Optional<String> valueForClassKey = getValueFromMap(map, classKey);
                if (valueForClassKey.isPresent()) {
                    return valueForClassKey;
                } else {
                    LOG.info(format("No VariantGenerationConfig value found for class:%s, fetching the Default/Fallback value", classKey));
                    return getValueFromMap(map, PRODUCT_REPLICATION_RULE_FALLBACK_KEY);
                }
            }
        );
    }

    @Override
    public Optional<Pair<String, String>> getVariantGenerationRule(final CatalogModel catalog, final String classKey) {
        Pair<String, String> resultingPair = null;
        final Optional<Map<String, String>> maybeVariantGenerationRules = getAllVariantGenerationRules(catalog);
        if (maybeVariantGenerationRules.isPresent()) {
            final Map<String, String> variantGenerationRules = maybeVariantGenerationRules.get();
            if (variantGenerationRules.containsKey(classKey)) {
                resultingPair = Pair.of(classKey, variantGenerationRules.get(classKey));
            } else {
                resultingPair = Pair.of(PRODUCT_REPLICATION_RULE_FALLBACK_KEY, variantGenerationRules.get(PRODUCT_REPLICATION_RULE_FALLBACK_KEY));
            }
        }
        return Objects.nonNull(resultingPair) ? Optional.of(resultingPair) : Optional.empty();
    }

    private Optional<Map<String, String>> getConfigurationMapForAttribute(final CatalogModel catalog, final Function<TkProductReplicationConfigurationModel, Map<String, String>> configValueFunc) {
        final Optional<TkProductReplicationConfigurationModel> maybeConfiguration = getProductReplicationConfiguration(catalog);
        return maybeConfiguration.map(configValueFunc);
    }

    private Optional<TkProductReplicationConfigurationModel> getProductReplicationConfiguration(final CatalogModel catalog) {
        if (catalog == null || catalog.getConfiguration() == null) {
            return empty();
        }
        return ofNullable(catalog.getConfiguration());
    }

    private Optional<String> getValueFromMap(final Map<String, String> map, final String key) {
        if (MapUtils.isEmpty(map) || isEmpty(key)) {
            return empty();
        }
        return ofNullable(map.get(key));
    }

    private Optional<String> getValueFromMapOrDefault(final Map<String, String> map, final String key) {
        if (MapUtils.isEmpty(map) || isEmpty(key)) {
            return empty();
        }
        return ofNullable(defaultString(map.get(key), map.get(PRODUCT_REPLICATION_RULE_FALLBACK_KEY)));
    }
}

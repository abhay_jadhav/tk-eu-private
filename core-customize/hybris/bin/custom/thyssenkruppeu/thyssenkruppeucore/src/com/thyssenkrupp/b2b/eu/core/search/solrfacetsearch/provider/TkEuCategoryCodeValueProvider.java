package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryCodeValueProvider;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


public class TkEuCategoryCodeValueProvider extends CategoryCodeValueProvider {

    private TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService;

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {

        Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model);
        if (CollectionUtils.isNotEmpty(categories)) {
            categories = filterVariantCategory(categories);
            final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();
            if (indexedProperty.isLocalized()) {
                getLocalizedFieldValues(categories, fieldValues, indexConfig, indexedProperty);
            } else {
                addFieldValues(categories, fieldValues, null, indexedProperty);
            }
            return fieldValues;
        } else {
            return Collections.emptyList();
        }
    }

    private void getLocalizedFieldValues(Collection<CategoryModel> categories, Collection<FieldValue> fieldValues, IndexConfig indexConfig, IndexedProperty indexedProperty) {
        final Collection<LanguageModel> languages = indexConfig.getLanguages();
        for (final LanguageModel language : languages) {
            addFieldValues(categories, fieldValues, language, indexedProperty);
        }
    }

    private void addFieldValues(Collection<CategoryModel> categories, Collection<FieldValue> fieldValues, LanguageModel language, IndexedProperty indexedProperty) {
        for (final CategoryModel category : categories) {
            fieldValues.addAll(createFieldValue(category, language, indexedProperty));
        }
    }

    private Collection<CategoryModel> filterVariantCategory(Collection<CategoryModel> categories) {
        return tkStorefrontCategoryProviderService.filterStorefrontCategories(categories);
    }

    public TkEuStorefrontCategoryProviderService getTkStorefrontCategoryProviderService() {
        return tkStorefrontCategoryProviderService;
    }

    public void setTkStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService) {
        this.tkStorefrontCategoryProviderService = tkStorefrontCategoryProviderService;
    }
}

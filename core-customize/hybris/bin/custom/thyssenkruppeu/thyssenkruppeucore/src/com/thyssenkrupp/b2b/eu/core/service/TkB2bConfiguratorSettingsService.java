package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;

public interface TkB2bConfiguratorSettingsService extends ConfiguratorSettingsService {
    <T extends AbstractConfiguratorSettingModel> Optional<T> getConfiguratorSettingById(String id);

    <T extends AbstractConfiguratorSettingModel> Optional<T> getConfiguratorSettingById(String id, CatalogVersionModel catalogVersion);

    Optional<TkLengthConfiguratorSettingsModel> getFirstTkLengthConfiguratorSetting(@Nonnull ProductModel product);

}

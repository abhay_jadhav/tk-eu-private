package com.thyssenkrupp.b2b.eu.core.service.user.daos.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;

import java.util.List;

import javax.annotation.Resource;

import com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus;
import com.thyssenkrupp.b2b.eu.core.service.user.daos.TkEuUserDao;

public class TkUserDao extends DefaultUserDao implements TkEuUserDao {

    private static final String USER_MODEL_QUERY = "SELECT {user." + UserModel.PK + "} " + "FROM {" + UserModel._TYPECODE + " AS user} " + "WHERE {user." + UserModel.UID + "} LIKE ?user ";
    private static final String USER_MODEL_QUERY_AND_CONDITION = " AND ({" + UserModel.LOGINDISABLED + "} = 0 OR {" + UserModel.LOGINDISABLED + "} IS null)";
    private static final String B2B_CUSTOMER_MODEL_QUERY = "SELECT {user." + B2BCustomerModel.PK + "} " + "FROM {" + B2BCustomerModel._TYPECODE + " AS user} " + "WHERE " + "{user." + B2BCustomerModel.UID + "} LIKE ?user";
    private static final String B2B_CUSTOMER_MODEL_QUERY_AND_CONDITION = " AND ({" + B2BCustomerModel.LOGINDISABLED + "} = 0 OR {" + B2BCustomerModel.LOGINDISABLED + "} IS null)";

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Override
    public UserModel findUserByUID(final String uid) {
        return findUser(uid, USER_MODEL_QUERY, USER_MODEL_QUERY_AND_CONDITION, B2B_CUSTOMER_MODEL_QUERY, B2B_CUSTOMER_MODEL_QUERY_AND_CONDITION);
    }

    @Override
    public UserModel findAllUserByUID(final String uuid) {
        return findUser(uuid, USER_MODEL_QUERY, "", B2B_CUSTOMER_MODEL_QUERY, "");
    }

    private UserModel findUser(final String uuid, final String userModelQuery, final String userModelAndQuery, final String b2bCustomerModelQuery, final String b2bCustomerModelAndQuery) {

        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(b2bCustomerModelQuery + b2bCustomerModelAndQuery);
        fQuery.addQueryParameter("user", "%" + uuid);
        final SearchResult<B2BCustomerModel> customerResult = flexibleSearchService.search(fQuery);
        final List resList = customerResult.getResult();

        if (resList != null && resList.isEmpty()) {
            return getCustomerModel(userModelQuery, userModelAndQuery, uuid);
        } else {
            return getB2bCustomerModel(resList, customerResult, uuid);
        }
    }

    private UserModel getCustomerModel(final String userModelQuery, final String userModelAndQuery, final String uuid) {
        final FlexibleSearchQuery fQuery1 = new FlexibleSearchQuery(userModelQuery + userModelAndQuery);
        fQuery1.addQueryParameter("user", "%" + uuid);

        final SearchResult<UserModel> userResult = flexibleSearchService.search(fQuery1);
        final List userResList = userResult.getResult();
        if (userResList.size() > 1) {
            throw new AmbiguousIdentifierException("Found " + userResList.size() + " users with the unique uid \'" + uuid + "\'");
        } else {
            return userResList.isEmpty() ? null : (UserModel) userResList.get(0);
        }
    }

    private UserModel getB2bCustomerModel(final List resList, final SearchResult<B2BCustomerModel> customerResult, final String uuid) {
        return resList == null ? null : validateB2bCustomerStatus(resList, customerResult, uuid);
    }

    private UserModel validateB2bCustomerStatus(final List resList, final SearchResult<B2BCustomerModel> customerResult, final String uuid) {
        if (resList.size() > 1) {
            throw new AmbiguousIdentifierException("Found " + resList.size() + " users with the unique uid \'" + uuid + "\'");
        } else {
            final B2bRegistrationStatus registrationStatus = customerResult.getResult().get(0).getRegistrationStatus();
            if (registrationStatus != null && (registrationStatus.equals(B2bRegistrationStatus.ACTIVATED) || registrationStatus.equals(B2bRegistrationStatus.PENDING_ACTIVATION))) {
                return (B2BCustomerModel) resList.get(0);
            }
        }
        return null;
    }

    @Override
    public UserModel findAllUserAllStatusesByUID(final String userId) {
        return findUserAllStatuses(userId, USER_MODEL_QUERY, "", B2B_CUSTOMER_MODEL_QUERY, "");
    }

    private UserModel findUserAllStatuses(final String uuid, final String userModelQuery, final String userModelAndQuery, final String b2bCustomerModelQuery, final String b2bCustomerModelAndQuery) {

        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(b2bCustomerModelQuery + b2bCustomerModelAndQuery);
        fQuery.addQueryParameter("user", "%" + uuid);
        final SearchResult<B2BCustomerModel> customerResult = flexibleSearchService.search(fQuery);
        final List resList = customerResult.getResult();

        if (resList != null && resList.isEmpty()) {
            return getCustomerModel(userModelQuery, userModelAndQuery, uuid);
        } else {
            return (B2BCustomerModel) resList.get(0);
        }
    }

}

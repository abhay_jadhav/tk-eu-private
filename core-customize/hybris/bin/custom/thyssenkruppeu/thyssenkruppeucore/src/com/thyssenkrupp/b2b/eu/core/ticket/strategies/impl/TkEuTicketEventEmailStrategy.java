package com.thyssenkrupp.b2b.eu.core.ticket.strategies.impl;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.ticket.email.context.AbstractTicketContext;
import de.hybris.platform.ticket.enums.CsEmailRecipients;
import de.hybris.platform.ticket.enums.CsInterventionType;
import de.hybris.platform.ticket.events.model.CsTicketEmailModel;
import de.hybris.platform.ticket.events.model.CsTicketEventModel;
import de.hybris.platform.ticket.events.model.CsTicketResolutionEventModel;
import de.hybris.platform.ticket.model.CsTicketEventEmailConfigurationModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.strategies.impl.DefaultTicketEventEmailStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import net.sourceforge.pmd.util.StringUtil;

public class TkEuTicketEventEmailStrategy extends DefaultTicketEventEmailStrategy {

    private static final Logger LOG = Logger.getLogger(TkEuTicketEventEmailStrategy.class);
    private I18NService i18nService;

    @Override
    public void sendEmailsForEvent(final CsTicketModel ticket, final CsTicketEventModel event) {

        final String text = event.getText();
        final List<CsTicketEventEmailConfigurationModel> filteredConfigurations = getEmailConfiguration(ticket, event);
        if (CollectionUtils.isEmpty(filteredConfigurations)) {
            LOG.info("No email events found for type [" + this.getTicketEventCommentTypeString(event) + "]");
        } else {

            for (final CsTicketEventEmailConfigurationModel config : filteredConfigurations) {
                final AbstractTicketContext ticketContext = this.createContextForEvent(config, ticket, event);
                if (ticketContext != null) {
                    final CsTicketEmailModel email = this.constructAndSendEmail(ticketContext, config);
                    LOG.info("EMAIL MODEL IS : " + email);
                    if (email != null) {
                        event.setText(text);
                        final ArrayList<CsTicketEmailModel> emails = new ArrayList<CsTicketEmailModel>();
                        emails.addAll(event.getEmails());
                        emails.add(email);
                        event.setEmails(emails);
                    }
                }
            }
            this.getModelService().save(event);
        }
    }

    private List<CsTicketEventEmailConfigurationModel> getEmailConfiguration(final CsTicketModel ticket,
        final CsTicketEventModel event) {
        CsEmailRecipients recepientType = null;
        if (ticket.isCustomerRegistrationTicket()) {
            recepientType = CsEmailRecipients.ASSIGNEDGROUP;
            final ResourceBundle javaBundle = ResourceBundle.getBundle("localization.thyssenkruppeucore-locales",
                getI18nService().getCurrentLocale(),
                getClass().getClassLoader());
            if (StringUtil.isNotEmpty(ticket.getCustomerNumber())) {
                event.setText(javaBundle.getString("ticket.registration.existing.customer.email.body"));
            } else {
                event.setText(javaBundle.getString("ticket.registration.new.customer.email.body"));
            }
        } else if (event instanceof CsTicketResolutionEventModel
            && CsInterventionType.PRIVATE.equals(((CsTicketResolutionEventModel) event).getInterventionType())) {
            recepientType = CsEmailRecipients.ASSIGNEDAGENT;
        }

        final List<CsTicketEventEmailConfigurationModel> filteredConfigurations = this.getApplicableConfigs(event, recepientType);
        return filteredConfigurations;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }
}

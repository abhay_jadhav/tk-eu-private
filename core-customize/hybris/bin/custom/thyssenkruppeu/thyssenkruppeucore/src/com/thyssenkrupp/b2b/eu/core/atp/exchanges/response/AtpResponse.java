package com.thyssenkrupp.b2b.eu.core.atp.exchanges.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AtpResponse implements Serializable {

    @JsonProperty("Line_Item_Number")
    private String lineItemNumber;
    @JsonProperty("Plant")
    private String plant;
    @JsonProperty("Material_Number")
    private String materialNumber;
    @JsonProperty("Storage_Location")
    private String storageLocation;
    @JsonProperty("Stock_Type")
    private String stockType;
    @JsonProperty("Quantity")
    private String quantity;
    @JsonProperty("Unit_Of_Measure")
    private String unitOfMeasure;
    @JsonProperty("Cumulative_Order_Quantity")
    private String cumulativeOrderQuantity;
    @JsonProperty("Sales_Unit")
    private String salesUnit;
    @JsonProperty("Batch_Indicator")
    private String batchIndicator;
    @JsonProperty("Delivery_Date")
    private String deliveryDate;
    @JsonProperty("Sequence_Number")
    private String sequenceNumber;
    @JsonProperty("Material_Availability_Date")
    private String materialAvailabilityDate;

    @JsonProperty("Line_Item_Number")
    public String getLineItemNumber() {
        return lineItemNumber;
    }

    @JsonProperty("Line_Item_Number")
    public void setLineItemNumber(String lineItemNumber) {
        this.lineItemNumber = lineItemNumber;
    }

    @JsonProperty("Plant")
    public String getPlant() {
        return plant;
    }

    @JsonProperty("Plant")
    public void setPlant(String plant) {
        this.plant = plant;
    }

    @JsonProperty("Material_Number")
    public String getMaterialNumber() {
        return materialNumber;
    }

    @JsonProperty("Material_Number")
    public void setMaterialNumber(String materialNumber) {
        this.materialNumber = materialNumber;
    }

    @JsonProperty("Storage_Location")
    public String getStorageLocation() {
        return storageLocation;
    }

    @JsonProperty("Storage_Location")
    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    @JsonProperty("Stock_Type")
    public String getStockType() {
        return stockType;
    }

    @JsonProperty("Stock_Type")
    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    @JsonProperty("Quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("Quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("Unit_Of_Measure")
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    @JsonProperty("Unit_Of_Measure")
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    @JsonProperty("Cumulative_Order_Quantity")
    public String getCumulativeOrderQuantity() {
        return cumulativeOrderQuantity;
    }

    @JsonProperty("Cumulative_Order_Quantity")
    public void setCumulativeOrderQuantity(String cumulativeOrderQuantity) {
        this.cumulativeOrderQuantity = cumulativeOrderQuantity;
    }

    @JsonProperty("Sales_Unit")
    public String getSalesUnit() {
        return salesUnit;
    }

    @JsonProperty("Sales_Unit")
    public void setSalesUnit(String salesUnit) {
        this.salesUnit = salesUnit;
    }

    @JsonProperty("Batch_Indicator")
    public String getBatchIndicator() {
        return batchIndicator;
    }

    @JsonProperty("Batch_Indicator")
    public void setBatchIndicator(String batchIndicator) {
        this.batchIndicator = batchIndicator;
    }

    @JsonProperty("Delivery_Date")
    public String getDeliveryDate() {
        return deliveryDate;
    }

    @JsonProperty("Delivery_Date")
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @JsonProperty("Sequence_Number")
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("Sequence_Number")
    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @JsonProperty("Material_Availability_Date")
    public String getMaterialAvailabilityDate() {
        return materialAvailabilityDate;
    }

    @JsonProperty("Material_Availability_Date")
    public void setMaterialAvailabilityDate(String materialAvailabilityDate) {
        this.materialAvailabilityDate = materialAvailabilityDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Line_Item_Number", lineItemNumber).append("Plant", plant).append("Material_Number", materialNumber)
            .append("Storage_Location", storageLocation).append("Stock_Type", stockType).append("Quantity", quantity).append("Unit_Of_Measure", unitOfMeasure)
            .append("Cumulative_Order_Quantity", cumulativeOrderQuantity).append("Sales_Unit", salesUnit).append("Batch_Indicator", batchIndicator)
            .append("Delivery_Date", deliveryDate).append("Sequence_Number", sequenceNumber).append("Material_Availability_Date", materialAvailabilityDate).toString();
    }
}

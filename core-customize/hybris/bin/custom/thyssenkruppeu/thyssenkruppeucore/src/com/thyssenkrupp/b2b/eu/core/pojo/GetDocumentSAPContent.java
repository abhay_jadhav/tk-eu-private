package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentSAPContent implements Serializable {

    @JsonProperty("Content_Length")
    private String contentLength;

    @JsonProperty("Document_Content")
    private String documentContent;

    @JsonProperty("Status_Message")
    private String statusMessage;

    @JsonProperty("Content_Type")
    private String contentType;

    @JsonProperty("Content_Length")
    public String getContentLength() {
        return contentLength;
    }

    @JsonProperty("Content_Length")
    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    @JsonProperty("Document_Content")
    public String getDocumentContent() {
        return documentContent;
    }

    @JsonProperty("Document_Content")
    public void setDocumentContent(String documentContent) {
        this.documentContent = documentContent;
    }

    @JsonProperty("Status_Message")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("Status_Message")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("Content_Type")
    public String getContentType() {
        return contentType;
    }

    @JsonProperty("Content_Type")
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "ClassPojo [Content_Length = " + contentLength + ", Document_Content = " + documentContent + ", Status_Message = " + statusMessage + ", Content_Type = " + contentType + "]";
    }
}

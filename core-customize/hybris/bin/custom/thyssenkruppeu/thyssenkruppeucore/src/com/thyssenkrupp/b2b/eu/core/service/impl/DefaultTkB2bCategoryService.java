package com.thyssenkrupp.b2b.eu.core.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.LENGTH_VARIANT_CATEGORY_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TK_EU_CATEGORY_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TRADE_LENGTH_CATEGORY_CODE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;
import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;
import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.pojo.VariantCategoryCreationParameter;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;



public class DefaultTkB2bCategoryService extends DefaultCategoryService implements TkB2bCategoryService {

    private static final Logger LOG = Logger.getLogger(DefaultTkB2bCategoryService.class);
    private static final String TRADE_LENGTH_CONFIGURATION_CODE = "tradelength";
    private static final List<String> TRADE_LENGTH_VARIANT_CATEGORY_CODES = Arrays.asList("VC_Trade_Length", "tk-TRADELENGTH");

    private static final String VARIANT_VALUES = "variantvalues";
    private static final String CODE_FIELD = "code";
    private static final String NAME_FIELD = "name";
    private static final String VALUE_FIELD = "value";

    private TkEuB2bCategoryDao tkCategoryDao;
    private              I18NService                 i18nService;
    private              CommonI18NService           commonI18NService;
    private              CategoryService             categoryService;
    private              ConfiguratorSettingsService configuratorSettingsService;
    private              CatalogVersionService       catalogVersionService;
    private              ClassificationSystemService classificationSystemService;

    @Override
    public VariantValueCategoryModel createVariantValueCategory(final Feature feature, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        VariantValueCategoryModel variantValueCategory = null;
        final String variantValueCodePattern = getFeatureValue(feature.getValue());
        final CategoryModel category = getCategoryForCode(variantValueCodePattern, parentCategory, catalogVersion);
        if (category == null) {
            variantValueCategory = setVariantValueCategoryData(variantValueCodePattern, feature, parentCategory, catalogVersion);
            LOG.debug("Created Variant Value Category: " + variantValueCategory.getCode() + " with feature Value " + variantValueCodePattern + ",  superCategory: " + parentCategory.getCode() + " and Sequence: " + variantValueCategory.getSequence());
        } else {
            variantValueCategory = (VariantValueCategoryModel) category;
            variantValueCategory.setSupercategories(Collections.singletonList(parentCategory));
            updateVariantValueCategoryNamebyLocale(variantValueCategory, feature);
            LOG.debug("Returned existing Variant Value Category " + variantValueCategory.getCode());
        }
        modelService.saveAll(variantValueCategory, parentCategory);
        return variantValueCategory;
    }

    @Override
    public VariantValueCategoryModel createVariantValueCategoryForTradeLength(final TkTradeLengthConfiguratorSettingsModel setting, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        VariantValueCategoryModel variantValueCategory = null;
        final String variantValueCodePattern = getVariantValueCodePatternForTradeLength(setting);

        final CategoryModel category = getCategoryForCode(variantValueCodePattern, parentCategory, catalogVersion);

        if (category == null) {
            variantValueCategory = setTradeLengthVariantValueCategoryData(variantValueCodePattern, setting, parentCategory, catalogVersion);
            LOG.debug("Created Variant Value Category: " + variantValueCategory.getCode() + " with Trade Length: " + variantValueCodePattern + ",  superCategory: " + parentCategory.getCode() + " and Sequence: " + variantValueCategory.getSequence());
        } else {
            variantValueCategory = (VariantValueCategoryModel) category;
            variantValueCategory.setSupercategories(Collections.singletonList(parentCategory));
            updateTradeLengthVariantValueCategoryNamebyLocale(variantValueCategory, setting);
            LOG.debug("Returned existing Variant Value Category " + variantValueCategory.getCode());
        }
        modelService.saveAll(variantValueCategory, parentCategory);
        return variantValueCategory;
    }

    @Override
    public VariantValueCategoryModel createVariantValueCategoryForConfigurableLength(final TkLengthConfiguratorSettingsModel setting, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(setting, "Parameter 'setting' cannot be null");
        VariantValueCategoryModel variantValueCategory;
        final String variantValueCodePattern = String.valueOf(setting.getLengthValue());
        final CategoryModel category = getCategoryForCode(variantValueCodePattern, parentCategory, catalogVersion);
        if (category == null) {
            variantValueCategory = createConfigurableLengthVariantValueCategory(variantValueCodePattern, setting, parentCategory, catalogVersion);
            LOG.debug("Created Variant Value Category: " + variantValueCategory.getCode() + " with Length: " + variantValueCodePattern + ",  superCategory: " + parentCategory.getCode() + " and Sequence: " + variantValueCategory.getSequence());
        } else {
            variantValueCategory = (VariantValueCategoryModel) category;
            variantValueCategory.setSupercategories(Collections.singletonList(parentCategory));
            updateLengthVariantValueCategoryNameByLocale(variantValueCategory, setting);
            LOG.debug("Returned existing Variant Value Category " + variantValueCategory.getCode());
        }
        getModelService().saveAll(variantValueCategory, parentCategory);
        return variantValueCategory;
    }

    @Override
    public String getVariantValueCodePatternForTradeLength(@NotNull final Object productInfo) {
        if (productInfo instanceof TkTradeLengthConfiguredProductInfoModel) {
            return getVariantValueCodePatternForTradeLengthFromProductInfo((TkTradeLengthConfiguredProductInfoModel) productInfo);
        } else if (productInfo instanceof TkTradeLengthConfiguratorSettingsModel) {
            return getVariantValueCodePatternForTradeLengthSettingsModel((TkTradeLengthConfiguratorSettingsModel) productInfo);
        }
        return "";
    }

    private String getVariantValueCodePatternForTradeLengthFromProductInfo(final TkTradeLengthConfiguredProductInfoModel productInfo) {
        // Used EN as default since during cronjob category creation the default will be EN not the
        // Store Specific locales like de_DE
        final String displayValue = productInfo.getDisplayValue(Locale.ENGLISH);
        if (isBlank(displayValue)) {
            for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
                final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
                final String displayName = productInfo.getDisplayValue(locale);
                if (isNotBlank(displayName)) {
                    return displayName + productInfo.getUnit().getCode();
                }
            }
        }

        return displayValue + productInfo.getUnit().getCode();
    }

    private String getVariantValueCodePatternForTradeLengthSettingsModel(final TkTradeLengthConfiguratorSettingsModel setting) {
        // Used EN as default since during cronjob category creation the default will be EN not the
        // Store Specific locales like de_DE
        final String displayValue = setting.getDisplayValue(Locale.ENGLISH) + setting.getUnit().getCode();
        if (isBlank(displayValue)) {
            for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
                final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
                final String displayName = setting.getDisplayValue(locale);
                if (isNotBlank(displayName)) {
                    return displayName + setting.getUnit().getCode();
                }
            }
        }

        return displayValue;
    }

    @Override
    public boolean isTradeLengthVariantCategory(final CategoryModel categoryModel) {
        validateParameterNotNull(categoryModel, "Parameter 'categoryModel' was null.");
        if (isNotEmpty(categoryModel.getCode()) && categoryModel instanceof VariantCategoryModel) {
            return isTradeLengthVariantCategory(categoryModel.getCode()) || TRADE_LENGTH_VARIANT_CATEGORY_CODES.contains(categoryModel.getCode());
        }
        return false;
    }

    private CategoryModel getCategoryForCode(final String variantValueCodePattern, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        try {
            return getCategoryForCode(catalogVersion, buildVariantValueCategoryCode(variantValueCodePattern, parentCategory));
        } catch (final Exception e) {
            LOG.debug("No category " + variantValueCodePattern + " found. Ignoring exception.", e);
            return null;
        }
    }

    String getFeatureValue(final FeatureValue value) {
        return getClassificationAttributeValueName(value, null);
    }

    String getFeatureValueNameByLocale(final FeatureValue featureValue, final Locale locale) {
        return getClassificationAttributeValueName(featureValue, locale);
    }

    String getClassificationAttributeValueName(final FeatureValue featureValue, final Locale locale) {
        String attributeValueName = StringUtils.EMPTY;
        if (featureValue != null) {
            final Object featureValueObject = featureValue.getValue();
            if (featureValueObject instanceof ClassificationAttributeValueModel) {
                final ClassificationAttributeValueModel attributeValueModel = (ClassificationAttributeValueModel) featureValueObject;
                if (locale == null) {
                    attributeValueName = attributeValueModel.getName();
                } else {
                    attributeValueName = attributeValueModel.getName(locale);
                }
                LOG.debug(" Feature Value from ClassificationAttributeValueModel: " + attributeValueName);
            } else {
                attributeValueName = featureValueObject.toString();
                LOG.debug(" No ClassificationAttributeValueModel Found for feature. Using Unlocalised Feature Value  " + attributeValueName);
            }
        }
        return attributeValueName;
    }

    @Override
    public String buildVariantValueCategoryCode(final String variantValueCodePattern, final VariantCategoryModel parentCategory) {
        return ThyssenkruppeucoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + tkCategoryDao.getVariantCategoryCodeKey(parentCategory.getCode()) + ThyssenkruppeucoreConstants.CHARACTER_UNDERSCORE + variantValueCodePattern;
    }

    private String buildVariantCategoryCode(final String code) {
        return ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX + code;
    }

    private VariantValueCategoryModel setTradeLengthVariantValueCategoryData(final String variantValueCodePattern, final TkTradeLengthConfiguratorSettingsModel setting, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        final VariantValueCategoryModel variantValueCategory = setMandatoryFields(parentCategory, catalogVersion, variantValueCodePattern);
        updateTradeLengthVariantValueCategoryNamebyLocale(variantValueCategory, setting);
        return variantValueCategory;
    }

    private VariantValueCategoryModel createConfigurableLengthVariantValueCategory(final String variantValueCodePattern, final TkLengthConfiguratorSettingsModel setting, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        final VariantValueCategoryModel variantValueCategory = setMandatoryFields(parentCategory, catalogVersion, variantValueCodePattern);
        updateLengthVariantValueCategoryNameByLocale(variantValueCategory, setting);
        return variantValueCategory;
    }

    private VariantValueCategoryModel setVariantValueCategoryData(final String variantValueCodePattern, final Feature feature, final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        final VariantValueCategoryModel variantValueCategory = setMandatoryFields(parentCategory, catalogVersion, variantValueCodePattern);
        updateVariantValueCategoryNamebyLocale(variantValueCategory, feature);
        return variantValueCategory;
    }

    private VariantValueCategoryModel setMandatoryFields(final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion, final String variantValueCodePattern) {
        final VariantValueCategoryModel variantValueCategory = modelService.create(VariantValueCategoryModel.class);
        variantValueCategory.setCode(buildVariantValueCategoryCode(variantValueCodePattern, parentCategory));
        variantValueCategory.setSequence(Integer.valueOf(generateSequenceNumber(parentCategory, catalogVersion)));
        variantValueCategory.setCatalogVersion(catalogVersion);
        variantValueCategory.setSupercategories(Lists.<CategoryModel>newArrayList(parentCategory));
        return variantValueCategory;
    }

    private void updateVariantValueCategoryNamebyLocale(final VariantValueCategoryModel category, final Feature feature) {
        setFeatureValueByLocale(feature, category);
    }

    private void updateLengthVariantValueCategoryNameByLocale(final VariantValueCategoryModel category, final TkLengthConfiguratorSettingsModel setting) {
        setTradeLengthValueByLocale(setting, category);
    }

    private void updateTradeLengthVariantValueCategoryNamebyLocale(final VariantValueCategoryModel category, final TkTradeLengthConfiguratorSettingsModel setting) {
        setTradeLengthValueByLocale(setting, category);
    }

    private void setTradeLengthValueByLocale(final TkTradeLengthConfiguratorSettingsModel setting, final VariantValueCategoryModel category) {
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            final String displayName = setting.getDisplayValue(locale);
            if (StringUtils.isEmpty(displayName)) {
                LOG.debug("Trade Length Value empty for: " + locale.toString() + " .Finding fallback locale.");
                final Locale fallbackLocale = getFallBackLocale(locale, lang);
                category.setName(setting.getDisplayValue(fallbackLocale), locale);
            } else {
                LOG.debug("Feature Value for: " + locale.toString() + " : " + displayName);
                category.setName(displayName, locale);
            }
        }
    }

    private void setFeatureValueByLocale(final Feature feature, final VariantValueCategoryModel category) {
        final FeatureValue value = feature.getValue();
        final boolean isFeatureOfTypeNumber = isFeatureOfTypeNumber(feature);
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            if (isFeatureOfTypeNumber) {
                category.setName(getNumberFeatureValue(value, locale), locale);
            } else {
                final String featureValueByLocale = getFeatureValueNameByLocale(value, locale);
                if (StringUtils.isEmpty(featureValueByLocale)) {
                    LOG.debug("Feature Value empty for: " + locale.toString() + " .Finding fallback locale.");
                    final Locale fallbackLocale = getFallBackLocale(locale, lang);
                    category.setName(getFeatureValueNameByLocale(value, fallbackLocale), locale);
                } else {
                    LOG.debug("Feature Value for: " + locale.toString() + " : " + featureValueByLocale);
                    category.setName(featureValueByLocale, locale);
                }
            }
        }
    }

    private boolean isFeatureOfTypeNumber(final Feature feature) {
        return isFeatureOfTypeNumberPredicate().test(feature) && isFeatureValueOfTypeNumberPredicate().test(feature.getValue());
    }

    private Predicate<Feature> isFeatureOfTypeNumberPredicate() {
        return feature -> nonNull(feature.getClassAttributeAssignment()) && ClassificationAttributeTypeEnum.NUMBER.equals(feature.getClassAttributeAssignment().getAttributeType());
    }

    private Predicate<FeatureValue> isFeatureValueOfTypeNumberPredicate() {
        return featureValue -> nonNull(featureValue) && nonNull(featureValue.getValue()) && (featureValue.getValue() instanceof Number);
    }

    private String getNumberFeatureValue(final FeatureValue featureValue, final Locale locale) {
        final NumberFormat numberFormat = getNumberFormatForLocale(locale);
        final String formattedValue = numberFormat.format(featureValue.getValue());
        LOG.debug(String.format("Formatted Numeric value from:%s to:%s for locale:%s", featureValue.getValue(), formattedValue, locale));
        return formattedValue;
    }

    private NumberFormat getNumberFormatForLocale(final Locale locale) {
        try {
            final NumberFormat numberFormat = nonNull(locale) ? NumberFormat.getInstance(locale) : null;
            if (nonNull(numberFormat)) {
                return numberFormat;
            }
        } catch (final Exception e) {
            LOG.error("Error in finding NumberFormat for locale: " + locale, e);
        }
        LOG.debug(String.format("No NumberFormat found for locale:%s, Using Fallback(US) NumberFormat instead", locale));
        return NumberFormat.getInstance(Locale.US);
    }

    int generateSequenceNumber(final VariantCategoryModel superCategory, final CatalogVersionModel catalogVersion) {
        final Collection<VariantValueCategoryModel> variantValueCategories = tkCategoryDao.findCategoriesBySuperCategory(superCategory, catalogVersion);
        final int maxSequenceNumber = variantValueCategories.isEmpty() ? 0 : variantValueCategories.stream().mapToInt(VariantValueCategoryModel::getSequence).max().getAsInt();
        return maxSequenceNumber + 1;
    }

    @Override
    public VariantCategoryModel createVariantCategory(final VariantProductModel variant, final String code, final CatalogVersionModel catalogVersion, final Feature feature) {
        VariantCategoryModel variantCategory = null;
        CategoryModel category = null;
        try {
            category = getCategoryForCode(catalogVersion, buildVariantCategoryCode(code));
        } catch (final Exception e) {
            LOG.debug("No category " + code + " found and will create new one. Ignoring exception.", e);
        }
        if (category == null) {
            variantCategory = setVariantCategoryData(variant, feature, code, catalogVersion);
            LOG.debug("Created new Variant Category for " + variantCategory.getCode());
        } else {
            variantCategory = (VariantCategoryModel) category;
            setVariantCategoryName(variant, variantCategory, feature, code);
            LOG.debug("Returned existing Variant Category for " + category.getCode());
        }
        modelService.save(variantCategory);
        return variantCategory;
    }

    @Override
    public VariantCategoryModel getOrCreateVariantCategory(final VariantCategoryCreationParameter parameter) {
        validateParameterNotNull(parameter, "parameter was null");
        final VariantCategoryModel variantCategory;
        final String vcCode = buildVariantCategoryCode(parameter.getClassCode(), parameter.getAttributeCode());
        final Optional<VariantCategoryModel> maybeVariantCategory = getVariantCategoryForCode(parameter.getCatalogVersionModel(), vcCode);
        if (maybeVariantCategory.isPresent()) {
            LOG.debug("VariantCategory already exists for code:" + vcCode);
            variantCategory = maybeVariantCategory.get();
        } else {
            LOG.debug("Creating New VariantCategory for code:" + vcCode);
            variantCategory = createVariantCategory(vcCode, parameter.getCatalogVersionModel());
        }
        populateVariantCategoryName(variantCategory, parameter);
        getModelService().save(variantCategory);
        return variantCategory;
    }

    @Override
    public void buildVariantCategoryHierarchy(final List<VariantCategoryModel> variantCategories) {
        if (CollectionUtils.isNotEmpty(variantCategories)) {
            setSuperCategoriesAsEmptyForFirstCategory((LinkedList<VariantCategoryModel>) variantCategories);
            for (int i = 1; i < variantCategories.size(); i++) {
                final VariantCategoryModel category = variantCategories.get(i);
                final VariantCategoryModel superCategory = variantCategories.get(i - 1);
                final Collection<CategoryModel> variantCategoriesOfSuperCategory = filterCategoriesOfType(superCategory.getCategories(), VariantCategoryModel.class);
                if (CollectionUtils.isNotEmpty(variantCategoriesOfSuperCategory)) {
                    LOG.debug("SuperCategory:" + superCategory.getCode() + " already has sub category assigned to it, Existing VC SubCategories will be removed");
                    final List<CategoryModel> newCategories = new ArrayList<>(superCategory.getCategories());
                    newCategories.removeAll(variantCategoriesOfSuperCategory);
                    newCategories.add(category);
                    superCategory.setCategories(newCategories);
                }
                LOG.debug(String.format("Setting superCategory:%s for VariantCategory:%s", superCategory.getCode(), category.getCode()));
                category.setSupercategories(Collections.singletonList(superCategory));
            }
            retainVvcSubCategoriesInLastCategory((LinkedList<VariantCategoryModel>) variantCategories);
            getModelService().saveAll(variantCategories);
        }
    }

    @Override
    public String buildVariantCategoryCode(final String classCode, final String attributeCode) {
        final String vcCode = classCode + ThyssenkruppeucoreConstants.CHARACTER_HYPHEN + attributeCode;
        return ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX + vcCode;
    }

    @Override
    public Collection<VariantCategoryModel> getAllVariantCategoriesWithCode(final String ruleCode, final CatalogVersionModel catalogVersion) {
        return getTkCategoryDao().findAllVariantCategories(ruleCode, catalogVersion);
    }

    @Override
    public Collection<VariantValueCategoryModel> getAllVariantValueCategoriesWithCode(final String ruleCode, final CatalogVersionModel catalogVersion) {
        return getTkCategoryDao().findAllVariantValueCategories(ruleCode, catalogVersion);
    }

    @Override
    public <T extends CategoryModel> Collection<T> getAllCategoriesWithoutIncludedProducts(final String ruleCode, final CatalogVersionModel catalogVersion) {
        return getTkCategoryDao().findAllCategoriesWithoutIncludedProducts(ruleCode, catalogVersion);
    }

    public CatalogVersionModel getCatalogVersionBySalesOrganization(final String salesOrganization) {
        validateParameterNotNullStandardMessage("salesOrganization", salesOrganization);
        return getTkCategoryDao().fetchProductCatalogBySalesOrganization(salesOrganization);
    }

    @Override
    public Collection<CategoryModel> filterCategoriesOfType(final Collection<CategoryModel> categories, final Class classType) {
        return emptyIfNull(categories).stream().filter(classType::isInstance).collect(Collectors.toList());
    }

    @Override
    public boolean isTradeLengthVariantCategory(final String categoryCode) {
        if (StringUtils.isNotEmpty(categoryCode)) {
            return categoryCode.startsWith(ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX) && categoryCode.endsWith(TRADE_LENGTH_CATEGORY_CODE);
        }
        return false;
    }

    @Override
    public boolean isLengthVariantCategory(final String categoryCode) {
        return StringUtils.isNotEmpty(categoryCode) && categoryCode.startsWith(ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX) && categoryCode.endsWith(LENGTH_VARIANT_CATEGORY_CODE);
    }

    @Override
    public void removeCategories(final Collection<CategoryModel> categories) {
        if (CollectionUtils.isNotEmpty(categories)) {
            getModelService().removeAll(categories);
        }
    }

    @Override
    public void setSubCategoriesAsEmpty(final Collection<CategoryModel> categories) {
        if (CollectionUtils.isNotEmpty(categories)) {
            categories.forEach(o -> o.setCategories(Collections.emptyList()));
            getModelService().saveAll(categories);
        }
    }

    private void populateVariantCategoryName(final VariantCategoryModel variantCategory, final VariantCategoryCreationParameter parameter) {
        final String attributeCode = parameter.getAttributeCode();
        if (StringUtils.equals(TRADE_LENGTH_CATEGORY_CODE, attributeCode)) {
            LOG.debug("Setting Name for TradeLength VariantCategory: " + variantCategory.getCode());
            final CategoryModel tradeLengthCategory = getRootTradeLengthCategory();
            setNameByLocaleFromCategory(variantCategory, tradeLengthCategory);
        } else {
            final Optional<ClassificationAttributeModel> maybeAttribute = getClassificationAttributeForCode(parameter.getClassificationClass(), parameter.getAttributeCode());
            if (maybeAttribute.isPresent()) {
                final ClassificationAttributeModel attribute = maybeAttribute.get();
                LOG.debug("Setting Name for VariantCategory: " + variantCategory.getCode() + " using ClassificationAttribute:" + attribute.getCode());
                setVariantCategoryNameFromClassificationAttribute(variantCategory, attribute);
            } else {
                LOG.debug("Setting Name for VariantCategory: " + variantCategory.getCode() + " using attributeCode:" + attributeCode);
                variantCategory.setName(attributeCode, getI18nService().getCurrentLocale());
            }
        }
    }

    private Optional<ClassificationAttributeModel> getClassificationAttributeForCode(final ClassificationClassModel classModel, final String attributeCode) {
        ClassificationAttributeModel resultingAttribute = null;
        try {
            resultingAttribute = getClassificationSystemService().getAttributeForCode(classModel.getCatalogVersion(), attributeCode);
        } catch (final Exception e) {
            LOG.warn("No ClassificationAttribute found for given attributeCode:" + attributeCode + ", Ignoring exception ");
        }
        return Optional.ofNullable(resultingAttribute);
    }

    private void setVariantCategoryNameFromClassificationAttribute(final VariantCategoryModel variantCategory, final ClassificationAttributeModel attribute) {
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            final String attributeName = attribute.getName(locale);
            if (StringUtils.isEmpty(attributeName)) {
                final Locale fallbackLocale = getFallBackLocale(locale, lang);
                variantCategory.setName(attribute.getName(fallbackLocale), locale);
            } else {
                variantCategory.setName(attributeName, locale);
            }
        }
    }

    private void setSuperCategoriesAsEmptyForFirstCategory(final LinkedList<VariantCategoryModel> list) {
        list.getFirst().setSupercategories(Collections.emptyList());
    }

    private void retainVvcSubCategoriesInLastCategory(final LinkedList<VariantCategoryModel> list) {
        final CategoryModel lastCategory = list.getLast();
        final Collection<CategoryModel> vvcs = emptyIfNull(lastCategory.getCategories().stream().filter(VariantValueCategoryModel.class::isInstance).collect(Collectors.toList()));
        lastCategory.setCategories((List<CategoryModel>) vvcs);
    }

    private Optional<VariantCategoryModel> getVariantCategoryForCode(final CatalogVersionModel catalogVersion, final String code) {
        VariantCategoryModel variantCategory = null;
        try {
            variantCategory = (VariantCategoryModel) getCategoryForCode(catalogVersion, code);
        } catch (final Exception e) {
            LOG.debug("No VariantCategory found for code:" + code + " Ignoring exception.");
        }
        return Optional.ofNullable(variantCategory);
    }

    private VariantCategoryModel createVariantCategory(final String vcCode, final CatalogVersionModel catalogVersion) {
        final VariantCategoryModel variantCategory = getModelService().create(VariantCategoryModel.class);
        variantCategory.setCode(vcCode);
        variantCategory.setCatalogVersion(catalogVersion);
        return variantCategory;
    }

    private CategoryModel getRootTradeLengthCategory() {
        final CatalogVersionModel storeCatalog = getCatalogVersionService().getCatalogVersion(TK_EU_CATEGORY_CATALOG_ID, CatalogManager.OFFLINE_VERSION);
        return getCategoryForCode(storeCatalog, TRADE_LENGTH_CONFIGURATION_CODE);
    }

    private VariantCategoryModel setVariantCategoryData(final VariantProductModel variant, final Feature feature, final String code, final CatalogVersionModel catalogVersion) {
        final VariantCategoryModel variantCategory = modelService.create(VariantCategoryModel.class);
        variantCategory.setCode(buildVariantCategoryCode(code));
        setVariantCategoryName(variant, variantCategory, feature, code);
        variantCategory.setCatalogVersion(catalogVersion);
        return variantCategory;
    }

    public void setVariantCategoryName(final VariantProductModel variant, final VariantCategoryModel category, final Feature feature, final String code) {
        if (StringUtils.equals(TRADE_LENGTH_CATEGORY_CODE, code)) {
            setVariantCategoryNameForTradeLength(variant, category);
        } else if (feature != null && StringUtils.isNotEmpty(feature.getName())) {
            LOG.debug("Variant Category: " + category.getCode() + "set with Feature Name:" + feature.getName());
            setNameByLocaleFromFeature(category, feature);
        } else {
            category.setName(code, getI18nService().getCurrentLocale());
        }
    }

    private void setVariantCategoryNameForTradeLength(final VariantProductModel variant, final VariantCategoryModel variantCategory) {
        final List<TkTradeLengthConfiguratorSettingsModel> settings = getTradeLengthSettingConfigurations(variant);

        if (CollectionUtils.isNotEmpty(settings) && CollectionUtils.size(settings) > 0) {
            final ConfigurationCategoryModel tradeLengthConfigurationCategory = settings.get(0).getConfigurationCategory();
            if (CollectionUtils.isNotEmpty(tradeLengthConfigurationCategory.getSupercategories())) {
                final CategoryModel tradeLengthRootCategory = tradeLengthConfigurationCategory.getSupercategories().get(0);
                setNameByLocaleFromCategory(variantCategory, tradeLengthRootCategory);
            }
        } else {
            LOG.debug("Incorrect Tradelength configurations for Variant. Variant Category Name will not be updated" + variant.getCode());
        }
    }

    private void setNameByLocaleFromCategory(final VariantCategoryModel variantCategory, final CategoryModel category) {
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            if (StringUtils.isEmpty(category.getName(locale))) {
                LOG.debug("Name is Empty for : " + category.getName() + " for locale " + locale.toString() + " Looking for a fall back locale");
                final Locale fallbackLocale = getFallBackLocale(locale, lang);
                variantCategory.setName(category.getName(fallbackLocale), locale);
            } else {
                variantCategory.setName(category.getName(locale), locale);
            }
        }
    }

    private void setNameByLocaleFromFeature(final VariantCategoryModel variantCategory, final Feature feature) {
        for (final LanguageModel lang : getCommonI18NService().getAllLanguages()) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            if (feature.getClassAttributeAssignment() == null) {
                LOG.warn("ClassAttributeAssignment missing for Feature: " + feature.getName());
                continue;
            }
            final ClassificationAttributeModel attribute = feature.getClassAttributeAssignment().getClassificationAttribute();
            if (attribute != null) {
                if (StringUtils.isEmpty(attribute.getName(locale))) {
                    LOG.debug("Name is Empty for : " + attribute.getName() + " for locale " + locale.toString() + " Looking for a fall back locale");
                    final Locale fallbackLocale = getFallBackLocale(locale, lang);
                    variantCategory.setName(attribute.getName(fallbackLocale), locale);
                } else {
                    variantCategory.setName(attribute.getName(locale), locale);
                }
            }
        }
    }

    private Locale getFallBackLocale(final Locale locale, final LanguageModel language) {
        final List<LanguageModel> languages = language.getFallbackLanguages();
        if (CollectionUtils.isNotEmpty(languages) && CollectionUtils.size(languages) > 0) {
            LOG.debug("Fall back language found for: " + locale.toString() + " Using : " + languages.get(0).getIsocode());
            return getCommonI18NService().getLocaleForLanguage(languages.get(0));
        } else {
            LOG.debug("No Fall back language found for: " + locale.toString() + " Setting Default Locale:" + Locale.GERMAN.toString());
            return Locale.GERMAN;
        }
    }

    @Override
    public List<TkTradeLengthConfiguratorSettingsModel> getTradeLengthSettingConfigurations(final VariantProductModel variant) {
        final List<TkTradeLengthConfiguratorSettingsModel> tradeLengthConfigurationSettings = new ArrayList<>();
        for (final AbstractConfiguratorSettingModel config : getConfiguratorSettingsService().getConfiguratorSettingsForProduct(variant)) {
            if (config != null && config instanceof TkTradeLengthConfiguratorSettingsModel) {
                tradeLengthConfigurationSettings.add((TkTradeLengthConfiguratorSettingsModel) config);
            }
        }
        return tradeLengthConfigurationSettings;
    }

    @Override
    public void updateVariantInfo(final ProductModel variant, final List<LanguageModel> baseStoreLaguages) {
        if (CollectionUtils.isNotEmpty(variant.getSupercategories())) {
            final List<CategoryModel> categories = (List<CategoryModel>) variant.getSupercategories();
            for (final LanguageModel lang : baseStoreLaguages) {
                final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
                variant.setTkProductInfo(null, locale);
            }
            for (final CategoryModel category : categories) {
                if (category instanceof VariantValueCategoryModel) {
                    final VariantValueCategoryModel variantValueCategoryModel = (VariantValueCategoryModel) category;
                    VariantCategoryModel variantCategory = null;
                    try {
                        final List<CategoryModel> superCategories = variantValueCategoryModel.getSupercategories();
                        // VariantValueCategories have only one VariantCategory
                        if (CollectionUtils.size(superCategories) > 0 && superCategories.get(0) instanceof VariantCategoryModel) {
                            variantCategory = (VariantCategoryModel) superCategories.get(0);
                        }
                    } catch (final Exception e) {
                        LOG.warn("Error finding Variant Category for Value: " + variantValueCategoryModel.getName());
                    }

                    buildTkProductInfoForLocale(variantCategory, variantValueCategoryModel, variant, baseStoreLaguages);
                }
            }
            modelService.save(variant);
            LOG.debug(" Product Info added to " + variant.getCode());
        }
    }

    private void buildTkProductInfoForLocale(final VariantCategoryModel variantCategory, final VariantValueCategoryModel variantValueCategoryModel, final ProductModel variant, final List<LanguageModel> baseStoreLaguages) {
        for (final LanguageModel lang : baseStoreLaguages) {
            final Locale locale = getCommonI18NService().getLocaleForLanguage(lang);
            final String tkProductInfo = variant.getTkProductInfo(locale);
            final StringBuilder productInfo = StringUtils.isEmpty(tkProductInfo) ? buildProductInfoHeader() : new StringBuilder(tkProductInfo);
            final int insertAt = productInfo.lastIndexOf("]}");
            final StringBuilder contentProductInfo = StringUtils.isEmpty(tkProductInfo) ? new StringBuilder() : new StringBuilder(",");
            buildProductInfoContent(contentProductInfo, variantCategory, variantValueCategoryModel, locale);
            productInfo.insert(insertAt, contentProductInfo);
            variant.setTkProductInfo(productInfo.toString(), locale);
        }
    }

    private StringBuilder buildProductInfoHeader() {
        return new StringBuilder("{\"" + VARIANT_VALUES + "\": []}");
    }

    private void buildProductInfoContent(final StringBuilder bodyProductInfo, final VariantCategoryModel variantCategory, final VariantValueCategoryModel variantValueCategoryModel, final Locale locale) {
        bodyProductInfo.append("{");
        bodyProductInfo.append("\"" + CODE_FIELD + "\":");
        bodyProductInfo.append("\"" + variantCategory.getCode() + "\",");
        bodyProductInfo.append("\"" + NAME_FIELD + "\":");
        bodyProductInfo.append("\"" + variantCategory.getName(locale) + "\",");
        bodyProductInfo.append("\"" + VALUE_FIELD + "\":");
        bodyProductInfo.append("\"" + variantValueCategoryModel.getName(locale) + "\"");
        bodyProductInfo.append("}");
    }




    public TkEuB2bCategoryDao getTkCategoryDao() {
        return tkCategoryDao;
    }

    public void setTkCategoryDao(final TkEuB2bCategoryDao tkCategoryDao) {
        this.tkCategoryDao = tkCategoryDao;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public ConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    public void setConfiguratorSettingsService(final ConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Override
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        super.setCatalogVersionService(catalogVersionService);
        this.catalogVersionService = catalogVersionService;
    }

    public ClassificationSystemService getClassificationSystemService() {
        return classificationSystemService;
    }

    @Required
    public void setClassificationSystemService(final ClassificationSystemService classificationSystemService) {
        this.classificationSystemService = classificationSystemService;
    }
}

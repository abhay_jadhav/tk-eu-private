package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.impl;

import com.sap.security.core.server.csi.XSSEncoder;
import com.thyssenkrupp.b2b.eu.core.search.data.TkEuCategorySuggestion;
import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.CategorySearchAutocompleteService;
import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.suggester.impl.TkEuSolrAutoSuggestService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.SolrIndexedTypeCodeResolver;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefaultSolrCategoryAutocompleteService implements CategorySearchAutocompleteService<TkEuCategorySuggestion> {

    private static final Logger LOG            = Logger.getLogger(DefaultSolrCategoryAutocompleteService.class);
    private static final String SEPARATOR      = ";";
    private static final String PATH_SEPARATOR = "/";
    private FacetSearchConfigService               facetSearchConfigService;
    private CommonI18NService                      commonI18NService;
    private SolrIndexedTypeCodeResolver            solrIndexedTypeCodeResolver;
    private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
    private TkEuSolrAutoSuggestService             solrAutoSuggestService;
    private CommerceCategoryService                categoryService;

    @Override
    public List<TkEuCategorySuggestion> getCategoryAutocompleteSuggestion(final String input) {
        try {
            final SolrFacetSearchConfigModel solrFacetSearchConfigModel = getSolrFacetSearchConfigSelectionStrategy().getCurrentSolrFacetSearchConfig();
            final FacetSearchConfig facetSearchConfig = getFacetSearchConfigService().getConfiguration(solrFacetSearchConfigModel.getName());
            final Optional<IndexedType> indexedType = getIndexedType(facetSearchConfig);
            return indexedType.isPresent() ? getCategorySuggestions(solrFacetSearchConfigModel, indexedType, input) : Collections.emptyList();
        } catch (final FacetConfigServiceException | NoValidSolrConfigException facetConfigOrNoValidException) {
            LOG.error("Error retrieving CategoryAutocompleteSuggestion suggestions : ", facetConfigOrNoValidException);
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private List<TkEuCategorySuggestion> getCategorySuggestions(final SolrFacetSearchConfigModel solrFacetSearchConfigModel, final Optional<IndexedType> indexedType, final String input) {
        final Optional<SolrIndexedTypeModel> indexedTypeModel = findIndexedTypeModel(solrFacetSearchConfigModel, indexedType);
        if (indexedTypeModel.isPresent()) {
            String encodedSearchTerm = encodeSearchText(input);
            final Map<String, Long> categorySuggestions = getSolrAutoSuggestService().getCategoryAutoSuggestionsForQuery(getCommonI18NService().getCurrentLanguage(), indexedTypeModel.get(), encodedSearchTerm);
            return findSuggestions(categorySuggestions, encodedSearchTerm);
        }
        return Collections.emptyList();
    }

    private String encodeSearchText(final String searchTerm) {
        try {
            return StringUtils.isBlank(searchTerm) ? searchTerm : XSSEncoder.encodeHTML(searchTerm);
        } catch (final UnsupportedEncodingException exception) {
            LOG.error("Error occurred during encoding the search term for category autosuggetion", exception);
        }
        return searchTerm;
    }

    private Optional<IndexedType> getIndexedType(final FacetSearchConfig config) {
        final Optional<IndexConfig> solrIndexConfig = Optional.ofNullable(config.getIndexConfig());
        final Optional<Collection<IndexedType>> solrIndexedTypes = solrIndexConfig.map(indexConfig -> indexConfig.getIndexedTypes().values());
        return solrIndexedTypes.flatMap(indexedTypes -> indexedTypes.stream().findFirst());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<SolrIndexedTypeModel> findIndexedTypeModel(final SolrFacetSearchConfigModel facetSearchConfigModel, final Optional<IndexedType> indexedType) {
        return indexedType.flatMap(indexedType1 -> facetSearchConfigModel.getSolrIndexedTypes().stream().filter(type -> getSolrIndexedTypeCodeResolver().resolveIndexedTypeCode(type).equals(indexedType1.getUniqueIndexedTypeCode())).findFirst());
    }

    private List<TkEuCategorySuggestion> findSuggestions(final Map<String, Long> categorySuggestions, final String input) {
        final String trimmedInput = input.trim();
        final List<TkEuCategorySuggestion> categoryAutoSuggestions = new ArrayList<>();
        final Map<String, Long> filteredCategorySuggestions = getFilteredCategorySuggestions(categorySuggestions, trimmedInput);
        filteredCategorySuggestions.forEach((categorySuggestion, numOfProducts) -> {
            final int semiColonIndex = categorySuggestion.lastIndexOf(SEPARATOR);
            if (isValidLength(semiColonIndex)) {
                final String categoryName = categorySuggestion.substring(0, semiColonIndex);
                buildCatAutoCompleteSuggestion(categorySuggestion, numOfProducts, categoryName, categoryAutoSuggestions);
            }
        });
        return categoryAutoSuggestions;
    }

    private Map<String, Long> getFilteredCategorySuggestions(final Map<String, Long> categorySuggestions, final String trimmedInput) {
        return categorySuggestions.entrySet().stream().filter(categorySuggest -> categorySuggest.getKey().toLowerCase().startsWith(trimmedInput.toLowerCase())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    // build the suggestions
    private void buildCatAutoCompleteSuggestion(final String categorySuggestion, final Long numOfProducts, final String categoryName,
        final List<TkEuCategorySuggestion> categoryAutoSuggestions) {

        final TkEuCategorySuggestion catSuggestion = createCategorySuggestion();
        final String seoUri = categorySuggestion.substring(categoryName.length() + 1, categorySuggestion.length());
        buildCatSuggestion(categoryName, numOfProducts, catSuggestion, seoUri);
        categoryAutoSuggestions.add(catSuggestion);
    }

    private void buildCatSuggestion(final String categoryName, final Long numOfProducts, final TkEuCategorySuggestion catSuggestion, final String seoUri) {
        catSuggestion.setDisplayName(categoryName);
        catSuggestion.setSeoURI(seoUri.replaceAll("\\s+", "-"));
        final Optional<String> categoryCode = getCategoryIdFromURI(seoUri);
        categoryCode.ifPresent(catSuggestion::setCode);
        final Optional<String> categoryMediaPath = getCategoryMediaPath(catSuggestion.getCode());
        categoryMediaPath.ifPresent(catSuggestion::setMediaURI);
        catSuggestion.setNumOfProducts(numOfProducts);
    }

    private Optional<String> getCategoryIdFromURI(final String seoUri) {
        final int forwardSlashIndex = seoUri.lastIndexOf(PATH_SEPARATOR);
        return isValidLength(forwardSlashIndex) ? Optional.of(seoUri.substring(forwardSlashIndex + 1)) : Optional.empty();
    }

    private Optional<String> getCategoryMediaPath(final String code) {
        final Optional<CategoryModel> categoryModelOptional = Optional.ofNullable(getCategoryService().getCategoryForCode(code));
        if (categoryModelOptional.isPresent()) {
            final Optional<MediaModel> categoryMedia = categoryModelOptional.map(CategoryModel::getPicture);
            if (categoryMedia.isPresent()) {
                return Optional.ofNullable(categoryMedia.get().getURL());
            }
        }
        return Optional.empty();
    }

    private boolean isValidLength(final int lengthOfWordOrChar) {
        return lengthOfWordOrChar > 0;
    }

    private TkEuCategorySuggestion createCategorySuggestion() {
        return new TkEuCategorySuggestion();
    }

    protected FacetSearchConfigService getFacetSearchConfigService() {
        return facetSearchConfigService;
    }

    @Required
    public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService) {
        this.facetSearchConfigService = facetSearchConfigService;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public SolrIndexedTypeCodeResolver getSolrIndexedTypeCodeResolver() {
        return solrIndexedTypeCodeResolver;
    }

    @Required
    public void setSolrIndexedTypeCodeResolver(final SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver) {
        this.solrIndexedTypeCodeResolver = solrIndexedTypeCodeResolver;
    }

    protected SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy() {
        return solrFacetSearchConfigSelectionStrategy;
    }

    @Required
    public void setSolrFacetSearchConfigSelectionStrategy(
        final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy) {
        this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
    }

    protected TkEuSolrAutoSuggestService getSolrAutoSuggestService() {
        return solrAutoSuggestService;
    }

    @Required
    public void setSolrAutoSuggestService(TkEuSolrAutoSuggestService solrAutoSuggestService) {
        this.solrAutoSuggestService = solrAutoSuggestService;
    }

    protected CommerceCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(CommerceCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

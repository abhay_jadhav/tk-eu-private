package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.service.ProductComparisonService;

public class DefaultProductComparisonService implements ProductComparisonService {

    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC);
    private static final String ATTRIBUTE_KEY = "attributes";
    private static final Logger LOG = Logger.getLogger(DefaultProductComparisonService.class.getName());


    private ProductFacade productVariantFacade;

    private ClassificationService classificationService;

    private ProductService productService;


    @Override
    public LinkedHashMap<String, String> getComparisonData(final List<String> comparisonProductList, final String[] attributeList) {

        final LinkedHashMap valueMap = new LinkedHashMap<>();

        for (final String productCode : comparisonProductList) {

            populateAttributeValueMap(productCode, valueMap, attributeList);
        }

        return valueMap;
    }

    private void populateAttributeValueMap(final String productCode, final LinkedHashMap valueMap, final String[] attributeList) {
        if (!productAttributesPopulated(productCode.toUpperCase(), valueMap, attributeList)) {
            productAttributesPopulated(productCode.toLowerCase(), valueMap, attributeList);
        }
    }

    private boolean productAttributesPopulated(final String productCode, final LinkedHashMap valueMap, final String[] attributeList) {
        boolean attributesPopulated = false;

        try {
            final ProductModel productModel = productService.getProductForCode(productCode);
            final LinkedList valueList = new LinkedList<>();
            valueMap.put(ATTRIBUTE_KEY, attributeList);
            valueList.add(productVariantFacade.getProductForOptions(productModel, PRODUCT_OPTIONS));

            for (final String attribute : attributeList) {

                final String normilisedAttribute = attribute.trim();
                final boolean isProductAttribute = getProductAttributes(productModel, normilisedAttribute, valueList);
                final boolean isProductFeature = getProductFeatures(productModel, normilisedAttribute, valueList);
                final boolean isClassificationAttribute = getClassificationAttributes(productModel, normilisedAttribute, valueList);

                if (!isProductAttribute && !isProductFeature && !isClassificationAttribute) {
                    valueList.add("");
                }
            }

            valueMap.put(productCode, valueList);

            attributesPopulated = true;
        } catch (final UnknownIdentifierException e) {
            LOG.error("Product #" + productCode + " not found");
        }

        return attributesPopulated;
    }

    private boolean getClassificationAttributes(final ProductModel productModel, final String attribute, final LinkedList valueList) {

        final FeatureList productFeatureList = classificationService.getFeatures(productModel);
        if (!(productFeatureList.isEmpty())) {
            for (final Feature feature : productFeatureList) {
                if (attribute.equalsIgnoreCase(feature.getName())) {
                    valueList.add(feature.getValue().getValue());
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getProductFeatures(final ProductModel productModel, final String attribute, final LinkedList valueList) {
        if (!productModel.getFeatures().isEmpty()) {
            for (final ProductFeatureModel feature : productModel.getFeatures()) {
                if (PropertyUtils.isReadable(feature, attribute)) {
                    valueList.add(feature.getProperty(attribute));
                    return true;
                }
            }
        }
        return false;
    }

    private boolean getProductAttributes(final ProductModel productModel, final String attribute, final LinkedList valueList) {
        if (PropertyUtils.isReadable(productModel, attribute)) {
            valueList.add(productModel.getProperty(attribute));
            return true;
        }
        return false;
    }

    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public void setProductVariantFacade(final ProductFacade productVariantFacade) {
        this.productVariantFacade = productVariantFacade;
    }

    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }
}

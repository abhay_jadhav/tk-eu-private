package com.thyssenkrupp.b2b.eu.core.atp.predicate;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.log4j.Logger;

import java.util.Optional;
import java.util.function.BiPredicate;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

public class SapAtpCheckEnabledPredicate implements BiPredicate<AtpRequestData, BaseStoreModel> {

    private static final Logger LOG = Logger.getLogger(SapAtpCheckEnabledPredicate.class);

    @Override
    public boolean test(final AtpRequestData request, final BaseStoreModel baseStore) {
        final boolean isSapCheckEnabledForStore = isTrue(baseStore.getSapATPCheckEnabled());
        LOG.debug("SapATPCheckEnabled is " + isSapCheckEnabledForStore + " for store:" + baseStore.getUid());
        if (isSapCheckEnabledForStore && isTrue(request.getIsPdpRequest())) {
            return isSapCheckEnabledForPdp(request, baseStore);
        }
        return isSapCheckEnabledForStore;
    }

    private boolean isSapCheckEnabledForPdp(final AtpRequestData request, final BaseStoreModel baseStore) {
        boolean isPdpCheckEnabled = isTrue(baseStore.getSapATPCheckOnPDPEnabled());
        LOG.debug("SapATPCheckOnPDPEnabled is " + isPdpCheckEnabled + " for store:" + baseStore.getUid());
        return isPdpCheckEnabled && isSapCheckEnabledForPdpQuantity(request, baseStore);
    }

    private boolean isSapCheckEnabledForPdpQuantity(final AtpRequestData request, final BaseStoreModel baseStore) {
        boolean isQtyCheckEnabled = isTrue(baseStore.getSapATPCheckOnPDPQtyOneEnabled());
        LOG.debug("SapATPCheckOnPDPQtyOneEnabled is " + isQtyCheckEnabled + " for store:" + baseStore.getUid());
        final long quantity = getQtyFromFirstRequestEntry(request);
        return isQtyCheckEnabled ? quantity > 0 : quantity > 1;
    }

    private long getQtyFromFirstRequestEntry(final AtpRequestData request) {
        return getFirstEntryFromRequest(request).map(AtpRequestEntryData::getQuantity).orElse(0L);
    }

    private Optional<AtpRequestEntryData> getFirstEntryFromRequest(final AtpRequestData request) {
        return emptyIfNull(request.getEntries()).stream().findFirst();
    }
}

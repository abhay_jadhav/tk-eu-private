package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.suggester.impl;

import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.suggester.SolrCategoryAutoSuggestService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.SolrIndexedTypeCodeResolver;
import de.hybris.platform.solrfacetsearch.model.SolrIndexModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexService;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProviderFactory;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import de.hybris.platform.solrfacetsearch.suggester.SolrSuggestion;
import de.hybris.platform.solrfacetsearch.suggester.exceptions.SolrAutoSuggestException;
import de.hybris.platform.solrfacetsearch.suggester.impl.DefaultSolrAutoSuggestService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.springframework.beans.factory.annotation.Required;
import reactor.util.CollectionUtils;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class TkEuSolrAutoSuggestService extends DefaultSolrAutoSuggestService implements SolrCategoryAutoSuggestService {

    private static final Logger    LOG      = Logger.getLogger(TkEuSolrAutoSuggestService.class);
    private static final String    EMPTY    = "";
    private static final Character WILDCARD = '*';
    private String                      categorySuggestName;
    private String                      categorySuggest;
    private String                      defaultLanguageIso;
    private SolrIndexService            solrIndexService;
    private SolrSearchProviderFactory   solrSearchProviderFactory;
    private SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver;
    private FieldNameProvider           fieldNameProvider;

    public SolrSuggestion getAutoSuggestionsForQuery(final LanguageModel language, final SolrIndexedTypeModel solrIndexedType, final String queryInput) throws SolrAutoSuggestException {
        if (StringUtils.isNotBlank(queryInput)) {
            final Optional<SolrClient> solrClient = getSolrClient(solrIndexedType);
            if (solrClient.isPresent()) {
                final SolrQuery solrQuery = getAutoSuggestQueryForSolr(queryInput, language);
                final Optional<Index> index = getIndex(solrIndexedType);
                final Optional<QueryResponse> solrQueryResponse = getSolrQueryResponse(solrClient, solrQuery, index);
                return getSolrSuggestion(solrQueryResponse);
            }
        }
        return new SolrSuggestion(Collections.emptyMap(), Collections.emptyList());
    }

    public Map<String, Long> getCategoryAutoSuggestionsForQuery(final LanguageModel language, final SolrIndexedTypeModel solrIndexedType, final String queryInput) {
        if (StringUtils.isNotBlank(queryInput)) {
            final Optional<SolrClient> solrClient = getSolrClient(solrIndexedType);
            if (solrClient.isPresent()) {
               return getCategoryAutoSuggestions(solrIndexedType, queryInput, language, solrClient);
            }
        }
        return Collections.emptyMap();
    }

    private Map<String, Long> getCategoryAutoSuggestions(final SolrIndexedTypeModel solrIndexedType, final String queryInput, final LanguageModel language, final Optional<SolrClient> solrClient) {
        final SolrQuery solrQuery = getCategoryAutoSuggestQueryForSolr(queryInput, solrIndexedType, language);
        final Optional<Index> index = getIndex(solrIndexedType);
        if (index.isPresent()) {
            final Optional<QueryResponse> solrQueryResponse;
            try {
                solrQueryResponse = getSolrQueryResponse(solrClient, solrQuery, index);
                return findCategorySuggestions(solrQueryResponse, solrIndexedType, language);
            } catch (SolrAutoSuggestException solrAutoSuggestException) {
                LOG.error("Excpetion in getCategoryAutoSuggestions : ", solrAutoSuggestException);
            }
        }
        return Collections.emptyMap();
    }

    private Optional<QueryResponse> getSolrQueryResponse(final Optional<SolrClient> solrClient, final SolrQuery solrQuery, final Optional<Index> index) throws SolrAutoSuggestException {
        return solrClient.isPresent() && index.isPresent() ? Optional.ofNullable(getQueryResponse(solrClient.get(), index.get(), solrQuery)) : Optional.empty();
    }

    private Map<String, Long> findCategorySuggestions(final Optional<QueryResponse> response, final SolrIndexedTypeModel solrIndexedType, final LanguageModel language) {
        final Map<String, Long> categorySuggestions = new HashMap<>();
        final Optional<String> categorySuggestForLocale = getCategorySuggestForLocale(solrIndexedType, getCategorySuggest(), language.getIsocode().toLowerCase());
        final Optional<String> categorySuggestForDefaultLocale = getCategorySuggestForLocale(solrIndexedType, getCategorySuggest(), getDefaultLanguageIso());
        if (response.isPresent() && categorySuggestForLocale.isPresent() && categorySuggestForDefaultLocale.isPresent()) {
            final Optional<FacetField> facetFieldForLocale = getCategoryFacetField(response, categorySuggestForLocale);
            findCategorySuggestionsFromFacetField(facetFieldForLocale, categorySuggestions);
            if (CollectionUtils.isEmpty(categorySuggestions)) {
                final Optional<FacetField> facetFieldForDefaultLocale = getCategoryFacetField(response, categorySuggestForDefaultLocale);
                findCategorySuggestionsFromFacetField(facetFieldForDefaultLocale, categorySuggestions);
            }
        }
        return categorySuggestions;
    }

    private Optional<FacetField> getCategoryFacetField(final Optional<QueryResponse> response, final Optional<String> categorySuggestForLocale) {
        return categorySuggestForLocale.flatMap(categoryFacetField -> response.map(queryResponse -> queryResponse.getFacetField(categoryFacetField)));
    }

    private void findCategorySuggestionsFromFacetField(final Optional<FacetField> facetField, final Map<String, Long> categorySuggestions) {
        if (facetField.isPresent()) {
            final Optional<List<FacetField.Count>> categorySuggests = facetField.map(FacetField::getValues);
            categorySuggests.ifPresent(categoryFacetField -> categoryFacetField.forEach(categoryValue -> {
                categorySuggestions.put(categoryValue.getName(), categoryValue.getCount());
            }));
        }
    }

    private SolrSuggestion getSolrSuggestion(final Optional<QueryResponse> response) {
        final Map<String, Collection<String>> resultSuggestionMap = new HashMap<>();
        final Collection<String> resultCollations = new ArrayList<>();
        if (response.isPresent()) {
            final Optional<SpellCheckResponse> spellCheckResponse = response.map(QueryResponse::getSpellCheckResponse);
            spellCheckResponse.ifPresent(spellCheck -> super.populateSuggestionsFromResponse(resultSuggestionMap, resultCollations, spellCheck));
        }
        return new SolrSuggestion(resultSuggestionMap, resultCollations);
    }

    private QueryResponse getQueryResponse(final SolrClient solrClient, final Index index, final SolrQuery solrQuery) throws SolrAutoSuggestException {
        try {
            final QueryResponse response = solrClient.query(index.getName(), solrQuery);
                LOG.debug("Solr Suggest Response: \n" + response);
            return response;
        } catch (SolrServerException | IOException solrServerOrIOException) {
            throw new SolrAutoSuggestException("Error issuing suggestion query", solrServerOrIOException);
        } finally {
            IOUtils.closeQuietly(solrClient);
        }
    }

    private Optional<SolrClient> getSolrClient(final SolrIndexedTypeModel solrIndexedType) {
        try {
            final Optional<SolrSearchProvider> solrSearchProvider = getSolrSearchProvider(solrIndexedType);
            final Optional<Index> index = getIndex(solrIndexedType);
            return solrSearchProvider.isPresent() && index.isPresent() ? Optional.ofNullable(solrSearchProvider.get().getClient(index.get())) : Optional.empty();
        } catch (SolrServiceException solrServiceException) {
            LOG.error("Exception in getSolrClient : " + solrServiceException);
        }
        LOG.warn("solrClient is empty, possible cause may be either solrSearchProvider or index is empty ");
        return Optional.empty();
    }

    private Optional<Index> getIndex(final SolrIndexedTypeModel solrIndexedType) {
        try {
            final Optional<FacetSearchConfig> facetSearchConfig = getFacetSearchConfig(solrIndexedType);
            final Optional<SolrSearchProvider> solrSearchProvider = getSolrSearchProvider(solrIndexedType);
            final Optional<IndexedType> indexedType = getIndexedType(facetSearchConfig, solrIndexedType);
            if (facetSearchConfig.isPresent() && solrSearchProvider.isPresent() && indexedType.isPresent()) {
                final SolrIndexModel solrIndex = this.solrIndexService.getActiveIndex(facetSearchConfig.get().getName(), indexedType.get().getIdentifier());
                return Optional.ofNullable(solrSearchProvider.get().resolveIndex(facetSearchConfig.get(), indexedType.get(), solrIndex.getQualifier()));
            }
        } catch (SolrServiceException solrServiceException) {
            LOG.error("Exception in getIndex : " + solrServiceException);
        }
        LOG.warn("getIndex returns empty");
        return Optional.empty();
    }

    private Optional<SolrSearchProvider> getSolrSearchProvider(final SolrIndexedTypeModel solrIndexedType) {
        try {
            final Optional<FacetSearchConfig> facetSearchConfig = getFacetSearchConfig(solrIndexedType);
            if (facetSearchConfig.isPresent()) {
                final Optional<IndexedType> indexedType = getIndexedType(facetSearchConfig, solrIndexedType);
                return indexedType.isPresent() ? Optional.ofNullable(this.solrSearchProviderFactory.getSearchProvider(facetSearchConfig.get(), indexedType.get())) : Optional.empty();
            }
        } catch (SolrServiceException solrServiceException) {
            LOG.error("Exception in getSolrClient : " + solrServiceException);
        }
        LOG.warn("getSolrSearchProvider returns empty");
        return Optional.empty();
    }

    private Optional<FacetSearchConfig> getFacetSearchConfig(final SolrIndexedTypeModel solrIndexedType) {
        try {
            return Optional.ofNullable(this.facetSearchConfigService.getConfiguration(solrIndexedType.getSolrFacetSearchConfig().getName()));
        } catch (FacetConfigServiceException facetConfigException) {
            LOG.error("Exception in getFacetSearchConfig : " + facetConfigException);
        }
        LOG.warn("getFacetSearchConfig is empty");
        return Optional.empty();
    }

    private Optional<IndexedType> getIndexedType(final Optional<FacetSearchConfig> facetSearchConfig, final SolrIndexedTypeModel solrIndexedType) {
        final Optional<Map<String, IndexedType>> indexedTypeMap = facetSearchConfig.map(FacetSearchConfig::getIndexConfig).map(IndexConfig::getIndexedTypes);
        return indexedTypeMap.flatMap(stringIndexedTypeMap -> Optional.ofNullable(stringIndexedTypeMap.get(this.solrIndexedTypeCodeResolver.resolveIndexedTypeCode(solrIndexedType))));
    }

    private SolrQuery getAutoSuggestQueryForSolr(final String queryInput, final LanguageModel language) {
        final SolrQuery query = new SolrQuery();
        try {
            query.setQuery(queryInput);
            query.setRequestHandler("/suggest");
            query.set("spellcheck.dictionary", language.getIsocode());
            query.set("spellcheck.q", queryInput);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Solr Suggest Query: \n" + URLDecoder.decode(query.toString(), "UTF-8"));
            }
        } catch (IOException ioException) {
            LOG.error("IOException in getAutoSuggestQueryForSolr : " + ioException);
        }
        return query;
    }

    private SolrQuery getCategoryAutoSuggestQueryForSolr(final String queryInput, final SolrIndexedTypeModel solrIndexedType, final LanguageModel language) {
        final SolrQuery categorySuggestQuery = new SolrQuery();
        final Optional<String> categorySuggestForLocale = getCategorySuggestForLocale(solrIndexedType, getCategorySuggestName(), language.getIsocode().toLowerCase());
        final Optional<String> catalogFilter = getCatalogFilter(solrIndexedType);
        categorySuggestForLocale.ifPresent(categorySuggestProp -> buildCategoryAutoSuggestQuery(categorySuggestQuery, queryInput, categorySuggestProp, catalogFilter));
        return categorySuggestQuery;
    }

    private void buildCategoryAutoSuggestQuery(final SolrQuery categorySuggestQuery, final String queryInput, final String categorySuggestNameForLocale, final Optional<String> catalogFilter) {
        categorySuggestQuery.setRequestHandler("/categorysuggest");
        categorySuggestQuery.setQuery(categorySuggestNameForLocale + ":" + queryInput.toLowerCase() + WILDCARD);
        categorySuggestQuery.addFilterQuery(catalogFilter.orElse(EMPTY));
    }

    private Optional<String> getCategorySuggestForLocale(final SolrIndexedTypeModel solrIndexedType, final String propertyName, final String languageIso) {
        final Optional<FacetSearchConfig> facetSearchConfig = getFacetSearchConfig(solrIndexedType);
        if (facetSearchConfig.isPresent()) {
            final Optional<Map<String, IndexedProperty>> indexedPropertyMap = getIndexedType(facetSearchConfig, solrIndexedType).map(IndexedType::getIndexedProperties);
            if (indexedPropertyMap.isPresent()) {
                final IndexedProperty categorySuggestIndexedProperty = indexedPropertyMap.get().get(propertyName);
                return Optional.ofNullable(getFieldNameProvider().getFieldName(categorySuggestIndexedProperty, languageIso, FieldNameProvider.FieldType.INDEX));
            }
        }
        LOG.warn("facetSearchConfig is empty");
        return Optional.empty();
    }

    private Optional<String> getCatalogFilter(final SolrIndexedTypeModel solrIndexedType) {
        final Optional<IndexConfig> indexConfig = getFacetSearchConfig(solrIndexedType).map(FacetSearchConfig::getIndexConfig);
        if (indexConfig.isPresent()) {
            final Collection<CatalogVersionModel> catalogVersions = indexConfig.get().getCatalogVersions();
            final List<CatalogVersionModel> onlineCatalogVersions = catalogVersions.stream().filter(catalogVersionModel -> catalogVersionModel.getVersion().equals("Online")).collect(Collectors.toList());
            return findCatalogFilter(onlineCatalogVersions);
        }
        LOG.warn("CatalogFilter is empty");
        return Optional.empty();
    }

    private Optional<String> findCatalogFilter(final List<CatalogVersionModel> onlineCatalogVersions) {
        final StringBuilder catalogFilter = new StringBuilder();
        boolean isFirst = true;
        for (final CatalogVersionModel onlineCatalogVersion : onlineCatalogVersions) {
            final CatalogModel catalog = onlineCatalogVersion.getCatalog();
            if (isFirst) {
                isFirst = false;
            } else {
                catalogFilter.append(" OR ");
            }
            buildCategoryFilter(catalogFilter, onlineCatalogVersion, catalog);
        }
        LOG.debug("catalogFilter : " + catalogFilter.toString());
        return Optional.of(catalogFilter.toString());
    }

    private void buildCategoryFilter(final StringBuilder catalogFilter, final CatalogVersionModel onlineCatalogVersion, final CatalogModel catalog) {
        catalogFilter.append('(');
        catalogFilter.append("catalogId").append(":\"").append(ClientUtils.escapeQueryChars(catalog.getId())).append('"');
        catalogFilter.append(" AND ");
        catalogFilter.append("catalogVersion").append(":\"").append(ClientUtils.escapeQueryChars(onlineCatalogVersion.getVersion())).append('"');
        catalogFilter.append(')');
    }

    public SolrIndexService getSolrIndexService() {
        return this.solrIndexService;
    }

    @Required
    public void setSolrIndexService(SolrIndexService solrIndexService) {
        this.solrIndexService = solrIndexService;
    }

    public SolrSearchProviderFactory getSolrSearchProviderFactory() {
        return this.solrSearchProviderFactory;
    }

    @Required
    public void setSolrSearchProviderFactory(SolrSearchProviderFactory solrSearchProviderFactory) {
        this.solrSearchProviderFactory = solrSearchProviderFactory;
    }

    public SolrIndexedTypeCodeResolver getSolrIndexedTypeCodeResolver() {
        return this.solrIndexedTypeCodeResolver;
    }

    @Required
    public void setSolrIndexedTypeCodeResolver(SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver) {
        this.solrIndexedTypeCodeResolver = solrIndexedTypeCodeResolver;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    protected String getCategorySuggestName() {
        return categorySuggestName;
    }

    public void setCategorySuggestName(final String categorySuggestName) {
        this.categorySuggestName = categorySuggestName;
    }

    protected String getCategorySuggest() {
        return categorySuggest;
    }

    public void setCategorySuggest(final String categorySuggest) {
        this.categorySuggest = categorySuggest;
    }

    protected String getDefaultLanguageIso() {
        return defaultLanguageIso;
    }

    public void setDefaultLanguageIso(final String defaultLanguageIso) {
        this.defaultLanguageIso = defaultLanguageIso;
    }
}

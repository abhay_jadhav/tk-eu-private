package com.thyssenkrupp.b2b.eu.core.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

public final class TkEuProductUtils {

    private TkEuProductUtils() {
        throw new IllegalAccessError("Utility class may not be instantiated");
    }

    public static boolean areAllValuesEmpty(@NotNull final Map<String, String> selectionMap) {
        return Collections.frequency(selectionMap.values(), "") == selectionMap.size();
    }

    public static Map<String, String> convertStringToMap(final String source, final String entriesSeparator, final String keyValueSeparator) {
        final Map<String, String> map = new HashMap<String, String>();
        final String mapString = source.replaceAll("\\}", "").replaceAll("\\{", "");
        final String[] entries = mapString.split(entriesSeparator);
        for (final String entry : entries) {
            if (!StringUtils.isEmpty(entry) && entry.contains(keyValueSeparator)) {
                final String[] keyValue = entry.split(keyValueSeparator);
                if (keyValue.length > 1) {
                    map.put(keyValue[0].trim(), keyValue[1]);
                } else {
                    map.put(keyValue[0].trim(), "");
                }
            }
        }
        return map;
    }

    public static void validateAndSetSelection(final Map<String, String> selectionMap, final String selectedVariantCategory, final String value, boolean ignoreSelection) {
        if (isNotEmpty(selectedVariantCategory) && !ignoreSelection) {
            final String existingValue = selectionMap.get(selectedVariantCategory);
            if (StringUtils.equals(value, existingValue)) {
                selectionMap.put(selectedVariantCategory, "");// reset selection
            } else {
                selectionMap.put(selectedVariantCategory, value);
            }
        }
    }

    public static Map<String, String> mergeSelectedVvc(Map<String, String> selectionMap, final Map<String, String> autoSelectedMap, final String key, final String value) {

        Map<String, String> mergedSelectionMap = Stream.of(selectionMap, autoSelectedMap)
            .flatMap(map -> map.entrySet().stream())
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey,
                    Map.Entry::getValue,
                    (v1, v2) -> v2)
            );
        return mergedSelectionMap;
    }
}

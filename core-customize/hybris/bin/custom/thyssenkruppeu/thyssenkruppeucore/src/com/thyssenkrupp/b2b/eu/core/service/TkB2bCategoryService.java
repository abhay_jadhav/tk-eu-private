package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Collection;
import java.util.List;

import com.thyssenkrupp.b2b.eu.core.pojo.VariantCategoryCreationParameter;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;


public interface TkB2bCategoryService extends CategoryService {

    VariantValueCategoryModel createVariantValueCategory(Feature featureValue, VariantCategoryModel parentCategory, CatalogVersionModel catalogVersion);

    VariantValueCategoryModel createVariantValueCategoryForTradeLength(TkTradeLengthConfiguratorSettingsModel setting, VariantCategoryModel parentCategory, CatalogVersionModel catalogVersion);

    VariantValueCategoryModel createVariantValueCategoryForConfigurableLength(TkLengthConfiguratorSettingsModel setting, VariantCategoryModel parentCategory, CatalogVersionModel catalogVersion);

    VariantCategoryModel createVariantCategory(VariantProductModel variant, String code, CatalogVersionModel catalogVersion, Feature feature);

    VariantCategoryModel getOrCreateVariantCategory(VariantCategoryCreationParameter parameter);

    void buildVariantCategoryHierarchy(List<VariantCategoryModel> variantCategories);

    String buildVariantCategoryCode(String classCode, String attributeCode);

    Collection<VariantCategoryModel> getAllVariantCategoriesWithCode(String ruleCode, CatalogVersionModel catalogVersion);

    Collection<VariantValueCategoryModel> getAllVariantValueCategoriesWithCode(String ruleCode, CatalogVersionModel catalogVersion);

    <T extends CategoryModel> Collection<T> getAllCategoriesWithoutIncludedProducts(String ruleCode, CatalogVersionModel catalogVersion);

    CatalogVersionModel getCatalogVersionBySalesOrganization(String salesOrganization);

    Collection<CategoryModel> filterCategoriesOfType(Collection<CategoryModel> categories, Class classType);

    boolean isTradeLengthVariantCategory(String categoryCode);

    boolean isLengthVariantCategory(String categoryCode);

    void removeCategories(Collection<CategoryModel> categories);

    void setSubCategoriesAsEmpty(Collection<CategoryModel> categories);

    void updateVariantInfo(ProductModel variant, List<LanguageModel> baseStoreLaguages);

    List<TkTradeLengthConfiguratorSettingsModel> getTradeLengthSettingConfigurations(VariantProductModel variant);

    boolean isTradeLengthVariantCategory(CategoryModel categoryModel);

    String buildVariantValueCategoryCode(String variantValueCodePattern, VariantCategoryModel parentCategory);

    String getVariantValueCodePatternForTradeLength(Object productInfo);
}

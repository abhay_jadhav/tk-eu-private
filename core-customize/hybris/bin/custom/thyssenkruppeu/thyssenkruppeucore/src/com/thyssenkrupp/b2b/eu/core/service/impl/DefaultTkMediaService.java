package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.servicelayer.media.impl.DefaultMediaService;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuMediaDao;
import com.thyssenkrupp.b2b.global.baseshop.core.model.MediaIconModel;

public class DefaultTkMediaService extends DefaultMediaService {

    private static String DEFAULT_DOWNLOAD_ICON = "Default Download Icon";

    private TkEuMediaDao tkEuMediaDao;

    public String getMediaIconUrlFromMimeType(final String mimeType) {
        final MediaIconModel mediaIconForMimeType = (MediaIconModel) getTkEuMediaDao().findMediaIconByMimeType(mimeType);
        if (mediaIconForMimeType == null) {
            final MediaIconModel defaultMediaIcon = (MediaIconModel) getTkEuMediaDao().findMediaIconByMimeType(DEFAULT_DOWNLOAD_ICON);
            return defaultMediaIcon != null ? defaultMediaIcon.getURL() : null;
        }
        return mediaIconForMimeType.getURL();
    }

    public TkEuMediaDao getTkEuMediaDao() {
        return tkEuMediaDao;
    }

    public void setTkEuMediaDao(final TkEuMediaDao tkEuMediaDao) {
        this.tkEuMediaDao = tkEuMediaDao;
    }

}

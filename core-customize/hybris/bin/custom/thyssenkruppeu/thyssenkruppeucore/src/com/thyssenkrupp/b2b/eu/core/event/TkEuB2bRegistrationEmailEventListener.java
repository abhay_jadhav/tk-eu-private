package com.thyssenkrupp.b2b.eu.core.event;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bRegistrationProcessModel;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

public class TkEuB2bRegistrationEmailEventListener extends AbstractAcceleratorSiteEventListener<TkEuB2bRegistrationEmailEvent> {

    private static final String REGISTRATION_EMAIL_BUSINESS_PROCESS = "tkEuB2bCustomerRegistrationEmailProcess";
    private static final Logger LOG = Logger.getLogger(TkEuB2bRegistrationEmailEventListener.class);
    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    @Override
    protected void onSiteEvent(final TkEuB2bRegistrationEmailEvent event) {
        final CustomerModel customerModel = event.getCustomer();
        LOG.debug("Received RegistrationEmailEvent for customerUid:" + customerModel.getUid());
        final String processId = "tkEuB2bCustomerRegistrationEmail-" + customerModel.getUid() + "-" + System.currentTimeMillis();
        final TkEuB2bRegistrationProcessModel processModel = getBusinessProcessService().createProcess(processId, REGISTRATION_EMAIL_BUSINESS_PROCESS);
        processModel.setSite(event.getSite());
        processModel.setCustomer(event.getCustomer());
        processModel.setLanguage(event.getLanguage());
        processModel.setCurrency(event.getCurrency());
        processModel.setStore(event.getBaseStore());
        processModel.setToken(event.getToken());
        getModelService().save(processModel);
        getBusinessProcessService().startProcess(processModel);
    }

    @Override
    protected SiteChannel getSiteChannelForEvent(final TkEuB2bRegistrationEmailEvent event) {
        final BaseSiteModel site = event.getSite();
        validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }
}


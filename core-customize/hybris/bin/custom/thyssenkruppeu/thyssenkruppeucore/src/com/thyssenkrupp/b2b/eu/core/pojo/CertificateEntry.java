package com.thyssenkrupp.b2b.eu.core.pojo;


public class CertificateEntry {

    private String lineItemNo;
    private boolean status;

    public String getLineItemNo() {
        return lineItemNo;
    }

    public void setLineItemNo(String lineItemNo) {
        this.lineItemNo = lineItemNo;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

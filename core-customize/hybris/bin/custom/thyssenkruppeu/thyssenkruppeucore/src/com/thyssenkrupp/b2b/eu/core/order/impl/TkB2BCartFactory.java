package com.thyssenkrupp.b2b.eu.core.order.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.order.impl.DefaultB2BCartFactory;
import de.hybris.platform.core.model.order.CartModel;

import org.apache.log4j.Logger;

public class TkB2BCartFactory extends DefaultB2BCartFactory {

    public static final Logger LOG = Logger.getLogger(TkB2BCartFactory.class);

    @Override
    public CartModel createCart() {
        final CartModel cartModel = super.createCart();
        setB2BUnitTerms(cartModel);
        getModelService().save(cartModel);
        return cartModel;
    }

    private void setB2BUnitTerms(final CartModel cartModel) {
        final B2BUnitModel b2BUnitModel = cartModel.getUnit();
        if (b2BUnitModel != null) {
            cartModel.setPaymentterms(b2BUnitModel.getPaymentterms());
            cartModel.setShippingcondition(b2BUnitModel.getShippingcondition());
            cartModel.setIncoterms(b2BUnitModel.getIncoterms());
        } else {
            LOG.warn("No B2BUnit associated to cart " + cartModel.getCode() + " created by " + cartModel.getUser().getUid());
        }
    }
}

package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentStatusSAP implements Serializable {

    @JsonProperty("Document_Number")
    private String documentNo;

    @JsonProperty("Line_Item_Number")
    private String lineItemNumber;

    @JsonProperty("Batch_Number")
    private String batchNo;

    @JsonProperty("Document_Exists_Flag")
    private String documentExistsFlag;

    @JsonProperty("Document_Type")
    private String documentType;

    @JsonProperty("Status_Message")
    private String statusMessage;

    @JsonProperty("Document_Number")
    public String getDocumentNo() {
        return documentNo;
    }

    @JsonProperty("Document_Number")
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    @JsonProperty("Line_Item_Number")
    public String getLineItemNumber() {
        return lineItemNumber;
    }

    @JsonProperty("Line_Item_Number")
    public void setLineItemNumber(String lineItemNumber) {
        this.lineItemNumber = lineItemNumber;
    }

    @JsonProperty("Batch_Number")
    public String getBatchNo() {
        return batchNo;
    }

    @JsonProperty("Batch_Number")
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @JsonProperty("Document_Exists_Flag")
    public String getDocumentExistsFlag() {
        return documentExistsFlag;
    }

    @JsonProperty("Document_Exists_Flag")
    public void setDocumentExistsFlag(String documentExistsFlag) {
        this.documentExistsFlag = documentExistsFlag;
    }

    @JsonProperty("Document_Type")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("Document_Type")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("Status_Message")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("Status_Message")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkUomConversionDao;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;

public class DefaultTkUomConversionService implements TkUomConversionService {

    private static final Logger LOG                    = LoggerFactory.getLogger(DefaultTkUomConversionService.class);
    private static final String SAP_KGM_UNIT_CODE      = "KGM";
    private static final int    SCALE_BY_EIGHT         = 8;
    private static final int    SCALE_BY_THREE         = 3;
    private static final String SAP_WEIGHTED_TYPE_CODE = "SAP-MASS";

    private TkUomConversionDao tkUomConversionDao;

    @Override
    public List<TkUomConversionModel> getUomConversionByProductCode(final String productCode) {
        validateParameterNotNullStandardMessage("productCode", productCode);
        final List<TkUomConversionModel> conversions = getTkUomConversionDao().findUomConversionByProductCode(productCode);
        return ListUtils.emptyIfNull(conversions);
    }

    @Override
    public Optional<BigDecimal> calculateFactor(final Double dividend, final Double divisor) {
        validateParameterNotNullStandardMessage("dividend", dividend);
        validateParameterNotNullStandardMessage("divisor", divisor);
        final BigDecimal divisorBigDecimal = valueOf(divisor);
        if (ZERO.compareTo(divisorBigDecimal) == 0) {
            LOG.error("divisor {} for the dividend {} is \"ZERO\" Returning factor as ONE", divisor, dividend);
            return Optional.of(BigDecimal.ONE);
        }
        return Optional.ofNullable(valueOf(dividend).divide(divisorBigDecimal, SCALE_BY_EIGHT, RoundingMode.HALF_UP).stripTrailingZeros());
    }

    @Override
    public Optional<TkUomConversionModel> getFirstWeightedUomConversion(final List<TkUomConversionModel> conversionModels) {
        final Stream<TkUomConversionModel> weightedUomConversionModelStream = emptyIfNull(conversionModels).stream().filter(this::isWeightedUomConversion);
        return weightedUomConversionModelStream.filter(tkUomConversionModel -> SAP_KGM_UNIT_CODE.equals(tkUomConversionModel.getSalesUnit().getCode())).findFirst();
    }

    @Override
    public boolean isWeightedUomConversion(final TkUomConversionModel conversionModel) {
        if (conversionModel != null && conversionModel.getSalesUnit() != null) {
            return StringUtils.equals(SAP_WEIGHTED_TYPE_CODE, conversionModel.getSalesUnit().getUnitType());
        }
        return false;
    }

    @Override
    public Pair<Double, UnitModel> calculateQuantityInTargetUnit(final Pair<Double, UnitModel> sourceMaterialPair, final UnitModel targetUnit, final ProductModel product) {
        validateParameterNotNullStandardMessage("sourceMaterialPair", sourceMaterialPair);
        validateParameterNotNullStandardMessage("targetUnit", targetUnit);
        validateParameterNotNullStandardMessage("product", product);
        if (ObjectUtils.equals(sourceMaterialPair.getValue(), targetUnit)) {
            return sourceMaterialPair;
        }
        final List<TkUomConversionModel> conversionModels = getUomConversionByProductCode(product.getCode());
        if (CollectionUtils.isEmpty(conversionModels)) {
            throw new RuntimeException(String.format("Can not find a matching conversion for the unit \"%s\" in product \"%s\"", targetUnit, product.getCode()));
        }

        final Optional<BigDecimal> sourceUnitToBaseUnit = findUnitFactorFromUomConversion(conversionModels, sourceMaterialPair.getValue());
        final Optional<BigDecimal> targetUnitToBaseUnit = findUnitFactorFromUomConversion(conversionModels, targetUnit);
        final Optional<BigDecimal> factorPerSourceUnit = calculateFactor(targetUnitToBaseUnit.orElse(ONE).doubleValue(), sourceUnitToBaseUnit.orElse(ONE).doubleValue());
        final BigDecimal calculatedQty = factorPerSourceUnit.orElse(ONE).multiply(valueOf(sourceMaterialPair.getKey()));
        final BigDecimal roundedQty = roundPurchaseQty(calculatedQty);
        return Pair.of(roundedQty.doubleValue(), targetUnit);
    }

    private Optional<BigDecimal> findUnitFactorFromUomConversion(final List<TkUomConversionModel> conversionModels, final UnitModel unit) {
        Optional<BigDecimal> unitToBaseUnitFactor = Optional.of(ONE);
        final Optional<TkUomConversionModel> firstConversionModel = conversionModels.stream().findFirst();
        if (firstConversionModel.isPresent() && ObjectUtils.equals(firstConversionModel.get().getBaseUnit(), unit)) {
            return unitToBaseUnitFactor;
        }
        for (final TkUomConversionModel conversionModel : conversionModels) {
            if (ObjectUtils.equals(conversionModel.getSalesUnit(), unit)) {
                unitToBaseUnitFactor = calculateBaseUnitToSalesUnitFactor(conversionModel.getSalesUnitFactor(), conversionModel.getBaseUnitFactor());
                LOG.debug("UOM conversion found for: " + unit + " Sales unit Factor: " + conversionModel.getSalesUnitFactor() + " Base unit factor: " + conversionModel.getBaseUnitFactor());
                return unitToBaseUnitFactor;
            }
        }
        return findUnitFactorIfUnitIsKgm(conversionModels, unit);
    }

    private Optional<BigDecimal> findUnitFactorIfUnitIsKgm(final List<TkUomConversionModel> conversionModels, final UnitModel unit) {
        Optional<BigDecimal> unitToBaseUnitFactor;
        if (StringUtils.equalsIgnoreCase(SAP_KGM_UNIT_CODE, unit.getCode())) {
            final Optional<TkUomConversionModel> firstWeightedUomConversion = getFirstWeightedUomConversion(conversionModels);
            if (firstWeightedUomConversion.isPresent()) {
                final TkUomConversionModel tkUomConversionModel = firstWeightedUomConversion.get();
                unitToBaseUnitFactor = calculateBaseUnitToSalesUnitFactor(tkUomConversionModel.getSalesUnitFactor(), tkUomConversionModel.getBaseUnitFactor());
                LOG.debug("UOM conversion found for: " + unit + " Sales unit Factor: " + tkUomConversionModel.getSalesUnitFactor() + " Base unit factor: " + tkUomConversionModel.getBaseUnitFactor());
                if (unitToBaseUnitFactor.isPresent()) {
                    return calculateFactor(unitToBaseUnitFactor.get().doubleValue(), tkUomConversionModel.getSalesUnit().getConversion());
                }
            }
        }
        throw new RuntimeException(String.format("Can not find a matching conversion for the unit \"%s\" ", unit));
    }

    protected Optional<BigDecimal> calculateBaseUnitToSalesUnitFactor(final Double salesUnitFactor, final Double baseUnitFactor) {
        return calculateFactor(salesUnitFactor, baseUnitFactor);
    }

    private BigDecimal roundPurchaseQty(@NotNull final BigDecimal purchaseQty) {
        final BigDecimal roundedQty = purchaseQty.setScale(SCALE_BY_THREE, BigDecimal.ROUND_HALF_UP);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will round PurchaseQty from \'{}\' to \'{}\'.", purchaseQty, roundedQty);
        }
        return roundedQty;
    }

    public TkUomConversionDao getTkUomConversionDao() {
        return tkUomConversionDao;
    }

    @Required
    public void setTkUomConversionDao(final TkUomConversionDao tkUomConversionDao) {
        this.tkUomConversionDao = tkUomConversionDao;
    }
}

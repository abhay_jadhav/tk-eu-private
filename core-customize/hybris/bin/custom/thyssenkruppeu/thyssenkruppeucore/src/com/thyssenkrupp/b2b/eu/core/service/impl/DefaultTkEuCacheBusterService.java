package com.thyssenkrupp.b2b.eu.core.service.impl;

import java.util.Properties;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCacheBusterService;

public class DefaultTkEuCacheBusterService implements TkEuCacheBusterService {
    private static final String BUILD_TIMESTAMP="thyssenkruppeu.build.timestamp";

    protected Properties properties;

    @Override
    public String getBuildTimestamp() {
        return getProperty(BUILD_TIMESTAMP);
    }

    protected String getProperty(final String key) {
        return properties.getProperty(key);
    }

    public void setProperties(final Properties properties) {
        this.properties = properties;
    }
}

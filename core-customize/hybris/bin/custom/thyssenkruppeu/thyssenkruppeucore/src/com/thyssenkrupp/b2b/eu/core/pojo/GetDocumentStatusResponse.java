package com.thyssenkrupp.b2b.eu.core.pojo;

import java.util.List;

public class GetDocumentStatusResponse {

    private String  documentNo;
    private boolean documentStatus;
    private String  downloadServiceStatus;

    private List<CertificateEntry> certificates;

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public boolean isDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(boolean documentStatus) {
        this.documentStatus = documentStatus;
    }

    public List<CertificateEntry> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<CertificateEntry> certificates) {
        this.certificates = certificates;
    }

    public String getDownloadServiceStatus() {
        return downloadServiceStatus;
    }

    public void setDownloadServiceStatus(String downloadServiceStatus) {
        this.downloadServiceStatus = downloadServiceStatus;
    }
}

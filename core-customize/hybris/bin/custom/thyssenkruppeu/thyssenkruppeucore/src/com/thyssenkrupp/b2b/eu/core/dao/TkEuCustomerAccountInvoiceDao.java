package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Set;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public interface TkEuCustomerAccountInvoiceDao {

    SearchPageData<InvoiceModel> orderInvoiceSearchList(String searchKey, PageableData pageableData, Set<B2BUnitModel> b2BUnitModel, BaseStoreModel baseStoreModel);

    int orderInvoicesSearchListCount(String searchKey, Set<B2BUnitModel> b2BUnitModel, BaseStoreModel baseStoreModel);

    List<InvoiceModel> fetchInvoices(String invoiceNo, Set<B2BUnitModel> b2BUnitModel,BaseStoreModel baseStoreModel);

}

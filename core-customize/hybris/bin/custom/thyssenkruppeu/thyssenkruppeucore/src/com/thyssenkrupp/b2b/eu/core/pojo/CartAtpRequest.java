package com.thyssenkrupp.b2b.eu.core.pojo;

public class CartAtpRequest {
    private String entryNumber;
    private String productCode;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public String getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(final String entryNumber) {
        this.entryNumber = entryNumber;
    }

}

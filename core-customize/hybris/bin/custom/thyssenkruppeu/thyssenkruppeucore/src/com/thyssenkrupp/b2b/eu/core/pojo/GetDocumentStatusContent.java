package com.thyssenkrupp.b2b.eu.core.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

public class GetDocumentStatusContent implements Serializable {

    private String      hybrisRegionBDEnvironment;
    private String      isCertsPaid;
    private String      isContentAnonymized;
    private String      customerNumber;
    private String      salesOrg;
    private String      distributionChannel;
    private String      division;
    private String      documentCategory;
    private ItemDetails itemDetails;

    @JsonProperty("HybrisRegion_BusinessDivision_Environment")
    public String getHybrisRegionBDEnvironment() {
        return hybrisRegionBDEnvironment;
    }

    public void setHybrisRegionBDEnvironment(String hybrisRegionBDEnvironment) {
        this.hybrisRegionBDEnvironment = hybrisRegionBDEnvironment;
    }

    @JsonProperty("Is_Certs_Are_Paid")
    public String getIsCertsPaid() {
        return isCertsPaid;
    }

    public void setIsCertsPaid(String isCertsPaid) {
        this.isCertsPaid = isCertsPaid;
    }

    @JsonProperty("Is_Content_Anonymized")
    public String getIsContentAnonymized() {
        return isContentAnonymized;
    }

    public void setIsContentAnonymized(String isContentAnonymized) {
        this.isContentAnonymized = isContentAnonymized;
    }

    @JsonProperty("Customer_Number")
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @JsonProperty("Sales_Organization")
    public String getSalesOrg() {
        return salesOrg;
    }

    public void setSalesOrg(String salesOrg) {
        this.salesOrg = salesOrg;
    }

    @JsonProperty("Distribution_Channel")
    public String getDistributionChannel() {
        return distributionChannel;
    }

    public void setDistributionChannel(String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    @JsonProperty("Division")
    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    @JsonProperty("Document_Category")
    public String getDocumentCategory() {
        return documentCategory;
    }

    public void setDocumentCategory(String documentCategory) {
        this.documentCategory = documentCategory;
    }

    @JsonProperty("Item_Details")
    public ItemDetails getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(ItemDetails itemDetails) {
        this.itemDetails = itemDetails;
    }
}

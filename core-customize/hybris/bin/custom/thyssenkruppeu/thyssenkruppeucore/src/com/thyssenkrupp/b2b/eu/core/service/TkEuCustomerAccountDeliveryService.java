package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;
import java.util.Set;

public interface TkEuCustomerAccountDeliveryService {

    SearchPageData<ConsignmentModel> orderDeliveriesSearchList(String searchKey, PageableData pageableData, Set<B2BUnitModel> b2BUnits);

    int orderDeliveriesSearchListCount(String searchKey, Set<B2BUnitModel> b2BUnits);

    List<ConsignmentModel> fetchDeliveries(String deliveryNo, Set<B2BUnitModel> b2BUnits);

    List<ConsignmentEntryModel> fetchDeliveriesForOrderEntry(String sapOrderId, int entryNumber);

}

package com.thyssenkrupp.b2b.eu.core.service;


public interface TkEuCacheBusterService  {

    String getBuildTimestamp();
}

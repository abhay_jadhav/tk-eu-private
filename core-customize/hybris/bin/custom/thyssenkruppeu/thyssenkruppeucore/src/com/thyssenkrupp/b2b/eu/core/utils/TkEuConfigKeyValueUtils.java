package com.thyssenkrupp.b2b.eu.core.utils;

import de.hybris.platform.core.Registry;

import java.math.BigDecimal;

import com.thyssenkrupp.b2b.eu.core.service.TkEuConfigKeyValueService;

/**
 *
 */
public final class TkEuConfigKeyValueUtils {
    public static final TkEuConfigKeyValueService CONFKEYVALUESERVICE;

    private TkEuConfigKeyValueUtils() {
        super();
    }

    static {
        CONFKEYVALUESERVICE = (TkEuConfigKeyValueService) Registry.getApplicationContext().getBean("tkEuConfigKeyValueService");
    }

    public static boolean getBoolean(final String key, final boolean defaultValue) {
        try {
            final boolean value = getKeyValueService().getBoolean(key);
            return value != defaultValue ? value : defaultValue;
        } catch (final Exception e) {
            return defaultValue;
        }
    }

    public static String getString(final String key, final String defaultValue) {
        try {
            final String value = getKeyValueService().getProperty(key);
            return value != null ? value : defaultValue;
        } catch (final Exception e) {
            return defaultValue;
        }
    }

    public static double getDouble(final String key, final double defaultValue) {
        try {
            final double value = getKeyValueService().getDouble(key);
            return value != defaultValue ? value : defaultValue;
        } catch (final Exception e) {
            return defaultValue;
        }
    }

    public static BigDecimal getBigDecimal(final String key, final BigDecimal defaultValue) {
        try {
            final BigDecimal value = getKeyValueService().getBigDecimal(key);
            return value != null ? value : defaultValue;
        } catch (final Exception e) {
            return defaultValue;
        }
    }

    private static TkEuConfigKeyValueService getKeyValueService() {
        return TkEuConfigKeyValueUtils.CONFKEYVALUESERVICE;
    }
}

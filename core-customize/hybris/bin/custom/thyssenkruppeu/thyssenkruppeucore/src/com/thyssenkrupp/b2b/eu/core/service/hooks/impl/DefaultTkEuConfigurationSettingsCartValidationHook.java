package com.thyssenkrupp.b2b.eu.core.service.hooks.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.hooks.CartValidationHook;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

public class DefaultTkEuConfigurationSettingsCartValidationHook implements CartValidationHook {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuConfigurationSettingsCartValidationHook.class);

    private TkEuConfiguratorSettingsService configuratorSettingsService;
    private ModelService                    modelService;

    @Override
    public void beforeValidateCart(CommerceCartParameter parameter, List<CommerceCartModification> modifications) {
        //no implementation
    }

    @Override
    public void afterValidateCart(CommerceCartParameter parameter, List<CommerceCartModification> modifications) {
        final CartModel cartModel = parameter.getCart();
        if (cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries())) {
            for (final AbstractOrderEntryModel orderEntryModel : cartModel.getEntries()) {
                CommerceCartModification commerceCartModification = validateCartEntryConfigurations(cartModel, (CartEntryModel) orderEntryModel);
                if (commerceCartModification != null) {
                    modifications.add(commerceCartModification);
                }
            }
        }
    }

    private CommerceCartModification validateCartEntryConfigurations(CartModel cartModel, CartEntryModel cartEntryModel) {
        if (isInvalidTradeLengthConfigurationSettings(cartEntryModel)) {
            LOG.warn("invalid tradeLength configurations settings found for line item {}", cartEntryModel.getProduct().getCode());
            return removeCartEntry(cartModel, cartEntryModel);
        }
        if (isInValidCertificateConfigurationsSettings(cartEntryModel)) {
            LOG.warn("invalid certificate configurations settings found for line item {}", cartEntryModel.getProduct().getCode());
            return removeCartEntry(cartModel, cartEntryModel);
        }

        return null;
    }

    private boolean isInValidCertificateConfigurationsSettings(CartEntryModel cartEntryModel) {
        List<CertificateConfiguredProductInfoModel> certificateConfiguredProductInfoModels = cartEntryModel.getProductInfos().stream().filter(isProductInfoOfTypeCertificateAndSelected()).map(CertificateConfiguredProductInfoModel.class::cast).filter(certificateConfiguredProductInfoModel -> certificateConfiguredProductInfoModel.isChecked()).collect(Collectors.toList());
        for (CertificateConfiguredProductInfoModel certificateConfiguredProductInfoModel : certificateConfiguredProductInfoModels) {
            Optional<CertificateConfiguratorSettingsModel> certificateConfiguratorSettingsById = getCertificateConfiguratorSettingsById(cartEntryModel.getProduct(), certificateConfiguredProductInfoModel.getCertificateId());
            if (!certificateConfiguratorSettingsById.isPresent()) {
                return true;
            }
        }
        return false;
    }

    private boolean isInvalidTradeLengthConfigurationSettings(CartEntryModel cartEntryModel) {
        List<TkTradeLengthConfiguredProductInfoModel> tradeLengthConfiguredProductInfoModels = cartEntryModel.getProductInfos().stream().filter(isProductInfoOfTypeTradeLengthAndSelected()).map(TkTradeLengthConfiguredProductInfoModel.class::cast).filter(tkTradeLengthConfiguredProductInfoModel -> tkTradeLengthConfiguredProductInfoModel.isChecked()).collect(Collectors.toList());
        for (TkTradeLengthConfiguredProductInfoModel tradeLengthConfiguredProductInfoModel : tradeLengthConfiguredProductInfoModels) {
            Optional<TkTradeLengthConfiguratorSettingsModel> tradeLengthConfiguratorSettingsByLabelId = getTradeLengthConfiguratorSettingsByLabelId(cartEntryModel.getProduct(), tradeLengthConfiguredProductInfoModel.getLabel());
            if (!tradeLengthConfiguratorSettingsByLabelId.isPresent()) {
                return true;
            }
        }
        return false;
    }

    private CommerceCartModification removeCartEntry(CartModel cartModel, CartEntryModel cartEntryModel) {
        final CommerceCartModification modification = new CommerceCartModification();
        modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
        modification.setQuantityAdded(0);
        modification.setQuantity(0);
        final CartEntryModel entry = new CartEntryModel();
        entry.setProduct(cartEntryModel.getProduct());
        modification.setEntry(entry);
        entry.setProduct(cartEntryModel.getProduct());
        modification.setEntry(entry);
        getModelService().remove(cartEntryModel);
        getModelService().refresh(cartModel);
        return modification;
    }

    private Optional<TkTradeLengthConfiguratorSettingsModel> getTradeLengthConfiguratorSettingsByLabelId(ProductModel productModel, String labelId) {
        final List<AbstractConfiguratorSettingModel> allConfiguratorSettings = getConfiguratorSettingsService().getConfiguratorSettingsForProduct(productModel);
        return allConfiguratorSettings.stream().filter(TkTradeLengthConfiguratorSettingsModel.class::isInstance).map(TkTradeLengthConfiguratorSettingsModel.class::cast)
          .filter(o -> (isNotEmpty(o.getLabel()) && o.getLabel().equalsIgnoreCase(labelId))).findFirst();
    }

    private Optional<CertificateConfiguratorSettingsModel> getCertificateConfiguratorSettingsById(ProductModel productModel, String certificateId) {
        final List<AbstractConfiguratorSettingModel> allConfiguratorSettings = getConfiguratorSettingsService().getConfiguratorSettingsForProduct(productModel);
        return allConfiguratorSettings.stream().filter(CertificateConfiguratorSettingsModel.class::isInstance).map(CertificateConfiguratorSettingsModel.class::cast)
          .filter(o -> (isNotEmpty(o.getCertificateId()) && o.getCertificateId().equalsIgnoreCase(certificateId))).findFirst();
    }

    private Predicate<AbstractOrderEntryProductInfoModel> isProductInfoOfTypeTradeLengthAndSelected() {
        return o -> (TkTradeLengthConfiguredProductInfoModel.class.isInstance(o) && TkTradeLengthConfiguredProductInfoModel.class.cast(o).isChecked());
    }

    private Predicate<AbstractOrderEntryProductInfoModel> isProductInfoOfTypeCertificateAndSelected() {
        return o -> (CertificateConfiguredProductInfoModel.class.isInstance(o) && CertificateConfiguredProductInfoModel.class.cast(o).isChecked());
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public TkEuConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(TkEuConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }
}

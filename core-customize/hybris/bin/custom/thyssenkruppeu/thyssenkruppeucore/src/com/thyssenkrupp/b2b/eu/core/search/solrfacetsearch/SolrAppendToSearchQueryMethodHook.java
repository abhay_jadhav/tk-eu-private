package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;

public interface SolrAppendToSearchQueryMethodHook {

    SolrSearchRequest appendToSearchQuery(SolrSearchRequest solrSearchRequest);
}

package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bUnitService;

public class TkEuCustomerAccountDaoImpl extends DefaultCustomerAccountDao implements TkEuCustomerAccountDao {

    protected static final String FILTER_ORDER_STATUS = " AND {" + OrderModel.STATUS + "} NOT IN (?filterStatusList)";

    protected static final String SORT_ORDERS_BY_CODE_DESC = " ORDER BY {ord." + OrderModel.CODE + "} DESC";

    protected static final String SORT_ORDERS_BY_CODE_ASC = " ORDER BY  {ord." + OrderModel.CODE + "} ASC";

    protected static final String SORT_ORDERS_BY_DATE_DESC = " ORDER BY {ord." + OrderModel.CREATIONTIME + "} DESC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_DATE_ASC = " ORDER BY {ord." + OrderModel.CREATIONTIME + "} ASC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_NAME_DESC = " ORDER BY {ord." + OrderModel.NAME + "} COLLATE Latin1_General_CI_AS DESC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_NAME_ASC = " ORDER BY {ord." + OrderModel.NAME + "} COLLATE Latin1_General_CI_AS ASC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_PO_DESC = " ORDER BY {ord." + OrderModel.PURCHASEORDERNUMBER + "} COLLATE Latin1_General_CI_AS DESC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_PO_ASC = " ORDER BY {ord." + OrderModel.PURCHASEORDERNUMBER + "} COLLATE Latin1_General_CI_AS ASC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_SAP_CODE_ASC = " ORDER BY {ord." + OrderModel.SAPORDERID + "} ASC";

    protected static final String SORT_ORDERS_BY_SAP_CODE_DESC = " ORDER BY {ord." + OrderModel.SAPORDERID + "} DESC";

    protected static final String SORT_ORDERS_BY_STATUS_DESC = " ORDER BY {os.name} DESC, {os.code} DESC, {" + OrderModel.CREATIONTIME + "} DESC";

    protected static final String SORT_ORDERS_BY_STATUS_ASC = " ORDER BY {os.name}, {os.code}, {" + OrderModel.CREATIONTIME + "}";

    protected static final String SORT_ORDERS_BY_PRICE_DESC = " ORDER BY {ord." + OrderModel.TOTALPRICE + "} +{ord." + OrderModel.TOTALTAX + "} DESC, {" + OrderModel.PK + "}";

    protected static final String SORT_ORDERS_BY_PRICE_ASC = " ORDER BY {ord." + OrderModel.TOTALPRICE + "}+{ord." + OrderModel.TOTALTAX + "}, {" + OrderModel.PK + "}";

    protected static final String FIND_ORDERS_BY_UNIT_STORE_QUERY = "SELECT {" + OrderModel.PK + "}, {" + OrderModel.CREATIONTIME + "}, {" + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + " AS ord}, {OrderStatus AS os} WHERE {os.PK} = {ord.status} AND {" + OrderModel.UNIT + "} IN (?unit) AND {" + OrderModel.STORE + "} = ?store AND {" + OrderModel.INITIALORDERVERSION + " }= 1 ";

    protected static final String FIND_ORDERS_CONFIRMATION_BY_UNIT_STORE_QUERY = "SELECT {ord.PK}, {ord.CREATIONTIME}, {ord.deliverydate}, {ord.CODE}" + " from {BaseStore as basestore \n" + "JOIN sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk}\n" + "JOIN Order as ord ON {ord.salesOrganization} = {sapconfiguration.sapcommon_salesOrganization} \n" + "JOIN OrderStatus as os  ON {os.PK} = {ord.status}} \n" + "WHERE {ord.UNIT} IN (?unit) AND {ord.VERSIONID} IS NULL AND {ord.STORE} = ?store AND {ord.STATUS} IN  (?orderStatus) \n" + "AND {ord.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) \n" + "AND {ord.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision}) ";

    protected static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY = "SELECT {ord." + OrderModel.PK + "}, {ord." + OrderModel.CREATIONTIME + "}, {ord." + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + " AS ord}, {OrderStatus AS os} WHERE {os.PK} = {ord.status} AND {" + OrderModel.USER + "} = ?customer AND {" + OrderModel.STORE + "} = ?store AND {" + OrderModel.INITIALORDERVERSION + " }= 1 ";

    protected static final String FIND_ORDERS_CONFIRMATION_BY_CUSTOMER_STORE_QUERY = "SELECT {ord." + OrderModel.PK + "},  {ord." + OrderModel.DELIVERYDATE + "}, {ord." + OrderModel.CREATIONTIME + "}, {ord." + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + " AS ord}, {OrderStatus AS os} WHERE {os.PK} = {ord.status} AND {" + OrderModel.USER + "} = ?customer AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.STORE + "} = ?store AND {" + OrderModel.STATUS + " } IN (?orderStatus) ";

    protected static final String ORDER_CONFIRMATION_DETAILS_BY_CUSTOMER_STORE_QUERY = "SELECT {order.pk} FROM {BaseStore as basestore JOIN sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk} JOIN Order as order ON {sapconfiguration.sapcommon_salesOrganization} = {order.salesOrganization} JOIN OrderStatus as os ON {order.status} = {os.PK}} WHERE {os.PK} = {order.status} AND {order.versionID} IS NULL AND {order.store} = ?store AND {order.status} IN (?status) AND {order.sapOrderId}= ?sapOrderId AND {order.unit} IN (?unit) AND {basestore.pk} = ?store AND {order.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) AND {order.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    protected static final String FIND_ORDER_CONFIRMATION_BY_CODE_UNIT_STORE_QUERY = "SELECT {order.pk} FROM {BaseStore as basestore JOIN sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk} JOIN Order as order ON {sapconfiguration.sapcommon_salesOrganization} = {order.salesOrganization} JOIN OrderStatus as os ON {order.status} = {os.PK}} WHERE {os.PK} = {order.status} AND {order.versionID} IS NULL AND {order.store} = ?store AND {order.status} IN (?status) AND {order.code}= ?code AND {order.unit} IN (?unit) AND {basestore.pk} = ?store AND {order.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) AND {order.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    protected static final String FIND_ORDER_CANCELLATION_BY_CODE_UNIT_STORE_QUERY = "SELECT {ord." + OrderModel.PK + "}  FROM {" + OrderModel._TYPECODE + " AS ord}, {OrderStatus AS os} WHERE {os.PK} = {ord.status} AND {" + OrderModel.CODE + "} = ?code AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.UNIT + "} IN (?unit) AND {" + OrderModel.STATUS + "} IN (?status) AND {" + OrderModel.STORE + "} = ?store";

    protected static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY + " AND {" + OrderModel.STATUS + "} IN (?statusList)";

    protected static final String FIND_ORDERS_BY_UNIT_CODE_STORE_QUERY = "SELECT {ord." + OrderModel.PK + "}, {ord." + OrderModel.CREATIONTIME + "}, {ord." + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + " AS ord}, {OrderStatus AS os} WHERE {os.PK} = {ord.status} AND {" + OrderModel.CODE + "} = ?code AND {" + OrderModel.INITIALORDERVERSION + "} = 1 AND {" + OrderModel.UNIT + "} IN (?unit) AND {" + OrderModel.STORE + "} = ?store";
    protected static final String ORDER_ACKN_SEARCH_COUNT              = "order.acknowledgement.search.count";

    private TkEuB2bUnitService   b2bUnitService;
    private ConfigurationService configurationService;

    @Override
    public OrderModel findOrderByCodeAndStore(final String code, final BaseStoreModel store) {
        return super.findOrderByCodeAndStore(code, store);
    }

    @Override
    public SearchPageData<OrderModel> findOrdersByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status, final PageableData pageableData) {
        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(store, "Store must not be null");

        final Map<String, Object> queryParams = loadQueryParams(customerModel, store, status);

        String filterClause = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(getFilterOrderStatusList())) {
            queryParams.put("filterStatusList", getFilterOrderStatusList());
            filterClause = FILTER_ORDER_STATUS;
        }

        final List<SortQueryData> sortQueries;

        String query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS;

        if (ArrayUtils.isNotEmpty(status)) {
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        } else {
            query = FIND_ORDERS_BY_UNIT_STORE_QUERY;
            if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
                query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY;
            }
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        }

        return getPagedFlexibleSearchService().search(sortQueries, "byDateDesc", queryParams, pageableData);
    }

    @Override
    public SearchPageData<OrderModel> orderAcknowledgementSearchList(final String searchKey, final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status, final PageableData pageableData) {
        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(store, "Store must not be null");

        final Map<String, Object> queryParams = loadQueryParams(customerModel, store, status);

        String filterClause = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(getFilterOrderStatusList())) {
            queryParams.put("filterStatusList", getFilterOrderStatusList());
            filterClause = FILTER_ORDER_STATUS;
        }

        final List<SortQueryData> sortQueries;
        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        String query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS;

        if (ArrayUtils.isNotEmpty(status)) {
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        } else {
            query = FIND_ORDERS_BY_UNIT_STORE_QUERY + stringBuilder.toString();
            if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
                query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY + stringBuilder.toString();
            }
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        }

        return getPagedFlexibleSearchService().search(sortQueries, "byDateDesc", queryParams, pageableData);
    }

    @Override
    public OrderModel getOrderConfirmationDetailsForCode(final String sapOrderId, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(sapOrderId, "SAP order id must not be null");
        validateParameterNotNull(store, "Store must not be null");
        final Map<String, Object> queryParams = new HashMap<String, Object>();
        final List<OrderStatus> orderStatuses = getOrderConfirmationPageValidStatuses();
        queryParams.put("store", store);
        queryParams.put("status", orderStatuses);
        queryParams.put("sapOrderId", sapOrderId);
        queryParams.put("unit", b2BUnitModel);

        final OrderModel result = getFlexibleSearchService().searchUnique(new FlexibleSearchQuery(ORDER_CONFIRMATION_DETAILS_BY_CUSTOMER_STORE_QUERY, queryParams));
        return result;
    }

    @Override
    public OrderModel getConfirmedOrderByCode(final String code, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(code, "Code must not be null");
        validateParameterNotNull(store, "Store must not be null");
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("store", store);
        final List<OrderStatus> orderStatuses = getOrderConfirmationPageValidStatuses();
        queryParams.put("code", code);
        queryParams.put("unit", b2BUnitModel);
        queryParams.put("status", orderStatuses);
        return getFlexibleSearchService().searchUnique(new FlexibleSearchQuery(FIND_ORDER_CONFIRMATION_BY_CODE_UNIT_STORE_QUERY, queryParams));
    }

    @Override
    public OrderModel getCancelledOrderByCode(final String code, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(code, "Code must not be null");
        validateParameterNotNull(store, "Store must not be null");
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("store", store);
        final OrderStatus orderStatus = OrderStatus.CANCELLED;
        queryParams.put("code", code);
        queryParams.put("unit", b2BUnitModel);
        queryParams.put("status", orderStatus);
        return getFlexibleSearchService().searchUnique(new FlexibleSearchQuery(FIND_ORDER_CANCELLATION_BY_CODE_UNIT_STORE_QUERY, queryParams));
    }

    private void createSearchQueryStatement(final String searchKey, final StringBuilder stringBuilder, final Map queryParams) {
        final String[] queryStr = searchKey.toUpperCase().split("\\s+");
        stringBuilder.append(" AND ( ");
        int searchKeyCount = getConfigurationService().getConfiguration().getInt(ORDER_ACKN_SEARCH_COUNT);

        if (queryStr.length < searchKeyCount) {
            searchKeyCount = queryStr.length;
        }

        for (int i = 0; i < searchKeyCount; i++) {
            if (i != 0) {
                stringBuilder.append(" AND ");
            }
            stringBuilder.append(" ( {" + OrderModel.PURCHASEORDERNUMBER + "} COLLATE Latin1_General_CI_AS LIKE ?poNum" + i + " OR {" + OrderModel.CODE + "} COLLATE Latin1_General_CI_AS LIKE ?orderCode" + i + " OR {" + OrderModel.NAME + "} COLLATE Latin1_General_CI_AS LIKE ?orderName" + i + " )");

            queryParams.put("poNum" + i, "%" + queryStr[i] + "%");
            queryParams.put("orderCode" + i, "%" + queryStr[i] + "%");
            queryParams.put("orderName" + i, "%" + queryStr[i] + "%");
        }
        stringBuilder.append(")");
    }

    private Map<String, Object> loadQueryParams(final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status) {
        final Map<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put("store", store);

        if (ArrayUtils.isNotEmpty(status)) {
            queryParams.put("customer", customerModel);
            queryParams.put("statusList", Arrays.asList(status));
            return queryParams;
        }

        if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
            queryParams.put("customer", customerModel);
        } else {
            queryParams.put("unit", getAllB2bUnits(customerModel));
        }

        return queryParams;
    }

    private List<OrderStatus> getOrderConfirmationPageValidStatuses() {
        return Arrays.asList(OrderStatus.CONFIRMED, OrderStatus.CANCELLED);
    }

    @Override
    public OrderModel findOrderByCustomerAndCodeAndStore(final CustomerModel customerModel, final String code, final BaseStoreModel store) {
        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(code, "Code must not be null");
        validateParameterNotNull(store, "Store must not be null");

        if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
            return super.findOrderByCustomerAndCodeAndStore(customerModel, code, store);
        }

        final Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("unit", getAllB2bUnits(customerModel));
        queryParams.put("code", code);
        queryParams.put("store", store);
        final OrderModel result = getFlexibleSearchService().searchUnique(new FlexibleSearchQuery(FIND_ORDERS_BY_UNIT_CODE_STORE_QUERY, queryParams));
        return result;
    }

    @Override
    public Integer findOrderAcknowledgementListCount(final String searchKey, final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status) {

        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(store, "Store must not be null");

        final Map<String, Object> queryParams = loadQueryParams(customerModel, store, status);

        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        String query = FIND_ORDERS_BY_UNIT_STORE_QUERY + stringBuilder.toString();
        if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
            query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY + stringBuilder.toString();
        }

        final SearchResult<Integer> searchResult = getFlexibleSearchService().search(query, queryParams);

        return searchResult.getResult().size();
    }

    @Override
    public SearchPageData<OrderModel> orderConfirmationSearchList(final String searchKey, final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status, final PageableData pageableData) {
        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(store, "Store must not be null");
        final List<OrderStatus> orderStatuses = getOrderConfirmationPageValidStatuses();
        final Map<String, Object> queryParams = loadQueryParams(customerModel, store, status);
        queryParams.put("orderStatus", orderStatuses);
        String filterClause = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(getFilterOrderStatusList())) {
            queryParams.put("filterStatusList", getFilterOrderStatusList());
            filterClause = FILTER_ORDER_STATUS;
        }

        final List<SortQueryData> sortQueries;
        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createOrderConfirmationSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        String query = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS;

        if (ArrayUtils.isNotEmpty(status)) {
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_SAP_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_SAP_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        } else {
            query = FIND_ORDERS_CONFIRMATION_BY_UNIT_STORE_QUERY + stringBuilder.toString();
            if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
                query = FIND_ORDERS_CONFIRMATION_BY_CUSTOMER_STORE_QUERY + stringBuilder.toString();
            }
            sortQueries = Arrays.asList(createSortQueryData("byDateDesc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_DESC)), createSortQueryData("byDateAsc", createQuery(query, filterClause, SORT_ORDERS_BY_DATE_ASC)), createSortQueryData("byOrderNumberDesc", createQuery(query, filterClause, SORT_ORDERS_BY_SAP_CODE_DESC)), createSortQueryData("byOrderNumberAsc", createQuery(query, filterClause, SORT_ORDERS_BY_SAP_CODE_ASC)), createSortQueryData("byOrderNameDesc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_DESC)), createSortQueryData("byOrderNameAsc", createQuery(query, filterClause, SORT_ORDERS_BY_NAME_ASC)), createSortQueryData("byOrderStatusDesc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_DESC)), createSortQueryData("byOrderStatusAsc", createQuery(query, filterClause, SORT_ORDERS_BY_STATUS_ASC)), createSortQueryData("byOrderPODesc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_DESC)), createSortQueryData("byOrderPOAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PO_ASC)), createSortQueryData("byOrderPriceDesc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_DESC)), createSortQueryData("byOrderPriceAsc", createQuery(query, filterClause, SORT_ORDERS_BY_PRICE_ASC)));
        }

        return getPagedFlexibleSearchService().search(sortQueries, "byDateDesc", queryParams, pageableData);
    }

    @Override
    public Integer findOrderConfirmationListCount(final String searchKey, final CustomerModel customerModel, final BaseStoreModel store, final OrderStatus[] status) {

        validateParameterNotNull(customerModel, "Customer must not be null");
        validateParameterNotNull(store, "Store must not be null");
        final List<OrderStatus> orderStatuses = getOrderConfirmationPageValidStatuses();
        final Map<String, Object> queryParams = loadQueryParams(customerModel, store, status);
        queryParams.put("orderStatus", orderStatuses);
        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createOrderConfirmationSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        String query = FIND_ORDERS_CONFIRMATION_BY_UNIT_STORE_QUERY + stringBuilder.toString();
        if (((B2BCustomerModel) customerModel).getDefaultB2BUnit() == null) {
            query = FIND_ORDERS_CONFIRMATION_BY_CUSTOMER_STORE_QUERY + stringBuilder.toString();
        }

        final SearchResult<Integer> searchResult = getFlexibleSearchService().search(query, queryParams);

        return searchResult.getResult().size();
    }

    private void createOrderConfirmationSearchQueryStatement(final String searchKey, final StringBuilder stringBuilder, final Map queryParams) {
        final String[] queryStr = searchKey.toUpperCase().split("\\s+");
        stringBuilder.append(" AND ( ");
        int searchKeyCount = getConfigurationService().getConfiguration().getInt(ORDER_ACKN_SEARCH_COUNT);

        if (queryStr.length < searchKeyCount) {
            searchKeyCount = queryStr.length;
        }

        for (int i = 0; i < searchKeyCount; i++) {
            if (i != 0) {
                stringBuilder.append(" AND ");
            }
            stringBuilder.append(" ( { ord." + OrderModel.PURCHASEORDERNUMBER + "} COLLATE Latin1_General_CI_AS LIKE ?poNum" + i + " OR { ord." + OrderModel.SAPORDERID + "} COLLATE Latin1_General_CI_AS LIKE ?sapOrderId" + i + " OR { ord." + OrderModel.NAME + "} COLLATE Latin1_General_CI_AS LIKE  ?orderName" + i + " )");

            queryParams.put("poNum" + i, "%" + queryStr[i] + "%");
            queryParams.put("sapOrderId" + i, "%" + queryStr[i] + "%");
            queryParams.put("orderName" + i, "%" + queryStr[i] + "%");
        }
        stringBuilder.append(")");
    }

    @Override
    public List<CustomerModel> getLockedCustomersList(final int minimumLockedDuration) {
        final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        // final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {user." + UserModel.PK
        // + "} " + "FROM {" + UserModel._TYPECODE + " AS user} " + "WHERE {" +
        // UserModel.LOGINDISABLED + "} = true AND ( ?timestamp >
        // DATE_ADD({loginDisabledTime},INTERVAL ?minimumLockedDuration MINUTE)) ");
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {user." + UserModel.PK + "} " + "FROM {" + UserModel._TYPECODE + " AS user} " + "WHERE {" + UserModel.LOGINDISABLED + "} = 1 AND ( ?timestamp > DATEADD(MINUTE,?minimumLockedDuration,{loginDisabledTime})) ");
        fQuery.addQueryParameter("timestamp", timestamp.toString());
        fQuery.addQueryParameter("minimumLockedDuration", minimumLockedDuration);

        final SearchResult<CustomerModel> result = getFlexibleSearchService().search(fQuery);

        return result.getResult();
    }

    private Object getAllB2bUnits(final CustomerModel customerModel) {
        return b2bUnitService.getCurrentAndParentB2BUnits((B2BCustomerModel) customerModel);
    }

    public void setB2bUnitService(final TkEuB2bUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}

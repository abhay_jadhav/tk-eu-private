package com.thyssenkrupp.b2b.eu.core.facade.impl;

import com.thyssenkrupp.b2b.eu.core.facade.RegistrationFacade;
import com.thyssenkrupp.b2b.eu.core.service.RegistrationService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;

import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.CustomerModel;

public class DefaultRegistrationFacade implements RegistrationFacade {

    private RegistrationService registrationService;
    private TkEuB2bCustomerAccountService b2bCustomerAccountService;

    @Override
    public boolean validateRegistrationToken(final SecureToken data) {
        return registrationService.validateRegistrationToken(data);
    }

    @Override
    public void resendEmail(final CustomerModel customerModel) {
        getB2bCustomerAccountService().sendRegistrationEmail(customerModel);
    }

    public RegistrationService getRegistrationService() {
        return registrationService;
    }

    public void setRegistrationService(final RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    public TkEuB2bCustomerAccountService getB2bCustomerAccountService() {
        return b2bCustomerAccountService;
    }

    public void setB2bCustomerAccountService(final TkEuB2bCustomerAccountService b2bCustomerAccountService) {
        this.b2bCustomerAccountService = b2bCustomerAccountService;
    }

}


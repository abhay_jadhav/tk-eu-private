package com.thyssenkrupp.b2b.eu.core.dao;

import com.thyssenkrupp.b2b.eu.core.model.ConfKeyValueModel;

/**
 *
 */
public interface TkEuConfigKeyValueDao {
    ConfKeyValueModel getProperty(String key);

}

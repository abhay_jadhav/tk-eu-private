package com.thyssenkrupp.b2b.eu.core.atp.service.strategies;

import static java.text.MessageFormat.format;
import static java.util.Collections.emptyList;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.isNoneEmpty;

import de.hybris.platform.basecommerce.enums.InStockStatus;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.atp.exchanges.response.AtpResponse;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.response.SapAtpResponse;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;

public class SapAtpResponseStrategy {

    private static final Logger   LOG                      = Logger.getLogger(SapAtpResponseStrategy.class);
    private static final String[] DATE_FORMATS             = new String[] { "yyyy-MM-dd" };
    private static final String   SAP_NON_DELIVERABLE_DATE = "0000-00-00";
    private static final String SAP_NON_DELIVERABLE_DATE_CHECK_ENABLED = "atp.sap.response.notavailable.fallback.disabled";

    public List<AtpResponseData> buildSapAtpResponse(final SapAtpResponse sapAtpResponse) {
        if (nonNull(sapAtpResponse)) {
            return emptyIfNull(sapAtpResponse.getAtpResponse()).stream().map(this::generateAtpResponseData).collect(Collectors.toList());
        }
        return emptyList();
    }

    private AtpResponseData generateAtpResponseData(final AtpResponse atpResponse) {
        try {
            return validateAndGetSAPReponse(atpResponse, TkEuConfigKeyValueUtils.getBoolean(SAP_NON_DELIVERABLE_DATE_CHECK_ENABLED, false));

        } catch (final Exception e) {
            LOG.error("Key Not defined!!" + e.getMessage());
        }
        return validateAndGetSAPReponse(atpResponse, false);

    }

    /**
     *
     */
    private AtpResponseData validateAndGetSAPReponse(final AtpResponse atpResponse, final boolean isSAPATPDateCheckEnabled) {
        final AtpResponseData data = new AtpResponseData();
        try {
            if (isNoneEmpty(atpResponse.getLineItemNumber())) {
            data.setPosition(Integer.parseInt(atpResponse.getLineItemNumber()));
        }
        if (isNoneEmpty(atpResponse.getMaterialNumber())) {
            data.setProductCode(atpResponse.getMaterialNumber());
        }
            if (isNoneEmpty(atpResponse.getDeliveryDate()) && !SAP_NON_DELIVERABLE_DATE.equals(atpResponse.getDeliveryDate())) {
                data.setSapAtpResult(DateUtils.parseDateStrictly(atpResponse.getDeliveryDate(), DATE_FORMATS));
                data.setValidSAPAtpResult(true);
                return data;
            } else if (isNoneEmpty(atpResponse.getDeliveryDate()) && SAP_NON_DELIVERABLE_DATE.equals(atpResponse.getDeliveryDate()) && isSAPATPDateCheckEnabled) {
                data.setHybrisAtpResult(InStockStatus.NOTAVAILABLE);
                data.setValidSAPAtpResult(true);
                return data;
            } else {
                LOG.info("SAP ATP date check is disabled!!Hence calling Fallback");
                data.setValidSAPAtpResult(false);
                return data;
            }
        } catch (final ParseException e) {
            LOG.warn(format("Error parsing SAP AtpResponse.DeliveryDate:{0} for position:{1} and productCode:{2}", atpResponse.getDeliveryDate(), atpResponse.getLineItemNumber(), atpResponse.getMaterialNumber()));
        }
        data.setValidSAPAtpResult(false);
        return data;
    }

}

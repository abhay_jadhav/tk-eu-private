package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.impl;

import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.TkEuFacetSearchService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.IndexedTypeFieldsValuesProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class TkEuSolrFacetSearchService extends DefaultFacetSearchService implements TkEuFacetSearchService {

    private String searchablePropertyName;
    private List<String> fieldNames;

    @Override
    public SearchQuery createProductSearchQuery(FacetSearchConfig facetSearchConfig, IndexedType indexedType, String productCode) {
        SearchQuery searchQuery = this.createSearchQuery(facetSearchConfig, indexedType);
        populateProductResponseFields(facetSearchConfig, indexedType, searchQuery);
        if (StringUtils.isNotBlank(productCode)) {
            this.populateProductSearchQuery(facetSearchConfig, indexedType, searchQuery, productCode);
        }
        return searchQuery;
    }

    private void populateProductResponseFields(FacetSearchConfig facetSearchConfig, IndexedType indexedType, SearchQuery searchQuery) {

        if (CollectionUtils.isEmpty(getFieldNames())) {
            super.populateFields(facetSearchConfig, indexedType, searchQuery);
        } else {
            addFieldNames(indexedType, searchQuery);
        }
        IndexedTypeFieldsValuesProvider provider = this.getFieldsValuesProvider(indexedType);
        if (provider != null) {
            provider.getFieldNamesMapping().keySet().forEach(searchQuery::addField);
        }
    }

    private void addFieldNames(IndexedType indexedType, SearchQuery searchQuery) {
        final Map<String, IndexedProperty> indexedProperties = indexedType.getIndexedProperties();
        for (String fieldName : getFieldNames()) {
            final IndexedProperty indexedProperty = indexedProperties.get(fieldName);
            if (indexedProperty != null) {
                searchQuery.addField(indexedProperty.getName());
            }
        }
    }

    // searching user query against product code
    private void populateProductSearchQuery(FacetSearchConfig facetSearchConfig, IndexedType indexedType, SearchQuery searchQuery, String userQuery) {

        searchQuery.setFreeTextQueryBuilder(indexedType.getFtsQueryBuilder());
        if (MapUtils.isNotEmpty(indexedType.getFtsQueryBuilderParameters())) {
            searchQuery.getFreeTextQueryBuilderParameters().putAll(indexedType.getFtsQueryBuilderParameters());
        }
        searchQuery.setUserQuery(userQuery);

        if (StringUtils.isNotBlank(getSearchablePropertyName())) {
            final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(getSearchablePropertyName());
            if (indexedProperty != null) {
                searchQuery.addFreeTextQuery(indexedProperty.getName(), indexedProperty.getFtsQueryMinTermLength(), indexedProperty.getFtsQueryBoost());
            }
        }
    }

    public String getSearchablePropertyName() {
        return searchablePropertyName;
    }

    public void setSearchablePropertyName(String searchablePropertyName) {
        this.searchablePropertyName = searchablePropertyName;
    }

    public List<String> getFieldNames() {
        return fieldNames;
    }

    public void setFieldNames(List<String> fieldNames) {
        this.fieldNames = fieldNames;
    }
}

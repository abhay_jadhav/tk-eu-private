package com.thyssenkrupp.b2b.eu.core.cronjob;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;
import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;
import com.thyssenkrupp.b2b.eu.core.model.TkEuProductNameGenarationCronJobModel;
import com.thyssenkrupp.b2b.eu.core.product.dao.TkEuProductDao;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;

public class TkEuProductNameGenarationJob extends AbstractJobPerformable<TkEuProductNameGenarationCronJobModel> {
    private static final Logger LOG   = Logger.getLogger(TkEuProductNameGenarationJob.class);
    private static final String DE_DE = "de_DE";
    private static final String EN_US = "en_US";
    private static final String DE    = "de";
    private static final String EN    = "en";

    private TkEuProductDao                              productDao;
    private TkEuB2bProductService                       tkB2bProductService;
    private ModelService                                modelService;
    private CommonI18NService                           commonI18NService;
    private ClassificationService                       classificationService;
    private TkB2bCatalogProductReplicationConfigService productReplicationConfigService;
    private TkEuB2bCategoryDao tkCategoryDao;


    @Override
    public PerformResult perform(final TkEuProductNameGenarationCronJobModel job) {
        try {
            LOG.info("Started Product Name Generation CronJob");
            final BaseStoreModel currentBaseStore = job.getBaseStore();
            if (currentBaseStore != null && currentBaseStore.getSAPConfiguration() != null) {
                final String salesOrganization = currentBaseStore.getSAPConfiguration().getSapcommon_salesOrganization();
                LOG.info("Fetching Product Catalog Version for salesOrganization: " + salesOrganization);
                final CatalogVersionModel catalogVersion = tkCategoryDao.fetchProductCatalogBySalesOrganization(salesOrganization);
                if (catalogVersion != null) {
                    LOG.info("Fetching products for " + catalogVersion.getCatalog().getName());
                    final List<ProductModel> productModels = productDao.findProductsByCatalogAndLifeCycleStatus(catalogVersion,ProductLifeCycleStatus.TK_020_READY_FOR_NAME_GENERATION);
                    for (final ProductModel product : productModels) {
                        final CategoryModel classificationClassCategory = tkB2bProductService.getClassificationCategoryForProduct(product);
                        if (classificationClassCategory != null) {
                            LOG.info("Classification Category for the product :" + product.getCode() + " : " + classificationClassCategory.getCode());
                            populateClassificationAttribute(product, classificationClassCategory, catalogVersion);
                        } else {
                            LOG.info("No classification Category for the product :" + product.getCode());
                        }
                    }
                }
            }
            LOG.info("Finished TkEu Product Name Genaration CronJob");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while Creating TkEu Product Name Genaration CronJob", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void populateClassificationAttribute(final ProductModel product, final CategoryModel classificationCategory, final CatalogVersionModel version) {
        final String[] attributeList = fetchNameGenerationRules(classificationCategory, version);
        LOG.info("Name generation Rule :" + Arrays.toString(attributeList));
        if (attributeList.length != 0) {

            for (final LanguageModel languageModel : commonI18NService.getAllLanguages()) {
                final StringBuilder productName = new StringBuilder();
                createProductName(product, classificationCategory, attributeList, productName, languageModel);
            }
        }
    }

    private void createProductName(final ProductModel product, final CategoryModel classificationCategory, final String[] attributeList,
      final StringBuilder productName, final LanguageModel languageModel) {
        Locale locale = commonI18NService.getLocaleForLanguage(languageModel);
        for (final String attribute : attributeList) {
            LOG.info("Name generation Rule Attribute :" + attribute);
            final FeatureList variantFeatureList = classificationService.getFeatures(product);
            final Feature feature = getFeatureByAttribute(variantFeatureList, classificationCategory, attribute);
            if (feature.getValue() != null) {
                LOG.info("Product feature value :" + feature.getValue());
                fetchAttributeValue(feature, productName, locale, languageModel);
            } else {
                LOG.info("Product feature value is NULL. Taking the variant");
                fetchAttributeValueFromVariant(product, classificationCategory, productName, languageModel, locale, attribute);
                LOG.info("Feature value is null for the attribute : " + attribute + " for the product : " + product.getCode());
            }
        }

        if (!languageModel.getIsocode().equals(EN) && !languageModel.getIsocode().equals(DE)) {
            if (languageModel.getIsocode().equals(EN_US)) {
                locale = Locale.ENGLISH;
            } else if (languageModel.getIsocode().equals(DE_DE)) {
                locale = Locale.GERMAN;
            }
            updateProduct(product, productName.toString(), locale);
        }
    }

    private void fetchAttributeValueFromVariant(final ProductModel product, final CategoryModel classificationCategory, final StringBuilder productName,
      final LanguageModel languageModel, final Locale locale, final String attribute) {
        Feature feature;
        final Collection<VariantProductModel> variantProductModels = product.getVariants();
        if (CollectionUtils.isNotEmpty(variantProductModels)) {
            LOG.info("Variant product : " + variantProductModels.iterator().next());
            final FeatureList variantFeatureList = classificationService.getFeatures(variantProductModels.iterator().next());
            feature = getFeatureByAttribute(variantFeatureList, classificationCategory, attribute);
            if (feature.getValue() != null) {
                fetchAttributeValue(feature, productName, locale, languageModel);
            }
        }
    }

    public void fetchAttributeValue(final Feature feature, final StringBuilder productName, final Locale locale, final LanguageModel languageModel) {
        String value = tkB2bProductService.getFeatureValueForLocale(feature.getValue(), locale);
        if (StringUtils.isNotEmpty(value)) {
            productName.append(value + " ");
        } else {
            if (CollectionUtils.isNotEmpty(languageModel.getFallbackLanguages())) {
                final Locale loc = commonI18NService.getLocaleForLanguage(languageModel.getFallbackLanguages().get(0));
                value = tkB2bProductService.getFeatureValueForLocale(feature.getValue(), loc);
                if (StringUtils.isNotEmpty(value)) {
                    productName.append(value + " ");
                }
            }
        }
    }

    public void updateProduct(final ProductModel product, final String productName, final Locale locale) {
        product.setName(productName, locale);
        product.setLifeCycleStatus(ProductLifeCycleStatus.TK_030_READY_FOR_CATEGORY_CREATION);
        modelService.save(product);
        modelService.refresh(product);
        updateVariantProduct(product, productName, locale);
        LOG.info(product.getCode() + " is updated with name : " + productName + " For the locale : " + locale);
    }

    public void updateVariantProduct(final ProductModel product, final String productName, final Locale locale) {
        final Collection<VariantProductModel> variantProductModels = product.getVariants();
        final List<VariantProductModel> list = new ArrayList<>();
        LOG.debug("Updating the variant ");
        for (final VariantProductModel variantProductModel : variantProductModels) {
            LOG.info("Variant product : " + variantProductModel.getCode() + " Product name :" + productName);
            variantProductModel.setName(productName, locale);
            list.add(variantProductModel);
        }
        modelService.saveAll(list);
    }

    private String[] fetchNameGenerationRules(final CategoryModel classificationCategory, final CatalogVersionModel version) {
        final Optional<String> sOptional = productReplicationConfigService.getNameGenerationRule(version.getCatalog(), classificationCategory.getCode());
        String categoryRules = "";
        if (sOptional.isPresent()) {
            categoryRules = sOptional.get();
            LOG.info(" Rule configuration for " + classificationCategory.getCode() + " : " + categoryRules);
        } else {
            LOG.warn(" No Rule configuration found for " + classificationCategory.getCode());
        }
        return (StringUtils.isEmpty(categoryRules)) ? ArrayUtils.EMPTY_STRING_ARRAY : categoryRules.split(ThyssenkruppeucoreConstants.REGEX_COMMA);
    }

    private Feature getFeatureByAttribute(final FeatureList featureList, final CategoryModel classificationCategory, final String attribute) {
        return featureList.getFeatureByCode("ERP_CLASSIFICATION_001/ERP_IMPORT/" + classificationCategory.getCode() + "." + attribute.toLowerCase());
    }

    public void setProductDao(final TkEuProductDao productDao) {
        this.productDao = productDao;
    }


    public void setTkB2bProductService(final TkEuB2bProductService tkB2bProductService) {
        this.tkB2bProductService = tkB2bProductService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public void setProductReplicationConfigService(final TkB2bCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }


    /**
     * @return the tkCategoryDao
     */
    public TkEuB2bCategoryDao getTkCategoryDao() {
        return tkCategoryDao;
    }

    /**
     * @param tkCategoryDao
     *            the tkCategoryDao to set
     */
    public void setTkCategoryDao(final TkEuB2bCategoryDao tkCategoryDao) {
        this.tkCategoryDao = tkCategoryDao;
    }

}

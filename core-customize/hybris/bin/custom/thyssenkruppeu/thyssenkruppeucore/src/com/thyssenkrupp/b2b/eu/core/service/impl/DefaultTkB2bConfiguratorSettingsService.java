package com.thyssenkrupp.b2b.eu.core.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TK_EU_CATEGORY_CATALOG_ID;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultConfiguratorSettingsService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bConfiguratorSettingsService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;


public class DefaultTkB2bConfiguratorSettingsService extends DefaultConfiguratorSettingsService implements TkB2bConfiguratorSettingsService {

    private static final String                CONFIGURATION_SETTINGS_QUERY = "SELECT {pk} FROM {AbstractConfiguratorSetting} WHERE {id}=?id AND {catalogVersion}=?catalogVersion";
    private static final Logger                LOG                          = Logger.getLogger(DefaultTkB2bConfiguratorSettingsService.class);
    private              FlexibleSearchService flexibleSearchService;
    private              CatalogVersionService catalogVersionService;

    @Override
    public <T extends AbstractConfiguratorSettingModel> Optional<T> getConfiguratorSettingById(final String id) {
        final CatalogVersionModel storeCatalog = getCatalogVersionService().getCatalogVersion(TK_EU_CATEGORY_CATALOG_ID, CatalogManager.ONLINE_VERSION);
        return getConfiguratorSettingById(id, storeCatalog);
    }

    @Override
    public <T extends AbstractConfiguratorSettingModel> Optional<T> getConfiguratorSettingById(final String id, final CatalogVersionModel catalogVersion) {
        validateParameterNotNullStandardMessage("id", id);
        validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);
        try {
            final FlexibleSearchQuery query = new FlexibleSearchQuery(CONFIGURATION_SETTINGS_QUERY);
            query.addQueryParameter("id", id);
            query.addQueryParameter("catalogVersion", catalogVersion);
            return Optional.ofNullable(getFlexibleSearchService().searchUnique(query));
        } catch (final ModelNotFoundException e) {
            LOG.error("Attempted to search a ConfiguratorSetting that does not exist, searched Id:" + id, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<TkLengthConfiguratorSettingsModel> getFirstTkLengthConfiguratorSetting(@Nonnull final ProductModel product) {
        return emptyIfNull(getConfiguratorSettingsForProduct(product)).stream()
            .filter(TkLengthConfiguratorSettingsModel.class::isInstance).map(TkLengthConfiguratorSettingsModel.class::cast).findFirst();
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

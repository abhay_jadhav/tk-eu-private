package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.TkEuInvoiceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDao;
import com.thyssenkrupp.b2b.eu.core.enums.DocumentDownloadServiceEnum;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentSAPContent;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAP;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPSingleResponse;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountDeliveryService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountInvoiceService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuDocumentDownloadService;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class TkEuMockDocumentDownloadService implements TkEuDocumentDownloadService {

    public static final  String ORDER_DOC    = "ORDER";
    public static final  String DELIVERY_DOC = "DELIVERY";
    public static final  String INVOICE_DOC  = "INVOICE";
    private static final String CERTIFICATE  = "Certificate";
    private static final Logger LOG          = LoggerFactory.getLogger(TkEuSAPDocumentDownloadService.class);
    private TkEuCustomerAccountDeliveryService           customerAccountDeliveryService;
    private DefaultB2BCommerceUnitService                defaultB2BCommerceUnitService;
    private UserService                                  userService;
    private TkEuCustomerAccountInvoiceService            invoiceService;
    private Converter<InvoiceModel, TkEuInvoiceData>     invoiceConverter;
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;
    private BaseStoreService                             baseStoreService;
    private TkEuCustomerAccountDao                       tkEuCustomerAccountDao;
    private DefaultTkEuB2BUnitService                    b2bUnitService;

    @Override
    public Object getDocumentStatus(final DocumentDownloadRequest documentDownloadRequest) {

        String orderType = "";
        String orderNumber = "";
        for (final DocumentRequest documentRequest : documentDownloadRequest.getDocumentRequests()) {
            orderNumber = documentRequest.getDocumentNo();
            orderType = documentRequest.getDocumenType();
        }
        //Order Confirmation Document
        if (ORDER_DOC.equalsIgnoreCase(orderType)) {
            return getOrderSAPResponse(orderNumber, documentDownloadRequest);
        }
        //Delivery Document
        if (DELIVERY_DOC.equalsIgnoreCase(orderType) || CERTIFICATE.equalsIgnoreCase(orderType)) {
            return getDeliverySAPResponse(orderNumber, documentDownloadRequest);
        }
        //Invoice Document
        if (INVOICE_DOC.equalsIgnoreCase(orderType)) {
            return getInvoiceSAPResponse(orderNumber, documentDownloadRequest);
        }
        return false;
    }

    private Object getOrderSAPResponse(final String orderNumber, final DocumentDownloadRequest documentDownloadRequest) {
        final Boolean isOrderDocument = getOrderDocument(orderNumber);
        final GetDocumentStatusSAPSingleResponse getDocumentStatusSAPSingleResponse = new GetDocumentStatusSAPSingleResponse();
        if (isOrderDocument != null) {
            final GetDocumentStatusSAP getDocumentStatusSAP = new GetDocumentStatusSAP();
            getDocumentStatusSAP.setDocumentNo(documentDownloadRequest.getDocumentRequests().get(0).getDocumentNo());
            getDocumentStatusSAP.setBatchNo(documentDownloadRequest.getDocumentRequests().get(0).getBatchNo());
            getDocumentStatusSAP.setLineItemNumber(documentDownloadRequest.getDocumentRequests().get(0).getLineItemNo());
            getDocumentStatusSAP.setDocumentExistsFlag(isOrderDocument ? "1" : "0");
            getDocumentStatusSAPSingleResponse.setGetDocumentStatusSAP(getDocumentStatusSAP);
        }
        return getDocumentStatusSAPSingleResponse;
    }

    private Object getDeliverySAPResponse(final String orderNumber, final DocumentDownloadRequest documentDownloadRequest) {
        final Boolean isDeliveryDocument = getDeliveryDocument(orderNumber);
        if (isDeliveryDocument != null) {
            if (CollectionUtils.size(documentDownloadRequest.getDocumentRequests()) > 1) {
                final GetDocumentStatusSAPResponse response = new GetDocumentStatusSAPResponse();
                final List<GetDocumentStatusSAP> getDocumentStatusSAPList = new ArrayList<>();
                for (final DocumentRequest documentRequest : documentDownloadRequest.getDocumentRequests()) {
                    final GetDocumentStatusSAP getDocumentStatusSAP = new GetDocumentStatusSAP();
                    getDocumentStatusSAP.setDocumentNo(documentRequest.getDocumentNo());
                    getDocumentStatusSAP.setBatchNo(documentRequest.getBatchNo());
                    getDocumentStatusSAP.setLineItemNumber(documentRequest.getLineItemNo());
                    getDocumentStatusSAP.setDocumentExistsFlag(isDeliveryDocument ? "1" : "0");
                    getDocumentStatusSAPList.add(getDocumentStatusSAP);
                }
                response.setGetDocumentStatusSAP(getDocumentStatusSAPList);
                return response;
            } else {
                final GetDocumentStatusSAPSingleResponse getDocumentStatusSAPSingleResponse = new GetDocumentStatusSAPSingleResponse();
                final GetDocumentStatusSAP getDocumentStatusSAP = new GetDocumentStatusSAP();
                getDocumentStatusSAP.setDocumentNo(documentDownloadRequest.getDocumentRequests().get(0).getDocumentNo());
                getDocumentStatusSAP.setBatchNo(documentDownloadRequest.getDocumentRequests().get(0).getBatchNo());
                getDocumentStatusSAP.setLineItemNumber(documentDownloadRequest.getDocumentRequests().get(0).getLineItemNo());
                getDocumentStatusSAP.setDocumentExistsFlag(isDeliveryDocument ? "1" : "0");
                getDocumentStatusSAPSingleResponse.setGetDocumentStatusSAP(getDocumentStatusSAP);

                return getDocumentStatusSAPSingleResponse;
            }
        }

        return null;
    }

    private Object getInvoiceSAPResponse(final String orderNumber, final DocumentDownloadRequest documentDownloadRequest) {
        final Boolean isInvoiceDocument = getInvoiceDocument(orderNumber);
        final GetDocumentStatusSAPSingleResponse getDocumentStatusSAPSingleResponse = new GetDocumentStatusSAPSingleResponse();
        if (isInvoiceDocument != null) {
            final GetDocumentStatusSAP getDocumentStatusSAP = new GetDocumentStatusSAP();
            getDocumentStatusSAP.setDocumentNo(documentDownloadRequest.getDocumentRequests().get(0).getDocumentNo());
            getDocumentStatusSAP.setBatchNo(documentDownloadRequest.getDocumentRequests().get(0).getBatchNo());
            getDocumentStatusSAP.setLineItemNumber(documentDownloadRequest.getDocumentRequests().get(0).getLineItemNo());
            getDocumentStatusSAP.setDocumentExistsFlag(isInvoiceDocument ? "1" : "0");
            getDocumentStatusSAPSingleResponse.setGetDocumentStatusSAP(getDocumentStatusSAP);
        }
        return getDocumentStatusSAPSingleResponse;
    }

    private Boolean getOrderDocument(final String orderNumber) {
        if ("11101".equals(orderNumber) || "11102".equals(orderNumber) || "11103".equals(orderNumber)) {
            LOG.error("Error while Order Confirmation Document existence check");
            return false;
        }
        //Time out
        if ("11110".equals(orderNumber) || "11120".equals(orderNumber) || "11130".equals(orderNumber)) {
            LOG.error("Error time out");
            return false;
        }
        final OrderModel order = getOrderConfirmationDetailsForCode(orderNumber);
        if (order != null) {
            final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
            final Integer orderEntryCount = orderEntries.size();
            if (orderEntryCount != null && orderEntryCount % 2 == 0) {
                return true;
            } else {
                return false;
            }
        }
        return null;
    }

    private Boolean getInvoiceDocument(final String orderNumber) {
        if ("33301".equals(orderNumber) || "33302".equals(orderNumber) || "33303".equals(orderNumber)) {
            LOG.error("Error while Order Invoice Document existence check");
            return false;
        }
        //Time out
        if ("33310".equals(orderNumber) || "33320".equals(orderNumber) || "33330".equals(orderNumber)) {
            LOG.error("Error time out");
            return false;
        }
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        final List<InvoiceModel> invoiceModels = getInvoiceService().fetchInvoices(orderNumber, b2bUnits);
        final List<TkEuInvoiceData> invoices = getInvoiceConverter().convertAll(invoiceModels);
        if (CollectionUtils.isNotEmpty(invoices)) {

            final Integer invoiceCount = invoices.get(0).getInvoiceEntries().size();
            if (invoiceCount != null && invoiceCount % 2 == 0) {
                return true;
            } else {
                return false;
            }
        }
        return null;
    }

    private Boolean getDeliveryDocument(final String orderNumber) {
        if ("22201".equals(orderNumber) || "22202".equals(orderNumber) || "22203".equals(orderNumber)) {
            LOG.error("Error while Order Delivery Document existence check");
            return false;
        }
        //Time out
        if ("22210".equals(orderNumber) || "22220".equals(orderNumber) || "22230".equals(orderNumber)) {
            LOG.error("Error time out");
        }
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        final List<ConsignmentModel> consignmentModels = getCustomerAccountDeliveryService().fetchDeliveries(orderNumber, b2bUnits);
        final List<ConsignmentData> consignments = getConsignmentConverter().convertAll(consignmentModels);
        if (CollectionUtils.isNotEmpty(consignments)) {

            final Integer deliveryCount = consignments.get(0).getEntries().size();
            if (deliveryCount != null && deliveryCount % 2 == 0) {
                return true;
            } else {
                return false;
            }
        }
        return null;
    }

    public OrderModel getOrderConfirmationDetailsForCode(final String code) {
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
        return getTkEuCustomerAccountDao().getOrderConfirmationDetailsForCode(code, baseStoreModel, b2bUnits);
    }

    @Override
    public GetDocumentContentResponse getDocument(final DocumentDownloadRequest documentDownloadRequest) {
        final GetDocumentContentResponse contentResponse = new GetDocumentContentResponse();

        DocumentRequest documentRequest = null;
        for (final DocumentRequest document : documentDownloadRequest.getDocumentRequests()) {
            documentRequest = document;
        }
        final String documentNo = documentRequest != null ? documentRequest.getDocumentNo() : "";
        //Order Confirmation Document
        String documentType = null;
        if (documentRequest != null) {
            if (documentRequest.getDocumenType().equals(ORDER_DOC)) {

                documentType = getOrderDocumentDownloadstatus(documentNo);
            } else if (documentRequest.getDocumenType().equals(DELIVERY_DOC)) {

                documentType = getDeliveryDocumentDownloadStatus(documentNo);
            } else if (documentRequest.getDocumenType().equals(INVOICE_DOC)) {

                documentType = getInvoiceDocumentDownloadStatus(documentNo);
            }
        }

        contentResponse.setDocumentContent(setDocumentContentResponseType(documentType));
        return contentResponse;
    }

    private String getInvoiceDocumentDownloadStatus(final String documentNo) {
        String documentType;
        if ("31000".equals(documentNo) || "32000".equals(documentNo) || "33000".equals(documentNo)) {
            documentType = "FAILD";
        } else if ("33100".equals(documentNo) || "33200".equals(documentNo) || "33300".equals(documentNo)) {
            documentType = "TIFF";
        } else {
            documentType = "FAILD";
        }
        return documentType;
    }

    private String getDeliveryDocumentDownloadStatus(final String documentNo) {
        String documentType;
        if ("21000".equals(documentNo) || "22000".equals(documentNo) || "23000".equals(documentNo)) {
            documentType = "FAILD";
        } else if ("22100".equals(documentNo) || "22200".equals(documentNo) || "22300".equals(documentNo)) {
            documentType = "TIFF";
        } else {
            documentType = "FAILD";
        }
        return documentType;
    }

    private String getOrderDocumentDownloadstatus(final String documentNo) {
        String documentType;
        if ("11000".equals(documentNo) || "12000".equals(documentNo) || "13000".equals(documentNo)) {
            documentType = "FAILD";
        } else if ("11100".equals(documentNo) || "11200".equals(documentNo) || "11300".equals(documentNo)) {
            documentType = "TIFF";
        } else {
            documentType = "FAILD";
        }
        return documentType;
    }

    public GetDocumentSAPContent setDocumentContentResponseType(final String documentType) {
        final GetDocumentSAPContent docStatus = new GetDocumentSAPContent();
        docStatus.setContentType(documentType);
        return docStatus;
    }

    @Override
    public DocumentDownloadServiceEnum getType() {
        return DocumentDownloadServiceEnum.MOCK;
    }

    @Override
    public boolean isAuthorizedUser(final DocumentDownloadRequest documents) {
        return true;
    }

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    public void setDefaultB2BCommerceUnitService(final DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public Converter<InvoiceModel, TkEuInvoiceData> getInvoiceConverter() {
        return invoiceConverter;
    }

    public void setInvoiceConverter(final Converter<InvoiceModel, TkEuInvoiceData> invoiceConverter) {
        this.invoiceConverter = invoiceConverter;
    }

    public TkEuCustomerAccountInvoiceService getInvoiceService() {
        return invoiceService;
    }

    public void setInvoiceService(final TkEuCustomerAccountInvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    public TkEuCustomerAccountDao getTkEuCustomerAccountDao() {
        return tkEuCustomerAccountDao;
    }

    public void setTkEuCustomerAccountDao(final TkEuCustomerAccountDao tkEuCustomerAccountDao) {
        this.tkEuCustomerAccountDao = tkEuCustomerAccountDao;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuCustomerAccountDeliveryService getCustomerAccountDeliveryService() {
        return customerAccountDeliveryService;
    }

    public void setCustomerAccountDeliveryService(final TkEuCustomerAccountDeliveryService customerAccountDeliveryService) {
        this.customerAccountDeliveryService = customerAccountDeliveryService;
    }

    public Converter<ConsignmentModel, ConsignmentData> getConsignmentConverter() {
        return consignmentConverter;
    }

    public void setConsignmentConverter(final Converter<ConsignmentModel, ConsignmentData> consignmentConverter) {
        this.consignmentConverter = consignmentConverter;
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(final DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }
}

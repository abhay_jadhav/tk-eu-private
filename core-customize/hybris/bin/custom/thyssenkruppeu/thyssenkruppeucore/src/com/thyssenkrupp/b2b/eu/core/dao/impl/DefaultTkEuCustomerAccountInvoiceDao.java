package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel.CODE;
import static com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel.DATE;
import static com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel.DUEDATE;
import static com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel.TOTALGROSS;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountInvoiceDao;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceEntryModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class DefaultTkEuCustomerAccountInvoiceDao extends AbstractItemDao implements TkEuCustomerAccountInvoiceDao {

    protected static final String SEARCH_INVOICES_BY_UNIT_AND_STORE = "SELECT distinct {invoice.pk},{invoice.code},{invoice.date},{invoice.dueDate},{invoice.totalgross}" + " from {BaseStore as basestore \n" + "JOIN sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk}\n" + "JOIN Invoice as invoice ON {invoice.salesOrg} = {sapconfiguration.sapcommon_salesOrganization} \n" + "LEFT OUTER JOIN InvoiceEntry as invoiceentry ON {invoice.pk} ={invoiceentry.invoice}} \n" + "WHERE {invoice.unit} IN (?unit) AND {basestore.pk} = ?store\n" + "AND {invoice.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) \n" + "AND {invoice.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";
    protected static final String SORT_ORDERS_BY_INVOICE_NO_DESC = " ORDER BY {invoice." + CODE + "} DESC";
    protected static final String SORT_ORDERS_BY_INVOICE_NO_ASC = " ORDER BY {invoice." + CODE + "} ASC";
    protected static final String SORT_ORDERS_BY_DATE_ASC = " ORDER BY {invoice." + DATE + "} ASC";
    protected static final String SORT_ORDERS_BY_DATE_DESC = " ORDER BY {invoice." + DATE + "} DESC";
    protected static final String SORT_ORDERS_BY_DUE_DATE_ASC = " ORDER BY {invoice." + DUEDATE + "} ASC";
    protected static final String SORT_ORDERS_BY_DUE_DATE_DESC = " ORDER BY {invoice." + DUEDATE + "} DESC";
    protected static final String SORT_ORDERS_BY_TOTAL_ASC = " ORDER BY {invoice." + TOTALGROSS + "} ASC";
    protected static final String SORT_ORDERS_BY_TOTAL_DESC = " ORDER BY {invoice." + TOTALGROSS + "} DESC";

    protected static final String FIND_INVOICE_BY_CODE_UNIT = "SELECT {invoice.pk} from {BaseStore as basestore JOIN sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk} JOIN Invoice as invoice ON {invoice.salesOrg} = {sapconfiguration.sapcommon_salesOrganization}} WHERE {invoice.code} COLLATE Latin1_General_CI_AS LIKE ?code and {invoice.unit} IN (?unit) AND {basestore.pk} = ?store AND {invoice.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) AND {invoice.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    private PagedFlexibleSearchService pagedFlexibleSearchService;
    private ConfigurationService configurationService;
    private int invoiceSearchCount;

    @Override
    public SearchPageData<InvoiceModel> orderInvoiceSearchList(final String searchKey, final PageableData pageableData, final Set<B2BUnitModel> b2BUnitModel, final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(b2BUnitModel, "b2BUnit must not be null");
        validateParameterNotNull(baseStoreModel, "baseStoreModel must not be null");

        final Map<String, Object> queryParams = loadQueryParams(b2BUnitModel, baseStoreModel);

        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }
        final List<SortQueryData> sortQueries;

        sortQueries = Arrays.asList(createSortQueryData("byInvoiceNoDesc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_INVOICE_NO_DESC), createSortQueryData("byInvoiceNoAsc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_INVOICE_NO_ASC), createSortQueryData("byInvoiceDateAsc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_DATE_ASC), createSortQueryData("byInvoiceDateDesc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_DATE_DESC), createSortQueryData("byDueDateAsc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_DUE_DATE_ASC), createSortQueryData("byDueDateDesc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_DUE_DATE_DESC), createSortQueryData("byTotalAsc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_TOTAL_ASC), createSortQueryData("byTotalDesc", SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString() + SORT_ORDERS_BY_TOTAL_DESC));

        return getPagedFlexibleSearchService().search(sortQueries, "byInvoiceNoDesc", queryParams, pageableData);

    }

    @Override
    public int orderInvoicesSearchListCount(final String searchKey, final Set<B2BUnitModel> b2BUnitModel, final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(b2BUnitModel, "b2BUnitModel must not be null");
        validateParameterNotNull(baseStoreModel, "baseStoreModel must not be null");

        final Map<String, Object> queryParams = loadQueryParams(b2BUnitModel, baseStoreModel);

        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        final String query = SEARCH_INVOICES_BY_UNIT_AND_STORE + stringBuilder.toString();

        final SearchResult<Integer> searchResult = getFlexibleSearchService().search(query, queryParams);

        return searchResult.getResult().size();
    }

    private Map<String, Object> loadQueryParams(final Object b2BUnitModel, final BaseStoreModel baseStoreModel) {
        final Map<String, Object> queryParams = new HashMap();
        queryParams.put("unit", b2BUnitModel);
        queryParams.put("store", baseStoreModel);
        return queryParams;
    }

    @Override
    public List<InvoiceModel> fetchInvoices(final String invoiceNo, final Set<B2BUnitModel> b2bUnitModel,final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(invoiceNo, "Invoice Number must not be null");
        validateParameterNotNull(b2bUnitModel, "b2BUnitModel must not be null");
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("code", invoiceNo);
        queryParams.put("unit", b2bUnitModel);
        queryParams.put("store", baseStoreModel);

        final SearchResult<InvoiceModel> result = getFlexibleSearchService().search(FIND_INVOICE_BY_CODE_UNIT, queryParams);
        return result.getResult();
    }

    private void createSearchQueryStatement(final String searchKey, final StringBuilder stringBuilder, final Map queryParams) {
        final String[] queryStr = searchKey.toUpperCase().split("\\s+");
        stringBuilder.append(" AND ( ");
        int searchKeyCount = getInvoiceSearchCount();
        searchKeyCount = Math.min(queryStr.length, searchKeyCount);

        for (int i = 0; i < searchKeyCount; i++) {
            if (i != 0) {
                stringBuilder.append(" AND ");
            }
            stringBuilder.append(" ( {invoiceentry." + InvoiceEntryModel.PRODUCTCODECUSTOMER + "} COLLATE Latin1_General_CI_AS LIKE ?prodCode" + i + " OR {invoice." + CODE + "} COLLATE Latin1_General_CI_AS LIKE ?invoiceNo" + i + ")");

            queryParams.put("prodCode" + i, "%" + queryStr[i] + "%");
            queryParams.put("invoiceNo" + i, "%" + queryStr[i] + "%");
        }
        stringBuilder.append(")");
    }

    protected SortQueryData createSortQueryData(final String sortCode, final String query) {
        final SortQueryData result = new SortQueryData();
        result.setSortCode(sortCode);
        result.setQuery(query);
        return result;
    }

    public PagedFlexibleSearchService getPagedFlexibleSearchService() {
        return pagedFlexibleSearchService;
    }

    @Required
    public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService) {
        this.pagedFlexibleSearchService = pagedFlexibleSearchService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public int getInvoiceSearchCount() {
        return invoiceSearchCount;
    }

    public void setInvoiceSearchCount(final int invoiceSearchCount) {
        this.invoiceSearchCount = invoiceSearchCount;
    }

}

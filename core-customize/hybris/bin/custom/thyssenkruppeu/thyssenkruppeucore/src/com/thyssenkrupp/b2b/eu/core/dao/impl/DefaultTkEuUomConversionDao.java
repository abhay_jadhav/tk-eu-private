package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel.PRODUCTID;
import static com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel._TYPECODE;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.thyssenkrupp.b2b.eu.core.dao.TkUomConversionDao;
import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;

public class DefaultTkEuUomConversionDao extends DefaultGenericDao<TkUomConversionModel> implements TkUomConversionDao {

    private static final String UOM_CONVERSION_QUERY = "select {pk} from {" + _TYPECODE + "} where {" + PRODUCTID + "} = ?productCode ";

    public DefaultTkEuUomConversionDao() {
        super(_TYPECODE);
    }

    public List<TkUomConversionModel> findUomConversionByProductCode(final String productCode) {
        final SearchResult<TkUomConversionModel> searchResult = getFlexibleSearchService().search(getFlexibleQueryFilteredByProductCode(productCode));
        return searchResult.getResult();
    }

    private FlexibleSearchQuery getFlexibleQueryFilteredByProductCode(final String productCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(UOM_CONVERSION_QUERY);
        fQuery.addQueryParameter("productCode", productCode);
        return fQuery;
    }

}

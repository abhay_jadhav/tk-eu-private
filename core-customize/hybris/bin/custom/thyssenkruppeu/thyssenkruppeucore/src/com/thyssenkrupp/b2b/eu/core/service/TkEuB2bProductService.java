package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Locale;
import java.util.Optional;

import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;

public interface TkEuB2bProductService extends TkB2bProductService {

    Optional<MaterialShape> getProductShape(ProductModel productModel);

    Optional<FeatureValue> getProductWidth(ProductModel productModel, String widthFeatureCode);

    String getFeatureValueForLocale(FeatureValue value, Locale locale);

    ProductModel createBaseProductWithCode(String productId, CatalogVersionModel catalogVersion);

    void updateLifeCycleStatus(ProductModel product, ProductLifeCycleStatus status);

    void setVariantTypeAsERPVariantProduct(ProductModel product);

    void updateBaseProductForVariant(VariantProductModel variant, ProductModel product);
}

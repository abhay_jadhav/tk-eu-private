package com.thyssenkrupp.b2b.eu.core.cronjob;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.cronjob.dao.TkEuCronJobHistoryDao;
import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.model.TkEuVariantNameAssignmentCronJobModel;
import com.thyssenkrupp.b2b.eu.core.product.dao.TkEuProductDao;

public class TkEuVariantNameAssignmentJob extends AbstractJobPerformable<TkEuVariantNameAssignmentCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkEuVariantNameAssignmentJob.class);
    private static final String DE_DE = "de_DE";
    private static final String EN_US = "en_US";

    private TkEuB2bCategoryDao tkCategoryDao;
    private TkEuProductDao      productDao;
    private CommonI18NService commonI18NService;
    private TkEuCronJobHistoryDao cronJobHistoryDao;

    @Override
    public PerformResult perform(final TkEuVariantNameAssignmentCronJobModel tkEuVariantNameAssignmentCronJobModel) {
        try {
            LOG.info("Started Variant Name Assignment CronJob");
            final BaseStoreModel currentBaseStore = tkEuVariantNameAssignmentCronJobModel.getBaseStore();
            if (currentBaseStore != null && currentBaseStore.getSAPConfiguration() != null) {
                final String salesOrganization = currentBaseStore.getSAPConfiguration().getSapcommon_salesOrganization();
                final CatalogVersionModel catalogVersion = tkCategoryDao.fetchProductCatalogBySalesOrganization(salesOrganization);
                if (catalogVersion != null) {
                    LOG.info("Fetching products for " + catalogVersion.getCatalog().getName());
                    final Date lastJobRunDate = cronJobHistoryDao.getLastVariantNameAssignmentJobRunDate();
                    final List<ProductModel> productModels = productDao.findProductsByCatalogVersionModifiedSince(catalogVersion,lastJobRunDate);
                    for (final ProductModel product : productModels) {
                        assignNameToVariants(product);
                    }
                }
            }
            LOG.info("Finished TK EU variant name assignment CronJob");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while Creating TK EU variant name assignment CronJob", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void assignNameToVariants(final ProductModel product) {
        for (final LanguageModel languageModel : commonI18NService.getAllLanguages()) {
            if (languageModel.getIsocode().equals(EN_US) || languageModel.getIsocode().equals(DE_DE)) {
                assignName(product, languageModel);
            }
        }
    }

    private void assignName(final ProductModel product, final LanguageModel languageModel) {
        final Locale locale = commonI18NService.getLocaleForIsoCode(languageModel.getIsocode());
        String productName = product.getName(locale);

        if (StringUtils.isEmpty(productName)) {
            final Locale fallbackLocale = commonI18NService.getLocaleForLanguage(languageModel.getFallbackLanguages().get(0));
            productName = product.getName(fallbackLocale);
        }
        updateProduct(product, productName, locale);
    }

    public void updateProduct(final ProductModel product, final String productName, final Locale locale) {
        product.setName(productName, locale);
        modelService.save(product);
        modelService.refresh(product);
        updateVariantProduct(product, productName, locale);
        LOG.info(product.getCode() + " is updated with name : " + productName + " For the locale : " + locale);
    }

    public void updateVariantProduct(final ProductModel product, final String productName, final Locale locale) {
        final Collection<VariantProductModel> variantProductModels = product.getVariants();
        final List<VariantProductModel> list = new ArrayList<VariantProductModel>();
        LOG.debug("Updating the variant ");
        for (final VariantProductModel variantProductModel : variantProductModels) {
            LOG.info("Variant product : " + variantProductModel.getCode() + " Product name :" + productName);
            variantProductModel.setName(productName, locale);
            list.add(variantProductModel);
        }
        modelService.saveAll(list);
    }

    public void setTkCategoryDao(final TkEuB2bCategoryDao tkCategoryDao) {
        this.tkCategoryDao = tkCategoryDao;
    }

    public void setProductDao(final TkEuProductDao productDao) {
        this.productDao = productDao;
    }

    public void setCronJobHistoryDao(final TkEuCronJobHistoryDao cronJobHistoryDao) {
        this.cronJobHistoryDao = cronJobHistoryDao;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }
}

package com.thyssenkrupp.b2b.eu.core.checkout.flow.impl;

import de.hybris.platform.acceleratorservices.enums.CheckoutFlowEnum;
import de.hybris.platform.b2bacceleratorservices.order.checkout.flow.B2BCheckoutFlowStrategy;
import de.hybris.platform.b2bacceleratorservices.order.checkout.flow.impl.FixedB2BCheckoutFlowStrategy;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;

/**
 * Uid site mapping based flow strategy. If there is no mapping configured for current site uid uses
 * {@link #getDefaultStrategy()} flow.
 *
 * @since 4.6
 * @spring.bean siteCheckoutFlowStrategy
 * @deprecated Since 5.5.
 */
@Deprecated
public class SiteCheckoutFlowStrategy extends FixedB2BCheckoutFlowStrategy {
    private static final Logger LOG = Logger.getLogger(SiteCheckoutFlowStrategy.class);

    private CMSSiteService cmsSiteService;
    private Map<String, B2BCheckoutFlowStrategy> siteMappings;

    @Override
    public CheckoutFlowEnum getCheckoutFlow() {
        final String siteUid = getCurrentSiteUid();
        final B2BCheckoutFlowStrategy strategy = getSiteMappings().get(siteUid);

        if (strategy == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Using default  for given site " + siteUid);
            }
            return super.getCheckoutFlow();
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Found a " + strategy + " for given site " + siteUid);
            }
            return strategy.getCheckoutFlow();
        }
    }

    protected String getCurrentSiteUid() {
        final CMSSiteModel siteModel = getCmsSiteService().getCurrentSite();
        Preconditions.checkNotNull(siteModel, "Could not find current site");

        final String siteUid = siteModel.getUid();
        Preconditions.checkNotNull(siteUid, "Site uid for  current site " + siteModel + " is empty");
        return siteUid;
    }

    protected Map<String, B2BCheckoutFlowStrategy> getSiteMappings() {
        return siteMappings;
    }

    @Required
    public void setSiteMappings(final Map<String, B2BCheckoutFlowStrategy> strategiesMappings) {
        this.siteMappings = strategiesMappings;
    }

    protected CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }
}

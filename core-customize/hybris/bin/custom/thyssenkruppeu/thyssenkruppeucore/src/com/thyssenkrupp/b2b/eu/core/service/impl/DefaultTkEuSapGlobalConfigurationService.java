package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuSapGlobalConfigurationService;
import de.hybris.platform.sap.core.configuration.global.dao.SAPGlobalConfigurationDAO;
import de.hybris.platform.sap.sapmodel.model.SAPPricingSalesAreaToCatalogModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

public class DefaultTkEuSapGlobalConfigurationService implements TkEuSapGlobalConfigurationService {

    private SAPGlobalConfigurationDAO globalConfigurationDAO;
    private BaseStoreService          baseStoreService;

    @Override
    public Set<SAPPricingSalesAreaToCatalogModel> getSapPricingSalesAreaToCatalogModels() {
        if (getGlobalConfigurationDAO().getSAPGlobalConfiguration() == null) {
            return Collections.emptySet();
        }
        return getGlobalConfigurationDAO().getSAPGlobalConfiguration().getSapcommon_sapPricingSalesArea();
    }

    /**
     * @Deprecated use
     * {@link BaseStoreService#getCurrentBaseStore()#getSAPConfiguration()#getOfflineDistributionChannel()} instead.
     */
    @Deprecated
    @Nullable
    @Override
    public SAPPricingSalesAreaToCatalogModel getCurrentSapPricingSalesAreaToCatalogModel() {
        Set<SAPPricingSalesAreaToCatalogModel> sapPricingSalesAreaToCatalogModels = getSapPricingSalesAreaToCatalogModels();
        final String sapCommonSalesOrganization = getBaseStoreService().getCurrentBaseStore().getSAPConfiguration().getSapcommon_salesOrganization();
        Optional<SAPPricingSalesAreaToCatalogModel> firstSapPricingSalesAreaToCatalogModel = sapPricingSalesAreaToCatalogModels.stream().filter(sapPricingSalesAreaToCatalogModel -> sapPricingSalesAreaToCatalogModel.getSalesOrganization().equals(sapCommonSalesOrganization)).findFirst();
        if (firstSapPricingSalesAreaToCatalogModel.isPresent()) {
            SAPPricingSalesAreaToCatalogModel sapPricingSalesAreaToCatalogModel = firstSapPricingSalesAreaToCatalogModel.get();
            return sapPricingSalesAreaToCatalogModel;
        }

        return null;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public SAPGlobalConfigurationDAO getGlobalConfigurationDAO() {
        return globalConfigurationDAO;
    }

    @Required
    public void setGlobalConfigurationDAO(SAPGlobalConfigurationDAO globalConfigurationDAO) {
        this.globalConfigurationDAO = globalConfigurationDAO;
    }
}

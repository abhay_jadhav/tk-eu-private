
package com.thyssenkrupp.b2b.eu.core.cronjob.dao.impl;

import java.util.Date;

import com.thyssenkrupp.b2b.eu.core.cronjob.dao.TkEuCronJobHistoryDao;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;
import de.hybris.platform.servicelayer.cronjob.impl.DefaultCronJobHistoryDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

public class DefaultTkEuCronJobHistoryDao extends DefaultCronJobHistoryDao implements TkEuCronJobHistoryDao {
    @Override
    public Date getLastVariantNameAssignmentJobRunDate() {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("select {" + CronJobHistoryModel.PK + "} from {" + CronJobHistoryModel._TYPECODE + "} where {" + CronJobHistoryModel.CRONJOBCODE + "} = ?cronJobCode AND {" + CronJobHistoryModel.RESULT + "} =?result order by {startTime} desc");
        fQuery.addQueryParameter("cronJobCode", "tkSchulteSite-TkEuVariantNameAssignmentJob");
        fQuery.addQueryParameter("result", CronJobResult.SUCCESS);
        fQuery.setCount(1);
        final SearchResult<CronJobHistoryModel> result = getFlexibleSearchService().search(fQuery);

        if (result.getResult().size() > 0) {
            return result.getResult().get(0).getStartTime();
        }

        return new Date(0);
    }
}

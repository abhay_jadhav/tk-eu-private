package com.thyssenkrupp.b2b.eu.core.service.impl;

import static com.thyssenkrupp.b2b.eu.core.service.MaterialShape.FLAT;
import static com.thyssenkrupp.b2b.eu.core.service.MaterialShape.LONG;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Optional.empty;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.endsWithIgnoreCase;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;
import com.thyssenkrupp.b2b.eu.core.service.MaterialShape;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;

public class DefaultTkEuB2bProductService extends DefaultTkB2bProductService implements TkEuB2bProductService {
    private static final Logger LOG = Logger.getLogger(DefaultTkEuB2bProductService.class);

    private ClassificationService classificationService;
    @NotNull
    private List<String>          flatClassifications;
    @NotNull
    private List<String>          longClassifications;
    @NotNull
    private String                shapeMaterialFeature;
    @NotNull
    private TypeService           typeService;


    @Override
    public Optional<MaterialShape> getProductShape(final ProductModel productModel) {
        final FeatureList baseProductFeatureList = getBaseProductFeatureList(productModel);
        if (baseProductFeatureList != null) {
            final List<Feature> features = emptyIfNull(baseProductFeatureList.getFeatures());
            final List featureShapes = features.stream().filter(feature -> StringUtils.endsWith(feature.getCode(), getShapeMaterialFeature().toLowerCase())).collect(Collectors.toList());
            if (featureShapes.stream().anyMatch(isTypeOf(getLongClassifications()))) {
                return Optional.of(LONG);
            } else if (featureShapes.stream().anyMatch(isTypeOf(getFlatClassifications()))) {
                return Optional.of(FLAT);
            }
        }
        return empty();
    }

    @Override
    public Optional<FeatureValue> getProductWidth(final ProductModel productModel, final String widthFeatureCode) {
        final FeatureList productFeatureList = getClassificationService().getFeatures(productModel);
        if (productFeatureList != null) {
            final Optional<Feature> optFeature = emptyIfNull(productFeatureList.getFeatures()).stream().filter(feature -> endsWithIgnoreCase(feature.getCode(), widthFeatureCode)).findFirst();
            return optFeature.map(Feature::getValue);
        }

        return empty();
    }

    private FeatureList getBaseProductFeatureList(@NotNull final ProductModel productModel) {
        if (productModel instanceof VariantProductModel) {
            final VariantProductModel variantProduct = (VariantProductModel) productModel;
            return getClassificationService().getFeatures(variantProduct.getBaseProduct());
        }
        return getClassificationService().getFeatures(productModel);
    }

    @Override
    public String getFeatureValueForLocale(final FeatureValue value, final Locale locale) {
        if (value != null) {
            final Object featureValue = value.getValue();
            if (featureValue instanceof ClassificationAttributeValueModel) {
                final ClassificationAttributeValueModel attributeValueModel = (ClassificationAttributeValueModel) featureValue;
                LOG.info(" Feature Value from ClassificationAttributeValueModel: " + attributeValueModel.getName());
                return attributeValueModel.getName(locale);
            }
        }
        LOG.warn(" Empty Feature Value found");
        return StringUtils.EMPTY;
    }

    @Override
    public ProductModel createBaseProductWithCode(final String productId, final CatalogVersionModel catalogVersion) {
        validateParameterNotNullStandardMessage("productId", productId);
        validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);
        final ProductModel product = getModelService().create(ProductModel.class);
        product.setCode(productId);
        product.setCatalogVersion(catalogVersion);
        product.setVariantType(VariantTypeModel.class.cast(getTypeService().getComposedTypeForCode("ERPVariantProduct")));
        getModelService().save(product);
        return product;
    }

    @Override
    public void updateLifeCycleStatus(final ProductModel product, final ProductLifeCycleStatus status) {
        validateParameterNotNullStandardMessage("product", product);
        validateParameterNotNullStandardMessage("status", status);
        product.setLifeCycleStatus(status);
        getModelService().save(product);
    }

    @Override
    public void setVariantTypeAsERPVariantProduct(final ProductModel product) {
        validateParameterNotNullStandardMessage("product", product);
        product.setVariantType(VariantTypeModel.class.cast(getTypeService().getComposedTypeForCode("ERPVariantProduct")));
        getModelService().save(product);
    }

    @Override
    public void updateBaseProductForVariant(final VariantProductModel variant, final ProductModel productModel) {
        validateParameterNotNullStandardMessage("productModel", productModel);
        validateParameterNotNullStandardMessage("variant", variant);
        variant.setBaseProduct(productModel);
        getModelService().save(variant);
    }

    protected Predicate<Feature> isTypeOf(@NotNull final List<String> mappedClassifications) {
        return feature -> {
            if (feature.getValue() != null) {
                final Object featureValue = feature.getValue().getValue();
                if (featureValue instanceof ClassificationAttributeValueModel) {
                    final ClassificationAttributeValueModel featureValue1 = (ClassificationAttributeValueModel) featureValue;
                    return mappedClassifications.contains(featureValue1.getCode());
                }
            }
            return false;
        };
    }

    public ClassificationService getClassificationService() {
        return classificationService;
    }

    @Required
    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public List<String> getFlatClassifications() {
        return flatClassifications;
    }

    @Required
    public void setFlatClassifications(final List<String> flatClassifications) {
        this.flatClassifications = flatClassifications;
    }

    public List<String> getLongClassifications() {
        return longClassifications;
    }

    @Required
    public void setLongClassifications(final List<String> longClassifications) {
        this.longClassifications = longClassifications;
    }

    public String getShapeMaterialFeature() {
        return shapeMaterialFeature;
    }

    @Required
    public void setShapeMaterialFeature(final String shapeMaterialFeature) {
        this.shapeMaterialFeature = shapeMaterialFeature;
    }

    public TypeService getTypeService() {
        return typeService;
    }

    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }
}



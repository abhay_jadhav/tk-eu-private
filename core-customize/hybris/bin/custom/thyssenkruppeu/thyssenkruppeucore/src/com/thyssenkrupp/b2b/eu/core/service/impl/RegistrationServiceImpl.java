package com.thyssenkrupp.b2b.eu.core.service.impl;

import java.util.Date;

import com.thyssenkrupp.b2b.eu.core.service.RegistrationService;

import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.servicelayer.config.ConfigurationService;

public class RegistrationServiceImpl implements RegistrationService {

    private static final String REGISTRATION_EMAIL_TOKEN_VALIDITY = "b2b.customer.registration.email.token.validity.seconds";
    private ConfigurationService configurationService;

    public static String getRegistrationEmailTokenValidity() {
        return REGISTRATION_EMAIL_TOKEN_VALIDITY;
    }

    @Override
    public boolean validateRegistrationToken(final SecureToken data) {
        final long registrationEmailTokenValidityInSeconds = getConfigurationService().getConfiguration().getInt(REGISTRATION_EMAIL_TOKEN_VALIDITY);
        if (registrationEmailTokenValidityInSeconds > 0L) {
            final long deltaInMillis = new Date().getTime() - data.getTimeStamp();
            if (deltaInMillis / 1000 > registrationEmailTokenValidityInSeconds) {
                return false;
            }
        }
        return true;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}


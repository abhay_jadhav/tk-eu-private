package com.thyssenkrupp.b2b.eu.core.cronjob.dao;

import java.util.Date;

import de.hybris.platform.servicelayer.cronjob.CronJobHistoryDao;

public interface TkEuCronJobHistoryDao extends CronJobHistoryDao {
    Date getLastVariantNameAssignmentJobRunDate();
}

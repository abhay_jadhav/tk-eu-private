package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Set;

public interface TkEuB2bCustomerAccountService extends TkB2bCustomerAccountService {

    void sendRegistrationEmail(CustomerModel customerModel);

    void sendDuplicateUidFoundRegistrationEmail(List<CustomerModel> duplicateUidEmails);

    SearchPageData<OrderModel> orderAcknowledgementSearchList(String searchKey, CustomerModel customerModel, BaseStoreModel store,
        OrderStatus[] status,
        PageableData pageableData);

    SearchPageData<OrderModel> orderConfirmationSearchList(String searchKey, CustomerModel customerModel, BaseStoreModel store,
        OrderStatus[] status,
        PageableData pageableData);

    OrderModel getOrderConfirmationDetailsForCode(String code, BaseStoreModel store, Set<B2BUnitModel> b2BUnitModel);

    OrderModel getConfirmedOrderByCode(String code, BaseStoreModel store, Set<B2BUnitModel> b2BUnitModel);

    OrderModel getCancelledOrderByCode(String code, BaseStoreModel store, Set<B2BUnitModel> b2BUnitModel);

    Integer findOrderConfirmationListCount(String searchKey, CustomerModel customerModel, BaseStoreModel store,
        OrderStatus[] status);

    Integer findOrderAcknowledgementListCount(String searchKey, CustomerModel customerModel, BaseStoreModel store,
        OrderStatus[] status);
}

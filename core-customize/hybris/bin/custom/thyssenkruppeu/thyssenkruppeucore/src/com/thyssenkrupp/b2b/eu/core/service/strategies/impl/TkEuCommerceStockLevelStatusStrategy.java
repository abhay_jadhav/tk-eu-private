package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.strategies.CommerceAvailabilityCalculationStrategy;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.strategy.StockLevelStatusStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;

public class TkEuCommerceStockLevelStatusStrategy implements StockLevelStatusStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(TkEuCommerceStockLevelStatusStrategy.class);

    private CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy;

    @Override
    public StockLevelStatus checkStatus(final StockLevelModel stockLevel) {
        StockLevelStatus resultStatus;
        if (isNotAvailble(stockLevel)) {
            resultStatus = StockLevelStatus.NOTAVAILABLE;
        } else {
            resultStatus = findAndSetAvailableStockStatus(stockLevel);
        }

        return resultStatus;
    }

    protected boolean isNotAvailble(StockLevelModel stockLevel) {
        return stockLevel == null || stockLevel.getInStockStatus() == null || InStockStatus.FORCEOUTOFSTOCK.equals(stockLevel.getInStockStatus()) || InStockStatus.NOTSPECIFIED.equals(stockLevel.getInStockStatus()) || InStockStatus.NOTAVAILABLE.equals(stockLevel.getInStockStatus());
    }

    protected StockLevelStatus findAndSetAvailableStockStatus(StockLevelModel stockLevel) {
        StockLevelStatus resultStatus;
        if (InStockStatus.EXPRESS.equals(stockLevel.getInStockStatus())) {
            resultStatus = StockLevelStatus.EXPRESS;
        } else if (InStockStatus.FORCEINSTOCK.equals(stockLevel.getInStockStatus()) || InStockStatus.AVAILABLE1.equals(stockLevel.getInStockStatus())) {
            resultStatus = StockLevelStatus.AVAILABLE1;
        } else if (InStockStatus.AVAILABLE2.equals(stockLevel.getInStockStatus())) {
            resultStatus = StockLevelStatus.AVAILABLE2;
        } else if (InStockStatus.AVAILABLE3.equals(stockLevel.getInStockStatus())) {
            resultStatus = StockLevelStatus.AVAILABLE3;
        } else if (InStockStatus.ONDEMAND.equals(stockLevel.getInStockStatus())) {
            resultStatus = StockLevelStatus.ONDEMAND;
        } else {
            resultStatus = calculateAndFindStockStatus(stockLevel);
        }
        return resultStatus;
    }

    protected StockLevelStatus calculateAndFindStockStatus(StockLevelModel stockLevel) {
        StockLevelStatus resultStatus;
        final int result = getCommerceStockLevelCalculationStrategy().calculateAvailability(
          Collections.singletonList(stockLevel)).intValue();
        if (result <= 0) {
            resultStatus = StockLevelStatus.NOTAVAILABLE;
        } else {
            resultStatus = StockLevelStatus.AVAILABLE1;
        }
        return resultStatus;
    }

    @Override
    public StockLevelStatus checkStatus(final Collection<StockLevelModel> stockLevels) {
        StockLevelStatus resultStatus = StockLevelStatus.NOTAVAILABLE;
        for (final StockLevelModel level : stockLevels) {
            StockLevelStatus tmpStatus = checkStatus(level);
            if (!StockLevelStatus.NOTAVAILABLE.equals(tmpStatus)) {
                resultStatus = tmpStatus;
                break;
            }
        }
        LOGGER.debug("Stock Level Status = {}" + resultStatus);
        return resultStatus;
    }

    protected CommerceAvailabilityCalculationStrategy getCommerceStockLevelCalculationStrategy() {
        return commerceStockLevelCalculationStrategy;
    }

    @Required
    public void setCommerceStockLevelCalculationStrategy(final CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy) {
        this.commerceStockLevelCalculationStrategy = commerceStockLevelCalculationStrategy;
    }
}

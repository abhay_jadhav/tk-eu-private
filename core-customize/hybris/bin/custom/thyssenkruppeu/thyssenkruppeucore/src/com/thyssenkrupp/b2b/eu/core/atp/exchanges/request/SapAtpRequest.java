package com.thyssenkrupp.b2b.eu.core.atp.exchanges.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SapAtpRequest implements Serializable {

    @JsonProperty("ATP_Request")
    private AtpRequest atpRequest;

    @JsonProperty("ATP_Request")
    public AtpRequest getAtpRequest() {
        return atpRequest;
    }

    @JsonProperty("ATP_Request")
    public void setAtpRequest(AtpRequest pAtpRequest) {
        this.atpRequest = pAtpRequest;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("ATP_Request", atpRequest).toString();
    }
}

package com.thyssenkrupp.b2b.eu.core.event;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bAmbiguousRegistrationProcessModel;

public class TkEuB2bRegistrationAmbiguousEmailEventListener extends AbstractEventListener<TkEuB2bRegistrationAmbiguousEmailEvent> {

    private static final String REGISTRATION_AMBIGUOUS_EMAIL_BUSINESS_PROCESS = "tkEuB2bCustomerAmbiguousRegistrationEmailProcess";
    private static final Logger LOG = Logger.getLogger(TkEuB2bRegistrationAmbiguousEmailEventListener.class);
    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Override
    protected void onEvent(final TkEuB2bRegistrationAmbiguousEmailEvent event) {
        final List<CustomerModel> customerModelList = event.getAmbiguousCustomerModelList();
        LOG.debug("Received Ambiguous customer size:" + customerModelList.size());
        final String processId = "tkEuB2bCustomerAmbiguousRegistrationEmail-" + System.currentTimeMillis();
        final TkEuB2bAmbiguousRegistrationProcessModel processModel = getBusinessProcessService().createProcess(processId, REGISTRATION_AMBIGUOUS_EMAIL_BUSINESS_PROCESS);
        processModel.setAmbiguousCustomers(event.getAmbiguousCustomerModelList());
        getModelService().save(processModel);
        getBusinessProcessService().startProcess(processModel);

    }

}

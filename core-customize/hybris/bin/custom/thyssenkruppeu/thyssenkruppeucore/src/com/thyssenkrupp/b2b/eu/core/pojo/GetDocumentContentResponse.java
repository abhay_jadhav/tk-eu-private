package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentContentResponse implements Serializable {

    @JsonProperty("Get_Document_Content")
    private GetDocumentSAPContent documentContent;

    @JsonProperty("Get_Document_Content")
    public GetDocumentSAPContent getDocumentContent() {
        return documentContent;
    }

    @JsonProperty("Get_Document_Content")
    public void setDocumentContent(GetDocumentSAPContent documentContent) {
        this.documentContent = documentContent;
    }

    @Override
    public String toString() {
        return "GetDocumentContentResponse.Get_DocumentContent = " + documentContent;
    }
}

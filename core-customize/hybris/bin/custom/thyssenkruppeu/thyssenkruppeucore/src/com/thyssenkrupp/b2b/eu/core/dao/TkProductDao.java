package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;

import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;

public interface TkProductDao extends ProductDao {

    List<ProductModel> findProductsByStatus(CatalogVersionModel catalogVersion);

    List<ProductModel> findProductsByCatalog(CatalogVersionModel catalogVersion);

    List<ProductModel> findProductsByCatalogAndLifeCycleStatus(CatalogVersionModel catalogVersion, ProductLifeCycleStatus productLifeCycleStatus);
}

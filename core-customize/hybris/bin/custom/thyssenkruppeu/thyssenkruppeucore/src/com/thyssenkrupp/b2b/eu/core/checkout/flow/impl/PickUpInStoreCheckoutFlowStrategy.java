package com.thyssenkrupp.b2b.eu.core.checkout.flow.impl;

import de.hybris.platform.acceleratorservices.enums.CheckoutFlowEnum;
import de.hybris.platform.b2bacceleratorservices.order.checkout.flow.B2BCheckoutFlowStrategy;
import de.hybris.platform.b2bacceleratorservices.order.checkout.flow.impl.FixedB2BCheckoutFlowStrategy;
import de.hybris.platform.commerceservices.enums.PickupInStoreMode;
import de.hybris.platform.commerceservices.strategies.PickupStrategy;

import org.springframework.beans.factory.annotation.Required;

/**
 * Checks if any of cart entries is created to be picked up in location. If so, the multi step
 * checkout is always chosen.
 *
 * @deprecated Since 5.5.
 */
@Deprecated
public class PickUpInStoreCheckoutFlowStrategy extends FixedB2BCheckoutFlowStrategy {
    private B2BCheckoutFlowStrategy multiStepCheckoutFlowStrategy;
    private PickupStrategy pickupStrategy;

    protected boolean canSupport() {
        final PickupInStoreMode pickupInStoreMode = getPickupStrategy().getPickupInStoreMode();
        return PickupInStoreMode.BUY_AND_COLLECT.equals(pickupInStoreMode);
    }

    @Override
    public CheckoutFlowEnum getCheckoutFlow() {
        if (canSupport()) {
            return getMultiStepCheckoutFlowStrategy().getCheckoutFlow();
        } else {
            return super.getCheckoutFlow();
        }
    }

    protected B2BCheckoutFlowStrategy getMultiStepCheckoutFlowStrategy() {
        return multiStepCheckoutFlowStrategy;
    }

    @Required
    public void setMultiStepCheckoutFlowStrategy(final B2BCheckoutFlowStrategy multiStepCheckoutFlowStrategy) {
        this.multiStepCheckoutFlowStrategy = multiStepCheckoutFlowStrategy;
    }

    protected PickupStrategy getPickupStrategy() {
        return pickupStrategy;
    }

    @Required
    public void setPickupStrategy(final PickupStrategy pickupStrategy) {
        this.pickupStrategy = pickupStrategy;
    }
}

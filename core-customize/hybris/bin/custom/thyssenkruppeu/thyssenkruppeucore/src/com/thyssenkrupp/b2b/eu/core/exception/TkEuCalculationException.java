package com.thyssenkrupp.b2b.eu.core.exception;

import de.hybris.platform.order.exceptions.CalculationException;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TkEuCalculationException extends CalculationException {

    private List<String> invalidItem ;

    public TkEuCalculationException(String message, @NotNull List<String> invalidItem) {
        super(message);
        this.invalidItem = invalidItem;
    }

    public TkEuCalculationException(String message, Throwable cause, @NotNull List<String> invalidItem) {
        super(message, cause);
        this.invalidItem = invalidItem;
    }

    public TkEuCalculationException(Throwable cause, @NotNull List<String> invalidItem) {
        super(cause);
        this.invalidItem = invalidItem;
    }

    public List<String> getInvalidItem() {
        return invalidItem;
    }
}

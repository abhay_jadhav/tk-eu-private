package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.i18n.impl.DefaultLocalizationService;

import java.util.Locale;

public class DefaultTkLocalizationService extends DefaultLocalizationService {

    private CommonI18NService commonI18NService;

    @Override
    protected DataLocale matchDataLocale(final Locale locale, final boolean throwError) {
        Locale localeForIsoCode = getCommonI18NService().getLocaleForIsoCode(locale.toString());
        if (localeForIsoCode == null) {
            localeForIsoCode = locale;
        }
        return super.matchDataLocale(localeForIsoCode, throwError);
    }

    @Override
    public void setCurrentLocale(final Locale locale) {
        validateParameterNotNull(locale, "locale was null");
        final Locale currentLocale = this.getCurrentLocale();
        final SessionContext context = JaloSession.getCurrentSession().getSessionContext();
        if (currentLocale != locale && (currentLocale == null || !currentLocale.equals(locale)) || currentLocale != null && context.getLanguage() == null) {
            final DefaultLocalizationService.DataLocale dataLocale = this.matchDataLocale(locale, true);
            context.setLanguage(dataLocale.getLang());
            context.setLocale(dataLocale.getLoc());
        }
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }
}

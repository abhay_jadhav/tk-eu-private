package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;
import com.thyssenkrupp.b2b.eu.core.dao.TkProductDao;

public class DefaultTkProductDao extends DefaultProductDao implements TkProductDao {

    public DefaultTkProductDao(final String typecode) {
        super(typecode);
    }

    @Override
    public List<ProductModel> findProductsByStatus(final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(catalogVersion, "CatalogVersion must not be null!");
        final Map parameters = new HashMap();
        parameters.put(ProductModel.CATEGORYCREATIONSTATUS, Boolean.FALSE);
        parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

        return find(parameters);
    }

    @Override
    public List<ProductModel> findProductsByCatalog(final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(catalogVersion, "CatalogVersion must not be null!");
        final Map parameters = new HashMap();
        parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

        return find(parameters);
    }

    @Override
    public List<ProductModel> findProductsByCatalogAndLifeCycleStatus(final CatalogVersionModel catalogVersion, final ProductLifeCycleStatus productLifeCycleStatus) {
        validateParameterNotNull(catalogVersion, "CatalogVersion must not be null!");
        validateParameterNotNull(productLifeCycleStatus, "productLifeCycleStatus must not be null!");
        final Map parameters = new HashMap();
        parameters.put(ProductModel.LIFECYCLESTATUS, productLifeCycleStatus);
        parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

        return find(parameters);
    }

}

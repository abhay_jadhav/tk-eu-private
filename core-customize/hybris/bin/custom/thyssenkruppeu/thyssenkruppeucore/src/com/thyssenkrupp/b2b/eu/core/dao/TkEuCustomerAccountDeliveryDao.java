package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Set;

public interface TkEuCustomerAccountDeliveryDao {

    SearchPageData<ConsignmentModel> orderDeliveriesSearchList(String searchKey, PageableData pageableData, Set<B2BUnitModel> b2bUnits, BaseStoreModel baseStoreModel);

    int orderDeliveriesSearchListCount(String searchKey, Set<B2BUnitModel> b2bUnits, BaseStoreModel baseStoreModel);

    List<ConsignmentModel> fetchDeliveries(String deliveryNo, Set<B2BUnitModel> b2BUnits, BaseStoreModel baseStoreModel);

    List<ConsignmentEntryModel> fetchDeliveriesForOrderEntry(String sapOrderId, int entryNumber, Set<B2BUnitModel> b2bUnits, BaseStoreModel currentBaseStore);
}

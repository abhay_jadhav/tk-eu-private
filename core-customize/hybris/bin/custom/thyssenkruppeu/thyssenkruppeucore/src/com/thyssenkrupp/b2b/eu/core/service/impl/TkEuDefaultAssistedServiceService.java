package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_NAME_ASC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_NAME_DESC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_UID_ASC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_UID_DESC;

import de.hybris.platform.assistedserviceservices.impl.DefaultAssistedServiceService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserConstants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 *
 */
public class TkEuDefaultAssistedServiceService extends DefaultAssistedServiceService {

    private static final String USERNAME = "username";
    private static final String CURRENTDATE = "currentDate";
    private static final String LOGINDISABLED_PARAMETER = "loginDisabled";

    @Override
    public SearchPageData<CustomerModel> getCustomers(final String searchCriteria, final PageableData pageableData) {
        final StringBuilder builder = getCustomerSearchQuery(searchCriteria);

        final Map<String, Object> params = new HashMap<>();
        params.put(CURRENTDATE, getTimeService().getCurrentTime());
        params.put(LOGINDISABLED_PARAMETER, Boolean.FALSE);

        if (StringUtils.isNotBlank(searchCriteria)) {
            params.put(USERNAME, searchCriteria.toLowerCase());
        }

        final List<SortQueryData> sortQueries = Arrays.asList(createSortQueryData(SORT_BY_UID_ASC, builder.toString() + " ORDER BY {c." + B2BCustomerModel.UID + "} ASC"), createSortQueryData(SORT_BY_UID_DESC, builder.toString() + " ORDER BY {c." + B2BCustomerModel.UID + "} DESC"), createSortQueryData(SORT_BY_NAME_ASC, builder.toString() + " ORDER BY {c." + B2BCustomerModel.NAME + "} ASC"), createSortQueryData(SORT_BY_NAME_DESC, builder.toString() + " ORDER BY {c." + B2BCustomerModel.NAME + "} DESC"));

        return getPagedFlexibleSearchService().search(sortQueries, SORT_BY_UID_ASC, params, pageableData);
    }

    @Override
    protected StringBuilder getCustomerSearchQuery(final String searchCriteria) {
        final StringBuilder builder = new StringBuilder();

        builder.append("SELECT ");
        builder.append("{c:" + B2BCustomerModel.PK + "} ");
        builder.append("FROM {" + B2BUnitModel._TYPECODE + " AS u JOIN " + B2BCustomerModel._TYPECODE + " AS c on {u:" + B2BUnitModel.PK + "}={c:" + B2BCustomerModel.DEFAULTB2BUNIT + "}} ");
        builder.append("WHERE NOT {" + B2BCustomerModel.UID + "}='" + UserConstants.ANONYMOUS_CUSTOMER_UID + "' ");
        builder.append("AND {c:" + B2BCustomerModel.LOGINDISABLED + "} = ?" + LOGINDISABLED_PARAMETER + " ");
        builder.append("AND ({c:" + B2BCustomerModel.DEACTIVATIONDATE + "} IS NULL OR {c:" + B2BCustomerModel.DEACTIVATIONDATE + "} > ?" + CURRENTDATE + ") ");

        if (!StringUtils.isBlank(searchCriteria)) {
            builder.append("AND (LOWER({c:" + B2BCustomerModel.UID + "}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
            builder.append("OR LOWER({c:" + B2BCustomerModel.CUSTOMERID + "}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
            builder.append("OR LOWER({u:" + B2BUnitModel.UID + "}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
            builder.append("OR LOWER({c:" + B2BCustomerModel.NAME + "}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
            builder.append("OR LOWER({c:" + B2BCustomerModel.EMAIL + "}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
            builder.append("OR LOWER({u:" + B2BUnitModel.NAME + "}) LIKE CONCAT('%', CONCAT(?username, '%'))) ");
        }
        return builder;
    }
}

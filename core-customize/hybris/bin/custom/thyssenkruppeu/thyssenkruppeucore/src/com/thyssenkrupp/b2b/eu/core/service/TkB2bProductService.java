package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2bacceleratorservices.product.B2BProductService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.List;

public interface TkB2bProductService extends B2BProductService {

    String getFeatureValue(FeatureValue value);

    boolean isClassificationCategoryEmpty(ProductModel product);

    CategoryModel getClassificationCategoryForProduct(ProductModel product);

    void updateVariantProduct(VariantProductModel variant, List<VariantValueCategoryModel> variantSuperCategories);

    boolean doesCategoryAlreadyExist(String code, List<CategoryModel> categories);

    void updateVariantProductConfigurationCategory(VariantProductModel variant, ConfigurationCategoryModel variantSuperCategories);

    boolean isBaseProduct(ProductModel product);

    void setCategoryCreationStatusToTrue(ProductModel product);

    void replaceExistingVariantCategoriesForProduct(ProductModel product, List<VariantCategoryModel> variantCategories);

    List<VariantCategoryModel> getAllVariantCategoriesForProduct(ProductModel product);
}

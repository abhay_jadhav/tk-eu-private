package com.thyssenkrupp.b2b.eu.core.dao;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.MediaDao;

public interface TkEuMediaDao extends MediaDao {

    MediaModel findMediaIconByMimeType(String mimeType);

}

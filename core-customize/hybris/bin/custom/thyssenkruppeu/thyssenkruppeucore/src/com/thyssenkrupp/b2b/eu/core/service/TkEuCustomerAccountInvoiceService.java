package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.List;
import java.util.Set;

import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;


public interface TkEuCustomerAccountInvoiceService {
    SearchPageData<InvoiceModel> orderInvoiceSearchList(String searchKey, PageableData pageableData, Set<B2BUnitModel> b2BUnitModel);

    List<InvoiceModel> fetchInvoices(String invoiceNo, Set<B2BUnitModel> b2BUnitModel);

    int orderInvoicesSearchListCount(String searchKey, Set<B2BUnitModel> b2bUnits);
}

package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentStatusSAPSingleResponse implements Serializable{

    @JsonProperty("Get_Document_Status")
    private GetDocumentStatusSAP getDocumentStatusSAP;

    @JsonProperty("Get_Document_Status")
    public GetDocumentStatusSAP getGetDocumentStatusSAP() {
        return getDocumentStatusSAP;
    }

    @JsonProperty("Get_Document_Status")
    public void setGetDocumentStatusSAP(GetDocumentStatusSAP getDocumentStatusSAP) {
        this.getDocumentStatusSAP = getDocumentStatusSAP;
    }
}

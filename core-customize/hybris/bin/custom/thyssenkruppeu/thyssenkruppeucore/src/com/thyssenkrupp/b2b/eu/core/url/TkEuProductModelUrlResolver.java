package com.thyssenkrupp.b2b.eu.core.url;

import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

public class TkEuProductModelUrlResolver extends DefaultProductModelUrlResolver {

    private TkEuStorefrontCategoryProviderService tkEuStorefrontCategoryProviderService;


    @Override
    protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product) {
        for (final CategoryModel category : product.getSupercategories()) {
            if (tkEuStorefrontCategoryProviderService.isStorefrontCategory(category)) {
                return category;
            }
        }
        return null;
    }

    public TkEuStorefrontCategoryProviderService getTkEuStorefrontCategoryProviderService() {
        return tkEuStorefrontCategoryProviderService;
    }

    public void setTkEuStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkEuStorefrontCategoryProviderService) {
        this.tkEuStorefrontCategoryProviderService = tkEuStorefrontCategoryProviderService;
    }

}

package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2bacceleratorservices.customer.B2BCustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;

public interface TkB2bCustomerAccountService extends B2BCustomerAccountService {

    boolean validatePasswordToken(SecureToken data);
}

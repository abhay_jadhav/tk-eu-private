package com.thyssenkrupp.b2b.eu.core.atp.service.impl;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import de.hybris.platform.basecommerce.enums.InStockStatus;

import java.util.Date;

public final class AtpResponseDataBuilder {

    private AtpResponseDataBuilder() {
        throw new UnsupportedOperationException();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private int           position;
        private Date          sapResult;
        private InStockStatus hybrisResult;
        private String        productCode;

        public Builder() {
            //empty constructor
        }

        public Builder withPosition(final int pPosition) {
            this.position = pPosition;
            return this;
        }

        public Builder withSapResult(final Date pSapResult) {
            this.sapResult = pSapResult;
            return this;
        }

        public Builder withHybrisResult(final InStockStatus pHybrisResult) {
            this.hybrisResult = pHybrisResult;
            return this;
        }

        public Builder withProductCode(final String pProductCode) {
            this.productCode = pProductCode;
            return this;
        }

        public AtpResponseData build() {
            final AtpResponseData responseData = new AtpResponseData();
            responseData.setPosition(this.position);
            responseData.setHybrisAtpResult(this.hybrisResult);
            responseData.setSapAtpResult(this.sapResult);
            responseData.setProductCode(this.productCode);
            return responseData;
        }
    }
}

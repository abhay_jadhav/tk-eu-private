package com.thyssenkrupp.b2b.eu.core.cronjob;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDao;
import com.thyssenkrupp.b2b.eu.core.model.TkEuCustomerAccountUnlockJobModel;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.apache.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

public class TkEuCustomerAccountUnlockJob extends AbstractJobPerformable<TkEuCustomerAccountUnlockJobModel> {

    public static final String ACCOUNT_UNLOCK_NOTIFICATION_EMAIL_FROM_ADDRESS = "tk.eu.customer.account.unlock.notification.from.address";
    public static final String ACCOUNT_UNLOCK_NOTIFICATION_EMAIL_SUBJECT      = "tk.eu.customer.account.unlock.notification.subject";
    public static final String COMMA                                          = ",";

    private static final Logger LOG = Logger.getLogger(TkEuCustomerAccountUnlockJob.class);

    private ModelService           modelService;
    private TkEuCustomerAccountDao tkEuCustomerAccountDao;
    private EmailService           emailService;
    private I18NService            i18nService;
    private ConfigurationService   configurationService;
    private TimeService            timeService;

    @Override
    public PerformResult perform(final TkEuCustomerAccountUnlockJobModel model) {
        try {
            LOG.info("TkEuCustomerAccountUnlockJob cron job Starts");
            final List<CustomerModel> customerList = getTkEuCustomerAccountDao().getLockedCustomersList(model.getMinimumLockedDuration().intValue());
            LOG.info("Locked user count :- " + customerList.size());
            if (customerList.size() > 0) {
                AtomicInteger index = new AtomicInteger();
                List<String> customerUid= customerList.stream().map(e->index.incrementAndGet()+") "+e.getOriginalUid()).collect(Collectors.toList());
                sendAccountUnlockNotificationEmail(model, customerList, String.join("<br>",customerUid));
                updateCustomerLoginDisabledStatus(customerList);
                getModelService().saveAll(customerList);
            }
            LOG.info("TkEuCustomerAccountUnlockJob cron job Finished");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while TkEuCustomerAccountUnlock job ", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void updateCustomerLoginDisabledStatus(List<CustomerModel> customerList) {
        customerList.forEach(c->{
            c.setLoginDisabled(false);
            c.setLoginDisabledTime(null);
        });
    }

    private void sendAccountUnlockNotificationEmail(TkEuCustomerAccountUnlockJobModel model, @NotNull List<CustomerModel> customerList, String customerUid) {

        final ResourceBundle javaBundle = ResourceBundle.getBundle("localization.thyssenkruppeucore-locales", getI18nService().getCurrentLocale(), getClass().getClassLoader());
        final String emailBody = MessageFormat.format(javaBundle.getString("customer.account.unlock.cronjob.email.template.body"), customerList.size(), getTimeService().getCurrentTime(), customerUid);
        EmailMessageModel emailMessageModel = createEmailMessage(model.getEmailAddress(), emailBody, null);

        if (emailMessageModel != null) {
            boolean status = getEmailService().send(emailMessageModel);
            LOG.info("Customer account unlock notification email status:" + status);
        } else {
            LOG.warn("Failed to generate email message");
        }
    }

    protected EmailMessageModel createEmailMessage(String toMailAddress, final String emailBody, final AbstractEmailContext<BusinessProcessModel> emailContext) {

        final String fromAddress = getConfigurationService().getConfiguration().getString(ACCOUNT_UNLOCK_NOTIFICATION_EMAIL_FROM_ADDRESS);
        final String emailSubject = getConfigurationService().getConfiguration().getString(ACCOUNT_UNLOCK_NOTIFICATION_EMAIL_SUBJECT);
        final List<EmailAddressModel> toEmails = new ArrayList<EmailAddressModel>();
        final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(toMailAddress, toMailAddress);
        toEmails.add(toAddress);
        final EmailAddressModel emailFromAddress = getEmailService().getOrCreateEmailAddressForEmail(fromAddress, fromAddress);
        return getEmailService().createEmailMessage(toEmails, new ArrayList<EmailAddressModel>(), new ArrayList<EmailAddressModel>(),
          emailFromAddress, null, emailSubject, emailBody, null);
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public TkEuCustomerAccountDao getTkEuCustomerAccountDao() {
        return tkEuCustomerAccountDao;
    }

    public void setTkEuCustomerAccountDao(final TkEuCustomerAccountDao tkEuCustomerAccountDao) {
        this.tkEuCustomerAccountDao = tkEuCustomerAccountDao;
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public TimeService getTimeService() {
        return timeService;
    }

    public void setTimeService(TimeService timeService) {
        this.timeService = timeService;
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.impl.LocalMediaWebURLStrategy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import reactor.util.StringUtils;


public class TkApacheMediaUrlStrategy extends LocalMediaWebURLStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(TkApacheMediaUrlStrategy.class);

    private String mediaHostName;
    private List<String> apacheServedFolders;

    @Override
    public String getUrlForMedia(final MediaStorageConfigService.MediaFolderConfig mediaFolderConfig, final MediaSource mediaSource) {
        if (StringUtils.isEmpty(mediaHostName) || !apacheServedFolders.contains(mediaSource.getFolderQualifier())) {
            return super.getUrlForMedia(mediaFolderConfig, mediaSource);
        }

        final String location = mediaSource.getLocation();
        final String url = new StringBuilder(mediaHostName.length() + location.length() + 3).append("//").append(mediaHostName)
                .append('/').append(location).toString();
        LOG.debug("Create static URL for {}, new Media URL: {}", location, url);
        return url;
    }

    @Required
    public void setMediaHostName(final String mediaHostName) {
        this.mediaHostName = mediaHostName;
    }

    public String getMediaHostName() {
        return this.mediaHostName;
    }

    @Required
    public void setApacheServedFolders(final List<String> apacheServedFolders) {
        this.apacheServedFolders = apacheServedFolders;
    }
}

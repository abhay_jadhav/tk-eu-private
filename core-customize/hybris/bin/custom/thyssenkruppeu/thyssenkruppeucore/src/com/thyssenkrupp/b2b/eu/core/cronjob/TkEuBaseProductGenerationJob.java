package com.thyssenkrupp.b2b.eu.core.cronjob;

import com.thyssenkrupp.b2b.eu.core.cronjob.service.TkEuBaseProductGenerationJobHelper;
import com.thyssenkrupp.b2b.eu.core.model.TkEuBaseProductGenerationCronJobModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Optional;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class TkEuBaseProductGenerationJob extends AbstractJobPerformable<TkEuBaseProductGenerationCronJobModel> {

    private static final Logger                             LOG = Logger.getLogger(TkEuBaseProductGenerationJob.class);
    private              TkEuBaseProductGenerationJobHelper baseProductGenerationJobHelper;

    @Override
    public PerformResult perform(TkEuBaseProductGenerationCronJobModel cronJob) {
        validateParameterNotNullStandardMessage("TkEuBaseProductGenerationCronJob:BaseStore", cronJob.getBaseStore());
        try {
            final BaseStoreModel baseStore = cronJob.getBaseStore();
            final Optional<CatalogVersionModel> maybeProductCatalogVersion = getBaseProductGenerationJobHelper().getProductCatalogVersionFromBaseStore(baseStore);
            if (maybeProductCatalogVersion.isPresent()) {
                final CatalogVersionModel catalogVersion = maybeProductCatalogVersion.get();
                final Collection<VariantProductModel> variants = getBaseProductGenerationJobHelper().getAllVariantsForSapDummyProduct(catalogVersion);
                if (isNotEmpty(variants)) {
                    LOG.info("Found " + variants.size() + " variants associated with SapDummyMasterProduct");
                    getBaseProductGenerationJobHelper().generateMasterProductsForVariants(variants);
                } else {
                    LOG.warn("No Variants found for SapDummyMasterProduct, Skipping Job");
                }
            } else {
                LOG.warn(format("No valid ProductCatalogVersion found for BaseStore:%s. Skipping cronjob", baseStore.getUid()));
            }
            LOG.info("Finished executing cronjob");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (Exception e) {
            LOG.error("Exception occurred while executing job", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    public TkEuBaseProductGenerationJobHelper getBaseProductGenerationJobHelper() {
        return baseProductGenerationJobHelper;
    }

    @Required
    public void setBaseProductGenerationJobHelper(TkEuBaseProductGenerationJobHelper baseProductGenerationJobHelper) {
        this.baseProductGenerationJobHelper = baseProductGenerationJobHelper;
    }
}

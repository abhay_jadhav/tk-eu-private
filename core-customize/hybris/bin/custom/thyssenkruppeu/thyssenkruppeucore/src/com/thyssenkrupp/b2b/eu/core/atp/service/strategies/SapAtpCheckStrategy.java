package com.thyssenkrupp.b2b.eu.core.atp.service.strategies;


import de.hybris.platform.order.CartService;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.config.ConfigurationManager;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.SapAtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.response.SapAtpResponse;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.cache.impl.DefaultTkEuSapAtpCartCacheService;

public class SapAtpCheckStrategy {

    private static final Logger LOG = Logger.getLogger(SapAtpCheckStrategy.class);

    private static final String HYSTRIX_ENABLED                       = "atp.hystirx.enabled";
    private static final String HYSTRIX_TIMEOUTINMILLISECONDS         = "atp.hystrix.timeoutInMilliseconds";
    private static final String HYSTRIX_REQUEST_VOLUME_THRESHOLD      = "atp.hystrix.requestVolumeThreshold";
    private static final String HYSTRIX_SLEEP_WINDOW                  = "atp.hystrix.sleepWindow";
    private static final String HYSTRIX_THREADPOOL_CORESIZE           = "atp.hystrix.threadpool.coresize";
    private static final String HYSTRIX_THREADPOOL_MAXQUEUESIZE       = "atp.hystrix.threadpool.maxQueueSize";
    private static final String HYSTRIX_THREADPOOL_KEEPALIVETIMEINMIN = "atp.hystrix.threadpool.keepAliveTimeInMin";
    private static final String HYSTRIX_THREADPOOL_THRESHOLD          = "atp.hystrix.threadpool.poolThreshold";

    private RestTemplate                      restTemplate;
    private ConfigurationService              configurationService;
    private CartService                       cartService;
    private DefaultTkEuSapAtpCartCacheService sapAtpCacheService;

    @PostConstruct
    public void init() throws Exception {
        setHystrixCommandProperties();
    }

    private void setHystrixCommandProperties() {
        final AbstractConfiguration abstractConfig = ConfigurationManager.getConfigInstance();
        abstractConfig.setProperty("hystrix.command.callAtpCheck.circuitBreaker.enabled", configurationService.getConfiguration().getBoolean(HYSTRIX_ENABLED));
        abstractConfig.setProperty("hystrix.command.callAtpCheck.execution.isolation.thread.timeoutInMilliseconds", configurationService.getConfiguration().getInt(HYSTRIX_TIMEOUTINMILLISECONDS));
        abstractConfig.setProperty("hystrix.command.callAtpCheck.circuitBreaker.requestVolumeThreshold", configurationService.getConfiguration().getInt(HYSTRIX_REQUEST_VOLUME_THRESHOLD));
        abstractConfig.setProperty("hystrix.command.callAtpCheck.circuitBreaker.sleepWindowInMilliseconds", configurationService.getConfiguration().getInt(HYSTRIX_SLEEP_WINDOW));
        abstractConfig.setProperty("hystrix.threadpool.atpThreadPool.coreSize", configurationService.getConfiguration().getInt(HYSTRIX_THREADPOOL_CORESIZE));
        abstractConfig.setProperty("hystrix.threadpool.atpThreadPool.maxQueueSize", configurationService.getConfiguration().getInt(HYSTRIX_THREADPOOL_MAXQUEUESIZE));
        abstractConfig.setProperty("hystrix.threadpool.atpThreadPool.keepAliveTimeMinutes", configurationService.getConfiguration().getInt(HYSTRIX_THREADPOOL_KEEPALIVETIMEINMIN));
        abstractConfig.setProperty("hystrix.threadpool.atpThreadPool.queueSizeRejectionThreshold", configurationService.getConfiguration().getInt(HYSTRIX_THREADPOOL_THRESHOLD));
    }

    @HystrixCommand(fallbackMethod = "getAtpFallback", commandKey = "callAtpCheck", groupKey = "sapAtpCheckStrategy",
        threadPoolKey = "atpThreadPool",
        commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "4000"),
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "4"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
            @HystrixProperty(name = "metrics.rollingPercentile.numBuckets", value = "12"),
            @HystrixProperty(name = "metrics.rollingPercentile.timeInMilliseconds", value = "1440")
        },
        threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "5"),
            @HystrixProperty(name = "maxQueueSize", value = "10"),
            @HystrixProperty(name = "keepAliveTimeMinutes", value = "2"),
            @HystrixProperty(name = "queueSizeRejectionThreshold", value = "10")
        })
    public SapAtpResponse callAtpCheck(final SapAtpRequest request, final AtpRequestData atpRequestData, final CacheKey cacheKey) {
        SapAtpResponse response = null;
        final HttpEntity<SapAtpRequest> httpEntity = new HttpEntity<>(request, createHeader());
        final Optional<Object> cachedValue = getCachedValue(atpRequestData, cacheKey);
        if (cachedValue.isPresent()) {
            LOG.debug("ATP cache hit at " + new Date());
            response = (SapAtpResponse) cachedValue.get();
        } else {
            LOG.debug("ATP cache missed at " + new Date());
            try {
                logDebug(request, "Request");
                final Instant start = Instant.now();
                final ResponseEntity<SapAtpResponse> responseEntity = getRestTemplate().exchange(getSapAtpURL(), HttpMethod.POST, httpEntity, SapAtpResponse.class);
                final Instant finish = Instant.now();
                response = responseEntity.getBody();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("TAT for ATP_Response (in ms) : " + Duration.between(start, finish).toMillis());
                }
                logDebug(response, "Response");
                sapAtpCacheService.put(cacheKey, response);

            } catch (final Exception e) {
                LOG.error("Error during SAP ATP check: " + e.getMessage());
            }
        }
        return response;
    }

    private Optional<Object> getCachedValue(final AtpRequestData request, final CacheKey cacheKey) {
        return sapAtpCacheService.get(cacheKey);

    }

    private HttpHeaders createHeader() {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        return headers;
    }

    private <T> void logDebug(final T body, final String preMessage) {
        if (LOG.isDebugEnabled()) {
            try {
                LOG.debug(preMessage + ":" + (new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(body)).replaceAll("\\n", " ").replaceAll("\\r", " ") + "");
            } catch (final JsonProcessingException e) {
                LOG.warn("Unable to log " + preMessage + " json");
            }
        }
    }

    public SapAtpResponse getAtpFallback(final SapAtpRequest request, final AtpRequestData atpRequestData, final CacheKey cacheKey) {
        LOG.info("Hystrix fallback invoked...");
        return new SapAtpResponse();
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private String getSapAtpURL() {
        return (String) configurationService.getConfiguration().getProperty("sap.atp.endPoint.url");
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }

    public CartService getCartService() {
        return cartService;
    }

    public DefaultTkEuSapAtpCartCacheService getSapAtpCacheService() {
        return sapAtpCacheService;
    }

    public void setSapAtpCacheService(final DefaultTkEuSapAtpCartCacheService sapAtpCacheService) {
        this.sapAtpCacheService = sapAtpCacheService;
    }
}

package com.thyssenkrupp.b2b.eu.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuConfigKeyValueDao;
import com.thyssenkrupp.b2b.eu.core.model.ConfKeyValueModel;

/**
 *
 */
public class TkEuConfigKeyValueDaoImpl extends AbstractItemDao implements TkEuConfigKeyValueDao {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuConfigKeyValueDaoImpl.class);
    private static final String SEARCH_VALUE_BY_KEY = "SELECT {sc:" + ConfKeyValueModel.PK + "} FROM {" + ConfKeyValueModel._TYPECODE + " AS sc} WHERE {sc:" + ConfKeyValueModel.KEY + "}=?key";

    @Override
    public ConfKeyValueModel getProperty(final String key) {
        try {
            final FlexibleSearchQuery query = new FlexibleSearchQuery(SEARCH_VALUE_BY_KEY);
            query.addQueryParameter("key", key);
            return getFlexibleSearchService().searchUnique(query);
        } catch (final Exception e) {
            LOG.error("Key Not found Exception:" + e.getMessage());
        }
        return null;
    }
}

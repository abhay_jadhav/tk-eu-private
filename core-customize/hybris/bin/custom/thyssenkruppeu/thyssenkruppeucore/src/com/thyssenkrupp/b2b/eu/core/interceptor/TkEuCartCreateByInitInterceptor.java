package com.thyssenkrupp.b2b.eu.core.interceptor;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;

/**
 *
 */
public class TkEuCartCreateByInitInterceptor implements InitDefaultsInterceptor<CartModel> {
    private static final Logger LOG = Logger.getLogger(TkEuCartCreateByInitInterceptor.class);
    private UserService userService;
    private AssistedServiceFacade assistedServiceFacade;

    @Override
    public void onInitDefaults(final CartModel cart, final InterceptorContext ctx) throws InterceptorException {

        final UserModel user = getUserService().getCurrentUser();
        if (user != null && ctx.isModified(cart, CartModel.CREATIONTIME)) {
        /* setting User type who create cart, Either Customer or ASM */
            cart.setCartCreatedBy(assistedServiceFacade.isAssistedServiceAgentLoggedIn() ? LoginType.ASM : LoginType.CUSTOMER);
            LOG.debug("TkEuCartCreateByInitInterceptor called : cart Created set as : " + cart.getCartCreatedBy());
        }
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the assistedServiceFacade
     */
    public AssistedServiceFacade getAssistedServiceFacade() {
        return assistedServiceFacade;
    }

    /**
     * @param assistedServiceFacade
     *            the assistedServiceFacade to set
     */
    public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade) {
        this.assistedServiceFacade = assistedServiceFacade;
    }


}

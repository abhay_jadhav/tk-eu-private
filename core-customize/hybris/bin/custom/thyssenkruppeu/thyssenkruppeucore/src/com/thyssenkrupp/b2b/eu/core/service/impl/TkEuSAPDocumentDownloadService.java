package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thyssenkrupp.b2b.eu.core.cache.TkEuSAPDocumentExistsCacheService;
import com.thyssenkrupp.b2b.eu.core.enums.DocumentDownloadServiceEnum;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContent;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentSAPContent;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAP;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentStatusSAPSingleResponse;
import com.thyssenkrupp.b2b.eu.core.pojo.ItemDetails;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountDeliveryService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountInvoiceService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuDocumentDownloadService;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;
import com.thyssenkrupp.b2b.global.baseshop.core.model.TkSapProductInfoModel;

public class TkEuSAPDocumentDownloadService implements TkEuDocumentDownloadService {

    private static final Logger LOG                                           = LoggerFactory.getLogger(TkEuSAPDocumentDownloadService.class);
    private static final String CONTENT_ANONYMIZED                            = "sap.document.download.contentAnonymized";
    private static final String HYBRIS_REGION                                 = "sap.document.download.hybrisRegion";
    private static final String SAP_GETDOCUMENTSTATUSURL                      = "sap.document.download.documentStatusURL";
    private static final String SAP_GETDOCUMENTCONTENTURL                     = "sap.document.download.documentContentURL";
    private static final String ONE                                           = "1";
    private static final String DELIVERY                                      = "Delivery";
    private static final String INVOICE                                       = "Invoice";
    private static final String ORDER                                         = "Order";
    private static final String CERTIFICATE                                   = "Certificate";
    private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE       = "SAP order ID with guid %s not found current BaseStore";
    private static final String INVOICE_NOT_FOUND_FOR_USER_AND_BASE_STORE     = "Invoice with id %s not found for current BaseStore";
    private static final String CONSIGNMENT_NOT_FOUND_FOR_USER_AND_BASE_STORE = "Consignment with id %s not found for current BaseStore";
    private static final String REQUEST                                       = "SAP Request";
    private static final String RESPONSE                                      = "SAP Response";
    private static final String SAP_DUMMY_PRODUCT                             = "sap_dummy_product";

    private DefaultB2BCommerceUnitService defaultB2BCommerceUnitService;

    private UserService userService;

    private DefaultB2BCustomerService defaultB2BCustomerService;

    private TkEuCustomerAccountDeliveryService customerAccountDeliveryService;

    private ConfigurationService configurationService;

    private RestTemplate restTemplate;

    private BaseStoreService                  baseStoreService;
    private TkEuB2bCustomerAccountService     tkEuCustomerAccountService;
    private TkEuCustomerAccountInvoiceService invoiceService;
    private DefaultTkEuB2BUnitService         b2bUnitService;
    private TkEuSAPDocumentExistsCacheService tkEuSAPDocumentExistsCacheService;

    public DefaultB2BCommerceUnitService getDefaultB2BCommerceUnitService() {
        return defaultB2BCommerceUnitService;
    }

    public void setDefaultB2BCommerceUnitService(final DefaultB2BCommerceUnitService defaultB2BCommerceUnitService) {
        this.defaultB2BCommerceUnitService = defaultB2BCommerceUnitService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public DefaultB2BCustomerService getDefaultB2BCustomerService() {
        return defaultB2BCustomerService;
    }

    public void setDefaultB2BCustomerService(final DefaultB2BCustomerService defaultB2BCustomerService) {
        this.defaultB2BCustomerService = defaultB2BCustomerService;
    }

    public TkEuCustomerAccountDeliveryService getCustomerAccountDeliveryService() {
        return customerAccountDeliveryService;
    }

    public void setCustomerAccountDeliveryService(final TkEuCustomerAccountDeliveryService customerAccountDeliveryService) {
        this.customerAccountDeliveryService = customerAccountDeliveryService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuB2bCustomerAccountService getTkEuCustomerAccountService() {
        return tkEuCustomerAccountService;
    }

    public void setTkEuCustomerAccountService(final TkEuB2bCustomerAccountService tkEuCustomerAccountService) {
        this.tkEuCustomerAccountService = tkEuCustomerAccountService;
    }

    public TkEuCustomerAccountInvoiceService getInvoiceService() {
        return invoiceService;
    }

    public void setInvoiceService(final TkEuCustomerAccountInvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(final DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    public TkEuSAPDocumentExistsCacheService getTkEuSAPDocumentExistsCacheService() {
        return tkEuSAPDocumentExistsCacheService;
    }

    public void setTkEuSAPDocumentExistsCacheService(final TkEuSAPDocumentExistsCacheService tkEuSAPDocumentExistsCacheService) {
        this.tkEuSAPDocumentExistsCacheService = tkEuSAPDocumentExistsCacheService;
    }

    @Override
    public DocumentDownloadServiceEnum getType() {
        return DocumentDownloadServiceEnum.REAL;
    }

    @Override
    public boolean isAuthorizedUser(final DocumentDownloadRequest documents) {
        DocumentRequest docRequest = null;
        if (CollectionUtils.isNotEmpty(documents.getDocumentRequests())) {
            docRequest = documents.getDocumentRequests().get(0);
        }
        final B2BUnitModel b2BUnitModel = getParentUnit();
        if (docRequest != null) {
            if (ORDER.equalsIgnoreCase(docRequest.getDocumenType())) {
                return getOrderDocumentAuthorizationStatus(docRequest, b2BUnitModel);
            } else if (DELIVERY.equalsIgnoreCase(docRequest.getDocumenType())
                || CERTIFICATE.equalsIgnoreCase(docRequest.getDocumenType())) {
                return getDeliveryDocumentAuthorizationStatus(docRequest, b2BUnitModel);
            } else if (INVOICE.equalsIgnoreCase(docRequest.getDocumenType())) {
                return getInvoiceDocumentAuthorizationStatus(docRequest, b2BUnitModel);
            }
        }
        return false;
    }

    private boolean getOrderDocumentAuthorizationStatus(final DocumentRequest docRequest, final B2BUnitModel b2BUnitModel) {
        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
        final OrderModel orderModel = getOrderModel(docRequest, baseStoreModel);
        if (orderModel != null
            && b2BUnitModel != null
            && b2BUnitModel.getUid().equals(orderModel.getUnit().getUid())) {
            return true;
        }

        return false;
    }

    private boolean getDeliveryDocumentAuthorizationStatus(final DocumentRequest docRequest, final B2BUnitModel b2BUnitModel) {
        final ConsignmentModel consignmentModel = getConsignment(docRequest.getDocumentNo());
        if (consignmentModel != null
            && consignmentModel.getUnit() != null
            && b2BUnitModel.getUid().equals(consignmentModel.getUnit().getUid())) {
            return true;
        }

        return false;
    }

    private boolean getInvoiceDocumentAuthorizationStatus(final DocumentRequest docRequest, final B2BUnitModel b2BUnitModel) {
        final InvoiceModel invoiceModel = getInvoiceModel(docRequest.getDocumentNo());
        if (invoiceModel != null
            && invoiceModel.getUnit() != null
            && b2BUnitModel.getUid().equals(invoiceModel.getUnit().getUid())) {
            return true;
        }

        return false;
    }

    @Override
    public Object getDocumentStatus(final DocumentDownloadRequest documentRequest) {
        return checkExistenceStatus(documentRequest);
    }

    private Object checkExistenceStatus(final DocumentDownloadRequest documentDownloadRequest) {

        String lineItemNo = "";
        String documentNo = "";
        String documentType = "";
        for (final DocumentRequest documentRequest : documentDownloadRequest.getDocumentRequests()) {
            if (!"".equals(documentRequest.getDocumentNo())) {
                documentNo = documentRequest.getDocumentNo();
            }
            if (StringUtils.isNotEmpty(documentRequest.getLineItemNo())) {
                lineItemNo = lineItemNo.concat("_").concat(documentRequest.getLineItemNo());
            }
            documentType = documentRequest.getDocumenType();
        }

        final String key = documentNo + lineItemNo + "_" + documentType;
        final CacheKey cacheKey = tkEuSAPDocumentExistsCacheService.getKey(key, getParentUnit().getUid());
        final Optional<Object> cachedValue = tkEuSAPDocumentExistsCacheService.get(cacheKey);
        if (cachedValue.isPresent()) {
            LOG.info("TKEuSAPDownloadService: Returning GetDocumentStatus value from cache");
            return cachedValue.get();
        }
        final Object response = callSAPProService(createDocumentStatusRequest(documentDownloadRequest), SAP_GETDOCUMENTSTATUSURL);
        if (response != null
            && checkPositiveResponse(response)) {
            LOG.info("TKEuSAPDownloadService: Inserting GetDocumentStatus value into cachMe");
            tkEuSAPDocumentExistsCacheService.put(cacheKey, response);
        }

        return response;
    }

    private boolean checkPositiveResponse(final Object response) {
        if (response instanceof GetDocumentStatusSAPSingleResponse) {
            final GetDocumentStatusSAPSingleResponse getDocumentStatusSAPSingleResponse = (GetDocumentStatusSAPSingleResponse) response;
            if (getDocumentStatusSAPSingleResponse.getGetDocumentStatusSAP() != null
                && ONE.equals(getDocumentStatusSAPSingleResponse.getGetDocumentStatusSAP().getDocumentExistsFlag())) {
                return true;
            }
        } else if (response instanceof GetDocumentStatusSAPResponse) {
            final GetDocumentStatusSAPResponse getDocumentStatusSAPResponse = (GetDocumentStatusSAPResponse) response;
            if (CollectionUtils.isNotEmpty(getDocumentStatusSAPResponse.getGetDocumentStatusSAP())) {
                for (final GetDocumentStatusSAP getDocumentStatusSAP : getDocumentStatusSAPResponse.getGetDocumentStatusSAP()) {
                    if (!ONE.equals(getDocumentStatusSAP.getDocumentExistsFlag())) {
                        return false;
                    }
                }
                return true;
            }
        }

        return false;
    }

    @Override
    public GetDocumentContentResponse getDocument(final DocumentDownloadRequest documentDownloadRequest) {

        final GetDocumentContentRequest getDocumentContentRequest = createDocumentContentRequest(documentDownloadRequest);
        final GetDocumentContentResponse getDocumentContentResponse = (GetDocumentContentResponse) callSAPProService(getDocumentContentRequest, SAP_GETDOCUMENTCONTENTURL);
        if (getDocumentContentResponse != null
            && getDocumentContentResponse.getDocumentContent() != null
            && StringUtils.isEmpty(getDocumentContentResponse.getDocumentContent().getContentType())) {
            LOG.info("TKEuSAPDownloadService: ContentType is empty for GetDocumentContentResponse with documentNo: " + getDocumentNo(getDocumentContentRequest));
        }
        return getDocumentContentResponse;
    }

    private GetDocumentContentRequest createDocumentContentRequest(final DocumentDownloadRequest documentDownloadRequest) {
        final GetDocumentContentRequest request = new GetDocumentContentRequest();
        request.setGetDocumentContent(createGetDocumentContent(documentDownloadRequest));
        return request;
    }

    private GetDocumentStatusRequest createDocumentStatusRequest(final DocumentDownloadRequest documentDownloadRequest) {
        final GetDocumentStatusRequest request = new GetDocumentStatusRequest();
        request.setGetDocumentStatus(createGetDocumentContent(documentDownloadRequest));
        return request;
    }

    private GetDocumentContent createGetDocumentContent(final DocumentDownloadRequest documentDownloadRequest) {
        GetDocumentContent docStatusContent = new GetDocumentContent();
        docStatusContent.setHybrisRegionBusinessDivisionEnvironment(getHybrisRegion());
        docStatusContent.setIsCertsArePaid(ONE);
        docStatusContent.setIsContentAnonymized(getContentAnonymized());
        docStatusContent = updateDetailsForDocumentType(documentDownloadRequest.getDocumentRequests().get(0), docStatusContent);
        docStatusContent.setItemDetails(createItemDetails(documentDownloadRequest));
        return docStatusContent;
    }

    private String getHybrisRegion() {
        return configurationService.getConfiguration().getString(HYBRIS_REGION);
    }

    private String getContentAnonymized() {
        return configurationService.getConfiguration().getString(CONTENT_ANONYMIZED);
    }

    private List<ItemDetails> createItemDetails(final DocumentDownloadRequest documentDownloadRequest) {
        final List<ItemDetails> itemList = new ArrayList<ItemDetails>();
        for (final DocumentRequest documentRequest : documentDownloadRequest.getDocumentRequests()) {
            final ItemDetails itemDetails = new ItemDetails();
            if (CERTIFICATE.equalsIgnoreCase(documentRequest.getDocumenType())) {
                itemDetails.setMaterialNo(getMaterialNo(documentRequest));
            }
            itemDetails.setDocumentNo(documentRequest.getDocumentNo());
            itemDetails.setLineItemNo(documentRequest.getLineItemNo());
            itemDetails.setBatchNo(documentRequest.getBatchNo());
            itemList.add(itemDetails);
        }
        return itemList;
    }

    private String getMaterialNo(final DocumentRequest documentRequest) {
        final ConsignmentModel consignmentModel = getConsignment(documentRequest.getDocumentNo());
        final Set<ConsignmentEntryModel> entries = consignmentModel.getConsignmentEntries();
        ConsignmentEntryModel entryModel = null;
        for (final ConsignmentEntryModel entry : entries) {
            if (entry.getEntryNumber().equals(documentRequest.getLineItemNo())) {
                entryModel = entry;
                break;
            }
        }

        if (entryModel != null
            && entryModel.getOrderEntry() != null
            && entryModel.getOrderEntry().getProduct() != null) {
            final String productCode = entryModel.getOrderEntry().getProduct().getCode();
            final String materialNo = SAP_DUMMY_PRODUCT.equalsIgnoreCase(productCode) ? getMaterialNoForDummyProduct(entryModel) : productCode;
            LOG.info("TKEuSAPDownloadService: MaterialNo for certificates with entryNo:" + documentRequest.getLineItemNo() + " -" + materialNo);
            return materialNo;
        }
        LOG.info("TKEuSAPDownloadService: Material Null for certificates");
        // Returning "0" as SAP does not return array for null materialNo which will fail during deserialization of SAP response
        return "0";
    }

    private String getMaterialNoForDummyProduct(final ConsignmentEntryModel entryModel) {
        LOG.debug("Inside getMaterialNoForDummyProduct");
        final List<AbstractOrderEntryProductInfoModel> productInfos = entryModel.getOrderEntry().getProductInfos();
        Optional<String> productCode = Optional.empty();
        if (productInfos != null) {
            productCode = productInfos.stream()
                .filter(abstractOrderEntryProductInfoModel -> ConfiguratorType.SAPPRODUCTINFO == abstractOrderEntryProductInfoModel.getConfiguratorType())
                .map(abstractOrderEntryProductInfoModel -> (TkSapProductInfoModel) abstractOrderEntryProductInfoModel).map(tkSapProductInfoModel -> tkSapProductInfoModel.getProductCode()).findFirst();
        }
        return productCode.isPresent() ? productCode.get(): "";
    }

    private Object callSAPProService(final Object docRequest, final String requestUrl) {
        final boolean validRequest = validateRequest(docRequest);
        logObject(docRequest, REQUEST);
        final HttpHeaders headers = createHeader();
        final HttpEntity<?> request = new HttpEntity<>(docRequest, headers);

        final String url = getUrl(requestUrl);

        addMessageConverter();
        Object response = null;
        if (validRequest && SAP_GETDOCUMENTSTATUSURL.equals(requestUrl)) {
            final GetDocumentStatusRequest getDocumentStatusRequest = (GetDocumentStatusRequest) docRequest;
            try {
                if (isCertificatesRequest(getDocumentStatusRequest)) {
                    final ResponseEntity<GetDocumentStatusSAPResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, GetDocumentStatusSAPResponse.class);
                    response = responseEntity.getBody();
                } else {
                    final ResponseEntity<GetDocumentStatusSAPSingleResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, GetDocumentStatusSAPSingleResponse.class);
                    response = responseEntity.getBody();
                }
                logObject(response, RESPONSE);
            } catch (final Exception e) {
                LOG.error("ERROR during SAP Document Exists call: " + e.getMessage());
            }
        } else if (validRequest && SAP_GETDOCUMENTCONTENTURL.equals(requestUrl)) {
            try {
                final ResponseEntity<GetDocumentContentResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, GetDocumentContentResponse.class);
                response = responseEntity.getBody();
            } catch (final Exception e) {
                LOG.error("ERROR during SAP Document Content call: " + e.getMessage());
                final GetDocumentContentResponse getDocumentContentResponse = new GetDocumentContentResponse();
                final GetDocumentSAPContent getDocumentSAPContent = new GetDocumentSAPContent();
                getDocumentSAPContent.setStatusMessage(e.getMessage());
                getDocumentContentResponse.setDocumentContent(getDocumentSAPContent);
                response = getDocumentContentResponse;
            }
        }
        return response;
    }

    private boolean validateRequest(final Object docRequest) {
        boolean isValid = true;
        if (docRequest instanceof GetDocumentContentRequest) {
            final GetDocumentContentRequest request = (GetDocumentContentRequest) docRequest;
            if (request.getGetDocumentContent() != null) {
                final String documentNo = getDocumentNo(docRequest);
                isValid = validateGetDocumentContentRequest(request, documentNo);
            }
        } else if (docRequest instanceof GetDocumentStatusRequest) {
            final GetDocumentStatusRequest request = (GetDocumentStatusRequest) docRequest;
            if (request.getGetDocumentStatus() != null) {
                final String documentNo = getDocumentNo(docRequest);
                isValid = validateGetDocumentStatusRequest(request, documentNo);
            }
        }

        return isValid;
    }

    private boolean validateGetDocumentStatusRequest(final GetDocumentStatusRequest request, final String documentNo) {
        boolean isValid = true;
        if (StringUtils.isEmpty(request.getGetDocumentStatus().getCustomerNumber())) {
            LOG.info("Missing required parameter CustomerNo in GetDocumentStatusSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentStatus().getDistributionChannel())) {
            LOG.info("Missing required parameter DistributionChannel in GetDocumentStatusSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentStatus().getSalesOrganization())) {
            LOG.info("Missing required parameter SalesOrganization in GetDocumentStatusSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentStatus().getDivision())) {
            LOG.info("Missing required parameter Division in GetDocumentStatusSAP request for document No: " + documentNo);
            isValid = false;
        }
        return isValid;
    }

    private boolean validateGetDocumentContentRequest(final GetDocumentContentRequest request, final String documentNo) {
        boolean isValid = true;
        if (StringUtils.isEmpty(request.getGetDocumentContent().getCustomerNumber())) {
            LOG.info("Missing required parameter CustomerNo in GetDocumentContentSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentContent().getDistributionChannel())) {
            LOG.info("Missing required parameter DistributionChannel in GetDocumentContentSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentContent().getSalesOrganization())) {
            LOG.info("Missing required parameter SalesOrganization in GetDocumentContentSAP request for document No: " + documentNo);
            isValid = false;
        }
        if (StringUtils.isEmpty(request.getGetDocumentContent().getDivision())) {
            LOG.info("Missing required parameter Division in GetDocumentContentSAP request for document No: " + documentNo);
            isValid = false;
        }
        return isValid;
    }

    private String getDocumentNo(final Object docRequest) {
        if (docRequest instanceof GetDocumentContentRequest) {
            final GetDocumentContentRequest request = (GetDocumentContentRequest) docRequest;
            if (request.getGetDocumentContent() != null) {
                final ItemDetails item = CollectionUtils.isNotEmpty(request.getGetDocumentContent().getItemDetails()) ? request.getGetDocumentContent().getItemDetails().get(0) : null;
                return item != null ? item.getDocumentNo() : null;
            }
        } else if (docRequest instanceof GetDocumentStatusRequest) {
            final GetDocumentStatusRequest request = (GetDocumentStatusRequest) docRequest;
            if (request.getGetDocumentStatus() != null) {
                final ItemDetails item = CollectionUtils.isNotEmpty(request.getGetDocumentStatus().getItemDetails()) ? request.getGetDocumentStatus().getItemDetails().get(0) : null;
                return item != null ? item.getDocumentNo() : null;
            }
        }

        return null;
    }

    private void logObject(final Object docRequest, final String type) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String value = mapper.writeValueAsString(docRequest);
            LOG.info(type + ":" + value);
        } catch (final Exception e) {
            LOG.error("TKEuSAPDownloadService: Error while parsing for logging" + type + ": " + e.getMessage());
        }
    }

    private boolean isCertificatesRequest(final GetDocumentStatusRequest docRequest) {
        if (CollectionUtils.size(docRequest.getGetDocumentStatus().getItemDetails()) > 1) {
            return true;
        }
        return false;
    }

    private HttpHeaders createHeader() {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        return headers;
    }

    private String getUrl(final String requestUrl) {
        return configurationService.getConfiguration().getString(requestUrl);
    }

    private void addMessageConverter() {
        final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        final StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();

        final List<HttpMessageConverter<?>> list = new ArrayList<HttpMessageConverter<?>>();
        list.add(converter);
        list.add(stringHttpMessageConverter);
        restTemplate.setMessageConverters(list);
    }

    protected B2BUnitModel getParentUnit() {
        final UserModel currentUser = userService.getCurrentUser();
        if (currentUser instanceof B2BCustomerModel) {
            final B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) currentUser;
            final B2BUnitModel defaultB2BUnit = b2BCustomerModel.getDefaultB2BUnit();
            if (defaultB2BUnit != null) {
                return defaultB2BUnit;
            }
        }
        return defaultB2BCommerceUnitService.getRootUnit();
    }

    private GetDocumentContent updateDetailsForDocumentType(final DocumentRequest documentRequest, final GetDocumentContent docStatusContent) {
        if (ORDER.equalsIgnoreCase(documentRequest.getDocumenType())) {
            docStatusContent.setDocumentCategory("C");
            populateDocumentStatusContentOrder(docStatusContent, documentRequest);
        } else if (DELIVERY.equalsIgnoreCase(documentRequest.getDocumenType())
            || CERTIFICATE.equalsIgnoreCase(documentRequest.getDocumenType())) {
            docStatusContent.setDocumentCategory("J");
            populateDocumentStatusContentDelivery(docStatusContent, documentRequest);
        } else if (INVOICE.equalsIgnoreCase(documentRequest.getDocumenType())) {
            docStatusContent.setDocumentCategory("M");
            populateDocumentStatusContentInvoice(docStatusContent, documentRequest);
        } else {
            LOG.error("SAP Document Download Service invalid DocumentType" + documentRequest.getDocumenType());
        }

        return docStatusContent;
    }

    private void populateDocumentStatusContentOrder(final GetDocumentContent docStatusContent, final DocumentRequest documentRequest) {

        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();

        final OrderModel orderModel = getOrderModel(documentRequest, baseStoreModel);
        docStatusContent.setDivision(orderModel.getDivision());
        docStatusContent.setDistributionChannel(orderModel.getDistributionChannel());
        docStatusContent.setSalesOrganization(orderModel.getSalesOrganization());
        docStatusContent.setCustomerNumber(getParentUnit().getUid());
    }

    private void populateDocumentStatusContentDelivery(final GetDocumentContent docStatusContent, final DocumentRequest documentRequest) {
        final ConsignmentModel consignmentModel = getConsignment(documentRequest.getDocumentNo());
        docStatusContent.setDivision(consignmentModel.getDivision());
        docStatusContent.setDistributionChannel(consignmentModel.getDistributionChannel());
        docStatusContent.setSalesOrganization(consignmentModel.getSalesOrg());
        docStatusContent.setCustomerNumber(consignmentModel.getGoodsRecipient() != null ? consignmentModel.getGoodsRecipient() : getParentUnit().getUid());
    }

    private void populateDocumentStatusContentInvoice(final GetDocumentContent docStatusContent, final DocumentRequest documentRequest) {
        final InvoiceModel invoiceModel = getInvoiceModel(documentRequest.getDocumentNo());
        docStatusContent.setDivision(invoiceModel.getDivision());
        docStatusContent.setDistributionChannel(invoiceModel.getDistributionChannel());
        docStatusContent.setSalesOrganization(invoiceModel.getSalesOrg());
        docStatusContent.setCustomerNumber(invoiceModel.getSoldToAddress() != null ? invoiceModel.getSoldToAddress().getSapCustomerID() : (invoiceModel.getPayerAddress() != null ? invoiceModel.getPayerAddress().getSapCustomerID() : getParentUnit().getUid()));
    }

    private OrderModel getOrderModel(final DocumentRequest documentRequest, final BaseStoreModel baseStoreModel) {
        OrderModel orderModel = null;
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        try {
            orderModel = getTkEuCustomerAccountService().getOrderConfirmationDetailsForCode(documentRequest.getDocumentNo(), baseStoreModel, b2bUnits);
        } catch (final ModelNotFoundException e) {
            throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, documentRequest.getDocumentNo()));
        }

        if (orderModel == null) {
            throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, documentRequest.getDocumentNo()));
        }

        return orderModel;
    }

    private InvoiceModel getInvoiceModel(final String invoiceNo) {
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        final List<InvoiceModel> invoiceModels = getInvoiceService().fetchInvoices(invoiceNo, b2bUnits);
        InvoiceModel invoiceModel = null;
        if (CollectionUtils.isNotEmpty(invoiceModels)) {
            invoiceModel = invoiceModels.get(0);
        }
        if (invoiceModel == null) {
            throw new UnknownIdentifierException(String.format(INVOICE_NOT_FOUND_FOR_USER_AND_BASE_STORE, invoiceNo));
        }
        return invoiceModel;
    }

    //Get Deliveries information
    private ConsignmentModel getConsignment(final String deliveryNo) {
        final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
        final List<ConsignmentModel> consignmentModels = customerAccountDeliveryService.fetchDeliveries(deliveryNo, b2bUnits);
        ConsignmentModel consignmentData = null;
        if (CollectionUtils.isNotEmpty(consignmentModels)) {
            consignmentData = consignmentModels.get(0);
            return consignmentData;
        }
        if (consignmentData == null) {
            throw new UnknownIdentifierException(String.format(CONSIGNMENT_NOT_FOUND_FOR_USER_AND_BASE_STORE, deliveryNo));
        }
        return null;
    }
}


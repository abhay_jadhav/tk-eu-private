package com.thyssenkrupp.b2b.eu.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class TkEuB2bRegistrationEmailEvent extends AbstractCommerceUserEvent<BaseSiteModel> {
    private String token;

    public TkEuB2bRegistrationEmailEvent(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import static de.hybris.platform.commerceservices.order.CommerceCartModificationStatus.SUCCESS;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.impl.CommerceUpdateCartEntryStrictStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.service.strategies.TkEuCommerceUpdateCartEntryStrategy;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.CertificateConfiguredProductInfoModel;

public class DefaultTkEuCommerceUpdateCartEntryStrategy extends CommerceUpdateCartEntryStrictStrategy implements TkEuCommerceUpdateCartEntryStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuCommerceUpdateCartEntryStrategy.class);
    @Override
    public CommerceCartModification updateAtpInformationForCartEntry(final CommerceCartParameter parameters) {
        final long entryNumber = parameters.getEntryNumber();
        final CartModel cartModel = parameters.getCart();
        final Date sapAtpResult = parameters.getSapAtpResult();
        final InStockStatus hybrisAtpResult = parameters.getHybrisAtpResult();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        final AbstractOrderEntryModel entryToUpdate = getEntryForNumber(cartModel, (int) entryNumber);
        updateAtpResults(sapAtpResult, hybrisAtpResult, entryToUpdate);
        getModelService().refresh(cartModel);
        return generateCommerceCartModification(entryToUpdate);
    }

    @Override
    public CommerceCartModification updateAtpConsolidatedDate(final CommerceCartParameter parameters) {
        final CartModel cartModel = parameters.getCart();
        final Date consolidatedDate = parameters.getAtpConsolidatedDate();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        cartModel.setOverallAtpDate(consolidatedDate);
        getModelService().save(cartModel);
        getModelService().refresh(cartModel);
        final CommerceCartModification modification = new CommerceCartModification();
        modification.setStatusCode(SUCCESS);
        return modification;
    }

    @Override
    public CommerceCartModification updateCertificateForCartEntry(final CommerceCartParameter parameter) {
        final String selectedCertificateId = parameter.getCertificateCode();
        final CartModel cartModel = parameter.getCart();
        validateParameterNotNull(cartModel, "CartModel cannot be null");
        validateParameterNotNull(selectedCertificateId, "CertificateCode cannot be null");
        final CartEntryModel entryToUpdate = getCartService().getEntryForNumber(cartModel, (int) parameter.getEntryNumber());
        final List<AbstractOrderEntryProductInfoModel> productInfos = entryToUpdate.getProductInfos();
        updateCertificateProductInfos(productInfos, selectedCertificateId);
        getModelService().refresh(cartModel);
        recalculateCart(cartModel);
        return generateCommerceCartModification(entryToUpdate);
    }

    private void updateCertificateProductInfos(final List<AbstractOrderEntryProductInfoModel> productInfos, final String selectedCertificateId) {
        if (CollectionUtils.isNotEmpty(productInfos)) {
            for (final AbstractOrderEntryProductInfoModel productInfo : productInfos) {
                if (productInfo instanceof CertificateConfiguredProductInfoModel) {
                    final String certificateId = ((CertificateConfiguredProductInfoModel) productInfo).getCertificateId();
                    if (selectedCertificateId.equals(certificateId)) {
                        ((CertificateConfiguredProductInfoModel) productInfo).setChecked(true);
                    } else {
                        ((CertificateConfiguredProductInfoModel) productInfo).setChecked(false);
                    }
                    getModelService().save(productInfo);
                }
            }
        }
    }

    private CommerceCartModification generateCommerceCartModification(final AbstractOrderEntryModel entryModel) {
        final CommerceCartModification modification = new CommerceCartModification();
        modification.setEntry(entryModel);
        modification.setStatusCode(SUCCESS);
        return modification;
    }

    private void recalculateCart(final CartModel cartModel) {
        final CommerceCartParameter parameterForCartCalculation = new CommerceCartParameter();
        parameterForCartCalculation.setEnableHooks(true);
        parameterForCartCalculation.setCart(cartModel);
        getCommerceCartCalculationStrategy().recalculateCart(parameterForCartCalculation);
    }

    private void updateAtpResults(final Date sapAtpResult, final InStockStatus hybrisAtpResult, final AbstractOrderEntryModel entryToUpdate) {
        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug("sapAtpResult= {}", sapAtpResult);
                LOG.debug("hybrisAtpResult= {}", hybrisAtpResult);
                LOG.debug("entryToUpdate= {}", entryToUpdate);
            }
            if (entryToUpdate != null) {
                if (sapAtpResult != null) {
                    entryToUpdate.setSapATPCheckResult(sapAtpResult);
                    entryToUpdate.setHybrisATPCheckResult(null);
                } else if (hybrisAtpResult != null) {
                    entryToUpdate.setSapATPCheckResult(null);
                    entryToUpdate.setHybrisATPCheckResult(hybrisAtpResult);
                }
                getModelService().save(entryToUpdate);
            } else {
                LOG.error("NO entryToUpdate found");
            }
        } catch (final Exception e) {
            LOG.error("Excpetion {} occur while sapATPCheck", e);
        }
    }
}

package com.thyssenkrupp.b2b.eu.core.process.actions;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.email.EmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bAmbiguousRegistrationProcessModel;

public class TkEuB2bCustomerAmbigiousRegistrationSendToEmailAction extends AbstractSimpleDecisionAction<TkEuB2bAmbiguousRegistrationProcessModel> {

    public static final String CONTENT_CATALOG = "tk.eu.customerservice.catalog";
    private static final Logger LOG = Logger.getLogger(TkEuB2bCustomerAmbigiousRegistrationSendToEmailAction.class);
    private ConfigurationService configurationService;
    private ModelService modelService;
    private ProcessContextResolutionStrategy contextResolutionStrategy;
    private EmailGenerationService emailGenerationService;
    private CMSEmailPageService cmsEmailPageService;
    private CatalogService catalogService;
    private String frontendTemplateName;
    private CatalogVersionService catalogVersionService;

    @Override
    public Transition executeAction(final TkEuB2bAmbiguousRegistrationProcessModel businessProcessModel) {
        try {
            final String contentCatalogId = getConfigurationService().getConfiguration().getString("tk.eu.customerservice.catalog");
            if (contentCatalogId.isEmpty()) {
                LOG.warn("Could not find any Content Catalog : tk.eu.customerservice.catalog");
                return Transition.NOK;
            }
            getContextResolutionStrategy().initializeContext(businessProcessModel);
            final CatalogModel catalogModel = getCatalogService().getCatalogForId(contentCatalogId);
            if (catalogModel == null) {
                LOG.warn("Could not resolve the content catalog , cannot generate email content");
                return Transition.NOK;
            }
            final CatalogVersionModel contentCatalogVersion = catalogModel.getActiveCatalogVersion();
            if (contentCatalogVersion == null) {
                LOG.warn("Could not resolve the content catalog version, cannot generate email content");
                return Transition.NOK;
            }
            catalogVersionService.setSessionCatalogVersions(Arrays.asList(contentCatalogVersion));

            final EmailPageModel emailPageModel = getCmsEmailPageService().getEmailPageForFrontendTemplate(getFrontendTemplateName(), contentCatalogVersion);
            if (emailPageModel == null) {
                LOG.warn("Could not retrieve email page model for " + getFrontendTemplateName() + " and " + contentCatalogVersion.getCatalog().getName() + ":" + contentCatalogVersion.getVersion() + ", cannot generate email content");
                return Transition.NOK;
            }

            final EmailMessageModel emailMessageModel = getEmailGenerationService().generate(businessProcessModel, emailPageModel);
            if (emailMessageModel == null) {
                LOG.warn("Failed to generate email message");
                return Transition.NOK;
            }

            getModelService().save(emailMessageModel);

            businessProcessModel.setEmails(Arrays.asList(emailMessageModel));

            getModelService().save(businessProcessModel);

            LOG.info("Email message generated");

            return Transition.OK;
        } catch (final Exception ex) {
            LOG.error("Error while Sending Ambiguous Registration mail", ex);
            return Transition.NOK;
        }
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }

    public void setCatalogService(final CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public ProcessContextResolutionStrategy getContextResolutionStrategy() {
        return contextResolutionStrategy;
    }

    public void setContextResolutionStrategy(final ProcessContextResolutionStrategy contextResolutionStrategy) {
        this.contextResolutionStrategy = contextResolutionStrategy;
    }

    public EmailGenerationService getEmailGenerationService() {
        return emailGenerationService;
    }

    public void setEmailGenerationService(final EmailGenerationService emailGenerationService) {
        this.emailGenerationService = emailGenerationService;
    }

    public CMSEmailPageService getCmsEmailPageService() {
        return cmsEmailPageService;
    }

    public void setCmsEmailPageService(final CMSEmailPageService cmsEmailPageService) {
        this.cmsEmailPageService = cmsEmailPageService;
    }

    public String getFrontendTemplateName() {
        return frontendTemplateName;
    }

    public void setFrontendTemplateName(final String frontendTemplateName) {
        this.frontendTemplateName = frontendTemplateName;
    }

    /**
     * @return the catalogVersionService
     */
    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService the catalogVersionService to set
     */
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

}

package com.thyssenkrupp.b2b.eu.core.decorators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus;
import com.thyssenkrupp.b2b.eu.core.service.impl.DefaultTkEuB2bCustomerService;

public class TkEuB2bCustomerRegStatusDecorator implements CSVCellDecorator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TkEuB2bCustomerRegStatusDecorator.class);
    private static final String IGNORE = "<ignore>";

    private final DefaultTkEuB2bCustomerService b2bCustomerService = (DefaultTkEuB2bCustomerService) Registry.getApplicationContext().getBean("b2bCustomerService");

    @Override
    public String decorate(final int position, final Map<Integer, String> impexLine) {
        final String uid = impexLine.get(position);

        final B2bRegistrationStatus registrationStatus = findRegistrationStatusForCustomer(uid);

        return (registrationStatus != null && registrationStatus.equals(B2bRegistrationStatus.IMPORTED)) ? registrationStatus.toString() : IGNORE;
    }

    private Optional<B2BCustomerModel> findCustomerForUid(final String uid) {
        Optional<B2BCustomerModel> b2BCustomer = Optional.empty();
        try {
            final CustomerModel userModel = b2bCustomerService.getAllUserAllStatusesForUID(uid, CustomerModel.class);
            if (userModel instanceof B2BCustomerModel) {
                b2BCustomer = Optional.of((B2BCustomerModel) userModel);
            }
        } catch (final UnknownIdentifierException exception) {
            LOGGER.debug("Initial dataload for user \"{}\", setting registrationStatus={}.", uid, IGNORE);
        }
        return b2BCustomer;
    }

    private B2bRegistrationStatus findRegistrationStatusForCustomer(final String uid) {
        final Optional<B2BCustomerModel> b2BCustomer = findCustomerForUid(uid);
        if (b2BCustomer.isPresent()) {
            return b2BCustomer.get().getRegistrationStatus();
        } else {
            return B2bRegistrationStatus.IMPORTED;
        }
    }
}

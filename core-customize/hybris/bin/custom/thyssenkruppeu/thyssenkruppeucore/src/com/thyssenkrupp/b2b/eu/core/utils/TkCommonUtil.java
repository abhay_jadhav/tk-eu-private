package com.thyssenkrupp.b2b.eu.core.utils;

import static org.apache.commons.lang.ArrayUtils.EMPTY_STRING_ARRAY;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;

public final class TkCommonUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    private static final Logger LOG = Logger.getLogger(TkCommonUtil.class);

    private TkCommonUtil() {
        throw new UnsupportedOperationException();
    }


    /**
     * This method convert the java.util.Date object to the specified date format.
     *
     * @param dateFormat
     * @return
     */
    public static String dateToString(final String dateFormat, final Date dateValue) {
        DateFormat df = null;
        String strDate = null;
        try {
            if (dateValue == null) {
                return null;
            }

            df = new SimpleDateFormat(dateFormat);
            strDate = df.format(dateValue);
        } catch (final Exception ex) {
            LOG.error(ex.getMessage());
        }

        return strDate;
    }

    public static String[] splitByCommaCharacter(final String commaSeparatedString) {
        if (isNotEmpty(commaSeparatedString)) {
            return commaSeparatedString.split(ThyssenkruppeucoreConstants.REGEX_COMMA);
        }
        return EMPTY_STRING_ARRAY;
    }

    public static String stripTrailingZeros(final double value) {
        return BigDecimal.valueOf(value).stripTrailingZeros().toPlainString();
    }
}

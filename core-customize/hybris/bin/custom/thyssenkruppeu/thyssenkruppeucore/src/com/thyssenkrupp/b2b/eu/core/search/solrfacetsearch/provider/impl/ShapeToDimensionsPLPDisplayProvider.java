package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.NumberFormat;
import java.util.*;

public class ShapeToDimensionsPLPDisplayProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {

    private static final Logger         LOG          = Logger.getLogger(ShapeToDimensionsPLPDisplayProvider.class);
    private static final String         VARIANT_CODE = "VariantProduct Code : ";
    private static final Character      COMMA        = ',';
    private static final String         COLON        = ":";
    private String                      shapeFeatureCode;
    private String                      shapeToDimensionsPLPDisplayPattern;
    private FieldNameProvider           fieldNameProvider;
    private CommonI18NService           commonI18NService;
    private ClassificationService       classificationService;
    private final Map<String, String>   dataTypeMap = new HashMap();
    private String                      identicalValuePLPDisplayPattern;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        if (model instanceof ProductModel) {
            final ProductModel productModel = (ProductModel) model;
            final Optional<FeatureList> featuresForMaster = getFeaturesForProduct(productModel);
            if (featuresForMaster.isPresent()) {
                return findFieldValues(indexConfig, indexedProperty, productModel, featuresForMaster.get());
            }
        }
        return Collections.emptyList();
    }

    private Collection<FieldValue> findFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final ProductModel productModel, final FeatureList features) {
        final Optional<String> featureValueOrEmpty = getShapeFeatureValueOrEmpty(features);
        if (featureValueOrEmpty.isPresent()) {
            LOG.debug("Product code : " + productModel.getCode() + ", shapeFeatureValue : " + featureValueOrEmpty.get());
            final List<String> dimensionsForShape = getDimensionsForShape(indexedProperty, featureValueOrEmpty.get());
            if (CollectionUtils.isNotEmpty(dimensionsForShape)) {
                return createLocalisedOrNonLocalizedFieldValues(indexConfig, indexedProperty, productModel, dimensionsForShape);
            }
        }
        return Collections.emptyList();
    }

    private List<FieldValue> createLocalisedOrNonLocalizedFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final ProductModel productModel, final List<String> dimensionsForShape) {
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (indexedProperty.isLocalized()) {
            indexConfig.getLanguages().forEach(language -> fieldValues.addAll(findFieldValuesForShape(productModel, dimensionsForShape, indexedProperty, language)));
        } else {
            fieldValues.addAll(findFieldValuesForShape(productModel, dimensionsForShape, indexedProperty, null));
        }
        return fieldValues;
    }

    private List<FieldValue> findFieldValuesForShape(final ProductModel productModel, final List<String> dimensionsForShape, final IndexedProperty indexedProperty, final LanguageModel language) {
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (language == null) {
            buildNonLocalisedFieldValues(productModel, dimensionsForShape, indexedProperty, fieldValues);
        } else {
            buildLocalisedFieldValues(productModel, dimensionsForShape, indexedProperty, language, fieldValues);
        }
        if (LOG.isDebugEnabled()) {
            fieldValues.forEach(fieldValue -> LOG.debug("fieldName : " + fieldValue.getFieldName() + " and fieldNameValue : " + fieldValue.getValue().toString()));
        }
        return fieldValues;
    }

    private void buildNonLocalisedFieldValues(final ProductModel productModel, final List<String> dimensionsForShape, final IndexedProperty indexedProperty, final List<FieldValue> fieldValues) {
        final List<String> dimensionValues = new LinkedList<>();
        final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
        final Map<String, List<String>> featuresForShape = getFeaturesForShape(productModel, dimensionsForShape, currentLanguage);
        buildDimensionValuesForShape(dimensionValues, featuresForShape, null);
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
        buildFieldValues(fieldValues, dimensionValues, fieldNames);
    }
    private void buildLocalisedFieldValues(final ProductModel productModel, final List<String> dimensionsForShape, final IndexedProperty indexedProperty, final LanguageModel language, final List<FieldValue> fieldValues) {
        final List<String> dimensionValues = new LinkedList<>();
        final Map<String, List<String>> featuresForShape = getFeaturesForShape(productModel, dimensionsForShape, language);
        buildDimensionValuesForShape(dimensionValues, featuresForShape, language);
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
        buildFieldValues(fieldValues, dimensionValues, fieldNames);
    }

    private void buildDimensionValuesForShape(final List<String> dimensionValues, final Map<String, List<String>> featuresForShape, final LanguageModel language) {
        featuresForShape.forEach((featureName, featureValues) -> {
            if (CollectionUtils.isNotEmpty(featureValues)) {
                Map<String, String> minMaxValue = new HashMap<>();
                getMinAndMaxValueFromFeatureValues(featureName, featureValues, minMaxValue, language);
                formatFeatureNameWithMinMaxValue(featureName, minMaxValue, dimensionValues);
            } else {
                LOG.warn("featureValues empty for the featureName : " + featureName);
            }
        });
    }

    private void formatFeatureNameWithMinMaxValue(String featureName, Map<String, String> minMaxValue, List<String> dimensionValues) {
        String featureMinValue = minMaxValue.get("featureMinValue");
        String featureMaxValue = minMaxValue.get("featureMaxValue");
        if (StringUtils.isNotBlank(featureMinValue) && StringUtils.isNotBlank(featureMaxValue)) {
            String formattedFeatureValue = featureMinValue.equals(featureMaxValue) ?
                    getFeatureNameWithIdenticalValue(featureName, featureMinValue) : getFeatureNameWithMinAndMaxValue(featureName, featureMinValue, featureMaxValue);
            dimensionValues.add(formattedFeatureValue);
        }
    }

    private String getFeatureNameWithIdenticalValue(String featureName, String featureMinValue) {
        return String.format(getIdenticalValuePLPDisplayPattern(), featureName, featureMinValue);
    }

    private void getMinAndMaxValueFromFeatureValues(String featureName, List<String> featureValues, Map<String, String> minMaxValues, LanguageModel language) {
        final String featureDataType = dataTypeMap.get(featureName);
        if ("number".equals(featureDataType)) {
            convertFeatureValuesToDouble(featureValues, minMaxValues, language);
        } else {
            convertFeatureValuesToString(featureValues, minMaxValues);
        }
    }

    private void convertFeatureValuesToString(List<String> featureValues, Map<String, String> minMaxValues) {
        String featureMinValue = featureValues.stream().min(String.CASE_INSENSITIVE_ORDER).orElseGet(null);
        String featureMaxValue = featureValues.stream().max(String.CASE_INSENSITIVE_ORDER).orElseGet(null);
        if (StringUtils.isNotEmpty(featureMinValue) && StringUtils.isNotEmpty(featureMaxValue)) {
            minMaxValues.put("featureMinValue", featureMinValue.substring(0, featureMinValue.indexOf(COLON)));
            minMaxValues.put("featureMaxValue", featureMaxValue.substring(0, featureMaxValue.indexOf(COLON)));
            minMaxValues.put("unit", featureMaxValue.substring(featureMaxValue.indexOf(COLON) + 1, featureMaxValue.length()));
        }
    }

    private void convertFeatureValuesToDouble(List<String> featureValues, Map<String, String> minMaxValues, LanguageModel language) {
        List<Double> featureValuesList = new ArrayList<>(featureValues.size());
        featureValues.forEach(featureValue -> {
            try {
                final String doubleValue = featureValue.contains(COLON) ? featureValue.substring(0, featureValue.indexOf(COLON)) : featureValue;
                featureValuesList.add(Double.parseDouble(doubleValue));
            } catch (NumberFormatException exception) {
                LOG.error("Expected data type is Number but found String : " + exception);
            }
        });
        addUnit(featureValues, minMaxValues);
        updateFeatureRangeValues(featureValuesList, minMaxValues, language);
    }
    private void addUnit(List<String> featureValues, Map<String, String> minMaxValues) {
        final String withUnit = featureValues.iterator().next();
        if (StringUtils.isNotBlank(withUnit)) {
            minMaxValues.put("unit", withUnit.contains(COLON) ? withUnit.substring(withUnit.indexOf(COLON) + 1, withUnit.length()) : "");
        }
    }
    private void updateFeatureRangeValues(List<Double> featureValuesList, Map<String, String> minMaxValues, LanguageModel language) {
        if (CollectionUtils.isNotEmpty(featureValuesList)) {
            Collections.sort(featureValuesList);
            minMaxValues.put("featureMinValue", getNumberFormattedForLocale(featureValuesList.get(0), language));
            minMaxValues.put("featureMaxValue", getNumberFormattedForLocale(featureValuesList.get(featureValuesList.size() - 1), language));
        }
    }

    private String getFeatureNameWithMinAndMaxValue(final String featureName, final String minValue, final String maxValue) {
        return String.format(getShapeToDimensionsPLPDisplayPattern(), featureName, minValue, maxValue);
    }

    private void buildFieldValues(final List<FieldValue> fieldValues, final List<String> dimensionValues, final Collection<String> fieldNames) {
        fieldNames.forEach(fieldName -> dimensionValues.forEach(dimensionValue -> fieldValues.add(new FieldValue(fieldName, dimensionValue))));
    }

    private Map<String, List<String>> getFeaturesForShape(final ProductModel productModel, final List<String> dimensionsForShape, final LanguageModel language) {
        final Collection<VariantProductModel> variantProducts = productModel.getVariants();
        final Map<String, List<String>> featuresForShape = new LinkedHashMap<>();
        variantProducts.forEach(variantProduct -> {
            LOG.debug(VARIANT_CODE + variantProduct.getCode());
            final Optional<FeatureList> featuresForVariant = getFeaturesForProduct(variantProduct);
            featuresForVariant.ifPresent(features -> extractFeaturesForShape(dimensionsForShape, featuresForShape, features, language, variantProduct.getCode()));
        });
        return featuresForShape;
    }

    private void extractFeaturesForShape(final List<String> dimensionsForShape, final Map<String, List<String>> featuresForShape, final FeatureList featuresForVariant, final LanguageModel language, final String variantCode) {
        dimensionsForShape.forEach(dimensionForShape -> {
            LOG.debug("dimensionForShape : " + dimensionForShape);
            final Optional<Feature> featureOrEmpty = getFeatureOrEmpty(featuresForVariant, dimensionForShape.trim());
            if (featureOrEmpty.isPresent()) {
                buildFeatureForShape(featuresForShape, featureOrEmpty.get(), language, variantCode);
            } else {
                LOG.debug(VARIANT_CODE + variantCode + " ,does not have the expected dimension : " + dimensionForShape + ", in its Class Attribute assignment");
            }
        });
    }

    private void buildFeatureForShape(final Map<String, List<String>> featuresForShape, final Feature feature, final LanguageModel language, final String variantCode) {
        final String featureNameForLocale = getFeatureNameForLocale(feature, language).orElseGet(feature::getName);
        if (StringUtils.isNotEmpty(featureNameForLocale)) {
            dataTypeMap.put(featureNameForLocale, getFeaturesDataType(feature));
            buildFeature(featuresForShape, feature, language, variantCode, featureNameForLocale);
        } else {
            LOG.warn(VARIANT_CODE + variantCode + " ,featureNameForLocale is empty for the feature code : " + feature.getCode());
        }
    }

    private String getFeaturesDataType(Feature feature) {
        String enumType = "";
        final ClassAttributeAssignmentModel classAttributeAssignment = feature.getClassAttributeAssignment();
        if (classAttributeAssignment != null && classAttributeAssignment.getAttributeType() != null) {
            enumType = classAttributeAssignment.getAttributeType().getCode();
        }
        return enumType;
    }

    private void buildFeature(final Map<String, List<String>> featuresForShape, final Feature feature, final LanguageModel language, final String variantCode, final String featureNameForLocale) {
        final Optional<FeatureValue> featureValue = Optional.ofNullable(feature.getValue());
        if (featureValue.isPresent()) {
            buildFeatureValue(featuresForShape, language, variantCode, featureNameForLocale, featureValue.get());
        } else {
            LOG.warn(VARIANT_CODE + variantCode + " ,missing feature value for the feature code : " + feature.getCode());
        }
    }

    private void buildFeatureValue(final Map<String, List<String>> featuresForShape, final LanguageModel language, final String variantCode, final String featureNameForLocale, final FeatureValue featureValue) {
        final Optional<Object> valueForFeatureValue = Optional.ofNullable(featureValue.getValue());
        valueForFeatureValue.ifPresent(presentValueForFeatureValue -> {
            final Optional<ClassificationAttributeUnitModel> unitModel = Optional.ofNullable(featureValue.getUnit());
            if (unitModel.isPresent()) {
                buildFeaturesForShape(featuresForShape, featureNameForLocale, presentValueForFeatureValue.toString(), unitModel.get());
            } else {
                LOG.warn(VARIANT_CODE + variantCode + " ,missing feature unit symbol for the feature Value : " + presentValueForFeatureValue.toString());
            }
        });
    }

    private void buildFeaturesForShape(final Map<String, List<String>> featuresForShape, final String featureNameForLocale, final String formattedFeatureValue, final ClassificationAttributeUnitModel unitModel) {
        if (!featuresForShape.containsKey(featureNameForLocale)) {
            featuresForShape.put(featureNameForLocale, new ArrayList<>());
        }
        featuresForShape.get(featureNameForLocale).add(formattedFeatureValue + COLON + unitModel.getSymbol());
    }

    private String getNumberFormattedForLocale(final Double valueForFeatureValue, final LanguageModel language) {
        try {
            if (language != null) {
                final NumberFormat numberFormat = NumberFormat.getInstance(getCommonI18NService().getLocaleForLanguage(language));
                return numberFormat.format(valueForFeatureValue);
            }
        } catch (NumberFormatException numberFormatException) {
            LOG.error("NumberFormatException in getNumberFormattedForLocale : ", numberFormatException);
        }
        return valueForFeatureValue.toString();
    }

    private List<String> getDimensionsForShape(final IndexedProperty indexedProperty, final String shapeFeatureValueCode) {
        final Map<String, String> shapeToDimensionsMapping = indexedProperty.getValueProviderParameters();
        if (shapeToDimensionsMapping.containsKey(shapeFeatureValueCode.toUpperCase())) {
            return findDimensionsForShape(shapeToDimensionsMapping, shapeFeatureValueCode);
        }
        return Collections.emptyList();
    }

    private List<String> findDimensionsForShape(final Map<String, String> shapeToDimensionsMapping, final String shapeFeatureValueCode) {
        final String shapeToDimensions = shapeToDimensionsMapping.get(shapeFeatureValueCode.toUpperCase());
        final int semiColonIndex = shapeToDimensions.indexOf(COMMA);
        return semiColonIndex <= 0 ? Collections.singletonList(shapeToDimensions) : Arrays.asList(shapeToDimensions.split(","));
    }

    private Optional<Feature> getFeatureOrEmpty(final FeatureList features, final String featureCode) {
        return features.getFeatures().stream().filter(feature -> StringUtils.endsWith(feature.getCode().toLowerCase(), featureCode.toLowerCase())).findFirst();
    }

    private Optional<String> getFeatureNameForLocale(final Feature feature, final LanguageModel language) {
        try {
            i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
            return Optional.ofNullable(feature.getName());
        } finally {
            i18nService.setCurrentLocale(i18nService.getCurrentLocale());
        }
    }

    private Optional<String> getShapeFeatureValueOrEmpty(final FeatureList features) {
        final Optional<Object> shapeFeatureValue = getFeatureOrEmpty(features, getShapeFeatureCode()).map(Feature::getValue).map(FeatureValue::getValue);
        return shapeFeatureValue.isPresent() && shapeFeatureValue.get() instanceof ClassificationAttributeValueModel
                ? Optional.ofNullable(((ClassificationAttributeValueModel) shapeFeatureValue.get()).getCode()) : Optional.empty();
    }

    private Optional<FeatureList> getFeaturesForProduct(final ProductModel productModel) {
        return Optional.ofNullable(getClassificationService().getFeatures(productModel));
    }

    protected String getShapeFeatureCode() {
        return shapeFeatureCode;
    }

    @Required
    public void setShapeFeatureCode(String shapeFeatureCode) {
        this.shapeFeatureCode = shapeFeatureCode;
    }

    protected String getShapeToDimensionsPLPDisplayPattern() {
        return shapeToDimensionsPLPDisplayPattern;
    }
    @Required
    public void setShapeToDimensionsPLPDisplayPattern(String shapeToDimensionsPLPDisplayPattern) {
        this.shapeToDimensionsPLPDisplayPattern = shapeToDimensionsPLPDisplayPattern;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    protected ClassificationService getClassificationService() {
        return classificationService;
    }

    @Required
    public void setClassificationService(ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public String getIdenticalValuePLPDisplayPattern() {
        return identicalValuePLPDisplayPattern;
    }

    public void setIdenticalValuePLPDisplayPattern(String identicalValuePLPDisplayPattern) {
        this.identicalValuePLPDisplayPattern = identicalValuePLPDisplayPattern;
    }
}

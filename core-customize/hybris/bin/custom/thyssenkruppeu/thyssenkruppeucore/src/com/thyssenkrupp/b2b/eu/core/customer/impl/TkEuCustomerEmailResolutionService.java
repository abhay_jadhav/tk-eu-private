package com.thyssenkrupp.b2b.eu.core.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerEmailResolutionService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

public class TkEuCustomerEmailResolutionService extends DefaultCustomerEmailResolutionService {
    private static final Logger LOG = Logger.getLogger(TkEuCustomerEmailResolutionService.class);

    @Override
    protected String validateAndProcessEmailForCustomer(final CustomerModel customerModel) {
        validateParameterNotNullStandardMessage("customerModel", customerModel);

        final String email = customerModel.getUid().contains("|") ? StringUtils.substringAfter(customerModel.getUid(), "|") : customerModel.getUid();
        try {
            MailUtils.validateEmailAddress(email, "customer email");
            return email;
        } catch (final EmailException e) {
            LOG.info("Given uid is not appropriate email. Customer PK : " + String.valueOf(customerModel.getPk()) + " Exception: " + e.getClass().getName());
        }
        return null;
    }
}

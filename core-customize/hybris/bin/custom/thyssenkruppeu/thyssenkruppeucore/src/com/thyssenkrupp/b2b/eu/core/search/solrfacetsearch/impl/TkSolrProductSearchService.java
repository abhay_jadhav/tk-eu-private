package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.commerceservices.search.solrfacetsearch.impl.DefaultSolrProductSearchService;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.SolrAppendToSearchQueryMethodHook;

public class TkSolrProductSearchService<ITEM> extends DefaultSolrProductSearchService {

    private static final Logger LOG = Logger.getLogger(TkSolrProductSearchService.class);
    private List<SolrAppendToSearchQueryMethodHook> solrAppendToSearchQueryMethodHooks;

    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> favouriteProductsTextSearch(final String text, final SearchQueryContext searchQueryContext, final PageableData pageableData) {
        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setFreeTextSearch(text);
        searchQueryData.setFilterTerms(Collections.emptyList());
        searchQueryData.setSearchQueryContext(searchQueryContext);
        return doSearchForFavouriteProducts(searchQueryData, pageableData);
    }

    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> favouriteProductsCategorySearch(final String categoryCode, final SearchQueryContext searchQueryContext, final PageableData pageableData) {
        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setCategoryCode(categoryCode);
        searchQueryData.setFilterTerms(Collections.emptyList());
        searchQueryData.setSearchQueryContext(searchQueryContext);
        return doSearchForFavouriteProducts(searchQueryData, pageableData);
    }

    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> favouriteProductsSearch(final SolrSearchQueryData searchQueryData, final PageableData pageableData) {
        return doSearchForFavouriteProducts(searchQueryData, pageableData);
    }

    protected ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> doSearchForFavouriteProducts(final SolrSearchQueryData searchQueryData, final PageableData pageableData) {
        LOG.debug("************ doSearchForFavouriteProducts Start ************");
        validateParameterNotNull(searchQueryData, "SearchQueryData cannot be null");
        final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = buildSearchQueryPageableData(searchQueryData, pageableData);
        final SolrSearchRequest solrSearchRequest = getSearchQueryPageableConverter().convert(searchQueryPageableData);
        appendToSearchQuery(solrSearchRequest);
        final SolrSearchResponse solrSearchResponse = getSearchRequestConverter().convert(solrSearchRequest);
        LOG.debug("************ doSearchForFavouriteProducts End ************");
        return getSearchResponseConverter().convert(solrSearchResponse);
    }

    private void appendToSearchQuery(final SolrSearchRequest solrSearchRequest) {
        if (getSolrAppendToSearchQueryMethodHooks() != null) {
            for (final SolrAppendToSearchQueryMethodHook appendToSearchQueryMethodHook : getSolrAppendToSearchQueryMethodHooks()) {
                appendToSearchQueryMethodHook.appendToSearchQuery(solrSearchRequest);
            }
        }
    }

    @Override
    protected Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> getSearchQueryPageableConverter() {
        return super.getSearchQueryPageableConverter();
    }

    @Override
    protected Converter<SolrSearchRequest, SolrSearchResponse> getSearchRequestConverter() {
        return super.getSearchRequestConverter();
    }

    @Override
    protected Converter<SolrSearchResponse, ProductCategorySearchPageData> getSearchResponseConverter() {
        return super.getSearchResponseConverter();
    }

    protected List<SolrAppendToSearchQueryMethodHook> getSolrAppendToSearchQueryMethodHooks() {
        return solrAppendToSearchQueryMethodHooks;
    }

    public void setSolrAppendToSearchQueryMethodHooks(final List<SolrAppendToSearchQueryMethodHook> solrAppendToSearchQueryMethodHooks) {
        this.solrAppendToSearchQueryMethodHooks = solrAppendToSearchQueryMethodHooks;
    }
}


package com.thyssenkrupp.b2b.eu.core.service.order.daos.impl;

import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.service.order.daos.TkEuB2bOrderDao;

public class DefaultTkEuB2bOrderDao implements TkEuB2bOrderDao {
    private static final Logger LOG = Logger.getLogger(DefaultTkEuB2bOrderDao.class);
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<OrderModel> getOrdersToExport(final int exportOrderCount, final Long graceTime) {

        final Date currentTime=new Date();
        // final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {order." +
        // OrderModel.PK + "} " + "FROM {"
        // + OrderModel._TYPECODE + " AS order} " + "WHERE {order." + OrderModel.STATUS + "}
        // =?orderStatus AND ( {"
        // + OrderModel.EXPORTSTATUS
        // + "} =?exportStatus OR {exportStatus} is null) AND ?currentTime >
        // DATE_ADD({creationTime}, INTERVAL ?graceTime MINUTE)");

        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {order." + OrderModel.PK + "} " + "FROM {"
                + OrderModel._TYPECODE + " AS order} " + "WHERE {order." + OrderModel.STATUS + "} =(?orderStatus) AND ( {" + OrderModel.EXPORTSTATUS + "} =(?exportStatus) OR {exportStatus} is null) AND (?currentTime) > DATEADD(MINUTE, (?graceTime), {creationTime})");

        fQuery.addQueryParameter("orderStatus", OrderStatus.CREATED);
        fQuery.addQueryParameter("exportStatus", ExportStatus.NOTEXPORTED);
        fQuery.addQueryParameter("currentTime",currentTime);
        fQuery.addQueryParameter("graceTime", graceTime);
        fQuery.setCount(exportOrderCount);
        final SearchResult<OrderModel> result = getFlexibleSearchService().search(fQuery);
        LOG.info("Grace Time in Min :" + graceTime + " Configured Export Order Count :" + exportOrderCount + " Actual Exporting order size :" + result.getResult().size());
        return result.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    public OrderModel findInitialOrderByCode(final String orderCode) {
        final Map<String, Object> attr = new HashMap<String, Object>(1);
        attr.put(OrderModel.CODE, orderCode);
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT {o:pk} from { ").append(OrderModel._TYPECODE).append(" as o} WHERE {o:code} = ?code ").append("AND {o:" + OrderModel.INITIALORDERVERSION + "} = 1");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(sql.toString());
        query.getQueryParameters().putAll(attr);
        final SearchResult<OrderModel> result = this.getFlexibleSearchService().search(query);
        final List<OrderModel> orders = result.getResult();
        return orders.isEmpty() ? null : orders.get(0);
    }
}

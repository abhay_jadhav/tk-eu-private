
package com.thyssenkrupp.b2b.eu.core.product.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.product.dao.TkEuProductDao;
import com.thyssenkrupp.b2b.eu.core.service.order.daos.impl.DefaultTkEuB2bOrderDao;
import com.thyssenkrupp.b2b.eu.core.dao.impl.DefaultTkProductDao;

public class DefaultTkEuProductDao extends DefaultTkProductDao implements TkEuProductDao {
    private static final Logger LOG = Logger.getLogger(DefaultTkEuB2bOrderDao.class);

    public DefaultTkEuProductDao(final String typecode) {
        super(typecode);
    }

    @Override
    public List<ProductModel> findProductsByStatusAndCatalogVersion(final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(catalogVersion, "CatalogVersion must not be null!");
        final Map parameters = new HashMap();
        parameters.put(ProductModel.APPROVALSTATUS, ArticleApprovalStatus.CHECK);
        parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

        return find(parameters);
    }

    @Override
    public List<ProductModel> findProductsByCatalogVersionModifiedSince(final CatalogVersionModel catalogVersion, final Date lastModifiedDate) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("Select {product." + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + " AS product " + "} where  {product." + ProductModel.CATALOGVERSION + "}=?catalogversion  AND {product." + ProductModel.MODIFIEDTIME + "} >=?lastJobRun");
        fQuery.addQueryParameter("catalogversion", catalogVersion);
        fQuery.addQueryParameter("lastJobRun", lastModifiedDate);
        LOG.info("Last Job Run Time : " + lastModifiedDate);

        final SearchResult<ProductModel> result = getFlexibleSearchService().search(fQuery);

        LOG.info("tkEuVariantNameAssignmentJob :- Product Count :" + result.getResult().size());
        return result.getResult();
    }

    @Override
    public Date getLastVariantNameAssignmentJobRunDate() {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("select {pk} from {CronJobHistory} where {cronJobCode} = 'tkSchulteSite-TkEuVariantNameAssignmentJob' order by {startTime} desc");
        fQuery.setCount(2);
        final SearchResult<CronJobHistoryModel> result = getFlexibleSearchService().search(fQuery);
        if (result.getResult().size() > 1) {
            return result.getResult().get(1).getStartTime();
        }

        return new Date(0);
    }
}

package com.thyssenkrupp.b2b.eu.core.atp.event.listener;

import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandMetrics;
import com.netflix.hystrix.HystrixEventType;
import com.thyssenkrupp.b2b.eu.core.atp.event.FetchAtpMerticsEvent;
import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.apache.log4j.Logger;

import java.text.MessageFormat;

public class AtpMetricsEventListener extends AbstractEventListener<FetchAtpMerticsEvent> {

    private static final Logger LOG = Logger.getLogger(AtpMetricsEventListener.class);

    @Override
    protected void onEvent(FetchAtpMerticsEvent fetchAtpMerticsEvent) {

        HystrixCommandMetrics metrics;
        HystrixCommandKey commandKey = HystrixCommandKey.Factory.asKey(ThyssenkruppeucoreConstants.SAP_ATP_COMMAND_KEY);
        metrics = HystrixCommandMetrics.getInstance(commandKey);

        if (metrics != null) {
            StringBuilder message = new StringBuilder("{0} \nExecutionTime(mean): {1}");
            message.append("\nExecutionTime(99.0): {2}");
            message.append("\nCumulative(SUCCESS): {3}");
            message.append("\nCumulative(FAILURE): {4}");
            message.append("\nCumulative(SHORT_CIRCUITED): {5}");
            message.append("\nCumulative(FALLBACK_SUCCESS): {6}");
            message.append("\nCumulative(THREAD_POOL_REJECTED): {7}");
            message.append("\nRolling(SUCCESS): {8}");
            message.append("\nRolling(FAILURE): {9}");
            message.append("\nRolling(SHORT_CIRCUITED): {10}");
            message.append("\nRolling(FALLBACK_SUCCESS): {11}");
            message.append("\nRolling(THREAD_POOL_REJECTED): {12}");
            message.append("\nTotalRequests: {13}");
            message.append("\nErrorCount: {14}");

            Object[] params = new Object[] {
                metrics.getCommandKey().name(),
                metrics.getExecutionTimeMean(),
                metrics.getExecutionTimePercentile(99.0),
                metrics.getCumulativeCount(HystrixEventType.SUCCESS),
                metrics.getCumulativeCount(HystrixEventType.FAILURE),
                metrics.getCumulativeCount(HystrixEventType.SHORT_CIRCUITED),
                metrics.getCumulativeCount(HystrixEventType.FALLBACK_SUCCESS),
                metrics.getCumulativeCount(HystrixEventType.THREAD_POOL_REJECTED),
                metrics.getRollingCount(HystrixEventType.SUCCESS),
                metrics.getRollingCount(HystrixEventType.FAILURE),
                metrics.getRollingCount(HystrixEventType.SHORT_CIRCUITED),
                metrics.getRollingCount(HystrixEventType.FALLBACK_SUCCESS),
                metrics.getRollingCount(HystrixEventType.THREAD_POOL_REJECTED),
                metrics.getHealthCounts().getTotalRequests(),
                metrics.getHealthCounts().getErrorCount() };

            LOG.info(MessageFormat.format(message.toString(), params));
        }
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.sap.hybris.sapcustomerb2b.inbound.DefaultSAPB2BUnitService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bUnitService;

public class DefaultTkEuB2BUnitService extends DefaultSAPB2BUnitService implements TkEuB2bUnitService {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuB2BUnitService.class);

    @Override
    public Set<B2BUnitModel> getCurrentAndParentB2BUnits(final B2BCustomerModel b2bCustomer) {
        final Set<B2BUnitModel> b2bUnitSet = new HashSet<>();
        b2bUnitSet.add(super.getParent(b2bCustomer));
        b2bUnitSet.add(b2bCustomer.getDefaultB2BUnit());
        return b2bUnitSet;

    }

    @Override
    public B2BUnitModel getUnitForUid(final String uid) {
        B2BUnitModel unit;
        try {
            unit = getUserService().getUserGroupForUID(uid, B2BUnitModel.class);
        } catch (final UnknownIdentifierException | ClassMismatchException e) {
            unit = null;
            LOG.error("Failed to get unit: " + uid + ":" + e.getMessage());
        }
        return unit;
    }

}

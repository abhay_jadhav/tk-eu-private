package com.thyssenkrupp.b2b.eu.core.cronjob.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Optional;

public interface TkEuBaseProductGenerationJobHelper {
    Optional<CatalogVersionModel> getProductCatalogVersionFromBaseStore(BaseStoreModel baseStore);

    Collection<VariantProductModel> getAllVariantsForSapDummyProduct(CatalogVersionModel catalogVersion);

    void generateMasterProductsForVariants(Collection<VariantProductModel> variants);
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.sap.hybris.sapcustomerb2c.outbound.DefaultB2CSapCustomerAccountService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.PasswordPolicyService;
import de.hybris.platform.servicelayer.user.PasswordPolicyViolation;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.user.exceptions.PasswordEncoderNotFoundException;
import de.hybris.platform.servicelayer.user.exceptions.PasswordPolicyViolationException;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

public class TkEuCustomerAccountService extends DefaultB2CSapCustomerAccountService {

    private TkEuB2bCustomerService b2bCustomerService;
    private UserService            userService;
    private PasswordPolicyService  passwordPolicyService;

    @Override
    public void updatePassword(String token, String newPassword) throws TokenInvalidatedException {
        Assert.hasText(token, "The field [token] cannot be empty");
        Assert.hasText(newPassword, "The field [newPassword] cannot be empty");

        if (!isTokenExpired(token)) {
            final SecureToken data = getSecureTokenService().decryptData(token);
            final CustomerModel customer = (CustomerModel) getB2bCustomerService().getAllUserForUID(data.getData(), CustomerModel.class);
            if (customer == null) {
                throw new IllegalArgumentException("user for token not found");
            }
            if (!token.equals(customer.getToken())) {
                throw new TokenInvalidatedException();
            }
            customer.setToken(null);
            customer.setLoginDisabled(false);
            getModelService().save(customer);

            setPassword(data.getData(), newPassword, getPasswordEncoding());
        }
    }

    public void setPassword(String userId, String password, String encoding) throws PasswordEncoderNotFoundException {
        UserModel userModel = getB2bCustomerService().getAllUserForUID(userId, CustomerModel.class);
        assurePasswordCompliance(userModel, password, encoding);
        getUserService().setPassword(userModel, password, encoding);
        getModelService().save(userModel);
    }

    protected void assurePasswordCompliance(UserModel user, String plainPassword, String encoding) {
        List<PasswordPolicyViolation> policyViolations = getPasswordPolicyService().verifyPassword(user, plainPassword, encoding);
        if (!policyViolations.isEmpty()) {
            throw new PasswordPolicyViolationException(policyViolations);
        }
    }

    public boolean isTokenExpired(final String token) {
        final SecureToken data = fetchDecryptedTokenData(token);
        final long delta = new Date().getTime() - data.getTimeStamp();
        long forgotPasswordTokenValidityInSeconds = getConfigurationService().getConfiguration().getInt("forgot.password.token.validity.seconds", 1800);
        return (delta / 1000 > forgotPasswordTokenValidityInSeconds);
    }

    private SecureToken fetchDecryptedTokenData(final String token) {
        Assert.hasText(token, "The field [token] cannot be empty");
        return getSecureTokenService().decryptData(token);
    }

    public TkEuB2bCustomerService getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(TkEuB2bCustomerService b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public PasswordPolicyService getPasswordPolicyService() {
        return passwordPolicyService;
    }

    public void setPasswordPolicyService(PasswordPolicyService passwordPolicyService) {
        this.passwordPolicyService = passwordPolicyService;
    }
}

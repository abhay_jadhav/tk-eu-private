package com.thyssenkrupp.b2b.eu.core.exception;

public class TkEuInvalidCartException extends IllegalStateException {

    public TkEuInvalidCartException() {
    }

    public TkEuInvalidCartException(String s) {
        super(s);
    }

    public TkEuInvalidCartException(String message, Throwable cause) {
        super(message, cause);
    }

    public TkEuInvalidCartException(Throwable cause) {
        super(cause);
    }
}

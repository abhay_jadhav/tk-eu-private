package com.thyssenkrupp.b2b.eu.core.stock.service;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.StockService;

import java.util.List;

public interface TkEuStockService extends StockService {
    InStockStatus getInStockStatus(String productCode, List<WarehouseModel> warehouses);
}

package com.thyssenkrupp.b2b.eu.core.interceptor;


import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.log4j.Logger;

public class TkEuOrderStatusValidateInterceptor implements ValidateInterceptor {

    private static final Logger LOG = Logger.getLogger(TkEuOrderStatusValidateInterceptor.class);

    @Override
    public void onValidate(Object object, InterceptorContext interceptorContext) throws InterceptorException {

        if (object instanceof OrderModel) {
            OrderStatus orderStatus = ((OrderModel) object).getStatus();
            if (orderStatus == null) {
                LOG.error("Aborting order saving process - trying to save order with status null from Backoffice");
                throw new InterceptorException("Not allowed to change the value of the field : Order Status to N/A");
            }
        }

    }
}


package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.model.process.InStockStatusToMaxAvailabilityDaysMappingModel;
import com.thyssenkrupp.b2b.eu.core.service.InStockStatusToAvailabilityMappingService;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

import static com.thyssenkrupp.b2b.eu.core.model.process.InStockStatusToMaxAvailabilityDaysMappingModel.INSTOCKSTATUS;
import static java.util.Collections.singletonMap;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class DefaultInStockStatusToAvailabilityMappingService implements InStockStatusToAvailabilityMappingService {

    private DefaultGenericDao<InStockStatusToMaxAvailabilityDaysMappingModel> availabilityDaysMappingGenericDao;

    @Override
    public InStockStatusToMaxAvailabilityDaysMappingModel findByInStockStatus(@NotNull InStockStatus inStockStatus) {
        final Map<String, InStockStatus> params = singletonMap(INSTOCKSTATUS, inStockStatus);
        final List<InStockStatusToMaxAvailabilityDaysMappingModel> list = getAvailabilityDaysMappingGenericDao().find(params);
        return isNotEmpty(list) ? list.get(0) : null;
    }

    public DefaultGenericDao<InStockStatusToMaxAvailabilityDaysMappingModel> getAvailabilityDaysMappingGenericDao() {
        return availabilityDaysMappingGenericDao;
    }

    @Required
    public void setAvailabilityDaysMappingGenericDao(DefaultGenericDao<InStockStatusToMaxAvailabilityDaysMappingModel> availabilityDaysMappingGenericDao) {
        this.availabilityDaysMappingGenericDao = availabilityDaysMappingGenericDao;
    }
}

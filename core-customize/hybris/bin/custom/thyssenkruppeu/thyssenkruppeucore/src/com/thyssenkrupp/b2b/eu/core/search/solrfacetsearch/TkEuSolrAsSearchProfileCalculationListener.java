package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkEuCatalogVersionService;

import de.hybris.platform.adaptivesearch.context.AsSearchProfileContext;
import de.hybris.platform.adaptivesearch.data.AsSearchProfileResult;
import de.hybris.platform.adaptivesearchsolr.listeners.SolrAsSearchProfileCalculationListener;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;

public class TkEuSolrAsSearchProfileCalculationListener extends SolrAsSearchProfileCalculationListener {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuSolrAsSearchProfileCalculationListener.class);
    private TkEuCatalogVersionService catalogVersionService;
    private String productCatalogName;

    @Override
    public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException {
        if (facetSearchContext.getFacetSearchConfig().getSearchConfig().isLegacyMode()) {
            LOG.warn("Adaptive search does not support search legacy mode: " + facetSearchContext.getFacetSearchConfig().getName() + "/" + facetSearchContext.getIndexedType().getIdentifier());
        } else {
            final SearchQuery searchQuery = facetSearchContext.getSearchQuery();
            final String indexConfiguration = facetSearchContext.getFacetSearchConfig().getName();
            final String indexType = facetSearchContext.getIndexedType().getIdentifier();
            final List catalogVersions = getSolrAsCatalogVersionResolver().resolveCatalogVersions(searchQuery);
            addProductCatalogVersion(catalogVersions);
            searchQuery.setCatalogVersions(catalogVersions);
            final Object sessionCatalogVersions = facetSearchContext.getParentSessionCatalogVersions() == null ? Collections.emptyList() : new ArrayList(facetSearchContext.getParentSessionCatalogVersions());
            final List categoryPath = getSolrAsCategoryPathResolver().resolveCategoryPath(searchQuery, catalogVersions);
            final LanguageModel language = this.resolveLanguage(searchQuery);
            final CurrencyModel currency = this.resolveCurrency(searchQuery);
            final AsSearchProfileContext context = getAsSearchProfileContextFactory().createContext(indexConfiguration, indexType, catalogVersions, (List) sessionCatalogVersions, categoryPath, language, currency);
            if (StringUtils.isNotBlank(searchQuery.getUserQuery())) {
                context.setQuery(searchQuery.getUserQuery());
                context.setKeywords(this.convertKeywords(searchQuery.getKeywords()));
            }

            final List groups = getAsSearchProfileActivationService().getSearchProfileActivationGroupsForContext(context);
            if (CollectionUtils.isNotEmpty(groups)) {
                final AsSearchProfileResult searchQueryResult = this.createResultFromFacetSearchContext(context, facetSearchContext);
                final AsSearchProfileResult result = getAsSearchProfileCalculationService().calculateGroups(context, searchQueryResult, groups);
                final Map contextAttributes = facetSearchContext.getAttributes();
                contextAttributes.put("adaptiveSearchResult", result);
                contextAttributes.put("adaptiveSearchCatalogVersions", catalogVersions);
                contextAttributes.put("adaptiveSearchCategoryPath", categoryPath);
                this.applyResult(facetSearchContext, result);
            }
        }
    }

    private void addProductCatalogVersion(final List catalogVersions) {
        if (CollectionUtils.isNotEmpty(catalogVersions) && StringUtils.isNotBlank(productCatalogName)) {
            try {
                final CatalogVersionModel catalogVersionModel = (CatalogVersionModel) catalogVersions.iterator().next();
                final CatalogVersionModel productCatalog = catalogVersionService.getCatalogVersion(productCatalogName, catalogVersionModel.getVersion());
                if (!catalogVersions.contains(productCatalog)) {
                    catalogVersions.add(productCatalog);
                }
            } catch (final UnknownIdentifierException unknownIdentifierException) {
                LOG.error("Specified product catalog :: {0} not found", productCatalogName, unknownIdentifierException);
            } catch (final AmbiguousIdentifierException ambiguousIdentifierException) {
                LOG.error("{0} is not unique. Found multiple catalog entries.", productCatalogName, ambiguousIdentifierException);
            } catch (final IllegalStateException illegalStateException) {
                LOG.error("Exception thrown while setting product catalog :: {0} to store", productCatalogName, illegalStateException);
            }
        }
    }

    @Required
    public void setCatalogVersionService(final TkEuCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    @Required
    public void setProductCatalogName(final String productCatalogName) {
        this.productCatalogName = productCatalogName;
    }
}

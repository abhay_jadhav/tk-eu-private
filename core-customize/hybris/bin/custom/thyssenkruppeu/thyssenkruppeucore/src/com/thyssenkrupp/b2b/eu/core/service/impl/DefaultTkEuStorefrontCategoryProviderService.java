package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Collection;
import java.util.stream.Collectors;

public class DefaultTkEuStorefrontCategoryProviderService implements TkEuStorefrontCategoryProviderService {

    @Override
    public Collection<CategoryModel> filterStorefrontCategories(Collection<CategoryModel> categoryModels) {
        return categoryModels.stream().filter(categoryModel -> isStorefrontCategory(categoryModel)).collect(Collectors.toList());
    }

    @Override
    public boolean isStorefrontCategory(CategoryModel categoryModel) {
        return !(categoryModel instanceof VariantCategoryModel) && !(categoryModel instanceof VariantValueCategoryModel) && !(categoryModel instanceof ClassificationClassModel) && !(categoryModel instanceof ConfigurationCategoryModel);
    }

}

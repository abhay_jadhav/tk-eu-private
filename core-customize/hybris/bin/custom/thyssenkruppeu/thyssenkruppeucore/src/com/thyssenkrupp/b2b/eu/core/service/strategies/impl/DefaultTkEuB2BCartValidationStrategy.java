package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BCartValidationStrategy;
import de.hybris.platform.core.model.order.CartModel;

public class DefaultTkEuB2BCartValidationStrategy extends DefaultB2BCartValidationStrategy {

    @Override
    protected void validateDelivery(final CartModel cartModel) {
        final CheckoutPaymentType paymentType = cartModel.getPaymentType();

        if (CheckoutPaymentType.CARD.equals(paymentType)) {
            super.validateDelivery(cartModel);
        }
    }
}

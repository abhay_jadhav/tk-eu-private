package com.thyssenkrupp.b2b.eu.core.translator;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BUnitService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TkSetPrimaryAddressTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(com.thyssenkrupp.b2b.eu.core.translator.TkSetPrimaryAddressTranslator.class);

    private DefaultB2BUnitService b2bUnitService = (DefaultB2BUnitService) Registry.getApplicationContext().getBean("defaultB2BUnitService");

    private ModelService modelService = (ModelService) Registry.getApplicationContext().getBean("modelService");

    @Override
    public void performImport(String cellValue, Item processedItem) throws ImpExException {
        String childUnitId = cellValue;
        B2BUnitModel childUnit = b2bUnitService.getUnitForUid(childUnitId);
        B2BUnitModel parentUnit = b2bUnitService.getUnitForUid(getParentUnitId(childUnitId));
        Set<B2BCustomerModel> b2BCustomers = b2bUnitService.getB2BCustomers(parentUnit);
        Collection<AddressModel> addresses = childUnit.getAddresses();
        populateCustomerWithAddress(b2BCustomers, addresses);
    }

    private String getParentUnitId(String childUnitId) {
        return childUnitId.split("_")[0];
    }

    private void populateCustomerWithAddress(final Set<B2BCustomerModel> b2bCustomerModels, final Collection<AddressModel> addresses) {
        Optional<AddressModel> primaryShippingAddress = getPrimaryShippingAddress(addresses);
        Optional<AddressModel> primaryBillingAddress = getPrimaryBillingAddress(addresses);

        List<Optional<B2BCustomerModel>> b2bCustomersWithEmpty = null;
        if (CollectionUtils.isNotEmpty(b2bCustomerModels)
            && (primaryShippingAddress.isPresent() || primaryBillingAddress.isPresent())) {
            b2bCustomersWithEmpty = getFinalCustomers(b2bCustomerModels, primaryShippingAddress, primaryBillingAddress);
        }

        saveCustomers(b2bCustomersWithEmpty);
    }


    private void saveCustomers(final List<Optional<B2BCustomerModel>> b2bCustomersWithEmpty) { // NOSONAR

        List<B2BCustomerModel> b2bCustomers = null;
        // To remove optional<Empty> to save only the customers that have been modified
        if (b2bCustomersWithEmpty !=null && b2bCustomersWithEmpty.size()>0) {
            b2bCustomers = b2bCustomersWithEmpty.stream()
                .filter(Optional::isPresent)
                .map(i -> {
                    return i.get();
                })
                .collect(Collectors.toList()); // NOSONAR
        }

        if (CollectionUtils.isNotEmpty(b2bCustomers)) {
            modelService.saveAll(b2bCustomers);
        }
    }

    private List<Optional<B2BCustomerModel>> getFinalCustomers(final Set<B2BCustomerModel> b2bCustomerModels, final Optional<AddressModel> primaryShippingAddress, final Optional<AddressModel> primaryBillingAddress) {
        return b2bCustomerModels.stream().map(b2BCustomerModel -> {
            boolean isModified = false;
            if (primaryBillingAddress.isPresent() && b2BCustomerModel.getPrimaryBillingAddress() == null) {
                isModified = true;
                b2BCustomerModel.setPrimaryBillingAddress(primaryBillingAddress.get());
            }
            if (primaryShippingAddress.isPresent() && b2BCustomerModel.getPrimaryShippingAddress() == null) {
                isModified = true;
                b2BCustomerModel.setPrimaryShippingAddress(primaryShippingAddress.get());
            }
            if (isModified) {
                return Optional.of(b2BCustomerModel);
            }
            return Optional.<B2BCustomerModel>empty();
        }).collect(Collectors.toList());
    }

    private Optional<AddressModel> getPrimaryShippingAddress(Collection<AddressModel> addresses) {
        List<AddressModel> shippingAddresses = addresses.stream().filter(addressModel -> addressModel.getShippingAddress()).collect(Collectors.toList());
        return shippingAddresses.size() == 1 ? Optional.ofNullable((AddressModel) shippingAddresses.get(0)) : Optional.<AddressModel>empty();
    }

    private Optional<AddressModel> getPrimaryBillingAddress(Collection<AddressModel> addresses) {
        List<AddressModel> billingAddresses = addresses.stream().filter(addressModel -> addressModel.getBillingAddress()).collect(Collectors.toList());
        return billingAddresses.size() == 1 ? Optional.ofNullable((AddressModel) billingAddresses.get(0)) : Optional.<AddressModel>empty();
    }
}


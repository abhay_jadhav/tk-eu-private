package com.thyssenkrupp.b2b.eu.core.service;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;

/**
 *
 */
public interface TkEuLoginDetailsService {

    void setLoginDetails(String customerId, LoginType loginType);
}

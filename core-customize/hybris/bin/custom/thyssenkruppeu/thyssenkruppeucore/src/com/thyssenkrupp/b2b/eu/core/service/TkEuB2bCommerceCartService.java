package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2bacceleratorservices.order.B2BCommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public interface TkEuB2bCommerceCartService extends B2BCommerceCartService {

    CommerceCartModification updateAtpInformation(CommerceCartParameter parameter);

    CommerceCartModification updateAtpConsolidatedDate(CommerceCartParameter parameter);

    CommerceCartModification updateCertificateForCartEntry(CommerceCartParameter parameter);
}

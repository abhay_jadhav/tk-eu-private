package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bOrderService;
import com.thyssenkrupp.b2b.eu.core.service.order.daos.TkEuB2bOrderDao;

public class DefaultTkEuB2bOrderService implements TkEuB2bOrderService {
    private TkEuB2bOrderDao tkEuB2bOrderDao;

    @Override
    public List<OrderModel> getOrdersToExport(final int exportOrderCount, final Long gracetime) {
        return getTkEuB2bOrderDao().getOrdersToExport(exportOrderCount, gracetime);
    }

    public TkEuB2bOrderDao getTkEuB2bOrderDao() {
        return tkEuB2bOrderDao;
    }

    public void setTkEuB2bOrderDao(final TkEuB2bOrderDao tkEuB2bOrderDao) {
        this.tkEuB2bOrderDao = tkEuB2bOrderDao;
    }

    @Override
    public OrderModel getInitialOrderForCode(final String orderCode) {
        return getTkEuB2bOrderDao().findInitialOrderByCode(orderCode);
    }

}

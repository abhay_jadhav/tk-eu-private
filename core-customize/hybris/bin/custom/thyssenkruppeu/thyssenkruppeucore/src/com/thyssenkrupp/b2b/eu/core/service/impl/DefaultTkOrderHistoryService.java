package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.impl.DefaultOrderHistoryService;

public class DefaultTkOrderHistoryService extends DefaultOrderHistoryService {

    @Override
    public OrderModel createHistorySnapshot(final OrderModel currentVersion) {
        final OrderModel snapshot = super.createHistorySnapshot(currentVersion);
        currentVersion.setInitialOrderVersion(false);
        getModelService().save(currentVersion);
        return snapshot;
    }
}


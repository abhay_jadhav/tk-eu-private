/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.order.dao.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

import com.thyssenkrupp.b2b.eu.core.order.dao.TkConsignmentDao;

/**
 * @author vagrant
 *
 */
public class DefaultTkConsignmentDao extends AbstractItemDao implements TkConsignmentDao {

    @Override
    public ConsignmentModel findConsignment(final String consignmentId) {
        final StringBuilder query = new StringBuilder("SELECT {cons." + ConsignmentModel.PK + "} ");
        ConsignmentModel consignment = null;
        query.append("FROM {" + ConsignmentModel._TYPECODE + " AS cons} ");
        query.append("WHERE {cons." + ConsignmentModel.CODE + "} = ?" + ConsignmentModel.CODE);
        final Map<String, Object> params = new HashMap<String, Object>(1);
        params.put(ConsignmentModel.CODE, consignmentId);
        final SearchResult<ConsignmentModel> searchRes = search(query.toString(), params);

        if (!searchRes.getResult().isEmpty()) {
            consignment = searchRes.getResult().get(0);
        }
        return consignment;
    }

}

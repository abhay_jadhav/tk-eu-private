package com.thyssenkrupp.b2b.eu.core.pojo;


public class DocumentRequest {

    private String documentNo;
    private String positionNo;
    private String batchNo;
    private String lineItemNo;
    private String documenType;

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getPositionNo() {
        return positionNo;
    }

    public void setPositionNo(String positionNo) {
        this.positionNo = positionNo;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getLineItemNo() {
        return lineItemNo;
    }

    public void setLineItemNo(String lineItemNo) {
        this.lineItemNo = lineItemNo;
    }

    public String getDocumenType() {
        return documenType;
    }

    public void setDocumenType(String documenType) {
        this.documenType = documenType;
    }
}

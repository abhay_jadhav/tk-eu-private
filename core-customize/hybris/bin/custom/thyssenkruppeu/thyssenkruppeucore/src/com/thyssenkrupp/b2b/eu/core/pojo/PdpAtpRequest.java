package com.thyssenkrupp.b2b.eu.core.pojo;

public class PdpAtpRequest {
    private String productCode;
    private String qty;
    private String unit;
    private String totalWeight;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(final String qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(final String totalWeight) {
        this.totalWeight = totalWeight;
    }

}

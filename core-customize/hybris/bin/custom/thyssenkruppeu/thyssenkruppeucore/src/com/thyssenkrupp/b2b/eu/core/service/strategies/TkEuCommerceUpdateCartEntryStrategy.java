package com.thyssenkrupp.b2b.eu.core.service.strategies;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public interface TkEuCommerceUpdateCartEntryStrategy extends CommerceUpdateCartEntryStrategy {

    CommerceCartModification updateAtpInformationForCartEntry(CommerceCartParameter parameters);

    CommerceCartModification updateAtpConsolidatedDate(CommerceCartParameter parameters);

    CommerceCartModification updateCertificateForCartEntry(CommerceCartParameter parameter);
}

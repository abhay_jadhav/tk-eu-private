package com.thyssenkrupp.b2b.eu.core.cache;

import de.hybris.platform.regioncache.key.CacheKey;

import java.util.Optional;

public interface TkEuSAPAtpCacheService {

    Optional<Object> get(CacheKey key);

    void put(CacheKey key, Object content);

    boolean useCache();

    CacheKey getKey(String key, String storeId);
}

package com.thyssenkrupp.b2b.eu.core.service;

import com.thyssenkrupp.b2b.eu.core.model.process.InStockStatusToMaxAvailabilityDaysMappingModel;
import de.hybris.platform.basecommerce.enums.InStockStatus;

public interface InStockStatusToAvailabilityMappingService {

    InStockStatusToMaxAvailabilityDaysMappingModel findByInStockStatus(InStockStatus inStockStatus);
}

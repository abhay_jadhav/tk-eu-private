package com.thyssenkrupp.b2b.eu.core.atp.service.strategies;

import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequest;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.AtpRequestItem;
import com.thyssenkrupp.b2b.eu.core.atp.exchanges.request.SapAtpRequest;
import org.apache.log4j.Logger;

import java.util.List;

import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class SapAtpRequestValidationStrategy {

    private static final Logger LOG = Logger.getLogger(SapAtpRequestValidationStrategy.class);

    public boolean validateSapAtpRequest(final SapAtpRequest request) {
        if (nonNull(request) && nonNull(request.getAtpRequest()) && isNotEmpty(request.getAtpRequest().getAtpRequestItems())) {
            return validateRequest(request);
        }
        LOG.error("Found NULL or Empty SapAtpRequest, skipping SapAtp call");
        return false;
    }

    private boolean validateRequest(final SapAtpRequest sapAtpRequest) {
        final AtpRequest request = sapAtpRequest.getAtpRequest();
        if (isEmpty(request.getRegionBusinessDivisionEnvironment()) || isEmpty(request.getCustomerShipToNumber()) || isEmpty(request.getShipToCountryCode())
            || isEmpty(request.getShipToPostalCode()) || isEmpty(request.getDivision()) || isEmpty(request.getSalesOrganization()) || isEmpty(request.getShippingConditions())
            || isEmpty(request.getDistributionChannel())) {
            LOG.error("Found Invalid SapAtpRequest, skipping SapAtp call. Request:" + request.toString());
            return false;
        }
        return true;
    }

    private boolean validateRequestItems(final SapAtpRequest sapAtpRequest) {
        final List<AtpRequestItem> requestItemList = sapAtpRequest.getAtpRequest().getAtpRequestItems();
        for (AtpRequestItem requestItem : requestItemList) {
            if (isEmpty(requestItem.getLineItemNumber()) || isEmpty(requestItem.getMaterialNumber()) || isEmpty(requestItem.getQuantity()) || isEmpty(requestItem.getUnitOfMeasure())) {
                LOG.error("Found Invalid SapAtpRequest Item, skipping SapAtp call. Request:" + requestItem.toString());
                return false;
            }
        }
        return true;
    }
}

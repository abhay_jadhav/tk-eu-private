package com.thyssenkrupp.b2b.eu.core.cache.impl;

import com.thyssenkrupp.b2b.eu.core.cache.TkEuSAPDocumentExistsCacheService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Optional;

public class DefaultTkEuSAPDocumentExistsCacheService implements TkEuSAPDocumentExistsCacheService {

    private static final String CACHE_ENABLED_KEY = "thyssenkruppeucore.documentexists.useCache.enabled";

    private ConfigurationService configurationService;

    private CacheController cacheController;

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public CacheController getCacheController() {
        return cacheController;
    }

    public void setCacheController(CacheController cacheController) {
        this.cacheController = cacheController;
    }

    @Override
    public Optional<Object> get(CacheKey key) {
        return Optional.ofNullable(getCacheController().get(key));
    }

    @Override
    public void put(CacheKey key, Object content) {
        getCacheController().getWithLoader(key, cacheKey -> content);
    }

    @Override
    public boolean useCache() {
        return getConfigurationService().getConfiguration().getBoolean(CACHE_ENABLED_KEY);
    }

    @Override
    public CacheKey getKey(String key, String storeId) {
        return new TkEuSAPDocumentExistsCacheKey(key, storeId, Registry.getCurrentTenant().getTenantID());
    }
}

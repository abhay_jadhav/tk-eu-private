package com.thyssenkrupp.b2b.eu.core.exception;

import org.springframework.security.core.AuthenticationException;

public class TkEuUserActivationPendingException extends AuthenticationException {

    public TkEuUserActivationPendingException(String msg) {
        super(msg);
    }

    public TkEuUserActivationPendingException(String msg, Throwable t) {
        super(msg, t);
    }
}

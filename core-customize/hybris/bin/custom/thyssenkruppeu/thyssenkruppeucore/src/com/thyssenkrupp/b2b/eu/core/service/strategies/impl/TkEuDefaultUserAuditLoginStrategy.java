package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import com.google.common.base.Preconditions;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.impl.DefaultUserAuditLoginStrategy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class TkEuDefaultUserAuditLoginStrategy extends DefaultUserAuditLoginStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(TkEuDefaultUserAuditLoginStrategy.class);
    private TkEuB2bCustomerService b2bCustomerService;

    public void auditUserOnCorrectCredentials(String uid) {
        Preconditions.checkNotNull(uid, "Uid must not be null");
        final String userId = uid.contains("|") ? StringUtils.substringAfter(uid, "|") : uid;
        this.loadAttempts(userId).ifPresent((attempts) -> {
            attempts.setAttempts(Integer.valueOf(0));
            this.modelService.save(attempts);
        });
    }

    public void auditUserOnWrongCredentials(final String uid) {
        super.auditUserOnWrongCredentials(uid);
        populateLoginDisableTime(uid);
    }

    private void populateLoginDisableTime(final String uid) {
        try {
            final UserModel userModel = b2bCustomerService.getAllUserForUID(uid, CustomerModel.class);
            if (userModel.isLoginDisabled()) {
                userModel.setLoginDisabledTime(new Date());
                modelService.save(userModel);
            }
        } catch (UnknownIdentifierException exception) {
            LOG.warn("Cannot find user with uid '" + uid + "'");
        }
    }

    public TkEuB2bCustomerService getB2bCustomerService() {
        return b2bCustomerService;
    }

    public void setB2bCustomerService(TkEuB2bCustomerService b2bCustomerService) {
        this.b2bCustomerService = b2bCustomerService;
    }
}

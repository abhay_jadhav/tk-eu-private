package com.thyssenkrupp.b2b.eu.core.cronjob.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TK_EU_PRODUCT_CATALOG_ID;
import static com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus.TK_010_READY_FOR_ATTRIBUTE_CONSOLIDATION;
import static com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil.splitByCommaCharacter;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.cronjob.service.TkEuBaseProductGenerationJobHelper;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;

public class DefaultTkEuBaseProductGenerationJobHelper implements TkEuBaseProductGenerationJobHelper {

    private static final Logger LOG                                = Logger.getLogger(DefaultTkEuBaseProductGenerationJobHelper.class);
    private static final String SAP_DUMMY_MASTER_PRODUCT_CODE      = "sap_dummy_master_product";
    private static final String PRODUCTID_SPECIALCHARS_REGEX       = "productid.specialchars.regex";
    private static final String PRODUCTID_SPECIALCHARS_REPLACEMENT = "productid.specialchars.regex.replacement";

    private TkB2bCategoryService                        categoryService;
    private ProductService                              productService;
    private TkEuB2bProductService                       b2bProductService;
    private CatalogService                              catalogService;
    private ClassificationService                       classificationService;
    private TkB2bCatalogProductReplicationConfigService productReplicationConfigService;
    private ConfigurationService                        configurationService;

    @Override
    public Optional<CatalogVersionModel> getProductCatalogVersionFromBaseStore(final BaseStoreModel baseStore) {
        final SAPConfigurationModel sapConfiguration = baseStore.getSAPConfiguration();
        if (nonNull(sapConfiguration) && isNotEmpty(sapConfiguration.getSapcommon_salesOrganization())) {
            return ofNullable(getCategoryService().getCatalogVersionBySalesOrganization(sapConfiguration.getSapcommon_salesOrganization()));
        }
        return empty();
    }

    @Override
    public Collection<VariantProductModel> getAllVariantsForSapDummyProduct(final CatalogVersionModel catalogVersion) {
        final ProductModel sapDummyProduct = getProductService().getProductForCode(catalogVersion, SAP_DUMMY_MASTER_PRODUCT_CODE);
        return sapDummyProduct.getVariants();
    }

    @Override
    public void generateMasterProductsForVariants(final Collection<VariantProductModel> variants) {
        final Map<String, String> allIdGenerationRules = getAllIdGenerationRulesForProductCatalog();
        for (final VariantProductModel variant : variants) {
            LOG.debug("Started generating master product for variant:" + variant.getCode());
            generateMasterProductsForVariant(variant, allIdGenerationRules);
            LOG.debug("Finished generating master product for variant:" + variant.getCode());
        }
    }

    private void generateMasterProductsForVariant(final VariantProductModel variant, final Map<String, String> allIdGenerationRules) {
        final CategoryModel classCategory = getB2bProductService().getClassificationCategoryForProduct(variant);
        if (nonNull(classCategory) && isNotEmpty(classCategory.getCode())) {
            final Optional<ProductModel> maybeMasterProduct = getOrCreateMasterProductForVariant(variant, classCategory, allIdGenerationRules);
            if (maybeMasterProduct.isPresent()) {
                final ProductModel masterProduct = maybeMasterProduct.get();
                LOG.debug("Setting baseProduct as:" + masterProduct.getCode() + " to variant:" + variant.getCode());
                getB2bProductService().updateBaseProductForVariant(variant, masterProduct);
            }
        } else {
            LOG.warn("No ClassificationCategory found for Variant:" + variant.getCode());
        }
    }

    private Optional<ProductModel> getOrCreateMasterProductForVariant(final VariantProductModel variant, final CategoryModel classCategory, final Map<String, String> allIdGenerationRules) {
        final Optional<Pair<String, String>> maybeIdGenerationRule = getIdGenerationRulesForRuleCode(classCategory.getCode(), allIdGenerationRules);
        if (maybeIdGenerationRule.isPresent()) {
            final Optional<String> maybeMasterProductId = buildMasterProductIdForVariant(variant, maybeIdGenerationRule.get());
            if (maybeMasterProductId.isPresent()) {
                final String masterProductId = maybeMasterProductId.get();
                LOG.debug(format("Constructed MasterProductId for Variant:%s is %s", variant.getCode(), masterProductId));
                return ofNullable(getOrCreateMasterProduct(variant.getCatalogVersion(), classCategory, masterProductId));
            }
        }
        return empty();
    }

    private ProductModel getOrCreateMasterProduct(final CatalogVersionModel catalogVersion, final CategoryModel classCategory, final String productId) {
        final Optional<ProductModel> maybeProduct = getProductForCode(catalogVersion, productId);
        if (maybeProduct.isPresent()) {
            LOG.debug("MasterProduct already exist for productCode:" + productId);
            final ProductModel product = maybeProduct.get();
            updateLifeCycleToReadyForAttributeConsolidation(product);
            updateVariantTypeIfNotPresent(product);
            return product;
        } else {
            LOG.info("Creating New MasterProduct with id:" + productId);
            final ProductModel product = getB2bProductService().createBaseProductWithCode(productId, catalogVersion);
            product.setSupercategories(Collections.singletonList(classCategory));
            getB2bProductService().updateLifeCycleStatus(product, TK_010_READY_FOR_ATTRIBUTE_CONSOLIDATION);
            return product;
        }
    }

    private Optional<ProductModel> getProductForCode(final CatalogVersionModel catalogVersion, final String productId) {
        ProductModel product = null;
        try {
            product = getProductService().getProductForCode(catalogVersion, productId);
        } catch (final Exception e) {
            LOG.warn("No product found for code:" + productId + ", Ignoring Exception");
        }
        return ofNullable(product);
    }

    private Optional<String> buildMasterProductIdForVariant(final VariantProductModel variant, final Pair<String, String> rule) {
        final FeatureList featureList = getClassificationService().getFeatures(variant);
        final String ruleAttributes = rule.getValue();
        if (nonNull(featureList) && isNotEmpty(ruleAttributes)) {
            final String ruleCode = rule.getKey();
            final String[] attributes = splitByCommaCharacter(ruleAttributes);
            return buildMasterProductIdByAttributes(attributes, ruleCode, featureList);
        }
        return empty();
    }

    private Optional<String> buildMasterProductIdByAttributes(final String[] attributes, final String ruleCode, final FeatureList featureList) {
        if (ArrayUtils.isNotEmpty(attributes)) {
            final StringBuilder masterProductId = new StringBuilder(ruleCode);
            for (final String attribute : attributes) {
                final Feature feature = getFeatureByClassCodeAndAttribute(featureList, ruleCode, attribute);
                final Optional<String> maybeFeatureValueCode = getFeatureValueCode(feature);
                maybeFeatureValueCode.ifPresent(s -> masterProductId.append(truncateAttributeCodeFromFeatureValueCode(attribute, s)));
            }
            return of(masterProductId.toString().replaceAll(configurationService.getConfiguration().getString(PRODUCTID_SPECIALCHARS_REGEX),
                configurationService.getConfiguration().getString(PRODUCTID_SPECIALCHARS_REPLACEMENT)));
        }
        return empty();
    }

    private Optional<String> getFeatureValueCode(final Feature feature) {
        String resultingValue = null;
        if (nonNull(feature) && nonNull(feature.getValue())) {
            final FeatureValue featureValue = feature.getValue();
            final Object value = featureValue.getValue();
            if (ClassificationAttributeValueModel.class.isInstance(value)) {
                final ClassificationAttributeValueModel classificationAttributeValue = ClassificationAttributeValueModel.class.cast(value);
                LOG.debug("Using ClassificationAttributeValue:" + classificationAttributeValue.getCode() + " to construct masterProductId:");
                resultingValue = classificationAttributeValue.getCode();
            } else if (nonNull(value)) {
                resultingValue = value.toString();
            }
        }
        return ofNullable(resultingValue);
    }

    private String truncateAttributeCodeFromFeatureValueCode(final String attributeCode, final String featureValueCode) {
        return featureValueCode.replaceFirst(attributeCode + "_", StringUtils.EMPTY);
    }

    private Optional<Pair<String, String>> getIdGenerationRulesForRuleCode(final String ruleCode, final Map<String, String> allIdGenerationRules) {
        return (isNotEmpty(allIdGenerationRules) && allIdGenerationRules.containsKey(ruleCode)) ? of(Pair.of(ruleCode, allIdGenerationRules.get(ruleCode))) : empty();
    }

    private Map<String, String> getAllIdGenerationRulesForProductCatalog() {
        final CatalogModel productCatalog = getCatalogService().getCatalogForId(TK_EU_PRODUCT_CATALOG_ID);
        final Optional<Map<String, String>> maybeIdGenerationRules = getProductReplicationConfigService().getAllIdGenerationRules(productCatalog);
        if (maybeIdGenerationRules.isPresent()) {
            return maybeIdGenerationRules.get();
        } else {
            LOG.error("No IdGenerationRules found for ProductCatalog");
            throw new UnknownIdentifierException("No IdGenerationRules found for ProductCatalog");
        }
    }

    private Feature getFeatureByClassCodeAndAttribute(final FeatureList featureList, final String classCode, final String attribute) {
        return featureList.getFeatureByCode("ERP_CLASSIFICATION_001/ERP_IMPORT/" + classCode + "." + attribute.toLowerCase());
    }

    private void updateVariantTypeIfNotPresent(final ProductModel product) {
        if (isNull(product.getVariantType())) {
            getB2bProductService().setVariantTypeAsERPVariantProduct(product);
        }
    }

    private void updateLifeCycleToReadyForAttributeConsolidation(final ProductModel product) {
        if (isNull(product.getLifeCycleStatus()) || TK_010_READY_FOR_ATTRIBUTE_CONSOLIDATION != product.getLifeCycleStatus()) {
            getB2bProductService().updateLifeCycleStatus(product, TK_010_READY_FOR_ATTRIBUTE_CONSOLIDATION);
        }
    }

    public TkB2bCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(final TkB2bCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    public TkEuB2bProductService getB2bProductService() {
        return b2bProductService;
    }

    @Required
    public void setB2bProductService(final TkEuB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }

    public TkB2bCatalogProductReplicationConfigService getProductReplicationConfigService() {
        return productReplicationConfigService;
    }

    @Required
    public void setProductReplicationConfigService(final TkB2bCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }

    @Required
    public void setCatalogService(final CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    public ClassificationService getClassificationService() {
        return classificationService;
    }

    @Required
    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}



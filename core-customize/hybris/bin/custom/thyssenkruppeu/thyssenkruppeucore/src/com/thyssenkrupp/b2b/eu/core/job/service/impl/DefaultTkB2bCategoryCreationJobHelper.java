package com.thyssenkrupp.b2b.eu.core.job.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.PRODUCT_REPLICATION_RULE_FALLBACK_KEY;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TK_EU_PRODUCT_CATALOG_ID;
import static de.hybris.platform.catalog.jalo.CatalogManager.OFFLINE_VERSION;
import static java.text.MessageFormat.format;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.job.service.TkB2bCategoryCreationJobHelper;
import com.thyssenkrupp.b2b.eu.core.pojo.VariantCategoryCreationParameter;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bProductService;
import com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguratorSettingsModel;

public class DefaultTkB2bCategoryCreationJobHelper implements TkB2bCategoryCreationJobHelper {

    private static final Logger                                      LOG = Logger.getLogger(DefaultTkB2bCategoryCreationJobHelper.class);
    private              TkB2bCatalogProductReplicationConfigService productReplicationConfigService;
    private              ClassificationSystemService                 classificationSystemService;
    private              TkB2bCategoryService                        categoryService;
    private              CatalogService                              catalogService;
    private              TkB2bProductService                         productService;
    private              CatalogVersionService                       catalogVersionService;
    private              TkB2bConfiguratorSettingsService            configuratorSettingsService;

    @Override
    public Map<String, String> getAllVariantGenerationRulesForProductCatalog() {
        final CatalogModel productCatalog = getCatalogService().getCatalogForId(TK_EU_PRODUCT_CATALOG_ID);
        final Optional<Map<String, String>> maybeVariantGenerationRules = getProductReplicationConfigService().getAllVariantGenerationRules(productCatalog);
        if (maybeVariantGenerationRules.isPresent()) {
            return maybeVariantGenerationRules.get();
        } else {
            throw new UnknownIdentifierException("No VariantGenerationRules found for ProductCatalog");
        }
    }

    @Override
    public void setVariantCategoriesToBaseProduct(final ProductModel product, final List<VariantCategoryModel> variantCategories) {
        if (isNotEmpty(variantCategories) && getProductService().isBaseProduct(product)) {
            getProductService().replaceExistingVariantCategoriesForProduct(product, variantCategories);
        }
    }

    @Override
    public Optional<List<VariantCategoryModel>> createVariantCategoriesForClass(final String ruleCode, final ClassificationClassModel classCategory, final CatalogVersionModel catalogVersion) {
        final Optional<Pair<String, String>> maybeVariantGenerationRule = getProductReplicationConfigService().getVariantGenerationRule(catalogVersion.getCatalog(), ruleCode);
        if (maybeVariantGenerationRule.isPresent()) {
            final Pair<String, String> rule = maybeVariantGenerationRule.get();
            LOG.info("Started creating VariantCategories for ruleCode:" + ruleCode);
            final List<VariantCategoryModel> variantCategoriesForClass = generateVariantCategoriesForClass(rule.getKey(), rule.getValue(), classCategory, catalogVersion);
            if (isNotEmpty(variantCategoriesForClass)) {
                LOG.info("Started creating VariantCategory Hierarchy for ruleCode:" + ruleCode);
                getCategoryService().buildVariantCategoryHierarchy(variantCategoriesForClass);
                LOG.info("Finished creating " + variantCategoriesForClass.size() + " VariantCategories and Hierarchy for ruleCode:" + ruleCode);
                return Optional.of(variantCategoriesForClass);
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<VariantCategoryModel> getVariantCategoryByCode(final String classCode, final String attributeCode, final List<VariantCategoryModel> variantCategories) {
        Optional<VariantCategoryModel> maybeVariantCategory = getVariantCategory(classCode, attributeCode, variantCategories);
        if (!maybeVariantCategory.isPresent()) {
            maybeVariantCategory = getVariantCategory(PRODUCT_REPLICATION_RULE_FALLBACK_KEY, attributeCode, variantCategories);
        }
        return maybeVariantCategory;
    }

    @Override
    public void cleanUpVariantCategories(final Map<String, List<VariantCategoryModel>> variantCategoriesMap) {
        final CatalogVersionModel productCatalog = getCatalogVersionService().getCatalogVersion(TK_EU_PRODUCT_CATALOG_ID, OFFLINE_VERSION);
        for (final Map.Entry<String, List<VariantCategoryModel>> entry : variantCategoriesMap.entrySet()) {
            final String ruleCode = entry.getKey();
            final List<VariantCategoryModel> variantCategories = entry.getValue();
            removeStaledVariantValueCategoriesByRuleCode(ruleCode, variantCategories, productCatalog);
            removeStaledVariantCategoriesByRuleCode(ruleCode, variantCategories, productCatalog);
        }
        removeStaledVariantCategoriesAndVariantValueCategories(productCatalog);
    }

    @Override
    public List<VariantValueCategoryModel> buildVariantCategoryValuesForConfigurableLength(final VariantProductModel variant, final VariantCategoryModel variantCategoryModel) {
        final List<VariantValueCategoryModel> resultingVvcList = new ArrayList<>();
        final Optional<TkLengthConfiguratorSettingsModel> maybeSetting = getConfiguratorSettingsService().getFirstTkLengthConfiguratorSetting(variant);
        if (maybeSetting.isPresent()) {
            final TkLengthConfiguratorSettingsModel setting = maybeSetting.get();
            if (Objects.isNull(setting.getLengthValue())) {
                LOG.warn("TkLengthConfiguratorSettings.LengthValue is null , Skipping creation of VariantValueCategoryModel");
            } else {
                resultingVvcList.add(getCategoryService().createVariantValueCategoryForConfigurableLength(setting, variantCategoryModel, variant.getCatalogVersion()));
            }
        }
        return resultingVvcList;
    }

    private void removeStaledVariantCategoriesByRuleCode(final String ruleCode, final List<VariantCategoryModel> variantCategories, final CatalogVersionModel productCatalog) {
        final Collection<VariantCategoryModel> allVariantCategories = getCategoryService().getAllVariantCategoriesWithCode(ruleCode, productCatalog);
        final Collection<CategoryModel> staledVariantCategories = CollectionUtils.subtract(allVariantCategories, variantCategories);
        LOG.info(format("Started cleanup {0} VariantCategories for ruleCode:{1}", staledVariantCategories.size(), ruleCode));
        if (isNotEmpty(staledVariantCategories)) {
            displayStaledCategories(staledVariantCategories, "VariantCategory");
            final List<CategoryModel> staledSubCategories = new ArrayList<>();
            for (final CategoryModel vc : staledVariantCategories) {
                final Collection<CategoryModel> allSubVariantCategories = vc.getCategories();
                if (isNotEmpty(allSubVariantCategories)) {
                    getCategoryService().setSubCategoriesAsEmpty(Collections.singletonList(vc));
                    staledSubCategories.addAll(allSubVariantCategories.stream().filter(isVariantCategoryOrVariantValueCategory()).collect(Collectors.toList()));
                }
            }
            displayStaledCategories(staledSubCategories, "Sub VariantCategory");
            getCategoryService().removeCategories(staledSubCategories);
            getCategoryService().removeCategories(staledVariantCategories);
        }
        LOG.info(format("Finished cleanup {0} VariantCategories  for ruleCode:{1}", staledVariantCategories.size(), ruleCode));
    }

    private void removeStaledVariantValueCategoriesByRuleCode(final String ruleCode, final List<VariantCategoryModel> variantCategories, final CatalogVersionModel productCatalog) {
        final Collection<VariantValueCategoryModel> allVariantValueCategories = getCategoryService().getAllVariantValueCategoriesWithCode(ruleCode, productCatalog);
        final Collection<CategoryModel> allSubVariantValueCategoriesForCategory = getAllSubVariantValueCategories(variantCategories.get(0));
        final Collection<CategoryModel> staledVariantValueCategories = CollectionUtils.subtract(allVariantValueCategories, allSubVariantValueCategoriesForCategory);
        LOG.info(format("Started cleanup {0} VariantValueCategories for ruleCode:{1}", staledVariantValueCategories.size(), ruleCode));
        if (isNotEmpty(staledVariantValueCategories)) {
            displayStaledCategories(staledVariantValueCategories, "VariantValueCategory");
            getCategoryService().removeCategories(staledVariantValueCategories);
        }
        LOG.info(format("Finished cleanup {0} VariantValueCategories  for ruleCode:{1}", staledVariantValueCategories.size(), ruleCode));
    }

    private void removeStaledVariantCategoriesAndVariantValueCategories(final CatalogVersionModel productCatalog) {
        final Collection<CategoryModel> vvc = getCategoryService().getAllCategoriesWithoutIncludedProducts("VVC_", productCatalog);
        LOG.info(format("Started cleanup of {0} VariantValueCategories without IncludedProducts", vvc.size()));
        displayStaledCategories(vvc, "VariantValueCategory");
        getCategoryService().removeCategories(vvc);
        LOG.info(format("Finished cleanup of {0} VariantValueCategories without IncludedProducts", vvc.size()));
        final Collection<CategoryModel> vc = getCategoryService().getAllCategoriesWithoutIncludedProducts("VC_", productCatalog);
        LOG.info(format("Started cleanup of {0} VariantCategories without IncludedProducts", vc.size()));
        displayStaledCategories(vc, "VariantCategory");
        getCategoryService().setSubCategoriesAsEmpty(vc);
        getCategoryService().removeCategories(vc);
        LOG.info(format("Finished cleanup of {0} VariantCategories without IncludedProducts", vc.size()));
    }

    private Collection<CategoryModel> getAllSubVariantValueCategories(final CategoryModel category) {
        if (nonNull(category)) {
            final Collection<CategoryModel> allSubcategoriesForCategory = getCategoryService().getAllSubcategoriesForCategory(category);
            return emptyIfNull(allSubcategoriesForCategory).stream().filter(VariantValueCategoryModel.class::isInstance).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private Optional<VariantCategoryModel> getVariantCategory(final String classCode, final String attributeCode, final List<VariantCategoryModel> variantCategories) {
        final String categoryCode = getCategoryService().buildVariantCategoryCode(classCode, attributeCode);
        return emptyIfNull(variantCategories).stream().filter(o -> categoryCode.equals(o.getCode())).findFirst();
    }

    private List<VariantCategoryModel> generateVariantCategoriesForClass(final String classCode, final String attributesString, final ClassificationClassModel classCategory, final CatalogVersionModel prodCatalogVersion) {
        final List<VariantCategoryModel> resultingList = new LinkedList<>();
        if (StringUtils.isNotEmpty(classCode) && StringUtils.isNotEmpty(attributesString)) {
            final String[] attributes = TkCommonUtil.splitByCommaCharacter(attributesString);
            for (final String attribute : attributes) {
                resultingList.add(getOrCreateVariantCategory(classCode, attribute, classCategory, prodCatalogVersion));
            }
        }
        return resultingList;
    }

    private VariantCategoryModel getOrCreateVariantCategory(final String classCode, final String attribute, final ClassificationClassModel classCategory, final CatalogVersionModel prodCatalogVersion) {
        final VariantCategoryCreationParameter parameter = new VariantCategoryCreationParameter();
        parameter.setClassCode(classCode);
        parameter.setAttributeCode(attribute.trim());
        parameter.setCatalogVersionModel(prodCatalogVersion);
        parameter.setClassificationClass(classCategory);
        if (PRODUCT_REPLICATION_RULE_FALLBACK_KEY.equals(classCode)) {
            parameter.setFallBackClassCode(true);
        }
        return getCategoryService().getOrCreateVariantCategory(parameter);
    }

    private void displayStaledCategories(final Collection<CategoryModel> categories, final String message) {
        if (LOG.isDebugEnabled()) {
            for (final CategoryModel category : categories) {
                LOG.debug("Staled " + message + ":" + category.getCode());
            }
        }
    }

    private Predicate<CategoryModel> isVariantCategoryOrVariantValueCategory() {
        return categoryModel -> (VariantCategoryModel.class.isInstance(categoryModel) || VariantValueCategoryModel.class.isInstance(categoryModel));
    }

    public TkB2bCatalogProductReplicationConfigService getProductReplicationConfigService() {
        return productReplicationConfigService;
    }

    @Required
    public void setProductReplicationConfigService(final TkB2bCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }

    public ClassificationSystemService getClassificationSystemService() {
        return classificationSystemService;
    }

    @Required
    public void setClassificationSystemService(final ClassificationSystemService classificationSystemService) {
        this.classificationSystemService = classificationSystemService;
    }

    public TkB2bCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(final TkB2bCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }

    @Required
    public void setCatalogService(final CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    public TkB2bProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final TkB2bProductService productService) {
        this.productService = productService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public TkB2bConfiguratorSettingsService getConfiguratorSettingsService() {
        return configuratorSettingsService;
    }

    @Required
    public void setConfiguratorSettingsService(final TkB2bConfiguratorSettingsService configuratorSettingsService) {
        this.configuratorSettingsService = configuratorSettingsService;
    }
}

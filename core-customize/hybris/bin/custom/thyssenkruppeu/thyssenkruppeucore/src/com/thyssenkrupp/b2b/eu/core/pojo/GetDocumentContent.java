package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class GetDocumentContent implements Serializable {
    @JsonProperty("Document_Category")
    private String documentCategory;

    @JsonProperty("Customer_Number")
    private String customerNumber;

    @JsonProperty("HybrisRegion_BusinessDivision_Environment")
    private String hybrisRegionBusinessDivisionEnvironment;

    @JsonProperty("Is_Certs_Are_Paid")
    private String isCertsArePaid;

    @JsonProperty("Is_Content_Anonymized")
    private String isContentAnonymized;

    @JsonProperty("Division")
    private String division;

    @JsonProperty("Sales_Organization")
    private String salesOrganization;

    @JsonProperty("Distribution_Channel")
    private String distributionChannel;

    @JsonProperty("Item_Details")
    private List<ItemDetails> itemDetails;

    @JsonProperty("Document_Category")
    public String getDocumentCategory() {
        return documentCategory;
    }

    @JsonProperty("Document_Category")
    public void setDocumentCategory(String documentCategory) {
        this.documentCategory = documentCategory;
    }

    @JsonProperty("Customer_Number")
    public String getCustomerNumber() {
        return customerNumber;
    }

    @JsonProperty("Customer_Number")
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @JsonProperty("HybrisRegion_BusinessDivision_Environment")
    public String getHybrisRegionBusinessDivisionEnvironment() {
        return hybrisRegionBusinessDivisionEnvironment;
    }

    @JsonProperty("HybrisRegion_BusinessDivision_Environment")
    public void setHybrisRegionBusinessDivisionEnvironment(String hybrisRegionBusinessDivisionEnvironment) {
        this.hybrisRegionBusinessDivisionEnvironment = hybrisRegionBusinessDivisionEnvironment;
    }

    @JsonProperty("Is_Certs_Are_Paid")
    public String getIsCertsArePaid() {
        return isCertsArePaid;
    }

    @JsonProperty("Is_Certs_Are_Paid")
    public void setIsCertsArePaid(String isCertsArePaid) {
        this.isCertsArePaid = isCertsArePaid;
    }

    @JsonProperty("Is_Content_Anonymized")
    public String getIsContentAnonymized() {
        return isContentAnonymized;
    }

    @JsonProperty("Is_Content_Anonymized")
    public void setIsContentAnonymized(String isContentAnonymized) {
        this.isContentAnonymized = isContentAnonymized;
    }

    @JsonProperty("Division")
    public String getDivision() {
        return division;
    }

    @JsonProperty("Division")
    public void setDivision(String division) {
        this.division = division;
    }

    @JsonProperty("Sales_Organization")
    public String getSalesOrganization() {
        return salesOrganization;
    }

    @JsonProperty("Sales_Organization")
    public void setSalesOrganization(String salesOrganization) {
        this.salesOrganization = salesOrganization;
    }

    @JsonProperty("Distribution_Channel")
    public String getDistributionChannel() {
        return distributionChannel;
    }

    @JsonProperty("Distribution_Channel")
    public void setDistributionChannel(String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    @JsonProperty("Item_Details")
    public List<ItemDetails> getItemDetails() {
        return itemDetails;
    }

    @JsonProperty("Item_Details")
    public void setItemDetails(List<ItemDetails> itemDetails) {
        this.itemDetails = itemDetails;
    }

    @Override
    public String toString() {
        return "ClassPojo [Document_Category = " + documentCategory + ", Customer_Number = " + customerNumber + ", HybrisRegion_BusinessDivision_Environment = " + hybrisRegionBusinessDivisionEnvironment + ", Is_Certs_Are_Paid = " + isCertsArePaid + ", Is_Content_Anonymized = " + isContentAnonymized + ", Division = " + division + ", Sales_Organization = " + salesOrganization + ", Distribution_Channel = " + distributionChannel + ", Item_Details = " + itemDetails + "]";
    }
}


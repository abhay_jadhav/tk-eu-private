package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import com.thyssenkrupp.b2b.eu.core.service.TkEuStorefrontCategoryProviderService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.CategorySource;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryNameValueProvider;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class TkEuCategoryNameValueProvider extends CategoryNameValueProvider {

    private CategorySource                        categorySource;
    private FieldNameProvider                     fieldNameProvider;
    private CommonI18NService                     commonI18NService;
    private TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService;

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {

        final Collection<FieldValue> fieldValues = new ArrayList();
        final Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model);
        if (CollectionUtils.isNotEmpty(categories)) {
            final Collection<CategoryModel> storefrontCategories = getTkStorefrontCategoryProviderService().filterStorefrontCategories(categories);
            if (indexedProperty.isLocalized()) {
                createLocalizedFieldValue(indexConfig, indexedProperty, storefrontCategories, fieldValues);
            } else {
                createNonLocalizedFieldValue(storefrontCategories, indexedProperty, fieldValues);
            }
        }
        return fieldValues;
    }

    private void createNonLocalizedFieldValue(Collection<CategoryModel> categories, IndexedProperty indexedProperty, Collection<FieldValue> fieldValues) {
        if (CollectionUtils.isNotEmpty(categories)) {
            for (final CategoryModel category : categories) {
                fieldValues.addAll(createFieldValue(category, null, indexedProperty));
            }
        }
    }

    private void createLocalizedFieldValue(IndexConfig indexConfig, IndexedProperty indexedProperty, Collection<CategoryModel> categories, Collection<FieldValue> fieldValues) {
        if (CollectionUtils.isNotEmpty(categories)) {
            final Collection<LanguageModel> languages = indexConfig.getLanguages();
            for (final LanguageModel language : languages) {
                for (final CategoryModel category : categories) {
                    fieldValues.addAll(createFieldValue(category, language, indexedProperty));
                }
            }
        }
    }

    protected List<FieldValue> createFieldValue(final CategoryModel category, final LanguageModel language, final IndexedProperty indexedProperty) {

        final List<FieldValue> fieldValues = new ArrayList();
        if (language == null) {
            final Object value = getPropertyValue(category);
            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
            for (final String fieldName : fieldNames) {
                fieldValues.add(new FieldValue(fieldName, value));
            }
        } else {
            Object value = null;
            try {
                i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
                value = getPropertyValue(category);
            } finally {
                final Locale locale = i18nService.getCurrentLocale();
                i18nService.setCurrentLocale(locale);
            }
            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
            for (final String fieldName : fieldNames) {
                fieldValues.add(new FieldValue(fieldName, value));
            }
        }
        return fieldValues;
    }

    @Override
    public CategorySource getCategorySource() {
        return categorySource;
    }

    @Override
    public void setCategorySource(CategorySource categorySource) {
        this.categorySource = categorySource;
    }

    @Override
    public FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Override
    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    @Override
    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Override
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public TkEuStorefrontCategoryProviderService getTkStorefrontCategoryProviderService() {
        return tkStorefrontCategoryProviderService;
    }

    public void setTkStorefrontCategoryProviderService(TkEuStorefrontCategoryProviderService tkStorefrontCategoryProviderService) {
        this.tkStorefrontCategoryProviderService = tkStorefrontCategoryProviderService;
    }
}

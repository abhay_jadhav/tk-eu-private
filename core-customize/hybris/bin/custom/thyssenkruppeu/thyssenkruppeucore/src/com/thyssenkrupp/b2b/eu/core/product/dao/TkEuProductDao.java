
package com.thyssenkrupp.b2b.eu.core.product.dao;

import java.util.Date;
import java.util.List;

import com.thyssenkrupp.b2b.eu.core.dao.TkProductDao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

public interface TkEuProductDao extends TkProductDao {
    List<ProductModel> findProductsByStatusAndCatalogVersion(CatalogVersionModel catalogVersion);

    List<ProductModel> findProductsByCatalogVersionModifiedSince(CatalogVersionModel catalogVersion, Date lastModifiedDate);

    Date getLastVariantNameAssignmentJobRunDate();
}

package com.thyssenkrupp.b2b.eu.core.job;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.PRODUCT_REPLICATION_RULE_FALLBACK_KEY;
import static com.thyssenkrupp.b2b.eu.core.utils.TkCommonUtil.splitByCommaCharacter;
import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.dao.TkProductDao;
import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;
import com.thyssenkrupp.b2b.eu.core.job.service.TkB2bCategoryCreationJobHelper;
import com.thyssenkrupp.b2b.eu.core.model.TkB2bCategoryCreationCronJobModel;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bProductService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;

public class TkB2bCategoryCreationJob extends AbstractJobPerformable<TkB2bCategoryCreationCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkB2bCategoryCreationJob.class);

    private ConfigurationService configurationService;
    private ClassificationService classificationService;
    private UnitService unitService;
    private TkProductDao productDao;
    private TkEuB2bCategoryDao tkCategoryDao;

    private TkB2bProductService tkB2bProductService;
    private TkB2bCategoryService tkB2bCategoryService;

    private TkB2bCatalogProductReplicationConfigService productReplicationConfigService;

    private String                           rangeConfigurationUnit;
    private TkB2bCategoryCreationJobHelper   categoryCreationJobHelper;

    @Override
    public PerformResult perform(final TkB2bCategoryCreationCronJobModel job) {
        try {
            LOG.info("Started Category Creation CronJob");
            final CMSSiteModel cmsSiteModel = job.getContentSite();
            final List<BaseStoreModel> stores = cmsSiteModel.getStores();
            final BaseStoreModel currentBaseStore = stores != null ? stores.get(0) : null;
            if (currentBaseStore != null && currentBaseStore.getSAPConfiguration() != null) {
                final String salesOrganization = currentBaseStore.getSAPConfiguration().getSapcommon_salesOrganization();
                LOG.info("Fetching Product Catalog Version for salesOrganization: " + salesOrganization);
                final CatalogVersionModel catalogVersion = tkCategoryDao.fetchProductCatalogBySalesOrganization(salesOrganization);
                if (catalogVersion != null) {
                    LOG.info("Fetching products for " + catalogVersion.getCatalog().getName());
                    final List<ProductModel> productModels = productDao.findProductsByCatalogAndLifeCycleStatus(catalogVersion,ProductLifeCycleStatus.TK_030_READY_FOR_CATEGORY_CREATION);
                    createCategoriesForProduct(productModels);
                } else {
                    LOG.warn("CatalogVersion is Null-Categories cannot be created");
                }
            } else {
                LOG.warn("BaseStore is Null-Categories cannot be created");
            }
            LOG.info("Finished Category Creation CronJob");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while Creating Categories", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void createCategoriesForProduct(final List<ProductModel> productModels) {

        final Map<String, String> variantGenerationRules = getCategoryCreationJobHelper().getAllVariantGenerationRulesForProductCatalog();
        if (MapUtils.isNotEmpty(variantGenerationRules)) {
            //map with key as ruleCode and value as list of VC's for that ruleCode
            final Map<String, List<VariantCategoryModel>> variantCategoriesMap = new HashMap<>();
            for (final ProductModel product : productModels) {
                buildCategoriesForProduct(product, variantGenerationRules, variantCategoriesMap);
            }
            if (MapUtils.isNotEmpty(variantCategoriesMap)) {
                getCategoryCreationJobHelper().cleanUpVariantCategories(variantCategoriesMap);
            }
        }
    }

    private void buildCategoriesForProduct(final ProductModel product, final Map<String, String> variantGenerationRules, final Map<String, List<VariantCategoryModel>> variantCategoriesMap) {
        final CategoryModel classCategory = tkB2bProductService.getClassificationCategoryForProduct(product);
        if (Objects.nonNull(classCategory) && StringUtils.isNotEmpty(classCategory.getCode())) {
            final String ruleCode = getRuleCodeBasedOnVariantGenerationRule(classCategory.getCode(), variantGenerationRules);
            // create VC for ruleCode
            createVariantCategoriesForClassCode(ruleCode, (ClassificationClassModel) classCategory, variantCategoriesMap, product.getCatalogVersion());
            // create VVC for variants
            createVariantValueCategoriesForProduct(product, classCategory, variantCategoriesMap.get(ruleCode), variantGenerationRules.get(ruleCode));
            //update baseProduct.superCategories  with VC list
            getCategoryCreationJobHelper().setVariantCategoriesToBaseProduct(product, variantCategoriesMap.get(ruleCode));
        } else {
            LOG.warn("No Valid ClassificationClass found for product" + product.getCode() + ". Skipping entry");
        }
        getTkB2bProductService().setCategoryCreationStatusToTrue(product);
        product.setLifeCycleStatus(ProductLifeCycleStatus.TK_040_READY_FOR_PRODUCTINFO_GENERATION);
        modelService.save(product);
        modelService.refresh(product);
    }

    private void createVariantCategoriesForClassCode(final String ruleCode, final ClassificationClassModel classCategory, final Map<String, List<VariantCategoryModel>> variantCategoryMap, final CatalogVersionModel catalogVersion) {
        if (StringUtils.isNotEmpty(ruleCode) && !variantCategoryMap.containsKey(ruleCode)) {
            final Optional<List<VariantCategoryModel>> maybeVariantCategories = getCategoryCreationJobHelper().createVariantCategoriesForClass(ruleCode, classCategory, catalogVersion);
            maybeVariantCategories.ifPresent(variantCategoryModels -> variantCategoryMap.put(ruleCode, variantCategoryModels));
        }
    }

    private String getRuleCodeBasedOnVariantGenerationRule(final String classCode, final Map<String, String> variantGenerationRulesMap) {
        if (variantGenerationRulesMap.containsKey(classCode) || variantGenerationRulesMap.containsKey(PRODUCT_REPLICATION_RULE_FALLBACK_KEY)) {
            return variantGenerationRulesMap.containsKey(classCode) ? classCode : PRODUCT_REPLICATION_RULE_FALLBACK_KEY;
        }
        return StringUtils.EMPTY;
    }

    private void createVariantValueCategoriesForProduct(final ProductModel product, final CategoryModel classificationCategory, final List<VariantCategoryModel> variantCategories, final String rulesForClass) {
        final Collection<VariantProductModel> variants = product.getVariants();
        if (StringUtils.isNotEmpty(rulesForClass) && CollectionUtils.isNotEmpty(variants)) {
            final String[] attributeList = splitByCommaCharacter(rulesForClass);
            LOG.info("Started creating VariantValueCategories for BaseProduct:" + product.getCode());
            for (final VariantProductModel variant : variants) {
                final Map<VariantCategoryModel, List<VariantValueCategoryModel>> vvcMap = createVariantValueCategoriesForVariant(variant, classificationCategory, attributeList, variantCategories);
                if (MapUtils.isNotEmpty(vvcMap)) {
                    final List<VariantValueCategoryModel> vvcForVariant = emptyIfNull(vvcMap.values()).stream().filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList());
                    LOG.debug("Adding " + vvcForVariant.size() + " VVC as SubCategories to Variant:" + variant.getCode());
                    tkB2bProductService.updateVariantProduct(variant, vvcForVariant);
                }
            }
            LOG.info("Finished creating VariantValueCategories for BaseProduct:" + product.getCode());
        }
    }

    private Map<VariantCategoryModel, List<VariantValueCategoryModel>> createVariantValueCategoriesForVariant(final VariantProductModel variant, final CategoryModel classCategory, final String[] attributes, final List<VariantCategoryModel> variantCategories) {
        final Map<VariantCategoryModel, List<VariantValueCategoryModel>> resultingMap = new HashMap<>();
        final FeatureList variantFeatures = classificationService.getFeatures(variant);
        LOG.debug("Started creating VariantValueCategories for Variant:" + variant.getCode());
        for (final String attribute : attributes) {
            final Feature feature = getFeatureByAttribute(variantFeatures, classCategory, attribute);
            final Optional<VariantCategoryModel> maybeVariantCategory = getCategoryCreationJobHelper().getVariantCategoryByCode(classCategory.getCode(), attribute, variantCategories);
            if (maybeVariantCategory.isPresent()) {
                final List<VariantValueCategoryModel> vvcForAttribute = buildVariantCategoryValues(variant, feature, maybeVariantCategory.get());
                if (CollectionUtils.isNotEmpty(vvcForAttribute)) {
                    resultingMap.put(maybeVariantCategory.get(), vvcForAttribute);
                }
            } else {
                LOG.warn("No VariantCategory found for attributeCode:" + attribute + " , skipping generation of VariantValueCategory");
            }
        }
        LOG.debug("Finished creating VariantValueCategories for variant:" + variant.getCode());
        return resultingMap;
    }

    private List<VariantValueCategoryModel> buildVariantCategoryValues(final VariantProductModel variant, final Feature feature, final VariantCategoryModel variantCategoryModel) {
        String featureValue = null;
        final List<VariantValueCategoryModel> valueCategoryList = new ArrayList<>();
        if (isLengthVariantCategoryWithoutFeatureValue(variantCategoryModel, feature)) {
            return getCategoryCreationJobHelper().buildVariantCategoryValuesForConfigurableLength(variant, variantCategoryModel);
        } else if (getTkB2bCategoryService().isTradeLengthVariantCategory(variantCategoryModel.getCode())) {
            return buildVariantCategoryValuesForTradeLength(variant, variantCategoryModel);
        } else if (feature != null && StringUtils.isNotEmpty(tkB2bProductService.getFeatureValue(feature.getValue()))) {
            featureValue = tkB2bProductService.getFeatureValue(feature.getValue());
            LOG.debug(" Attribute Value for " + feature.getCode() + " :" + featureValue);
            valueCategoryList.add(tkB2bCategoryService.createVariantValueCategory(feature, variantCategoryModel, variant.getCatalogVersion()));
        }
        if (StringUtils.isEmpty(featureValue)) {
            LOG.debug(" No variant value category found for " + variantCategoryModel.getCode());
            return Collections.emptyList();
        }
        return valueCategoryList;
    }

    private boolean isLengthVariantCategoryWithoutFeatureValue(final VariantCategoryModel variantCategoryModel, final Feature feature) {
        return getTkB2bCategoryService().isLengthVariantCategory(variantCategoryModel.getCode()) && (Objects.isNull(feature) || Objects.isNull(feature.getValue()));
    }

    private List<VariantValueCategoryModel> buildVariantCategoryValuesForTradeLength(final VariantProductModel variant, final VariantCategoryModel variantCategoryModel) {
        final List<VariantValueCategoryModel> tradeLengthValueCategoryList = new ArrayList();
        final List<TkTradeLengthConfiguratorSettingsModel> tradeLengthSettings = tkB2bCategoryService.getTradeLengthSettingConfigurations(variant);

        if (CollectionUtils.isNotEmpty(tradeLengthSettings)) {
            for (final TkTradeLengthConfiguratorSettingsModel setting : tradeLengthSettings) {
                if (setting == null || setting.getUnit() == null || StringUtils.isEmpty(setting.getDisplayValue())) {
                    LOG.warn(" Invalid TradeLength Settings. VVC will not be created ");
                    continue;
                }
                tradeLengthValueCategoryList.add(tkB2bCategoryService.createVariantValueCategoryForTradeLength(setting, variantCategoryModel, variant.getCatalogVersion()));
            }
        }
        return tradeLengthValueCategoryList;
    }

    private Feature getFeatureByAttribute(final FeatureList featureList, final CategoryModel classificationCategory, final String attribute) {
        return featureList.getFeatureByCode("ERP_CLASSIFICATION_001/ERP_IMPORT/" + classificationCategory.getCode() + "." + attribute.toLowerCase());
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    protected ClassificationService getClassificationService() {
        return classificationService;
    }

    @Required
    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    protected TkProductDao getProductDao() {
        return productDao;
    }

    @Required
    public void setProductDao(final TkProductDao productDao) {
        this.productDao = productDao;
    }

    public TkEuB2bCategoryDao getTkCategoryDao() {
        return tkCategoryDao;
    }

    @Required
    public void setTkCategoryDao(final TkEuB2bCategoryDao tkCategoryDao) {
        this.tkCategoryDao = tkCategoryDao;
    }

    public TkB2bProductService getTkB2bProductService() {
        return tkB2bProductService;
    }

    public void setTkB2bProductService(final TkB2bProductService tkB2bProductService) {
        this.tkB2bProductService = tkB2bProductService;
    }

    public TkB2bCategoryService getTkB2bCategoryService() {
        return tkB2bCategoryService;
    }

    public void setTkB2bCategoryService(final TkB2bCategoryService tkB2bCategoryService) {
        this.tkB2bCategoryService = tkB2bCategoryService;
    }

    public TkB2bCatalogProductReplicationConfigService getProductReplicationConfigService() {
        return productReplicationConfigService;
    }

    @Required
    public void setProductReplicationConfigService(final TkB2bCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }

    public String getRangeConfigurationUnit() {
        return rangeConfigurationUnit;
    }

    @Required
    public void setRangeConfigurationUnit(final String rangeConfigurationUnit) {
        this.rangeConfigurationUnit = rangeConfigurationUnit;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    public TkB2bCategoryCreationJobHelper getCategoryCreationJobHelper() {
        return categoryCreationJobHelper;
    }

    @Required
    public void setCategoryCreationJobHelper(final TkB2bCategoryCreationJobHelper categoryCreationJobHelper) {
        this.categoryCreationJobHelper = categoryCreationJobHelper;
    }
}

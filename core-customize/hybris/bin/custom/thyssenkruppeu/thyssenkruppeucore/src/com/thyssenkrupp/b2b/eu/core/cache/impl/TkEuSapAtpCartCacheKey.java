package com.thyssenkrupp.b2b.eu.core.cache.impl;

import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;

public class TkEuSapAtpCartCacheKey implements CacheKey {

    private static final String ATP_CART_EXISTS_CACHE_UNIT_CODE = "__ATP_CART_EXISTS_CACHE__";
    private final        String key;
    private final        String tenantId;
    private final        String storeId;

    public TkEuSapAtpCartCacheKey(String key, String storeId, String tenantId) {
        this.key = key;
        this.storeId = storeId;
        this.tenantId = tenantId;
    }

    @Override
    public CacheUnitValueType getCacheValueType() {
        return CacheUnitValueType.SERIALIZABLE;
    }

    @Override
    public Object getTypeCode() {
        return ATP_CART_EXISTS_CACHE_UNIT_CODE;
    }

    @Override
    public String getTenantId() {
        return tenantId;
    }

    @Override
    public boolean equals(Object cacheObject) {
        // START OF GENERATED CODE
        if (this == cacheObject) {
            return true;
        }
        if (cacheObject == null || getClass() != cacheObject.getClass()) {
            return false;
        }
        TkEuSapAtpCartCacheKey that = (TkEuSapAtpCartCacheKey) cacheObject;
        if (!key.equals(that.key)) {
            return false;
        }
        if (!tenantId.equals(that.tenantId)) {
            return false;
        }
        // END OF GENERATED CODE
        return storeId.equals(that.storeId);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + tenantId.hashCode();
        result = 31 * result + storeId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("TkEuSapAtpCartCacheKey{key='%s', storeId='%s'}", key, storeId);
    }
}

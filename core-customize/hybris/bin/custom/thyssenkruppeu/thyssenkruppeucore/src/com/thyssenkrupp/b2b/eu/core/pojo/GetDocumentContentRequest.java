package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentContentRequest implements Serializable {

    @JsonProperty("Get_Document_Content")
    private GetDocumentContent getDocumentContent;

    @JsonProperty("Get_Document_Content")
    public GetDocumentContent getGetDocumentContent() {
        return getDocumentContent;
    }

    @JsonProperty("Get_Document_Content")
    public void setGetDocumentContent(GetDocumentContent getDocumentContent) {
        this.getDocumentContent = getDocumentContent;
    }

    @Override
    public String toString() {
        return "GetDocumentContentRequest.getDocumentContent = " + getDocumentContent;
    }
}

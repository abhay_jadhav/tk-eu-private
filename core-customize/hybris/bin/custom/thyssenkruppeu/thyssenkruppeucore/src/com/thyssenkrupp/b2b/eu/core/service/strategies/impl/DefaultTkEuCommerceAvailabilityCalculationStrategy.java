package com.thyssenkrupp.b2b.eu.core.service.strategies.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commerceservices.stock.strategies.impl.DefaultCommerceAvailabilityCalculationStrategy;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.Collection;

public class DefaultTkEuCommerceAvailabilityCalculationStrategy extends DefaultCommerceAvailabilityCalculationStrategy {

    @Override
    public Long calculateAvailability(final Collection<StockLevelModel> stockLevels) {
        long totalActualAmount = 0;
        for (final StockLevelModel stockLevel : stockLevels) {
            if (!isNotAvailable(stockLevel)) {
                return null;
            }
        }
        return Long.valueOf(totalActualAmount);
    }

    protected boolean isNotAvailable(StockLevelModel stockLevel) {
        return stockLevel == null || InStockStatus.FORCEOUTOFSTOCK.equals(stockLevel.getInStockStatus()) || InStockStatus.NOTSPECIFIED.equals(stockLevel.getInStockStatus()) || InStockStatus.NOTAVAILABLE.equals(stockLevel.getInStockStatus());
    }
}

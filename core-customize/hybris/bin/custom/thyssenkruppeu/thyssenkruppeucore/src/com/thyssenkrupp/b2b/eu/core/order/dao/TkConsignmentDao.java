/**
 *
 */
package com.thyssenkrupp.b2b.eu.core.order.dao;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

/**
 * @author vagrant
 *
 */
public interface TkConsignmentDao extends Dao {
    ConsignmentModel findConsignment(String consignmentId);

}

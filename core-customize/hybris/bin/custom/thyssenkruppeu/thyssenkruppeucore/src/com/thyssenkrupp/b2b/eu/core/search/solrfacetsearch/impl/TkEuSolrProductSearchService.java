package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;

import org.springframework.core.convert.converter.Converter;

public class TkEuSolrProductSearchService<ITEM> extends TkSolrProductSearchService {

    private Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> productSearchQueryPageableConverter;

    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> productCodeSearch(final SolrSearchQueryData solrSearchQueryData, final PageableData pageableData) {
        return doProductCodeSearch(solrSearchQueryData, pageableData);
    }

    private ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> doProductCodeSearch(final SolrSearchQueryData solrSearchQueryData, final PageableData pageableData) {
        validateParameterNotNull(solrSearchQueryData, "SearchQueryData cannot be null");

        final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = buildSearchQueryPageableData(solrSearchQueryData, pageableData);

        final SolrSearchRequest solrSearchRequest = getProductSearchQueryPageableConverter().convert(searchQueryPageableData);

        final SolrSearchResponse solrSearchResponse = getSearchRequestConverter().convert(solrSearchRequest);
        return getSearchResponseConverter().convert(solrSearchResponse);
    }

    @Override
    protected Converter<SolrSearchRequest, SolrSearchResponse> getSearchRequestConverter() {
        return super.getSearchRequestConverter();
    }

    @Override
    protected Converter<SolrSearchResponse, ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel>> getSearchResponseConverter() {
        return super.getSearchResponseConverter();
    }

    public Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> getProductSearchQueryPageableConverter() {
        return productSearchQueryPageableConverter;
    }

    public void setProductSearchQueryPageableConverter(final Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> productSearchQueryPageableConverter) {
        this.productSearchQueryPageableConverter = productSearchQueryPageableConverter;
    }
}

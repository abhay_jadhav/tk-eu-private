package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.sap.sapmodel.model.SAPPricingSalesAreaToCatalogModel;

import java.util.Set;

public interface TkEuSapGlobalConfigurationService {

    Set<SAPPricingSalesAreaToCatalogModel> getSapPricingSalesAreaToCatalogModels();

    @Deprecated
    SAPPricingSalesAreaToCatalogModel getCurrentSapPricingSalesAreaToCatalogModel();
}

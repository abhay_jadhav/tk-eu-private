package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TkEuCategorySuggestValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private static final Logger    LOG            = Logger.getLogger(TkEuCategorySuggestValueProvider.class);
    private static final Character PATH_SEPARATOR = '/';
    private String                 categorySeoUriPattern;
    private Long                   categorySuggestLevels;
    private FieldNameProvider      fieldNameProvider;
    private CommonI18NService      commonI18NService;
    private DefaultCategoryService categoryService;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        final ProductModel productModel = (ProductModel) model;
        final Collection<CategoryModel> categoryPathForProduct = getCategoryService().getCategoryPathForProduct(productModel, CategoryModel.class);
        return CollectionUtils.isEmpty(categoryPathForProduct) ? Collections.emptyList() : getFieldValues(indexConfig, indexedProperty, categoryPathForProduct);
    }

    private Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Collection<CategoryModel> categoryPathForProduct) {
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (indexedProperty.isLocalized()) {
            final Collection<LanguageModel> languages = indexConfig.getLanguages();
            languages.forEach(language -> fieldValues.addAll(createFieldValue(categoryPathForProduct, indexedProperty, language)));
        } else {
            fieldValues.addAll(createFieldValue(categoryPathForProduct, indexedProperty, null));
        }
        return fieldValues;
    }

    private List<FieldValue> createFieldValue(final Collection<CategoryModel> categoryPath, final IndexedProperty indexedProperty, final LanguageModel language) {
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (language != null) {
            buildFieldValuesForLocale(categoryPath, indexedProperty, language, fieldValues);
        } else {
            buildFieldValuesForNonLocale(categoryPath, indexedProperty, fieldValues);
        }
        return fieldValues;
    }

    private void buildFieldValuesForLocale(final Collection<CategoryModel> categoryPath, final IndexedProperty indexedProperty, final LanguageModel language, final List<FieldValue> fieldValues) {
        final Set<String> categorySuggests = findCategorySuggests(categoryPath, language);
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
        buildFieldValues(fieldValues, categorySuggests, fieldNames);
    }

    private void buildFieldValuesForNonLocale(final Collection<CategoryModel> categoryPath, IndexedProperty indexedProperty, List<FieldValue> fieldValues) {
        final Set<String> categorySuggests = findCategorySuggests(categoryPath, null);
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
        buildFieldValues(fieldValues, categorySuggests, fieldNames);
    }

    private void buildFieldValues(List<FieldValue> fieldValues, Set<String> categorySuggests, Collection<String> fieldNames) {
        fieldNames.forEach(fieldName -> categorySuggests.forEach(categorySuggest -> fieldValues.add(new FieldValue(fieldName, categorySuggest))));
    }

    private Set<String> findCategorySuggests(final Collection<CategoryModel> categoryPath, final LanguageModel language) {
        final List<CategoryModel> categoryPathForSuggests = categoryPath.stream().limit(getCategorySuggestLevels()).collect(Collectors.toList());
        final Set<String> categorySuggests = new HashSet<>();
        categoryPathForSuggests.forEach(categoryPathForSuggest -> {
            //Filter for rootCategory
            if (!CollectionUtils.isEmpty(categoryPathForSuggest.getSupercategories())) {
                buildCategorySuggest(categoryPathForSuggest, language, categorySuggests);
            }
        });
        return categorySuggests;
    }

    private void buildCategorySuggest(final CategoryModel category, final LanguageModel language, final Set<String> categorySuggests) {
        final String code = category.getCode();
        final String categoryName = getCategoryNameForLocale(category, language);
        LOG.debug("category :" + code + "categoryName :" + categoryName);
        if (StringUtils.isNotBlank(categoryName)) {
            final Collection<CategoryModel> superCategories = category.getAllSupercategories();
            final String categorySuggest = getCategorySuggest(categoryName, buildPathForLocale(superCategories, categoryName, language), code);
            LOG.debug("CategorySuggest :" + categorySuggest + "for the categoryId : " + code);
            categorySuggests.add(categorySuggest);
        }
    }

    private String getCategorySuggest(final String categoryName, final String categoryPath, final String code) {
        return String.format(getCategorySeoUriPattern(), categoryName, categoryPath, code);
    }

    private String buildPathForLocale(final Collection<CategoryModel> superCategories, final String categoryName, final LanguageModel language) {
        final StringBuilder result = new StringBuilder();
        final List<CategoryModel> orderedSuperCategories = getOrderedSuperCategories(superCategories);
        orderedSuperCategories.forEach(categoryModel -> result.append(PATH_SEPARATOR).append(getCategoryNameForLocale(categoryModel, language)));
        result.append(PATH_SEPARATOR).append(categoryName);
        LOG.debug("categoryPath : " + result.toString() + " for locale :" + language.getIsocode());
        return result.toString();
    }

    //NOSONAR //ToDo: Refactor for generics
    private List<CategoryModel> getOrderedSuperCategories(final Collection<CategoryModel> superCategories) {
        try {
            final List<CategoryModel> orderedSuperCategories = new ArrayList<>(superCategories.size());
            superCategories.forEach(superCategory -> {
                if (superCategory.getAllSupercategories().size() == 0) {
                    orderedSuperCategories.add(0, superCategory);
                } else {
                    orderedSuperCategories.add(superCategory);
                }
            });
            return orderedSuperCategories;
        } catch (IndexOutOfBoundsException exception) {
            LOG.error("Exception processing at getOrderedSuperCategories ", exception);
        }
        return new ArrayList<>(superCategories);
    }

    private String getCategoryNameForLocale(final CategoryModel category, LanguageModel language) {
        final Locale locale = i18nService.getCurrentLocale();
        try {
            i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
            final Optional<String> categoryName = Optional.ofNullable(getCategoryName(category));
            return categoryName.orElseGet(() -> category.getName(locale));
        } finally {
            i18nService.setCurrentLocale(locale);
        }
    }

    private String getCategoryName(final CategoryModel categoryModel) {
        return modelService.getAttributeValue(categoryModel, "name");
    }

    protected String getCategorySeoUriPattern() {
        return categorySeoUriPattern;
    }

    public void setCategorySeoUriPattern(String categorySeoUriPattern) {
        this.categorySeoUriPattern = categorySeoUriPattern;
    }

    protected Long getCategorySuggestLevels() {
        return categorySuggestLevels;
    }

    public void setCategorySuggestLevels(final Long categorySuggestLevels) {
        this.categorySuggestLevels = categorySuggestLevels;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    protected DefaultCategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(DefaultCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

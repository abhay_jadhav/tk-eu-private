package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Optional;

import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;

public interface TkEuConfiguratorSettingsService extends TkB2bConfiguratorSettingsService {

    TkTradeLengthConfiguratorSettingsModel getSelectedTradeLengthConfigurationForProduct(ProductModel productModel);

    Optional<ProductLengthConfigurationParam> getProductLengthInfoForOrderEntry(AbstractOrderEntryModel entryModel);

    boolean hasValidLengthConfigurationSetting(AbstractOrderEntryModel entryModel);

    boolean hasValidTradeLengthConfigurationSetting(AbstractOrderEntryModel entryModel);

    boolean hasValidC2lConfigurationSetting(AbstractOrderEntryModel entryModel);
}

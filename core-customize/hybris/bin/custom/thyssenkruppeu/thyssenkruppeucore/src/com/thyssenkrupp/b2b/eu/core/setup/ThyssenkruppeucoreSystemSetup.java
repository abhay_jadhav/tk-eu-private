package com.thyssenkrupp.b2b.eu.core.setup;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;
import com.thyssenkrupp.b2b.eu.core.service.ThyssenkruppeucoreService;

@SystemSetup(extension = ThyssenkruppeucoreConstants.EXTENSIONNAME)
public class ThyssenkruppeucoreSystemSetup extends AbstractSystemSetup {
    public static final String IMPORT_ACCESS_RIGHTS = "accessRights";

    private final ThyssenkruppeucoreService thyssenkruppeucoreService;

    public ThyssenkruppeucoreSystemSetup(final ThyssenkruppeucoreService thyssenkruppeucoreService) {
        this.thyssenkruppeucoreService = thyssenkruppeucoreService;
    }
    //
    // @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    // public void createEssentialData() {
    // thyssenkruppeucoreService.createLogo(PLATFORM_LOGO_CODE);
    // }

    /**
     * This method will be called by system creator during initialization and system update. Be sure
     * that this method can be called repeatedly.
     *
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        importImpexFile(context, "/thyssenkruppeucore/import/common/countries.impex");
        importImpexFile(context, "/thyssenkruppeucore/import/common/delivery-modes.impex");

        importImpexFile(context, "/thyssenkruppeucore/import/common/themes.impex");
        importImpexFile(context, "/thyssenkruppeucore/import/common/user-groups.impex");

        if (context.getProcess().isInit()) {

            thyssenkruppeucoreService.createLogo(PLATFORM_LOGO_CODE);
        }

    }

    // private InputStream getImageStream() {
    // return
    // ThyssenkruppeucoreSystemSetup.class.getResourceAsStream("/thyssenkruppeucore/sap-hybris-platform.png");
    // }
    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();

        params.add(createBooleanSystemSetupParameter(IMPORT_ACCESS_RIGHTS, "Import Users & Groups", true));

        return params;

    }

    /**
     * This method will be called during the system initialization.
     *
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        final boolean importAccessRights = getBooleanSystemSetupParameter(context, IMPORT_ACCESS_RIGHTS);

        final List<String> extensionNames = getExtensionNames();

        processCockpit(context, importAccessRights, extensionNames, "cmscockpit", "/thyssenkruppeucore/import/cockpits/cmscockpit/cmscockpit-users.impex", "/thyssenkruppcore/import/cockpits/cmscockpit/cmscockpit-access-rights.impex");

        processCockpit(context, importAccessRights, extensionNames, "productcockpit", "/thyssenkruppeucore/import/cockpits/productcockpit/productcockpit-users.impex", "/thyssenkruppcore/import/cockpits/productcockpit/productcockpit-access-rights.impex", "/thyssenkruppcore/import/cockpits/productcockpit/productcockpit-constraints.impex");

        processCockpit(context, importAccessRights, extensionNames, "cscockpit", "/thyssenkruppeucore/import/cockpits/cscockpit/cscockpit-users.impex", "/thyssenkruppcore/import/cockpits/cscockpit/cscockpit-access-rights.impex");

        processCockpit(context, importAccessRights, extensionNames, "reportcockpit", "/thyssenkruppeucore/import/cockpits/reportcockpit/reportcockpit-users.impex", "/thyssenkruppcore/import/cockpits/reportcockpit/reportcockpit-access-rights.impex");

        if (extensionNames.contains("mcc")) {
            importImpexFile(context, "/thyssenkruppcore/import/common/mcc-sites-links.impex");
        }
        importImpexFile(context, "/thyssenkruppcore/import/common/site-essential-data.impex");
    }

    protected void processCockpit(final SystemSetupContext context, final boolean importAccessRights, final List<String> extensionNames, final String cockpit, final String... files) {
        if (importAccessRights && extensionNames.contains(cockpit)) {
            for (final String file : files) {
                importImpexFile(context, file);
            }
        }
    }

    protected List<String> getExtensionNames() {
        return Registry.getCurrentTenant().getTenantSpecificExtensionNames();
    }

    protected <T> T getBeanForName(final String name) {
        return (T) Registry.getApplicationContext().getBean(name);
    }

}


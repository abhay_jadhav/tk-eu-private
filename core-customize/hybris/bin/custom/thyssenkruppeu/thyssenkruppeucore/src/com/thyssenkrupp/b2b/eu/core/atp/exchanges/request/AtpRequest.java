package com.thyssenkrupp.b2b.eu.core.atp.exchanges.request;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AtpRequest implements Serializable {

    @JsonProperty("Region_BusinessDivision_Environment")
    private String               regionBusinessDivisionEnvironment;
    @JsonProperty("Customer_SoldTo_Number")
    private String customerSoldToNumber;
    @JsonProperty("WNAE")
    private boolean wnae;
    @JsonProperty("Customer_ShipTo_Number")
    private String               customerShipToNumber;
    @JsonProperty("Ship_To_Country_Code")
    private String               shipToCountryCode;
    @JsonProperty("Ship_To_PostalCode")
    private String               shipToPostalCode;
    @JsonProperty("Division")
    private String               division;
    @JsonProperty("Sales_Organization")
    private String               salesOrganization;
    @JsonProperty("Shipping_Conditions")
    private String               shippingConditions;
    @JsonProperty("Distribution_Channel")
    private String               distributionChannel;
    @JsonProperty("Items")
    private List<AtpRequestItem> atpRequestItems = null;
    @JsonProperty("Configuration")
    private List<AtpRequestItemConfiguration> atpRequestItemConfigurations = null;

    @JsonProperty("Region_BusinessDivision_Environment")
    public String getRegionBusinessDivisionEnvironment() {
        return regionBusinessDivisionEnvironment;
    }

    @JsonProperty("Region_BusinessDivision_Environment")
    public void setRegionBusinessDivisionEnvironment(final String regionBusinessDivisionEnvironment) {
        this.regionBusinessDivisionEnvironment = regionBusinessDivisionEnvironment;
    }

    @JsonProperty("Customer_ShipTo_Number")
    public String getCustomerShipToNumber() {
        return customerShipToNumber;
    }

    @JsonProperty("Customer_ShipTo_Number")
    public void setCustomerShipToNumber(final String customerShipToNumber) {
        this.customerShipToNumber = customerShipToNumber;
    }

    @JsonProperty("Ship_To_Country_Code")
    public String getShipToCountryCode() {
        return shipToCountryCode;
    }

    @JsonProperty("Ship_To_Country_Code")
    public void setShipToCountryCode(final String shipToCountryCode) {
        this.shipToCountryCode = shipToCountryCode;
    }

    @JsonProperty("Ship_To_PostalCode")
    public String getShipToPostalCode() {
        return shipToPostalCode;
    }

    @JsonProperty("Ship_To_PostalCode")
    public void setShipToPostalCode(final String shipToPostalCode) {
        this.shipToPostalCode = shipToPostalCode;
    }

    @JsonProperty("Division")
    public String getDivision() {
        return division;
    }

    @JsonProperty("Division")
    public void setDivision(final String division) {
        this.division = division;
    }

    @JsonProperty("Sales_Organization")
    public String getSalesOrganization() {
        return salesOrganization;
    }

    @JsonProperty("Sales_Organization")
    public void setSalesOrganization(final String salesOrganization) {
        this.salesOrganization = salesOrganization;
    }

    @JsonProperty("Shipping_Conditions")
    public String getShippingConditions() {
        return shippingConditions;
    }

    @JsonProperty("Shipping_Conditions")
    public void setShippingConditions(final String shippingConditions) {
        this.shippingConditions = shippingConditions;
    }

    @JsonProperty("Distribution_Channel")
    public String getDistributionChannel() {
        return distributionChannel;
    }

    @JsonProperty("Distribution_Channel")
    public void setDistributionChannel(final String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    @JsonProperty("Items")
    public List<AtpRequestItem> getAtpRequestItems() {
        return atpRequestItems;
    }

    @JsonProperty("Items")
    public void setAtpRequestItems(final List<AtpRequestItem> atpRequestItems) {
        this.atpRequestItems = atpRequestItems;
    }

    @JsonProperty("Customer_SoldTo_Number")
    public String getCustomerSoldToNumber() {
        return customerSoldToNumber;
    }

    @JsonProperty("Customer_SoldTo_Number")
    public void setCustomerSoldToNumber(final String customerSoldToNumber) {
        this.customerSoldToNumber = customerSoldToNumber;
    }

    @JsonProperty("Configuration")
    public List<AtpRequestItemConfiguration> getAtpRequestItemConfigurations() {
        return atpRequestItemConfigurations;
    }

    @JsonProperty("Configuration")
    public void setAtpRequestItemConfigurations(final List<AtpRequestItemConfiguration> atpRequestItemConfigurations) {
        this.atpRequestItemConfigurations = atpRequestItemConfigurations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Region_BusinessDivision_Environment", regionBusinessDivisionEnvironment)
                .append("Customer_SoldTo_Number", customerSoldToNumber).append("Customer_ShipTo_Number", customerShipToNumber).append("WNAE", wnae).append("Ship_To_Country_Code", shipToCountryCode).append("Ship_To_PostalCode", shipToPostalCode)
            .append("Division", division).append("Sales_Organization", salesOrganization).append("Shipping_Conditions", shippingConditions)
                .append("Distribution_Channel", distributionChannel).append("Items", atpRequestItems).append("Configuration", atpRequestItemConfigurations).toString();
    }

    @JsonProperty("WNAE")
    public boolean getWnae() {
        return wnae;
    }

    @JsonProperty("WNAE")
    public void setWnae(final boolean wnae) {
        this.wnae = wnae;
    }
}

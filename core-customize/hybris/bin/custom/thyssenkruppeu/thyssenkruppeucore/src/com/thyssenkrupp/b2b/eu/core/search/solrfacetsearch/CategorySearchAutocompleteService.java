package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch;

import com.thyssenkrupp.b2b.eu.core.search.data.TkEuCategorySuggestion;

import java.util.List;

public interface CategorySearchAutocompleteService<RESULT extends TkEuCategorySuggestion> {

    List<RESULT> getCategoryAutocompleteSuggestion(String input);
}

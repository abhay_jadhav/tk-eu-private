package com.thyssenkrupp.b2b.eu.core.atp.service.impl;

import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpRequestEntryData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpResponseData;
import com.thyssenkrupp.b2b.eu.core.atp.service.AtpCheckService;
import com.thyssenkrupp.b2b.eu.core.stock.service.TkEuStockService;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class HybrisAtpCheckService implements AtpCheckService {

    private static final Logger                     LOG = Logger.getLogger(HybrisAtpCheckService.class);
    private              TkEuStockService           stockService;
    private              WarehouseSelectionStrategy warehouseSelectionStrategy;

    @Override
    public List<AtpResponseData> doAtpCheck(final AtpRequestData request, final BaseStoreModel baseStore) {
        return generateAtpResponses(request, baseStore);
    }

    private List<AtpResponseData> generateAtpResponses(final AtpRequestData request, final BaseStoreModel baseStore) {
        final List<AtpRequestEntryData> requestEntries = request.getEntries();
        if (isNotEmpty(requestEntries)) {
            final List<AtpResponseData> responseDataList = new ArrayList<>(requestEntries.size());
            final List<WarehouseModel> warehouses = getWarehouseSelectionStrategy().getWarehousesForBaseStore(baseStore);
            for (AtpRequestEntryData requestEntry : requestEntries) {
                LOG.debug("Started generating InStockStatus and AtpResponseData for productCode:" + requestEntry.getProductCode());
                final InStockStatus inStockStatus = getStockService().getInStockStatus(requestEntry.getProductCode(), warehouses);
                final AtpResponseData responseData = AtpResponseDataBuilder.builder().withPosition(requestEntry.getPosition())
                    .withProductCode(requestEntry.getProductCode()).withHybrisResult(inStockStatus).build();
                LOG.debug("Finished generating InStockStatus and AtpResponseData for productCode" + requestEntry.getProductCode());
                responseDataList.add(responseData);
            }
            return responseDataList;
        }
        return Collections.emptyList();
    }

    public TkEuStockService getStockService() {
        return stockService;
    }

    @Required
    public void setStockService(TkEuStockService stockService) {
        this.stockService = stockService;
    }

    public WarehouseSelectionStrategy getWarehouseSelectionStrategy() {
        return warehouseSelectionStrategy;
    }

    @Required
    public void setWarehouseSelectionStrategy(WarehouseSelectionStrategy warehouseSelectionStrategy) {
        this.warehouseSelectionStrategy = warehouseSelectionStrategy;
    }
}

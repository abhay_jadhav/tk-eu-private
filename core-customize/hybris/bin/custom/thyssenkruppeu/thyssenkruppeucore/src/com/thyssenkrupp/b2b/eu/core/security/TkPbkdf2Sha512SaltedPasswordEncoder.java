package com.thyssenkrupp.b2b.eu.core.security;

import de.hybris.platform.persistence.security.EJBCannotDecodePasswordException;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.util.Config;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.codec.Hex;

public class TkPbkdf2Sha512SaltedPasswordEncoder implements PasswordEncoder, InitializingBean {

    private static final Logger LOGGER = Logger.getLogger(TkPbkdf2Sha512SaltedPasswordEncoder.class);

    private SecureRandom saltGenerator;
    private SecretKeyFactory keyFactory;
    private int iterations = 500000;
    private int keyLength = 512;
    private String keyAlgorithm = "PBKDF2WithHmacSHA512";
    private int saltLength = 16;
    private String saltAlgorithm = "SHA1PRNG";

    @Override
    public void afterPropertiesSet() throws NoSuchAlgorithmException {
        this.saltGenerator = SecureRandom.getInstance(this.saltAlgorithm);
        this.keyFactory = SecretKeyFactory.getInstance(this.keyAlgorithm);
    }

    @Override
    public String encode(final String uid, final String password) {
        this.iterations = Config.getInt("tk.pbkdf2.password.encoder.iterations", 10000);
        LOGGER.debug("iteration count to use " + this.iterations);

        final byte[] salt = generateSalt();
        final byte[] hash = calculateHash(password, salt, this.iterations, this.keyLength);
        return (new EncodedHash(this.iterations, salt, hash)).toString();
    }

    @Override
    public boolean check(final String uid, final String encoded, final String plain) {
        final EncodedHash parsedEncodedHash = new EncodedHash(encoded);
        LOGGER.debug("iterations from encoded hash : " + parsedEncodedHash.iterations);
        final byte[] hash = this.calculateHash(plain, parsedEncodedHash.salt, parsedEncodedHash.iterations, parsedEncodedHash.hash.length * 8);
        return Arrays.equals(parsedEncodedHash.hash, hash);
    }

    @Override
    public String decode(final String encoded) throws EJBCannotDecodePasswordException {
        final int vendorCode = 0;
        throw new EJBCannotDecodePasswordException(new Throwable("You cannot decode a PBKDF2WithHmacSHA512 hash!"), "Cannot decode", vendorCode);
    }

    protected byte[] calculateHash(final String password, final byte[] salt, final int hashIterations, final int hashKeyLength) {
        final String hashPassword = password == null ? "" : password;
        try {
            final PBEKeySpec keySpec = new PBEKeySpec(hashPassword.toCharArray(), salt, hashIterations, hashKeyLength);
            return this.keyFactory.generateSecret(keySpec).getEncoded();
        } catch (final InvalidKeySpecException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    private byte[] generateSalt() {
        final byte[] salt = new byte[this.saltLength];
        this.saltGenerator.nextBytes(salt);
        return salt;
    }

    public void setIterations(final int iterations) {
        this.iterations = iterations;
    }

    public void setKeyLength(final int keyLength) {
        this.keyLength = keyLength;
    }

    public void setKeyAlgorithm(final String keyAlgorithm) {
        this.keyAlgorithm = keyAlgorithm;
    }

    public void setSaltAlgorithm(final String saltAlgorithm) {
        this.saltAlgorithm = saltAlgorithm;
    }

    public void setSaltLength(final int saltLength) {
        this.saltLength = saltLength;
    }

    private static class EncodedHash {
        private final int iterations;
        private final byte[] salt;
        private final byte[] hash;

        EncodedHash(final String encoded) {
            final String[] parts = encoded.split(":");
            this.iterations = Integer.parseInt(parts[0]);
            this.salt = Hex.decode(parts[1]);
            this.hash = Hex.decode(parts[2]);
        }

        EncodedHash(final int iterations, final byte[] salt, final byte[] hash) {
            this.iterations = iterations;
            this.hash = Arrays.copyOf(hash, hash.length);
            this.salt = Arrays.copyOf(salt, salt.length);
        }

        @Override
        public String toString() {
            return this.iterations + ":" + new String(Hex.encode(this.salt)) + ":" + new String(Hex.encode(this.hash));
        }
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuConfigKeyValueDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConfigKeyValueService;
/**
 *
 */
public class TkEuConfigKeyValueServiceImpl implements TkEuConfigKeyValueService {

    private TkEuConfigKeyValueDao tkEuConfigKeyValueDao;

    @Override
    public String getProperty(final String key) {
        return getTkEuConfigKeyValueDao().getProperty(key).getValue();
    }

    @Override
    public double getDouble(final String key) {
        return NumberUtils.toDouble(getProperty(key));

    }

    @Override
    public boolean getBoolean(final String key) {
        return BooleanUtils.toBoolean(getProperty(key));
    }

    @Override
    public BigDecimal getBigDecimal(final String key) {
        return BigDecimal.valueOf(getDouble(key));
    }

    /**
     * @return the tkEuConfigKeyValueDao
     */
    public TkEuConfigKeyValueDao getTkEuConfigKeyValueDao() {
        return tkEuConfigKeyValueDao;
    }

    /**
     * @param tkEuConfigKeyValueDao
     *            the tkEuConfigKeyValueDao to set
     */
    public void setTkEuConfigKeyValueDao(final TkEuConfigKeyValueDao tkEuConfigKeyValueDao) {
        this.tkEuConfigKeyValueDao = tkEuConfigKeyValueDao;
    }

}

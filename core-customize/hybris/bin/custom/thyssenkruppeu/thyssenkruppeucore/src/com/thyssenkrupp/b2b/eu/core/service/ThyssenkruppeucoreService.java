package com.thyssenkrupp.b2b.eu.core.service;

public interface ThyssenkruppeucoreService {
    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);
}


package com.thyssenkrupp.b2b.eu.core.pojo;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;

public class VariantCategoryCreationParameter {

    private String                   classCode;
    private String                   attributeCode;
    private CatalogVersionModel      catalogVersionModel;
    private ClassificationClassModel classificationClass;
    private boolean                  fallBackClassCode;

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(final String classCode) {
        this.classCode = classCode;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public void setAttributeCode(final String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public CatalogVersionModel getCatalogVersionModel() {
        return catalogVersionModel;
    }

    public void setCatalogVersionModel(final CatalogVersionModel catalogVersionModel) {
        this.catalogVersionModel = catalogVersionModel;
    }

    public boolean isFallBackClassCode() {
        return fallBackClassCode;
    }

    public void setFallBackClassCode(final boolean fallBackClassCode) {
        this.fallBackClassCode = fallBackClassCode;
    }

    public ClassificationClassModel getClassificationClass() {
        return classificationClass;
    }

    public void setClassificationClass(final ClassificationClassModel classificationClass) {
        this.classificationClass = classificationClass;
    }
}

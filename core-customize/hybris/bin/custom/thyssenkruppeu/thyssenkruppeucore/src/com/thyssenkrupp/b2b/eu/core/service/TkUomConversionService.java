package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import com.thyssenkrupp.b2b.eu.core.model.TkUomConversionModel;

public interface TkUomConversionService {

    List<TkUomConversionModel> getUomConversionByProductCode(String productCode);

    Pair<Double, UnitModel> calculateQuantityInTargetUnit(Pair<Double, UnitModel> sourceMaterialQty, UnitModel targetUnit, ProductModel product);

    Optional<BigDecimal> calculateFactor(Double dividend, Double divisor);

    Optional<TkUomConversionModel> getFirstWeightedUomConversion(List<TkUomConversionModel> conversionModels);

    boolean isWeightedUomConversion(TkUomConversionModel conversionModel);
}

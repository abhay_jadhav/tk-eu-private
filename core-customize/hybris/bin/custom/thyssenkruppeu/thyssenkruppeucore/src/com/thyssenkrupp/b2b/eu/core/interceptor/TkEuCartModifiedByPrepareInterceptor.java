
package com.thyssenkrupp.b2b.eu.core.interceptor;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;

/**
 *
 */
public class TkEuCartModifiedByPrepareInterceptor implements PrepareInterceptor<CartModel> {
    private static final Logger LOG = Logger.getLogger(TkEuCartModifiedByPrepareInterceptor.class);
    private AssistedServiceFacade assistedServiceFacade;

    @Override
    public void onPrepare(final CartModel cart, final InterceptorContext ctx) throws InterceptorException {

        /* setting User type who Modified cart, Either Customer or ASM */
        if (null != cart) {

            cart.setCartModifiedBy(assistedServiceFacade.isAssistedServiceAgentLoggedIn() ? LoginType.ASM : LoginType.CUSTOMER);
            LOG.debug("TkEuCartModifiedByPrepareInterceptor called : cart Modified set as : " + cart.getCartModifiedBy());
        }
    }

    /**
     * @return the assistedServiceFacade
     */
    public AssistedServiceFacade getAssistedServiceFacade() {
        return assistedServiceFacade;
    }

    /**
     * @param assistedServiceFacade
     *            the assistedServiceFacade to set
     */
    public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade) {
        this.assistedServiceFacade = assistedServiceFacade;
    }


}

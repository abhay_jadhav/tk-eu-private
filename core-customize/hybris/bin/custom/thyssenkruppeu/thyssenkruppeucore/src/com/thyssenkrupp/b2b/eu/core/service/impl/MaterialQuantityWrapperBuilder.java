package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import de.hybris.platform.core.model.product.UnitModel;

public final class MaterialQuantityWrapperBuilder {

    private MaterialQuantityWrapperBuilder() {
        //empty constructor
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private double    quantity;
        private UnitModel unit;

        public Builder() {
            //empty constructor
        }

        public Builder withQuantity(final double pQuantity) {
            this.quantity = pQuantity;
            return this;
        }

        public Builder withUnit(final UnitModel pUnit) {
            this.unit = pUnit;
            return this;
        }

        public MaterialQuantityWrapper build() {
            MaterialQuantityWrapper materialQuantityWrapper = new MaterialQuantityWrapper();
            materialQuantityWrapper.setUnitModel(this.unit);
            materialQuantityWrapper.setQuantity(this.quantity);
            return materialQuantityWrapper;
        }
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDao;
import com.thyssenkrupp.b2b.eu.core.event.TkEuB2bRegistrationAmbiguousEmailEvent;
import com.thyssenkrupp.b2b.eu.core.event.TkEuB2bRegistrationEmailEvent;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bCustomerAccountService;

public class DefaultTkEuB2bCustomerAccountService extends DefaultTkB2bCustomerAccountService implements TkEuB2bCustomerAccountService {
    private static final Logger                 LOG = Logger.getLogger(DefaultTkEuB2bCustomerAccountService.class);
    private              long                   registrationEmailTokenValidityInSeconds;
    private              TkEuCustomerAccountDao tkEuCustomerAccountDao;

    @Override
    public void sendRegistrationEmail(final CustomerModel customerModel) {
        validateParameterNotNullStandardMessage("customerModel", customerModel);
        final String secureToken = generateSecureToken(customerModel);
        customerModel.setToken(secureToken);
        getModelService().save(customerModel);
        LOG.debug("Publishing RegistrationEmailEvent for customerUid:" + customerModel.getUid());
        getEventService().publishEvent(initializeEvent(new TkEuB2bRegistrationEmailEvent(secureToken), customerModel));
    }

    private String generateSecureToken(final CustomerModel customerModel) {
        final long timeStamp = getRegistrationEmailTokenValidityInSeconds() > 0L ? new Date().getTime() : 0L;
        final SecureToken data = new SecureToken(customerModel.getUid(), timeStamp);
        return getSecureTokenService().encryptData(data);
    }

    public long getRegistrationEmailTokenValidityInSeconds() {
        return registrationEmailTokenValidityInSeconds;
    }

    public void setRegistrationEmailTokenValidityInSeconds(final long registrationEmailTokenValidityInSeconds) {
        this.registrationEmailTokenValidityInSeconds = registrationEmailTokenValidityInSeconds;
    }

    @Override
    public void sendDuplicateUidFoundRegistrationEmail(final List<CustomerModel> customerModelList) {
        try {
            validateParameterNotNullStandardMessage("customerModel ", customerModelList);
            LOG.debug("Publishing Ambiiguous RegistrationEmailEvent for customers : Customer List Size : " + customerModelList.size());
            final TkEuB2bRegistrationAmbiguousEmailEvent event = new TkEuB2bRegistrationAmbiguousEmailEvent();
            event.setAmbiguousCustomerModelList(customerModelList);
            getEventService().publishEvent(event);
        } catch (final Exception ex) {
            LOG.error("sendDuplicateUidFoundRegistrationEmail Error :" + ex);
        }
    }

    @Override
    public SearchPageData<OrderModel> orderAcknowledgementSearchList(final String searchKey, final CustomerModel customerModel,
        final BaseStoreModel store,
        final OrderStatus[] status, final PageableData pageableData) {
        validateParameterNotNull(customerModel, "Customer model cannot be null");
        validateParameterNotNull(store, "Store must not be null");
        validateParameterNotNull(pageableData, "PageableData must not be null");
        return getTkEuCustomerAccountDao().orderAcknowledgementSearchList(searchKey, customerModel, store, status, pageableData);
    }

    @Override
    public Integer findOrderAcknowledgementListCount(final String searchKey, final CustomerModel customerModel,
        final BaseStoreModel store, final OrderStatus[] status) {

        return getTkEuCustomerAccountDao().findOrderAcknowledgementListCount(searchKey, customerModel, store, status);
    }

    @Override
    public SearchPageData<OrderModel> orderConfirmationSearchList(final String searchKey, final CustomerModel customerModel,
        final BaseStoreModel store,
        final OrderStatus[] status, final PageableData pageableData) {
        validateParameterNotNull(customerModel, "Customer model cannot be null");
        validateParameterNotNull(store, "Store must not be null");
        validateParameterNotNull(pageableData, "PageableData must not be null");
        return getTkEuCustomerAccountDao().orderConfirmationSearchList(searchKey, customerModel, store, status, pageableData);
    }

    @Override
    public OrderModel getOrderConfirmationDetailsForCode(final String code, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        return getTkEuCustomerAccountDao().getOrderConfirmationDetailsForCode(code, store, b2BUnitModel);
    }

    @Override
    public OrderModel getConfirmedOrderByCode(final String code, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        return getTkEuCustomerAccountDao().getConfirmedOrderByCode(code, store, b2BUnitModel);
    }

    @Override
    public OrderModel getCancelledOrderByCode(final String code, final BaseStoreModel store, final Set<B2BUnitModel> b2BUnitModel) {
        return getTkEuCustomerAccountDao().getCancelledOrderByCode(code, store, b2BUnitModel);
    }

    @Override
    public Integer findOrderConfirmationListCount(final String searchKey, final CustomerModel customerModel,
        final BaseStoreModel store, final OrderStatus[] status) {

        return getTkEuCustomerAccountDao().findOrderConfirmationListCount(searchKey, customerModel, store, status);
    }

    public TkEuCustomerAccountDao getTkEuCustomerAccountDao() {
        return tkEuCustomerAccountDao;
    }

    public void setTkEuCustomerAccountDao(final TkEuCustomerAccountDao tkEuCustomerAccountDao) {
        this.tkEuCustomerAccountDao = tkEuCustomerAccountDao;
    }
}

package com.thyssenkrupp.b2b.eu.core.interceptor;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class TkEuProductReferenceValidateInterceptor implements ValidateInterceptor {

    private static final Logger LOG = Logger.getLogger(TkEuProductReferenceValidateInterceptor.class);
    private I18NService i18nService;

    @Override
    public void onValidate(final Object object, final InterceptorContext interceptorContext) throws InterceptorException {

        if (object instanceof ProductReferenceModel) {
            final ProductReferenceModel productReferenceComponent = (ProductReferenceModel) object;
            if (productReferenceComponent.getTarget().getClass() != productReferenceComponent.getSource().getClass()) {

                final ResourceBundle javaBundle = ResourceBundle.getBundle("localization.thyssenkruppeucore-locales",
                  getI18nService().getCurrentLocale(), getClass().getClassLoader());
                throw new InterceptorException(MessageFormat.format(javaBundle.getString("product.reference.erpVariant.error.msg"),
                  productReferenceComponent.getTarget().getCode()));
            }
        }
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }
}

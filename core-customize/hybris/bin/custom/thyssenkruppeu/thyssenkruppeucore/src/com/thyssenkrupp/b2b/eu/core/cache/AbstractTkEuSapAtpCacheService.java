package com.thyssenkrupp.b2b.eu.core.cache;

import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.key.CacheKey;

import java.util.Optional;

public abstract class AbstractTkEuSapAtpCacheService implements TkEuSAPAtpCacheService {

    private CacheController cacheController;

    public CacheController getCacheController() {
        return cacheController;
    }

    public void setCacheController(CacheController cacheController) {
        this.cacheController = cacheController;
    }

    public Optional<Object> get(CacheKey key) {
        return Optional.ofNullable(getCacheController().get(key));
    }

    public void put(CacheKey key, Object content) {
        getCacheController().getWithLoader(key, cacheKey -> content);
    }
}

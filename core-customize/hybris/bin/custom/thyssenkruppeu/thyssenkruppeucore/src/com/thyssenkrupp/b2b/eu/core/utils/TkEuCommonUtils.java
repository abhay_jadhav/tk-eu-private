package com.thyssenkrupp.b2b.eu.core.utils;

public final class TkEuCommonUtils {

    public static final String REGEX_MULTIPLE_EMPTY_LINES = "(?m)^$([\\r\\n])+";
    public static final String REGEX_SINGLE_EMPTY_LINE    = "\r\n";

    private TkEuCommonUtils() {
        throw new IllegalAccessError("Utility class may not be instantiated");
    }

    public static String truncateMultipleEmptyLinesToSingle(String comment) {

        String editedComment = comment != null ? comment.trim().replaceAll(" +", " ").replaceAll(REGEX_MULTIPLE_EMPTY_LINES, REGEX_SINGLE_EMPTY_LINE) : null;
        return editedComment;
    }
}

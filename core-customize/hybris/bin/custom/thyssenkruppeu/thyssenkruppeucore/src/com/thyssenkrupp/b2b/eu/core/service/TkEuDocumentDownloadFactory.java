package com.thyssenkrupp.b2b.eu.core.service;

import com.thyssenkrupp.b2b.eu.core.enums.DocumentDownloadServiceEnum;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TkEuDocumentDownloadFactory {

    protected BaseSiteService baseSiteService;

    private List<TkEuDocumentDownloadService> downloadServices;

    private final Map<DocumentDownloadServiceEnum, TkEuDocumentDownloadService> downloadServiceCache = new HashMap<DocumentDownloadServiceEnum, TkEuDocumentDownloadService>();

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    public List<TkEuDocumentDownloadService> getDownloadServices() {
        return downloadServices;
    }

    public void setDownloadServices(List<TkEuDocumentDownloadService> downloadServices) {
        this.downloadServices = downloadServices;
    }

    @PostConstruct
    public void initDownloadServiceCache() {
        downloadServices.forEach(service -> downloadServiceCache.put(service.getType(), service));
    }

    public TkEuDocumentDownloadService getDocumentDownloadService() {
        DocumentDownloadServiceEnum enumVal = baseSiteService.getCurrentBaseSite().getDocumentDownloadService();
        return downloadServiceCache.get(enumVal);
    }
}

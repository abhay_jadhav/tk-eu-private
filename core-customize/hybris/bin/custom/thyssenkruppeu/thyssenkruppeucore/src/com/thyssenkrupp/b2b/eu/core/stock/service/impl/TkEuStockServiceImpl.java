package com.thyssenkrupp.b2b.eu.core.stock.service.impl;

import com.thyssenkrupp.b2b.eu.core.stock.service.TkEuStockService;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.impl.DefaultStockService;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import static de.hybris.platform.basecommerce.enums.InStockStatus.NOTAVAILABLE;
import static java.util.Objects.nonNull;

public class TkEuStockServiceImpl extends DefaultStockService implements TkEuStockService {

    private static final Logger LOG = Logger.getLogger(TkEuStockServiceImpl.class);

    @Override
    public InStockStatus getInStockStatus(final String productCode, final List<WarehouseModel> warehouses) {
        return getInStockStatusForProduct(productCode, warehouses);
    }

    private InStockStatus getInStockStatusForProduct(final String productCode, final List<WarehouseModel> warehouses) {
        final Collection<StockLevelModel> stockLevels = getStockLevelDao().findStockLevels(productCode, warehouses);
        return getValidInStockStatus(productCode, stockLevels);
    }

    private InStockStatus getValidInStockStatus(final String productCode, final Collection<StockLevelModel> stockLevels) {
        for (StockLevelModel stockLevel : stockLevels) {
            final InStockStatus inStockStatus = stockLevel.getInStockStatus();
            if (isValidInStockStatus(inStockStatus)) {
                return inStockStatus;
            }
        }
        LOG.warn(MessageFormat.format("No Valid InStockStatus found for productCode:{0} returning InStockStatus as ''NOTAVAILABLE''", productCode));
        return InStockStatus.NOTAVAILABLE;
    }

    private boolean isValidInStockStatus(final InStockStatus inStockStatus) {
        return nonNull(inStockStatus) && NOTAVAILABLE != inStockStatus;
    }
}

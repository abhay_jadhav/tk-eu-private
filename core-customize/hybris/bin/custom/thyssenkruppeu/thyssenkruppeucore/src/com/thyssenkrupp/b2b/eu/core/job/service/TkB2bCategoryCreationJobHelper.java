package com.thyssenkrupp.b2b.eu.core.job.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface TkB2bCategoryCreationJobHelper {

    Map<String, String> getAllVariantGenerationRulesForProductCatalog();

    void setVariantCategoriesToBaseProduct(ProductModel product, List<VariantCategoryModel> variantCategories);

    Optional<List<VariantCategoryModel>> createVariantCategoriesForClass(String ruleCode, ClassificationClassModel classCategory, CatalogVersionModel catalogVersion);

    Optional<VariantCategoryModel> getVariantCategoryByCode(String classCode, String attributeCode, List<VariantCategoryModel> variantCategories);

    void cleanUpVariantCategories(Map<String, List<VariantCategoryModel>> variantCategoriesMap);

    List<VariantValueCategoryModel> buildVariantCategoryValuesForConfigurableLength(VariantProductModel variant, VariantCategoryModel variantCategoryModel);
}

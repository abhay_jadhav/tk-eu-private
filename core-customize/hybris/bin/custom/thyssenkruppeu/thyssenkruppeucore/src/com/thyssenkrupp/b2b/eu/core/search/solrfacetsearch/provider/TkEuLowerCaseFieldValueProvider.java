package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class TkEuLowerCaseFieldValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private static final Logger LOG       = Logger.getLogger(TkEuLowerCaseFieldValueProvider.class);
    private static final String CODE_TYPE = "_string";
    private static final String CODE      = "code";

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        if (model == null) {
            throw new IllegalArgumentException("No model given");
        }

        try {
            final ArrayList<FieldValue> fieldValues = new ArrayList<>();
            String fieldName = getFieldName(indexedProperty);
            final Object value = modelService.getAttributeValue(model, fieldName);
            if (fieldName.equals(CODE)) {
                fieldName = fieldName + CODE_TYPE;
            }
            addFieldValues(fieldValues, fieldName, value);
            return fieldValues;
        } catch (final Exception e) {
            LOG.error(e);
            throw new FieldValueProviderException("Cannot evaluate " + indexedProperty.getName() + " using "
                + this.getClass().getName(), e);
        }
    }

    private String getFieldName(final IndexedProperty indexedProperty) {
        String fieldName = indexedProperty.getValueProviderParameter();
        if (StringUtils.isBlank(fieldName)) {
            fieldName = indexedProperty.getName();
        }
        return fieldName;
    }

    private void addFieldValues(final List<FieldValue> result, final String fieldName, final Object value) {
        if (value instanceof String) {
            final String stringValue = (String) value;
            result.add(new FieldValue(fieldName, stringValue.toLowerCase()));
        } else {
            result.add(new FieldValue(fieldName, value));
        }
    }
}

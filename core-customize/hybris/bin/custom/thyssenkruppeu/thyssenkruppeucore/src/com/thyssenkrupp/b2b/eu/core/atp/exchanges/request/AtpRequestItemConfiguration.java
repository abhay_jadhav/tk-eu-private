package com.thyssenkrupp.b2b.eu.core.atp.exchanges.request;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AtpRequestItemConfiguration implements Serializable {

    @JsonProperty("Line_Item_Number")
    private String lineItemNumber;
    @JsonProperty("Instance_Number")
    private String instanceNumber;
    @JsonProperty("Characteristic_Name")
    private String characteristicName;
    @JsonProperty("Characteristic_Value")
    private String characteristicValue;

    public AtpRequestItemConfiguration(final String lineItemNumber, final String instanceNumber, final String characteristicName, final String characteristicValue) {
        this.lineItemNumber = lineItemNumber;
        this.instanceNumber = instanceNumber;
        this.characteristicName = characteristicName;
        this.characteristicValue = characteristicValue;
    }

    @JsonProperty("Line_Item_Number")
    public String getLineItemNumber() {
        return lineItemNumber;
    }

    @JsonProperty("Line_Item_Number")
    public void setLineItemNumber(final String lineItemNumber) {
        this.lineItemNumber = lineItemNumber;
    }

    @JsonProperty("Instance_Number")
    public String getInstanceNumber() {
        return instanceNumber;
    }

    @JsonProperty("Instance_Number")
    public void setInstanceNumber(final String instanceNumber) {
        this.instanceNumber = instanceNumber;
    }

    @JsonProperty("Characteristic_Name")
    public String getCharacteristicName() {
        return characteristicName;
    }

    @JsonProperty("Characteristic_Name")
    public void setCharacteristicName(final String characteristicName) {
        this.characteristicName = characteristicName;
    }

    @JsonProperty("Characteristic_Value")
    public String getCharacteristicValue() {
        return characteristicValue;
    }

    @JsonProperty("Characteristic_Value")
    public void setCharacteristicValue(final String characteristicValue) {
        this.characteristicValue = characteristicValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Line_Item_Number", lineItemNumber).append("Instance_Number", instanceNumber).append("Characteristic_Name", characteristicName).append("Characteristic_Value", characteristicValue).toString();
    }


}

package com.thyssenkrupp.b2b.eu.core.atp.event;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

public class FetchAtpMerticsEvent extends AbstractEvent implements ClusterAwareEvent {

    private static final String ATP_METRICS_NODES_TO_IGNORE = "atp.hystrix.metrics.ignoreNodeId";
    
    @Override
    public boolean publish(int sourceNodeId, int targetNodeId) {
        ConfigurationService configurationService = (ConfigurationService) Registry.getApplicationContext().getBean("configurationService");
        return targetNodeId != Integer.parseInt((String)configurationService.getConfiguration().getProperty(ATP_METRICS_NODES_TO_IGNORE));
    }
}

package com.thyssenkrupp.b2b.eu.core.atp.service;

import java.util.List;

public interface AtpService {
    List<AtpResponseData> doAtpCheck(AtpRequestData request);
}

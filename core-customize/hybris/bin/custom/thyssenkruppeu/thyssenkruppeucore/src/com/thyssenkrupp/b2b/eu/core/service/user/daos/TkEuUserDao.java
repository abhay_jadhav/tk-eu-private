package com.thyssenkrupp.b2b.eu.core.service.user.daos;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;

public interface TkEuUserDao extends UserDao {

    UserModel findAllUserByUID(String userId);

    UserModel findAllUserAllStatusesByUID(String userId);
}

package com.thyssenkrupp.b2b.eu.core.cache.impl;

import com.thyssenkrupp.b2b.eu.core.cache.AbstractTkEuSapAtpCacheService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.servicelayer.config.ConfigurationService;

public class DefaultTkEuSapAtpPdpCacheService extends AbstractTkEuSapAtpCacheService {

    private static final String CACHE_ENABLED_KEY = "thyssenkruppeucore.atpPdp.useCache.enabled";

    private ConfigurationService configurationService;

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public boolean useCache() {
        return getConfigurationService().getConfiguration().getBoolean(CACHE_ENABLED_KEY, true);
    }

    @Override
    public CacheKey getKey(String key, String storeId) {
        return new TkEuSapAtpPdpCacheKey(key, storeId, Registry.getCurrentTenant().getTenantID());
    }
}

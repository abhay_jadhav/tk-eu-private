package com.thyssenkrupp.b2b.eu.core.process.actions;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.model.process.TkEuB2bRegistrationProcessModel;

/**
 *
 */
public class TkEuSendEmailAction extends AbstractProceduralAction {

    public static final String RETRY_DELAY = "email.retry.later.delay";
    private static final Logger LOG = LoggerFactory.getLogger(TkEuSendEmailAction.class);
    private EmailService emailService;
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    @Resource(name = "modelService")
    private ModelService modelService;

    @Override
    public void executeAction(final de.hybris.platform.processengine.model.BusinessProcessModel businessProcessModel) throws RetryLaterException {
        for (final EmailMessageModel email : businessProcessModel.getEmails()) {

            if (email != null && CollectionUtils.isEmpty(email.getToAddresses())) {
                setEmailAddresses(businessProcessModel, email);
            }
            final boolean status = getEmailService().send(email);
            if (!status) {
                final RetryLaterException ex = new RetryLaterException("Retry Later Again");
                final Long delay = Long.parseLong(getConfigurationService().getConfiguration().getString(RETRY_DELAY));
                ex.setDelay(delay);
                LOG.debug("Retry Trigger after every ={} milliseconds", delay);
                throw ex;
            }
        }
    }

    /**
     *
     */
    private void setEmailAddresses(final de.hybris.platform.processengine.model.BusinessProcessModel businessProcessModel, final EmailMessageModel email) {
        final List<EmailAddressModel> toEmails = new ArrayList<>();
        EmailAddressModel addModel = new EmailAddressModel();
        if (businessProcessModel instanceof OrderProcessModel) {
            final CustomerModel user=(CustomerModel) ((OrderProcessModel) businessProcessModel).getOrder().getUser();
            addModel = emailService.getOrCreateEmailAddressForEmail(user.getContactEmail(), user.getDisplayName());
        }
        if (businessProcessModel instanceof TkEuB2bRegistrationProcessModel) {
            final CustomerModel customer = ((TkEuB2bRegistrationProcessModel) businessProcessModel).getCustomer();
            addModel = emailService.getOrCreateEmailAddressForEmail(customer.getContactEmail(), customer.getDisplayName());
        }
        toEmails.add(addModel);
        email.setToAddresses(toEmails);
        modelService.save(email);
    }

    protected EmailService getEmailService() {
        return emailService;
    }

    @Required
    public void setEmailService(final EmailService emailService) {
        this.emailService = emailService;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @return the modelService
     */
    @Override
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

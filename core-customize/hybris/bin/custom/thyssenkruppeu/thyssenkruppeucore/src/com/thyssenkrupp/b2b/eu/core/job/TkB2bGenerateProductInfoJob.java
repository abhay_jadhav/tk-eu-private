package com.thyssenkrupp.b2b.eu.core.job;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.dao.TkProductDao;
import com.thyssenkrupp.b2b.eu.core.enums.ProductLifeCycleStatus;
import com.thyssenkrupp.b2b.eu.core.model.TkB2bGenerateProductInfoCronJobModel;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCatalogProductReplicationConfigService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bCategoryService;
import com.thyssenkrupp.b2b.eu.core.service.TkB2bProductService;

public class TkB2bGenerateProductInfoJob extends AbstractJobPerformable<TkB2bGenerateProductInfoCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkB2bGenerateProductInfoJob.class);
    private ConfigurationService configurationService;
    private TkProductDao productDao;
    private TkEuB2bCategoryDao tkCategoryDao;

    private TkB2bProductService tkB2bProductService;
    private TkB2bCategoryService tkB2bCategoryService;
    private TkB2bCatalogProductReplicationConfigService productReplicationConfigService;

    @Override
    public PerformResult perform(final TkB2bGenerateProductInfoCronJobModel job) {
        try {
            LOG.info("Started Generate Product Info CronJob");
            final CMSSiteModel cmsSiteModel = job.getContentSite();
            final List<BaseStoreModel> stores = cmsSiteModel.getStores();
            final BaseStoreModel currentBaseStore = stores != null ? stores.get(0) : null;
            if (currentBaseStore != null && currentBaseStore.getSAPConfiguration() != null) {
                final String salesOrganization = currentBaseStore.getSAPConfiguration().getSapcommon_salesOrganization();
                LOG.info("Fetching Product Catalog Version for salesOrganization: " + salesOrganization);
                final CatalogVersionModel catalogVersion = tkCategoryDao.fetchProductCatalogBySalesOrganization(salesOrganization);
                if (catalogVersion != null) {
                    LOG.info("Fetching products for " + catalogVersion.getCatalog().getName());
                    final List<ProductModel> productModels = productDao.findProductsByCatalogAndLifeCycleStatus(catalogVersion, ProductLifeCycleStatus.TK_040_READY_FOR_PRODUCTINFO_GENERATION);
                    updateProductInfo(productModels, currentBaseStore);
                } else {
                    LOG.warn("CatalogVersion is Null-Products cannot be updated");
                }
            } else {
                LOG.warn("BaseStore is Null-Products cannot be updated");
            }
            LOG.info("Finished Generate Product Info CronJob");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while Updating Products", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void updateProductInfo(final List<ProductModel> productModels, final BaseStoreModel currentBaseStore) {
        for (final ProductModel product : productModels) {
            final CategoryModel classificationClassCategory = tkB2bProductService.getClassificationCategoryForProduct(product);
            if (tkB2bProductService.isClassificationCategoryEmpty(product)) {
                continue;
            }
            final Collection<VariantProductModel> variantProductModels = product.getVariants();
            if (CollectionUtils.isNotEmpty(variantProductModels)) {

                final Optional<String> maybeCategoryRules = getProductReplicationConfigService().getVariantGenerationRuleOrDefault(product.getCatalogVersion().getCatalog(), classificationClassCategory.getCode());
                if (maybeCategoryRules.isPresent()) {
                    populateProductInfo(variantProductModels, currentBaseStore);
                    product.setLifeCycleStatus(ProductLifeCycleStatus.TK_050_COMPLETE);
                    LOG.info(" Category Rule configuration found for " + classificationClassCategory.getCode());
                    modelService.save(product);
                    modelService.refresh(product);
                } else {
                    LOG.warn(" No Category Rule configuration found for" + classificationClassCategory.getCode() + ", Skipping ProductInfo Creation");
                }
            }
        }
    }

    private void populateProductInfo(final Collection<VariantProductModel> variants, final BaseStoreModel currentBaseStore) {
        LOG.debug("Updating Products...");
        final List<LanguageModel> baseStoreLaguages = getBaseStoreSupportLanguages(currentBaseStore).stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (final VariantProductModel variant : variants) {
            if (variant != null) {
                tkB2bCategoryService.updateVariantInfo(variant, baseStoreLaguages);
            }
        }
    }

    private Set<LanguageModel> getBaseStoreSupportLanguages(final BaseStoreModel currentBaseStore) {
        final Set<LanguageModel> collectLanguages = new HashSet<>();

        currentBaseStore.getLanguages().stream().filter(Objects::nonNull).forEach(c -> {
            collectLanguages.add(c);
            collectLanguages.addAll(c.getFallbackLanguages());
        });
        return collectLanguages;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    protected TkProductDao getProductDao() {
        return productDao;
    }

    @Required
    public void setProductDao(final TkProductDao productDao) {
        this.productDao = productDao;
    }

    public TkEuB2bCategoryDao getTkCategoryDao() {
        return tkCategoryDao;
    }

    @Required
    public void setTkCategoryDao(final TkEuB2bCategoryDao tkCategoryDao) {
        this.tkCategoryDao = tkCategoryDao;
    }

    public TkB2bProductService getTkB2bProductService() {
        return tkB2bProductService;
    }

    public void setTkB2bProductService(final TkB2bProductService tkB2bProductService) {
        this.tkB2bProductService = tkB2bProductService;
    }

    public TkB2bCategoryService getTkB2bCategoryService() {
        return tkB2bCategoryService;
    }

    public void setTkB2bCategoryService(final TkB2bCategoryService tkB2bCategoryService) {
        this.tkB2bCategoryService = tkB2bCategoryService;
    }

    public TkB2bCatalogProductReplicationConfigService getProductReplicationConfigService() {
        return productReplicationConfigService;
    }

    @Required
    public void setProductReplicationConfigService(final TkB2bCatalogProductReplicationConfigService productReplicationConfigService) {
        this.productReplicationConfigService = productReplicationConfigService;
    }
}

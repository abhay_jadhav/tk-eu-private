package com.thyssenkrupp.b2b.eu.core.cronjob;

import static de.hybris.platform.cronjob.enums.CronJobResult.ERROR;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.ABORTED;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.outbound.SendToDataHubHelper;
import de.hybris.platform.sap.orderexchange.outbound.SendToDataHubResult;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thyssenkrupp.b2b.eu.core.model.TkEuExportOrderCronJobModel;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bOrderService;

public class TkEuExportOrderJob extends AbstractJobPerformable<TkEuExportOrderCronJobModel> {

    private static final Logger LOG = Logger.getLogger(TkEuExportOrderJob.class);
    private static final String EXPORT_ORDER_COUNT = "tk.export.order.count";

    private TkEuB2bOrderService tkEuB2bOrderService;
    private SendToDataHubHelper<OrderModel> sendOrderToDataHubHelper;
    private ModelService modelService;
    private ConfigurationService configurationService;

    @Override
    public PerformResult perform(final TkEuExportOrderCronJobModel cronJobModel) {
        try {
            LOG.info("Start Export order cron job");

            final int orderCount = getConfigurationService().getConfiguration().getInt(EXPORT_ORDER_COUNT);

            final List<OrderModel> orderList = tkEuB2bOrderService.getOrdersToExport(orderCount, cronJobModel.getGracePeriod());
            exportOrderToErp(orderList);
            LOG.info("Finished Export order cron job ");
            return new PerformResult(SUCCESS, FINISHED);
        } catch (final ConversionException ce) {
            LOG.error("The value given for tk.export.order.count in the configuration file is not valid", ce);
            return new PerformResult(ERROR, ABORTED);
        } catch (final Exception e) {
            LOG.error("Exception occurred while export order cron job ", e);
            return new PerformResult(ERROR, ABORTED);
        }
    }

    private void exportOrderToErp(final List<OrderModel> orderList) {
        for (final OrderModel order : orderList) {
            LOG.info("Start Export order :" + order.getCode());
            try {
                final SendToDataHubResult result = getSendOrderToDataHubHelper().createAndSendRawItem(order);
                if (result.isSuccess()) {
                    setOrderStatus(order, ExportStatus.EXPORTED);
                } else {
                    setOrderStatus(order, ExportStatus.NOTEXPORTED);
                }
                LOG.info("End export Order code : " + order.getCode() + "Export status : " + result.isSuccess());
            } catch (final Exception e) {
                LOG.error("Exception occurred while export order : " + order.getCode(), e);
                continue;
            }
        }
    }

    protected void setOrderStatus(final OrderModel order, final ExportStatus exportStatus) {
        order.setExportStatus(exportStatus);
        getModelService().save(order);
    }

    public TkEuB2bOrderService getTkEuB2bOrderService() {
        return tkEuB2bOrderService;
    }

    public void setTkEuB2bOrderService(final TkEuB2bOrderService tkEuB2bOrderService) {
        this.tkEuB2bOrderService = tkEuB2bOrderService;
    }

    public SendToDataHubHelper<OrderModel> getSendOrderToDataHubHelper() {
        return sendOrderToDataHubHelper;
    }

    public void setSendOrderToDataHubHelper(final SendToDataHubHelper<OrderModel> sendOrderToDataHubHelper) {
        this.sendOrderToDataHubHelper = sendOrderToDataHubHelper;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

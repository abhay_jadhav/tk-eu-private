package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;

import java.util.Set;

public interface TkEuB2bUnitService {
    Set<B2BUnitModel> getCurrentAndParentB2BUnits(B2BCustomerModel b2bCustomer);

    B2BUnitModel getUnitForUid(String uid);
}

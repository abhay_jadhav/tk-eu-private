package com.thyssenkrupp.b2b.eu.core.service.impl;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.C2L_CONFIGURATION_CATEGORY;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_PIECE_TYPE_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.TK_EU_CATEGORY_CATALOG_ID;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TEXTFIELD;
import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.category.model.ConfigurationCategoryModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.sap.sapmodel.services.SAPUnitService;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguratorSettingModel;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;

public class TradeLengthUomConversionHelper {

    private CatalogVersionService catalogVersionService;
    private CategoryService       categoryService;
    private TkEuB2bProductService b2bProductService;
    private SAPUnitService        unitService;
    private String                productWidthFeatureCode;

    public static final boolean isValidTradeLengthSupportedUnit(final String unitCode) {
        return equalsIgnoreCase(SAP_PIECE_TYPE_CODE, unitCode);
    }

    public final List<String> getC2lSupportedUnits() {
        final CatalogVersionModel storeCatalog = getOnlineStoreCatalogVersion();
        final CategoryModel c2lCategory = getC2lCategory(storeCatalog);
        if (c2lCategory != null && c2lCategory instanceof ConfigurationCategoryModel) {
            final ConfigurationCategoryModel configurationCategory = (ConfigurationCategoryModel) c2lCategory;
            final List<AbstractConfiguratorSettingModel> configuratorSettings = configurationCategory.getConfiguratorSettings();
            return getC2lUnitsFromSettings(configuratorSettings);
        }
        return emptyList();
    }

    private List<String> getC2lUnitsFromSettings(final List<AbstractConfiguratorSettingModel> configuratorSettings) {
        final Optional<TextFieldConfiguratorSettingModel> textFieldConfiguratorSettingModel =
            emptyIfNull(configuratorSettings).stream()
                .filter(isTextFieldConfigurationSettings())
                .map(TextFieldConfiguratorSettingModel.class::cast).findFirst();
        final String commaSeparatedUnits = textFieldConfiguratorSettingModel.map(TextFieldConfiguratorSettingModel::getConfigurationValue).orElse("");
        if (isNotEmpty(commaSeparatedUnits)) {
            return Arrays.asList(commaSeparatedUnits.split("\\s*,\\s*"));
        }
        return emptyList();
    }

    public Optional<MaterialQuantityWrapper> getProductWidth(@NotNull final ProductModel productModel) {
        final Optional<FeatureValue> productWidthFeatureValue = getB2bProductService().getProductWidth(productModel, getProductWidthFeatureCode());
        return productWidthFeatureValue.map(mapMaterialWidth());
    }

    private Function<FeatureValue, MaterialQuantityWrapper> mapMaterialWidth() {
        return featureValue -> {
            final double value = featureValue.getValue() != null ? (Double) featureValue.getValue() : 0;
            final ClassificationAttributeUnitModel unit = featureValue.getUnit();
            final UnitModel unitForSAPCode = getUnitService().getUnitForSAPCode(unit.getSapCode());
            return MaterialQuantityWrapperBuilder.builder().withUnit(unitForSAPCode).withQuantity(value).build();
        };
    }

    private Predicate<AbstractConfiguratorSettingModel> isTextFieldConfigurationSettings() {
        return abstractConfiguratorSettingModel -> abstractConfiguratorSettingModel.getConfiguratorType() == TEXTFIELD;
    }

    private CategoryModel getC2lCategory(final CatalogVersionModel storeCatalog) {
        return getCategoryService().getCategoryForCode(storeCatalog, C2L_CONFIGURATION_CATEGORY);
    }

    private CatalogVersionModel getOnlineStoreCatalogVersion() {
        return getCatalogVersionService().getCatalogVersion(TK_EU_CATEGORY_CATALOG_ID, CatalogManager.ONLINE_VERSION);
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public TkEuB2bProductService getB2bProductService() {
        return b2bProductService;
    }

    public SAPUnitService getUnitService() {
        return unitService;
    }

    public String getProductWidthFeatureCode() {
        return productWidthFeatureCode;
    }

    @Required
    public void setProductWidthFeatureCode(final String productWidthFeatureCode) {
        this.productWidthFeatureCode = productWidthFeatureCode;
    }

    @Required
    public void setUnitService(final SAPUnitService unitService) {
        this.unitService = unitService;
    }

    @Required
    public void setB2bProductService(final TkEuB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}

package com.thyssenkrupp.b2b.eu.core.service;

import java.math.BigDecimal;

/**
 *
 */
public interface TkEuConfigKeyValueService {
    String getProperty(String key);

    double getDouble(String key);

    boolean getBoolean(String key);

    BigDecimal getBigDecimal(String key);

}

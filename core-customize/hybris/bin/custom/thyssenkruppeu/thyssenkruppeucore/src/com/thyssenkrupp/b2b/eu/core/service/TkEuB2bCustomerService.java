package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Optional;

import com.thyssenkrupp.b2b.eu.core.enums.B2bRegistrationStatus;

public interface TkEuB2bCustomerService<U, C> extends B2BCustomerService<U, C> {

    SearchPageData<U> getPagedCustomersByRegistrationStatus(PageableData pageableData, B2bRegistrationStatus statusCode);

    Optional<B2BUnitModel> getCurrentB2bUnit();

    <T extends UserModel> T getAllUserForUID(String var1, Class<T> var2);

    SearchPageData<U> getPagedCustomersByAllRegistrationStatus(PageableData pageableData);

    <T extends UserModel> T getAllUserAllStatusesForUID(String var1, Class<T> var2);
}

package com.thyssenkrupp.b2b.eu.core.atp.logging;

import com.netflix.hystrix.HystrixCircuitBreaker;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixEventType;
import com.netflix.hystrix.metric.HystrixCommandCompletion;
import com.netflix.hystrix.metric.HystrixCommandCompletionStream;
import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;
import org.apache.log4j.Logger;
import rx.Observable;

import javax.annotation.PostConstruct;

public class HystrixEventLogsPrinter {

    private static final Logger LOG = Logger.getLogger(HystrixEventLogsPrinter.class);

    @PostConstruct
    public void init() throws Exception {
        HystrixCommandCompletionStream commandCompletionStream = HystrixCommandCompletionStream.getInstance(HystrixCommandKey.Factory.asKey("callAtpCheck"));
        Observable<HystrixCommandCompletion> commandCompletionObservable = commandCompletionStream.observe();

        commandCompletionObservable.subscribe(
            HystrixEventLogsPrinter::printOnNextResult,
            error -> LOG.debug("error during printOnNextResult"),
            () -> LOG.debug("ATP Hystrix Log printed successfully")
        );
    }

    private static void printOnNextResult(HystrixCommandCompletion e) {
        boolean isCircuitOpen = HystrixCircuitBreaker.Factory.getInstance(HystrixCommandKey.Factory.asKey(ThyssenkruppeucoreConstants.SAP_ATP_COMMAND_KEY)).isOpen();
        LOG.debug(e + " Circuit open status:" + isCircuitOpen
            + ", Latency:" + e.getTotalLatency()
            + ", ThreadPoolRejected:" + e.isResponseThreadPoolRejected()
            + ", Success:" + (e.getEventCounts().getCount(HystrixEventType.SUCCESS) > 0 ? true : false)
            + ", Failure:" + e.getEventCounts().getCount(HystrixEventType.FAILURE));
    }
}

package com.thyssenkrupp.b2b.eu.core.exception;

public class TokenExpiredException extends Exception {

    public TokenExpiredException() {
        super();
    }

    public TokenExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TokenExpiredException(final String message) {
        super(message);
    }

    public TokenExpiredException(final Throwable cause) {
        super(cause);
    }
}

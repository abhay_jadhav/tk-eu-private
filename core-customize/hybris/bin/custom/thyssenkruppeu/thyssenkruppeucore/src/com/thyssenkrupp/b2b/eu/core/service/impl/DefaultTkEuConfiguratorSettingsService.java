package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.service.MaterialShape;
import com.thyssenkrupp.b2b.eu.core.service.ProductLengthConfigurationParam;
import com.thyssenkrupp.b2b.eu.core.service.TkEuB2bProductService;
import com.thyssenkrupp.b2b.eu.core.service.TkEuConfiguratorSettingsService;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.core.service.TkUomConversionService;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkCutToLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkLengthConfiguredProductInfoModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguratorSettingsModel;
import com.thyssenkrupp.b2b.global.baseshop.thyssenkruppconfigurableproductsaddon.model.TkTradeLengthConfiguredProductInfoModel;
import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_MTK_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants.SAP_MTR_UNIT_CODE;
import static com.thyssenkrupp.b2b.eu.core.service.MaterialShape.FLAT;
import static de.hybris.platform.catalog.enums.ConfiguratorType.TKTRADELENGTH;
import static java.util.Optional.of;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

public class DefaultTkEuConfiguratorSettingsService extends DefaultTkB2bConfiguratorSettingsService implements TkEuConfiguratorSettingsService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuConfiguratorSettingsService.class);

    private UnitService                    unitService;
    private TkEuB2bProductService          b2bProductService;
    private TradeLengthUomConversionHelper tradeLengthUomConversionHelper;
    private TkUomConversionService         uomConversionService;

    @Nullable
    @Override
    public TkTradeLengthConfiguratorSettingsModel getSelectedTradeLengthConfigurationForProduct(final ProductModel productModel) {
        final List<AbstractConfiguratorSettingModel> settings = getConfiguratorSettingsForProduct(productModel);
        if (CollectionUtils.isNotEmpty(settings)) {
            for (final AbstractConfiguratorSettingModel setting : settings) {
                if (TKTRADELENGTH.equals(setting.getConfiguratorType())) {
                    final TkTradeLengthConfiguratorSettingsModel tkTradeLengthConfiguratorSettingsModel = (TkTradeLengthConfiguratorSettingsModel) setting;
                    if (tkTradeLengthConfiguratorSettingsModel.isChecked()) {
                        LOG.debug("Trade Length Configured for Product: " + productModel.getCode() + " " + tkTradeLengthConfiguratorSettingsModel.getLengthValue());
                        return tkTradeLengthConfiguratorSettingsModel;
                    }
                }
            }
        }
        LOG.debug("No Trade Lengths Configured for Product: " + productModel.getCode());
        return null;
    }

    @Override
    public Optional<ProductLengthConfigurationParam> getProductLengthInfoForOrderEntry(@NotNull AbstractOrderEntryModel entryModel) {
        final ProductModel productModel = entryModel.getProduct();
        final Optional<MaterialShape> productShape = getB2bProductService().getProductShape(productModel);
        List<AbstractOrderEntryProductInfoModel> productInfos = entryModel.getProductInfos();
        if (productInfos != null) {
            if (of(FLAT).equals(productShape)) {
                return getConfigLengthProdcutInfoFromOrderEntry(productInfos, productModel);
            } else {
                for (AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel : productInfos) {
                    if (isValidTradeLengthProductInfo(abstractOrderEntryProductInfoModel)) {
                        return createProductLengthConfigurationParam((TkTradeLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel);
                    } else if (isValidCutToLengthProductInfo(abstractOrderEntryProductInfoModel)) {
                        return createProductLengthConfigurationParam((TkCutToLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel);
                    }
                }
            }
        }
        return Optional.empty();
    }

    private Optional<ProductLengthConfigurationParam> getConfigLengthProdcutInfoFromOrderEntry(List<AbstractOrderEntryProductInfoModel> productInfos, ProductModel productModel) {
        for (AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel : productInfos) {

            if (isValidCCLengthProductInfo(abstractOrderEntryProductInfoModel)) {
                return createProductLengthConfigurationParamForFlatProducts((TkLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel, productModel);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean hasValidLengthConfigurationSetting(@NotNull AbstractOrderEntryModel entryModel) {
        Collection<AbstractOrderEntryProductInfoModel> productInfos = emptyIfNull(entryModel.getProductInfos());
        if (productInfos.stream().noneMatch(this::isValidCutToLengthProductInfo)) {
            return productInfos.stream().anyMatch(this::isValidTradeLengthProductInfo);
        }
        return true;
    }

    @Override
    public boolean hasValidTradeLengthConfigurationSetting(@NotNull AbstractOrderEntryModel entryModel) {
        Collection<AbstractOrderEntryProductInfoModel> productInfos = emptyIfNull(entryModel.getProductInfos());
        return productInfos.stream().anyMatch(this::isValidTradeLengthProductInfo);
    }

    @Override
    public boolean hasValidC2lConfigurationSetting(@NotNull AbstractOrderEntryModel entryModel) {
        Collection<AbstractOrderEntryProductInfoModel> productInfos = emptyIfNull(entryModel.getProductInfos());
        return productInfos.stream().anyMatch(this::isValidCutToLengthProductInfo);
    }

    protected Optional<ProductLengthConfigurationParam> createProductLengthConfigurationParam(TkCutToLengthConfiguredProductInfoModel abstractOrderEntryProductInfoModel) {
        UnitModel lengthUnit = abstractOrderEntryProductInfoModel.getUnit();
        UnitModel unitMtr = getUnitService().getUnitForCode(SAP_MTR_UNIT_CODE);
        Double lengthValueInMtr = abstractOrderEntryProductInfoModel.getValue() * lengthUnit.getConversion();
        ProductLengthConfigurationParam productLengthConfigurationParam = buildProductLengthConfigurationParam(unitMtr, lengthValueInMtr, abstractOrderEntryProductInfoModel.getConfiguratorType());
        return of(productLengthConfigurationParam);
    }

    protected Optional<ProductLengthConfigurationParam> createProductLengthConfigurationParamForFlatProducts(final TkLengthConfiguredProductInfoModel abstractOrderEntryProductInfo, ProductModel productModel) {
        final Optional<MaterialQuantityWrapper> productWidth = getTradeLengthUomConversionHelper().getProductWidth(productModel);
        return productWidth.map(mapProductSurfaceArea(abstractOrderEntryProductInfo));
    }

    private Function<MaterialQuantityWrapper, ProductLengthConfigurationParam> mapProductSurfaceArea(final TkLengthConfiguredProductInfoModel abstractOrderEntryProductInfo) {
        return materialQuantityWrapper -> {
            double surfaceArea = materialQuantityWrapper.getQuantity() * materialQuantityWrapper.getUnitModel().getConversion() * abstractOrderEntryProductInfo.getLengthValue() * abstractOrderEntryProductInfo.getUnit().getConversion();
            UnitModel unitMtk = getUnitService().getUnitForCode(SAP_MTK_UNIT_CODE);
            return buildProductLengthConfigurationParam(unitMtk, surfaceArea, abstractOrderEntryProductInfo.getConfiguratorType());
        };
    }

    protected Optional<ProductLengthConfigurationParam> createProductLengthConfigurationParam(TkTradeLengthConfiguredProductInfoModel abstractOrderEntryProductInfo) {
        ProductLengthConfigurationParam productLengthConfigurationParam = buildProductLengthConfigurationParam(abstractOrderEntryProductInfo.getUnit(), abstractOrderEntryProductInfo.getLengthValue(), abstractOrderEntryProductInfo.getConfiguratorType());
        return of(productLengthConfigurationParam);
    }

    private boolean isValidCCLengthProductInfo(AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel) {
        return ConfiguratorType.TKCCLENGTH == abstractOrderEntryProductInfoModel.getConfiguratorType();
    }

    private ProductLengthConfigurationParam buildProductLengthConfigurationParam(UnitModel unitMtr, Double lengthValueInMtr, ConfiguratorType configuratorType) {
        ProductLengthConfigurationParam productLengthConfigurationParam = new ProductLengthConfigurationParam();
        productLengthConfigurationParam.setUnit(unitMtr);
        productLengthConfigurationParam.setLengthValue(lengthValueInMtr);
        productLengthConfigurationParam.setConfigurationType(configuratorType);
        return productLengthConfigurationParam;
    }

    private boolean isValidCutToLengthProductInfo(AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel) {
        return abstractOrderEntryProductInfoModel instanceof TkCutToLengthConfiguredProductInfoModel && BooleanUtils.isTrue(((TkCutToLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).getChecked());
    }

    private boolean isValidTradeLengthProductInfo(AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel) {
        return abstractOrderEntryProductInfoModel instanceof TkTradeLengthConfiguredProductInfoModel && BooleanUtils.isTrue(((TkTradeLengthConfiguredProductInfoModel) abstractOrderEntryProductInfoModel).isChecked());
    }

    public TkUomConversionService getUomConversionService() {
        return uomConversionService;
    }

    @Required
    public void setUomConversionService(TkUomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    public TradeLengthUomConversionHelper getTradeLengthUomConversionHelper() {
        return tradeLengthUomConversionHelper;
    }

    @Required
    public void setTradeLengthUomConversionHelper(TradeLengthUomConversionHelper tradeLengthUomConversionHelper) {
        this.tradeLengthUomConversionHelper = tradeLengthUomConversionHelper;
    }

    public UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    public TkEuB2bProductService getB2bProductService() {
        return b2bProductService;
    }

    @Required
    public void setB2bProductService(TkEuB2bProductService b2bProductService) {
        this.b2bProductService = b2bProductService;
    }
}

package com.thyssenkrupp.b2b.eu.core.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.dynamic.ProductEurope1DiscountsAttributeHandler;
import de.hybris.platform.europe1.model.DiscountRowModel;

import java.util.Collection;

public class TkEuProductEurope1DiscountsAttributeHandler extends ProductEurope1DiscountsAttributeHandler {

    @Override
    public Collection<DiscountRowModel> get(ProductModel model) {
        return super.getOwnPdtRowModels(model);
    }
}

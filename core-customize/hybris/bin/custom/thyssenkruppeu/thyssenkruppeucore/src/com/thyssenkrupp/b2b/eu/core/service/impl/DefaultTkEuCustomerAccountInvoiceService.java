package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountInvoiceDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountInvoiceService;
import com.thyssenkrupp.b2b.global.baseshop.core.model.InvoiceModel;

public class DefaultTkEuCustomerAccountInvoiceService implements TkEuCustomerAccountInvoiceService {

    private TkEuCustomerAccountInvoiceDao customerAccountInvoiceDao;
    private BaseStoreService baseStoreService;

    @Override
    public SearchPageData<InvoiceModel> orderInvoiceSearchList(final String searchKey, final PageableData pageableData, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(b2BUnitModel, "B2BUnit model cannot be null");
        validateParameterNotNull(pageableData, "PageableData must not be null");
        return getCustomerAccountInvoiceDao().orderInvoiceSearchList(searchKey, pageableData, b2BUnitModel, getBaseStoreService().getCurrentBaseStore());
    }

    @Override
    public int orderInvoicesSearchListCount(final String searchKey, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(b2BUnitModel, "B2BUnit model cannot be null");
        return getCustomerAccountInvoiceDao().orderInvoicesSearchListCount(searchKey, b2BUnitModel, getBaseStoreService().getCurrentBaseStore());
    }

    @Override
    public List<InvoiceModel> fetchInvoices(final String invoiceNo, final Set<B2BUnitModel> b2BUnitModel) {
        validateParameterNotNull(invoiceNo, "Delivery number cannot be null");
        return getCustomerAccountInvoiceDao().fetchInvoices(invoiceNo, b2BUnitModel,getBaseStoreService().getCurrentBaseStore());
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuCustomerAccountInvoiceDao getCustomerAccountInvoiceDao() {
        return customerAccountInvoiceDao;
    }

    @Required
    public void setCustomerAccountInvoiceDao(final TkEuCustomerAccountInvoiceDao customerAccountInvoiceDao) {
        this.customerAccountInvoiceDao = customerAccountInvoiceDao;
    }
}

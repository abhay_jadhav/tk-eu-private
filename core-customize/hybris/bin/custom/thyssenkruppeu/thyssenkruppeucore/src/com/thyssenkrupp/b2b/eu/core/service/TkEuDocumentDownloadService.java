package com.thyssenkrupp.b2b.eu.core.service;

import com.thyssenkrupp.b2b.eu.core.enums.DocumentDownloadServiceEnum;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;

public interface TkEuDocumentDownloadService {

    Object getDocumentStatus(DocumentDownloadRequest documentRequest);

    GetDocumentContentResponse getDocument(DocumentDownloadRequest documentRequest);

    DocumentDownloadServiceEnum getType();

    boolean isAuthorizedUser(DocumentDownloadRequest documents);
}

package com.thyssenkrupp.b2b.eu.core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class TkEuAtpDateUtils {

    private TkEuAtpDateUtils() {
        throw new UnsupportedOperationException();
    }

    public static Date findNextWorkingDayFrom(int daysToAdd) {
        Calendar calendar = Calendar.getInstance();

        for (int i = 0; i < daysToAdd; ) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            if (!isWeekEnd(calendar)) {
                i++;
            }
        }
        return calendar.getTime();
    }

    public static String convertDateToStringDateMonthYear(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String convertedDate = date == null ? null : simpleDateFormat.format(date);
        return convertedDate;
    }

    private static boolean isWeekEnd(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }
}

package com.thyssenkrupp.b2b.eu.core.service;

import java.util.Collection;

public interface TkSynchronizationService {

    void syncCatalogAndIndexForShops(Collection<String> shopNames);

    void runSolrIndex(String storeName);

    void synchronizeContentCatalog(String catalogName);

    void synchronizeProductCatalog(String catalogName);

    void synchronizeCategoryCatalog(String catalogName);
}

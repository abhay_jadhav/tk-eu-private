package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Date;

import org.assertj.core.util.Preconditions;

import com.thyssenkrupp.b2b.eu.core.service.TkB2bCustomerAccountService;

public class DefaultTkB2bCustomerAccountService extends DefaultB2BCustomerAccountService implements TkB2bCustomerAccountService {

    private static final String PASSWORD_EMAIL_TOKEN_VALIDITY = "b2b.customer.password.token.validity.seconds"; //NOSONAR
    private long                 passwordEmailTokenValidityInSeconds;
    private ConfigurationService configurationService;

    @Override
    public boolean validatePasswordToken(final SecureToken data) {
        final long deltaInMillis = new Date().getTime() - data.getTimeStamp();
        if (getPasswordEmailTokenValidityInSeconds() < 1L || deltaInMillis / 1000 > getPasswordEmailTokenValidityInSeconds()) {
            return false;
        }
        return true;
    }

    @Override
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Override
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public long getPasswordEmailTokenValidityInSeconds() {
        passwordEmailTokenValidityInSeconds = getConfigurationService().getConfiguration().getInt(PASSWORD_EMAIL_TOKEN_VALIDITY); //NOSONAR
        Preconditions.checkArgument(this.passwordEmailTokenValidityInSeconds >= 0, "tokenValidityInSeconds (" + this.passwordEmailTokenValidityInSeconds + ") < 0");
        return passwordEmailTokenValidityInSeconds;
    }
}

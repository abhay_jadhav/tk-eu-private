package com.thyssenkrupp.b2b.eu.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDeliveryDao;
import com.thyssenkrupp.b2b.eu.core.service.TkEuCustomerAccountDeliveryService;

public class DefaultTkEuCustomerAccountDeliveryService implements TkEuCustomerAccountDeliveryService {

    private static final Logger LOG = Logger.getLogger(DefaultTkEuCustomerAccountDeliveryService.class);

    private TkEuCustomerAccountDeliveryDao customerAccountDeliveryDao;
    private BaseStoreService baseStoreService;
    private DefaultTkEuB2BUnitService b2bUnitService;
    private UserService userService;

    @Override
    public SearchPageData<ConsignmentModel> orderDeliveriesSearchList(final String searchKey, final PageableData pageableData, final Set<B2BUnitModel> b2bUnits) {
        validateParameterNotNull(b2bUnits, "B2BUnit cannot be null");
        validateParameterNotNull(pageableData, "PageableData must not be null");
        return getCustomerAccountDeliveryDao().orderDeliveriesSearchList(searchKey, pageableData, b2bUnits, getBaseStoreService().getCurrentBaseStore());
    }

    @Override
    public int orderDeliveriesSearchListCount(final String searchKey, final Set<B2BUnitModel> b2bUnits) {
        validateParameterNotNull(b2bUnits, "B2BUnit cannot be null");
        return getCustomerAccountDeliveryDao().orderDeliveriesSearchListCount(searchKey, b2bUnits, getBaseStoreService().getCurrentBaseStore());
    }

    @Override
    public List<ConsignmentModel> fetchDeliveries(final String deliveryNo, final Set<B2BUnitModel> b2BUnits) {
        validateParameterNotNull(deliveryNo, "Delivery number cannot be null");
        return getCustomerAccountDeliveryDao().fetchDeliveries(deliveryNo, b2BUnits, getBaseStoreService().getCurrentBaseStore());
    }

    public BaseStoreService getBaseStoreService() {
        return baseStoreService;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    public TkEuCustomerAccountDeliveryDao getCustomerAccountDeliveryDao() {
        return customerAccountDeliveryDao;
    }

    @Required
    public void setCustomerAccountDeliveryDao(final TkEuCustomerAccountDeliveryDao customerAccountDeliveryDao) {
        this.customerAccountDeliveryDao = customerAccountDeliveryDao;
    }

    @Override
    public List<ConsignmentEntryModel> fetchDeliveriesForOrderEntry(final String sapOrderId, final int entryNumber) {
        try {
            validateParameterNotNull(sapOrderId, "Cannot be null");
            final Set<B2BUnitModel> b2bUnits = getB2bUnitService().getCurrentAndParentB2BUnits((B2BCustomerModel) getUserService().getCurrentUser());
            return getCustomerAccountDeliveryDao().fetchDeliveriesForOrderEntry(sapOrderId, entryNumber, b2bUnits, getBaseStoreService().getCurrentBaseStore());
        } catch (final Exception e) {
            LOG.error("Exception in Fetching Consignments for " + sapOrderId + ": " + entryNumber + " :" + e.getMessage());
            return Collections.emptyList();
        }
    }

    public DefaultTkEuB2BUnitService getB2bUnitService() {
        return b2bUnitService;
    }

    public void setB2bUnitService(final DefaultTkEuB2BUnitService b2bUnitService) {
        this.b2bUnitService = b2bUnitService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }
}

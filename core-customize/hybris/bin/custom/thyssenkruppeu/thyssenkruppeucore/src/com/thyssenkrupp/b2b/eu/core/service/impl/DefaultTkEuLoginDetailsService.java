package com.thyssenkrupp.b2b.eu.core.service.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;

import org.apache.log4j.Logger;

import com.thyssenkrupp.b2b.eu.core.enums.LoginType;
import com.thyssenkrupp.b2b.eu.core.service.TkEuLoginDetailsService;

/**
 *
 */
public class DefaultTkEuLoginDetailsService implements TkEuLoginDetailsService {
    private static final Logger LOG = Logger.getLogger(DefaultTkEuLoginDetailsService.class);
    private ModelService modelService;
    private UserService userService;

    @Override
    public void setLoginDetails(final String customerId, final LoginType loginType) {
        final UserModel user;
        if (loginType.equals(LoginType.ASM)) {
            user = getUserService().getUserForUID(customerId);
        } else {
            user = getUserService().getCurrentUser();
        }
        user.setLoginBy(loginType);
        user.setLastLogin(new Date());
        LOG.debug("User uid: " + user.getUid());
        LOG.debug("User Login Type : " + user.getLoginBy());
        getModelService().save(user);
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

}

package com.thyssenkrupp.b2b.eu.core.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

public interface TkEuB2bOrderService {

    List<OrderModel> getOrdersToExport(int exportOrderCount, Long gracetime);

    OrderModel getInitialOrderForCode(String orderCode);

}

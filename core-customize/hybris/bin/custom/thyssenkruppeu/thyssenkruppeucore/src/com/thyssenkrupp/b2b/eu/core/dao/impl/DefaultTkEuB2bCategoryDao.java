package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.sap.sapmodel.model.SAPProductSalesAreaToCatalogMappingModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuB2bCategoryDao;
import com.thyssenkrupp.b2b.eu.core.constants.ThyssenkruppeucoreConstants;

public class DefaultTkEuB2bCategoryDao extends DefaultCategoryDao implements TkEuB2bCategoryDao {

    @Override
    public Collection<VariantValueCategoryModel> findCategoriesBySuperCategory(final VariantCategoryModel parentCategory, final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(parentCategory, "parentCategory must not be null!");
        validateParameterNotNull(catalogVersion, "catalogVersion must not be null!");
        final StringBuilder query = new StringBuilder("SELECT {").append(VariantValueCategoryModel.PK);
        query.append("} FROM {").append(VariantValueCategoryModel._TYPECODE);
        query.append("} WHERE {").append(VariantValueCategoryModel.CODE);
        query.append("} LIKE ?").append(VariantValueCategoryModel.CODE).append(" AND {");
        query.append(VariantValueCategoryModel.CATALOGVERSION).append("}=?").append(VariantValueCategoryModel.CATALOGVERSION);
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(VariantValueCategoryModel.CODE, buildVariantValueCategoryCode(parentCategory.getCode()));
        params.put(VariantValueCategoryModel.CATALOGVERSION, catalogVersion);
        final SearchResult<VariantValueCategoryModel> searchRes = search(query.toString(), params);
        return searchRes.getResult();
    }

    @Override
    public CatalogVersionModel fetchProductCatalogBySalesOrganization(final String salesOrganization) {
        validateParameterNotNull(salesOrganization, "salesOrganization must not be null!");
        final StringBuilder query = new StringBuilder("SELECT {").append(SAPProductSalesAreaToCatalogMappingModel.CATALOGVERSION);
        query.append("} FROM {").append(SAPProductSalesAreaToCatalogMappingModel._TYPECODE);
        query.append("} WHERE {").append(SAPProductSalesAreaToCatalogMappingModel.SALESORGANIZATION);
        query.append("} = ?").append(SAPProductSalesAreaToCatalogMappingModel.SALESORGANIZATION);
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(SAPProductSalesAreaToCatalogMappingModel.SALESORGANIZATION, salesOrganization);
        final SearchResult<CatalogVersionModel> searchRes = search(query.toString(), params);
        return searchRes != null ? searchRes.getResult().get(0) : null;
    }

    private String buildVariantValueCategoryCode(final String parentCategoryCode) {
        return ThyssenkruppeucoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + getVariantCategoryCodeKey(parentCategoryCode) + ThyssenkruppeucoreConstants.LIKE_CHARACTER;
    }

    @Override
    public String getVariantCategoryCodeKey(final String parentCategoryCode) {
        return Arrays.toString(parentCategoryCode.split(ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX)).replaceAll(ThyssenkruppeucoreConstants.STRING_ARRAY_BRACKETS_REGEX_PATTERN, "");
    }

    @Override
    public Collection<VariantCategoryModel> findAllVariantCategories(final String ruleCode, final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(ruleCode, "ruleCode cannot be null");
        validateParameterNotNull(catalogVersion, "catalogVersion cannot be null");
        final Map<String, Object> params = new HashMap<>();
        params.put(VariantCategoryModel.CODE, ThyssenkruppeucoreConstants.VARIANT_CATEGORY_CODE_PREFIX + ruleCode +ThyssenkruppeucoreConstants.CHARACTER_HYPHEN + ThyssenkruppeucoreConstants.LIKE_CHARACTER);
        params.put(VariantCategoryModel.CATALOGVERSION, catalogVersion);
        final String query = "SELECT {" + VariantCategoryModel.PK
                + "} FROM {" + VariantCategoryModel._TYPECODE
                + "} WHERE {" + VariantCategoryModel.CODE
                + "} LIKE ?" + VariantCategoryModel.CODE + " AND {"
                + VariantCategoryModel.CATALOGVERSION + "}=?" + VariantCategoryModel.CATALOGVERSION;
        final SearchResult<VariantCategoryModel> searchResult = search(query, params);
        return searchResult.getResult();
    }

    @Override
    public Collection<VariantValueCategoryModel> findAllVariantValueCategories(final String ruleCode, final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(ruleCode, "ruleCode cannot be null");
        validateParameterNotNull(catalogVersion, "catalogVersion cannot be null");
        final Map<String, Object> params = new HashMap<>();
        params.put(VariantValueCategoryModel.CODE, ThyssenkruppeucoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + ruleCode +ThyssenkruppeucoreConstants.CHARACTER_HYPHEN+ ThyssenkruppeucoreConstants.LIKE_CHARACTER);
        params.put(VariantValueCategoryModel.CATALOGVERSION, catalogVersion);
        final String query = "SELECT {" + VariantValueCategoryModel.PK
            + "} FROM {" + VariantValueCategoryModel._TYPECODE
            + "} WHERE {" + VariantValueCategoryModel.CODE
            + "} LIKE ?" + VariantValueCategoryModel.CODE + " AND {"
            + VariantValueCategoryModel.CATALOGVERSION + "}=?" + VariantValueCategoryModel.CATALOGVERSION;
        final SearchResult<VariantValueCategoryModel> searchResult = search(query, params);
        return searchResult.getResult();
    }

    public <T extends CategoryModel> Collection<T> findAllCategoriesWithoutIncludedProducts(final String categoryPrefix, final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(categoryPrefix, "categoryPrefix cannot be null");
        validateParameterNotNull(catalogVersion, "catalog Version cannot be null");
        final Map<String, Object> params = new HashMap<>();
        params.put(CategoryModel.CODE, categoryPrefix + ThyssenkruppeucoreConstants.LIKE_CHARACTER);
        params.put(CategoryModel.CATALOGVERSION, catalogVersion);
        final String query = "SELECT {" + CategoryModel.PK + "} FROM {" + CategoryModel._TYPECODE + " as cat}"
            + " WHERE NOT EXISTS ("
            + "{{ SELECT {PK} FROM {CategoryProductRelation as c2p} WHERE {c2p.source}={cat.pk} }} )"
            + " AND {cat.catalogVersion}=?" + CategoryModel.CATALOGVERSION
            + " AND {cat:code} like ?" + CategoryModel.CODE;
        final SearchResult<T> searchResult = search(query, params);
        return searchResult.getResult();
    }
}

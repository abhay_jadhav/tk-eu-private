package com.thyssenkrupp.b2b.eu.core.search.solrfacetsearch.provider;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class TkEuDimensionPropertyValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {

    private static final Logger LOG                  = Logger.getLogger(TkEuDimensionPropertyValueProvider.class);
    private static final String SHAPE_ATTRIBUTE      = "xx_shape";
    private static final String RULE_SEPARATOR       = ",";
    private static final String ATTRIBUTES_SEPARATOR = "%";
    private ClassificationService classificationService;
    private FieldNameProvider     fieldNameProvider;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        if (model instanceof ProductModel) {
            final Map<String, String> valueProvideParametersMap = indexedProperty.getValueProviderParameters();
            final ProductModel productModel = (ProductModel) model;
            final String dimensionRulesForShape = getDimensionRulesForShape(valueProvideParametersMap, productModel);
            if (StringUtils.isNotBlank(dimensionRulesForShape)) {
                final Set<String> dimensionValues = new HashSet<>();
                getDimensionValuesForBaseProduct(dimensionRulesForShape, productModel, dimensionValues);
                getDimensionValuesForVariants(dimensionRulesForShape, productModel, dimensionValues);
                return getFieldValuesForDimensionValues(dimensionValues, indexedProperty);
            }
            return Collections.emptyList();
        } else {
            throw new FieldValueProviderException("Cannot provide classification property of non-product item");
        }
    }

    private void getDimensionValuesForBaseProduct(String dimensionRulesForShape, ProductModel productModel, Set<String> dimensionValues) {
        final FeatureList featureList = getClassificationService().getFeatures(productModel);
        if (isValidFeatureList(featureList)) {
            buildDimensionValues(dimensionRulesForShape, featureList, dimensionValues);
        }
    }

    private boolean isValidFeatureList(FeatureList featureList) {
        return featureList != null && !featureList.isEmpty();
    }

    private Collection<FieldValue> getFieldValuesForDimensionValues(final Set<String> dimensionValues, final IndexedProperty indexedProperty) {
        if (CollectionUtils.isNotEmpty(dimensionValues) && getFieldNameProvider() != null) {
            List<FieldValue> fieldValues = new ArrayList<>();
            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
            fieldNames.forEach(fieldName -> dimensionValues.forEach(dimensionValue -> fieldValues.add(new FieldValue(fieldName, dimensionValue))));
            return fieldValues;
        }
        LOG.debug("The dimension values are empty");
        return Collections.emptyList();
    }

    private void getDimensionValuesForVariants(final String dimensionRulesForShape, final ProductModel productModel, final Set<String> dimensionValues) {
        final Collection<VariantProductModel> variantProductModels = productModel.getVariants();
        for (VariantProductModel variantProduct : variantProductModels) {
            final FeatureList variantFeatures = classificationService.getFeatures(variantProduct);
            if (isValidFeatureList(variantFeatures)) {
                buildDimensionValues(dimensionRulesForShape, variantFeatures, dimensionValues);
            }
        }
        LOG.debug("Dimension rule is not specified for the product" + productModel.getCode());
    }

    private void buildDimensionValues(String dimensionRulesForShape, FeatureList variantFeatures, Set<String> dimensionValues) {
        if (hasMultipleRulesSeparatedByComma(dimensionRulesForShape)) {
            getDimensionValuesForMultipleRule(dimensionRulesForShape, variantFeatures, dimensionValues);
        } else {
            getDimensionValueForRule(dimensionRulesForShape, variantFeatures, dimensionValues);
        }
    }

    private void getDimensionValuesForMultipleRule(final String dimensionRulesForShape, final FeatureList featureList, final Set<String> dimensionValues) {
        final String[] dimensionRules = dimensionRulesForShape.split(RULE_SEPARATOR);
        if (ArrayUtils.isNotEmpty(dimensionRules)) {
            for (String dimensionRule : dimensionRules) {
                getDimensionValueForRule(dimensionRule, featureList, dimensionValues);
            }
        }
    }

    private void getDimensionValueForRule(final String dimensionRule, final FeatureList featureList, final Set<String> dimensionValues) {

        Map<String, String> featureMap = new HashedMap();
        getFeatureValuesMap(dimensionRule, featureList, featureMap);
        if (featureMap != null && !featureMap.isEmpty()) {
            final List<String> classAttributeName = new ArrayList(featureMap.size());
            final List<String> classAttributeValues = new ArrayList(featureMap.size());
            for (Map.Entry<String, String> entry : featureMap.entrySet()) {
                classAttributeName.add(entry.getKey());
                classAttributeValues.add(entry.getValue());
            }
            buildDimensionValuesString(classAttributeName, classAttributeValues, dimensionRule, dimensionValues);
        }
    }

    private void buildDimensionValuesString(final List<String> classAttributeName, final List<String> classAttributeValues, final String dimensionRule, final Set<String> dimensionValues) {
        final String[] classAttributeNameArray = classAttributeName.toArray(new String[classAttributeName.size()]);
        final String[] classAttributeValuesArray = classAttributeValues.toArray(new String[classAttributeValues.size()]);
        final String dimensionValue = StringUtils.replaceEach(dimensionRule, classAttributeNameArray, classAttributeValuesArray);
        dimensionValues.add(dimensionValue);
    }

    private void getFeatureValuesMap(String dimensionRule, FeatureList featureList, Map<String, String> featureMap) {
        featureList.getFeatures().stream().forEach(feature -> {
            final String name = getClassificationAttributeName(feature);
            final String value = getClassificationAttributeValue(feature);
            if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(value)) {
                if (dimensionRule.toLowerCase().contains(name.toLowerCase())) {
                    StringBuilder sb = new StringBuilder(ATTRIBUTES_SEPARATOR).append(name).append(ATTRIBUTES_SEPARATOR);
                    featureMap.put(sb.toString(), value);
                }
            }
        });
    }

    private boolean hasMultipleRulesSeparatedByComma(final String rule) {
        return rule.contains(RULE_SEPARATOR);
    }

    private String getDimensionRulesForShape(final Map<String, String> valueProvideParametersMap, final ProductModel productModel) {
        String dimensionRule = "";
        final FeatureList features = getClassificationService().getFeatures(productModel);
        if (valueProvideParametersMap != null && isValidFeatureList(features)) {
            final Optional<Feature> classAttribute = features.getFeatures().stream().filter(feature -> StringUtils.endsWith(feature.getCode().toLowerCase(), SHAPE_ATTRIBUTE)).findFirst();
            dimensionRule = classAttribute.isPresent() ? valueProvideParametersMap.get(getClassificationAttributeValue(classAttribute.get())) : "";
        }
        return dimensionRule;
    }

    private String getClassificationAttributeValue(Feature feature) {
        String classAttributeValue = "";
        if (feature != null && feature.getValue() != null && feature.getValue().getValue() != null) {
            classAttributeValue = (feature.getValue().getValue() instanceof ClassificationAttributeValueModel) ? ((ClassificationAttributeValueModel) feature.getValue().getValue()).getCode() : feature.getValue().getValue().toString();
        }
        return removeTailingZeros(classAttributeValue);
    }

    private String removeTailingZeros(String attributeValueString) {
        if (StringUtils.isNotBlank(attributeValueString) && isvalidNumber(attributeValueString)) {
            Double doubleValue = Double.parseDouble(attributeValueString);
            DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
            format.applyPattern("0.##");
            return format.format(doubleValue);
        }
        return attributeValueString;
    }

    private boolean isvalidNumber(String attributeValueString) {
        try {
            Double.parseDouble(attributeValueString);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    private String getClassificationAttributeName(final Feature feature) {
        String classAttributeName = "";
        if (feature != null && feature.getClassAttributeAssignment() != null) {
            final ClassificationAttributeModel classificationAttributeModel = feature.getClassAttributeAssignment().getClassificationAttribute();
            classAttributeName = classificationAttributeModel == null ? "" : classificationAttributeModel.getCode();
        }
        return classAttributeName;
    }

    public ClassificationService getClassificationService() {
        return classificationService;
    }

    public void setClassificationService(ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    public FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }
}

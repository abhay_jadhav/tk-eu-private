package com.thyssenkrupp.b2b.eu.core.dao.impl;

import static de.hybris.platform.ordersplitting.model.ConsignmentModel.CODE;
import static de.hybris.platform.ordersplitting.model.ConsignmentModel.NAMEDDELIVERYDATE;
import static de.hybris.platform.ordersplitting.model.ConsignmentModel.SHIPPINGDATE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.b2b.eu.core.dao.TkEuCustomerAccountDeliveryDao;

public class DefaultTkEuCustomerAccountDeliveryDao extends AbstractItemDao implements TkEuCustomerAccountDeliveryDao {

    protected static final String SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER = "SELECT distinct {consignment.pk},{consignment.code},{consignment.shippingDate},{consignment.namedDeliveryDate} from {Consignment as consignment JOIN sapconfiguration as sapconfiguration ON {consignment.salesOrg} = {sapconfiguration.sapcommon_salesOrganization} JOIN BaseStore as basestore ON {basestore.sapconfiguration} = {sapconfiguration.pk} LEFT OUTER JOIN ConsignmentEntry as consignmentEntry  ON {consignmentEntry.consignment} = {consignment.pk} LEFT OUTER JOIN OrderEntry as orderEntry  ON {orderEntry.pk} = {consignmentEntry.orderEntry} LEFT OUTER JOIN Order as order  ON {order.pk} = {orderEntry.order}} WHERE {consignment.unit} IN (?unit) AND {basestore.pk} = ?store AND {consignment.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) AND {consignment.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";
    // "SELECT distinct
    // {consignment.pk},{consignment.code},{consignment.shippingDate},{consignment.namedDeliveryDate}"
    // + " from {ConsignmentEntry as consignmentEntry\n" + "JOIN OrderEntry as orderEntry ON
    // {consignmentEntry.orderEntry} = {orderEntry.pk}\n" + "JOIN Consignment as consignment ON
    // {consignmentEntry.consignment} = {consignment.pk}\n" + "JOIN Order as order ON
    // {orderEntry.order} = {order.pk}\n" + "JOIN BaseStore as basestore ON {order.store} =
    // {basestore.pk}\n" + "JOIN sapconfiguration as sapconfiguration ON
    // {basestore.sapconfiguration} = {sapconfiguration.pk}}\n" + "WHERE {consignment.unit} IN
    // (?unit) AND {order.store} = ?store\n" + "AND {consignment.distributionChannel} IN
    // ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel})\n"
    // + "AND {consignment.division} IN
    // ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    protected static final String FIND_CONSIGNMENTS_BY_CODE_UNIT = "SELECT {consignment.pk}  from {Consignment as consignment JOIN sapconfiguration as sapconfiguration ON {consignment.salesOrg} = {sapconfiguration.sapcommon_salesOrganization} JOIN BaseStore as basestore ON {basestore.sapconfiguration} = {sapconfiguration.pk} LEFT OUTER JOIN ConsignmentEntry as consignmentEntry  ON {consignmentEntry.consignment} = {consignment.pk}} WHERE {consignment.code} = ?code and {consignment.unit} IN (?unit) AND {basestore.pk} = ?store AND {consignment.distributionChannel} IN ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel}) AND {consignment.division} IN ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    // protected static final String FIND_CONSIGNMENTS_BY_CODE_UNIT = "SELECT {consignment.pk} FROM
    // {ConsignmentEntry as consignmentEntry JOIN OrderEntry as orderEntry ON
    // {consignmentEntry.orderEntry} = {orderEntry.pk} JOIN Consignment as consignment ON
    // {consignmentEntry.consignment} = {consignment.pk} JOIN Order as order ON {orderEntry.order} =
    // {order.pk} JOIN BaseStore as basestore ON {order.store} = {basestore.pk} JOIN
    // sapconfiguration as sapconfiguration ON {basestore.sapconfiguration} = {sapconfiguration.pk}}
    // WHERE {consignment.code} COLLATE Latin1_General_CI_AS LIKE ?code and {consignment.unit} IN
    // (?unit) AND {order.store} = ?store AND {consignment.distributionChannel} IN
    // ({sapconfiguration.sapcommon_distributionChannel},{sapconfiguration.offlineDistributionChannel})
    // AND {consignment.division} IN
    // ({sapconfiguration.sapcommon_division},{sapconfiguration.offlineDivision})";

    protected static final String SORT_ORDERS_BY_DELIVERY_NO_DESC = " ORDER BY {consignment." + CODE + "} DESC";
    protected static final String SORT_ORDERS_BY_DELIVERY_NO_ASC = " ORDER BY {consignment." + CODE + "} ASC";
    protected static final String SORT_ORDERS_BY_SHIPPING_DATE_ASC = " ORDER BY {consignment." + SHIPPINGDATE + "} ASC";
    protected static final String SORT_ORDERS_BY_SHIPPING_DATE_DESC = " ORDER BY {consignment." + SHIPPINGDATE + "} DESC";
    protected static final String SORT_ORDERS_BY_DELIVERY_DATE_ASC = " ORDER BY {consignment." + NAMEDDELIVERYDATE + "} ASC";
    protected static final String SORT_ORDERS_BY_DELIVERY_DATE_DESC = " ORDER BY {consignment." + NAMEDDELIVERYDATE + "} DESC";
    protected static final String FIND_CONSIGNMENT_ENTRY_BY_ORDER_ENTRY_UNIT = "SELECT {" + ConsignmentEntryModel.PK + "}  from {" + ConsignmentEntryModel._TYPECODE + "  as ce JOIN " + ConsignmentModel._TYPECODE + " as c ON {ce." + ConsignmentEntryModel.CONSIGNMENT + "} = {c." + ConsignmentModel.PK + "}} WHERE {ce." + ConsignmentEntryModel.SAPORDERID + "} = ?sapOrderId and {ce." + ConsignmentEntryModel.SAPORDERENTRYROWNUMBER + "} = ?entryNumber  and  {c." + ConsignmentModel.UNIT + "} IN (?unit)";

    private int deliverySearchCount;

    private PagedFlexibleSearchService pagedFlexibleSearchService;
    private ConfigurationService configurationService;

    @Override
    public SearchPageData<ConsignmentModel> orderDeliveriesSearchList(final String searchKey, final PageableData pageableData, final Set<B2BUnitModel> b2bUnits, final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(b2bUnits, "b2BUnit must not be null");
        validateParameterNotNull(baseStoreModel, "baseStoreModel must not be null");

        final Map<String, Object> queryParams = loadQueryParams(b2bUnits, baseStoreModel);
        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }
        final List<SortQueryData> sortQueries;

        sortQueries = Arrays.asList(createSortQueryData("byDeliveryNoDesc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_DELIVERY_NO_DESC), createSortQueryData("byDeliveryNoAsc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_DELIVERY_NO_ASC), createSortQueryData("byShippingDateAsc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_SHIPPING_DATE_ASC), createSortQueryData("byShippingDateDesc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_SHIPPING_DATE_DESC), createSortQueryData("byDeliveryDateAsc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_DELIVERY_DATE_ASC), createSortQueryData("byDeliveryDateDesc", SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString() + SORT_ORDERS_BY_DELIVERY_DATE_DESC));

        return getPagedFlexibleSearchService().search(sortQueries, "byDeliveryNoDesc", queryParams, pageableData);
    }

    @Override
    public int orderDeliveriesSearchListCount(final String searchKey, final Set<B2BUnitModel> b2bUnits, final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(b2bUnits, "b2BUnitModel must not be null");
        validateParameterNotNull(baseStoreModel, "baseStoreModel must not be null");

        final Map<String, Object> queryParams = loadQueryParams(b2bUnits, baseStoreModel);

        final StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(searchKey)) {
            createSearchQueryStatement(searchKey, stringBuilder, queryParams);
        }

        final String query = SEARCH_CONSIGNMENTS_BY_UNIT_AND_ORDER + stringBuilder.toString();

        final SearchResult<Integer> searchResult = getFlexibleSearchService().search(query, queryParams);

        return searchResult.getResult().size();
    }

    @Override
    public List<ConsignmentModel> fetchDeliveries(final String deliveryNo, final Set<B2BUnitModel> b2BUnits, final BaseStoreModel baseStoreModel) {
        validateParameterNotNull(deliveryNo, "Delivery Number must not be null");
        validateParameterNotNull(b2BUnits, "b2BUnitModel must not be null");
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("code", deliveryNo);
        queryParams.put("unit", b2BUnits);
        queryParams.put("store", baseStoreModel);

        final SearchResult<ConsignmentModel> result = getFlexibleSearchService().search(FIND_CONSIGNMENTS_BY_CODE_UNIT, queryParams);
        return result.getResult();
    }

    private Map<String, Object> loadQueryParams(final Set<B2BUnitModel> b2bUnits, final BaseStoreModel baseStoreModel) {
        final Map<String, Object> queryParams = new HashMap();
        queryParams.put("unit", b2bUnits);
        queryParams.put("store", baseStoreModel);
        return queryParams;
    }

    private void createSearchQueryStatement(final String searchKey, final StringBuilder stringBuilder, final Map queryParams) {
        final String[] queryStr = searchKey.toUpperCase().split("\\s+");
        stringBuilder.append(" AND ( ");
        int searchKeyCount = getDeliverySearchCount();
        searchKeyCount = Math.min(queryStr.length, searchKeyCount);

        for (int i = 0; i < searchKeyCount; i++) {
            if (i != 0) {
                stringBuilder.append(" AND ");
            }
            stringBuilder.append(" ( {order." + OrderModel.PURCHASEORDERNUMBER + "} COLLATE Latin1_General_CI_AS LIKE ?poNum" + i + " OR {order." + OrderModel.NAME + "} COLLATE Latin1_General_CI_AS LIKE ?orderName" + i + " OR {consignment." + CODE + "} COLLATE Latin1_General_CI_AS LIKE ?deliveryNo" + i + ")");

            queryParams.put("poNum" + i, "%" + queryStr[i] + "%");
            queryParams.put("orderCode" + i, "%" + queryStr[i] + "%");
            queryParams.put("orderName" + i, "%" + queryStr[i] + "%");
            queryParams.put("deliveryNo" + i, "%" + queryStr[i] + "%");
        }
        stringBuilder.append(")");
    }

    protected SortQueryData createSortQueryData(final String sortCode, final String query) {
        final SortQueryData result = new SortQueryData();
        result.setSortCode(sortCode);
        result.setQuery(query);
        return result;
    }

    public int getDeliverySearchCount() {
        return deliverySearchCount;
    }

    public void setDeliverySearchCount(final int deliverySearchCount) {
        this.deliverySearchCount = deliverySearchCount;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public PagedFlexibleSearchService getPagedFlexibleSearchService() {
        return pagedFlexibleSearchService;
    }

    @Required
    public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService) {
        this.pagedFlexibleSearchService = pagedFlexibleSearchService;
    }

    @Override
    public List<ConsignmentEntryModel> fetchDeliveriesForOrderEntry(final String sapOrderId, final int entryNumber, final Set<B2BUnitModel> b2bUnits, final BaseStoreModel currentBaseStore) {
        validateParameterNotNull(sapOrderId, "sapOrderId must not be null");
        validateParameterNotNull(b2bUnits, "b2BUnitModel must not be null");
        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("sapOrderId", sapOrderId);
        queryParams.put("entryNumber", Integer.valueOf(entryNumber));
        queryParams.put("unit", b2bUnits);

        final SearchResult<ConsignmentEntryModel> result = getFlexibleSearchService().search(FIND_CONSIGNMENT_ENTRY_BY_ORDER_ENTRY_UNIT, queryParams);
        if(CollectionUtils.isNotEmpty(result.getResult())) {
            return result.getResult();
        } else {
            return Collections.emptyList();
        }

    }
}

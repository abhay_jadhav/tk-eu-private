package com.thyssenkrupp.b2b.eu.core.atp.exchanges.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SapAtpResponse implements Serializable {

    @JsonProperty("ATP_Response")
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<AtpResponse> atpResponses = null;

    @JsonProperty("ATP_Response")
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public List<AtpResponse> getAtpResponse() {
        return atpResponses;
    }

    @JsonProperty("ATP_Response")
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public void setAtpResponse(List<AtpResponse> pAtpResponses) {
        this.atpResponses = pAtpResponses;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("ATP_Response", atpResponses).toString();
    }
}

package com.thyssenkrupp.b2b.eu.core.service.impl;

import com.thyssenkrupp.b2b.eu.core.enums.DocumentDownloadServiceEnum;
import com.thyssenkrupp.b2b.eu.core.pojo.DocumentDownloadRequest;
import com.thyssenkrupp.b2b.eu.core.pojo.GetDocumentContentResponse;
import com.thyssenkrupp.b2b.eu.core.service.TkEuDocumentDownloadService;

public class TkEuDisableDocumentDownloadService implements TkEuDocumentDownloadService {

    @Override
    public Object getDocumentStatus(DocumentDownloadRequest documentRequest) {
        return null;
    }

    @Override
    public GetDocumentContentResponse getDocument(DocumentDownloadRequest documentRequest) {
        return null;
    }

    @Override
    public DocumentDownloadServiceEnum getType() {
        return DocumentDownloadServiceEnum.SWITCHOFF;
    }

    @Override
    public boolean isAuthorizedUser(DocumentDownloadRequest documents) {
        return false;
    }
}

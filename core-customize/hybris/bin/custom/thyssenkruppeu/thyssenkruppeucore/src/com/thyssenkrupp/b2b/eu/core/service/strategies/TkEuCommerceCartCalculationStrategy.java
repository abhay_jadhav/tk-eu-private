package com.thyssenkrupp.b2b.eu.core.service.strategies;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public interface TkEuCommerceCartCalculationStrategy extends CommerceCartCalculationStrategy {

    boolean calculateCartTotals(CommerceCartParameter parameter, CommerceCartModification commerceCartModification);
}

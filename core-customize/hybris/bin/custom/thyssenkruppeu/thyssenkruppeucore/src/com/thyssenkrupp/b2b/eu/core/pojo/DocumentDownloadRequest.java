package com.thyssenkrupp.b2b.eu.core.pojo;

import java.util.List;

public class DocumentDownloadRequest {

    private List<DocumentRequest> documentRequests;

    public List<DocumentRequest> getDocumentRequests() {
        return documentRequests;
    }

    public void setDocumentRequests(List<DocumentRequest> documentRequests) {
        this.documentRequests = documentRequests;
    }
}

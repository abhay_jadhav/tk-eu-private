package com.thyssenkrupp.b2b.eu.core.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GetDocumentStatusRequest implements Serializable {
    @JsonProperty("Get_Document_Status")
    private GetDocumentContent getDocumentStatus;

    @JsonProperty("Get_Document_Status")
    public GetDocumentContent getGetDocumentStatus() {
        return getDocumentStatus;
    }

    @JsonProperty("Get_Document_Status")
    public void setGetDocumentStatus(GetDocumentContent getDocumentStatus) {
        this.getDocumentStatus = getDocumentStatus;
    }

    @Override
    public String toString() {
        return "ClassPojo [Get_Document_Content = " + getDocumentStatus + "]";
    }
}

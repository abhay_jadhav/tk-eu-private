package com.thyssenkrupp.b2b.eu.prospricing.mock.service;

import java.util.List;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;

public interface TkEuMockPROSAxisService {

    List<PriceDetailsResponseItem> getPROSMockPriceResponse(PriceDetailsRecords[] priceDetailsRequestArray);

    List<PriceDetailsResponseItem> getPROSMockPriceResponseWithError(List<PriceDetailsRecords> priceDetails,
            List<String> errorlines);

    PriceDetailsRecords getPROSMockReqDetails(String productCode, long lineItem, double weight);

    PriceDetailsRecords getPROSMockReqDetails(TkEuPriceParameter tkEuPriceParameter);

}

package com.thyssenkrupp.b2b.eu.prospricing.strategy.impl;

import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.prospricing.constants.ThyssenkruppeuprospricingConstants;
import com.thyssenkrupp.b2b.eu.prospricing.service.impl.DefaultTkEuFetchPriceFactorServiceImpl;
import com.thyssenkrupp.b2b.eu.prospricing.strategy.TkEuProsPricingRequestStrategy;
import com.thyssenkrupp.b2b.eu.prospricing.utils.TkEuProsPricingUtils;
import com.thyssenkrupp.b2b.eu.prospricing.utils.TkEuProsSCPIConnectionUtils;

/**
 *
 */
public class DefaultTkEuProsPricingRequestStrategy implements TkEuProsPricingRequestStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuProsPricingRequestStrategy.class);
    private DefaultTkEuFetchPriceFactorServiceImpl tkEuFetchPriceFactorServiceImpl;

    @Resource
    private BaseStoreService baseStoreService;

    @Override
    public PriceDetailsRecords[] convertPriceParamterToSCPIRequest(final TkEuPriceParameter tkEuPriceParameter) {

        final PriceDetailsRecords[] requestArray = new PriceDetailsRecords[1];
        try {
            final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
            final SAPConfigurationModel sapConfigurationModel = currentBaseStore.getSAPConfiguration();

            requestArray[0] = createSapReqFromParamters(tkEuPriceParameter, tkEuPriceParameter.getQuantityWrapper(),
                    sapConfigurationModel);
        } catch (final Exception e) {
            LOG.error("Invalid Request Generation for Pros SCPI Service", e);
        }
        return requestArray;

    }

    @Override
    public PriceDetailsRecords[] convertPriceParamterToSCPIRequestForOrderEntry(
            final TkEuPriceParameter tkEuPriceParameter, final MaterialQuantityWrapper quantityWrapper) {
        final PriceDetailsRecords[] requestArray = new PriceDetailsRecords[1];
        try {
            final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
            final SAPConfigurationModel sapConfigurationModel = currentBaseStore.getSAPConfiguration();

            requestArray[0] = createSapReqFromParamters(tkEuPriceParameter, quantityWrapper, sapConfigurationModel);
        } catch (final Exception e) {
            LOG.error("Invalid Request Generation for Pros SCPI Service", e);
        }
        return requestArray;
    }
    private PriceDetailsRecords createSapReqFromParamters(final TkEuPriceParameter tkEuPriceParameter,
            final MaterialQuantityWrapper quantityWrapper, final SAPConfigurationModel sapConfigurationModel) {
        final PriceDetailsRecords soapReqRecordObj = new PriceDetailsRecords();
        final BigDecimal priceDefault = new BigDecimal(
                (ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_PRICE_DEFAULT));
        soapReqRecordObj.setSystem(
                TkEuConfigKeyValueUtils.getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_SYSTEM, ""));
        soapReqRecordObj.setClient(
                TkEuConfigKeyValueUtils.getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_CLIENT, ""));
        soapReqRecordObj.setSalesOrg(sapConfigurationModel.getSapcommon_salesOrganization());
        soapReqRecordObj.setDistChannel(TkEuConfigKeyValueUtils.getString(
                ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DIST_CHANNEL,
                sapConfigurationModel.getSapcommon_distributionChannel()));

        soapReqRecordObj.setCustomer(tkEuPriceParameter.getB2bUnit().getUid());
        soapReqRecordObj.setIncoTerm(tkEuPriceParameter.getChildB2bUnit().getIncoterms().getCode());
        soapReqRecordObj.setCustomerClassification(TkEuConfigKeyValueUtils
                .getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_CUSTOMERCLASSIFICATION, ""));
        soapReqRecordObj.setSalesOffice(tkEuPriceParameter.getChildB2bUnit().getSalesOffice());
        soapReqRecordObj.setLineItemNo(String.valueOf(
                null != tkEuPriceParameter.getOrderEntry() ? tkEuPriceParameter.getOrderEntry().getEntryNumber() : 1));
        soapReqRecordObj.setQty(BigDecimal.valueOf(
                null != tkEuPriceParameter.getOrderEntry() ? tkEuPriceParameter.getOrderEntry().getQuantity() : 1));
        soapReqRecordObj.setUom(tkEuPriceParameter.getProduct().getBaseUnit().getSapCode());
        soapReqRecordObj.setNetweight(BigDecimal.valueOf(quantityWrapper.getQuantity()));
        soapReqRecordObj.setSalesUom(getStoreSalesUnit(baseStoreService.getCurrentBaseStore()));
        soapReqRecordObj
                .setCumulativeNetWeight((BigDecimal.valueOf(tkEuPriceParameter.getQuantityWrapper().getQuantity())));
        soapReqRecordObj.setDocCurrencyCode(tkEuPriceParameter.getCurrencyCode());
        soapReqRecordObj.setPriceDate(TkEuProsPricingUtils.getCurrentDate(
                TkEuConfigKeyValueUtils.getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATEFORMAT,
                        ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATEFORMAT_DEFAULT)));
        soapReqRecordObj.setPlant(
                TkEuConfigKeyValueUtils.getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_PLANT, ""));
        soapReqRecordObj.setPricingProc(TkEuConfigKeyValueUtils
                .getString(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_PRICINGPROC, ""));
        soapReqRecordObj.setPaymentTerms(tkEuPriceParameter.getChildB2bUnit().getPaymentterms().getCode());
        soapReqRecordObj.setShippingCondition(tkEuPriceParameter.getChildB2bUnit().getShippingcondition().getCode());
        soapReqRecordObj.setCustomerGroup1(null != tkEuPriceParameter.getChildB2bUnit().getWeightingType()
                ? tkEuPriceParameter.getChildB2bUnit().getWeightingType().getCode()
                : "");
        soapReqRecordObj.setPriceFactor(getPriceFactorForProduct(tkEuPriceParameter.getProduct().getCode(),
                tkEuPriceParameter.getProduct().getBaseUnit()));
        soapReqRecordObj.setMaterial(tkEuPriceParameter.getProduct().getCode());
        soapReqRecordObj.setY2BA_PriceProposal(priceDefault);
        soapReqRecordObj.setY2AA_FloorPrice(priceDefault);
        soapReqRecordObj.setY2ZA_TargetPrice(priceDefault);
        soapReqRecordObj.setY2SA_StartPrice(priceDefault);
        soapReqRecordObj.setProcessStartTime(
                TkEuProsPricingUtils.getCurrentDate(TkEuConfigKeyValueUtils.getString(
                        ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATETIMEFORMAT,
                        ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATETIMEFORMAT)));
        LOG.info(TkEuProsSCPIConnectionUtils.debugRequest(soapReqRecordObj));
        return soapReqRecordObj;
    }

    private String getStoreSalesUnit(final BaseStoreModel baseStoreModel) {
        if (!Objects.isNull(baseStoreModel)) {
            return baseStoreModel.getSalesUnits().get(0).getSapCode();
        }
        return StringUtils.EMPTY;
    }

    private BigInteger getPriceFactorForProduct(final String productCode, final UnitModel baseUnit) {
        if (StringUtils.isNotEmpty(productCode) && !Objects.isNull(baseUnit)) {
            final Optional<Long> priceFactor = tkEuFetchPriceFactorServiceImpl.getPriceFactorFromMaterialId(productCode,
                baseUnit);
            if (priceFactor.isPresent()) {
            return BigInteger.valueOf(priceFactor.get());
        }
        }
        return BigInteger.valueOf(1);
    }

    public DefaultTkEuFetchPriceFactorServiceImpl getTkEuFetchPriceFactorServiceImpl() {
        return tkEuFetchPriceFactorServiceImpl;
    }

    @Required
    public void setTkEuFetchPriceFactorServiceImpl(
            final DefaultTkEuFetchPriceFactorServiceImpl tkEuFetchPriceFactorServiceImpl) {
        this.tkEuFetchPriceFactorServiceImpl = tkEuFetchPriceFactorServiceImpl;
    }
}

package com.thyssenkrupp.b2b.eu.prospricing.service.impl;

import de.hybris.platform.sap.core.configuration.http.HTTPDestination;
import de.hybris.platform.sap.core.configuration.http.HTTPDestinationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.GetPriceDetails_OutBindingStub;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.b2b.eu.core.utils.TkEuConfigKeyValueUtils;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.prospricing.constants.ThyssenkruppeuprospricingConstants;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;
import com.thyssenkrupp.b2b.eu.prospricing.mock.service.TkEuMockPROSAxisService;
import com.thyssenkrupp.b2b.eu.prospricing.service.TkEuProsPricingService;
import com.thyssenkrupp.b2b.eu.prospricing.strategy.TkEuProsPricingRequestStrategy;
import com.thyssenkrupp.b2b.eu.prospricing.strategy.TkEuProsPricingResponseStrategy;
import com.thyssenkrupp.b2b.eu.prospricing.utils.TkEuProsSCPIConnectionUtils;

public class DefaultTkEuProsPricingService implements TkEuProsPricingService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuProsPricingService.class);

    @Resource
    private TkEuMockPROSAxisService tkEuMockPROSAxisService;

    @Resource
    private TkEuProsPricingRequestStrategy tkEuProsPricingRequestStrategy;

    @Resource
    private TkEuProsPricingResponseStrategy tkEuProsPricingResponseStrategy;

    @Resource(name = "sapCoreHTTPDestinationService")
    private HTTPDestinationService hTTPDestinationService;

    @Override
    public Optional<TkEuProsPriceReponseParameter> getProsPriceForProduct(final TkEuPriceParameter tkEuPriceParameter) {
        try {
            final PriceDetailsRecords[] priceDetailsRequestArray = tkEuProsPricingRequestStrategy
                    .convertPriceParamterToSCPIRequest(tkEuPriceParameter);
            return getProsSCPIResponseForRequest(priceDetailsRequestArray);

        } catch (final Exception e) {
            LOG.error("Exception while geting Response from PROS SCPI  Service", e);
        }
        LOG.info("No Response from PROS for product:" + tkEuPriceParameter.getProduct().getCode());
        return Optional.empty();
    }

    @Override
    public Optional<TkEuProsPriceReponseParameter> getProsPriceForOrderEntry(
            final TkEuPriceParameter tkEuPriceParameter, final MaterialQuantityWrapper quantityWrapper) {
        try {
            final PriceDetailsRecords[] priceDetailsRequestArray = tkEuProsPricingRequestStrategy
                    .convertPriceParamterToSCPIRequestForOrderEntry(tkEuPriceParameter, quantityWrapper);

            return getProsSCPIResponseForRequest(priceDetailsRequestArray);
        } catch (final Exception e) {
            LOG.error("Exception while geting Response from PROS SCPI  Service", e);
        }
        LOG.info("No Response from PROS for getProsPriceForOrderEntry :" + tkEuPriceParameter.getProduct().getCode());
        return Optional.empty();
    }


    private Optional<TkEuProsPriceReponseParameter> getProsSCPIResponseForRequest(
            final PriceDetailsRecords[] priceDetailsRequestArray) {
        List<PriceDetailsResponseItem> response = new ArrayList<>();
        try {
            Assert.notNull(priceDetailsRequestArray, "The class must not be null");
            final GetPriceDetails_OutBindingStub stub = getConnectionToPROSScpi();
            if (null != stub) {
                final PriceDetailsResponseItem[] reponseItems = stub.getPriceDetails_Out(priceDetailsRequestArray);
                LOG.info("Received Response from SCPI :" + response);
                if (null != reponseItems && reponseItems.length > 0) {
                    response.addAll(Arrays.asList(reponseItems));
                }
            }
        } catch (final Exception e) {
            LOG.error("Exception while geting Response to PROS SCPI  Service", e);
            if (TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeuprospricingConstants.PROS_MOCK_PRICING_SERVICE_ENABLED,
                    true)) {
            response = getMockResponse(priceDetailsRequestArray);
            }
        }
        return validateAndReturnResponse(response, priceDetailsRequestArray);
    }

    /**
     * @param priceDetailsRequestArray
     *
     */
    private Optional<TkEuProsPriceReponseParameter> validateAndReturnResponse(final List<PriceDetailsResponseItem> response,
            final PriceDetailsRecords[] priceDetailsRequestArray) {
        if (CollectionUtils.isNotEmpty(response)) {

            final Optional<TkEuProsPriceReponseParameter> responseFromPros = tkEuProsPricingResponseStrategy
                    .convertProsReponseToPriceParamter(response.get(0));
            if (TkEuConfigKeyValueUtils.getBoolean(ThyssenkruppeuprospricingConstants.PROS_MOCK_PRICING_SERVICE_ENABLED,
                    true) && responseFromPros.isPresent()
                    && (StringUtils.isNotEmpty(responseFromPros.get().getErrorMessage())
                            || responseFromPros.get().getTargetPrice().doubleValue() <= 0.0d)) {
                final List<PriceDetailsResponseItem> responseObj = getMockResponse(priceDetailsRequestArray);
                LOG.info("No valid Reponse from PROS calling Mock service :");
                return tkEuProsPricingResponseStrategy.convertProsReponseToPriceParamter(responseObj.get(0));
            }
            return responseFromPros;
        }
        return Optional.empty();
    }

    @Override
    public void createMockRequestAndTestAxisSCPIPROSResponse(final TkEuPriceParameter tkEuPriceParameter,
            final boolean mockActivated) {
        List<PriceDetailsResponseItem> response = new ArrayList<>();
        try {
            final PriceDetailsRecords[] detailsArray = new PriceDetailsRecords[1];
            detailsArray[0] = tkEuMockPROSAxisService.getPROSMockReqDetails(tkEuPriceParameter);
            if (mockActivated) {
                response = getMockResponse(detailsArray);
            }
            response.forEach(items ->
            LOG.info(TkEuProsSCPIConnectionUtils.debugResponse(items))
            );

        } catch (final Exception e) {
            LOG.error("Exception while Mock Response for PROS SCPI :", e);

        }

    }

    /**
     *
     */
    private GetPriceDetails_OutBindingStub getConnectionToPROSScpi() {
        try {

        final HTTPDestination prosHttpDestinationDetails = hTTPDestinationService
                .getHTTPDestination(ThyssenkruppeuprospricingConstants.PROS_HTTP_DESTINATION_NAME);

        return TkEuProsSCPIConnectionUtils.getSOAPConnectionToSCPI(prosHttpDestinationDetails);
        } catch (final Exception e) {
            LOG.error("Exception in getting PROS connection to SCPI :", e);
        }
        return null;
    }

    private List<PriceDetailsResponseItem> getMockResponse(final PriceDetailsRecords[] detailsArray) {
        return tkEuMockPROSAxisService.getPROSMockPriceResponse(detailsArray);

    }

}

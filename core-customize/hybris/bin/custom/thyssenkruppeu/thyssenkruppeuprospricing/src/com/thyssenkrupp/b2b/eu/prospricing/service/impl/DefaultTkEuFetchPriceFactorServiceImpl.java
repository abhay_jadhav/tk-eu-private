package com.thyssenkrupp.b2b.eu.prospricing.service.impl;

import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.ProductService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.prospricing.dao.impl.DefaultTkEuPriceFactorDao;
import com.thyssenkrupp.b2b.eu.prospricing.model.PriceFactorConfigModel;
import com.thyssenkrupp.b2b.eu.prospricing.service.TkEuFetchPriceFactorService;

public class DefaultTkEuFetchPriceFactorServiceImpl implements TkEuFetchPriceFactorService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuFetchPriceFactorServiceImpl.class);
    private DefaultTkEuPriceFactorDao defaultTkEuPriceFactorDao;
    private ProductService productService;

    @Override
    public Optional<Long> getPriceFactorFromMaterialId(final String materialId, final UnitModel unitCode) {

        LOG.debug("INTO SERVICE with material id {}", materialId);
        try {
            final List<PriceFactorConfigModel> priceFactorList = getDefaultTkEuPriceFactorDao()
                    .findPriceFactorByMaterialId(materialId);
            PriceFactorConfigModel priceFactor;
            if (CollectionUtils.isNotEmpty(priceFactorList)) {
                priceFactor = priceFactorList.stream().findFirst().get();
                if (!Objects.isNull(priceFactor) && !Objects.isNull(priceFactor.getUnit())
                        && priceFactor.getUnit().getCode().equalsIgnoreCase(unitCode.getCode())) {
                    LOG.debug("Price factor is", priceFactor.getPriceFactor() + " for material " + materialId);
                    if (priceFactor.getPriceFactor() == 0) {
                        return Optional.ofNullable(1L);
                    } else {
                        return Optional.of(priceFactor.getPriceFactor());
                    }
                }
            }
            LOG.debug("Price factor not found for material id:" + materialId);
            return Optional.ofNullable(1L);
        } catch (final Exception e) {
            LOG.debug("Exception in calculating price factor");
            return Optional.ofNullable(1L);
        }
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    public DefaultTkEuPriceFactorDao getDefaultTkEuPriceFactorDao() {
        return defaultTkEuPriceFactorDao;
    }

    public void setDefaultTkEuPriceFactorDao(final DefaultTkEuPriceFactorDao defaultTkEuPriceFactorDao) {
        this.defaultTkEuPriceFactorDao = defaultTkEuPriceFactorDao;
    }
}

package com.thyssenkrupp.b2b.eu.prospricing.constants;

public final class ThyssenkruppeuprospricingConstants extends GeneratedThyssenkruppeuprospricingConstants {
    public static final String EXTENSIONNAME = "thyssenkruppeuprospricing";
    public static final String PLATFORM_LOGO_CODE = "thyssenkruppeuprospricingPlatformLogo";

    // Get Price Service Constants
    public static final String PROS_PRICING_REQUEST_SYSTEM = "pricing.pros.scpi.request.system";
    public static final String PROS_PRICING_REQUEST_CLIENT = "pricing.pros.scpi.request.client";
    public static final String PROS_PRICING_REQUEST_CUSTOMERCLASSIFICATION = "pricing.pros.scpi.request.customerclassification";
    public static final String PROS_PRICING_REQUEST_PLANT = "pricing.pros.scpi.request.plant";
    public static final String PROS_PRICING_REQUEST_DIST_CHANNEL = "pricing.pros.scpi.request.distchannel";
    public static final String PROS_PRICING_REQUEST_DATEFORMAT = "pricing.pros.scpi.request.dateformat";
    public static final String PROS_PRICING_REQUEST_PRICINGPROC = "pricing.pros.scpi.request.pricingproc";
    public static final String PROS_PRICING_REQUEST_DATEFORMAT_DEFAULT = "yyyyMMdd";
    public static final String PROS_PRICING_REQUEST_PRICE_DEFAULT = "0.00";
    public static final String PROS_PRICING_REQUEST_DATETIMEFORMAT = "pricing.pros.scpi.request.datetimeformat";
    public static final String PROS_PRICING_REQUEST_DATETIMEFORMAT_DEFAULT = "yyyyMMdd HHmmssSSS";
    public static final String PROS_HTTP_DESTINATION_NAME = "PROSSCPIConnection";
    public static final String PROS_MOCK_PRICING_SERVICE_ENABLED = "pricing.pros.mock.enabled";

    private ThyssenkruppeuprospricingConstants() {
    }

}

package com.thyssenkrupp.b2b.eu.prospricing.service;

import de.hybris.platform.core.model.product.UnitModel;

import java.util.Optional;

public interface TkEuFetchPriceFactorService {

    Optional<Long> getPriceFactorFromMaterialId(String materialId, UnitModel unitCode);

}

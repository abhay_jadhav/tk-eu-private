package com.thyssenkrupp.b2b.eu.prospricing.strategy;

import java.util.Optional;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;

/**
 *
 */
public interface TkEuProsPricingResponseStrategy {

    Optional<TkEuProsPriceReponseParameter> convertProsReponseToPriceParamter(
            PriceDetailsResponseItem priceDetailsResponseItem);

}

package com.thyssenkrupp.b2b.eu.prospricing.mock.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.prospricing.mock.service.TkEuMockPROSAxisService;

public class DefaultTkEuMockPROSAxisService implements TkEuMockPROSAxisService {

    @Override
    public List<PriceDetailsResponseItem> getPROSMockPriceResponse(final PriceDetailsRecords[] priceDetailsArray) {

        final List<PriceDetailsResponseItem> responseItemList = new ArrayList<>();
        final double basePrice = 100d;
        final List<PriceDetailsRecords> priceDetailsList = Arrays.asList(priceDetailsArray);
        priceDetailsList.forEach(eachLineItemReq -> {
            final PriceDetailsResponseItem eachLineItemResponse = new PriceDetailsResponseItem();
            eachLineItemResponse.setLineItemNo(eachLineItemReq.getLineItemNo());
            eachLineItemResponse.setMaterial(eachLineItemReq.getMaterial());
            eachLineItemResponse.setNetweight(eachLineItemReq.getNetweight());
            eachLineItemResponse.setCumulativeNetWeight(eachLineItemReq.getCumulativeNetWeight());
            eachLineItemResponse.setUom(eachLineItemReq.getUom());
            eachLineItemResponse.setSalesUom(eachLineItemReq.getSalesUom());
            eachLineItemResponse.setDocCurrencyCode(eachLineItemReq.getDocCurrencyCode());
            eachLineItemResponse.setPriceDate(eachLineItemReq.getPriceDate());
            eachLineItemResponse.setPriceFactor(eachLineItemReq.getPriceFactor());
            final double calculatedPrice = returnMockTargetPrice(
                    (eachLineItemReq.getCumulativeNetWeight().doubleValue()));
            eachLineItemResponse.setQty(eachLineItemReq.getQty());

            eachLineItemResponse.setY2ZA_TargetPrice(BigDecimal.valueOf(calculatedPrice));
            eachLineItemResponse.setTargetMaterialPrice(BigDecimal.valueOf(calculatedPrice));
            eachLineItemResponse.setClient(eachLineItemReq.getClient());
            eachLineItemResponse.setSystem(eachLineItemReq.getSystem());
            eachLineItemResponse.setCustomer(eachLineItemReq.getCustomer());
            eachLineItemResponse.setDistChannel(eachLineItemReq.getDistChannel());
            eachLineItemResponse.setCustomerClassification(eachLineItemReq.getCustomerClassification());
            eachLineItemResponse.setIncoTerm(eachLineItemReq.getIncoTerm());
            eachLineItemResponse.setPaymentTerms(eachLineItemReq.getPaymentTerms());
            eachLineItemResponse.setPlant(eachLineItemReq.getPlant());
            eachLineItemResponse.setCustomerGroup1(eachLineItemReq.getCustomerGroup1());
            eachLineItemResponse.setSalesOrg(eachLineItemReq.getSalesOrg());
            responseItemList.add(eachLineItemResponse);
        });

        return responseItemList;
    }


    private double returnMockTargetPrice(final double weight) {
        if (weight > 2500) {
            return 0.6;
        }
        if (weight > 1000) {
            return 0.85;
        }
        if (weight > 500) {
            return 0.91;
        }
        if (weight > 400) {
            return 0.8;
        }
        if (weight > 200) {
            return 1.2;
        }
        if (weight > 100) {
            return 1.8;
        }
        if (weight > 50) {
            return 2.6;
        }
        if (weight > 0) {
            return 3.1;
        }
        return 0.0;
    }

    @Override
    public List<PriceDetailsResponseItem> getPROSMockPriceResponseWithError(
            final List<PriceDetailsRecords> priceDetails, final List<String> errorlines) {

        final List<PriceDetailsResponseItem> responseItemList = new ArrayList<>();
        final double basePrice = 0.5d;
        priceDetails.forEach(eachLineItemReq -> {
            final PriceDetailsResponseItem eachLineItemResponse = new PriceDetailsResponseItem();
            if (errorlines.contains(eachLineItemReq.getLineItemNo())) {
                eachLineItemResponse.setError("Mock Error for Material " + eachLineItemReq.getMaterial()
                        + " line Item no: " + eachLineItemReq.getLineItemNo());
            } else {
                eachLineItemResponse.setLineItemNo(eachLineItemReq.getLineItemNo());
                eachLineItemResponse.setMaterial(eachLineItemReq.getMaterial());
                eachLineItemResponse.setNetweight(eachLineItemReq.getNetweight());
                eachLineItemResponse.setCumulativeNetWeight(eachLineItemReq.getCumulativeNetWeight());
                eachLineItemResponse.setUom(eachLineItemReq.getUom());
                eachLineItemResponse.setSalesUom(eachLineItemReq.getSalesUom());
                eachLineItemResponse.setDocCurrencyCode(eachLineItemReq.getDocCurrencyCode());
                eachLineItemResponse.setPriceDate(eachLineItemReq.getPriceDate());
                eachLineItemResponse.setPriceFactor(eachLineItemReq.getPriceFactor());
                final double calculatedPrice = basePrice * (eachLineItemReq.getCumulativeNetWeight().doubleValue());
                eachLineItemResponse.setQty(eachLineItemReq.getQty());
                eachLineItemResponse.setY2ZA_TargetPrice(BigDecimal.valueOf(calculatedPrice));
                eachLineItemResponse.setTargetMaterialPrice(BigDecimal.valueOf(calculatedPrice));
                eachLineItemResponse.setClient(eachLineItemReq.getClient());
                eachLineItemResponse.setSystem(eachLineItemReq.getSystem());
                eachLineItemResponse.setCustomer(eachLineItemReq.getCustomer());
                eachLineItemResponse.setDistChannel(eachLineItemReq.getDistChannel());
                eachLineItemResponse.setCustomerClassification(eachLineItemReq.getCustomerClassification());
                eachLineItemResponse.setIncoTerm(eachLineItemReq.getIncoTerm());
                eachLineItemResponse.setPaymentTerms(eachLineItemReq.getPaymentTerms());
                eachLineItemResponse.setPlant(eachLineItemReq.getPlant());
                eachLineItemResponse.setCustomerGroup1(eachLineItemReq.getCustomerGroup1());
                eachLineItemResponse.setSalesOrg(eachLineItemReq.getSalesOrg());
            }
            responseItemList.add(eachLineItemResponse);
        });

        return responseItemList;
    }

    @Override
    public PriceDetailsRecords getPROSMockReqDetails(final String productCode, final long lineItem,
            final double weight) {
        final PriceDetailsRecords record = new PriceDetailsRecords();
        record.setSystem("Q32");
        record.setClient("150");
        record.setCustomer("0024216000");
        record.setSalesOrg("W111");
        record.setDistChannel("01");
        record.setPricingProc("YVAA11");
        record.setIncoTerm("FH");
        record.setCustomerClassification("B2");
        record.setSalesOffice("0470");
        record.setLineItemNo(String.valueOf(lineItem));
        record.setQty(BigDecimal.valueOf(10));
        record.setUom("KG");
        record.setNetweight(BigDecimal.valueOf(weight));
        record.setSalesUom("KG");
        record.setCumulativeNetWeight(BigDecimal.valueOf(weight));
        record.setDocCurrencyCode("EUR");
        record.setPriceDate(getCurrentDate("yyyymmdd"));
        record.setPlant("0470");
        record.setPaymentTerms("1609");
        record.setShippingCondition("01");
        record.setCustomerGroup1("01");
        record.setPriceFactor(BigInteger.valueOf(1));
        record.setMaterial(productCode);
        record.setY2BA_PriceProposal(BigDecimal.ZERO);
        record.setY2AA_FloorPrice(BigDecimal.ZERO);
        record.setY2ZA_TargetPrice(BigDecimal.ZERO);
        record.setY2SA_StartPrice(BigDecimal.ZERO);
        return record;
    }

    private String getCurrentDate(final String format) {
        final LocalDateTime now = LocalDateTime.now();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return now.format(formatter);
    }

    @Override
    public PriceDetailsRecords getPROSMockReqDetails(final TkEuPriceParameter tkEuPriceParameter) {
        final PriceDetailsRecords record = new PriceDetailsRecords();
        record.setSystem("Q32");
        record.setClient("150");
        record.setCustomer("0024216000");
        record.setSalesOrg("W111");
        record.setDistChannel("01");
        record.setPricingProc("YVAA11");
        record.setIncoTerm("FH");
        record.setCustomerClassification("B2");
        record.setSalesOffice("0470");
        if (!Objects.isNull(tkEuPriceParameter.getOrderEntry())) {
            record.setLineItemNo(String.valueOf(tkEuPriceParameter.getOrderEntry().getEntryNumber()));
        } else {
            record.setLineItemNo(String.valueOf(1));
        }
        record.setQty(BigDecimal.valueOf(10));
        record.setUom("KG");
        record.setNetweight(BigDecimal.valueOf(tkEuPriceParameter.getQuantityWrapper().getQuantity()));
        record.setSalesUom("KG");
        record.setCumulativeNetWeight(BigDecimal.valueOf(tkEuPriceParameter.getQuantityWrapper().getQuantity()));
        record.setDocCurrencyCode("EUR");
        record.setPriceDate(getCurrentDate("yyyymmdd"));
        record.setPlant("0470");
        record.setPaymentTerms("1609");
        record.setShippingCondition("01");
        record.setCustomerGroup1("01");
        record.setPriceFactor(BigInteger.ONE);
        record.setMaterial(tkEuPriceParameter.getProduct().getCode());
        record.setY2BA_PriceProposal(BigDecimal.ZERO);
        record.setY2AA_FloorPrice(BigDecimal.ZERO);
        record.setY2ZA_TargetPrice(BigDecimal.ZERO);
        record.setY2SA_StartPrice(BigDecimal.ZERO);
        return record;
    }

}

package com.thyssenkrupp.b2b.eu.prospricing.utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class TkEuProsPricingUtils {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuProsPricingUtils.class);

    private TkEuProsPricingUtils() {
        throw new UnsupportedOperationException();
    }

    public static BigDecimal getValidTargetPrice(final BigDecimal targetPrice) {
        try {
            if (null != targetPrice) {
                return targetPrice;
            }
        } catch (final Exception ex) {
            LOG.error("Invalid TagetPrice from PROS SCPI: " + targetPrice, ex);
        }
        return BigDecimal.ZERO;
    }

    public static String getCurrentDate(final String format) {
        final LocalDateTime now = LocalDateTime.now();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return now.format(formatter);
    }

    public static long calculateTimeDiff(final String processStartTime, final String processEndTime,
            final String format) {
        try {
            if (StringUtils.isNotEmpty(processStartTime) && StringUtils.isNotEmpty(processEndTime)) {
                final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
                final LocalDateTime startTime = LocalDateTime.parse(processStartTime, formatter);
                final LocalDateTime endTime = LocalDateTime.parse(processEndTime, formatter);
                return ChronoUnit.MILLIS.between(startTime, endTime);
            }
        } catch (final Exception e) {
            return -1;
        }
        return -1;
    }

}

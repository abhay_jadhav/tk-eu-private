package com.thyssenkrupp.b2b.eu.prospricing.utils;

import de.hybris.platform.sap.core.configuration.http.HTTPDestination;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.axis.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.GetPriceDetails_OutBindingStub;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.ServiceLocator;
import com.thyssenkrupp.b2b.eu.prospricing.constants.ThyssenkruppeuprospricingConstants;

/**
 *
 */
public final class TkEuProsSCPIConnectionUtils {
    private static final Logger LOG = LoggerFactory.getLogger(TkEuProsSCPIConnectionUtils.class);


    private TkEuProsSCPIConnectionUtils() {
        throw new UnsupportedOperationException();
    }

    public static GetPriceDetails_OutBindingStub getSOAPConnectionToSCPI(
            final HTTPDestination prosHttpDestinationDetails) {
        try {
            if (null != prosHttpDestinationDetails) {
            final ServiceLocator serviceAxis = new ServiceLocator();
            final GetPriceDetails_OutBindingStub stub = new GetPriceDetails_OutBindingStub(
                    new URL(prosHttpDestinationDetails.getTargetURL()),
                    serviceAxis);
            stub.setUsername(prosHttpDestinationDetails.getUserid());
            stub.setPassword(prosHttpDestinationDetails.getPassword());
            return stub;
            }
        } catch (final AxisFault e) {
            LOG.error("AxisFault Exception while connecting to PROS SCPI  Service", e);
        } catch (final MalformedURLException e) {
            LOG.error("MalformedURLException Exception while connecting to PROS SCPI  Service", e);
        } catch (final Exception e) {
            LOG.error("Exception while connecting to PROS SCPI  Service", e);
        }

        return null;
    }

    public static String debugResponse(final PriceDetailsResponseItem items) {
        final String processEndTime = TkEuProsPricingUtils
                .getCurrentDate(ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATETIMEFORMAT_DEFAULT);

        return "PROS SCPI Response: LineItemNo:" + items.getLineItemNo() + "  Customer:" + items.getCustomer()
                + "  Material:" + items.getMaterial()
                + "  TargetMaterialPrice:" + items.getTargetMaterialPrice() + " Error Message:"
                + items.getError() + " TAT Time(ms):"
                + TkEuProsPricingUtils.calculateTimeDiff(items.getProcessStartTime(), processEndTime,
                        ThyssenkruppeuprospricingConstants.PROS_PRICING_REQUEST_DATETIMEFORMAT_DEFAULT)
                + "  Qty:" + items.getQty() + "  Uom:" + items.getUom()
                + "  Netweight:" + items.getNetweight() + "  SalesUom:" + items.getSalesUom() + "  CumulativeNetweight:"
                + items.getCumulativeNetWeight() + "  DocCurrencyCode:" + items.getDocCurrencyCode() + "  PriceDate:"
                + items.getPriceDate() + "  Plant:" + items.getPlant() + "  Y2SAStartPrice:"
                + items.getY2SA_StartPrice() + "  Y2ZATargetPrice:" + items.getY2ZA_TargetPrice() + "  Y2AAFloorPrice:"
                + items.getY2AA_FloorPrice() + "  Y2BAPriceProposal:" + items.getY2BA_PriceProposal()
                + "  PaymentTerms:" + items.getPaymentTerms() + "  ShippingCondition:" + items.getShippingCondition()
                + "  CustomerGroup1:" + items.getCustomerGroup1() + "  PriceFactor:" + items.getPriceFactor()
                + "  ProcessStartTime:" + items.getProcessStartTime() + "  ProcessEndTime:" + processEndTime
                + " SCPIProcessEndTime:" + items.getProcessEndTime()
                + "  StartMaterialPrice:"
                + items.getStartMaterialPrice() + "  FloorMaterialrice:" + items.getFloorMaterialPrice()
                + " DistChannel:" + items.getDistChannel();
    }

    public static String debugRequest(final PriceDetailsRecords soapReqRecordObj) {
        return "PROS SCPI Request: System:" + soapReqRecordObj.getSystem() + " Client:" + soapReqRecordObj.getClient()
                + " Customer:" + soapReqRecordObj.getCustomer() + " SalesOrg:" + soapReqRecordObj.getSalesOrg()
                + " DistChannel:" + soapReqRecordObj.getDistChannel() + " PricingProc:"
                + soapReqRecordObj.getPricingProc() + " IncoTerms:" + soapReqRecordObj.getIncoTerm()
                + " CustomerClassification:" + soapReqRecordObj.getCustomerClassification() + " SalesOff:"
                + soapReqRecordObj.getSalesOffice() + " LineItemNo:" + soapReqRecordObj.getLineItemNo() + "  Material:"
                + soapReqRecordObj.getMaterial() + "  Qty:" + soapReqRecordObj.getQty() + "  Uom:"
                + soapReqRecordObj.getUom() + "  Netweight:" + soapReqRecordObj.getNetweight() + "  SalesUom:"
                + soapReqRecordObj.getSalesUom() + "  CumulativeNetweight:" + soapReqRecordObj.getCumulativeNetWeight()
                + "  DocCurrencyCode:" + soapReqRecordObj.getDocCurrencyCode() + "  PriceDate:"
                + soapReqRecordObj.getPriceDate() + "  Plant:" + soapReqRecordObj.getPlant() + "  Y2SAStartPrice:"
                + soapReqRecordObj.getY2SA_StartPrice() + "  Y2ZATargetPrice:" + soapReqRecordObj.getY2ZA_TargetPrice()
                + "  Y2AAFloorPrice:" + soapReqRecordObj.getY2AA_FloorPrice() + "  Y2BAPriceProposal:"
                + soapReqRecordObj.getY2BA_PriceProposal() + "  PaymentTerms:" + soapReqRecordObj.getPaymentTerms()
                + "  ShippingCondition:" + soapReqRecordObj.getShippingCondition() + "  CustomerGroup1:"
                + soapReqRecordObj.getCustomerGroup1() + "  PriceFactor:" + soapReqRecordObj.getPriceFactor()
                + "  ProcessStartTime:" + soapReqRecordObj.getProcessStartTime() + "  ProcessEndTime:"
                + soapReqRecordObj.getProcessEndTime() + "  TargetMaterialPrice:" + "  FloorMaterialrice:";
    }
}

package com.thyssenkrupp.b2b.eu.prospricing.strategy.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsResponseItem;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;
import com.thyssenkrupp.b2b.eu.prospricing.strategy.TkEuProsPricingResponseStrategy;
import com.thyssenkrupp.b2b.eu.prospricing.utils.TkEuProsPricingUtils;
import com.thyssenkrupp.b2b.eu.prospricing.utils.TkEuProsSCPIConnectionUtils;

/**
 *
 */
public class DefaultTkEuProsPricingResponseStrategy implements TkEuProsPricingResponseStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuProsPricingResponseStrategy.class);

    @Override
    public Optional<TkEuProsPriceReponseParameter> convertProsReponseToPriceParamter(
            final PriceDetailsResponseItem priceDetailsResponseItem) {
        try {

            LOG.info(TkEuProsSCPIConnectionUtils.debugResponse(priceDetailsResponseItem));
            final TkEuProsPriceReponseParameter tkEuProsPriceReponseParameterObj = new TkEuProsPriceReponseParameter();
            tkEuProsPriceReponseParameterObj.setEntryNumber(priceDetailsResponseItem.getLineItemNo());
            tkEuProsPriceReponseParameterObj.setProductCode(priceDetailsResponseItem.getMaterial());
            tkEuProsPriceReponseParameterObj.setErrorMessage(priceDetailsResponseItem.getError());
            tkEuProsPriceReponseParameterObj.setTargetPrice(
                    TkEuProsPricingUtils.getValidTargetPrice(priceDetailsResponseItem.getTargetMaterialPrice()));
            tkEuProsPriceReponseParameterObj
                    .setPriceFactor(null != priceDetailsResponseItem.getPriceFactor()
                            ? priceDetailsResponseItem.getPriceFactor().longValue()
                            : 1L);
            return Optional.of(tkEuProsPriceReponseParameterObj);
        } catch (final Exception e) {
            LOG.error("Exception in convertProsReponseToPriceParamter from PROS SCPI: ", e);
        }

        return Optional.empty();
    }
}

package com.thyssenkrupp.b2b.eu.prospricing.service;

import java.util.Optional;

import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;
import com.thyssenkrupp.b2b.eu.prospricing.data.TkEuProsPriceReponseParameter;

public interface TkEuProsPricingService {
    void createMockRequestAndTestAxisSCPIPROSResponse(TkEuPriceParameter tkEuPriceParameter,
            boolean mockActivated);

    Optional<TkEuProsPriceReponseParameter> getProsPriceForProduct(TkEuPriceParameter tkEuPriceParameter);

    /**
     *
     */
    Optional<TkEuProsPriceReponseParameter> getProsPriceForOrderEntry(TkEuPriceParameter tkEuPriceParameter,
            MaterialQuantityWrapper quantityWrapper);
}

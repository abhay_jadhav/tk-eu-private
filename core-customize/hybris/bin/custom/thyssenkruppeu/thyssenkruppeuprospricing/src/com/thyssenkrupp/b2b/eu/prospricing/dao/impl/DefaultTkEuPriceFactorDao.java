package com.thyssenkrupp.b2b.eu.prospricing.dao.impl;

import static com.thyssenkrupp.b2b.eu.prospricing.model.PriceFactorConfigModel.MATERIALID;
import static com.thyssenkrupp.b2b.eu.prospricing.model.PriceFactorConfigModel._TYPECODE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thyssenkrupp.b2b.eu.prospricing.model.PriceFactorConfigModel;

public class DefaultTkEuPriceFactorDao {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTkEuPriceFactorDao.class);
    private static final String QUERY_BY_MATERIALID = "select {pk} from {" + _TYPECODE + "} where {" + MATERIALID
            + "} = ?materialId ";
    private FlexibleSearchService flexibleSearchService;

    public List<PriceFactorConfigModel> findPriceFactorByMaterialId(final String materialId) {
        validateParameterNotNull(materialId, "materialGroup must not be null!");
        final Map<String, Object> params = new HashMap<>();
        final SearchResult<PriceFactorConfigModel> result;
        params.put(MATERIALID, materialId);
        result = getFlexibleSearchService().search(QUERY_BY_MATERIALID, params);
        LOG.debug("Queried table \'PriceFactor\' for params: materialId \'{}\'. Found {} results.", materialId,
                result.getResult().size());
        if (result.getResult().size() > 0) {
            final PriceFactorConfigModel priceFactorModel = result.getResult().get(0);
            LOG.debug("In table \'PriceFactor\' for params: materialId \'{}\'. we have price Factor {}.", materialId,
                    priceFactorModel.getPriceFactor());
        }

        if (result.getResult().size() == 0) {
            LOG.debug("In table \'PriceFactor\' for params: materialId \'{}\'. no Price factor found.", materialId,
                    result.getResult().size());
        }
        return result.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}

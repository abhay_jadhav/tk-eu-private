package com.thyssenkrupp.b2b.eu.prospricing.strategy;

import com.thyssenkrupp.ESR.PI.ERP_OTC.ERP.PROS._3015_GetPriceDetails.PriceDetailsRecords;
import com.thyssenkrupp.b2b.eu.pricedata.data.TkEuPriceParameter;
import com.thyssenkrupp.b2b.eu.pricedata.service.MaterialQuantityWrapper;

public interface TkEuProsPricingRequestStrategy {
    PriceDetailsRecords[] convertPriceParamterToSCPIRequest(TkEuPriceParameter tkEuPriceParameter);

    PriceDetailsRecords[] convertPriceParamterToSCPIRequestForOrderEntry(TkEuPriceParameter tkEuPriceParameter,
            MaterialQuantityWrapper quantityWrapper);

}

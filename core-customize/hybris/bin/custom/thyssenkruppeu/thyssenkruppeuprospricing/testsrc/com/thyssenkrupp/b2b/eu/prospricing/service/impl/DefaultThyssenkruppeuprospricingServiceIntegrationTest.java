package com.thyssenkrupp.b2b.eu.prospricing.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.thyssenkrupp.b2b.eu.prospricing.service.TkEuProsPricingService;

@IntegrationTest
public class DefaultThyssenkruppeuprospricingServiceIntegrationTest extends ServicelayerBaseTest {
    @Resource
    private TkEuProsPricingService tkEuProsPricingService;
    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldReturnProperUrlForLogo() throws Exception {
    }
}

package com.thyssenkrupp.b2b.eu.mediaconversion.service.impl;

import com.thyssenkrupp.b2b.eu.mediaconversion.service.ThyssenkruppeumediaconversionService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static com.thyssenkrupp.b2b.eu.mediaconversion.constants.ThyssenkruppeumediaconversionConstants.PLATFORM_LOGO_CODE;
import static org.fest.assertions.Assertions.assertThat;

@IntegrationTest
public class DefaultThyssenkruppeumediaconversionServiceIntegrationTest extends ServicelayerBaseTest {
    @Resource
    private ThyssenkruppeumediaconversionService thyssenkruppeumediaconversionService;
    @Resource
    private FlexibleSearchService                flexibleSearchService;

    @Before
    public void setUp() throws Exception {
        thyssenkruppeumediaconversionService.createLogo(PLATFORM_LOGO_CODE);
    }

    @Test
    public void shouldReturnProperUrlForLogo() throws Exception {
        // given
        final String logoCode = "thyssenkruppeumediaconversionPlatformLogo";

        // when
        final String logoUrl = thyssenkruppeumediaconversionService.getHybrisLogoUrl(logoCode);

        // then
        assertThat(logoUrl).isNotNull();
        assertThat(logoUrl).isEqualTo(findLogoMedia(logoCode).getURL());
    }

    private MediaModel findLogoMedia(final String logoCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {PK} FROM {Media} WHERE {code}=?code");
        fQuery.addQueryParameter("code", logoCode);

        return flexibleSearchService.searchUnique(fQuery);
    }
}

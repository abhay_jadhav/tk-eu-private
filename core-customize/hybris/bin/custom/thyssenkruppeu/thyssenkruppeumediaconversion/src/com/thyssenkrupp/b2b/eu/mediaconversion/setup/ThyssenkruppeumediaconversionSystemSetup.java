package com.thyssenkrupp.b2b.eu.mediaconversion.setup;

import com.thyssenkrupp.b2b.eu.mediaconversion.constants.ThyssenkruppeumediaconversionConstants;
import com.thyssenkrupp.b2b.eu.mediaconversion.service.ThyssenkruppeumediaconversionService;
import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import static com.thyssenkrupp.b2b.eu.mediaconversion.constants.ThyssenkruppeumediaconversionConstants.PLATFORM_LOGO_CODE;

@SystemSetup(extension = ThyssenkruppeumediaconversionConstants.EXTENSIONNAME)
public class ThyssenkruppeumediaconversionSystemSetup {
    private final ThyssenkruppeumediaconversionService thyssenkruppeumediaconversionService;

    public ThyssenkruppeumediaconversionSystemSetup(final ThyssenkruppeumediaconversionService thyssenkruppeumediaconversionService) {
        this.thyssenkruppeumediaconversionService = thyssenkruppeumediaconversionService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
        thyssenkruppeumediaconversionService.createLogo(PLATFORM_LOGO_CODE);
    }

    private InputStream getImageStream() {
        return ThyssenkruppeumediaconversionSystemSetup.class.getResourceAsStream("/thyssenkruppeumediaconversion/sap-hybris-platform.png");
    }
}

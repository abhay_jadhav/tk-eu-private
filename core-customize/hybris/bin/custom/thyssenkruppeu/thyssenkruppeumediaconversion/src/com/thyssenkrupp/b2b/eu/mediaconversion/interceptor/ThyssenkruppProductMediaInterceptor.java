package com.thyssenkrupp.b2b.eu.mediaconversion.interceptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.thyssenkrupp.b2b.eu.mediaconversion.service.ThyssenkruppeumediaconversionService;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

public class ThyssenkruppProductMediaInterceptor implements PrepareInterceptor {

    private static final String MASTER_MEDIA = "masterMedia";
    private static final String PRODUCT_CATALOG_STAGED = "Staged";

    private ThyssenkruppeumediaconversionService thyssenkruppeumediaconversionService;

    @Override
    public void onPrepare(Object object, InterceptorContext interceptorContext) throws InterceptorException {

        ProductModel productModel = (ProductModel) object;
        if (productModel.getRescaleMedia() != null) {
            if (PRODUCT_CATALOG_STAGED.equalsIgnoreCase(productModel.getCatalogVersion().getVersion()) &&  (isProductMediaUpdated(productModel, interceptorContext) || productModel.getRescaleMedia())){
                cleanUpProductGalleries(productModel);
                createMediaContainers(productModel);
                scaleMediaContainerList(productModel);
                productModel.setRescaleMedia(false);
            }
        } else {
            productModel.setRescaleMedia(false);
        }
        interceptorContext.registerElementFor(object, PersistenceOperation.SAVE);
    }

    private void createMediaContainers(ProductModel productModel) {
        final List<MediaContainerModel> containerList = new ArrayList<>();
        final List<MediaModel> processedMedia = new ArrayList<>();
        final Collection<MediaModel> masterMedias = productModel.getMasterMedia();
        if (CollectionUtils.isNotEmpty(masterMedias)) {
            for (MediaModel masterMediaModel : masterMedias) {
                if (!processedMedia.contains(masterMediaModel)) {
                    MediaContainerModel mediaContainerModel = thyssenkruppeumediaconversionService.createMediaContainerFromMasterForProduct(masterMediaModel, productModel);
                    thyssenkruppeumediaconversionService.cloneMasterMediaForProduct(masterMediaModel, productModel, mediaContainerModel);
                    containerList.add(mediaContainerModel);
                    processedMedia.add(masterMediaModel);
                }
            }
        }
        productModel.setGalleryImages(containerList);
    }

    private void cleanUpProductGalleries(ProductModel productModel) {
        List<MediaContainerModel> galleryImages = productModel.getGalleryImages();
        if (CollectionUtils.isNotEmpty(galleryImages)) {
            for (MediaContainerModel galleryImage : galleryImages) {
                thyssenkruppeumediaconversionService.removeMediaContainerAndMedia(galleryImage);
            }
        }
    }

    private void scaleMediaContainerList(ProductModel productModel) {
        thyssenkruppeumediaconversionService.scaleProductMediaContainer(productModel);
    }

    boolean isProductMediaUpdated(ProductModel productModel, InterceptorContext ctx) {
        return ctx.isModified(productModel, MASTER_MEDIA);
    }

    public void setThyssenkruppeumediaconversionService(ThyssenkruppeumediaconversionService thyssenkruppeumediaconversionService) {
        this.thyssenkruppeumediaconversionService = thyssenkruppeumediaconversionService;
    }
}

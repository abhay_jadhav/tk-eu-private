package com.thyssenkrupp.b2b.eu.mediaconversion.dao;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface TkEuMediaConversionDao {
    List<MediaContainerModel> getGalleryImages(ProductModel productModel);
}

package com.thyssenkrupp.b2b.eu.mediaconversion;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.Utilities;

public class ThyssenkruppeumediaconversionStandalone {
    public static void main(final String[] args) {
        new ThyssenkruppeumediaconversionStandalone().run();
    }

    public void run() {
        Registry.activateStandaloneMode();
        Registry.activateMasterTenant();

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        System.out.println("Session ID: " + jaloSession.getSessionID());
        System.out.println("User: " + jaloSession.getUser());
        Utilities.printAppInfo();

        RedeployUtilities.shutdown();
    }
}

package com.thyssenkrupp.b2b.eu.mediaconversion.service;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;

import java.util.List;

public interface ThyssenkruppeumediaconversionService {
    String getHybrisLogoUrl(String logoCode);

    void createLogo(String logoCode);

    ConversionGroupModel getConversionGroup();

    ProductModel scaleProductMediaContainer(ProductModel productModel);

    void removeMediaContainerAndMedia(MediaContainerModel mediaContainerModel);

    MediaModel cloneMasterMediaForProduct(MediaModel mediaModel, ProductModel productModel, MediaContainerModel mediaContainerModel);

    MediaContainerModel createMediaContainerFromMasterForProduct(MediaModel newMediaModel, ProductModel productModel);

    List<MediaContainerModel> retrieveSavedGalleryImages(ProductModel productModel);
}

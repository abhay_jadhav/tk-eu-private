package com.thyssenkrupp.b2b.eu.mediaconversion.service.impl;

import com.thyssenkrupp.b2b.eu.mediaconversion.dao.TkEuMediaConversionDao;
import com.thyssenkrupp.b2b.eu.mediaconversion.service.ThyssenkruppeumediaconversionService;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.mediaconversion.MediaConversionService;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.mediaconversion.model.ConversionMediaFormatModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class DefaultThyssenkruppeumediaconversionService implements ThyssenkruppeumediaconversionService {
    private static final Logger LOG                         = LoggerFactory.getLogger(DefaultThyssenkruppeumediaconversionService.class);
    private static final String FIND_LOGO_QUERY             = "SELECT {" + CatalogUnawareMediaModel.PK + "} FROM {"
        + CatalogUnawareMediaModel._TYPECODE + "} WHERE {" + CatalogUnawareMediaModel.CODE + "}=?code";
    private static final String FIND_CONVERSION_GROUP_QUERY = "SELECT {" + ConversionGroupModel.PK + "} FROM {"
        + ConversionGroupModel._TYPECODE + "} WHERE {" + ConversionGroupModel.CODE + "}=?code";

    private static final String FORMAT_30W_30H     = "30Wx30H";
    private static final String FORMAT_65W_65H     = "65Wx65H";
    private static final String FORMAT_96W_96H     = "96Wx96H";
    private static final String FORMAT_300W_300H   = "300Wx300H";
    private static final String FORMAT_515W_515H   = "515Wx515H";
    private static final String FORMAT_1200W_1200H = "1200Wx1200H";

    private static final String CONVERSION_GROUP_CODE = "DefaultConversionGroup";
    private MediaService           mediaService;
    private ModelService           modelService;
    private FlexibleSearchService  flexibleSearchService;
    private MediaConversionService mediaConversionService;
    private TkEuMediaConversionDao tkEuMediaConversionDao;

    @Override
    public String getHybrisLogoUrl(final String logoCode) {
        final MediaModel media = mediaService.getMedia(logoCode);

        // Keep in mind that with Slf4j you don't need to check if debug is enabled, it is done under the hood.
        LOG.debug("Found media [code: {}]", media.getCode());

        return media.getURL();
    }

    @Override
    public void createLogo(final String logoCode) {
        final Optional<CatalogUnawareMediaModel> existingLogo = findExistingLogo(logoCode);

        final CatalogUnawareMediaModel media = existingLogo.isPresent() ? existingLogo.get()
            : modelService.create(CatalogUnawareMediaModel.class);
        media.setCode(logoCode);
        media.setRealFileName("sap-hybris-platform.png");
        modelService.save(media);

        mediaService.setStreamForMedia(media, getImageStream());
    }

    @Override
    public ConversionGroupModel getConversionGroup() {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_CONVERSION_GROUP_QUERY);
        fQuery.addQueryParameter("code", CONVERSION_GROUP_CODE);

        try {
            return flexibleSearchService.searchUnique(fQuery);
        } catch (final SystemException e) {
            return null;
        }
    }

    @Override
    public ProductModel scaleProductMediaContainer(ProductModel productModel) {
        Collection<MediaModel> thumbnailMedias = new ArrayList<>();
        Collection<MediaModel> otherMedias = new ArrayList<>();
        Collection<MediaModel> normalMedias = new ArrayList<>();
        Collection<MediaModel> detailMedias = new ArrayList<>();
        initialiseProductMedia(productModel);

        List<MediaContainerModel> productMediaContainers = productModel.getGalleryImages();
        for (MediaContainerModel mediaContainer : productMediaContainers) {
            if (mediaContainer != null && mediaContainer.getConversionGroup() != null) {
                autoScaleMediaContainer(mediaContainer, productModel, thumbnailMedias, otherMedias, normalMedias, detailMedias);
            }
        }
        setProductMedia(productModel, thumbnailMedias, otherMedias, normalMedias, detailMedias);
        return productModel;
    }

    private void autoScaleMediaContainer(MediaContainerModel mediaContainer, ProductModel productModel, Collection<MediaModel> thumbnailMedias, Collection<MediaModel> otherMedias, Collection<MediaModel> normalMedias, Collection<MediaModel> detailMedias) {
        Collection<ConversionMediaFormatModel> mediaFormats = mediaContainer.getConversionGroup().getSupportedFormats();
        for (ConversionMediaFormatModel mediaFormat : mediaFormats) {
            if (mediaFormat != null) {
                autoScaleAndAssignMedia(mediaContainer, mediaFormat, productModel, thumbnailMedias, otherMedias, normalMedias, detailMedias);
            }
        }
    }

    private void initialiseProductMedia(ProductModel productModel) {
        productModel.setThumbnails(null);
        productModel.setOthers(null);
        productModel.setNormal(null);
        productModel.setDetail(null);
        productModel.setThumbnail(null);
        productModel.setPicture(null);
    }

    @Override
    public void removeMediaContainerAndMedia(MediaContainerModel mediaContainerModel) {
        for (MediaModel containerMedia : mediaContainerModel.getMedias()) {
            modelService.remove(containerMedia);
        }
        modelService.remove(mediaContainerModel);
    }

    @Override
    public MediaModel cloneMasterMediaForProduct(MediaModel mediaModel, ProductModel productModel, MediaContainerModel mediaContainerModel) {
        MediaModel newMediaModel = modelService.clone(mediaModel);
        newMediaModel.setCode(productModel.getCode() + "-" + mediaModel.getCode());
        newMediaModel.setMediaContainer(mediaContainerModel);
        newMediaModel.setCatalogVersion(productModel.getCatalogVersion());
        newMediaModel.setOwner(mediaContainerModel);
        modelService.save(newMediaModel);
        return newMediaModel;
    }

    @Override
    public MediaContainerModel createMediaContainerFromMasterForProduct(MediaModel masterMediaModel, ProductModel productModel) {

        MediaContainerModel mediaContainerModel = modelService.create(MediaContainerModel.class);

        mediaContainerModel.setOwner(productModel);
        mediaContainerModel.setQualifier("MC_" + productModel.getCode() + "_" + masterMediaModel.getCode());
        mediaContainerModel.setCatalogVersion(productModel.getCatalogVersion());
        mediaContainerModel.setConversionGroup(getConversionGroup());

        modelService.save(mediaContainerModel);

        return mediaContainerModel;
    }

    @Override
    public List<MediaContainerModel> retrieveSavedGalleryImages(ProductModel productModel) {
        return tkEuMediaConversionDao.getGalleryImages(productModel);
    }

    private void setProductMedia(ProductModel productModel, Collection<MediaModel> thumbnailMedias, Collection<MediaModel> otherMedias, Collection<MediaModel> normalMedias, Collection<MediaModel> detailMedias) {
        productModel.setThumbnails(thumbnailMedias);
        productModel.setOthers(otherMedias);
        productModel.setNormal(normalMedias);
        productModel.setDetail(detailMedias);
    }

    private void autoScaleAndAssignMedia(MediaContainerModel mediaContainer, ConversionMediaFormatModel mediaFormat, ProductModel productModel, Collection<MediaModel> thumbnailMedias, Collection<MediaModel> otherMedias, Collection<MediaModel> normalMedias, Collection<MediaModel> detailMedias) {
        final String qualifier = mediaFormat.getQualifier();

        MediaModel mediaModel = mediaConversionService.getOrConvert(mediaContainer, mediaFormat);
        if (FORMAT_30W_30H.equals(qualifier) || FORMAT_65W_65H.equals(qualifier) || FORMAT_515W_515H.equals(qualifier)) {
            otherMedias.add(mediaModel);
        } else if (FORMAT_96W_96H.equals(qualifier)) {
            thumbnailMedias.add(mediaModel);
        } else if (FORMAT_300W_300H.equals(qualifier)) {
            normalMedias.add(mediaModel);
        } else if (FORMAT_1200W_1200H.equals(qualifier)) {
            detailMedias.add(mediaModel);
        }
        setThumbnailAndPicture(qualifier, mediaModel, productModel);
    }

    private void setThumbnailAndPicture(String qualifier, MediaModel mediaModel, ProductModel productModel) {
        if (FORMAT_96W_96H.equals(qualifier) && productModel.getThumbnail() == null) {
            productModel.setThumbnail(mediaModel);
        } else if (FORMAT_300W_300H.equals(qualifier) && productModel.getPicture() == null) {
            productModel.setPicture(mediaModel);
        }
    }

    private Optional<CatalogUnawareMediaModel> findExistingLogo(final String logoCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_LOGO_QUERY);
        fQuery.addQueryParameter("code", logoCode);

        try {
            return Optional.of(flexibleSearchService.searchUnique(fQuery));
        } catch (final SystemException e) {
            return Optional.empty();
        }
    }

    private InputStream getImageStream() {
        return DefaultThyssenkruppeumediaconversionService.class.getResourceAsStream("/thyssenkruppeumediaconversion/sap-hybris-platform.png");
    }

    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public void setMediaConversionService(MediaConversionService mediaConversionService) {
        this.mediaConversionService = mediaConversionService;
    }

    public void setTkEuMediaConversionDao(TkEuMediaConversionDao tkEuMediaConversionDao) {
        this.tkEuMediaConversionDao = tkEuMediaConversionDao;
    }
}

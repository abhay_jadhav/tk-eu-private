package com.thyssenkrupp.b2b.eu.mediaconversion.constants;

public final class ThyssenkruppeumediaconversionConstants extends GeneratedThyssenkruppeumediaconversionConstants {
    public static final String EXTENSIONNAME      = "thyssenkruppeumediaconversion";
    public static final String PLATFORM_LOGO_CODE = "thyssenkruppeumediaconversionPlatformLogo";

    private ThyssenkruppeumediaconversionConstants() {
        //empty to avoid instantiating this constant class
    }
}

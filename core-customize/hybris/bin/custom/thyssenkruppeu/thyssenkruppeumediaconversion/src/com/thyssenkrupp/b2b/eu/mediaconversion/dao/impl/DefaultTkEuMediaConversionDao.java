package com.thyssenkrupp.b2b.eu.mediaconversion.dao.impl;

import com.thyssenkrupp.b2b.eu.mediaconversion.dao.TkEuMediaConversionDao;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.media.dao.impl.DefaultMediaContainerDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

public class DefaultTkEuMediaConversionDao extends DefaultMediaContainerDao implements TkEuMediaConversionDao {

    @Override
    public List<MediaContainerModel> getGalleryImages(ProductModel productModel) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT {").append("pk").append("} ");
        builder.append("FROM {").append("MediaContainer").append("} ");
        builder.append("WHERE {").append("qualifier").append("} LIKE ").append("'%").append(productModel.getCode()).append("%'");
        SearchResult<MediaContainerModel> result = getFlexibleSearchService().search(builder.toString());
        return result.getResult();
    }
}

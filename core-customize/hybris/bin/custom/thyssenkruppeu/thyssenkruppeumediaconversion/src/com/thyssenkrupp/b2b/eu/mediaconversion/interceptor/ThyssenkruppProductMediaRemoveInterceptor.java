package com.thyssenkrupp.b2b.eu.mediaconversion.interceptor;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;

public class ThyssenkruppProductMediaRemoveInterceptor implements RemoveInterceptor {

    private ModelService modelService;

    @Override
    public void onRemove(Object object, InterceptorContext interceptorContext) throws InterceptorException {
        if (object instanceof MediaContainerModel) {
            final MediaContainerModel mediaContainer = (MediaContainerModel) object;
            Collection<MediaModel> allMedias = mediaContainer.getMedias();
            modelService.removeAll(allMedias);
        }
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
